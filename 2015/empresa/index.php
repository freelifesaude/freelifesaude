<?php 
  require_once("../comum/logout.php");  
  require_once("../comum/apagaArquivos.php");  
  require_once("../comum/autoload.php");
    
  $seg->secureSessionStart();
    
  $_SESSION['sistema'] = 'Empresa';
  
  require_once("../comum/config.php");
  
  $bd   = new Oracle();  
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/index.html");
  
  $entrou = "";
  
  if (isset($_POST['entrar'])) {
    if ($_SESSION['apelido_operadora'] == 'saudemed')  
      $operador = $formata->somenteCaracteres(strtoupper(htmlentities($_POST['operador'])),$caracteres = 'ABCDEFGHIJKLMNOPQRSTUVXYWZ 01234567890') ;  
    else
      $operador = strtoupper(htmlentities($_POST['operador']));  
      
    $senha      = htmlentities($_POST['senha']);
  
    $tpl->OPERADOR = $operador;
  
    if ($operador <> '' and $senha <> '') {  
      $tentativas = $seg->bloqueiaLogin($operador);
      
      if ( $tentativas < 6 ) {
            
        $sql = new Query();
        $txt = "SELECT COUNT(*) QTDE,SEGUSUA.NNUMEUSUA, CSENHUSUA,TRUNC(SYSDATE) - DVENSUSUA DIAS_VENC_SENHA,
                       CSVENUSUA,CNOMEUSUA,INITCAP(CNCOMUSUA) CNCOMUSUA,CCRIPEMPR,CSENEINTER
                  FROM SEGUSUA,HSSOPTIT,HSSTITU,HSSINTER,FINEMPR
                 WHERE SEGUSUA.CSTATUSUA = 'A'
                   AND UPPER(SEGUSUA.CNOMEUSUA) = :operador
                   AND SEGUSUA.NNUMEUSUA = HSSOPTIT.NNUMEUSUA
                   AND HSSOPTIT.NNUMETITU = HSSTITU.NNUMETITU                    
				   AND ( (HSSTITU.CSITUTITU = 'A') OR ( (CSITUTITU <> 'A') AND (DSITUTITU + :dias > SYSDATE)) )
                   AND HSSOPTIT.NNUMETITU IN (SELECT HSSOPTIT.NNUMETITU FROM HSSSETOR,HSSOPTIT 
                                               WHERE CSITUSETOR = 'A'
                                                 AND HSSOPTIT.NNUMEUSUA = SEGUSUA.NNUMEUSUA                                                       
                                                 AND HSSOPTIT.NNUMESETOR = HSSSETOR.NNUMESETOR
                                               UNION ALL
                                              SELECT NNUMETITU FROM HSSOPTIT
                                               WHERE NNUMESETOR IS NULL
                                                 AND HSSOPTIT.NNUMEUSUA = SEGUSUA.NNUMEUSUA
                                               )
                 GROUP BY SEGUSUA.NNUMEUSUA, CSENHUSUA,TRUNC(SYSDATE) - DVENSUSUA,
                          CSVENUSUA,CNOMEUSUA,CNCOMUSUA,CCRIPEMPR,CSENEINTER ";
        $sql->addParam(":operador",$operador);
        $sql->addParam(":dias",$_SESSION['qtde_dias_acesso_cancelado']);
        $sql->executeQuery($txt);
                       
        $_SESSION['id_operador']        = $sql->result("NNUMEUSUA");             
        $_SESSION['logando']            = "PJ";
        if ($senha <> "" and 
           (($sql->result("CCRIPEMPR") == "N" and strToUpper($sql->result("CSENHUSUA")) == strToUpper($senha)) or
            ($sql->result("CCRIPEMPR") == "S" and $sql->result("CSENHUSUA") == md5($senha)) or
            ($sql->result("CSENEINTER") == md5($senha) and $seg->permissaoOutros($bd,"WEBEMPRESASENHAMASTER",false)))) { 
            
          $idSessao = $seg->geraID($sql->result("NNUMEUSUA"));
          $_SESSION['idSessao'] = $idSessao;
          $_SESSION['sistemaLogado']      = $_SESSION['sistema'];
          $_SESSION['id_operador']        = $sql->result("NNUMEUSUA");
          $_SESSION['nome_operador']      = $sql->result("CNCOMUSUA");		  
          $_SESSION['numero_contratos']   = $sql->result("QTDE");
          
          if ( ($sql->result('DIAS_VENC_SENHA') >= 0) and ($sql->result('CSVENUSUA') == "S") ) {
            $_SESSION['troca_de_senha'] = 'Automatica';
            $entrou = "novasenha";
            //$util->redireciona("../comum/trocaSenha.php?idSessao=".$idSessao);            
          } 
		      else {      
            $seg->registraLog('LOG WEB EMPRESA');
            $entrou = "principal";
            //$util->redireciona("principal.php?idSessao=".$idSessao);
          }
          
  /*        
          $_SESSION['operador']           = $sql->result("CNOMEUSUA");
          $_SESSION['id_estabelecimento'] = $sql->result("NNUMEESTAB");
          
          */
        }
        else {
          $seg->registraLogin($operador);
          
          $_SESSION['idSessao']    = '';
          $_SESSION['id_operador'] = 0;
          
          $texto_msg  = "Login incorreto ou senha incorreta.";
                        
          if ($tentativas > 0)
            $texto_msg .= " Tentativa ".$tentativas." de 5.";

          $tpl->MSG_ERRO = $texto_msg;
          $tpl->block("MOSTRA_ERRO");        
        }
      }
      else {       
        $tpl->MSG_ERRO = "Voc� excedeu o n�mero de tentativas (5). Por favor aguarde alguns instantes e tente novamente.";
        $tpl->block("MOSTRA_ERRO");      
      }      
    }
  }
  else {
    if (isset($_GET['erro'])) {
      if ($_GET['erro'] == -1) {
        $tpl->MSG_ERRO = "Esta � uma �rea exclusiva. � preciso identificar-se antes de acess�-la.";
        $tpl->block("MOSTRA_ERRO"); 
      }
    }
  }

  //if ($_SESSION['apelido_operadora'] == 'saudemed') {
  //  $tpl->DESC_OPERADOR   = "CNPJ / CPF";  
  //} else {    
    $tpl->DESC_OPERADOR   = "Login";
 // }
  
  $tpl->DEFINICAO_SENHA   = 'S';
  $tpl->TIPO_SENHA        = "password";
  $tpl->TEXTO_SENHA       = "Senha";
	$tpl->MASCARA_OPERADOR = ""; 
  $tpl->FRASE_AUXILIAR   = "";  
    
  if ($_SESSION['apelido_operadora'] <> 'coamo')
    $tpl->block("RECUPERA_SENHA");	  

  $bd->close();
  if ($entrou == "novasenha")
    $util->redireciona("../comum/trocaSenha.php?idSessao=".$idSessao);            
  else if($entrou == "principal")
    $util->redireciona("principal.php?idSessao=".$idSessao);
  else 
    $tpl->show();    
?>