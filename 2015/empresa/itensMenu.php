<?php    
  if (substr(phpversion(),0,3) == '5.4') {
    if(session_status() != 2)
      session_start();
  }
  else
    session_start();
    
  /****************** A T E N � � O **********************************/
  /*   Ao adicionar um item no menu, atentar-se em corrigir o indice.
  /*  
  /*******************************************************************/    
    
  $menu          = array();
  $utilitarios   = array();
  $corpo_clinico = array();
  $subpcmso      = array();
    
  if (isset($_SESSION['idSessao']) and isset($_SESSION['id_contrato'])) {
    if($_SESSION['apelido_operadora'] == 'carloschagaspcmso'){
	  
	  $autorizacao[0]['nome'] = "Autoriza��o para atendimento";
      $autorizacao[0]['valor'] = "../empresa/autorizacao.php";
	      
	  $configuracoes[0]['nome'] = "Alterar dados do operador";
	  $configuracoes[0]['valor'] = "../comum/alterarOperador.php"; 
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESATROCARSENHA",true)) {     
		$configuracoes[1]['nome'] = "Alterar senha";
		$configuracoes[1]['valor'] = "../comum/trocaSenha.php"; 
	  }
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESACONSULTALOGS",true)) {  
		$configuracoes[2]['nome'] = "Logs";
		$configuracoes[2]['valor'] = "../comum/listaLogs.php";   
	  }     
	  
	  /**** Nivel 0 ****/
	  
	  $menu[0]['nome'] = "<i class='fa fa-home'></i> Home";  
	  $menu[0]['valor'] = "../empresa/principal.php";
	  
	  if (sizeOf($autorizacao) > 0) {
		$menu[1]['nome'] = "Medicina ocupacional";  
		$menu[1]['valor'] = $autorizacao;  
	  }		
	  
	  if (sizeOf($configuracoes) > 0) {
		$menu[2]['nome'] = "Configura��es";  
		$menu[2]['valor'] = $configuracoes;  
	  }		  
	  
	  $menu[3]['nome'] = "Sair";  
	  $menu[3]['valor'] = "../empresa/index.php";
	}
	else{
	  $carteirinha      = ($seg->permissaoOutros($bd,"WEBEMPRESASEGUNDAVIACARTEIRA") and ($_SESSION['situacao_contrato'] == 'A'));
	  $inclusao         = (($seg->permissaoOutros($bd,"WEBEMPRESAINCLUSODEBENEFICIRIO") or
						              $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO")) and ($_SESSION['situacao_contrato'] == 'A'));
	  $alteracao        = (($seg->permissaoOutros($bd,"WEBEMPRESAALTERAODEBENEFICIRIO") or
						              $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO")) and ($_SESSION['situacao_contrato'] == 'A'));
	  $cancelamento     = (($seg->permissaoOutros($bd,"WEBEMPRESACANCELAMENTODEBENEFICIRIO") or
						              $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO")) and ($_SESSION['situacao_contrato'] == 'A'));
	  $rel_cadastro     = (($seg->permissaoOutros($bd,"WEBEMPRESARELATORIODECADASTRO") or
						              $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO")) and ($_SESSION['situacao_contrato'] == 'A'));
	  $direito_cpf      = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOCPF",false);
	  $direito_csus     = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOCARTOSUS",false);
	  $direito_ndnv     = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTODECLARAONASCIDOVIVO",false);
	  $direito_tel      = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOTELEFONE",false);
	  
	  $utiloutro      = $seg->permissaoOutros($bd,"WEBEMPRESAUTILITRIOOUTROS",false);
  
	  /**** Nivel 2 ****/
  	  if ($seg->permissaoOutros($bd,"WEBEMPRESACONSULTACOMUNICADO",true)) {     
		$comunicados[0]['nome'] = "Comunicados";
		$comunicados[0]['valor'] = "../empresa/comunicados.php"; 
	  }
	  /** Cadastro **/  
	  if ($carteirinha) {
		$cadastro[0]['nome'] = "Carteirinhas solicitadas";
		$cadastro[0]['valor'] = "../empresa/relCarteiras.php";    
	  }
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAPOPULACAODEUSURIOS")) {
		$cadastro[1]['nome'] = "Popula��o de usu�rios";
		$cadastro[1]['valor'] = "../empresa/relTotalizacao.php"; 
	  }
  
	  if($_SESSION['contrato_pcmso'] == 'S'){
	    $subpcmso[0]['nome'] = "Exames vencidos";
		  $subpcmso[0]['valor'] = "examesVencidos.php";
	  }  
  
  
	  if ($inclusao or $cancelamento or $alteracao or $rel_cadastro) {  
		$cadastro[2]['nome'] = "Movimenta��o";
		$cadastro[2]['valor'] = "../empresa/movimentacao.php";  
		
		$cadastro[3]['nome'] = "Opera��es de cadastro";
		$cadastro[3]['valor'] = "../empresa/pendentes.php"; 
	  }
  
	  /** Utilizacao **/    
	  if ($seg->permissaoOutros($bd,"WEBEMPRESARELATORIODEUTILIZACAODELIMITE")) {  
		$utilizacao[0]['nome']  = "Utiliza��o de limites";
		$utilizacao[0]['valor'] = "../empresa/relLimites.php"; 
	  }
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESARELATORIODELIMITEDEIDADE")) {
		$utilizacao[1]['nome'] = "Estouro de limite de idade";
		$utilizacao[1]['valor'] = "../empresa/relMaioridade.php";     
	  }
  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAEXTRATODEATENDIMENTO") or
		  $seg->permissaoOutros($bd,"WEBEMPRESAEXTRATODEATENDIMENTONOCOBRADO")) {
		$utilizacao[2]['nome'] = "Extrato de atendimentos n�o cobrados";
		$utilizacao[2]['valor'] = "../empresa/atendimentosNaoCobrados.php";
	  }
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAEXTRATODEATENDIMENTO") or
		  $seg->permissaoOutros($bd,"WEBEMPRESAEXTRATODEATENDIMENTOCOBRADO")) {
		$utilizacao[3]['nome'] = "Extrato de atendimentos cobrados";
		$utilizacao[3]['valor'] = "../empresa/atendimentosCobrados.php";
	  }  
		  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAEXTRATODEATENDIMENTO") or
		  $seg->permissaoOutros($bd,"WEBEMPRESAEXTRATODEATENDIMENTOCOBRADO")) {
		$utilizacao[4]['nome'] = "Extrato de mensalidades e atendimentos cobrados";
		$utilizacao[4]['valor'] = "../empresa/mensalAtendimentosCobrados.php";
	  }    
		   
	  /**** Nivel 1 ****/
	  
	  /** Boletos **/ 
    $b = 0;    
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMEBOLETO",true)) {  
      $b++;
		  $boletos[$b]['nome'] = "Rela��o boletos";
		  $boletos[$b]['valor'] = "../empresa/boletos.php"; 	
	  }
	  /** Faturas **/
	  else if ($seg->permissaoOutros($bd,"WEBEMPRESAVISUALIZABOLETO",false)) {
		  $faturas[0]['nome'] = "Rela��o de faturas";
		  $faturas[0]['valor'] = "../empresa/boletos.php";    
	  }
    
	  if ($_SESSION['apelido_operadora'] <> 'unimedSalto') {
      $b++;
	  	$boletos[$b]['nome']  = "Altera��o do vencimento";	
      $boletos[$b]['valor'] = "../empresa/alteraVencimento.php";
	  }
    
	  if ($seg->permissaoOutros($bd,"WEBEMPRESADEMONSTRATIVODEQUITAODEDBITOS",true)) {  
      $b++;
      $boletos[$b]['nome'] = "Demonstrativo de quita��o de d�bitos";
      $boletos[$b]['valor'] = "../empresa/quitacao.php";
    }
	  
	  /** Rede **/
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAREDECREDENCIADA")) {   
		$rede[0]['nome'] = "Consulta rede credenciada";
		$rede[0]['valor'] = "../comum/redeCredenciada.php";  
	  }
	  
	  /** movimentacao **/
	  if ($carteirinha) {
		$movimentacao[0]['nome'] = "Solicita��o de carteirinha";
		$movimentacao[0]['valor'] = "../empresa/carteiras.php"; 
	  }
  
	  if ($inclusao) {     
		$movimentacao[1]['nome'] = "Inclus�es";
		$movimentacao[1]['valor'] = "../empresa/inclusao.php"; 
	  }
	  
	  if ($alteracao) {     
		$movimentacao[2]['nome'] = "Altera��es";
		$movimentacao[2]['valor'] = "../empresa/cadastro.php?op=A"; 
	  }
	  
	  if ($cancelamento) {     
		$movimentacao[3]['nome'] = "Cancelamentos";
		$movimentacao[3]['valor'] = "../empresa/cadastro.php?op=C";   
	  } 
	  if ($seg->permissaoOutros($bd,"WEBEMPRESAAUDITAGUIAS",false)) {     
		$movimentacao[4]['nome'] = "Auditar Guias";
		$movimentacao[4]['valor'] = "../empresa/auditoriaGuias.php";   
	  }     
    
    if ($seg->permissaoOutros($bd,"WEBEMPRESAEMITIRGUIAPEDIDOEXAME",false)) {
      $movimentacao[5]['nome'] = "Pedido de Exame ambulatorial";
      $movimentacao[5]['valor'] = "../empresa/procedimento.php?pagina=".md5('pedidoExame');		
    }
    
	  if ($inclusao or $cancelamento or $alteracao) {     
		$movimentacao[6]['nome'] = "Inclus�o via Layout";
		$movimentacao[6]['valor'] = "../empresa/movimentacaoArq.php";   
	  }  
  
	  /** relatorios **/
	  if ($seg->permissaoOutros($bd,"WEBEMPRESARELACAODEATENDIMENTO",false) or
		  $seg->permissaoOutros($bd,"WEBEMPRESARELACAODEATENDIMENTOCOMVALOR",false)) {                        
		$relatorios[0]['nome'] = "Atendimentos";
		$relatorios[0]['valor'] = "../empresa/atendimentos.php";  
	  }
	  
	  if ($_SESSION['apelido_operadora'] == 'saudemed') {
		if ($carteirinha) {
		  $relatorios[1]['nome'] = "Carteirinhas solicitadas";
		  $relatorios[1]['valor'] = "../empresa/relCarteiras.php";    
		}
		
		if ($seg->permissaoOutros($bd,"WEBEMPRESAPOPULACAODEUSUARIO")) {
		  $relatorios[2]['nome'] = "Popula��o de usu�rios";
		  $relatorios[2]['valor'] = "../empresa/relTotalizacao.php"; 
		}
  
		if ($inclusao or $cancelamento or $alteracao) {  
		  $relatorios[3]['nome'] = "Movimenta��o";
		  $relatorios[3]['valor'] = "../empresa/movimentacao.php";  
		  
		  $relatorios[4]['nome'] = "Opera��es de cadastro";
		  $relatorios[4]['valor'] = "../empresa/pendentes.php"; 
		}
	  
		if (sizeOf($utilizacao) > 0) {    
		  $relatorios[5]['nome'] = "Utiliza��o";
		  $relatorios[5]['valor'] = $utilizacao; 
		}	
	   
	  } else {
		if (sizeOf($cadastro) > 0) {    
		  $relatorios[1]['nome'] = "Cadastro";
		  $relatorios[1]['valor'] = $cadastro;  
		}
	  
		if (sizeOf($utilizacao) > 0) {    
		  $relatorios[2]['nome'] = "Utiliza��o";
		  $relatorios[2]['valor'] = $utilizacao; 
		}
	   
		if ($seg->permissaoOutros($bd,"WEBEMPRESARESUMOGERENCIAL",true)) {
		  $relatorios[6]['nome'] = "Resumo gerencial";
		  $relatorios[6]['valor'] = "../empresa/resumoGerencial.php"; 
		}				
		
		if ($seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMEAUTORIZACAO")) {
		  $relatorios[7]['nome'] = "Autoriza��es";
		  $relatorios[7]['valor'] = "../empresa/autorizacoes.php";   
		}
		if ($seg->permissaoOutros($bd,"WEBEMPRESACONSULTAUSUARIO")) {
				$relatorios[8]['nome'] = "Consulta de benefici�rios";
				$relatorios[8]['valor'] = "consultaBeneficiario.php";
		}
		if (sizeOf($subpcmso) > 0) {    
		  $relatorios[9]['nome'] = "Sa�de Ocupacional";
		  $relatorios[9]['valor'] = $subpcmso; 
		}
		if ($_SESSION['apelido_operadora'] == 'agemed') {
		  $relatorios[9]['nome'] = "Historico de Reajuste";
		  $relatorios[9]['valor'] = "../empresa/historico_reajuste.php"; 

		  $relatorios[9]['nome'] = "Tabela de pre�os";
		  $relatorios[9]['valor'] = "../empresa/tabelaPrecos.php";     
		}  
	}
	  
	  /** utilitarios **/
    $u = 0;
	  
    if ($seg->permissaoOutros($bd,"WEBEMPRESAUPLOADDEARQUIVOS",true)) {   
      $u++;
		  $utilitarios[$u]['nome'] = "Envio e recebimento de arquivo";
		  $utilitarios[$u]['valor'] = "../empresa/upload.php"; 
	  }    
    
    $u++;
	  $utilitarios[$u]['nome'] = "Carta de orienta��o";
    
    if ($_SESSION['apelido_operadora'] == 'vitallis')
	    $utilitarios[$u]['valor'] = "../empresa/documentos/carta_orientacao_vitallis.pdf";  
	  else
	    $utilitarios[$u]['valor'] = "../empresa/documentos/carta_orientacao.pdf"; 
	   
	  $utilitarios[$u]['novo'] = "S"; 
    
    if ($_SESSION['apelido_operadora'] == 'vitallis') {
      $u++;
	    $utilitarios[$u]['nome'] = "Declara��o de sa�de";
	    $utilitarios[$u]['valor'] = "../empresa/documentos/declaracaoSaude.pdf"; 
	    $utilitarios[$u]['novo'] = "S";  

      $formulario[0]['nome'] = "FEB PDF";
      $formulario[0]['valor'] = "../empresa/documentos/feb_new.pdf";
	    $formulario[0]['novo'] ="S"; 
		 
      $formulario[1]['nome'] = "FEB XLS";
      $formulario[1]['valor'] = "../empresa/documentos/feb_new.xls";
	   
	    if (sizeOf($formulario) > 0){    
        $u++;
        $utilitarios[$u]['nome'] = "FEB - Formul�rio de exclus�o";
        $utilitarios[$u]['valor'] = $formulario;  
	    } 		 
	  }
  
	  if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
      $u++;
      $utilitarios[$u]['nome'] = "Autoriza��o para D�bito em C/C - Banco Bradesco";
      $utilitarios[$u]['valor'] = "../empresa/autorizacaoDebitoImp.php"; 
      $utilitarios[$u]['novo'] = "S"; 
	  }  
	  
    if (file_exists("../empresa/documentos/manual_empresa.pdf")) {
      $u++;
      $utilitarios[$u]['nome'] = "Manual";
      $utilitarios[$u]['valor'] = "../empresa/documentos/manual_empresa.pdf"; 
      $utilitarios[$u]['novo'] = "S";     
    }
    
	  if ($_SESSION['apelido_operadora'] == 'agemed') {
      $u++;
      $utilitarios[$u]['nome'] = "Manual";
      $utilitarios[$u]['valor'] = "../empresa/documentos/manual_agemed.pdf"; 
      $utilitarios[$u]['novo'] = "S";     
      
      $u++;
      $utilitarios[$u]['nome'] = "Declara��o de sa�de";
      $utilitarios[$u]['valor'] = "../empresa/documentos/declaracao_agemed.pdf";       
      $utilitarios[$u]['novo'] = "S";     
	  } 
    else if ($_SESSION['apelido_operadora'] == 'promedmg') {
      $u++;
      $utilitarios[$u]['nome'] = "Declara��o de sa�de";
      $utilitarios[$u]['valor'] = "../empresa/documentos/declaracao_promed.pdf";       
      $utilitarios[$u]['novo'] = "S";     
      
      if ($_SESSION['id_contrato'] == 33297095) {
        $u++;
        $utilitarios[$u]['nome'] = "Contrato Fettrominas BH";
        $utilitarios[$u]['valor'] = "../empresa/documentos/Fettrominas_BH.pdf";
        $utilitarios[$u]['novo'] = "S";
      }    
	  } 
    else if ($_SESSION['apelido_operadora'] == 'portoAlegreClinicas') {
      $u++;
      $utilitarios[$u]['nome'] = "Declara��o de sa�de";
      $utilitarios[$u]['valor'] = "../empresa/documentos/declaracao_poa.pdf";       
      $utilitarios[$u]['novo'] = "S";       
	  }
    
    if ($_SESSION['apelido_operadora'] == 'promedmg') {  
      $u++;
      $utilitarios[$u]['nome'] = "Termo de Ades�o ou Recusa - Planos Inativos";
      $utilitarios[$u]['valor'] = "../empresa/termo_adesao.pdf";
      $utilitarios[$u]['novo'] = "S";
    }        
			
	  /** Configura��o **/     
    if (($seg->permissaoOutros($bd,"WEBALTERARDADOSDOOPERADOR",true))) {
	  $configuracoes[0]['nome'] = "Alterar dados do operador";
	  $configuracoes[0]['valor'] = "../comum/alterarOperador.php"; 
    }
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESATROCARSENHA",true)) {     
		$configuracoes[1]['nome'] = "Alterar senha";
		$configuracoes[1]['valor'] = "../comum/trocaSenha.php"; 
	  }
	  
	  if ($seg->permissaoOutros($bd,"WEBEMPRESACONSULTALOGS",true)) {  
		$configuracoes[2]['nome'] = "Logs";
		$configuracoes[2]['valor'] = "../comum/listaLogs.php";   
	  }     

	  $configuracoes[3]['nome'] = "Alterar dados do usu�rio";
	  $configuracoes[3]['valor'] = "atualizacaoCadastral.php"; 

	  $configuracoes[4]['nome'] = "Atualizar informa��es";
      $configuracoes[4]['valor'] = "../empresa/atualizacaoResponsavel.php";   
	  
	  /**** Nivel 0 ****/
	  
	  $menu[0]['nome'] = "Principal";  
	  $menu[0]['valor'] = "../empresa/principal.php";
	  
	  if (sizeOf($boletos) > 0) {
		$menu[1]['nome'] = "Boletos";  
		$menu[1]['valor'] = $boletos;  
	  } 
	  else if (sizeOf($faturas) > 0) {
		$menu[1]['nome'] = "Faturas";  
		$menu[1]['valor'] = $faturas;
	  }
	  
	  if (sizeOf($rede) > 0) {   
		$menu[2]['nome'] = "Rede credenciada";  
		$menu[2]['valor'] = $rede;
	  }
	  
	  if (sizeOf($movimentacao) > 0) {   
		$menu[3]['nome'] = "Movimenta��o";  
		$menu[3]['valor'] = $movimentacao;
	  }
	  
	  if (sizeOf($relatorios) > 0) {   
		$menu[4]['nome'] = "Relat�rios";  
		$menu[4]['valor'] = $relatorios;
	  }
	  
    if (isset($comunicados)) {    
      if (sizeOf($comunicados) > 0) {   
      $menu[11]['nome'] = "Comunicados";  
      $menu[11]['valor'] = $comunicados;
      }
    } 
    
	  if ($utiloutro) {    
		$menu[5]['nome'] = "Utilit�rio/Outros";  
		$menu[5]['valor'] = $utilitarios;
	  }
	  
	  if (sizeOf($configuracoes) > 0) {  
		$menu[6]['nome'] = "Configura��es";  
		$menu[6]['valor'] = $configuracoes;
	  } 
	   
	  $menu[10]['nome'] = "Consulta de usu�rios";  
	  $menu[10]['valor'] = "../empresa/consultaUsuarios.php";

	  $menu[7]['nome'] = "Sair";  
	  $menu[7]['valor'] = "../empresa/index.php";  
	}
  }
?>