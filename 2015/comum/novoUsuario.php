<?php
  require_once("../comum/autoload.php"); 
  include_once '../comum/securimage/securimage.php';
  $seg->secureSessionStart(); 
    
  $_SESSION['titulo'] = "CADASTRO DE SENHA";
    
  require_once("../comum/layout.php");   
  $tpl->addFile("CONTEUDO","novoUsuario.html");
  
  if (isset($_POST['voltar'])) {   
    if ($_SESSION['diretorio'] <> '')
      $util->redireciona($_SESSION['diretorio'].'/index.php');
    else
      $util->redireciona('../index.html');
  }
  else if (isset($_POST['cadastrar'])) {
  
    $operador      = $seg->antiInjection($_POST['operador']);
    $tpl->OPERADOR = $operador;
     
    if ($operador <> '') {     
      $securimage = new Securimage();
      
      $tentativas = $seg->bloqueiaLogin($operador);
        
      if ( $tentativas < 6 ) {
        
        if ($securimage->check($_POST['captcha_code']) == false) {
        
          $tpl->MSG_CLASSE = "alert-error";
          $seg->registraLogin($operador);         
          $texto_msg  = "Desculpe, o c�digo de seguran�a informado est� incorreto. ";
                        
          if ($tentativas > 0)
            $texto_msg .= " Tentativa ".$tentativas." de 5.";

          $tpl->MSG = $texto_msg;
          $tpl->block("MOSTRA_MSG");            
        }      
        else {        
          $sql = new Query(); 
          $txt = "SELECT CSENHUSUA,CNCOMUSUA,CNOMEUSUA,CMAILUSUA,NNUMEUSUA
                    FROM SEGUSUA
                   WHERE CNOMEUSUA = UPPER(:operador)
                     AND CSTATUSUA = 'A' ";
                  
          $sql->addParam(":operador",$operador);
          $sql->executeQuery($txt);
            
          if ($sql->result("NNUMEUSUA") > 0) {
            $usuario = $sql->result("NNUMEUSUA");
            $email   = $sql->result("CMAILUSUA");
              
            if ($email == '') {                  
              $tpl->MSG_CLASSE = "alert-error";
              $tpl->MSG        = "Operador sem email cadastrado. Entre em contato com a operadora para a atualiza��o do cadastro. Ap�s a atualiza��o tente novamente.";
              $tpl->block("MOSTRA_MSG");           
            }
            else if ($sql->result("CSENHUSUA") <> '') {
                                                
              $novaSenha  = $seg->geraSenha();
              $sql_update = new Query();
              $txt_update = "UPDATE SEGUSUA SET CSENHUSUA = :senha,CCRIPUSUA = 'S', CSVENUSUA = 'S', DVENSUSUA = SYSDATE -1
                              WHERE NNUMEUSUA = :id";                                                           
              $sql_update->addParam(":senha",$novaSenha);
              $sql_update->addParam(":id",$usuario);
              $sql_update->executeSQL($txt_update);

              $msg = '<!DOCTYPE HTML>
                      <html lang="pt">
                      <head>
                        <meta charset="ISO-8859-1">                
                      </head>
                      <body>
                        <p>Ol&aacute; <b>'.$formata->initCap($sql->result("CNCOMUSUA")).'</b>, 
                        <br/><br/>
                        Para acessar a �rea restrita de prestadores, utilize a senha tempor�ria abaixo:
                        
                        <br/><br/>
                               Login: '.$sql->result("CNOMEUSUA").'<br />
                               Senha: '.$novaSenha.'<br /><br />
                               
                               Cordialmente<br />'.
                               $formata->initCap($_SESSION['nome_operadora']).'
                        </p>
                      </body>
                      </html>';                           
                                  
              if ($_SESSION['apelido_operadora'] == 'coamo') {
                $tpl->MSG_CLASSE = "alert-success";
                $tpl->MSG        = "A sua senha �: ".$sql->result("CSENHUSUA");
                $tpl->block("MOSTRA_MSG");
              } else {
                      
                $erro = $func->enviaEmail($email,'Senha tempor�ria',$msg);
                
                if (($erro == '') and ($email <> '')) {
                  $tpl->MSG_CLASSE = "alert-success";
                  $tpl->MSG        = "Uma nova senha foi enviada para o email (".$email.")."; 
                  $tpl->block("MOSTRA_MSG");            
                } else {    
                  $tpl->MSG_CLASSE = "alert-error";
                  $tpl->MSG        = "Erro ao enviar email. Entre em contato com a Operadora<br>".$erro;
                  $tpl->block("MOSTRA_MSG"); 
                }           
              }        
            }                
          } else {        
            $tpl->MSG_CLASSE = "alert-error";
            $seg->registraLogin($operador);         
            $texto_msg  = "N�o foi possivel localizar o operador informado. Por favor entre em contato com a operadora.";
                          
            if ($tentativas > 0)
              $texto_msg .= " Tentativa ".$tentativas." de 5.";

            $tpl->MSG = $texto_msg;
            $tpl->block("MOSTRA_MSG");  
          }        
        }
      }
      else {       
        $tpl->MSG_CLASSE = "alert-error";
        $tpl->MSG = "Voc� excedeu o n�mero de tentativas (5). Por favor aguarde alguns instantes e tente novamente.";
        $tpl->block("MOSTRA_MSG");  
      }     
    }
  }
    
  $tpl->show();     

?>