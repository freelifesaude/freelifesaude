<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "ATENDIMENTOS";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/atendimentos.htm");

  $tpl->ID_SESSAO = $_GET['idSessao'];
  $tpl->block("FILTRO_NOME");
  
  if ($tpl->AREA == "Empresa"){
    $tpl->MSG2 = "Este relat�rio trata-se apenas de um hist�rico, sendo que o mesmo poder� n�o ser correspondente ao desconto em folha.";  
  }
  
  $tpl->MASCARA = $_SESSION['mascara'];  
  $datainicial = $_POST['datainicial'];
  $datafinal = $_POST['datafinal'];
  $valores = $_POST['valores'];
  $codigo = $_POST['codigo'];  
  $faltas = $_POST['listar_faltas'];
        
  if (isset($_POST['imprimir'])) {
  
    $id_locacao = $_SESSION['id_locacao'];
    $id_contrato = $_SESSION['id_contrato'];

    if ($_SESSION['apelido_operadora'] == 'vitallis') {
      $txt_congenere = "SELECT NNUMECONGE FROM HSSTITU
                         WHERE NNUMETITU = :contrato
                           AND HSSTITU.NNUMECONGE = 2040382163 ";
      $sql_conge = new Query($bd);                         
      $sql_conge->addParam(":contrato",$id_contrato);
      $sql_conge->executeQuery($txt_congenere);

      if ($sql_conge->count() > 0) {
        $f_at1 = "   AND HSSCONT.CAMBUCONT <> '6' ";
        $f_at2 = "   AND HSSGUIA.CTIPOGUIA <> '6' ";
      }
    } else {
      $f_at1 = "";
      $f_at2 = "";
    }
    
    if ($codigo <> "")
      $f_codigo = "   AND TITU.CCODIUSUA LIKE :codigo ";
    else
      $f_codigo = "";
      
    if ($id_locacao > 0) 
      $f_locacao = "   AND TITU.NNUMESETOR = :id_locacao ";     
    else
      $f_locacao = "";
    
    $txt = "";
    if (($valores == '1') or ($valores == '3')) {
      $txt .= "SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSCONT.NNUMECONT,DATENCONT,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                     USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'C' TIPO,NFRANCONT, 
                     TO_CHAR(DATENCONT,'DD/MM/YY') DATA, TOTAL_CONTA_PLANO(HSSCONT.NNUMECONT) CONTA,HSSCONT.NNUMECONT,USUA.NNUMEPLAN, 
                     IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,'Pago' STATUS
                FROM HSSUSUA TITU,HSSUSUA USUA,HSSCONT,FINPRES,FINPRES SOLI 
               WHERE TITU.NNUMETITU = :id_contrato 
                 AND HSSCONT.NNUMECONT IN (SELECT NNUMECONT FROM HSSPACON
                                              WHERE NNUMECONT = HSSPACON.NNUMECONT
                                              UNION ALL
                                             SELECT CONT.NNUMECONT FROM HSSCONT CONT
                                              WHERE CONT.NNUMECONT = HSSCONT.NNUMECONT
                                                AND ((CONT.NFRANCONT = 0) OR (CONT.CCOBRCONT = 'X'))
                                              UNION ALL
                                             SELECT NNUMECONT FROM HSSGCON,HSSGPAGA
                                              WHERE NNUMECONT = HSSCONT.NNUMECONT
                                                AND HSSGCON.NNUMEGUIA = HSSGPAGA.NNUMEGUIA) ".
             $f_codigo.
             $f_locacao.
             "   AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
                 AND USUA.CTIPOUSUA <> 'F' 
                 AND USUA.NNUMEUSUA = HSSCONT.NNUMEUSUA
                 AND DATENCONT >= TO_DATE(:datainicial,'DD/MM/YYYY')
                 AND DATENCONT <= TO_DATE(:datafinal,'DD/MM/YYYY') ".
             $f_at1.
             "   AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES(+)
                 AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
               UNION ALL
              SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,DEMISGUIA,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                     USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'G',0,
                     TO_CHAR(DEMISGUIA,'DD/MM/YY') DATA, TOTAL_GUIA(HSSGUIA.NNUMEGUIA) CONTA,HSSGUIA.NNUMEGUIA,USUA.NNUMEPLAN,
                     IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,'Pago' STATUS
                FROM HSSUSUA TITU,HSSUSUA USUA,HSSGUIA,FINPRES,FINPRES SOLI
               WHERE TITU.NNUMETITU = :id_contrato 
                 AND HSSGUIA.NNUMEGUIA IN (SELECT NNUMEGUIA FROM HSSGPAGA
                                            WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                            UNION ALL
                                           SELECT NNUMEGUIA FROM HSSPGUI
                                            WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                            GROUP BY NNUMEGUIA
                                           HAVING SUM(NFRANPGUI) = 0
                                            UNION ALL
                                           SELECT NNUMEGUIA FROM HSSGCON,HSSPACON
                                            WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                              AND HSSGCON.NNUMECONT = HSSPACON.NNUMECONT) ".
             $f_codigo.
             $f_locacao.                                              
             "   AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
                 AND USUA.NNUMEUSUA = HSSGUIA.NUTILUSUA
                 AND HSSGUIA.NNUMEGUIA NOT IN (SELECT NNUMEGUIA FROM HSSGCON WHERE HSSGCON.NNUMEGUIA = HSSGUIA.NNUMEGUIA)
                 AND DEMISGUIA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                 AND DEMISGUIA < TO_DATE(:datafinal,'DD/MM/YYYY') + 1".
             $f_at2.
             "   AND CSTATGUIA IS NULL
                 AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES(+)
                 AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+) ";
    }
    
    if ($valores == '1')
      $txt .= " UNION ALL ";    
    
    if (($valores == '1') or ($valores == '2')) {    
      $txt .= "SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSCONT.NNUMECONT,DATENCONT,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                     USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'C' TIPO,NFRANCONT, 
                     TO_CHAR(DATENCONT,'DD/MM/YY') DATA, TOTAL_CONTA_PLANO(HSSCONT.NNUMECONT) CONTA,HSSCONT.NNUMECONT,USUA.NNUMEPLAN, 
                     IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,'N�o pago' STATUS 
                FROM HSSUSUA TITU,HSSUSUA USUA,HSSCONT,FINPRES,FINPRES SOLI 
               WHERE TITU.NNUMETITU = :id_contrato 
                 AND HSSCONT.NNUMECONT NOT IN (SELECT NNUMECONT FROM HSSPACON
                                            WHERE NNUMECONT = HSSPACON.NNUMECONT
                                            UNION ALL
                                           SELECT CONT.NNUMECONT FROM HSSCONT CONT
                                            WHERE CONT.NNUMECONT = HSSCONT.NNUMECONT
                                              AND ((CONT.NFRANCONT = 0) OR (CONT.CCOBRCONT = 'X'))
                                            UNION ALL
                                           SELECT NNUMECONT FROM HSSGCON,HSSGPAGA
                                            WHERE NNUMECONT = HSSCONT.NNUMECONT
                                              AND HSSGCON.NNUMEGUIA = HSSGPAGA.NNUMEGUIA) ".
             $f_codigo.
             $f_locacao.
             "   AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
                 AND USUA.CTIPOUSUA <> 'F' 
                 AND USUA.NNUMEUSUA = HSSCONT.NNUMEUSUA
                 AND DATENCONT >= TO_DATE(:datainicial,'DD/MM/YYYY')
                 AND DATENCONT <= TO_DATE(:datafinal,'DD/MM/YYYY') ".
             $f_at1.
             "   AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES(+)
                 AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
               UNION ALL
              SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,DEMISGUIA,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                     USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'G',0,
                     TO_CHAR(DEMISGUIA,'DD/MM/YY') DATA, TOTAL_GUIA(HSSGUIA.NNUMEGUIA) CONTA,HSSGUIA.NNUMEGUIA,USUA.NNUMEPLAN,
                     IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,'N�o pago' STATUS
                FROM HSSUSUA TITU,HSSUSUA USUA,HSSGUIA,FINPRES,FINPRES SOLI
               WHERE TITU.NNUMETITU = :id_contrato 
                 AND HSSGUIA.NNUMEGUIA NOT IN (SELECT NNUMEGUIA FROM HSSGPAGA
                                                WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                                UNION ALL
                                               SELECT NNUMEGUIA FROM HSSPGUI
                                                WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                                GROUP BY NNUMEGUIA
                                               HAVING SUM(NFRANPGUI) = 0
                                                UNION ALL
                                               SELECT NNUMEGUIA FROM HSSGCON,HSSPACON
                                                WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                                  AND HSSGCON.NNUMECONT = HSSPACON.NNUMECONT) ".
             $f_codigo.
             $f_locacao.
             "   AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
                 AND USUA.NNUMEUSUA = HSSGUIA.NUTILUSUA
                 AND HSSGUIA.NNUMEGUIA NOT IN (SELECT NNUMEGUIA FROM HSSGCON WHERE HSSGCON.NNUMEGUIA = HSSGUIA.NNUMEGUIA)
                 AND DEMISGUIA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                 AND DEMISGUIA < TO_DATE(:datafinal,'DD/MM/YYYY') + 1".
             $f_at2.
             "   AND CSTATGUIA IS NULL
                 AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES(+)
                 AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+) ";
    }
    
    $txt .= " ORDER BY 8,2,4 DESC, 3";
             
    $sql = new Query();                         
    $sql->addParam(":datainicial",$datainicial);   
    $sql->addParam(":datafinal",$datafinal);   
    $sql->addParam(":id_contrato",$_SESSION['id_contrato']); 

    if ($codigo <> "") {
      $codigo2 = $codigo."%";
      $sql->addParam(":codigo",$codigo2);
    }
     
    if ($id_locacao > 0)   
      $sql->addParam(":id_locacao",$id_locacao);  
      
    $sql->executeQuery($txt);
          
    if ($sql->count() > 0) {
      if ($_SESSION['apelido_operadora'] == 'saudemed')
        require_once("atendimentosImp2.php");
      elseif ($_SESSION['apelido_operadora'] == 'consaude')
        require_once("atendimentosImp3.php");        
      else
        require_once("atendimentosImp.php");
    } else {
      $tpl->CLASSE = "msg_alerta";
      $tpl->IMAGEM = "alerta";
      $tpl->MSG = "N�o existe nenhum atendimento para o per�odo informado.";
      $tpl->block("ERRO");
    }
    
    $tpl->CODIGO = $codigo;    
    $tpl->DATA_INICIAL = $datainicial;
    $tpl->DATA_FINAL = $datafinal;
    $tpl->{"VALORES_".$valores} = 'selected';
		
    if ($faltas == 'S')
      $tpl->FALTAS = 'checked';
    else
      $tpl->FALTAS = '';   
  }
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     

?>