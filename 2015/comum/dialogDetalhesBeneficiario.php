<?php  

  $sql_usu = new Query($bd);
  $txt_usu = "SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,HSSUSUA.CCODIUSUA,HSSUSUA.CTIPOUSUA,CDESCTLOGR,HSSUSUA.CENDEUSUA,HSSUSUA.CBAIRUSUA,
                     HSSUSUA.CCEP_USUA,HSSUSUA.CCIDAUSUA,HSSUSUA.CESTAUSUA,TITULAR.CNOMEUSUA TITULAR, 
                     CDESCPLAN,CRAZAEMPR,HSSUSUA.C_CPFUSUA,HSSUSUA.CORRGUSUA,HSSUSUA.C__RGUSUA, 
                     TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY') DSITUUSUA,HSSUSUA.NNUMEUSUA, 
                     TO_CHAR(HSSUSUA.DALIBUSUA,'DD/MM/YYYY') DALIBUSUA, 
                     TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,HSSUSUA.CSITUUSUA, 
                     TO_CHAR(HSSUSUA.DDEMIUSUA,'DD/MM/YYYY') DDEMIUSUA, 
                     TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,HSSUSUA.CCARTUSUA,HSSTITU.NCPOSTITU, 
                     DECODE(HSSSTAT.NNUMESTAT,NULL,STAT_TITU.NNUMESTAT,HSSSTAT.NNUMESTAT) NNUMESTAT,
                     DECODE(HSSSTAT.NNUMESTAT,NULL,STAT_TITU.CWEB_STAT,HSSSTAT.CWEB_STAT) CWEB_STAT,
                     DECODE(HSSSTAT.NNUMESTAT,NULL,STAT_TITU.CGUIASTAT,HSSSTAT.CGUIASTAT) CGUIASTAT,
                     VALIDA_REDE_ATENDIMENTO30(HSSUSUA.NNUMEUSUA,:contratado) REDE,
                     VALOR_USUARIO_VENCIMENTO(HSSUSUA.NNUMEUSUA,SYSDATE) VALOR,CCOBEPLAN,CABRAPLAN,
                     RETORNA_NOME_ACOMODACAO(NVL(HSSUSUA.NNUMEACOM,NVL(HSSTITU.NNUMEACOM,HSSPLAN.NNUMEACOM))) ACOMODACAO,
                     HSSUSUA.CNMAEUSUA,HSSUSUA.CNUMEUSUA NUMERO,HSSUSUA.NFATUTLOGR
                FROM HSSUSUA,HSSUSUA TITULAR,HSSPLAN,HSSTITU,HSSEMPR,HSSSTAT,HSSSTAT STAT_TITU,HSSTLOGR 
               WHERE HSSUSUA.NNUMEUSUA = :usuario 
                 AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA 
                 AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU 
                 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+) 
                 AND HSSUSUA.NNUMESTAT = HSSSTAT.NNUMESTAT(+)
                 AND HSSTITU.NNUMESTAT = STAT_TITU.NNUMESTAT(+)               
                 AND HSSUSUA.NFATUTLOGR = HSSTLOGR.NNUMETLOGR(+)
               ORDER BY 1 DESC,2";
  $sql_usu->addParam(":usuario",$idBeneficiario);
  
  if (isset($_SESSION['id_contratado']))
    $sql_usu->addParam(":contratado",$_SESSION['id_contratado']);    
  else
    $sql_usu->addParam(":contratado",0);    
    
  $sql_usu->executeQuery($txt_usu);
  
  if ($sql_usu->result("CSITUUSUA") == 'A') {
    if (strtotime($data->dataInvertida($sql_usu->result("DINCLUSUA"))) > strtotime($data->dataAtual('YYYYMMDD'))) {
      $tpl->STYLE = "color:red;";
      $situacao = 'Atendimento liberado ap�s ' . $sql_usu->result("DINCLUSUA");
    }  
    else if ($sql_usu->result("NNUMESTAT") > 0) {
    
      $tpl->STYLE = "background-color:yellow;";
      
      $situacao = $func->retornaNomeStatus($bd,$sql_usu->result("NNUMESTAT"));          
      
      if (($sql_usu->result("CWEB_STAT") == 'N') or ($sql_usu->result("CGUIASTAT") == 'N'))
        $situacao .= " - Entre em contato com a operadora";
      
    } 
    else {
      $tpl->STYLE = "color:green;";
      $situacao = 'ATIVO ' . $func->trocaDePlanos($bd,$sql_usu->result("NNUMEUSUA"));
    }
  }
  else if ($sql_usu->result("CSITUUSUA") == 'F') {
    $tpl->STYLE = "color:red;";
    $situacao = 'Falecido em ' . $sql_usu->result("DSITUUSUA");
  }
  else if ($sql_usu->result("CSITUUSUA") == 'C') {
    if ($sql_usu->result("DALIBUSUA") <> null && strtotime($data->dataInvertida($sql_usu->result("DALIBUSUA"))) >= strtotime($data->dataAtual('YYYYMMDD'))) {
      $tpl->STYLE = "background-color:yellow;";
      $situacao = 'Atendimento at� ' . $sql_usu->result("DALIBUSUA") . ' - Cancelado em ' . $sql_usu->result("DSITUUSUA");
    }
    else {
      $tpl->STYLE = "color:red;";
      $situacao = 'Cancelado em ' . $sql_usu->result("DSITUUSUA");
    }
  }
  else if ($sql_usu->result("CSITUUSUA") == 'S') {
    $tpl->STYLE = "color:red;";
    $situacao = 'Suspenso em ' . $sql_usu->result("DSITUUSUA");
  }
  else if ($sql_usu->result("CSITUUSUA") == 'M') {
    if ($sql_usu->result("DALIBUSUA") <> null && strtotime($data->dataInvertida($sql_usu->result("DALIBUSUA"))) >= strtotime($data->dataAtual('YYYYMMDD'))) {
      $tpl->STYLE = "background-color:yellow;";
      $situacao = 'Atendimento at� ' . $sql_usu->result("DALIBUSUA") . ' - Migrado em ' . $sql_usu->result("DSITUUSUA");
    }
    else if ($sql_usu->result("DDEMIUSUA") <> null) {
      $tpl->STYLE = "color:red;";
      $situacao = 'Demitido em ' . $sql_usu->result("DDEMIUSUA") . '(' . codigo_titulo($bd,$sql_usu->result("NCPOSTITU")) . ')';
    }
    else {
      $tpl->STYLE = "color:red;";
      $situacao = 'Migrou em ' . $sql_usu->result("DSITUUSUA") . ' para ' . codigo_titulo($bd,$sql_usu->result("NCPOSTITU"));
    }
  }    

  $tpl->CONSULTA_CODIGO     = $sql_usu->result("CCODIUSUA");
  $tpl->CONSULTA_NOME       = $sql_usu->result("CNOMEUSUA");
  $tpl->CONSULTA_INCLUSAO   = $sql_usu->result("DINCLUSUA");
  $tpl->CONSULTA_NASCIMENTO = $sql_usu->result("DNASCUSUA");
  $tpl->CONSULTA_CATEGORIA  = $func->categoriaUsuario($sql_usu->result("CTIPOUSUA"));
  $tpl->CONSULTA_PARENTESCO = $func->grauDeParentesco($sql_usu->result("CGRAUUSUA"));
  $tpl->CONSULTA_MAE        = $sql_usu->result("CNMAEUSUA");
  $tpl->CONSULTA_SITUACAO   = $situacao;
  $tpl->CONSULTA_VALOR      = $formata->formataNumero($sql_usu->result("VALOR"));
  
  if ($seg->permissaoOutros($bd,"WEBPRESTADORMOSTRACPFENDERECO",true)){
    $tpl->CONSULTA_CPF    = $formata->formataCPF($sql_usu->result("C_CPFUSUA"));
    $tpl->CONSULTA_TLOGR  = $sql_usu->result("NFATUTLOGR");
    $tpl->CONSULTA_LOGR   = $sql_usu->result("CENDEUSUA");
    $tpl->CONSULTA_NUMERO = $sql_usu->result("NUMERO");
    $tpl->CONSULTA_BAIRRO = $sql_usu->result("CBAIRUSUA");
    $tpl->CONSULTA_CEP    = $formata->formataCEP($sql_usu->result("CCEP_USUA"));
    $tpl->CONSULTA_CIDADE = $sql_usu->result("CCIDAUSUA");
    $tpl->CONSULTA_ESTADO = $sql_usu->result("CESTAUSUA");
    
    $txt1 = "SELECT NNUMETLOGR,INITCAP(CDESCTLOGR) CDESCTLOGR
              FROM HSSTLOGR 
             ORDER BY 2";  
      
    $sql1 = new Query();
    $sql1->executeQuery($txt1);  
    
    while (!$sql1->eof()) {
      $tpl->TIPO_LOGRADOURO_ID        = $sql1->result("NNUMETLOGR");
      $tpl->TIPO_LOGRADOURO_DESCRICAO = $sql1->result("CDESCTLOGR");
      $tpl->block("ITEM_TIPO_LOGRADOURO");
      $sql1->next();
    }  
  
    $tpl->block("MOSTRA_CPF");                  
    $tpl->block("MOSTRA_END");
  } 
  else {
    $tpl->CONSULTA_CPF = $formata->formataCPF($sql_usu->result("C_CPFUSUA"));
    $tpl->CONSULTA_RG  = $sql_usu->result("C__RGUSUA")." / ".$sql_usu->result("CORRGUSUA");
    $tpl->block("MOSTRA_CPF");
    $tpl->block("MOSTRA_RG");
  }
  
  $tpl->CONSULTA_CARTEIRA    = $sql_usu->result("CCARTUSUA");
  $tpl->CONSULTA_PLANO       = $sql_usu->result("CDESCPLAN");
  $tpl->CONSULTA_ACOMODACAO  = $sql_usu->result("ACOMODACAO");
  $tpl->CONSULTA_COBERTURA   = $func->coberturaPlano($sql_usu->result("CCOBEPLAN"));
  $tpl->CONSULTA_ABRANGENCIA = $func->abrangenciaPlano($sql_usu->result("CABRAPLAN"));
  
  if (($sql_usu->result("CTIPOUSUA") <> 'T') and ($sql_usu->result("CTIPOUSUA") <> 'F')) {
    $tpl->CONSULTA_TITULAR  = $sql_usu->result("TITULAR");
    $tpl->block("MOSTRA_TITULAR");
  }
  
  if ($sql_usu->result("CRAZAEMPR") <> '') {
    $tpl->CONSULTA_EMPRESA = $sql_usu->result("CRAZAEMPR");
    $tpl->block("MOSTRA_EMPRESA");        
  }
  
  if (isset($_SESSION['id_contratado'])) {
    if ($sql_usu->result("REDE") == 0)
      $tpl->CONSULTA_REDE = "N�o";
    else
      $tpl->CONSULTA_REDE = "Sim";
    
    $tpl->block("MOSTRA_REDE");
  }
  
  $sql2 = new Query($bd);
  $txt2 = "SELECT HSSREDEA.NNUMEREDEA, CNOMEREDEA
             FROM HSSUSUA,HSSREDPL,HSSREDEA
            WHERE HSSUSUA.NNUMEUSUA   = :usuario
              AND HSSUSUA.NNUMEPLAN   = HSSREDPL.NNUMEPLAN
              AND HSSREDPL.NNUMEREDEA = HSSREDEA.NNUMEREDEA
              AND HSSUSUA.NNUMETITU NOT IN (SELECT HSSREDCO.NNUMETITU FROM HSSREDCO
                                             WHERE HSSREDCO.NNUMETITU = HSSUSUA.NNUMETITU
                                               AND HSSREDCO.NNUMEUSUA IS NULL
                                               AND HSSREDCO.NNUMESETOR IS NULL)
              AND HSSUSUA.NNUMEUSUA NOT IN (SELECT HSSREDCO.NNUMEUSUA FROM HSSREDCO
                                             WHERE HSSREDCO.NNUMEUSUA = HSSUSUA.NNUMEUSUA)
              AND HSSUSUA.NNUMESETOR NOT IN (SELECT HSSREDCO.NNUMESETOR FROM HSSREDCO
                                              WHERE HSSREDCO.NNUMESETOR = HSSUSUA.NNUMESETOR)
            UNION ALL
           SELECT HSSREDEA.NNUMEREDEA, CNOMEREDEA
             FROM HSSUSUA,HSSREDCO,HSSREDEA
            WHERE HSSUSUA.NNUMEUSUA  = :usuario
              AND HSSUSUA.NNUMETITU  = HSSREDCO.NNUMETITU
              AND HSSREDCO.NNUMEUSUA IS NULL
              AND HSSREDCO.NNUMESETOR IS NULL
              AND HSSREDCO.NNUMEREDEA = HSSREDEA.NNUMEREDEA
            UNION ALL
           SELECT HSSREDEA.NNUMEREDEA, CNOMEREDEA
             FROM HSSUSUA,HSSREDCO,HSSREDEA
            WHERE HSSUSUA.NNUMEUSUA  = :usuario
              AND HSSUSUA.NNUMETITU  = HSSREDCO.NNUMETITU
              AND HSSREDCO.NNUMEUSUA IS NULL
              AND HSSREDCO.NNUMESETOR > 0
              AND HSSREDCO.NNUMESETOR = HSSUSUA.NNUMESETOR
              AND HSSREDCO.NNUMEREDEA = HSSREDEA.NNUMEREDEA
            UNION ALL
           SELECT HSSREDEA.NNUMEREDEA, CNOMEREDEA
             FROM HSSREDCO,HSSREDEA
            WHERE HSSREDCO.NNUMEUSUA  = :usuario
              AND HSSREDCO.NNUMEREDEA = HSSREDEA.NNUMEREDEA
            ORDER BY 2";
  $sql2->addParam(":usuario",$idBeneficiario);
  $sql2->executeQuery($txt2);   
  
  $redes = '';
  while (!$sql2->eof()) {
    $redes .= $sql2->result("CNOMEREDEA")."&#13;&#10;";
    $sql2->next();
  } 
  
  $tpl->CONSULTA_REDE_ATENDIMENTO = $redes;
  /*
  $sql2 = new Query($bd);
  $sql2->clear();
  $txt = "SELECT CFONETLUSU,COBSETLUSU FROM HSSTLUSU
           WHERE NNUMEUSUA = :usuario
           UNION ALL
          SELECT CFONETLCON,COBSETLCON FROM HSSUSUA,HSSTLCON
           WHERE NNUMEUSUA = :usuario
             AND HSSUSUA.NNUMETITU = HSSTLCON.NNUMETITU
           UNION ALL
          SELECT CFONETLUSU,COBSETLUSU FROM HSSUSUA,HSSTLUSU
           WHERE HSSUSUA.NNUMEUSUA = :usuario
             AND HSSUSUA.NTITUUSUA <> HSSUSUA.NNUMEUSUA
             AND HSSUSUA.NTITUUSUA = HSSTLUSU.NNUMEUSUA ";
            
  $sql2->addParam(":usuario",$sql_usu->result("NNUMEUSUA"));
  $sql2->executeQuery($txt); 

  $telefone = '';
  while (!$sql2->eof()) {
    $telefone .= $sql2->result("CFONETLUSU")." ".$sql2->result("COBSETLUSU")."<br />";
    $sql2->next();
  }       
  
  $tpl->CONSULTA_TELEFONES = substr($telefone,0,strlen($telefone)-6);
  
  $sql2->clear();
  $txt = "SELECT CDESCTXMEN
            FROM HSSTXUSU,HSSTXMEN
           WHERE NNUMEUSUA = :usuario
             AND DCANCTXUSU IS NULL
             AND HSSTXUSU.NNUMETXMEN = HSSTXMEN.NNUMETXMEN";
  $sql2->addParam(":usuario",$sql_usu->result("NNUMEUSUA"));
  $sql2->executeQuery($txt); 

  $aditivo = '';
  while (!$sql2->eof()) {
    $aditivo .= $sql2->result("CDESCTXMEN")."<br />";
    $sql2->next();
  }  
     
  $tpl->CONSULTA_ADITIVOS = substr($aditivo,0,strlen($aditivo)-6);
  
  $sql_usu->clear();       
  $txt = "SELECT CGRAUUSUA,CNOMEUSUA,CNOMECARE,DVENCCRUS,CCODIUSUA,HSSCRUS.NNUMECARE,HSSCRUS.NNUMEUSUA
            FROM HSSUSUA,HSSCRUS,HSSCARE
           WHERE HSSUSUA.CCODIUSUA = :carteira
             AND HSSUSUA.NNUMEUSUA = HSSCRUS.NNUMEUSUA
             AND HSSCRUS.NNUMECARE = HSSCARE.NNUMECARE
           ORDER BY 1 DESC,2,4 DESC";
  $sql_usu->addParam(":carteira",$carteira);
  $sql_usu->executeQuery($txt);
      
  if ($sql_usu->count() > 0) { 
    while (!$sql_usu->eof()) {
      $tpl->CARENCIA_NOME = $sql_usu->result("CNOMECARE");
      $tpl->CARENCIA_VENCIMENTO = $sql_usu->result("DVENCCRUS");
      $tpl->CARENCIA_ID = $sql_usu->result("NNUMECARE");
      $tpl->ID_USUARIO = $sql_usu->result("NNUMEUSUA");
      $tpl->block("LINHA");        
      $sql_usu->next();     
    }
    $tpl->block("CARENCIAS");              
    
  } else {
    $tpl->MSG = "** Benefici�rio sem car�ncia **";
    $tpl->block("SEM_CARENCIA");
  }      
  $tpl->block("RESULTADO_CARENCIAS"); 

  $tpl->block("RESULTADO");
  */
?>