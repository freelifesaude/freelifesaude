<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
     
  if (isset($_POST['codigo'])) {
        
    if ($_POST['codigo'] <> "") {
      $sql = new Query($bd);
      $txt = "SELECT COBSEOBUSU,DDATAOBUSU FROM HSSOBUSU,HSSUSUA
               WHERE HSSOBUSU.CWEBAOBUSU = 'S'
                 AND HSSOBUSU.COBSEOBUSU IS NOT NULL
                 AND HSSOBUSU.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                 AND HSSUSUA.CCODIUSUA = :codigo 
                 AND NVL(HSSOBUSU.DLIMIOBUSU,TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
               ORDER BY 2 DESC";      

      $sql->clear();
      $sql->addParam(":codigo",$_POST['codigo']);
      $sql->executeQuery($txt);
      
      $obse = "";              
      while (!$sql->eof()) {       
        $obse .= $sql->result("COBSEOBUSU").chr(13).chr(10);          
        $sql->next();
      }  
      
      $observacao = '';
      if ($obse <> '') {        
        $r = new rtf($obse);
        $r->output( "xml");
        $r->parse();
        if( count( $r->err) == 0)  // no errors detected
          $observacao = $r->out;
        else
          $observacao = $r->err;
        //$r->close();
      }
           
      if ($observacao == '') {

        $obse = "";           
        $txt = "SELECT CTEXTALERT,DDATAALERT FROM HSSUSUA,HSSTITU,HSSALERT
                 WHERE HSSUSUA.CCODIUSUA = :codigo                  
                   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMETITU = HSSALERT.NNUMETITU
                   AND HSSALERT.CTEXTALERT IS NOT NULL
                   AND HSSALERT.CWEBAALERT = 'S'
                   AND NVL(HSSALERT.DVENCALERT,TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
               ORDER BY 2 DESC";
               
        $sql->clear();
        $sql->addParam(":codigo",$_POST['codigo']);
        $sql->executeQuery($txt);
      
        while (!$sql->eof()) {       
          $observacao .= $sql->result("CTEXTALERT").chr(13).chr(10);          
          $sql->next();
        }
      }        
      $observacao .= "";
      if ($observacao == '') {
            
        $obse = "";
        $txt = "SELECT COBSETITU FROM HSSUSUA,HSSTITU
                 WHERE HSSUSUA.CCODIUSUA = :codigo                  
                   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                   AND HSSTITU.COBSETITU IS NOT NULL
                   AND HSSTITU.CMOBWTITU = 'S' ";

        $sql->clear();
        $sql->addParam(":codigo",$_POST['codigo']);
        $sql->executeQuery($txt);
                           
        $obse = $sql->result("COBSETITU");

        if ($obse <> '') {        
          $r1 = new rtf($obse);
          $r1->output( "xml");
          $r1->parse();
          if( count( $r1->err) == 0)  // no errors detected
            $observacao = $r1->out;
          else
            $observacao = $r1->err; 
          //$r1->close();            
        }  
        
        if ($observacao == '<br/>')
          $observacao = "";
      }     
        
      if ($observacao == '') {

        $obse = "";
        $txt = "SELECT COBSEPLAN FROM HSSUSUA,HSSPLAN
                 WHERE HSSUSUA.CCODIUSUA = :codigo                  
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                   AND HSSPLAN.COBSEPLAN IS NOT NULL
                   AND HSSPLAN.CMOBWPLAN = 'S' ";

        $sql->clear();
        $sql->addParam(":codigo",$_POST['codigo']);
        $sql->executeQuery($txt);
                             
        $observacao = utf8_encode($sql->result("COBSEPLAN"));      
      }
      if ($_SESSION['apelido_operadora'] == 'unimedFranca'){
      
        $txt = "SELECT RETORNA_NOME_ACOMODACAO(NVL(HSSUSUA.NNUMEACOM,NVL(HSSTITU.NNUMEACOM,HSSPLAN.NNUMEACOM))) ACOMODACAO
              FROM HSSUSUA,HSSPLAN,HSSTITU
             WHERE HSSUSUA.CCODIUSUA = :codigo 
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU";

        $sql->clear();
        $sql->addParam(":codigo",$_POST['codigo']);
        $sql->executeQuery($txt);
                             
        $observacao .= " ACOMODACAO : " .utf8_encode($sql->result("ACOMODACAO"));      
      }      
    }        
  }
                      
  $observacao = strip_tags($observacao);
  $observacao = stripslashes($observacao);
  $observacao = htmlentities($observacao);
  $observacao = htmlspecialchars($observacao);
  
  echo $observacao;
  
  $bd->close();
  
?>