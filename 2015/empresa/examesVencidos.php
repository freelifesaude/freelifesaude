<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "RELATÓRIO DE EXAMES VENCIDOS";
  require_once("../comum/layout.php");
  $tpl->addFile("CONTEUDO","examesVencidos.htm");  
   
          
//  $tpl->ID_SESSAO = $_GET['idSessao'];
  
 $data_fim = $_POST['data'];
  
  if (isset($_POST['imprimir'])) {
  
    require_once('examesVencidosImp.php');
  
  }
  $tpl->DATA_FIM = $data_fim;
    
  $tpl->block("MOSTRA_MENU");    
  $bd->close();  
  $tpl->show();     
  
?>