<?php
  Session_start();
  
  require_once('../comum/sessao.php');
  require_once("../comum/autoload.php");

  $pagina = $_GET['pagina'] ;
    
  $bd = new Oracle();

  $_SESSION['titulo'] = "PEDIDO DE EXAMES E PROCEDIMENTOS AMBULATORIAIS";
  $tipoGuia           = 'guiaPedido'; 

  require_once("../comum/layout.php");  
  $tpl->addFile("CONTEUDO","procedimento.htm");

  $tpl->ID_SESSAO = $_GET['idSessao'];  
  
  $solicitante        = '';
  $executante         = 0;
  $codigo             = '';
  $especialidade      = '';
  $regime             = '';
  $tipo_doenca        = '';
  $tempo              = '';
  $unidade            = '';
  $indicador          = '';
  $data_internacao    = '';
  $hora_internacao    = '';
  $data_prev_alta     = '';
  $hora_prev_alta     = '';
  $leito              = '';  
  $tipo_consulta      = '';
  $cid                = '';
  $hipotese           = '';
  $senha              = '';
  $nascimento         = '';
  $guiaprincipal      = '';
  $observacoes        = '';
  $msgVisualizada     = '';
  $nomeSolicitante    = '';
  $texto_msg          = '';
  $natureza           = '';
  $regimeInternacao   = '';
  $tipoAtendimento    = '';
  $tipoSaidaSadt      = '';
  $atendimentoRn      = '';
  $previsao           = '';
  $procedimentos      = array();
  $dentes             = array();
  $faces              = array();
  $quantidades        = array();

  $indicador     = '9';
  $tpl->{"INDICADOR_".$indicador}     = "selected";      

  $tpl->TIPO_ATENDIMENTO = '5'; 
  
  $tpl->PEDIDO       = 'S';
  
  $executante        = 0;      
  $localAtendimento  = 0;
  
 
  $natureza           = 'A';    
  $regimeInternacao   = '';
  $informaSolicitante = true;    
  
  
  if (isset($_POST['autorizar'])) {  
    $solicitante        = $seg->antiInjection($_POST['solicitante']);  
    $nomeSolicitante    = $seg->antiInjection(utf8_decode($_POST['nome_solicitante']));    
    $tipoAtendimento	  = $seg->antiInjection($_POST['tipo_atendimento']);
    $tipoSaidaSadt  	  = $seg->antiInjection($_POST['tipo_de_saida']);     
    $codigo            = $seg->antiInjection($_POST['codigo']);    
    
    if (isset($_POST['especialidade']))
      $especialidade     = $seg->antiInjection($_POST['especialidade']); 
      
    $regime            = $seg->antiInjection($_POST['regime']); 
    $tipo_doenca       = $seg->antiInjection($_POST['tipo_doenca']); 
    $tempo             = $seg->antiInjection($_POST['tempo']);       
    $unidade           = $seg->antiInjection($_POST['unidade']);     
    $indicador         = $seg->antiInjection($_POST['indicador']);     
	

    if (isset($_POST['tipo_consulta']))
      $tipo_consulta     = $seg->antiInjection($_POST['tipo_consulta']);   
    
    if (isset($_POST['tipo_de_saida']))
      $tipoSaidaSadt     = $seg->antiInjection($_POST['tipo_de_saida']);  
      
    $cid               = $seg->antiInjection($_POST['cid']);
    $hipotese          = $seg->antiInjection($_POST['hipotese']); 
    $previsao          = $seg->antiInjection($_POST['previsao']);   
    $observacoes       = $seg->antiInjection($_POST['observacao_guia']);
    $msgVisualizada    = $seg->antiInjection($_POST['mensagem_visualizada']);    
  } 

  
  if (isset($_POST['_procedimento'])) {
    foreach ($_POST['_procedimento'] as $numero) {

      if (isset($_POST['procedimento'.$numero])) {
        if ($_POST['procedimento'.$numero] <> '') {
          $procedimentos[$numero] = $seg->antiInjection($_POST['procedimento'.$numero]);
          
          if (array_key_exists('quantidade'.$numero,$_POST))
            $quantidades[$numero]   = $seg->antiInjection($_POST['quantidade'.$numero]);
          else
            $quantidades[$numero]   = 1;             

        }
      }          
    }   
  }
  
  $liberado = sizeOf($procedimentos); 
   
  if (isset($_POST['autorizar'])) {
    
    $camposObrigatorios = '';
    
    for ($p = 1;$p <= sizeof($_FILES);$p++) {
      $anexo = array();
      $anexo = $_FILES['upload'.$p];
      
      if ($anexo['name'] <> '')
        $camposObrigatorios .= $util->validaExtensaoArquivo($anexo['name']);
    } 
    
    if ($codigo == '')
      $camposObrigatorios .= '- Informe o c�digo do benefici�rio<br>';	  
     
    if (($solicitante == 0) and ($informaSolicitante))
      $camposObrigatorios .= '- Informe o prestador solicitante<br>';
        
    if (($executante == 0 ) and (($_SESSION['pedido_exames'] == '1') or ($tipoGuia == 'guiaProcedimento') or ($tipoGuia == 'guiaConsulta')))
      $camposObrigatorios .= '- Informe o prestador executante<br>';

    if (($especialidade == 0 ) and (($_SESSION['pedido_exames'] == '1') or ($tipoGuia == 'guiaProcedimento')))
      $camposObrigatorios .= '- Informe a especialidade<br>';
      
    if ( $liberado == 0 )
      $camposObrigatorios .= '- Informe pelo menos um item da guia.<br>';            
    
	  $tem_carencia = false;
	
  
    if ($camposObrigatorios <> '') {
      $msg = new Dialog();      
      $msg->title = "Aten��o";
      $msg->type  = "erro";
      $msg->text  = $camposObrigatorios;
      $msg->setWidth(500);
      $msg->showTpl($tpl);  
    } 
    else {
      $beneficiario = new Beneficiario($bd,$codigo);      
      $beneficiario->leitorCartao = $_SESSION['tem_leitora'];
      $beneficiario->validaBeneficiarioGuia($nascimento,$senha,$regime,$natureza,$guiaprincipal);
      
      if ($beneficiario->getQtdeErrosFatal() == 0) {
      
        $guia = new Guia($bd);     
        $guia->idBeneficiario      = $beneficiario->idBeneficiario;
        $guia->tipoGuia            = $tipoGuia;                
        $guia->idSolicitante       = $solicitante;
        $guia->idExecutante        = $executante;
        $guia->idLocalAtendimento  = $localAtendimento;
        $guia->idEspecialidade     = $especialidade;
        $guia->regime              = $regime;
        $guia->natureza            = $natureza;
        $guia->tipoAtendimento     = $tipoAtendimento;
        $guia->tipoSaidaSadt       = $tipoSaidaSadt;
        $guia->indicadorAcidente   = $indicador;
        $guia->data_internacao     = $data_internacao;
        $guia->hora_internacao     = $hora_internacao;
        $guia->data_prev_alta      = $data_prev_alta;
        $guia->hora_prev_alta      = $hora_prev_alta;
        $guia->leito               = $leito;		
        $guia->tipoConsulta        = $tipo_consulta;
        $guia->tipoDoenca          = $tipo_doenca;
        $guia->tempoDoenca         = $tempo;
        $guia->unidadeTempoDoenca  = $unidade;
        $guia->hipoteseDiagnostica = $hipotese;
        $guia->idGuiaPrincipal     = $guiaprincipal;
        $guia->observacoes         = $observacoes;
        $guia->regimeInternacao    = $regimeInternacao;
        $guia->atendimentoRn       = $atendimentoRn;
        $guia->previsao            = $previsao;
        
        if ($cid <> '')
          $guia->adicionaCID($cid);
          
        $guia->idOperador         = $_SESSION['id_operador'];
        
        $guia->localEmissao = '9';          

        for ($p = 0;$p < sizeof($procedimentos);$p++) {
          $arr = array_keys($procedimentos);
          $n = $arr[$p];
          $guia->adicionaProcedimento($procedimentos[$n],$quantidades[$n],$dentes[$n],$faces[$n]);
        }             
        
        for ($p = 1;$p <= sizeof($_FILES);$p++) {
          $anexo = array();
          $anexo = $_FILES['upload'.$p];
          $guia->adicionaAnexo($anexo['name'],$anexo['tmp_name'],$anexo['size']);
        }         
        
        $guia->save();        

        if ($guia->qtdeErros() > 0) {
          $msg = new Dialog();
          $msg->type = "erro";
          $msg->title = "Erro";
          $msg->text = $guia->getErros();          
          $msg->setWidth(700);
          $msg->showTpl($tpl);
        }
        else {
          $guia->validarGuia();
          $guia->calcularGuia();
          $Intercambio = $beneficiario->verificaIntercambio($codigo);
         
		      if ( ($beneficiario->getQtdeErros() > 0) and ($Intercambio == 'N') ) {
            $guia->setStatusGuia('N');
            
            $erros = array();
            $erros = $beneficiario->getArrayErros();       
            for ($b = 0;$b < sizeof($erros);$b++) {      
              $erro = array();
              $erro = $erros[$b];            
              
              if ($erro[2] <> '')
                $guia->adicionarCritica($erro[2]);
            } 
                
            if ($_SESSION['gera_negada'] == "S") {
              $msg = new Dialog();
              $msg->type = "erro";
              $msg->title = "Emiss�o de guia";
              $msg->text = "Autoriza��o n�o concedida, protocolo n�mero: ".$guia->getIdGuia().".<br><br>".$beneficiario->getErros().$guia->getErros();         
              $msg->addButton("OK","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);            
              $msg->setWidth(700);
              $msg->showTpl($tpl);
              $func->salvaGuiaErro($guia->getIdGuia(),$guia->getErros(),$beneficiario->getErros());              
            }
            else {
              $msg = new Dialog();
              $msg->type = "erro";
              $msg->title = "Emiss�o de guia";
              $msg->text = "Autoriza��o n�o concedida.<br><br>".$beneficiario->getErros().$guia->getErros();         
              $msg->addButton("OK","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);            
              $msg->setWidth(700);
              $msg->showTpl($tpl);          
              $func->salvaGuiaErro($guia->getIdGuia(),$guia->getErros(),$beneficiario->getErros());
              $guia->excluir();              
            }
          } 
          else if ($guia->qtdeErros() > 0) {
            $guia->setStatusGuia('N');
            
            if ($_SESSION['gera_negada'] == "S") {
              $msg = new Dialog();
              $msg->type = "erro";
              $msg->title = "Emiss�o de guia";
              $msg->text = "Autoriza��o n�o concedida, protocolo n�mero: ".$guia->getIdGuia().".<br><br>".$guia->getErros();          
              $msg->addButton("OK","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);            
              $msg->setWidth(700);            
              $msg->showTpl($tpl);
              $func->salvaGuiaErro($guia->getIdGuia(),$guia->getErros(),$beneficiario->getErros());
            }
            else {
              $msg = new Dialog();
              $msg->type = "erro";
              $msg->title = "Emiss�o de guia";
              $msg->text = "Autoriza��o n�o concedida.<br><br>".$guia->getErros();         
              $msg->addButton("OK","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);            
              $msg->setWidth(700);
              $msg->showTpl($tpl);          
              $func->salvaGuiaErro($guia->getIdGuia(),$guia->getErros(),$beneficiario->getErros());
              $guia->excluir();              
            }           
          }  
          else {    		
            $guia->calcularGuia();                   
                        
            $msg = new Dialog();
            $msg->type = "confirmacao";
            $msg->title = "Guia gerada";
            
	
			      /* Verifica se h� necessidade de autorizacao no contrato */ 
            $txt = "SELECT NVL(CAUTOTITU,'N') CAUTOTITU
                      FROM HSSTITU,HSSUSUA,HSSGUIA
                     WHERE HSSGUIA.NNUMEGUIA = :guia
                       AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
					   AND HSSTITU.NNUMETITU = HSSUSUA.NNUMETITU";
            $sql2 = new Query($bd);
            $sql2->addParam(":guia",$guia->getIdGuia());
            $sql2->executeQuery($txt);
            
            if ($sql2->result("CAUTOTITU") <> "N") {
              $guia->setStatusGuia('P');
            
              $msg->text = "Esta libera��o N�O FOI CONCEDIDA. Ela necessita de autoriza��o da empresa.<br/> 
                            Acompanhe pelo menu Emiss�o de guias/Rela��o de guias emitidas, o processo n� ".$guia->getIdGuia();                            
            }				
            else {                          
              if (($tipoGuia == 'guiaPedido') and ($guia->getStatusGuia() <> '29')) { 		      //Verifica se � intercambio
                if ($guia->getStatusGuia() == 'A')
                  $msg->text = "Pedido <b>".$guia->getIdGuia()."</b> gerado com sucesso, por�m enviada para Auditoria M�dica.
                                Por favor aguarde a libera��o ou entre em contato com a Operadora.";
								else {
                  $msg->text = "Pedido de exame n� <b>".$guia->getIdGuia()."</b> gerado com sucesso, para o c�digo <b>".$codigo."</b>.";				
								}
                                  
              }
              else {
                if ($guia->getStatusGuia() == 'A')
                  $msg->text = "Solicita��o <b>".$guia->getIdGuia()."</b> gerada com sucesso, por�m enviada para Auditoria M�dica. 
                                Por favor aguarde a libera��o ou entre em contato com a Operadora. ";
                else if ($guia->getStatusGuia() == '29')
                  $msg->text = "Guia <b>".$guia->getIdGuia()."</b> gerada com sucesso, por�m est� sendo encaminhada para a unimed 
                                origem em virtude de se tratar de um paciente de interc�mbio. 
                                Acompanhe o andamento da mesma em janela ao lado.";
                else if ($guia->getStatusGuia() == '') {
                
                  $filtros = array();
                  $filtros['guia']     = $guia->getIdGuia();
                  $filtros['pagina']   = $tipoGuia;
                  $_SESSION['filtros'] = $filtros; 
                  
								  $msg->text = "Guia <b>".$guia->getIdGuia()."</b> gerada com sucesso, para o c�digo <b>".$codigo."</b>.";
								  $msg->addButton("Imprimir","popup","imprimeGuia.php?idSessao=".$_GET['idSessao'],"procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
              
           
                }
                else if ($guia->getStatusGuia() == 'P')
                  $msg->text = "Solicita��o <b>".$guia->getIdGuia()."</b> gerada com sucesso, por�m Aguardando autoriza��o da Operadora. ";
                else if ($guia->getStatusGuia() == 'C')
                  $msg->text = "Solicita��o <b>".$guia->getIdGuia()."</b> gerada com sucesso, por�m Aguardando confirma��o da Operadora. ";                
                else
                  $msg->text = "Guia n�o liberada, favor entrar em contato com a operadora.".$guia->getIdGuia()."com o status.".$guia->getStatusGuia()."</b>.";                          
              }
            }
            
            $msg->addButton("OK","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
            $msg->setWidth(500);
            $msg->showTpl($tpl);           
          }
        }
      } 
      else { 
        $msg = new Dialog();
        $msg->type = "erro";
        $msg->title = "Emiss�o de guia";
        $msg->text = $beneficiario->getErrosFatal();          
        $msg->setWidth(500);
        $msg->showTpl($tpl);         
      }
      
      unset($_POST['autorizar']);
      unset($_POST['codigo']);       
    }   
  }

  /**
    Incio de montagem da tela
  **/ 

  $func->carregaSolicitantes($bd,$tpl,$solicitante,"pedido_empresa");  

  $tpl->block("SOLICITANTES");    
  $tpl->NOME_SOLICITANTE = $nomeSolicitante;
  $tpl->ID_SOLICITANTE   = $solicitante;
   
  $tpl->NOME_CONTRATADO = "";
    
 

  $tpl->MASCARA          = $_SESSION['mascara'];
  $tpl->UNIMED           = $_SESSION['codigo_unimed'];  
  $tpl->CODIGO           = $codigo;
  $tpl->NOME_SOLICITANTE = $nomeSolicitante;
  $tpl->ID_SOLICITANTE   = $solicitante;
  $tpl->OBSERVACAO_GUIA  = $observacoes;
    
  $tpl->CODIGO_CLASSE = "span12";
        

  $tpl->TYPE = "text";    
  $tpl->LABEL_CODIGO  = 'C�digo do benefici�rio:';    
  $tpl->HABILITADO = "";   

 
  $tpl->CODIGO_CLASSE = "input-append span10";

  $tpl->TITLE_LEITORA = "Localizar benefici�rio sem cart�o";
  $tpl->DESC_LEITORA = "Sem cart�o";
  $tpl->block("LEITORA");     
   
  $regime_nome['E'] = "Eletiva";
  $regime_nome['U'] = "Urgente";
  $regimes = array('E','U');    

  
  if ($_SESSION['regime_pedido'] == 'S')
    $regimes = array('E','U');    
  else if ($_SESSION['regime_pedido'] == 'E')
    $regimes = array('E');    
  else if ($_SESSION['regime_pedido'] == 'U')
    $regimes = array('U');        

  
  foreach($regimes as $n) {
    $tpl->REGIME_ID = $n;
    $tpl->REGIME_NOME = $regime_nome[$n];   
    
    if ($regime == $n)
      $tpl->REGIME_SELECIONADO = "selected";
    else
      $tpl->REGIME_SELECIONADO = "";
      
    $tpl->block("REGIME");
  }
      
  $tpl->block("MOSTRA_REGIME");        
  
  $tpl->block("MOSTRA_TIPO_ATENDIMENTO");

  $tpl->block("MOSTRA_TIPO_DE_SAIDA_SADT");

  $tpl->ALTERA_QTDE = "readyonly";    
  
  $numero = 0;
  
  $tpl->block("PLANO_MEDICO_TITULO");
  $tpl->EH_ODONTO = 'N';


  if (isset($_POST['_procedimento'])) {
    foreach ($_POST['_procedimento'] as $numero) {    
    
      $tpl->NUMERO = $numero;

      if (array_key_exists($numero,$procedimentos))
        $tpl->PROCEDIMENTO_VALOR = $procedimentos[$numero];
      else
        $tpl->PROCEDIMENTO_VALOR = "";

      if (array_key_exists($numero,$quantidades))
        $tpl->PROCEDIMENTO_QTDE = $quantidades[$numero];   
      else
        $tpl->PROCEDIMENTO_QTDE = 1;

      $tpl->block("PLANO_MEDICO");

      $tpl->block("LINHA_PROCEDIMENTO");
    }
  }
  else {      
    while ($numero < 5) { 
    
      $tpl->NUMERO             = $numero;
      $tpl->PROCEDIMENTO_VALOR = "";
      $tpl->PROCEDIMENTO_DENTE = "";
      $tpl->PROCEDIMENTO_FACE  = "";
      $tpl->PROCEDIMENTO_QTDE  = 1;
      
      $tpl->block("PLANO_MEDICO");
          
      $tpl->block("LINHA_PROCEDIMENTO");
      $numero = $numero + 1;    
    }
  }
  
  $tpl->block("PROCEDIMENTOS");      
  
  
  if ($anexos)
    $tpl->block("ANEXOS"); 
            
  if ($tipoGuia == 'guiaPedido') {
    $tpl->NOME_BOTAO = "Gerar pedido";  
  }
  
  $tpl->PREVISAO = $previsao;
  $tpl->ESPECIALIDADE_SELECIONADA = $especialidade;  
  $tpl->MENSAGEM_VISUALIZADA      = $msgVisualizada;
  
  $tpl->block("LIBERA_PROCEDIMENTO");
    
  if ($texto_msg <> '') {
    $msg = new Dialog();      
    $msg->type  = $tipo_msg;
    $msg->title = $titulo_msg;
    $msg->text  = $texto_msg;
    $msg->setWidth(500);
    $msg->showTpl($tpl);
  }  
      
  $tpl->block("MOSTRA_MENU");
  
  $bd->close();
  $tpl->show();     


?>