<?php
  Session_start();
  require_once('../sessao.php');
  require_once("../autoload.php"); 
  require_once("../funcao.php");


  
  $tpl = new Template("capturaBio.htm");
  $tpl->ID_USUARIO2 = $_GET["id_usuario"];
  
  $bd = new Oracle();
  
  $sql = new Query($bd);
    
  $txt = " SELECT CDPOLUSUA,CDINDUSUA,CDMEDUSUA,CDANEUSUA,CDMINUSUA,
           CDPO2USUA,CDIN2USUA,CDME2USUA,CDAN2USUA,CDMI2USUA,CCODIUSUA
             FROM HSSUSUA
           WHERE LOWER(MD5(NNUMEUSUA)) = :usuario";
    
  $sql->addParam(":usuario",$_GET['id_usuario']);
  $retorno = $sql->executeQuery($txt);
    
  if (($sql->result("CDPOLUSUA") == '') and ($sql->result("CDINDUSUA") == '') and  ($sql->result("CDMEDUSUA") == '') 
      and ($sql->result("CDANEUSUA") == '') and ($sql->result("CDMINUSUA") == '') and ($sql->result("CDPO2USUA") == '') 
      and ($sql->result("CDIN2USUA") == '') and ($sql->result("CDME2USUA") == '') and ($sql->result("CDAN2USUA") == '')
      and ($sql->result("CDMI2USUA") == '')) {
   //Variavel utilizada para exibir o bot�o Gravar Biometria caso o beneficiario n�o tenha cadastradas.
  //  $tpl->TEM_BIOMETRIA = 'N';
          $tpl->BIO_TEMP_ID = "template1";
          $tpl->BIO_TEMP_VALUE = "";
          $tpl->BIO_TEMP_DEDO = "";
          $tpl->block("BIO_TEMP");
          $tpl->STATUS_INICIAL = 0;
          $tpl->block("BIO_NAO_LOCALIZADA");
          //Mao e Dedos para Biometria
          $tpl->ID_MAO = 'D';
          $tpl->DESC_MAO = 'Direita'; 
          $tpl->block("MAO");
          $tpl->ID_MAO = 'E';
          $tpl->DESC_MAO = 'Esquerda'; 
          $tpl->block("MAO");
          $arrayD = '"P":"Polegar","I":"Indicador","M":"M�dio","A":"Anular","N":"M�nimo",';
          $arrayE = $arrayD;
          $tpl->MAO_DIREITA = substr($arrayD,0,-1);
          $tpl->MAO_ESQUERDA = substr($arrayE,0,-1);          
  } else {
    $dedos = array();
        if($sql->result("CDPOLUSUA")!=''){
          $dedos['MD_P'] = $sql->result("CDPOLUSUA");   
        }
        if($sql->result("CDINDUSUA")!=''){
          $dedos['MD_I'] = $sql->result("CDINDUSUA");
        }
        if($sql->result("CDMEDUSUA")!=''){
          $dedos['MD_M'] = $sql->result("CDMEDUSUA"); 
        }
        if($sql->result("CDANEUSUA")!=''){
          $dedos['MD_A'] = $sql->result("CDANEUSUA"); 
        }
        if($sql->result("CDMINUSUA")!=''){
          $dedos['MD_N'] = $sql->result("CDMINUSUA");
        }
        if($sql->result("CDPO2USUA")!=''){
          $dedos['ME_P'] = $sql->result("CDPO2USUA");
        }
        if($sql->result("CDIN2USUA")!=''){
          $dedos['ME_I'] = $sql->result("CDIN2USUA");
        }
        if($sql->result("CDME2USUA")!=''){
          $dedos['ME_M'] = $sql->result("CDME2USUA");
        }
        if($sql->result("CDAN2USUA")!=''){
          $dedos['ME_A'] = $sql->result("CDAN2USUA");
        }
        if($sql->result("CDMI2USUA")!=''){
          $dedos['ME_N'] = $sql->result("CDMI2USUA");  
        }
        $i=1;
        $direita = 1;
        $esquerda = 1;
        $arrayD = "";
        $arrayE = "";
        foreach ($dedos as $dedo=>$value) {
          $tpl->BIO_TEMP_ID = "template".$i;
          $tpl->BIO_TEMP_VALUE = $value;
          $tpl->BIO_TEMP_DEDO = $dedo;
          $i++;
          
          if( (strstr($dedo, 'MD')) and ($direita == 1) ){
            $tpl->ID_MAO = 'D';
            $tpl->DESC_MAO = 'Direita'; 
            $tpl->block("MAO");
            $direita = 0;
          }
          if( (strstr($dedo, 'ME')) and ($esquerda == 1) ){
            $tpl->ID_MAO = 'E';
            $tpl->DESC_MAO = 'Esquerda'; 
            $tpl->block("MAO");
            $esquerda = 0;
          }
            
            if( strstr($dedo, '_P') ){
                if( strstr($dedo, 'MD'))             
                $arrayD .= '"P":"Polegar",';
              else
                $arrayE .= '"P":"Polegar",';
            }
            if( strstr($dedo, '_I') ){
                if( strstr($dedo, 'MD'))             
                $arrayD .= '"I":"Indicador",';
              else
                $arrayE .= '"I":"Indicador",';                
            }
            if( strstr($dedo, '_M') ){
                if( strstr($dedo, 'MD'))             
                $arrayD .= '"M":"M�dio",';
              else
                $arrayE .= '"M":"M�dio",';                
            }
            if( strstr($dedo, '_A') ){
                if( strstr($dedo, 'MD'))             
                $arrayD .= '"A":"Anular",';
              else
                $arrayE .= '"A":Anular",';
            }
            if( strstr($dedo, '_N') ){
                if( strstr($dedo, 'MD'))             
                $arrayD .= '"N":"M�nimo",';
              else
                $arrayE .= '"N":"M�nimo",';                
            
            }    
          $tpl->block("BIO_TEMP");
        }
          $tpl->MAO_DIREITA = substr($arrayD,0,-1);
          $tpl->MAO_ESQUERDA = substr($arrayE,0,-1);
          $tpl->block("TEM_BIO");
          $tpl->STATUS_INICIAL = 6;
        //Variavel utilizada para n�o exibir o bot�o Gravar Biometria     
  //  $tpl->TEM_BIOMETRIA = 'S';

  }
    
  $bd->close();    
  $tpl->show();     

?>