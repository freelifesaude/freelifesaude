<?php
  class PDFTable {
    private $colunas;
    private $alinhamento;
    private $preenchimento;
    private $altura;
    private $fpdf;
    
    public function PDFTable($pdf) {
      $this->altura = 5;
      $this->fpdf = $pdf;
    }      
    
    function setFont($family,$style,$size) {
      $this->fpdf->SetFont($family,$style,$size);
    }

    function setAlturaLinha($a) {
      $this->altura = $a;
    }
    
    function setColunas($w) {
      //Set the array of column widths
      $this->colunas = $w;
    }

    function setAlinhamentoColunas($a) {
      //Set the array of column alignments
      $this->alinhamento = $a;
    }

    function setPreenchimentoColunas($p) {
      $this->preenchimento = $p;
    }
    
    function linha($data) {
         
      //Calculate the height of the row
      $nb=0;
      for($i=0;$i<count($data);$i++)
        $nb=max($nb, $this->NbLines($this->colunas[$i], $data[$i]));
      $h=$this->altura*$nb;
      //Issue a page break first if needed
      $this->CheckPageBreak($h);
      //Draw the cells of the row
      for($i=0;$i<count($data);$i++) {
        $w=$this->colunas[$i];
        $a=isset($this->alinhamento[$i]) ? $this->alinhamento[$i] : 'L';
        $p=isset($this->preenchimento[$i]) ? $this->preenchimento[$i] : 'D';
        
        //Save the current position
        $x=$this->fpdf->GetX();
        $y=$this->fpdf->GetY();
        //Draw the border
        $this->fpdf->Rect($x+5, $y, $w, $h,$p);
        
        $this->fpdf->Cell(5,$this->altura);
        //Print the text        
        $this->fpdf->MultiCell($w, $this->altura, $data[$i], 0, $a);
        //Put the position to the right of the cell
        $this->fpdf->SetXY($x+$w, $y);
      }
      //Go to the next line
      $this->fpdf->Ln($h);
    }

    function CheckPageBreak($h) {
      //If the height h would cause an overflow, add a new page immediately
      if($this->fpdf->GetY()+$h>$this->fpdf->PageBreakTrigger)
        $this->fpdf->AddPage($this->fpdf->CurOrientation);
    }

    function NbLines($w, $txt) {
      //Computes the number of lines a MultiCell of width w will take
      $cw=&$this->fpdf->CurrentFont['cw'];
      if($w==0)
          $w=$this->fpdf->w-$this->fpdf->rMargin-$this->fpdf->x;
      $wmax=($w-2*$this->fpdf->cMargin)*1000/$this->fpdf->FontSize;
      $s=str_replace("\r", '', $txt);
      $nb=strlen($s);
      if($nb>0 and $s[$nb-1]=="\n")
          $nb--;
      $sep=-1;
      $i=0;
      $j=0;
      $l=0;
      $nl=1;
      while($i<$nb) {
        $c=$s[$i];
        if($c=="\n") {
          $i++;
          $sep=-1;
          $j=$i;
          $l=0;
          $nl++;
          continue;
        }
        
        if($c==' ')
          $sep=$i;
          
        $l+=$cw[$c];
        
        if($l>$wmax) {
          if($sep==-1) {
            if($i==$j)
              $i++;
          }
          else
            $i=$sep+1;
            
          $sep=-1;
          $j=$i;
          $l=0;
          $nl++;
        }
        else
          $i++;
      }
      return $nl;
    }
  }
?>