<?php
  require_once("../comum/autoload.php"); 
  include_once '../comum/securimage/securimage.php';
  $seg->secureSessionStart(); 
  
  require_once('../comum/sessao.php'); 
 
  $_SESSION['titulo'] = "FALE CONOSCO";
 
  require_once("../comum/layout.php");   
 
  $tpl->addFile("CONTEUDO","faleconosco.html");

  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  if ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista')
    $tpl->MASCARA = "111.9999.999999.99-9";
  
  $sql = new Query($bd);
  $txt = "SELECT NNUMECLATE,CDESCCLATE
            FROM HSSCLATE
           WHERE CLOCACLATE IN ('A','P')
             AND CSITUCLATE = 'S'
             AND NSUPECLATE IS NULL
           ORDER BY 2";
  $sql->executeQuery($txt);

  $tpl->CLASSIFICACAO_ID   = "";
  $tpl->CLASSIFICACAO_NOME = "";
  $tpl->block("LINHA_CLASSIFICACAO");
  
  while (!$sql->eof()) {
    $tpl->CLASSIFICACAO_ID   = $sql->result("NNUMECLATE");
    $tpl->CLASSIFICACAO_NOME = $sql->result("CDESCCLATE");
    $tpl->block("LINHA_CLASSIFICACAO");
    $sql->next();
  }  
  
  $tpl->parse();      
  $tpl->show();
?>