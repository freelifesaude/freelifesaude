<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();
        
  $pIdArquivo = $seg->antiInjection($_POST['pIdArquivo']);
  
  $sql = new Query($bd);
  $txt = "SELECT HSSARQUI.CDESCARQUI,HSSARQUI.CEXT_ARQUI EXTENSAO,HSSARQUI.NNUMEARQUI,
                 DECODE(HSSARQUI.CEXT_ARQUI,'PNG','S','JPG','S','JPEG','S','BMP','S','GIF','S','N') IMAGEM
            FROM HSSARQUI
           WHERE HSSARQUI.NNUMEARQUI = :idArquivo";   
  $sql->addParam(":idArquivo",$pIdArquivo);
  $sql->executeQuery($txt);
    
  $sql2 = new Query($bd);
  $txt2 = "SELECT HSSARQNT.CARQUARQNT
            FROM HSSARQNT
           WHERE HSSARQNT.NNUMEARQUI = :idArquivo";   
  $sql2->addParam(":idArquivo",$pIdArquivo);
  $sql2->executeQuery($txt2);
  
  if ($sql2->count() > 0)
    $img = $sql2->result("CARQUARQNT");
  else {
  
    $sql2 = new Query($bd);
    $txt2 = "SELECT HSSARQME.CARQUARQME
              FROM HSSARQME
             WHERE HSSARQME.NNUMEARQUI = :idArquivo";   
    $sql2->addParam(":idArquivo",$pIdArquivo);
    $sql2->executeQuery($txt2);
    
    $img = $sql2->result("CARQUARQME");     
  }
 
  if ($sql->result("IMAGEM") == 'S')
    header('Content-Type: image/jpeg');
  else if ($sql->result("EXTENSAO") == 'PDF')
    header('Content-Type: application/pdf');
  else
    header('Content-Disposition: attachment; filename="'.$sql->result("CDESCARQUI").".".strtolower($sql->result("EXTENSAO")).'"');      
  
  echo $img;
  
  $bd->close();
?>