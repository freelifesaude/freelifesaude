/**
 * 
 * API Javascript para Applet br.com.veridistec.vrbioapplet.VrBioApplet
 * <p>
 * Essa API foi constru�da para ser usada em conjunto com um Formul�rio,
 * populando campos com os templates biom�tricos capturados. O applet PODE ser
 * usado separadamente, embora n�o seja recomendado.
 * </p>
 * 
 * Javacript API to be used with br.com.veridistec.vrbioapplet.VrBioApplet
 * <p>
 * This API is built to be used with a Form, populating fields with the captured
 * biometric templates. The applet MAY be used separetedly, thou it is not
 * recommended.
 * </p>
 * 
 * @author <a href="mailto:moacyr.ricardo@veridistec.com.br">Moacyr Ricardo</a>
 * @version 1.0
 * 
 */
var VrBioApplet = {
    lastField : -1,
    finishedCapture : false,
    form : false,
    /**
     * Esse m�todo inicia a API, para ser utilizada junto com o applet cujo id �
     * <b>id</b>, utilizando os campos descritos no form <b>form</b>.
     * 
     * This method must be called before anything else. It starts the API,
     * marking that it should be used with the applet identified by <b>id</b>
     * and that it should use <b>form</b> as the form.
     * 
     * @param id -
     *            Esse par�metro � o o id do elemento applet. | The id that
     *            identifies the applet.
     * @param form -
     *            id do form "pai" do applet, onde os campos ser�o. | The form
     *            id, where the form fields are.
     */
    start : function(id, form) {
        this.fields = new Array();
        this.applet = document.getElementById(id);
        this.lastField = -1;
        this.form = form;
    },
    /**
     * Esse m�todo permite "adicionar" um campo � lista de campos que o callback
     * ir� percorrer preenchendo com os templates em BASE64. Se houver um
     * element IMG cujo atributo id seja composto do atributo id de um campo com
     * o sufixo "Img", ser� inserido nele um thumbnail da imagem biom�trica
     * (100px X 100px).
     * 
     * This method "adds" a form field to be receiver of the captured template.
     * This means that once a template is captured, the callback
     * VrBioApplet.onCapture will fill the fields with the biometric templates
     * in the order that they were added with this method.
     * 
     * @param field -
     *            id do campo | the form field id
     */
    addField : function(field) {
        if (!this.hasField(field))
            this.fields.push(field);
    },
    hasField : function(field) {
        for (f in this.fields) {
            if (f == field) {
                return true;
            }
        }
        return false;
    },
    isFinishedCapture : function() {
        return this.lastField == this.fields.length - 1;
    },
    /**
     * This method is a callback, to be called from within the applet. It
     * populates the form fields in order, with biometric templates captured by
     * the applet, in BASE64. It may populate IMG elements with id formed by a
     * form field id plus the suffix "Img".
     * 
     * Esse � o m�todo callback para captura. � chamado pelo applet quando uma
     * captura biom�trica � feita. Preenche os campos do form em ordem,
     * utilizando os templates biom�tricos codificados em BASE64. Caso haja um
     * elemento IMG cujo id � formado pelo id de um dos campos mais o sufixo
     * "Img", coloca uma imagem reduzida da biometria.
     */
    onCapture : function() {
        this.lastField = (this.lastField+1) % this.fields.length;

    var form         = document.getElementById(this.formclearTemplates);
    var fieldCmp     = document.getElementById(this.fields[this.lastField]);
//    var fieldPreview = document.getElementById(this.fields[this.lastField] + "Img");

    if (fieldCmp != null)
      fieldCmp.value = this.applet.getTemplate();
//    if (fieldPreview != null) {
//      fieldPreview.src = "data:image/png;base64," + this.applet.getImage(fieldPreview.width, fieldPreview.height);
//    }
    },

      /**
   * This method is a callback, to be called from within the applet. It
   * populates the form fields in order, with biometric templates captured by
   * the applet, in BASE64. It may populate IMG elements with id formed by a
   * form field id plus the suffix "Img".
   *
   * Esse � o m�todo callback para captura. � chamado pelo applet quando uma
   * captura biom�trica � feita. Preenche os campos do form em ordem,
   * utilizando os templates biom�tricos codificados em BASE64. Caso haja um
   * elemento IMG cujo id � formado pelo id de um dos campos mais o sufixo
   * "Img", coloca uma imagem reduzida da biometria.
   */   
  reset : function() {
    var form = document.getElementById(this.formclearTemplates);
    for (var i=0; i<this.fields.length; i++) {
      var fieldCmp = document.getElementById(this.fields[i]);
      var fieldPreview = document.getElementById(this.fields[i] + "Img");

      if (fieldCmp != null)
        fieldCmp.value = "";
      if (fieldPreview != null) {
        fieldPreview.src = "fingerprint.png";
      }
    }
    this.lastField = -1;
  //  this.applet.resetTemplateData();
  }
}
