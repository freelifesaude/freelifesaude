<?php
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");

  class PDF extends PDFSolus {
    
    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(150,5,'RELAT�RIO DE ERROS DE IMPORTA��O DE USU�RIOS',0,1);
      $this->SetFont('Arial','B',7);
      $this->Cell(40,5,'');
      $this->Cell(35,3,"Empresa:",0,0,'R');      
      $this->Cell(115,3,$_SESSION['logado'],0,1);      
      $this->Cell(40,5,'');
      $this->Cell(35,3,"Data do processamento:",0,0,'R');
      $this->Cell(115,3,$_SESSION['hoje'],0,1);      
      $this->Cell(40,5,'');
      $this->Cell(35,3,"Arquivo:",0,0,'R');
      $this->Cell(115,3,basename($this->getArray("arquivo")),0,1);
      $this->Ln(8);

      $this->SetFont('Arial','B',6);
      $this->Cell(10,3,'Linha',0,0);
      $this->Cell(50,3,'Usu�rio',0,0);
      $this->Cell(150,3,'Descri��o',0,0);      
      $this->Cell(60,3,'Valor',0,0);      
      $this->Ln(2);
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  
  $nome_arquivo = '../temp/'.$_POST['arquivo'];

  $xml = simplexml_load_file($nome_arquivo,"SimpleXMLElement",LIBXML_NOCDATA);      

  $arr = array("arquivo" => $nome_arquivo);
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','B',13);

  $pdf->SetFont('Arial','',6);
  foreach ($xml->erro as $linha) {
    $pdf->Cell(10,3,$linha->linha,0,0);        
    $pdf->Cell(50,3,$pdf->Copy($linha->nome,49),0,0);        
    $y = $pdf->getY();
    $pdf->MultiCell(150,3,utf8_decode($linha->descricao),0);  
    $pdf->setY($y);
    $pdf->setX(220);
    $pdf->MultiCell(60,3,utf8_decode($linha->valor),0);  
    $pdf->Ln(2);
  }

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 
?>
