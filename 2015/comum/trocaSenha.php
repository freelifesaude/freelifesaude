<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();
  require_once("../comum/sessao.php");

  $bd = new Oracle();
  
  $_SESSION['titulo'] = "ALTERAR SENHA";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/trocaSenha.html");
  
  if (isset($_POST['alterar'])) {
    $senhaAtual = $seg->antiInjection($_POST['atual']);  
    $novasenha  = $seg->antiInjection($_POST['nova_senha']);
	  $novasenha2 = $seg->antiInjection($_POST['nova_senha2']);
        
    if (($_SESSION['apelido_operadora'] == 'casse') and (!preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/",$novasenha))) {
      $tpl->MSG_CLASSE = "alert-error";
      $tpl->MSG        = "A nova senha deve possuir no m�nimo 8 d�gitos, uma letra min�scula, uma letra mai�scula e um caracter especial.";
      $tpl->block("MOSTRA_MSG"); 
    }
    else if (($_SESSION['apelido_operadora'] <> 'casse') and ((strlen($novasenha) < 6) or (strlen($novasenha) > 10))) {
      $tpl->MSG_CLASSE = "alert-error";
      $tpl->MSG        = "A nova senha deve possuir de 6 a 10 d�gitos.";
      $tpl->block("MOSTRA_MSG"); 
    }
    else if ($novasenha <> $novasenha2) {
      $tpl->MSG_CLASSE = "alert-error";
      $tpl->MSG        = "A senha informada n�o confere com a repeti��o. Tente novamente.";
      $tpl->block("MOSTRA_MSG"); 
    }
    else if ($novasenha == $senhaAtual) {
      $tpl->MSG = "A senha informada n�o pode ser igual a senha atual.";
      $tpl->MSG_CLASSE = "alert-error";
      $tpl->block("MOSTRA_MSG");
    }	
    else if ($novasenha == $novasenha2 and $novasenha <> '') {     

      $sql = new Query($bd);
      $txt = "SELECT NNUMEUSUA 
                FROM SEGUSUA,FINEMPR
               WHERE NNUMEUSUA = :usuario
                 AND ((CCRIPEMPR = 'N' AND CSENHUSUA = UPPER(:senha)) OR 
                      (CCRIPEMPR = 'S' AND CSENHUSUA = LOWER(MD5(:senha))))";
      $sql->addParam(":usuario",$_SESSION['id_operador']);
      $sql->addParam(":senha",$senhaAtual);
      $sql->executeQuery($txt);
                 
      if ($sql->result("NNUMEUSUA") > 0) {      
        $sql = new Query();
        $txt = "UPDATE SEGUSUA 
                   SET CSENHUSUA = DECODE(CATADUSUA,'N',UPPER(:novasenha),:novasenha), 
                       DVENSUSUA = DECODE(NVL(NIDIAUSUA,0),0,NULL,TRUNC(SYSDATE)+NVL(NIDIAUSUA,0)), 
                       CSVENUSUA = DECODE(NVL(NIDIAUSUA,0),0,'N',CSVENUSUA),
                       CCRIPUSUA = 'S'
                 WHERE NNUMEUSUA = :operador ";
        $sql->addParam(":operador",$_SESSION['id_operador']);
        $sql->addParam(":novasenha",$novasenha);
        $sql->executeSQL($txt);

        $tpl->MSG_CLASSE = "alert-success";
        
        if ($_SESSION['troca_de_senha'] == 'Automatica') {
          $_SESSION['troca_de_senha'] = '';
          $tpl->MSG        = "Senha alterada com sucesso. Clique <a href='".$_SESSION['diretorio']."/principal.php?idSessao=".$_GET['idSessao']."'>aqui</a> para continuar.";
        }
        else
          $tpl->MSG        = "Senha alterada com sucesso.";
        
        $tpl->block("MOSTRA_MSG");  
      }
      else {
        $tpl->MSG = "A senha atual est� incorreta.";
        $tpl->MSG_CLASSE = "alert-error";
        $tpl->block("MOSTRA_MSG");      
      }      
    } else if ($novasenha == '') {
      $tpl->MSG_CLASSE = "alert-error";
      $tpl->MSG        = "Informe uma senha v�lida para continuar.";
      $tpl->block("MOSTRA_MSG"); 
    }     
  }
  
  if ($_SESSION['troca_de_senha'] <> 'Automatica')
    $tpl->block("MOSTRA_MENU");    
    
  $bd->close();
  $tpl->show();     

?>