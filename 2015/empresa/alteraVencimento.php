<?php
  require_once("../comum/autoload.php");   
  $seg->secureSessionStart();  
  require_once('../comum/sessao.php');   
    
  $bd = new Oracle();
   
  $_SESSION['titulo'] = "ALTERA��O DA DATA DE VENCIMENTO DA MENSALIDADE";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/alteraVencimento.htm");
  
  $sql = new Query($bd);
  $txt = "SELECT NDIA_VENCI 
            FROM HSSTITU
           WHERE NNUMETITU = :contrato";
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
    
  $vencimento_atual = $sql->result("NDIA_VENCI");
  $tpl->VENCIMENTO_ATUAL = $vencimento_atual;
  
  if (isset($_POST['alterar'])) {
    if (isset($_POST['dia']))
	  $vencimento_proposto = $_POST['dia'];
	
	$motivo = $_POST['motivo'];
    
    if ($vencimento_atual == $vencimento_proposto) {
      $tpl->MSG = "O dia de vencimento proposto n�o pode ser igual o dia de vencimento atual.";
      $tpl->MSG_CLASSE = "alert-error";
      $tpl->block("MOSTRA_MSG");
    } 
    else {     
      $sql = new Query($bd);
      $sql->clear();
      $txt = "INSERT INTO HSSVCWEB (NNUMETITU,DDIGIVCWEB,NDIAAVCWEB,NDIAPVCWEB,CMOTIVCWEB,CSITUVCWEB)
                            VALUES (:contrato,sysdate,:vencimento_atual,:vencimento_proposto,:motivo,'P')";
      $sql->addParam(":contrato",$_SESSION['id_contrato']);
      $sql->addParam(":vencimento_atual",$vencimento_atual);
      $sql->addParam(":vencimento_proposto",$vencimento_proposto);
      $sql->addParam(":motivo",$motivo);
	  $sql->executeSQL($txt);
      
	  $tpl->MSG = "Solicita��o feita com sucesso.";
                
      $tpl->MSG_CLASSE = "alert-success";   
      $tpl->block("MOSTRA_MSG");      
    }   
  }
    
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT NDIA_VENCI,1 TIPO
            FROM HSSVENCI
           WHERE NDIA_VENCI > 0
             AND CSTATVENCI = 'A'";
		  
  $sql->executeQuery($txt);
  
  while (!$sql->eof()) {
    $tpl->VENCIMENTO_ID = $sql->result("NDIA_VENCI");
    $tpl->VENCIMENTO = $sql->result("NDIA_VENCI");
    $tpl->block("VENCIMENTOS");
    $sql->next();
  } 
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     
?>

