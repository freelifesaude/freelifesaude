<?php

  /**
   * Classes armazenar os dados dos prestadores
   *
   * @author Augusto
   */
  /* Classes */
  class Prestador {
    /* Propriedades */

    public $idPrestador          = 0; /* PK */
    public $rg                   = '';
    public $fax                  = '';
    public $cpf                  = '';
    public $cnpj                 = '';
    public $nome                 = '';
    public $foto                 = '';
    public $plano                = '';
    public $conselho             = '';
    public $telefone             = '';
    public $fantasia             = '';
    public $homepage             = '';
    public $documento            = '';
    public $tipo_pessoa          = '';
    public $uf_conselho          = '';
    public $doc_conselho         = '';
    public $tipo_documento       = '';
    public $tipo_prestador       = '';
    public $ultima_alteracao     = '';
    public $data_cancelamento    = '';
    public $inicio_atendimento   = '';
    public $inicio_substituicao  = '';
    public $urgencia_emergencia  = '';
    public $tipo_estabelecimento = '';
    public $temSubstituto        = '';

    /* Listas */ /* Tipos das Listas */
    public $lista_enderecos      = array();
    public $lista_telefones      = array();
    public $lista_graduacoes     = array();
    public $lista_especialidades = array();
    public $lista_corpo_clinico  = array();
    public $lista_substitutos    = array();
    
    public function __construct($id,$espec) {
      $this->idPrestador     = $id;
      $this->bd              = new Oracle();
      $this->carregaDados();
    }
    
    public function carregaDados() {
      $sql = new Query($this->bd);
      $txt = "SELECT * FROM FINPRES WHERE NNUMEPRES = :id";
      $sql->addParam(":id",$this->idPrestador);
      $sql->executeQuery($txt);
      
      $this->tipo_pessoa          = $sql->result('CPESSPRES');
      $this->cnpj                 = $sql->result('CCGC_PRES');
      $this->doc_conselho         = $sql->result('NCRM_PRES');
      $this->uf_conselho          = $sql->result('CUFCRPRES');
      $this->conselho             = $sql->result('CCONSPRES');
      $this->data_cancelamento    = $sql->result('DCANCPRES');
      $this->tipo_prestador       = $sql->result('CGRUPPRES');
      $this->homepage             = $sql->result('CHOMEPRES');
      $this->ultima_alteracao     = $sql->result("DALTEPRES");
      $this->telefone             = $sql->result('CFONEPRES');
      $this->fax                  = $sql->result('CFAX_PRES');
      $this->urgencia_emergencia  = $sql->result('CURGEPRES');
      $this->foto                 = $this->getImagemPrestador();
      $this->inicio_atendimento   = $sql->result('DINCLPRES');
      
      if ($_SESSION['apelido_operadora'] == 'freelife') {
        $this->nome                 = $sql->result("CFANTPRES");
        $this->fantasia             = $sql->result("CNOMEPRES");
      }       
      else {
        $this->nome                 = $sql->result("CNOMEPRES");
        $this->fantasia             = $sql->result("CFANTPRES");
      }
      
      if ($sql->result('CPESSPRES') == 'F') {
        $this->fantasia           = '';
        
        if (($this->conselho <> '') and ($this->doc_conselho <> '') and ($this->uf_conselho <> ''))
          $this->documento = '(' . $this->conselho . '-' . $this->uf_conselho . ' ' . $this->doc_conselho . ')';
      }
      else {
        if ($this->cnpj <> '')
          $this->documento = '( CNPJ: ' . $this->cnpj . ')';
      }
      
      $this->processaGraduacao();
      $this->lista_enderecos      = $this->processaEndereco();
      $this->lista_telefones      = $this->processaTelefone();
      $this->processaEspecialidade();       
      $this->processaSubstituto();
      
      $this->inicio_substituicao  = $this->inicioSubstituicao();
    }
    
    private function getImagemPrestador() {
      $sql = new Query($this->bd);
      $txt = "SELECT CIMAGIMAGE FROM HSSIMAGE WHERE NNUMEPRES = :prestador AND CNOMEIMAGE = 'WEB_FOTO'";
      $sql->addParam(':prestador', $this->idPrestador);
      $sql->executeQuery($txt);
      
      return $sql->result("CIMAGIMAGE");
    } 

    private function processaSubstituto() {
      $sql = new Query($this->bd);
      $txt = "SELECT TO_CHAR(DDATASUBPR,'DD/MM/YYYY') DATA,NSUBSPRES FROM HSSSUBPR WHERE NNUMEPRES = :prestador";
      $sql->addParam(':prestador', $this->idPrestador);
      $sql->executeQuery($txt);
      
      $this->lista_substitutos = array();
      $this->temSubstituto = "N";
      
      while (!$sql->eof()) {
        $this->temSubstituto = "S";
        $substituto = new Substituto();
        $substituto->idPrestador = $sql->result("NSUBSPRES");
        $substituto->inicio      = $sql->result("DATA");
        
        $this->lista_substitutos[] = $substituto;
        $sql->next();
      }
    }
    
    public function inicioSubstituicao($prestador,$substituto) {
      $sql = new Query($this->bd);
      $txt = "SELECT TO_CHAR(DDATASUBPR,'DD/MM/YYYY') DATA FROM HSSSUBPR 
               WHERE NNUMEPRES = :prestador
                 AND NSUBSPRES = :substituto";
      $sql->addParam(':prestador', $prestador);
      $sql->addParam(':substituto', $substituto);
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");
    }
    
    public function processaGraduacao($espec = 0) {
      /* Inicia rotina para trazer as gradua��es dos prestadores para impress�o */
      $this->lista_graduacoes = array();
      
      $sql_docs = new Query($this->bd);
      $txt_doc = "SELECT DECODE(CDESCPGRAD,'N',0,1),CDESCPGRAD 
                    FROM HSSPGRAD
                   WHERE NNUMEPRES = :prestador";
                     
      if ($espec > 0) {
        $txt_doc .= "   AND NNUMEESPEC = :idEspecialidade ";
        $sql_docs->addParam(":idEspecialidade", $espec);
      }
      
      $txt_doc .= " ORDER BY 1,2 ASC";
      
      $sql_docs->addParam(":prestador", $this->idPrestador);
      $sql_docs->executeQuery($txt_doc);

      $lista = array();
      
      while (!$sql_docs->eof()) {
        $graduacao = new Graduacao();
        $graduacao->sigla = $sql_docs->result("CDESCPGRAD");
        
        switch($graduacao->sigla) {
          case  "E" : $graduacao->imagem = '../comum/img/documentacao/e.png'; break;
          case "AP" : $graduacao->imagem = '../comum/img/documentacao/ap.png'; break;
          case "AD" : $graduacao->imagem = '../comum/img/documentacao/ad.png'; break;
          case "AO" : $graduacao->imagem = '../comum/img/documentacao/ao.png'; break;
          case "AC" : $graduacao->imagem = '../comum/img/documentacao/ac.png'; break;
          case "AI" : $graduacao->imagem = '../comum/img/documentacao/ai.png'; break;
          case  "P" : $graduacao->imagem = '../comum/img/documentacao/p.png'; break;
          case  "R" : $graduacao->imagem = '../comum/img/documentacao/r.png'; break;
          case  "Q" : $graduacao->imagem = '../comum/img/documentacao/q.png'; break;
          case  "N" : $graduacao->imagem = '../comum/img/documentacao/n.png'; break;
        }        

        switch($graduacao->sigla) {
          case  "E" : $graduacao->descricao = 'E - T�tulo de especialista'; break;
          case "AP" : $graduacao->descricao = 'APALC - Padr�o nacional de qualidade'; break;
          case "AD" : $graduacao->descricao = 'ADICQ - Padr�o nacional de qualidade'; break;
          case "AO" : $graduacao->descricao = 'AONA - Padr�o nacional de qualidade'; break;
          case "AC" : $graduacao->descricao = 'ACBA - Padr�o internacional de qualidade'; break;
          case "AI" : $graduacao->descricao = 'AIQG - Padr�o internacional de qualidade'; break;
          case  "P" : $graduacao->descricao = 'P - Profissional com especializa��o'; break;
          case  "R" : $graduacao->descricao = 'R - Profissional com resid�ncia'; break;
          case  "Q" : $graduacao->descricao = 'Q - Qualidade monitorada'; break;
          case  "N" : $graduacao->descricao = 'N - Comunica��o de eventos adversos'; break;              
        }       
        
        $this->lista_graduacoes[] = $graduacao;
        $sql_docs->next();
      }
    }    

    private function processaEndereco($espec = 0) {
      $sql = new Query($this->bd);

      $f_cidade         = "";
      $f_cidade2        = "";
      $f_estado         = "";
      $f_estado2        = "";
      $f_bairro         = "";
      $f_bairro2        = "";
      $f_indicavel      = "";
      $f_indicavel2     = "";
      $f_especialidade  = "";
      $f_especialidade2 = "";
      
      if ($this->cidade <> '') {
        $f_cidade  = "   AND FINPRES.CCIDAPRES = :cidade ";
        $f_cidade2 = "   AND HSSEPRES.CCIDAEPRES = :cidade ";
        $sql->addParam(":cidade", $this->cidade);
      }

      if ($this->estado <> '') {
        $f_estado  = "   AND FINPRES.CESTAPRES = :estado ";
        $f_estado2 = "   AND HSSEPRES.CESTAEPRES = :estado ";
        $sql->addParam(":estado", $this->estado);
      }

      if ($this->bairro <> '') {
        $f_bairro  = "   AND FINPRES.CBAIRPRES = :bairro ";
        $f_bairro2 = "   AND HSSEPRES.CBAIREPRES = :bairro ";
        $sql->addParam(":bairro", $this->bairro);
      }

      if ($this->indicavel <> '') {
        $f_indicavel  = "   AND FINPRES.CINDIPRES = :indicavel ";
        $f_indicavel2 = "   AND HSSEPRES.CINDIEPRES = :indicavel ";
        $sql->addParam(":indicavel", $this->indicavel);
      }

      if ($this->especialidade <> '') {
        $f_especialidade  = "   AND FINPRES.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE WHERE HSSESPRE.NNUMEESPEC = :especialidade AND HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES)";
        $f_especialidade2 = "   AND HSSEPRES.NNUMEEPRES IN (SELECT HSSESPRE.NNUMEEPRES FROM HSSESPRE WHERE HSSESPRE.NNUMEESPEC = :especialidade AND HSSESPRE.NNUMEEPRES = HSSEPRES.NNUMEEPRES)";
        $sql->addParam(":especialidade", $this->especialidade);
      }

      if ($_SESSION['apelido_operadora'] == 'nossasaude') {
        $txt = "SELECT FINPRES.CENDEPRES,FINPRES.CCIDAPRES,FINPRES.CBAIRPRES,FINPRES.CESTAPRES,FINPRES.CFONEPRES,
                       HSSTLOGR.CDESCTLOGR,FINPRES.CNENDPRES,FINPRES.CREFEPRES,FINPRES.CCEP_PRES,FINPRES.CFAX_PRES
                  FROM FINPRES,HSSTLOGR
                 WHERE FINPRES.NNUMEPRES = :prestador " .
               $f_cidade.
               $f_estado.             
               $f_bairro.
               $f_especialidade.
               $f_indicavel.               
               "   AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                              WHERE CPUBLESPRE = 'S'
                                                AND NNUMEEPRES IS NULL
                                                AND HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES)
                   AND FINPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)
                 UNION
                SELECT HSSEPRES.CENDEEPRES,HSSEPRES.CCIDAEPRES,HSSEPRES.CBAIREPRES,HSSEPRES.CESTAEPRES,HSSEPRES.CFONEEPRES,
                       HSSTLOGR.CDESCTLOGR,HSSEPRES.CNENDEPRES,HSSEPRES.CREFEEPRES,HSSEPRES.CCEP_EPRES,HSSEPRES.CFAX_EPRES
                  FROM HSSEPRES,HSSTLOGR
                 WHERE HSSEPRES.NNUMEPRES = :prestador
                   AND HSSEPRES.CSITUEPRES = 'S'
                   AND HSSEPRES.CTIPOEPRES = 'A' " .
               $f_cidade2.
               $f_estado2.
               $f_bairro2.
               $f_especialidade2.
               $f_indicavel2.               
               "   AND (HSSEPRES.NNUMEEPRES,HSSEPRES.NNUMEPRES) IN (SELECT NNUMEEPRES,NNUMEPRES FROM HSSESPRE
                                                                     WHERE CPUBLESPRE = 'S'
                                                                       AND HSSESPRE.NNUMEEPRES = HSSEPRES.NNUMEEPRES
                                                                       AND HSSESPRE.NNUMEPRES = HSSEPRES.NNUMEPRES " .
               "                                                    )
                   AND HSSEPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+) ";
      } 
      else {
        $txt = "SELECT FINPRES.CENDEPRES,FINPRES.CCIDAPRES,FINPRES.CBAIRPRES,FINPRES.CESTAPRES,FINPRES.CFONEPRES,
                       HSSTLOGR.CDESCTLOGR,FINPRES.CNENDPRES,FINPRES.CREFEPRES,FINPRES.CCEP_PRES,FINPRES.CFAX_PRES
                  FROM FINPRES,HSSTLOGR
                 WHERE FINPRES.NNUMEPRES = :prestador " .
               $f_cidade.
               $f_estado.
               $f_bairro.
               $f_especialidade.
               $f_indicavel.
               "   AND FINPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)
                 UNION
                SELECT HSSEPRES.CENDEEPRES,HSSEPRES.CCIDAEPRES,HSSEPRES.CBAIREPRES,HSSEPRES.CESTAEPRES,HSSEPRES.CFONEEPRES,
                       HSSTLOGR.CDESCTLOGR,HSSEPRES.CNENDEPRES,HSSEPRES.CREFEEPRES,HSSEPRES.CCEP_EPRES,HSSEPRES.CFAX_EPRES
                  FROM HSSEPRES,HSSTLOGR
                 WHERE HSSEPRES.NNUMEPRES = :prestador
                   AND HSSEPRES.CSITUEPRES = 'S'
                   AND HSSEPRES.CTIPOEPRES = 'A' " .
               $f_cidade2.
               $f_estado2.
               $f_bairro2.
               $f_especialidade2.
               $f_indicavel2 .
               "   AND HSSEPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+) ";
      }

      $sql->addParam(":prestador", $this->idPrestador);
      $sql->executeQuery($txt);
      
      $lista_endereco = array();
      
      while (!$sql->eof()) {
        $endereco = new Endereco();

        if ($sql->result("CDESCTLOGR") <> '')
          $endereco->endereco  = $sql->result("CDESCTLOGR") . " ";
          
        $endereco->endereco   .= $sql->result('CENDEPRES');
        
        if ($sql->result("CNENDPRES") <> '')
          $endereco->endereco .= ", " . $sql->result("CNENDPRES");

        if ($sql->result("CREFEPRES") <> '')
          $endereco->endereco .= " - " . $sql->result("CREFEPRES");
          
        $endereco->bairro      = $sql->result("CBAIRPRES");
        $endereco->cidade      = $sql->result("CCIDAPRES");
        $endereco->estado      = $sql->result("CESTAPRES");
        $endereco->cep         = $sql->result("CCEP_PRES");
        $endereco->telefone    = $sql->result('CFONEPRES');
        $endereco->fax         = $sql->result('CFAX_EPRES');

        $lista_endereco[] = $endereco;
        $sql->next();
      }

      return $lista_endereco;      
    }
    
    private function processaTelefone() {
      $sql = new Query($this->bd);
      $txt = "SELECT CFONETLPRE FROM HSSTLPRE WHERE NNUMEPRES = :prestador";

      if ($_SESSION['apelido_operadora'] == 'vitallis')
        $txt .= " AND CTIPOTLPRE = 'T'";

      $sql->addParam(":prestador", $this->idPrestador);
      $sql->executeQuery($txt);

      $lista_telefone = array();
      while (!$sql->eof()) {
        $telefone = new Telefone();
        $telefone->numero = $sql->result('CFONETLPRE');
        $telefone->tipo   = $sql->result('CTIPOTLPRE');
        $lista_telefone[] = $telefone;
        $sql->next();
      }

      return $lista_telefone;
    }  

    private function processaEspecialidade() {
      // Especialidades 
      $this->lista_especialidades = array();
      
      $sql = new Query($this->bd);
      $txt = "SELECT HSSESPEC.NNUMEESPEC,HSSESPEC.CNOMEESPEC
                FROM HSSESPRE,HSSESPEC
               WHERE HSSESPRE.NNUMEPRES = :idPrestador
                 AND HSSESPRE.NNUMEESPEC = HSSESPEC.NNUMEESPEC
                 AND HSSESPRE.NNUMEEPRES IS NULL 
                 AND HSSESPRE.CPUBLESPRE = 'S'
               UNION
              SELECT HSSESPEC.NNUMEESPEC,HSSESPEC.CNOMEESPEC
                FROM HSSESPRE,HSSESPEC,HSSEPRES
               WHERE HSSEPRES.NNUMEPRES = :idPrestador
                 AND HSSESPRE.CPUBLESPRE = 'S'
                 AND HSSEPRES.NNUMEEPRES = HSSESPRE.NNUMEEPRES
                 AND HSSESPRE.NNUMEESPEC = HSSESPEC.NNUMEESPEC";
      $sql->addParam(":idPrestador" , $this->idPrestador);                 
      $sql->executeQuery($txt);

      while (!$sql->eof()) {
        $especialidade = new Especialidade();
        $especialidade->nome             = $sql->result("CNOMEESPEC");
        $especialidade->nnumeespec       = $sql->result('NNUMEESPEC');
        
        $sql_cert = new Query($this->bd);
        $txt_cert = "SELECT HSSESPRE.CNRQESSPRE
                       FROM HSSESPRE
                      WHERE HSSESPRE.NNUMEPRES  = :idPrestador
                        AND HSSESPRE.NNUMEESPEC = :idEspecialidade
                        AND HSSESPRE.NNUMEEPRES IS NULL ";
        $sql_cert->addParam(":idPrestador"    , $this->idPrestador);                 
        $sql_cert->addParam(":idEspecialidade", $sql->result('NNUMEESPEC'));                 
        $sql_cert->executeQuery($txt_cert);
      
        $especialidade->num_certificacao = $sql_cert->result('CNRQESSPRE');

        $sql_areas = new Query($this->bd);
        $txt_areas = "SELECT HSSAATUA.CDESCAATUA,HSSAAPRE.CNRQ2AAPRE 
                        FROM HSSAAPRE,HSSAATUA
                       WHERE HSSAAPRE.NNUMEPRES = :prestador
                         AND HSSAAPRE.NNUMEESPEC = :especialidade
                         AND HSSAAPRE.CPUBLAAPRE = 'S'                           
                         AND HSSAAPRE.NNUMEAATUA = HSSAATUA.NNUMEAATUA
                       ORDER BY 1 ";
        $sql_areas->addParam(":prestador"    , $this->idPrestador);
        $sql_areas->addParam(":especialidade", $especialidade->nnumeespec);
        $sql_areas->executeQuery($txt_areas);

        $lista_area = array();
        $i = 0;
        while (!$sql_areas->eof()) {
          $i++;
          $area = new AreaAtuacao();
          $area->nome             = $sql_areas->result("CDESCAATUA");
          $area->num_certificacao = $sql_cert->result('CNRQESSPRE')."-".$i;
          
          $lista_area[]           = $area;
          $sql_areas->next();
        }
        
        $especialidade->lista_areas = $lista_area;

        if (($this->tipo_pessoa == 'J') and ( $_SESSION['apelido_operadora'] <> 'saudemed')) {
        
          $sql_corpo_clinico = new Query($this->bd);
          $txt = "SELECT FINPRES.NNUMEPRES
                    FROM HSSCCLIN,FINPRES
                   WHERE HSSCCLIN.NHOSPPRES = :prestador
                     AND HSSCCLIN.NNUMEESPEC = :especialidade
                     AND HSSCCLIN.CSTATCCLIN = 'A'
                     AND HSSCCLIN.CPUBLCCLIN = 'S'
                     AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   ORDER BY 1";
          $sql_corpo_clinico->addParam(":prestador"    , $this->idPrestador);
          $sql_corpo_clinico->addParam(":especialidade", $especialidade->nnumeespec);

          $sql_corpo_clinico->executeQuery($txt);

          $lista_corpo_clinico = array();
          while (!$sql_corpo_clinico->eof()) {
            $lista_corpo_clinico[] = $sql_corpo_clinico->result('NNUMEPRES');
            $sql_corpo_clinico->next();
          }

          $especialidade->lista_corpo_clinico = $lista_corpo_clinico;
        }
          
        $this->lista_especialidades[] = $especialidade;
        $sql->next();
      }
    }     
  }
  
  class Endereco {
    // Propriedades

    public $cep       = '';
    public $fax       = '';
    public $bairro    = '';
    public $cidade    = '';
    public $estado    = '';
    public $endereco  = '';
    public $telefone  = '';
    public $principal = false;
  }  
  
  class Telefone {
    // Propriedades

    public $tipo   = '';
    public $numero = '';
  }  
  
  class Especialidade {
    // Propriedades

    public $nnumeespec       = 0;
    public $nome             = '';
    public $num_certificacao = ''; 

    // Listas
    public $lista_areas         = array();
    public $lista_corpo_clinico = array();

  }
  
  class AreaAtuacao {
    // Propriedades

    public $nome             = '';
    public $num_certificacao = ''; 
  }  

  class Graduacao {
    // Propriedades 

    public $descricao = '';
    public $sigla     = '';
    public $imagem    = '';

  }
  
  class Substituto {
    // Propriedades 

    public $idPrestador = '';
    public $inicio      = '';
  }  
?>