<?php
  class Tiss 
  {

    function trataErroTiss($message,$codigo) {

      if ($codigo == 76) {
        $message = str_replace('Opening and ending tag mismatch: ','Erro na abertura ou encerramento da tag: ',$message);
        //$message = str_replace('line','linha',$message);
        $message = substr($message,0,strpos($message,'line'));
      } else if ($codigo == 1824) {
        $message = str_replace('Element ','Erro no elemento: ',$message);
        $message = str_replace('{http://www.ans.gov.br/padroes/tiss/schemas}','',$message);
        $message = str_replace('is not a valid value of the atomic type','n�o � um valor v�lido para o tipo',$message);     
        $message = str_replace('\'st_data\'','\'st_data\'. Formato v�lido: "AAAA-MM-DD"',$message);   
        $message = str_replace('\'st_hora\'','\'st_data\'. Formato v�lido: "hh:mm:ss"',$message);   
        $message = str_replace('\'st_dataHora\'','\'st_dataHora\'. Formato v�lido: "AAAA-MM-DDThh:mm:ss"',$message);   
      } else if ($codigo == 1832) {
        $message = str_replace('Element ','Erro no elemento: ',$message);
        $message = str_replace('{http://www.ans.gov.br/padroes/tiss/schemas}','',$message);
        $message = str_replace('[facet \'maxLength\'] The value has a length of ','Quantidade de d�gitos informado: ',$message);     
        $message = str_replace('this exceeds the allowed maximum length of','Quantidade de d�gitos m�ximo: ',$message);   
      } else if ($codigo == 1831) {
        $message = str_replace('Element ','Erro no elemento: ',$message);
        $message = str_replace('{http://www.ans.gov.br/padroes/tiss/schemas}','',$message);
        $message = str_replace('[facet \'minLength\'] The value has a length of ','Quantidade de d�gitos informado: ',$message);     
        $message = str_replace('this underruns the allowed minimum length of','Quantidade de d�gitos m�nimo: ',$message);   
      } else if ($codigo == 1871) {

        if (strpos($message,'Element ') > -1)
          $message = str_replace('Element ','Erro no elemento: ',$message);

        if (strpos($message,'{http://www.ans.gov.br/padroes/tiss/schemas}') > -1)
          $message = str_replace('{http://www.ans.gov.br/padroes/tiss/schemas}','',$message);

        if (strpos($message,'This element is not expected.') > -1) 
          $message = str_replace('This element is not expected.','Este elemento n�o � suportado.',$message);  

        if (strpos($message,': Missing child element(s)') > -1) 
          $message = str_replace(': Missing child element(s)','',$message);       

        if (strpos($message,'Expected is one of') > -1)
          $message = str_replace('Expected is one of','� necess�rio informar um dos seguintes elementos ',$message);
        else if (strpos($message,'Expected is ') > -1)
          $message = str_replace('Expected is ','� necess�rio informar o seguinte elemento ',$message);           
      } else {
        $message = "codigo: ".$codigo." ".$message;
      }
      
      return $message;
    } 
    function adicionaErro ($dom,$codigo,$mensagem,$linha,$guia_prestador='',$guia_operadora='',$nome_beneficiario='') {

      $mensagem = utf8_encode($mensagem);
      
      $erro = $dom->createElement('erro');
      $codigo_erro = $dom->createElement('codigo', $codigo);
      $descricao_erro = $dom->createElement('descricao',$mensagem);
      $linha_erro = $dom->createElement('linha',$linha);
      $guia1_erro = $dom->createElement('guia_prestador',$guia_prestador);
      $guia2_erro = $dom->createElement('guia_operadora',$guia_operadora);
      $nome_beneficiario = $dom->createElement('nome_beneficiario',$nome_beneficiario);

      $erro->appendChild($codigo_erro);
      $erro->appendChild($descricao_erro);
      $erro->appendChild($linha_erro);
      $erro->appendChild($guia1_erro);
      $erro->appendChild($guia2_erro);
      $erro->appendChild($nome_beneficiario);

      return $erro;
    }  
  
    function mensagemTiss ($tipo,$mensagem) {

      echo "  <tr align='center'>";
      echo "    <td style='border:0;'>";
      echo "      <table class='mensagem_".$tipo."'>";
      echo "        <tr>";
      echo "          <td rowspan='3' align='center' width='50'><img src='../img/botoes/".$tipo.".png' border='0'/></td>";
      echo "          <td>".$mensagem."</td>";
      echo "        </tr>";
      echo "      </table>";              
      echo "    </td>";
      echo "  </tr>";
    }    
    
    function validaGuiaTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$data_emissao,
                            $valida,$senha,$atendimento,$datasaidainternacao,$tipoXML,$regime_atendimento) {
      $msg_erros = '';            
      $formata = new Formata();  
      $seg     = new Seguranca();
      $data1   = new Data();
      $func    = new Funcao();
      
      $guia_operadora = $formata->somenteNumeros($guia_operadora);
      $guia_prestador = $formata->somenteCaracteres($guia_prestador,'0123456789-');
      
      if (($guia_operadora == 0) and ($_SESSION['apelido_operadora'] == 'vitallis')) {
        $senha = $formata->somenteNumeros($senha);
        $guia_operadora = $senha;
      }  
      

      if (($guia_operadora <> $formata->somenteNumeros($guia_operadora)) or ($guia_operadora == '')) {
        $msg_erros = 'N�mero de guia inv�lido'.chr(10).
                     'Guia na operadora: '.$guia_operadora.chr(10).
                     'Guia prestador: '.$guia_prestador.chr(10).
                     'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
        $retorno['erro'] = 1;	  
      }
      
      else if ( ($valida == 'X') or 
                ( ($valida == 'N') and ($guia_operadora > 0) ) or 
                ( ($valida == 'U') and ( ($regime_atendimento == 'E') or ($regime_atendimento == '1') ) ) ) {
                
        if ($guia_prestador == -2) { //senha autoriza��o
          $sql = new Query($bd);
          $txt = "SELECT NNUMEGUIA,NNUMEUSUA,CCORTGUIA,CSTATGUIA, NVL(TO_CHAR(TRUNC(DVALIGUIA),'YYYYMMDD'),' ') DVALIGUIA, TO_CHAR(DEMISGUIA,'YYYYMMDD') DATA_EMISSAO,NNUMEPRES,NLOCAPRES
                    FROM HSSGUIA WHERE CSENHGUIA = :senha";
          $sql->addParam(":senha",$senha);
          $sql->executeQuery($txt);        
          $id_guia = $sql->result("NNUMEGUIA");                 
        }else if ((count($guia_operadora) == 0) or (($guia_operadora == 0) and ($guia_prestador == 0))){
          $id_guia = 0;
        }else if(($guia_operadora === 0) and ($guia_prestador > 0)){ 
          $sql = new Query($bd);
          $txt = "SELECT NNUMEGUIA,NNUMEUSUA,CCORTGUIA,CSTATGUIA, NVL(TO_CHAR(TRUNC(DVALIGUIA),'YYYYMMDD'),' ') DVALIGUIA, TO_CHAR(DEMISGUIA,'YYYYMMDD') DATA_EMISSAO,NNUMEPRES,NLOCAPRES
                    FROM HSSGUIA WHERE (NNUMEGUIA = :guia or CSENHGUIA = :guia)
                    ORDER BY 1 DESC";
          $sql->addParam(":guia",$guia_prestador);
          $sql->executeQuery($txt);        
          $id_guia = $sql->result("NNUMEGUIA");      
        }else {
          $sql = new Query($bd);
          $txt = "SELECT NNUMEGUIA,NNUMEUSUA,CCORTGUIA,CSTATGUIA, NVL(TO_CHAR(TRUNC(DVALIGUIA),'YYYYMMDD'),' ') DVALIGUIA, TO_CHAR(DEMISGUIA,'YYYYMMDD') DATA_EMISSAO,NNUMEPRES,NLOCAPRES
                    FROM HSSGUIA WHERE (NNUMEGUIA = :guia or CSENHGUIA = :guia)
                    ORDER BY 1 DESC";
          $sql->addParam(":guia",$guia_operadora);
          $sql->executeQuery($txt);        
          $id_guia = $sql->result("NNUMEGUIA");               
        }
        if ($atendimento > 0)
          $data_atendimento = $data1->formataData($bd,substr($atendimento,0,10),'YYYYMMDD','YYYY-MM-DD');
         
        // campos para a subtra��o entre as datas.
        $dataEmissaSubtr = substr($sql->result("DATA_EMISSAO"),6,2).'/'.substr($sql->result("DATA_EMISSAO"),4,2).'/'.substr($sql->result("DATA_EMISSAO"),0,4);
        $dataAtendiSubtr = substr($data_atendimento,6,2).'/'.substr($data_atendimento,4,2).'/'.substr($data_atendimento,0,4);

        //Franca solicitou a valida��o do direito WEBPRESTADORVERIFICAVALIDADEDAGUIANOENVIODOXML somente quando a autorizacao do prestador for igual a autorizacao da unimed
        if ($id_guia <> 0)
          $data_vencimento_guia = $sql->result("DVALIGUIA");

        if ($data_vencimento_guia == ' ') //Franca solicitou caso a data de vencimento da guia esteja vazio � colocado a mesma data de atendimento.
          $data_vencimento_guia = $data_atendimento;
					  
        if (($datasaidainternacao > 0) and ($tipoXML = 'I'))
          $data_saida_internacao = $data1->formataData($bd,substr($datasaidainternacao,0,10),'YYYYMMDD','YYYY-MM-DD');
                      
        if ($id_guia <= 0) {
        
          if ($guia_prestador == -2) {
            $msg_erros = 'N�mero da senha de autoriza��o inv�lida.'.chr(10).
                         'Senha de autoriza��o: '.$guia_operadora.chr(10);
          } else if ($guia_prestador == -1) {
            $msg_erros = 'N�mero da guia principal inv�lido.'.chr(10).
                         'Guia principal: '.$guia_operadora.chr(10);
          } else {
            $msg_erros = 'N�mero de guia inv�lido.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');
          }
          $retorno['erro'] = 1;        
        } else {
          if ($sql->result("CSTATGUIA") == 'N') {
            $msg_erros = 'Guia negada.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;    
          } else if ($sql->result("CSTATGUIA") == 'D') {
            $msg_erros = 'Guia negada na origem.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1; 
          } else if ($sql->result("CSTATGUIA") == '1') {
            $msg_erros = 'Pedido de exame, n�o pode ser faturado.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;               
          } else if ($sql->result("CSTATGUIA") == 'C') {
            $msg_erros = 'Guia aguardando confirma��o.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;    
          } else if ($sql->result("CSTATGUIA") == 'P') {
            $msg_erros = 'Guia aguardando autoriza��o.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;     
          } else if ($sql->result("CSTATGUIA") == 'A') {
            $msg_erros = 'Guia sob auditoria.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;         
          } else if ($sql->result("CSTATGUIA") == 'X') {
            $msg_erros = 'Guia cancelada.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;
          } else if ( ($sql->result("CSTATGUIA") == 'E') ) {
            $msg_erros = 'Guia cancelada na origem.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Emiss�o: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD');        
            $retorno['erro'] = 1;               
          } else if ($data_atendimento > $data1->dataAtual('YYYYMMDD')) {
             $msg_erros = 'Data de atendimento superior a data do processamento.'.chr(10).
                          'Guia na operadora: '.$guia_operadora.chr(10).
                          'Guia prestador: '.$guia_prestador.chr(10).
                          'Data Atendimento XML: '.$data1->formataData($bd,$data_atendimento,'DD/MM/YYYY','YYYYMMDD');
             $retorno['erro'] = 1;              
             
          //Franca solicitou a valida��o do direito somente quando a autorizacao do prestador for igual a autorizacao da unimed
          } else if ( ($sql->result("DVALIGUIA") <> ' ') and ($data_atendimento > $data_vencimento_guia) and ($seg->permissaoOutros($bd,"WEBPRESTADORVERIFICAVALIDADEDAGUIANOENVIODOXML")) and ($guia_prestador = $guia_operadora) ) {
             $msg_erros = 'Guia com validade vencida.'.chr(10).
                          'Guia na operadora: '.$guia_operadora.chr(10).
                          'Guia prestador: '.$guia_prestador.chr(10).
                          'Data Atendimento XML: '.$data1->formataData($bd,$data_atendimento,'DD/MM/YYYY','YYYYMMDD');
                          'Data vencimento da guia: '.$data_vencimento_guia;
             $retorno['erro'] = 1;    	
          } else if ( ($sql->result("DVALIGUIA") <> ' ') and ($data_atendimento > $data_vencimento_guia) and ($seg->permissaoOutros($bd,"WEBPRESTADORVERIFICAVALIDADEDAGUIANOENVIODOXML")) ) {
             $msg_erros =  'Guia com validade vencida.'.chr(10).
                         'Guia na operadora: '.$guia_operadora.chr(10).
                         'Guia prestador: '.$guia_prestador.chr(10).
                         'Data Atendimento XML: '.$data1->formataData($bd,$data_atendimento,'DD/MM/YYYY','YYYYMMDD');
												 'Data vencimento da guia: '.$data_vencimento_guia;
             $retorno['erro'] = 1;  
                  
          } else if ( ($sql->result("DATA_EMISSAO") > $data_atendimento) and 
                      ($atendimento > 0) and 
                      (!$seg->permissaoOutros($bd,"WEBPRESTADORNAOVALIDARDATAATENDIMENONOENVIODOXML",false)) and
                      ($data1->numeroDias($dataEmissaSubtr,$dataAtendiSubtr) > $func->parametroNumber($bd,"NVADAEGUIA")) 
										){
              $msg_erros = 'Data de atendimento/interna�ao do XML � menor que a data de emiss�o da guia autorizada.'.chr(10).
                           'Guia na operadora: '.$guia_operadora.chr(10).
                           'Guia prestador: '.$guia_prestador.chr(10).
                           'Data Atendimento XML: '.$data1->formataData($bd,$data_atendimento,'DD/MM/YYYY','YYYYMMDD').chr(10).
                           'Data Emiss�o na Operadora: '.$data1->formataData($bd,$sql->result("DATA_EMISSAO"),'DD/MM/YYYY','YYYYMMDD');
             $retorno['erro'] = 1;                   
          } else if (($sql->result("DATA_EMISSAO") > $data1->formataData($bd,$data_emissao,'YYYYMMDD','YYYY-MM-DD')) and ($data_emissao > 0) and (!$seg->permissaoOutros($bd,"WEBPRESTADORNAOVALIDARDATAEMISSAONOENVIODOXML",false))) {
             $msg_erros = 'Data de emiss�o do XML � menor que a data de emiss�o da guia autorizada.'.chr(10).
                          'Guia na operadora: '.$guia_operadora.chr(10).
                          'Guia prestador: '.$guia_prestador.chr(10).
                          'Data Emis�o XML: '.$data1->formataData($bd,$data_emissao,'DD/MM/YYYY','YYYY-MM-DD').chr(10).
                          'Data Emiss�o na Operadora: '.$data1->formataData($bd,$sql->result("DATA_EMISSAO"),'DD/MM/YYYY','YYYYMMDD');
             $retorno['erro'] = 1;
          } else if ( (( ($sql->result("DATA_EMISSAO") > $data_saida_internacao) and ($tipoXML == 'I') ) or (($datasaidainternacao <= 0) and ($tipoXML == 'I'))) and (!$seg->permissaoOutros($bd,"WEBPRESTADORNAOVALIDARDATAEMISSAONOENVIODOXML",false))) 
          {
             $msg_erros = 'Data de saida de interna�ao nao preenchida ou menor que a data de emissao da guia na operadora.'.chr(10).
                          'Guia na operadora: '.$guia_operadora.chr(10).
                          'Guia prestador: '.$guia_prestador.chr(10).
                          'Data de saida de interna��o no XML: '.$data1->formataData($bd,$data_saida_internacao,'DD/MM/YYYY','YYYYMMDD').chr(10).
                          'Data Emiss�o na Operadora: '.$data1->formataData($bd,$sql->result("DATA_EMISSAO"),'DD/MM/YYYY','YYYYMMDD');
             $retorno['erro'] = 1;
          } else {
            $retorno['id_guia'] = $id_guia;
            $retorno['erro'] = 0;
          }
          
          $retorno['id_usuario'] = $sql->result("NNUMEUSUA");        
          $retorno['cortesia'] = $sql->result("CCORTGUIA"); 
        }
      } else {
        $retorno['id_usuario'] = -1;
        $retorno['cortesia'] = '';
        $retorno['erro'] = 0;
        $retorno['id_guia'] = 0;
      }
      
      if ($msg_erros <> '') {
        $root->appendChild($this->adicionaErro($dom,'1307',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));    
      }

      return $retorno;
    }

    function validaGuiaEletiva($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$id_guia) {
      $formata = new Formata();
      
      $msg_erros = '';

      $guia_operadora = $formata->somenteNumeros($guia_operadora);
      $guia_prestador = $formata->somenteCaracteres ($guia_prestador,'0123456789-');
               
      $sql = new Query($bd);
      $txt = "SELECT NNUMEGUIA
                FROM HSSGUIA 
               WHERE NNUMEGUIA = :guia
                 AND CURGEGUIA = 'E'
                 AND CTIPOGUIA = 'A' ";
      $sql->addParam(":guia",$id_guia);
      $sql->executeQuery($txt);        
      
      if ($sql->count() == 0) {
        $msg_erros = 'A guia informada n�o � do tipo ambulatorial/eletiva.'.chr(10).
                     'Para guias de consulta � permitido somente atendimentos de natureza ambulatorial e regime eletivo.'.chr(10).
                     'Guia na operadora: '.$guia_operadora.chr(10).
                     'Guia prestador: '.$guia_prestador;
                     
        $retorno['erro'] = 1;      
      } 
      else 
        $retorno['erro'] = 0;
            
      if ($msg_erros <> '') 
        $root->appendChild($this->adicionaErro($dom,'1307',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));    

      return $retorno;
    }

    function validaBeneficiarioTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,
                                      $plano,$carteira,$identificador,$cns,$id_usuario_guia,$cortesia) {

      $sql = new Query($bd);
      $txt = "SELECT LOCALIZA_USUARIO_TISS(:codigo1,:codigo2,:codigo3) RESULTADO FROM DUAL";
      $sql->addParam(":codigo1",$carteira);
      $sql->addParam(":codigo2",$identificador);
      $sql->addParam(":codigo3",$cns);
      $sql->executeQuery($txt);        
      
	  
      if (($sql->result("RESULTADO") <= 0) and ($cortesia == '')) {
        $msg_erros = 'Identifica��o do benefici�rio n�o consistente'.chr(10).
                     'Nome do benefici�rio: '.$nome_beneficiario.chr(10).
                     'Plano: '.$plano;

        if ($carteira <> '')
          $msg_erros .= chr(10).'N�mero da carteira: '.$carteira;

        if ($identificacao <> '')
          $msg_erros .= chr(10).'Identifica��o do benefici�rio: '.$identificacao;

        if ($cns <> '')
          $msg_erros .= chr(10).'N�mero do CNS: '.$cns;
          
        $root->appendChild($this->adicionaErro($dom,'1011',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));
        $retorno['erro'] = 1;        
      } else {
        if (($id_usuario_guia <> $sql->result("RESULTADO")) and
            ($id_usuario_guia >= 0) and
            ($cortesia == '') and 
            ($_SESSION['valida_beneficiario_guia'] == '1') ) {
            
          $sql2 = new Query($bd);
          $txt = "SELECT CNOMEUSUA FROM HSSUSUA WHERE NNUMEUSUA = :usuario ";
          $sql2->addParam(":usuario",$id_usuario_guia);
          $sql2->executeQuery($txt); 
            
          $msg_erros = 'Benefici�rio da guia diferente do benefici�rio do XML'.chr(10).
                       'Nome do benefici�rio XML: '.$nome_beneficiario.chr(10).
                       'Nome do benefici�rio GUIA: '.$sql2->result("CNOMEUSUA").chr(10).
                       'Plano: '.$plano;

          if ($carteira <> '')
            $msg_erros .= chr(10).'N�mero da carteira: '.$carteira;

          if ($identificacao <> '')
            $msg_erros .= chr(10).'Identifica��o do benefici�rio: '.$identificacao;

          if ($cns <> '')
            $msg_erros .= chr(10).'N�mero do CNS: '.$cns;
            
          $root->appendChild($this->adicionaErro($dom,'1011',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));
        
          $retorno['erro'] = 1;    
        
        } else {
          $retorno['id_usuario'] = $sql->result("RESULTADO");
          $retorno['erro'] = 0;    
        }
      }
      
      return $retorno;
    }

    function validaPrestador($bd,$identificador,$tipo,$operador,$solicitante,$versao='') {
      $sql = new Query($bd);                          
      $txt = "SELECT VALIDA_PRESTADOR_TISS(:identificador,:tipo,:operador,:podecancelado,:versao) RESULTADO FROM DUAL";
      $sql->clear();
      $sql->addParam(":identificador",$identificador);
      $sql->addParam(":tipo",$tipo);
      $sql->addParam(":operador",$operador);    
      $sql->addParam(":versao",$versao);          
      
      if ($solicitante == 'S')
        $sql->addParam(":podecancelado",'S');        
      else
        $sql->addParam(":podecancelado",'N');        
      
      $sql->executeQuery($txt);
      
      return $sql->result("RESULTADO");
    }

    function validaPrestadorTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$nome_prestador,$tipo_prestador,$cpf,$cnpj,$codigo,$cnes,
                                 $conselho = '',$numero_conselho = '',$uf_conselho = '',$versao = '') {
      $formata = new Formata();
      $id_prestador = 0;

      if (($tipo_prestador == 'Solicitante contratado') and ($cnpj == '99999999999999')) {
        $retorno['erro'] = 0;    
        $retorno['id_prestador'] = '';
      }
      else {
         
        if (($tipo_prestador == 'Solicitante contratado') or
            ($tipo_prestador == 'Profissional solicitante'))
          $solicitante = 'S';
        else
          $solicitante = 'N';
        
        if ($cpf <> '') {
          $tipo = "CPF: ";
          $identificacao = $cpf;
          $id_prestador = $this->validaPrestador($bd,$cpf,'CPF',0,$solicitante,$versao);
        } 
        
        if (($cnpj <> '') and ($id_prestador == 0)) {
          $tipo = "CNPJ: ";
          $identificacao = $cnpj;
          $id_prestador = $this->validaPrestador($bd,$cnpj,'CNPJ',0,$solicitante,$versao);
        }

        if (($codigo <> '') and ($id_prestador == 0)) {
          $tipo = "C�digo: ";
          $identificacao = $codigo;
          $id_prestador = $this->validaPrestador($bd,$codigo,'codigoPrestadorNaOperadora',0,$solicitante,$versao);
        } 
        
        if (($id_prestador == 0) and ($conselho <> '')) {
          $tipo = "C�digo do conselho: ";
          
          if ($versao == '3.02')
            $identificacao = $this->conselhoMedicoTISS30($conselho)."-".$this->retornaTissEstado($uf_conselho)." ".$numero_conselho;
          else                         
            $identificacao = $conselho."-".$uf_conselho." ".$numero_conselho;
            
          $id_prestador = $this->validaPrestador($bd,$formata->acrescentaBrancos($uf_conselho,2).$numero_conselho,$conselho,0,$solicitante,$versao);
        }
        
        if (($id_prestador == 0) and ($cnes <> '')) { 
          $tipo = "CNES: ";
          $identificacao = $cnes;
          $id_prestador = $this->validaPrestador($bd,$cnes,'CNES',0,$solicitante,$versao);
        }

        if ($id_prestador <= 0) {
          if (($id_prestador == -1) or ($id_prestador == 0 and $cpf <> '')) {
            $msg_erro = $tipo_prestador." n�o identificado! O CPF informado � inv�lido. (".$cpf.").";
            $codigo_erro = "1206";
          } else if (($id_prestador == -2) or ($id_prestador == 0 and $cnpj <> '')) {
            $msg_erro = $tipo_prestador." n�o identificado! O CNPJ informado � inv�lido. (".$cnpj.").";
            $codigo_erro = "1206";
          } else if ($id_prestador == -3) {
            $msg_erro = "Execu��o ap�s o desligamento do credenciado.";
            $codigo_erro = "1209";
          } else {
            if ($tipo == 'C�digo: ') {
              $msg_erro = $tipo_prestador." n�o identificado! O c�digo informado � inv�lido. (".$codigo.").";
              $codigo_erro = '1203';
            } else if ($tipo == 'CNES: ') {
              $msg_erro = $tipo_prestador." n�o identificado! O c�digo CNES informado � inv�lido. (".$cnes.").";
              $codigo_erro = '1202';
            } else {
            
              if ($versao == '3.02')
                $msg_erro = $tipo_prestador." n�o identificado! O c�digo ".$this->conselhoMedicoTISS30($conselho).
                            " informado � inv�lido. (".$this->conselhoMedicoTISS30($conselho)."-".$this->retornaTissEstado($uf_conselho)." ".$numero_conselho.").";            
              else
                $msg_erro = $tipo_prestador." n�o identificado! O c�digo ".$conselho.
                            " informado � inv�lido. (".$conselho."-".$uf_conselho." ".$numero_conselho.").";
              $codigo_erro = '1203';
            }
          }

          $msg_erros = $msg_erro;
          
          if ($nome_prestador <> '')
            $msg_erros .= chr(10).$tipo_prestador.': '.$nome_prestador;

          $msg_erros .= chr(10).$tipo.$identificacao;

          $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));        
          $retorno['erro'] = 1;      
        } else {
          $retorno['erro'] = 0;    
          $retorno['id_prestador'] = $id_prestador;        
        }
      }

      return $retorno;
    }
    
    function conselhoMedicoTISS30($conselho) {
      $sql = new Query($bd);                          
      $txt = "SELECT CONSELHOMEDICOTISS_3_0(:conselho,'S') RETORNO FROM DUAL";
      $sql->addParam(":conselho",$conselho);
      $sql->executeQuery($txt); 

      return $sql->result("RETORNO");     
    }
    
    function retornaTissEstado($estado) {
      $sql = new Query($bd);                          
      $txt = "SELECT RETORNA_TISS_ESTADO(:estado,'3.02') ESTADO FROM DUAL";
      $sql->addParam(":estado",$estado);
      $sql->executeQuery($txt); 

      return $sql->result("ESTADO");      
    }

    function validaPrestadorGuia($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$id_guia,$id_informado=0,$id_executante=0) {

      if ($id_guia > 0) {

        $sql = new Query($bd);                          
        $txt = "SELECT NNUMEPRES,NLOCAPRES FROM HSSGUIA WHERE NNUMEGUIA = :guia";
        $sql->addParam(":guia",$id_guia);
        $sql->executeQuery($txt);
      
        if ((($_SESSION['valida_prestador_guia'] == 'S') and ($sql->result('NNUMEPRES') <> $_SESSION['id_contratado'])) or
            (($_SESSION['valida_prestador_guia'] == 'L') and ($sql->result('NLOCAPRES') <> $_SESSION['id_contratado'])) or
            (($_SESSION['valida_prestador_guia'] == 'E') and ($sql->result('NNUMEPRES') <> $id_executante)) or
            (($_SESSION['valida_prestador_guia'] == 'C') and ($sql->result('NLOCAPRES') <> $id_informado))) {
                              
          $msg_erros = 'A guia existe, por�m n�o pertence a este prestador e/ou local de atendimento';
                                            
          $root->appendChild($this->adicionaErro($dom,'1316',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));        
          return 1;                   
        } else
          return 0;
          
      } else {   
        return 0;
      }  
    }

    function validaCidTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$codigo,$descricao) {
      $func = new Funcao();
      
      if ((count($codigo) > 0) and ($func->validaCid($bd,$codigo) == '')) {
      
        $msg_erros = 'C�digo CID inv�lido'.chr(10).
                     'C�digo do CID: '.$codigo.chr(10).
                     'Descri��o do CID: '.$descricao;
        $root->appendChild($this->adicionaErro($dom,'1509',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
        return 1;                   
      } else {
        return 0;
      }
    }

    function validaCodigoProcedimentoTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$codigo,$descricao = '',$quantidade,$id_prestador) {
      $seg  = new Seguranca();
      $func = new Funcao();
      
      $msg_erros = '';
      
      $id_pacote = $func->validaPacote($bd,$codigo,$_SESSION['id_contratado'],$_SESSION['id_contratado'],'N');

      
      if ($id_pacote <= 0) {    
        $sql = new Query($bd);
        $txt = "SELECT CCODIPMED 
                  FROM HSSPMED 
                 WHERE CCODIPMED = :codigo
                   AND SUBSTR(CCODIPMED,5,3) <> '000' ";
        $sql->addParam(":codigo",$codigo);
        $sql->executeQuery($txt);
        
        if ($sql->result("CCODIPMED") == ''){
          $msg_erros = 'Procedimento/Pacote n�o existe ou cancelado'.chr(10).
                       'C�digo do procedimento/pacote: '.$codigo;

          if ($descricao <> '')
            $msg_erros .= chr(10).'Descri��o do procedimento/pacote: '.$descricao;
        } 
        else if ( ($seg->permissaoOutros($bd,"WEBPRESTADORVALIDAPROCEDIMENTOSDAGUIA",false)) and ($guia_operadora <> 0) ){
          $sql = new Query($bd);
          $txt = "SELECT CCODIPMED 
                    FROM HSSPGUI 
                   WHERE CCODIPMED = :codigo
                     AND NNUMEGUIA = :guia
                     AND CSTATPGUI IS NULL";
          $sql->addParam(":codigo",$codigo);
          $sql->addParam(":guia",$guia_operadora);
          $sql->executeQuery($txt);

          if (($sql->result("CCODIPMED") == '') and ($_SESSION['apelido_operadora'] <> 'unimedFranca')){
            $msg_erros = 'Procedimento/Pacote n�o liberado na guia'.chr(10).
                         'C�digo do procedimento: '.$codigo;
          
            if ($descricao <> '')
              $msg_erros .= chr(10).'Descri��o do procedimento/pacote: '.$descricao;
          }
        }
      }else if ( ($seg->permissaoOutros($bd,"WEBPRESTADORVALIDAPROCEDIMENTOSDAGUIA",false)) and ($guia_operadora <> 0) ){
        $sql = new Query($bd);
        $txt = "SELECT CCODIPACOT 
                  FROM HSSPCGUI, HSSPACOT 
                 WHERE HSSPACOT.CCODIPACOT = :codigo
                   AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT
                   AND HSSPCGUI.NNUMEGUIA  = :guia 
                   AND HSSPCGUI.CSTATPCGUI IS NULL";
        $sql->addParam(":codigo",$codigo);
        $sql->addParam(":guia",$guia_operadora);
        $sql->executeQuery($txt);

        if (($sql->result("CCODIPACOT") == '') and ($_SESSION['apelido_operadora'] <> 'unimedFranca')){
          $msg_erros = 'Pacote n�o liberado na guia'.chr(10).
                       'C�digo do pacote: '.$codigo;
        
          if ($descricao <> '')
            $msg_erros .= chr(10).'Descri��o do pacote: '.$descricao;
        }
      }  

      if ($quantidade <= 0) {
        $msg_erros = 'Quantidade do procedimento informada zerada.'.chr(10).
                     'C�digo do procedimento: '.$codigo.chr(10).
                     'Guia operadora: '.$guia_operadora;
      }    
          
      if ($msg_erros <> '') {        
        $root->appendChild($this->adicionaErro($dom,'1801',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
        return 1;        
      } else {
        return 0;
      }
    }

    function validaDenteTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$regiao,$dente,$id_prestador) {
      
      if ($regiao == '')
        $codigo = $dente;
      else
        $codigo = $regiao;
        
      $sql = new Query($bd);
      $txt = "SELECT CCODIDENTE 
                FROM HSSDENTE
               WHERE CCODIDENTE = :codigo ";
      $sql->addParam(":codigo",$codigo);
      $sql->executeQuery($txt);
      
      if (($codigo <> '') and ($sql->result("CCODIDENTE") == '')) {
        $msg_erros = 'Dente/regi�o inv�lido.'.chr(10).
                     'C�digo: '.$codigo;
      }
          
      if ($msg_erros <> '') {        
        $root->appendChild($this->adicionaErro($dom,'1801',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
        return 1;        
      } else {
        return 0;
      }
    }

    function validaFaceTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$face,$id_prestador) {
      
      $sql = new Query($bd);
      $txt = "SELECT CCODIFACE 
                FROM HSSFACE
               WHERE CCODIFACE = :codigo ";
      $sql->addParam(":codigo",$face);
      $sql->executeQuery($txt);
      
      if (($face <> '') and ($sql->result("CCODIFACE") == '')) {
        $msg_erros = 'Face inv�lida.'.chr(10).
                     'C�digo: '.$face;
      }
          
      if ($msg_erros <> '') {        
        $root->appendChild($this->adicionaErro($dom,'1801',$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
        return 1;        
      } else {
        return 0;
      }
    }

    function validaTaxasTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,$taxa,$quantidade,$prestador,$descricao,$data,$id_usuario,$codigo_erro) {
      $data1 = new Data();
      $func  = new Funcao();
      
      if ($quantidade <= 0) {
        $msg_erros = 'Quantidade inv�lida ('.$quantidade.')'.chr(10).
                     'C�digo da taxa: '.$taxa;

        if ($descricao <> '')
          $msg_erros .= chr(10).'Descri��o da taxa: '.$descricao;
        
        $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
        return 1; 
      }
      else {    
        $data = $data1->formataData($bd,$data,'DD/MM/YYYY','YYYY-MM-DD') ;
        
        if ($func->validaTaxa($bd,$taxa,$prestador,$data,$id_usuario) == 0) {
          $msg_erros = 'Taxa inv�lida e/ou n�o autorizada.'.chr(10).
                       'C�digo da taxa: '.$taxa;

          if ($descricao <> '')
            $msg_erros .= chr(10).'Descri��o da taxa: '.$descricao;
          
          $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
          return 1;        
        } else {
          return 0;
        }
      }
    }

    function validaMatMedTiss($bd,$dom,$root,$numero_guia,$guia_prestador,$guia_operadora,$nome_beneficiario,
                                $codigo,$quantidade,$prestador,$data,$descricao = '',$codigo_erro,$tabela,
                                $idGuia,$id_usuario,$opcao) {
      $seg = new Seguranca();
      
      $msg_erros = '';
      
      if ($quantidade <= 0) {
      
        if (($codigo_erro == '2001') or ($codigo_erro == '2201')) {
          $msg_erros .= 'Quantidade inv�lida ('.$quantidade.').'.chr(10).
                        'C�digo do medicamento: '.$codigo.chr(10).
                        'Descri��o do medicamento: '.$descricao;
        } else {
          $msg_erros .= 'Quantidade inv�lida ('.$quantidade.').'.chr(10).
                        'C�digo do medicamento: '.$codigo.chr(10).
                        'Descri��o do medicamento: '.$descricao;
        }
              
        $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       

        return 1;         
      }
      else {
      // n�o valida guia de interna��o CSAUIPRES desta forma n�o tras valor no idguia e 
      // para validar matmed o usuario vem da guia.
        if ( ($idGuia == 0) and ($guia_operadora > 0) and ($_SESSION['valida_guia_internacao'] = 'S') ) 
          $idGuia = $guia_operadora;
      
        $sql_usuario = new Query($bd);                              
        $txt_usuario = "SELECT NNUMEUSUA FROM HSSGUIA WHERE NNUMEGUIA  = :guia";                 
        $sql_usuario->addParam(":guia",$idGuia);
        $sql_usuario->executeQuery($txt_usuario);    
          
        $sql = new Query($bd);                              
        $txt = "SELECT VALIDA_MATMED(:codigo,:prestador,NVL(:data,SYSDATE),:tabela,:tipo,:idUsuario,:versaoTiss) RESULTADO FROM DUAL";
        $sql->addParam(":codigo",$codigo);
        $sql->addParam(":prestador",$prestador);
        $sql->addParam(":data",$data);
		    $sql->addParam(":versaoTiss",$_SESSION['versaoTiss']);

        $sql->addParam(":tabela",'');	  
      
        if (($codigo_erro == '2001') or ($codigo_erro == '2201'))
          $sql->addParam(":tipo",'3');
        else
          $sql->addParam(":tipo",'2');
        
        // Caso n�o tenha guia, preciso procurar o usu�rio para validar matmed.
        // Pois tem configura��o que n�o valida guia no prestador.
        if ($sql_usuario->result("NNUMEUSUA") <> 0 )
          $sql->addParam(":idUsuario",$sql_usuario->result("NNUMEUSUA"));
        else
          $sql->addParam(":idUsuario",$id_usuario);
          
        $sql->executeQuery($txt);
        
      // Franca n�o valida se o mat/med se encontra ou n�o na autoriza��o.
        if( $sql->result("RESULTADO") == 0) {

          if (($codigo_erro == '2001') or ($codigo_erro == '2201')) {
            $msg_erros .= 'Material inv�lido e/ou n�o autorizado e/ou n�o pertencente a tabela "'.$tabela.'".'.chr(10).
                          'C�digo do material: '.$codigo.chr(10).
                          'Descri��o do material: '.$descricao;
          } else {
            $msg_erros .= 'Medicamento inv�lido e/ou n�o autorizado e/ou n�o pertencente a tabela "'.$tabela.'".'.chr(10).
                          'C�digo do medicamento: '.$codigo.chr(10).
                          'Descri��o do medicamento: '.$descricao;
          }
          
          $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
          return 1;      
        } else {    
          if ( ($seg->permissaoOutros($bd,"WEBPRESTADORVALIDAMATMEDAUTORIZADONAGUIA",false)) and ($idGuia > 0) ) {
          
            $sql_liberado = new Query($bd);                              
            $txt_liberado = "SELECT NNUMEGUIA FROM HSSMMGUI
                              WHERE NNUMEGUIA  = :guia
                                AND NNUMEPRODU = :produto";
                       
            $sql_liberado->addParam(":guia",$idGuia);
            $sql_liberado->addParam(":produto",$sql->result("RESULTADO"));
            $sql_liberado->executeQuery($txt_liberado);
            
            if ($sql_liberado->result("NNUMEGUIA") == 0) {
              if (($codigo_erro == '2001') or ($codigo_erro == '2201')) {
                $msg_erros .= 'Material n�o autorizado na guia.'.chr(10).
                              'C�digo do material: '.$codigo.chr(10).
                              'Descri��o do material: '.$descricao;
              } else {
                $msg_erros .= 'Medicamento n�o autorizado na guia.'.chr(10).
                              'C�digo do medicamento: '.$codigo.chr(10).
                              'Descri��o do medicamento: '.$descricao;
              }
            
              $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
              return 1;      
            }
            else
              return 0;
          }
          else
            return 0;
        }
      }
    }

    function validaDataTiss($bd,$dom,$root,$data,$local,$guia_prestador,$guia_operadora,$nome_beneficiario) {
      $data1 = new Data();
      $data = $data1->formataData($bd,substr($data,0,10),'DD/MM/YYYY','YYYY-MM-DD') ;
      $novadata = explode("/",$data);
      
      if (($local == 'dataAtendimento') or
          ($local == 'dataHoraAtendimento'))
        $data_str = ' de atendimento ';
      else if (($local == 'dataEmissaoGuia'))
        $data_str = ' de emiss�o da guia ';
      else if (($local == 'dataRealizacaoMedicamento'))
        $data_str = ' de realiza��o do medicamento ';
      else if (($local == 'dataRealizacaoMaterial'))
        $data_str = ' de realiza��o do material ';
      else if (($local == 'dataRealizacaoTaxas'))
        $data_str = ' de realiza��o das di�rias ou taxas ';
      else if (($local == 'dataProcedimento'))
        $data_str = ' de realiza��o do procedimento ';
      else
        $data_str = ' ';    
        
      if (($novadata[2] > $data1->dataAtual('YYYY')) or ($novadata[2] < 1900)) {
          $msg_erros = 'Data'.$data_str.'inv�lida.'.chr(10).
                       'Data informada: '.$data;
       
          $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
          return 1;        
      
      } else {
        $timestamp = mktime(0, 0, 0, $novadata[1], $novadata[0], $novadata[2]);
         
        if ($timestamp > time()) {
         
          $msg_erros = 'Data'.$data_str.'inv�lida.'.chr(10).
                       'Data informada: '.$data;
       
          $root->appendChild($this->adicionaErro($dom,$codigo_erro,$msg_erros,0,$guia_prestador,$guia_operadora,$nome_beneficiario));       
          return 1;        
        } else {
          return 0;
        }  
      }
    }

    function validaGuiasConsulta($bd,$dom,$root,$xml,$tipo_guia,$id_lote) {

      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;
      $seg         = new Seguranca();
         
      foreach ($xml->prestadorParaOperadora->loteGuias->guias->guiaFaturamento->$tipo_guia as $guia) {
        $numero_guia++;

        // Guia
        $retorno = $this->validaGuiaTiss($bd,
                                         $dom,
                                         $root,
                                         $numero_guia,
                                         $guia->identificacaoGuia->numeroGuiaPrestador,
                                         $guia->identificacaoGuia->numeroGuiaOperadora,
                                         $guia->beneficiario->nomeBeneficiario,                                 
                                         $guia->identificacaoGuia->dataEmissaoGuia,
                                         $_SESSION['valida_guia_consulta'],
                                         '',
                                         $guia->dadosAtendimento->dataAtendimento,
                                         '',
                                         'C',
                                         'E');

        if ($retorno['erro'] == 1) {
          $erros += 1;
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia'];
        }else {
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia'];
          $id_guia = $retorno['id_guia'];        
        }
        
        if ($id_guia > 0) {      
          $retorno = $this->validaGuiaEletiva($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->identificacaoGuia->numeroGuiaPrestador,
                                              $guia->identificacaoGuia->numeroGuiaOperadora,
                                              $guia->beneficiario->nomeBeneficiario,                                 
                                              $id_guia);
                             
          if ($retorno['erro'] == 1)
            $erros += 1;                                  
        }
                  
        $erros += $this->validaDataTiss($bd,
                                        $dom,
                                        $root,
                                        $guia->identificacaoGuia->dataEmissaoGuia,
                                        'dataEmissaoGuia',
                                        $guia->identificacaoGuia->numeroGuiaPrestador,
                                        $guia->identificacaoGuia->numeroGuiaOperadora,
                                        $guia->beneficiario->nomeBeneficiario);     
          
        // Benefici�rio
        $retorno = $this->validaBeneficiarioTiss($bd,
                                                 $dom,
                                                 $root,
                                                 $numero_guia,
                                                 $guia->identificacaoGuia->numeroGuiaPrestador,
                                                 $guia->identificacaoGuia->numeroGuiaOperadora,
                                                 $guia->beneficiario->nomeBeneficiario,
                                                 '',
                                                 $guia->beneficiario->numeroCarteira,
                                                 $guia->beneficiario->identificadorBeneficiario,
                                                 $guia->beneficiario->numeroCNS,
                                                 $id_usuario_guia,
                                                 $cortesia);
        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_usuario = $retorno['id_usuario'];      
        
        // Se tiver o direito, n�o ir� validar o executante                            
        if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {                                    
          // Prestador contratado
          $retorno = $this->validaPrestadorTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia,
                                                $guia->identificacaoGuia->numeroGuiaPrestador,
                                                $guia->identificacaoGuia->numeroGuiaOperadora,  
                                                $guia->beneficiario->nomeBeneficiario,                                       
                                                $guia->dadosContratado->nomeContratado,
                                                'Prestador contratado',
                                                $guia->dadosContratado->identificacao->CPF,
                                                $guia->dadosContratado->identificacao->CNPJ,
                                                $guia->dadosContratado->identificacao->codigoPrestadorNaOperadora,
                                                $guia->dadosContratado->numeroCNES);
                                         
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_contratado = $retorno['id_prestador'];                                          
                                     
          // Profissional executante
          $retorno = $this->validaPrestadorTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia, 
                                                $guia->identificacaoGuia->numeroGuiaPrestador,
                                                $guia->identificacaoGuia->numeroGuiaOperadora,
                                                $guia->beneficiario->nomeBeneficiario,                                         
                                                $guia->profissionalExecutante->nomeProfissional,
                                                'Profissional executante','','','','',
                                                $guia->profissionalExecutante->conselhoProfissional->siglaConselho,
                                                $guia->profissionalExecutante->conselhoProfissional->numeroConselho,
                                                $guia->profissionalExecutante->conselhoProfissional->ufConselho);
                                          
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_executante = $retorno['id_prestador']; 
        }                                   
        
        $erros += $this->validaPrestadorGuia($bd,
                                             $dom,
                                             $root,
                                             $numero_guia,                                       
                                             $guia->identificacaoGuia->numeroGuiaPrestador,
                                             $guia->identificacaoGuia->numeroGuiaOperadora,
                                             $guia->beneficiario->nomeBeneficiario,
                                             $id_guia,
                                             $id_contratado,
                                             $id_executante); 
        
        $erros += $this->validaDataTiss($bd,
                                        $dom,
                                        $root,
                                        $guia->dadosAtendimento->dataAtendimento,
                                        'dataAtendimento',
                                        $guia->identificacaoGuia->numeroGuiaPrestador,
                                        $guia->identificacaoGuia->numeroGuiaOperadora,
                                        $guia->beneficiario->nomeBeneficiario);      
          
        // CID principal
        $erros += $this->validaCidTiss($bd,
                                       $dom,
                                       $root,
                                       $numero_guia,
                                       $guia->identificacaoGuia->numeroGuiaPrestador,
                                       $guia->identificacaoGuia->numeroGuiaOperadora, 
                                       $guia->beneficiario->nomeBeneficiario,                                
                                       $guia->hipoteseDiagnostica->CID->codigoDiagnostico,
                                       $guia->hipoteseDiagnostica->CID->descricaoDiagnostico);
                                                   
        // CIDs secund�rios
        if (count($guia->hipoteseDiagnostica->diagnosticoSecundario->CID) > 0) {
          foreach ($guia->hipoteseDiagnostica->diagnosticoSecundario->CID as $cids) {

            $erros += $this->validaCidTiss($bd,
                                           $dom,
                                           $root,
                                           $numero_guia,
                                           $guia->identificacaoGuia->numeroGuiaPrestador,
                                           $guia->identificacaoGuia->numeroGuiaOperadora, 
                                           $guia->beneficiario->nomeBeneficiario,                                    
                                           $cids->codigoDiagnostico,
                                           $cids->descricaoDiagnostico);
                                      
          }
        }
   
        // C�digo do procedimento e quantidade
        $erros += $this->validaCodigoProcedimentoTiss($bd,
                                                      $dom,
                                                      $root,
                                                      $numero_guia,
                                                      $guia->identificacaoGuia->numeroGuiaPrestador,
                                                      $guia->identificacaoGuia->numeroGuiaOperadora, 
                                                      $guia->beneficiario->nomeBeneficiario,                                                 
                                                      $guia->dadosAtendimento->procedimento->codigoProcedimento,
                                                      '',
                                                      1,
                                                      $id_executante);
     
      }       

      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }    
    }



    function validaGuiasSadt($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {

      $data1       = new Data();
      $seg         = new Seguranca();
      
      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;      

      foreach ($xml->prestadorParaOperadora->loteGuias->guias->guiaFaturamento->$tipo_guia as $guia) {
        $numero_guia++;
        
        $total += str_replace(',','.',$guia->valorTotal->totalGeral);      
        
        if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
          if ($guia->identificacaoGuiaSADTSP->numeroGuiaOperadora <> '')
            $guia_principal = $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora;
          else
            $guia_principal = $guia->numeroGuiaPrincipal;          
        } 
        else if ($_SESSION['apelido_operadora'] == 'universal') {
          if ($guia->numeroGuiaPrincipal <> '')
            $guia_principal = $guia->numeroGuiaPrincipal;
          else
            $guia_principal = $guia->dadosAutorizacao->senhaAutorizacao;	  
        } 
        else if ($_SESSION['apelido_operadora'] == 'vitallis') {
          if ($guia->identificacaoGuiaSADTSP->numeroGuiaOperadora <> '')
            $guia_principal = $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora;
          else
            $guia_principal = $guia->numeroGuiaPrincipal;
        }
        else {
          $guia_principal = $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora;
        }
        
        if ($erros <= 50) {
          // Guia
          
          $retorno = $this->validaGuiaTiss($bd,
                                           $dom,
                                           $root,
                                           $numero_guia,
                                           $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                           $guia_principal,
                                           $guia->dadosBeneficiario->nomeBeneficiario,                                   
                                           $guia->identificacaoGuiaSADTSP->dataEmissaoGuia,
                                           $_SESSION['valida_guia_sadt'],
                                           $guia->dadosAutorizacao->senhaAutorizacao,
                                           $guia->dataHoraAtendimento,
                                           '',
                                           'S',
                                           $guia->caraterAtendimento);
                                     
          if ($retorno['erro'] == 1){
            $erros += 1;
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
          }else {
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
            $id_guia = $retorno['id_guia'];
          }                                   

          $erros += $this->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $guia->identificacaoGuiaSADTSP->dataEmissaoGuia,
                                          'dataEmissaoGuia',
                                          $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                          $guia_principal,
                                          $guia->dadosBeneficiario->nomeBeneficiario);  
                                        
          // Senha autorizacao
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDARSENHANOENVIODOXML",false)) {    
          
            if ((count($guia->dadosAutorizacao->senhaAutorizacao) > 0) and ($guia->dadosAutorizacao->senhaAutorizacao <> '') and ($id_guia == '')) {
              $retorno = $this->validaGuiaTiss($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,
                                               '-2',
                                               $guia->dadosAutorizacao->senhaAutorizacao,
                                               $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                               '',
                                               $_SESSION['valida_guia_sadt'],
                                               $guia->dadosAutorizacao->senhaAutorizacao,
                                               '',
                                               '',
                                               'S',
                                               $guia->caraterAtendimento);
              if ($retorno['erro'] == 1){
                $erros += 1;
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }else {
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }                      
            }             
          }

          // Benefici�rio
          $retorno = $this->validaBeneficiarioTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                   $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora,                                          
                                                   $guia->dadosBeneficiario->nomeBeneficiario,
                                                   $guia->dadosBeneficiario->nomePlano,
                                                   $guia->dadosBeneficiario->numeroCarteira,
                                                   $guia->dadosBeneficiario->identificadorBeneficiario,
                                                   $guia->dadosBeneficiario->numeroCNS,
                                                   $id_usuario_guia,
                                                   $cortesia);
                                      
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_usuario = $retorno['id_usuario']; 
          
          // Se tiver o direito, n�o ir� validar o solicitante                            
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDARSOLICITANTENOENVIODOXML",false)) {
            // Solicitante contratado
            $retorno = $this->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora,   
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $guia->dadosSolicitante->contratado->nomeContratado,
                                                  'Solicitante contratado',
                                                  $guia->dadosSolicitante->contratado->identificacao->cpf,
                                                  $guia->dadosSolicitante->contratado->identificacao->CNPJ,
                                                  $guia->dadosSolicitante->contratado->identificacao->codigoPrestadorNaOperadora,
                                                  $guia->dadosSolicitante->contratado->numeroCNES,
                                                  $guia->dadosSolicitante->contratado->identificacao->conselhoProfissional->siglaConselho,
                                                  $guia->dadosSolicitante->contratado->identificacao->conselhoProfissional->numeroConselho,
                                                  $guia->dadosSolicitante->contratado->identificacao->conselhoProfissional->ufConselho);

            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_solicitante = $retorno['id_prestador'];  
            
            // Profissional solicitante
            $retorno = $this->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $guia->dadosSolicitante->profissional->nomeProfissional,
                                                  'Profissional solicitante',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  $guia->dadosSolicitante->profissional->conselhoProfissional->siglaConselho,
                                                  $guia->dadosSolicitante->profissional->conselhoProfissional->numeroConselho,
                                                  $guia->dadosSolicitante->profissional->conselhoProfissional->ufConselho);
                                            
            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_solicitante = $retorno['id_prestador'];                                          
          }                                        
         
          // Se tiver o direito, n�o ir� validar o executante                            
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {  
            // Executante contratado
            $retorno = $this->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                  $guia->prestadorExecutante->nomeContratado,
                                                  'Executante contratado',
                                                  $guia->prestadorExecutante->identificacao->CPF,
                                                  $guia->prestadorExecutante->identificacao->CNPJ,
                                                  $guia->prestadorExecutante->identificacao->codigoPrestadorNaOperadora,
                                                  $guia->prestadorExecutante->numeroCNES);

            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_prestador = $retorno['id_prestador'];          
                        
            // Profissional executante complementar
            if (count($guia->prestadorExecutante->profissionalExecutanteCompl) > 0 ) {
              $retorno = $this->validaPrestadorTiss($bd,
                                                    $dom,
                                                    $root,
                                                    $numero_guia,
                                                    $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                    $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora,  
                                                    $guia->dadosBeneficiario->nomeBeneficiario,                                             
                                                    $guia->prestadorExecutante->profissionalExecutanteCompl->nomeExecutante,
                                                    'Profissional executante complementar',
                                                    $guia->prestadorExecutante->profissionalExecutanteCompl->codigoProfissionalCompl->cpf,
                                                    '',
                                                    $guia->prestadorExecutante->profissionalExecutanteCompl->codigoProfissionalCompl->codigoPrestadorNaOperadora,
                                                    '',
                                                    $guia->prestadorExecutante->profissionalExecutanteCompl->conselhoProfissional->siglaConselho,
                                                    $guia->prestadorExecutante->profissionalExecutanteCompl->conselhoProfissional->numeroConselho,
                                                    $guia->prestadorExecutante->profissionalExecutanteCompl->conselhoProfissional->ufConselho);
                                              
              if ($retorno['erro'] == 1)
                $erros += 1;
              else
                $id_prestador_compl = $retorno['id_prestador'];                                        
            }
          }
          
          $erros += $this->validaPrestadorGuia($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,                                       
                                               $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                               $guia_principal,
                                               $guia->dadosBeneficiario->nomeBeneficiario,
                                               $id_guia,
                                               $id_prestador,
                                               $id_prestador);
            
          if (count($guia->dataHoraAtendimento) > 0) {
            $erros += $this->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->dataHoraAtendimento,
                                            'dataHoraAtendimento',
                                            $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                            $guia_principal,
                                            $guia->dadosBeneficiario->nomeBeneficiario);
          }         
        
          // CID principal
          $erros += $this->validaCidTiss($bd,
                                         $dom,
                                         $root,
                                         $numero_guia,
                                         $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                         $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora, 
                                         $guia->dadosBeneficiario->nomeBeneficiario,                                  
                                         $guia->diagnosticoAtendimento->CID->codigoDiagnostico,
                                         $guia->diagnosticoAtendimento->CID->descricaoDiagnostico);

          if (count($guia->procedimentosRealizados->procedimentos) > 0) {
            // Procedimentos
            foreach ($guia->procedimentosRealizados->procedimentos as $proc) {

              // C�digos dos procedimentos
              $erros += $this->validaCodigoProcedimentoTiss($bd,
                                                            $dom,
                                                            $root,
                                                            $numero_guia,
                                                            $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador, 
                                                            $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora,  
                                                            $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                                            $proc->procedimento->codigo,
                                                            $proc->procedimento->descricao,
                                                            $proc->quantidadeRealizada,
                                                            $id_prestador);
                                                        
              $erros += $this->validaDataTiss($bd,
                                              $dom,
                                              $root,
                                              $proc->data,
                                              'dataProcedimento',
                                              $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                              $guia_principal,
                                              $guia->dadosBeneficiario->nomeBeneficiario);                                                       

              if (count($proc->equipe->membroEquipe) > 0) {
                foreach ($proc->equipe->membroEquipe as $membro) {

                  // Prestador da equipe
                  switch ($membro->posicaoProfissional) {
                    case '00' : $tipo_prestador = 'Cirurgi�o';                 break;
                    case '01' : $tipo_prestador = 'Primeiro auxiliar';         break;
                    case '02' : $tipo_prestador = 'Segundo auxiliar';          break;
                    case '03' : $tipo_prestador = 'Terceiro auxiliar';         break;
                    case '04' : $tipo_prestador = 'Quarto auxiliar';           break;
                    case '05' : $tipo_prestador = 'Instrumentador';            break;
                    case '06' : $tipo_prestador = 'Anestesista';               break;
                    case '07' : $tipo_prestador = 'Auxiliar anestesista';      break;
                    case '08' : $tipo_prestador = 'Consultor';                 break;
                    case '09' : $tipo_prestador = 'Perfusionista';             break;
                    case '10' : $tipo_prestador = 'Pediatra na sala de parto'; break;
                    case '11' : $tipo_prestador = 'Auxiliar SADT';             break;
                    case '12' : $tipo_prestador = 'Clinico';                   break;
                    case '13' : $tipo_prestador = 'Intensivista';              break;
                  }
                    
                  $cpf = $membro->codigoProfissional->cpf;
                  
                  if ($cpf == '')
                    $cpf = $membro->cpf;

                  // Se tiver o direito, n�o ir� validar o executante                            
                  if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {                  
                    $retorno = $this->validaPrestadorTiss($bd,
                                                          $dom,
                                                          $root,
                                                          $numero_guia,
                                                          $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador, 
                                                          $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora,  
                                                          $guia->dadosBeneficiario->nomeBeneficiario,                                                   
                                                          $membro->identificacaoProfissional->nomeExecutante,
                                                          $tipo_prestador,
                                                          $cpf,
                                                          '',
                                                          $membro->codigoProfissional->codigoPrestadorNaOperadora,
                                                          '',
                                                          $membro->identificacaoProfissional->conselhoProfissional->siglaConselho,
                                                          $membro->identificacaoProfissional->conselhoProfissional->numeroConselho,
                                                          $membro->identificacaoProfissional->conselhoProfissional->ufConselho);

                    if ($retorno['erro'] == 1)
                      $erros += 1;
                    else
                      $id_pres_equipe = $retorno['id_prestador'];                                                
                  }
                }
              }
            }
          }

          if (count($guia->outrasDespesas->despesa) > 0) {
            // Despesas
            foreach ($guia->outrasDespesas->despesa as $despesas) {

              if (($despesas->tipoDespesa == '2') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Medicamento
                
                $erros += $this->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->identificadorDespesa->codigo,
                                                  $despesas->quantidade,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->dataRealizacao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->identificadorDespesa->descricao,
                                                  '2101',
                                                  $despesas->identificadorDespesa->tipoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '1');  

                $erros += $this->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->dataRealizacao,
                                                'dataRealizacaoMedicamento',
                                                $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            

              } else if (($despesas->tipoDespesa == '3') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Material
                $erros += $this->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->identificadorDespesa->codigo,
                                                  $despesas->quantidade,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->dataRealizacao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->identificadorDespesa->descricao,
                                                  '2001',
                                                  $despesas->identificadorDespesa->tipoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '2');
                                             
                $erros += $this->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->dataRealizacao,
                                                'dataRealizacaoMaterial',
                                                $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            
                                             
              } else if (($despesas->tipoDespesa <> '2') and
                         ($despesas->tipoDespesa <> '3')) {

                  // Taxas, Diarias
                  $erros += $this->validaTaxasTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                   $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora,
                                                   $guia->dadosBeneficiario->nomeBeneficiario,                                            
                                                   $despesas->identificadorDespesa->codigo,
                                                   $despesas->quantidade,
                                                   $id_contratado,
                                                   $despesas->identificadorDespesa->descricao,
                                                   $despesas->dataRealizacao,
                                                   $id_usuario,
                                                   '2401');
                                              
                  $erros += $this->validaDataTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $despesas->dataRealizacao,
                                                  'dataRealizacaoTaxas',
                                                  $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario);                                             
              }
            }
          }         

          if ((count($guia->OPMUtilizada->OPM->identificacaoOPM) > 0) and ($_SESSION['valida_mat_med'] == 'D')) {
            // OPM
            foreach ($guia->OPMUtilizada->OPM->identificacaoOPM as $opm) {
              $erros += $this->validaMatMedTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia,
                                                $guia->identificacaoGuiaSADTSP->numeroGuiaPrestador,
                                                $guia->identificacaoGuiaSADTSP->numeroGuiaOperadora, 
                                                $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                $opm->OPM->codigo,
                                                $opm->quantidade,
                                                $id_contratado,
                                                $data1->dataAtual('DD/MM/YYYY'),
                                                $opm->OPM->descricao,
                                                '2201',
                                                $opm->OPM->tipoTabela,
                                                $id_guia,
                                                $id_usuario,
                                                '3');
            }
          }
        }
      }
      
      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function validaGuiasInternacao($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {

      $seg   = new Seguranca();
      $data1 = new Data();
      
      $numero_guia  = 0;
      $msg_erros    = '';
      $erros        = 0;
      $id_prestador = 0;

      foreach ($xml->prestadorParaOperadora->loteGuias->guias->guiaFaturamento->$tipo_guia as $guia) {
        $numero_guia++;      

        $total += str_replace(',','.',$guia->valorTotal->totalGeral);      
        
        if ($erros <= 50) {
          // Guia
          if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
            if ($guia->identificacaoGuiaInternacao->numeroGuiaOperadora <> '')
              $guia_principal = $guia->identificacaoGuiaInternacao->numeroGuiaOperadora;
            else
              $guia_principal = $guia->numeroGuiaSolicitacao;            
          } else if ($_SESSION['apelido_operadora'] == 'vitallis') {
              if ($guia->identificacaoGuiaInternacao->numeroGuiaOperadora <> '')
                $guia_principal = $guia->identificacaoGuiaInternacao->numeroGuiaOperadora;
              else
                $guia_principal = $guia->numeroGuiaSolicitacao;            
          } else {
            $guia_principal = $guia->identificacaoGuiaInternacao->numeroGuiaOperadora;
          }      
        
          $retorno = $this->validaGuiaTiss($bd,
                                           $dom,
                                           $root,
                                           $numero_guia,
                                           $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                           $guia_principal,
                                           $guia->dadosBeneficiario->nomeBeneficiario,                                   
                                           $guia->identificacaoGuiaInternacao->dataEmissaoGuia,
                                           $_SESSION['valida_guia_internacao'],
                                           $guia->dadosAutorizacao->senhaAutorizacao,
                                           $guia->dataHoraInternacao,
                                           $guia->dataHoraSaidaInternacao,
                                           'I',
                                           $guia->caraterInternacao);
                                      
          if ($retorno['erro'] == 1){
            $erros += 1;
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
          }else {
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];               
            $id_guia = $retorno['id_guia'];
          }                                     

          $erros += $this->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $guia->identificacaoGuiaInternacao->dataEmissaoGuia,
                                          'dataEmissaoGuia',
                                          $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                          $guia_principal,
                                          $guia->dadosBeneficiario->nomeBeneficiario);         

          // Benefici�rio
          $retorno = $this->validaBeneficiarioTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                   $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,                                              
                                                   $guia->dadosBeneficiario->nomeBeneficiario,
                                                   $guia->dadosBeneficiario->nomePlano,
                                                   $guia->dadosBeneficiario->numeroCarteira,
                                                   $guia->dadosBeneficiario->identificadorBeneficiario,
                                                   $guia->dadosBeneficiario->numeroCNS,
                                                   $id_usuario_guia,
                                                   $cortesia);

          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_usuario = $retorno['id_usuario']; 
            
          // Executante contratado
          $retorno = $this->validaPrestadorTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia, 
                                                $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,  
                                                $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                $guia->identificacaoExecutante->nomeContratado,
                                                'Executante contratado',
                                                $guia->identificacaoExecutante->identificacao->CPF,
                                                $guia->identificacaoExecutante->identificacao->CNPJ,
                                                $guia->identificacaoExecutante->identificacao->codigoPrestadorNaOperadora,
                                                $guia->identificacaoExecutante->numeroCNES);

          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_prestador = $retorno['id_prestador']; 
              
          $erros += $this->validaPrestadorGuia($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,                                       
                                               $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                               $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,  
                                               $guia->dadosBeneficiario->nomeBeneficiario, 
                                               $id_guia,
                                               $id_prestador,
                                               $id_prestador);
              
          $erros += $this->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $guia->dataHoraInternacao,
                                          'dataHoraInternacao',
                                          $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                          $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,
                                          $guia->dadosBeneficiario->nomeBeneficiario); 

            if (count($guia->dataHoraSaidaInternacao) > 0) {
            $erros += $this->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->dataHoraSaidaInternacao,
                                            'dataHoraSaidaInternacao',
                                            $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                            $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,
                                            $guia->dadosBeneficiario->nomeBeneficiario);
          }                                          
                    
          // CID principal
          $erros += $this->validaCidTiss($bd,
                                         $dom,
                                         $root,
                                         $numero_guia,
                                         $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                         $guia->identificacaoGuiaInternacao->numeroGuiaOperadora, 
                                         $guia->dadosBeneficiario->nomeBeneficiario,                                  
                                         $guia->diagnosticosSaidaInternacao->diagnosticoPrincipal->codigoDiagnostico,
                                         $guia->diagnosticosSaidaInternacao->diagnosticoPrincipal->descricaoDiagnostico);

          // CIDs secundario
          if (count($guia->diagnosticosSaidaInternacao->diagnosticosSecundarios->CID) > 0) {
            foreach ($guia->diagnosticosSaidaInternacao->diagnosticosSecundarios->CID as $cids) {

              $erros += $this->validaCidTiss($bd,
                                             $dom,
                                             $root,
                                             $numero_guia,
                                             $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                             $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,
                                             $guia->dadosBeneficiario->nomeBeneficiario,                                      
                                             $cids->codigoDiagnostico,
                                             $cids->descricaoDiagnostico);
            }
          }

          // CIDs por �bito
          if (count($guia->diagnosticosSaidaInternacao->obito) > 0) {
            foreach ($guia->diagnosticosSaidaInternacao->obito as $cids) {

              $erros += $this->validaCidTiss($bd,
                                             $dom,
                                             $root,
                                             $numero_guia,
                                             $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                             $guia->identificacaoGuiaInternacao->numeroGuiaOperadora, 
                                             $guia->dadosBeneficiario->nomeBeneficiario,                                      
                                             $cids->CID->codigoDiagnostico,
                                             $cids->CID->descricaoDiagnostico);
            }
          }

          // Procedimentos
          if (count($guia->procedimentosRealizados->procedimentos) > 0) {      
            foreach ($guia->procedimentosRealizados->procedimentos as $proc) {

              // C�digos dos procedimentos
              $erros += $this->validaCodigoProcedimentoTiss($bd,
                                                            $dom,
                                                            $root,
                                                            $numero_guia,
                                                            $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                            $guia->identificacaoGuiaInternacao->numeroGuiaOperadora, 
                                                            $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                                            $proc->procedimento->codigo,
                                                            $proc->procedimento->descricao,
                                                            $proc->quantidadeRealizada,
                                                            $id_prestador);
                                                        
              $erros += $this->validaDataTiss($bd,
                                              $dom,
                                              $root,
                                              $proc->data,
                                              'dataProcedimento',
                                              $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                              $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,
                                              $guia->dadosBeneficiario->nomeBeneficiario);                                                       

              if (count($proc->equipe->membroEquipe) > 0) {
                foreach ($proc->equipe->membroEquipe as $membro) {

                  // Prestador da equipe
                  switch ($membro->posicaoProfissional) {
                    case '00' : $tipo_prestador = 'Cirurgi�o';                 break;
                    case '01' : $tipo_prestador = 'Primeiro auxiliar';         break;
                    case '02' : $tipo_prestador = 'Segundo auxiliar';          break;
                    case '03' : $tipo_prestador = 'Terceiro auxiliar';         break;
                    case '04' : $tipo_prestador = 'Quarto auxiliar';           break;
                    case '05' : $tipo_prestador = 'Instrumentador';            break;
                    case '06' : $tipo_prestador = 'Anestesista';               break;
                    case '07' : $tipo_prestador = 'Auxiliar anestesista';      break;
                    case '08' : $tipo_prestador = 'Consultor';                 break;
                    case '09' : $tipo_prestador = 'Perfusionista';             break;
                    case '10' : $tipo_prestador = 'Pediatra na sala de parto'; break;
                    case '11' : $tipo_prestador = 'Auxiliar SADT';             break;
                    case '12' : $tipo_prestador = 'Clinico';                   break;
                    case '13' : $tipo_prestador = 'Intensivista';              break;
                  }
                
                  $cpf = $membro->codigoProfissional->cpf;
                  
                  if ($cpf == '')
                    $cpf = $membro->cpf;          

                  // Se tiver o direito, n�o ir� validar o executante                            
                  if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {
                    $retorno = $this->validaPrestadorTiss($bd, 
                                                          $dom,
                                                          $root,
                                                          $numero_guia, 
                                                          $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                          $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,  
                                                          $guia->dadosBeneficiario->nomeBeneficiario,                                                   
                                                          $membro->identificacaoProfissional->nomeExecutante,
                                                          $tipo_prestador,
                                                          $cpf,
                                                          '',
                                                          $membro->codigoProfissional->codigoPrestadorNaOperadora,
                                                          '',
                                                          $membro->identificacaoProfissional->conselhoProfissional->siglaConselho,
                                                          $membro->identificacaoProfissional->conselhoProfissional->numeroConselho,
                                                          $membro->identificacaoProfissional->conselhoProfissional->ufConselho);
                                                     
                    if ($retorno['erro'] == 1)
                      $erros += 1;
                    else
                      $id_prestador = $retorno['id_prestador'];                                                 
                  }
                }
              }
            }
          }
          //OPM
          if ((count($guia->OPMUtilizada->OPM->identificacaoOPM) > 0) and ($_SESSION['valida_mat_med'] == 'D')) {
                foreach ($guia->OPMUtilizadas->OPM->identificacaoOPM as $opm) {
                  $erros += $this->validaMatMedTiss($bd,
                                                    $dom,
                                                    $root,
                                                    $numero_guia,
                                                    $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                    $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,
                                                    $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                    $opm->OPM->codigo,
                                                    $opm->quantidade,
                                                    $id_contratado,
                                                    $data1->dataAtual('DD/MM/YYYY'),
                                                    $opm->OPM->descricao,
                                                    '2201',
                                                    $opm->OPM->tipoTabela,
                                                    $id_guia,
                                                    $id_usuario,
                                                    '4');
                }
              }

          // Despesas
          if (count($guia->outrasDespesas->despesa) > 0) {
            foreach ($guia->outrasDespesas->despesa as $despesas) {

              if (($despesas->tipoDespesa == '2') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Medicamento
                $erros += $this->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                  $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->identificadorDespesa->codigo,
                                                  $despesas->quantidade,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->dataRealizacao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->identificadorDespesa->descricao,
                                                  '2101',
                                                  $despesas->identificadorDespesa->tipoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  $id_usuario,
                                                  '5');
                                             
                $erros += $this->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->dataRealizacao,
                                                'dataRealizacaoMedicamento',
                                                $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            
   
              } else if (($despesas->tipoDespesa == '3') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Material
                $erros += $this->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                  $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,   
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->identificadorDespesa->codigo,
                                                  $despesas->quantidade,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->dataRealizacao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->identificadorDespesa->descricao,
                                                  '2001',
                                                  $despesas->identificadorDespesa->tipoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  $id_usuario,
                                                  '6');
                                             
                $erros += $this->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->dataRealizacao,
                                                'dataRealizacaoMaterial',
                                                $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);
                                          
              } else if (($despesas->tipoDespesa <> '2') and
                         ($despesas->tipoDespesa <> '3')) {

                  // Taxas, Diarias
                  $erros += $this->validaTaxasTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                   $guia->identificacaoGuiaInternacao->numeroGuiaOperadora,                                                                                   
                                                   $guia->dadosBeneficiario->nomeBeneficiario,                                            
                                                   $despesas->identificadorDespesa->codigo,
                                                   $despesas->quantidade,
                                                   $id_contratado,
                                                   $despesas->identificadorDespesa->descricao,
                                                   $despesas->dataRealizacao,
                                                   $id_usuario,
                                                   '2401');
                                              
                  $erros += $this->validaDataTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $despesas->dataRealizacao,
                                                  'dataRealizacaoTaxas',
                                                  $guia->identificacaoGuiaInternacao->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario);                                            
              }
            }
          }
        }
      }

      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function validaGuiasHonorario($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {  

      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;

      foreach ($xml->prestadorParaOperadora->loteGuias->guias->guiaFaturamento->$tipo_guia as $guia) {
        $numero_guia++;      
        
        $total += str_replace(',','.',$guia->procedimentosExamesRealizados->totalGeralHonorario);      
        
        if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
          if ($guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora <> '')
            $guia_principal = $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora;
          else
            $guia_principal = $guia->numeroGuiaPrincipal;      
        }
        else if ($_SESSION['apelido_operadora'] == 'promedmg') {
          if ($guia->numeroGuiaPrincipal <> '')
            $guia_principal = $guia->numeroGuiaPrincipal;
          else
            $guia_principal = $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora;

        } else if ($_SESSION['apelido_operadora'] == 'vitallis') {
            if ($guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora <> '')
              $guia_principal = $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora;
            else
              $guia_principal = $guia->numeroGuiaPrincipal;          
        } else {
          $guia_principal = $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora;
        } 
        
        // Guia
        $retorno = $this->validaGuiaTiss($bd,
                                         $dom,
                                         $root,
                                         $numero_guia,
                                         $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                         $guia_principal,
                                         $guia->dadosBeneficiario->nomeBeneficiario,                                 
                                         $guia->identificacaoGuiaHonorarioIndividual->dataEmissaoGuia,
                                         $_SESSION['valida_guia_internacao'],
                                         '',
                                         $guia->procedimentosExamesRealizados->procedimentoRealizado->data,
                                         '',
                                         'H',
                                         'E');

        if ($retorno['erro'] == 1){
          $erros += 1;
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia'];
        }else {
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia']; 
          $id_guia = $retorno['id_guia'];
        } 
        
        $erros += $this->validaDataTiss($bd,
                                        $dom,
                                        $root,
                                        $guia->identificacaoGuiaHonorarioIndividual->dataEmissaoGuia,
                                        'dataEmissaoGuia',
                                        $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                        $guia_principal,
                                        $guia->dadosBeneficiario->nomeBeneficiario);      
          
        // Guia principal
        /*
        $erros += $this->validaGuiaTiss($dom,
                                   $root,
                                   $conn,
                                   $numero_guia,
                                   '-1',
                                   $guia->numeroGuiaPrincipal,
                                   '',
                                   $_SESSION['valida_guia_internacao']);
        */                                 

        // Benefici�rio
        $retorno = $this->validaBeneficiarioTiss($bd,
                                                 $dom,
                                                 $root,
                                                 $numero_guia,
                                                 $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                                 $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora,
                                                 $guia->dadosBeneficiario->nomeBeneficiario,                                          
                                                 $guia->dadosBeneficiario->nomePlano,
                                                 $guia->dadosBeneficiario->numeroCarteira,
                                                 $guia->dadosBeneficiario->identificadorBeneficiario,
                                                 $guia->dadosBeneficiario->numeroCNS,
                                                 $id_usuario_guia,
                                                 $cortesia);

        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_usuario = $retorno['id_usuario']; 

        // Contratado
        $retorno = $this->validaPrestadorTiss($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                              $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora,
                                              $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                              $guia->contratado->nomeContratado,
                                              'Contratado',
                                              $guia->contratado->identificacao->CPF,
                                              $guia->contratado->identificacao->CNPJ,
                                              $guia->contratado->identificacao->codigoPrestadorNaOperadora,
                                              $guia->contratado->numeroCNES);
                                        
        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_contratado = $retorno['id_prestador'];                                        

        // Contratado executante
        $retorno = $this->validaPrestadorTiss($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                              $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora, 
                                              $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                              $guia->contratadoExecutante->nomeContratado,
                                              'Contratado executante',
                                              $guia->contratadoExecutante->identificacao->CPF,
                                              $guia->contratadoExecutante->identificacao->CNPJ,
                                              $guia->contratadoExecutante->identificacao->codigoPrestadorNaOperadora,
                                              $guia->contratadoExecutante->numeroCNES);
                                        
        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_prestador = $retorno['id_prestador'];  

        $erros += $this->validaPrestadorGuia($bd,
                                             $dom,
                                             $root,
                                             $numero_guia,                                       
                                             $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                             $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora, 
                                             $guia->dadosBeneficiario->nomeBeneficiario,     
                                             $id_guia,
                                             $id_contratado,
                                             $id_prestador);         

        // Procedimentos
        foreach ($guia->procedimentosExamesRealizados->procedimentoRealizado as $proc) {

          // C�digos dos procedimentos
          $erros += $this->validaCodigoProcedimentoTiss($bd,
                                                        $dom,
                                                        $root,
                                                        $numero_guia,
                                                        $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                                        $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora, 
                                                        $guia->dadosBeneficiario->nomeBeneficiario,                                                  
                                                        $proc->procedimento->codigo,
                                                        $proc->procedimento->descricao,
                                                        $proc->quantidadeRealizada,
                                                        $id_prestador);
                                                    
          $erros += $this->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $proc->data,
                                          'dataProcedimento',
                                          $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaPrestador,
                                          $guia->identificacaoGuiaHonorarioIndividual->numeroGuiaOperadora, 
                                          $guia->dadosBeneficiario->nomeBeneficiario);                                                  
        }
      }

      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function validaGuiasOdontologia($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {

      $seg         = new Seguranca();
      $data1       = new Data();
      
      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;

      foreach ($xml->prestadorParaOperadora->loteGuias->guias->guiaFaturamento->$tipo_guia as $guia) {
        $numero_guia++;
             
        if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
          if ($guia->cabecalhoGuia->numeroGuiaPrincipal <> '')
            $guia_principal = $guia->cabecalhoGuia->numeroGuiaPrincipal;
          else
            $guia_principal = $guia->cabecalhoGuia->numeroGuiaOperadora;
        } else {
          $guia_principal = $guia->cabecalhoGuia->numeroGuiaOperadora;
        }
        
        if ($erros <= 50) {
          // Guia
          
          $retorno = $this->validaGuiaTiss($bd,
                                           $dom,
                                           $root,
                                           $numero_guia,
                                           $guia->cabecalhoGuia->numeroGuiaPrestador,
                                           $guia_principal,
                                           $guia->dadosBeneficiario->nomeBeneficiario,                                   
                                           $guia->cabecalhoGuia->dataAutorizacao,
                                           $_SESSION['valida_guia_sadt'],
                                           $guia->cabecalhoGuia->senhaAutorizacao,
                                           $guia->cabecalhoGuia->dataAutorizacao,
                                           '',
                                           'O',
                                           $guia->dadosAtendimento->tipoAtendimento);
                                     
          if ($retorno['erro'] == 1){
            $erros += 1;
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
          }else {
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
            $id_guia = $retorno['id_guia'];
          }                                   

          if (count($guia->cabecalhoGuia->dataEmissaoGuia) > 0) {
            $erros += $this->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->cabecalhoGuia->dataEmissaoGuia,
                                            'dataEmissaoGuia',
                                            $guia->cabecalhoGuia->numeroGuiaPrestador,
                                            $guia_principal,
                                            $guia->dadosBeneficiario->nomeBeneficiario);  
          }
                                        
          // Senha autorizacao
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDARSENHANOENVIODOXML",false)) {    
     
            if ((count($guia->cabecalhoGuia->senhaAutorizacao) > 0) and ($guia->cabecalhoGuia->senhaAutorizacao <> '') and ($id_guia == '')) {
              $retorno = $this->validaGuiaTiss($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,
                                               '-2',
                                               $guia->cabecalhoGuia->senhaAutorizacao,
                                               $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                               '',
                                               $_SESSION['valida_guia_sadt'],
                                               $guia->cabecalhoGuia->senhaAutorizacao,
                                               '',
                                               '',
                                               'O',
                                               $guia->dadosAtendimento->tipoAtendimento);
                                          
              if ($retorno['erro'] == 1){
                $erros += 1;
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }else {
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }                      
            }             
          }

          // Benefici�rio
          $retorno = $this->validaBeneficiarioTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                   $guia->cabecalhoGuia->numeroGuiaOperadora,                                          
                                                   $guia->dadosBeneficiario->nomeBeneficiario,
                                                   $guia->dadosBeneficiario->nomePlano,
                                                   $guia->dadosBeneficiario->numeroCarteira,
                                                   $guia->dadosBeneficiario->identificadorBeneficiario,
                                                   $guia->dadosBeneficiario->numeroCNS,
                                                   $id_usuario_guia,
                                                   $cortesia);
                                      
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_usuario = $retorno['id_usuario']; 
                                                      
          // Se tiver o direito, n�o ir� validar o executante                            
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {  
            $retorno = $this->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia->cabecalhoGuia->numeroGuiaOperadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                  $guia->dadosContratadoExecutante->dadosContratado->nomeContratado,
                                                  'Contratado',
                                                  $guia->dadosContratadoExecutante->dadosContratado->identificacao->CPF,
                                                  $guia->dadosContratadoExecutante->dadosContratado->identificacao->CNPJ,
                                                  $guia->dadosContratadoExecutante->dadosContratado->identificacao->codigoPrestadorNaOperadora,
                                                  $guia->dadosContratadoExecutante->dadosContratado->numeroCNES);

            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_prestador = $retorno['id_prestador'];          
                        
            // executante
            if (count($guia->dadosContratadoExecutante->dadosExecutante) > 0 ) {
              $retorno = $this->validaPrestadorTiss($bd,
                                                    $dom,
                                                    $root,
                                                    $numero_guia,
                                                    $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                    $guia->cabecalhoGuia->numeroGuiaOperadora,  
                                                    $guia->dadosBeneficiario->nomeBeneficiario,                                             
                                                    $guia->dadosContratadoExecutante->dadosExecutante->nomeExecutante,
                                                    'Executante',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    $guia->dadosContratadoExecutante->dadosExecutante->conselhoProfissional->siglaConselho,
                                                    $guia->dadosContratadoExecutante->dadosExecutante->conselhoProfissional->numeroConselho,
                                                    $guia->dadosContratadoExecutante->dadosExecutante->conselhoProfissional->ufConselho);
                                                   
              if ($retorno['erro'] == 1)
                $erros += 1;
              else
                $id_prestador_compl = $retorno['id_prestador'];                                        
            }
        
            $erros += $this->validaPrestadorGuia($bd,
                                                 $dom,
                                                 $root,
                                                 $numero_guia,                                       
                                                 $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                 $guia_principal,
                                                 $guia->dadosBeneficiario->nomeBeneficiario,
                                                 $id_guia,
                                                 $id_prestador,
                                                 $id_prestador);
        
          }
      
          if (count($guia->cabecalhoGuia->dataAutorizacao) > 0) {
            $erros += $this->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->cabecalhoGuia->dataAutorizacao,
                                            'dataAutorizacao',
                                            $guia->cabecalhoGuia->numeroGuiaPrestador,
                                            $guia_principal,
                                            $guia->dadosBeneficiario->nomeBeneficiario);
          }         
        
          if (count($guia->procedimentosExecutados->procedimentoExecutado) > 0) {
            // Procedimentos
            foreach ($guia->procedimentosExecutados->procedimentoExecutado as $proc) {

              // C�digos dos procedimentos
              $erros += $this->validaCodigoProcedimentoTiss($bd,
                                                            $dom,
                                                            $root,
                                                            $numero_guia,
                                                            $guia->cabecalhoGuia->numeroGuiaPrestador, 
                                                            $guia->cabecalhoGuia->numeroGuiaOperadora,  
                                                            $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                                            $proc->procedimentoOdonto->codigo,
                                                            $proc->procedimentoOdonto->descricao,
                                                            $proc->quantidadeRealizada,
                                                            $id_prestador);

              $erros += $this->validaDenteTiss($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,
                                               $guia->cabecalhoGuia->numeroGuiaPrestador, 
                                               $guia->cabecalhoGuia->numeroGuiaOperadora,  
                                               $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                               $proc->denteRegiao->regiao,
                                               $proc->denteRegiao->dente,
                                               $id_prestador);

              $erros += $this->validaFaceTiss($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->cabecalhoGuia->numeroGuiaPrestador, 
                                              $guia->cabecalhoGuia->numeroGuiaOperadora,  
                                              $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                              $proc->face,
                                              $id_prestador);                                                      
                                                        
              $erros += $this->validaDataTiss($bd,
                                              $dom,
                                              $root,
                                              $proc->dataRealizacao,
                                              'dataRealizacao',
                                              $guia->cabecalhoGuia->numeroGuiaPrestador,
                                              $guia_principal,
                                              $guia->dadosBeneficiario->nomeBeneficiario);                                                       
            }
          }

          if (count($guia->outrasDespesas->despesa) > 0) {
            // Despesas
            foreach ($guia->outrasDespesas->despesa as $despesas) {

              if (($despesas->tipoDespesa == '2') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Medicamento
                
                $erros += $this->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->identificadorDespesa->codigo,
                                                  $despesas->quantidade,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->dataRealizacao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->identificadorDespesa->descricao,
                                                  '2101',
                                                  $despesas->identificadorDespesa->tipoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '7');  

                $erros += $this->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->dataRealizacao,
                                                'dataRealizacaoMedicamento',
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            

              } else if (($despesas->tipoDespesa == '3') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Material
                $erros += $this->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia->cabecalhoGuia->numeroGuiaOperadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->identificadorDespesa->codigo,
                                                  $despesas->quantidade,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->dataRealizacao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->identificadorDespesa->descricao,
                                                  '2001',
                                                  $despesas->identificadorDespesa->tipoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '8');
                                             
                $erros += $this->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->dataRealizacao,
                                                'dataRealizacaoMaterial',
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            
                                             
              } else if (($despesas->tipoDespesa <> '2') and
                         ($despesas->tipoDespesa <> '3')) {

                  // Taxas, Diarias
                  $erros += $this->validaTaxasTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                   $guia->cabecalhoGuia->numeroGuiaOperadora,
                                                   $guia->dadosBeneficiario->nomeBeneficiario,                                            
                                                   $despesas->identificadorDespesa->codigo,
                                                   $despesas->quantidade,
                                                   $id_contratado,
                                                   $despesas->identificadorDespesa->descricao,
                                                   $despesas->dataRealizacao,
                                                   $id_usuario,
                                                   '2401');
                                              
                  $erros += $this->validaDataTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $despesas->dataRealizacao,
                                                  'dataRealizacaoTaxas',
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario);                                             
              }
            }
          }         
        }
      }
      
      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function insereLoteTiss($bd,$prestador,$transacao,$sequencial,$data,$hora,$lote,$arquivo,$tipo_guia,
                            $identificador_prestador,$tipo_identificador,$versao) {

      $sql = new Query($bd);
      $txt = "BEGIN ".
             "  INSERE_LOTE_TISS(:prestador,:transacao,:sequencial,TO_DATE(:data,'YYYY-MM-DD'),:hora,:lote,:arquivo,:forma,:id_mens,:tipo_guia,:identificador_prestador,:tipo_identificador,:versao_padrao); ".
             "END; ";
      $sql->addParam(":prestador",$prestador);
      $sql->addParam(":transacao",$transacao);
      $sql->addParam(":sequencial",$sequencial);
      $sql->addParam(":data",$data);
      $sql->addParam(":hora",$hora);
      $sql->addParam(":lote",$lote);
      $sql->addParam(":arquivo",$arquivo);
      $sql->addParam(":identificador_prestador",$identificador_prestador);
      $sql->addParam(":tipo_identificador",$tipo_identificador);   
      $sql->addParam(":versao_padrao",$versao);    

      $forma = 'W';
      $sql->addParam(":forma",$forma);
      $sql->addParam(":id_mens",0,12);
      $sql->addParam(":tipo_guia",$tipo_guia);  
      $sql->executeSQL($txt);

      return $sql->getReturn(":id_mens");
    }
    
    function removeTags ($msg,$ns = 'ansTISS') {

      $msg = str_replace(chr(13),'',$msg);
      $msg = str_replace(chr(10),'',$msg);
      $msg = str_replace(chr(9),'',$msg);    
      $msg = trim($msg);
      
      if (strpos($msg,'<'.$ns.':epilogo>') > 0) {
        $msg = trim(substr($msg,0,strpos($msg,'<'.$ns.':epilogo>')-1).substr($msg,strpos($msg,'</'.$ns.':epilogo>') + 14,strlen($msg)));
      }
      
      $msg = substr($msg,0,strpos($msg,'<')).substr($msg,strpos($msg,'>')+1,strlen($msg));
      
      $entrar = strpos($msg,'<'.$ns.':');
          
      $count = 0;
      while (($entrar !== false) and ($count < 100000)) {
      
        $inicio = strpos($msg,'<'.$ns.':');
        $fim = strpos($msg,'>',$inicio);
        $tag = substr($msg,$inicio+1,$fim - $inicio -1);   
        
        $tag_ini = '<'.$tag.'>'; 
        $tag_fim = '</'.$tag.'>';
        
        if (strpos($msg,$tag_ini) !== false)
          $msg = trim(str_replace($tag_ini,'',$msg));
          
        if (strpos($msg,$tag_fim,0) !== false)
          $msg = trim(str_replace($tag_fim,'',$msg));
          
        $entrar = strpos($msg,'<'.$ns.':');
        $count++;
      }
          
      $msg = str_replace('</'.$ns.':mensagemTISS>','',$msg);
      return $msg;

    }     
  }
?>