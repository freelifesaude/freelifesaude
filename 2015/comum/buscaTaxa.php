<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
     
  $desc = "";      
  
  if (isset($_POST['codigo'])) {
        
    if ($_POST['codigo'] <> "") {
      $sql = new Query($bd);
      $txt = "SELECT NNUMEUSUA FROM HSSUSUA WHERE CCODIUSUA = :usuario";
      $sql->clear();
      $sql->addParam(":usuario",$_POST['usuario']);
      $sql->executeQuery($txt);      
      $id_usuario = $sql->result("NNUMEUSUA");      
      
      $sql->clear();
      $txt = "SELECT VALIDA_TAXA(:codigo,:prestador,SYSDATE,:usuario) RESULTADO FROM DUAL";
      $sql->clear();
      $sql->addParam(":codigo",$_POST['codigo']);
      $sql->addParam(":prestador",$_SESSION['id_contratado']);
      $sql->addParam(":usuario",$id_usuario);  
      $sql->executeQuery($txt);
      $id_taxa = $sql->result("RESULTADO");
                      
      if ($id_taxa > 0) {
        $sql->clear();
        $txt = "SELECT CDESCTAXA FROM HSSTAXA WHERE NNUMETAXA = :id";
        $sql->clear();
        $sql->addParam(":id",$id_taxa);
        $sql->executeQuery($txt);      
      
        $desc = $sql->result("CDESCTAXA");        
      }        
      
      if ($desc == '')
        $desc = "Taxa n�o localizada!";    

      $json['nome']           = utf8_encode($desc);
    }
  }
  
  echo json_encode($json);
  
  $bd->close();
  
?>