<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");

  class PDF extends PDFSolus {
    
    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',9);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(150,5,'RELAT�RIO DE UTILIZA��O DE LIMITES',0,1);
      $this->SetFont('Arial','B',7);
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(205,3,$_SESSION['titular_contrato']." - ".$_SESSION['cnpj_contrato'],0,1);
      
      if ($_SESSION['id_locacao'] > 0) {
        $this->Cell(40,3,'');      
        $this->Cell(25,3,"Loca��o:",0,0,'R');
        $this->Cell(205,3,$_SESSION['nome_locacao'],0,1);    
      }    
      
      $this->Ln(7);
      $this->Cell(190,1,' ','B',0);
      $this->Ln(2);

      $this->SetFont('Arial','B',8);
      $this->Ln(1);
      $this->SetFont('Arial','B',8);
      $this->Cell(25,3,'C�digo',0,0);
      $this->Cell(70,3,'Benefici�rio',0,0);
      $this->Cell(70,3,'Titular',0,0);
      $this->Cell(25,3,'Situa��o',0,1);

      $this->Cell(45,3,'',0,0);
      $this->Cell(80,3,'Descri��o',0,0);
      $this->Cell(20,3,'Data de in�cio',0,0);
      $this->Cell(15,3,'Limite',0,0);
      $this->Cell(15,3,'Consumo',0,0);
      $this->Cell(15,3,'Restante',0,1);
      $this->Ln(1);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('P','mm','A4');
  $pdf->AliasNbPages();
  
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  $id_contrato = $_SESSION['id_contrato'];
  $locacao     = $_SESSION['id_locacao'];
  $codigo      = $_POST['codigo'];
  
  if ($locacao > 0)
    $filtro_locacao = "   AND TITU.NNUMESETOR = :locacao";
  else
    $filtro_locacao = "";
  
  $txt = "SELECT USUA.CCODIUSUA, USUA.CNOMEUSUA, TITU.CNOMEUSUA TITULAR, USUA.NNUMEUSUA,CNOMELIMI,
                 NVL(NQUANLIMI,NVALOLIMI) LIMITE,NNUMELIMI,INICIO_CONTAGEM_LIMITES(USUA.NNUMEUSUA,NNUMELIMI) INICIO,
                 DECODE(USUA.CSITUUSUA,'A','Ativo','Cancelado') SITUACAO,
                 TOTAL_CONSUMO (NNUMELIMI,USUA.NNUMEUSUA,INICIO_CONTAGEM_LIMITES(USUA.NNUMEUSUA,NNUMELIMI),SYSDATE) CONSUMO 
            FROM HSSUSUA USUA, HSSUSUA TITU, LIMITES_USUARIO LIMITE 
           WHERE USUA.NNUMETITU = :contrato 
             AND USUA.CCODIUSUA = :codigo ".
         $filtro_locacao.
         "   AND USUA.NTITUUSUA = TITU.NNUMEUSUA 
             AND USUA.NNUMEUSUA = LIMITE.NNUMEUSUA 
           ORDER BY 3,2,5 ";

  $sql = new Query($bd);
  $sql->addParam(":contrato",$id_contrato);
  $sql->addParam(":codigo",$codigo);
  
  if ($locacao > 0)
    $sql->addParam(":locacao",$locacao);
    
  $sql->executeQuery($txt);
  
  $usuario = "";

  while (!$sql->eof()) {
    if ($usuario <> $sql->result("CNOMEUSUA")) {
      $pdf->Cell(25,3,$sql->result("CCODIUSUA"),0,0);    
      $pdf->Cell(70,3,$pdf->Copy($sql->result("CNOMEUSUA"),69),0,0);    
      $pdf->Cell(70,3,$pdf->Copy($sql->result("TITULAR"),69),0,0);  
      $pdf->Cell(25,3,$sql->result("SITUACAO"),0,1);  
      $usuario = $sql->result("CNOMEUSUA");
    }

    $pdf->Cell(45,3,'',0,0);
    $pdf->Cell(80,3,$sql->result("CNOMELIMI"),0,0);
    $pdf->Cell(20,3,$data->formataData($bd,$sql->result("INICIO"),'DD/MM/YYYY'),0,0,'C');
    $pdf->Cell(15,3,$formata->formataNumero($sql->result("LIMITE"),0),0,0,'R');
    $pdf->Cell(15,3,$formata->formataNumero($sql->result("CONSUMO"),0),0,0,'R');

    $restante = $sql->result("LIMITE") - $sql->result("CONSUMO");

    if ($restante < 0) 
      $restante = 0;

    $pdf->Cell(15,3,$formata->formataNumero($restante,0),0,1,'R');

    $sql->next();

    if ($usuario <> $sql->result("CNOMEUSUA"))
      $pdf->Ln(3);
  }

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  
  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 


?>