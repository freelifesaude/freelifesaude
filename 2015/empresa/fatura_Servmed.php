<?php             
  if ($sql4->result("CDESCTLOGR") <> '')
    $endereco = $sql4->result("CDESCTLOGR")." ";
  else
    $endereco = '';
    
  $endereco .= $sql4->result("CENDETITU");
  
  if ($sql4->result("CNUMETITU") <> '')
    $endereco .= ", ".$sql4->result("CNUMETITU");
  
  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Empresa:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(170,5,$sql4->result("TITULAR"),0,1);

  $pdf->SetFont('Arial','B',8); 
  $pdf->Cell(20,5,'CNPJ:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(95,5,$formata->formataCNPJ($sql4->result("C_CGCEMPR")),0,0);
  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Contrato:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(55,5,$sql4->result("CCODITITU"),0,1);

  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Endereço:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(95,5,$pdf->Copy($endereco,94),0,0);
  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Emissão:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(55,5,$sql4->result("DGERAPAGA"),0,1);
  
  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Município:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(95,5,$sql4->result("CCIDATITU"),0,0);
  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Vencimento:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(55,5,$sql4->result("DVENCPAGA"),0,1);

  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'UF:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(95,5,$sql4->result("CESTATITU"),0,0);
  $pdf->SetFont('Arial','B',8);    
  $pdf->Cell(20,5,'Fatura:',0,0,'R');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(55,5,$sql4->result("NDOCUPAGA"),0,1);
      
  $txt = "SELECT CNOMESETOR, 1 ORDEM, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 'MENSALIDADE' TIPO, NVL(NVLIQUSUPG,0)-NVL(NPRORUSUPG,0) VALOR, 0 NINSSPACON, USUARIO.NNUMEUSUA ID, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSUSUPG, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
             AND HSSUSUPG.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 1 ORDEM, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 'ADESAO' TIPO, NADESUSUPG VALOR, 0 NINSSPACON, USUARIO.NNUMEUSUA ID, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSUSUPG, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
             AND NADESUSUPG <> 0
             AND HSSUSUPG.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 2 ORDEM, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 'PRO-RATA MENSALIDADE' TIPO, NPRORUSUPG VALOR, 0 NINSSPACON, USUARIO.NNUMEUSUA ID, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSUSUPG, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
             AND HSSUSUPG.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND NVL(NPRORUSUPG,0) > 0 
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 3, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 CDESCTXMEN, NVALOADTPG - NVL(NPRORADTPG,0) NVALOADTPG, 0 NINSSPACON, HSSTXMEN.NNUMETXMEN ID, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSADTPG, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSTXMEN, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSADTPG.NNUMEPAGA
             AND HSSADTPG.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND HSSADTPG.NNUMETXMEN = HSSTXMEN.NNUMETXMEN
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 4, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 'PRO-RATA TAXAS', NPRORADTPG, 0 NINSSPACON, HSSTXMEN.NNUMETXMEN ID, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSADTPG, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSTXMEN, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSADTPG.NNUMEPAGA
             AND HSSADTPG.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND HSSADTPG.NNUMETXMEN = HSSTXMEN.NNUMETXMEN
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 5, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO,USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 CNOMEPRES, NVALOPACON, NINSSPACON, HSSPACON.NNUMECONT, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,TIPOCONTAAGEMED(HSSPACON.NNUMECONT) TIPO_CONTA
            FROM HSSPAGA, HSSPACON, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSCONT, FINPRES, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
             AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
             AND HSSCONT.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 6, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 CNOMEPRES, NVALOGPAGA, 0 NINSSPACON, HSSGPAGA.NNUMEGUIA, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSGPAGA, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSGUIA, FINPRES, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSGPAGA.NNUMEPAGA
             AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
             AND HSSGUIA.NUTILUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 7, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 CNOMEPRES, NVALOAPAGA, 0 NINSSPACON, HSSAPAGA.NNUMEAGEND, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSAPAGA, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSAGEND, FINPRES, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSAPAGA.NNUMEPAGA
             AND HSSAPAGA.NNUMEAGEND = HSSAGEND.NNUMEAGEND
             AND HSSAGEND.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND HSSAGEND.NNUMEPRES = FINPRES.NNUMEPRES
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CNOMESETOR, 8, TITULAR.CCARTUSUA, TITULAR.CNOMEUSUA NOME_TITULAR, USUARIO.NTITUUSUA, USUARIO.NNUMEUSUA, HSSSETOR.NNUMESETOR, USUARIO.CNOMEUSUA NOME_USUARIO, USUARIO.CTIPOUSUA,HSSPLAN.CCODIPLAN,
                 'CARTEIRINHAS', NVALOCPAGA, 0 NINSSPACON, HSSCARTE.NNUMERCART, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSCPAGA, HSSUSUA TITULAR, HSSUSUA USUARIO, HSSCARTE, HSSSETOR, HSSTITU, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSCPAGA.NNUMEPAGA
             AND HSSCPAGA.NNUMECARTE = HSSCARTE.NNUMECARTE
             AND HSSCARTE.NNUMEUSUA = USUARIO.NNUMEUSUA
             AND USUARIO.NTITUUSUA = TITULAR.NNUMEUSUA
             AND USUARIO.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND USUARIO.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND TITULAR.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CRAZAEMPR, 9, CCODITITU, CRAZAEMPR NOME_TITULAR, 0, 0, 0, CRAZAEMPR NOME_USUARIO, 'T',HSSPLAN.CCODIPLAN,
                 CMOTIADPAG, NVALOADPAG, 0 NINSSPACON, HSSADPAG.NNUMEADPR, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSADPAG, HSSTITU, HSSEMPR, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSADPAG.NNUMEPAGA
             AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION ALL
          SELECT CRAZAEMPR, 10, CCODITITU, CRAZAEMPR NOME_TITULAR, 0, 0, 0, CRAZAEMPR NOME_USUARIO, 'T',HSSPLAN.CCODIPLAN,
                 'FATURAMENTO MINIMO', NVALOMINFA, 0 NINSSPACON, HSSMINFA.NNUMEPLAN, CCODIGRFAT, NVENCPAGA, HSSTITU.NNUMETITU, NDOCUPAGA,'Saude' TIPO_CONTA
            FROM HSSPAGA, HSSMINFA, HSSTITU, HSSEMPR, HSSGRFAT,HSSPLAN
           WHERE HSSPAGA.NNUMEPAGA = :Id_Pagamento
             AND HSSPAGA.NNUMEPAGA = HSSMINFA.NNUMEPAGA
             AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
             AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)
             AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";
             
  if ($sql4->result("CLFATTITU") == "¨") 
    $txt .= " ORDER BY 4, 10, 8, 2 ";
  else
    $txt .= " ORDER BY 4, 8, 2 ";
    
  $sql = new Query();
  $sql->addParam(":id_pagamento",$id_pagamento);
  $sql->executeQuery($txt);
  
  $total_mensalidade           = 0;
  $total_geral                 = 0;
  $total_conta                 = 0;
  $total_prorata_mensalidade   = 0;
  $total_aditivo               = 0;
  $total_prorata_aditivo       = 0;
  $total_falta                 = 0;
  $total_carteirinhas          = 0;
  $total_acrescimos_descontos  = 0;
  $total_faturamento_minimo    = 0;
  $total_inss                  = 0;
  
  while (!$sql->eof()) {
         
    if ($sql->result("ORDEM") == 5) {
    
      $total_conta += str_replace(',','.',$sql->result("VALOR"));   
      
      $inss = $sql->result("NINSSPACON");

      if ($inss > 0)
        $total_inss += str_replace(',','.',$inss);   
    }
    
    if ($sql->result("ORDEM") == 1)
      $total_mensalidade += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 2) 
      $total_prorata_mensalidade += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 3) 
      $total_aditivo += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 4) 
      $total_prorata_aditivo += str_replace(',','.',$sql->result("VALOR"));   
    // 05 que é conta estou somando no detalhe do loop acima
    else if ($sql->result("ORDEM") == 6) 
      $total_conta += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 7) 
      $total_falta += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 8) 
      $total_carteirinhas += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 9) 
      $total_acrescimos_descontos += str_replace(',','.',$sql->result("VALOR"));   
    else if ($sql->result("ORDEM") == 10) 
      $total_faturamento_minimo += str_replace(',','.',$sql->result("VALOR"));        

    $total_geral += str_replace(',','.',$sql->result("VALOR"));        
      
    $sql->next();
  }
  
  $sql->first();

  $pdf->Ln(6);
  $pdf->SetFont('Arial','B',8);        
  $pdf->Cell(20,5,'',0,0);    
  $pdf->Cell(60,5,'DESCRIÇÃO DOS SERVIÇOS',0,0);
  $pdf->Cell(20,5,'VALOR TOTAL',0,1);

  $pdf->SetFont('Arial','',8);        
  $pdf->Cell(20,5,'',0,0);    
  $pdf->Cell(60,5,'MENSALIDADES',0,0);
  $pdf->Cell(20,5,$formata->formataNumero($total_mensalidade),0,1,'R');

  if ($total_prorata_mensalidade <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'PRO-RATA MENSALIDADE',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_prorata_mensalidade),0,1,'R');
  }

  if ($total_prorata_aditivo <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'PRO-RATA TAXAS',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_prorata_aditivo),0,1,'R');
  }

  if ($total_aditivo <> 0) {
    $sql_aditivos = new Query();
    $txt_aditivos = "SELECT CDESCTXMEN,SUM(NVALOADTPG - NVL(NPRORADTPG,0)) NVALOADTPG
                       FROM HSSADTPG,HSSTXMEN
                      WHERE HSSADTPG.NNUMEPAGA = :id_pagamento
                        AND HSSADTPG.NNUMETXMEN = HSSTXMEN.NNUMETXMEN
                      GROUP BY CDESCTXMEN
                      ORDER BY CDESCTXMEN ";
    $sql_aditivos->addParam(":id_pagamento",$id_pagamento);
    $sql_aditivos->executeQuery($txt_aditivos);
    
    while (!$sql_aditivos->eof()) {
      $pdf->Cell(20,5,'',0,0);    
      $pdf->Cell(60,5,'TAXA '.$sql_aditivos->result("CDESCTXMEN"),0,0);
      $pdf->Cell(20,5,$formata->formataNumero($sql_aditivos->result("NVALOADTPG")),0,1,'R');
    
      $sql_aditivos->next();
    }      
  }

  if ($total_conta <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'DESPESA SAUDE',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_conta),0,1,'R');
  }

  if ($total_inss <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'INSS PRESTADOR',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_inss),0,1,'R');
  }

  if ($total_carteirinhas <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'CARTEIRINHAS',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_carteirinhas),0,1,'R');
  }

  if ($total_falta <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'FALTAS COBRADAS',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_falta),0,1,'R');
  }
  
  if ($total_acrescimos_descontos <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'ACRESCIMO/DESCONTO',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_acrescimos_descontos),0,1,'R');
  }
  
  if ($total_faturamento_minimo <> 0) {    
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'FATURAMENTO MINIMO',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_faturamento_minimo),0,1,'R');
  }
     
  $sqln = new Query();
  $txt =  "SELECT NIR__NOTAF, NISS_NOTAF FROM HSSCNOTA,HSSNOTAF
            WHERE HSSCNOTA.NNUMEPAGA = :id_pagamento
              AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF
              AND CSITUNOTAF = 'A' ";
  $sqln->addParam(":id_pagamento" ,$id_pagamento);
  $sqln->executeQuery($txt);
  
  If ($sqln->result("NIR__NOTAF") <> 0) {
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'IMPOSTO DE RENDA',0,0);
    $pdf->Cell(20,3,$formata->formataNumero($sqln->result("NIR__NOTAF")),'',1,'R');
  }

  If ($sqln->result("NISS_NOTAF") <> 0) {
    $pdf->Cell(20,5,'',0,0);    
    $pdf->Cell(60,5,'ISS',0,0);
    $pdf->Cell(20,3,$formata->formataNumero($sqln->result("NISS_NOTAF")),'',1,'R');
  }

  $pdf->Cell(20,5,'',0,0);    
  $pdf->Cell(60,5,'TOTAL GERAL',0,0);
  $pdf->Cell(20,5,$formata->formataNumero($sql4->result("NVENCPAGA")),0,1,'R');
  
  $pdf->AddPage();
        
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(65,5,'Usuário'    ,'B',0);
  $pdf->Cell(15,5,'Item'       ,'B',0);
  $pdf->Cell(65,5,'Descrição'  ,'B',0);
  $pdf->Cell(15,5,'Vlr Benef'  ,'B',0);
  $pdf->Cell(15,5,'Vlr Empresa','B',0);
  $pdf->Cell(15,5,'Total'      ,'B',1,'R');
  $pdf->Ln(3);
  
  $Total_Titular = 0;

  $Titular = -1;
  $Id_Setor = -1;

  $Total_Setor_Tit = 0;
  $Total_Setor_Dep = 0;
  $Total_Geral_Tit = 0;
  $Total_Geral_Dep = 0;

  $Total_Mensalidade = 0;
  $Total_Aditivo = 0;
  $Total_Conta = 0;
  $Total_Guia = 0;
  $Total_Falta = 0;
  $Total_Carteirinhas = 0;
  $Total_Acrescimo_Desconto = 0;
  $Total_Fat_Minimo = 0;
  $Total_Inss = 0;
  $Total_Pro_Rata_Men = 0;
  $Total_Pro_Rata_Adt = 0;

  $Num_Titulares = 0;
  $Num_Dependentes = 0;

  $Num_Titulares_Tot = 0;
  $Num_Dependentes_Tot = 0;
  
  $sql_num = new Query();
  $txt_num = "SELECT COUNT(DISTINCT FE.NNUMESETOR) QTDE
                FROM FATURA_EMPRESA FE,HSSPAGA
               WHERE FE.NNUMETITU      = :Contrato
                 AND CNOMESETOR IS NOT NULL
                 AND FE.NNUMETITU      = HSSPAGA.NNUMETITU
                 AND HSSPAGA.DVENCPAGA = :Vencimento ";
  $sql_num->addParam(":contrato",$sql4->result("NNUMETITU"));
  $sql_num->addParam(":vencimento",$sql4->result("DVENCPAGA"));
  $sql_num->executeQuery($txt_num);
  $setores = $sql_num->result("QTDE");
          
  $i = 1; 
  
  while (!$sql->eof()) {
/*      
    if (($Id_Setor <> $sql->result("NNUMESETOR")) and ($setores > 1)) {                
      $Id_Setor = $sql->result("NNUMESETOR");

      $Total_Setor_Tit = 0;
      $Total_Setor_Dep = 0;
      $Num_Titulares = 0;
      $Num_Dependentes = 0;

      $pdf->SetFont('Arial','B',8);
      $pdf->Ln(3);
      $pdf->Cell(190,3,"Locação: ".$sql->result("CNOMESETOR"),0,1);
      $pdf->Ln(6);
    }
*/      
    $Vl_Valor_Usuario = $sql->result("VALOR");

    $pdf->SetFont('Arial','',8);      
    If ($sql->result("NTITUUSUA") <> $Titular) {        
    
      if ($i==1)
        $pdf->SetFillColor(244,244,244);
      else
        $pdf->SetFillColor(255,255,255);
      
      $i = $i * (-1);
    
      $pdf->Cell(40,3,$sql->result("CCARTUSUA"),'',0,'L',true);
      $pdf->Cell(60,3,'Titular: '.$sql->result("NOME_TITULAR"),'',0,'L',true);
      $pdf->Cell(15,3,'','',0,'L',true);
      $pdf->Cell(60,3,'','',0,'L',true);
      $pdf->Cell(15,3,'','',1,'R',true);        
      
      $Total_VlBeneficiario = 0;
      $Total_ValorEmpresa = 0;
      $Imprimiu_Cabecalho = false;
    }
    
    $usuario = $sql->result("NOME_USUARIO");
    $conta = $sql->result("ID");
    
    $pdf->Cell(05,3,''                          ,'',0,'L',true);
    $pdf->Cell(60,3,$sql->result("NOME_USUARIO"),'',0,'L',true);
    $pdf->Cell(15,3,$sql->result("ID")          ,'',0,'L',true);
    $pdf->Cell(50,3,$sql->result("TIPO")        ,'',0,'L',true);
    $pdf->Cell(15,3,''                          ,'',0,'R',true);        

    If ($sql->result("ORDEM") <> 5) {
      $pdf->Cell(15,3,'0.00'                           ,'',0,'R',true);
      $pdf->Cell(15,3,$formata->formataNumero($Vl_Valor_Usuario),'',0,'R',true);
      $pdf->Cell(15,3,$formata->formataNumero($Vl_Valor_Usuario),'',1,'R',true);                  
      
      $Total_VlBeneficiario = (str_replace(',','.',$Total_VlBeneficiario) + str_replace(',','.',0));
      $Total_ValorEmpresa   = (str_replace(',','.',$Total_ValorEmpresa) + str_replace(',','.',$Vl_Valor_Usuario));
    }
    else If ($sql->result("ORDEM") == 5) {
      $pdf->Cell(45,3,'','',1,'L',true);
      
      $sql1 = new Query();
      $txt1 = "SELECT TIPODACONTATIGRE(HSSPACON.NNUMECONT,CAMBUCONT,ITENS.CODIGO_ITEM) TIPO,
                     CCODIPMED, CNOMEPMED, NQUANPCON, NFRANPCON, DATENCONT, CAMBUCONT, CURGECONT, CTIPOPMED, NNUMETAXA
                FROM HSSPAGA,HSSPACON,HSSCONT, (
                                                SELECT NQUANPCON, NFRANPCON,NNUMECONT,HSSPMED.CCODIPMED,CNOMEPMED, HSSPMED.CCODIPMED CODIGO_ITEM, CTIPOPMED, 0 NNUMETAXA FROM HSSPCON, HSSPMED
                                                 WHERE HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                                                 UNION ALL
                                                SELECT NQUANTCON, NFRANTCON,NNUMECONT,CCODITAXA,CDESCTAXA, TO_CHAR(NULL), TO_CHAR(NULL), HSSTCON.NNUMETAXA FROM HSSTCON,HSSTAXA
                                                  WHERE HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                                                 UNION ALL
                                                SELECT 1, NFRMMCONT,NNUMECONT,TO_CHAR(NULL),'MAT/MED', TO_CHAR(NULL), TO_CHAR(NULL), 0 FROM HSSCONT
                                                 WHERE NFRMMCONT <> 0) ITENS
               WHERE HSSPACON.NNUMECONT = :id_conta
                 AND HSSPACON.NNUMEPAGA = :Id_Pagamento
                 AND HSSPACON.NNUMEPAGA = HSSPAGA.NNUMEPAGA
                 AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                 AND HSSCONT.NNUMECONT = ITENS.NNUMECONT
               ORDER BY 1 ";        
      $sql1->addParam(":id_conta",$sql->result("ID"));
      $sql1->addParam(":Id_Pagamento",$id_pagamento);
      $sql1->executeQuery($txt1);
     
      If ($Imprimiu_Cabecalho == false) {
        $Imprimiu_Cabecalho = true;
        $pdf->Cell(05,3,''           ,'',0,'L',true);
        $pdf->Cell(60,3,''           ,'',0,'L',true);
        $pdf->Cell(05,3,''           ,'',0,'L',true);
        $pdf->Cell(15,3,'Codigo'     ,'',0,'L',true);
        $pdf->Cell(15,3,'Data'       ,'',0,'L',true);
        $pdf->Cell(35,3,'Descrição'  ,'',0,'L',true);
        $pdf->Cell(10,3,'Qtde'       ,'',0,'L',true);
        $pdf->Cell(15,3,'Vlr Benef'  ,'',0,'L',true);
        $pdf->Cell(15,3,'Vlr Empresa','',0,'L',true);
        $pdf->Cell(15,3,'Total'      ,'',1,'R',true);                  
      }
      While (!$sql1->eof()) {

        If ($sql->result("CCODIGRFAT") <> '') { // Tigre
        
          $txt1 = "BEGIN ".
                  "  DEFINERATEIO(:Grupo, :Codigo, :Valor, :Percentual,:ValorEmpresa, :VlBeneficiario, :PercentualFuncionario, :Tipo); ".
                  "END;";
                   
          $Percentual = 0;
          $ValorEmpresa = 0;
          $VlBeneficiario = 0;
          $PercentualFuncionario = 0;
          $Tipo = '';
          
          $sql2 = new Query();
          $sql2->addParam(":Grupo",$sql->result("CCODIGRFAT"));
          $sql2->addParam(":Codigo",$sql1->result("TIPO"));
          $sql2->addParam(":Valor",$sql1->result("NFRANPCON"));              
          $sql2->addParam(":Percentual",0,12);
          $sql2->addParam(":ValorEmpresa",0,12);
          $sql2->addParam(":VlBeneficiario",0,12);
          $sql2->addParam(":PercentualFuncionario",0,12);
          $sql2->addParam(":Tipo",$Tipo,20);             
          $sql2->executeSQL($txt1);
          
          $Percentual = $sql2->getReturn(":Percentual");
          $ValorEmpresa = $sql2->getReturn(":ValorEmpresa");
          $VlBeneficiario = $sql2->getReturn(":VlBeneficiario");
          $PercentualFuncionario = $sql2->getReturn(":PercentualFuncionario");            
                       
          $VlBeneficiario = round($VlBeneficiario,2);
          $ValorEmpresa = str_replace(',','.',$sql1->result("NFRANPCON")) - str_replace(',','.',$VlBeneficiario);

        }
        else {
          $txt1 = "SELECT PERCENTUALBENEFICIARIO(:Contrato, :Categoria, :Natureza, :Regime, :Tipo, :Taxa) PERC
                     FROM DUAL ";
          $sql2 = new Query();
          $sql2->addParam(":Contrato" ,$sql->result("NNUMETITU"));
          $sql2->addParam(":Categoria",$sql->result("CTIPOUSUA"));
          $sql2->addParam(":Natureza" ,$sql1->result("CAMBUCONT"));
          $sql2->addParam(":Regime"   ,$sql1->result("CURGECONT"));
          $sql2->addParam(":Tipo"     ,$sql1->result("CTIPOPMED"));
          $sql2->addParam(":Taxa"     ,$sql1->result("NNUMETAXA"));
          $sql2->executeQuery($txt1);
          
          $PercentualFuncionario = $sql2->result("PERC");
          
          if (str_replace(',','.',$PercentualFuncionario) > 0) {
            $VlBeneficiario = (str_replace(',','.',$sql1->result("NFRANPCON")) * str_replace(',','.',$PercentualFuncionario));
            $ValorEmpresa = (str_replace(',','.',$sql1->result("NFRANPCON")) - str_replace(',','.',$VlBeneficiario));
          }
          else {
            $VlBeneficiario = 0;
            $ValorEmpresa = $sql1->result("NFRANPCON");
          }
        }
        
        $pdf->Cell(05,3,''                                        ,'',0,'L',true);
        $pdf->Cell(60,3,''                                        ,'',0,'L',true);
        $pdf->Cell(05,3,''                                        ,'',0,'L',true);
        $pdf->Cell(15,3,$sql1->result("CCODIPMED")                ,'',0,'L',true);
        $pdf->Cell(15,3,$sql1->result("DATENCONT")                ,'',0,'L',true);
        $pdf->Cell(35,3,substr($sql1->result("CNOMEPMED"),0,20)   ,'',0,'L',true);
        $pdf->Cell(10,3,$formata->formataNumero($sql1->result("NQUANPCON")),'',0,'R',true);
        $pdf->Cell(15,3,$formata->formataNumero($VlBeneficiario)           ,'',0,'R',true);
        $pdf->Cell(15,3,$formata->formataNumero($ValorEmpresa)             ,'',0,'R',true);
        $pdf->Cell(15,3,$formata->formataNumero($sql1->result("NFRANPCON")),'',1,'R',true);                  
        
        $Total_Conta = (str_replace(',','.',$Total_Conta) + str_replace(',','.',$sql1->result("NFRANPCON")));

        $Total_VlBeneficiario = (str_replace(',','.',$Total_VlBeneficiario) + str_replace(',','.',$VlBeneficiario));
        $Total_ValorEmpresa = (str_replace(',','.',$Total_ValorEmpresa) + str_replace(',','.',$ValorEmpresa));
        
        $sql1->next();
      }

      $Inss = $sql->result("NINSSPACON");

      if ($Inss > 0) {
        $pdf->Cell(05,3,''           ,'',0,'L',true);
        $pdf->Cell(60,3,''           ,'',0,'L',true);
        $pdf->Cell(05,3,''           ,'',0,'L',true);
        $pdf->Cell(15,3,'','',0,'L',true);
        $pdf->Cell(15,3,'','',0,'L',true);
        $pdf->Cell(35,3,'INSS PRESTADOR','',0,'L',true);
        $pdf->Cell(10,3,'','',0,'L',true);          
        $pdf->Cell(15,3,$formata->formataNumero(round(str_replace(',','.',$PercentualFuncionario)/100 * str_replace(',','.',$Inss),2)),'',0,'R',true);
        $pdf->Cell(15,3,$formata->formataNumero(str_replace(',','.',$Inss) - round(str_replace(',','.',$PercentualFuncionario)/100 * str_replace(',','.',$Inss),2)),'',0,'R',true);
        $pdf->Cell(15,3,$formata->formataNumero($Inss),'',1,'R',true);                  
        
        $Total_VlBeneficiario = (str_replace(',','.',$Total_VlBeneficiario) + round(str_replace(',','.',$PercentualFuncionario)/100 * str_replace(',','.',$Inss),2));
        $Total_ValorEmpresa = (str_replace(',','.',$Total_ValorEmpresa) + str_replace(',','.',$Inss) - round(str_replace(',','.',$PercentualFuncionario)/100 * str_replace(',','.',$Inss),2));

        $Total_Inss = str_replace(',','.',$Total_Inss) + str_replace(',','.',$Inss);
      }

    } 

    if ($sql->result("NTITUUSUA") == $sql->result("NNUMEUSUA")) {
      $Total_Setor_Tit = str_replace(',','.',$Total_Setor_Tit) + str_replace(',','.',$Vl_Valor_Usuario);
      $Total_Geral_Tit = str_replace(',','.',$Total_Geral_Tit) + str_replace(',','.',$Vl_Valor_Usuario);
      if ($sql->result("ORDEM") == 0)
        $Num_Titulares = $Num_Titulares + 1;
    } 
    else {
      $Total_Setor_Dep = str_replace(',','.',$Total_Setor_Dep) + str_replace(',','.',$Vl_Valor_Usuario);
      $Total_Geral_Dep = str_replace(',','.',$Total_Geral_Dep) + str_replace(',','.',$Vl_Valor_Usuario);
      if ($sql->result("ORDEM") == 0)
        $Num_Dependentes = $Num_Dependentes + 1;
    }

    If (($sql->result("ORDEM") == 0) or ($sql->result("ORDEM") == 1))
      $Total_Mensalidade = str_replace(',','.',$Total_Mensalidade) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 2)
      $Total_Pro_Rata_Men = str_replace(',','.',$Total_Pro_Rata_Men) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 3)
      $Total_Aditivo = str_replace(',','.',$Total_Aditivo)  + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 4)
      $Total_Pro_Rata_Adt = str_replace(',','.',$Total_Pro_Rata_Adt) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 6)
      $Total_Guia = str_replace(',','.',$Total_Guia) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 7)
      $Total_Falta = str_replace(',','.',$Total_Falta) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 8)
      $Total_Carteirinhas = str_replace(',','.',$Total_Carteirinhas) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 9)
      $Total_Acrescimo_Desconto = str_replace(',','.',$Total_Acrescimo_Desconto) + str_replace(',','.',$Vl_Valor_Usuario);
    else If ($sql->result("ORDEM") == 10)
      $Total_Fat_Minimo = str_replace(',','.',$Total_Fat_Minimo) + str_replace(',','.',$Vl_Valor_Usuario);


    $Total_Titular = str_replace(',','.',$Total_Titular) + str_replace(',','.',$Vl_Valor_Usuario);
    $Titular = $sql->result("NTITUUSUA");
    $pdf->Cell(190,3,''                                  ,'',1,'L',true);
    $sql->next();
     
    if (($sql->result("NOME_USUARIO") <> $usuario) or ($sql->eof()))
      $pdf->Cell(190,3,''                                  ,'',1,'L',true);
    
    if (($sql->result("NTITUUSUA") <> $Titular) or
        ($sql->eof())) {
      $pdf->Cell(190,3,''                                  ,'',1,'L',true);
      $pdf->Cell(05,3,''                                   ,'',0,'L',true);
      $pdf->Cell(60,3,''                                   ,'',0,'L',true);
      $pdf->Cell(05,3,''                                   ,'',0,'L',true);
      $pdf->Cell(15,3,''                                   ,'',0,'L',true);
      $pdf->Cell(15,3,''                                   ,'',0,'L',true);
      $pdf->Cell(45,3,'Total da familia:'                  ,'',0,'L',true);
      $pdf->Cell(15,3,$formata->formataNumero($Total_VlBeneficiario),'',0,'R',true);
      $pdf->Cell(15,3,$formata->formataNumero($Total_ValorEmpresa)  ,'',0,'R',true);
      $pdf->Cell(15,3,$formata->formataNumero($Total_Titular)       ,'',1,'R',true);                  
      $pdf->Cell(190,3,''                                  ,'',1,'L',true);
        
      $Total_Titular = 0;
    }
     
  }
  
  $pdf->SetFont('Arial','',8);        
  $pdf->Cell(10,5,'',0,0);    
  $pdf->Cell(60,5,'MENSALIDADES',0,0);
  $pdf->Cell(20,5,$formata->formataNumero($total_mensalidade),0,1,'R');

  if ($total_prorata_mensalidade <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'PRO-RATA MENSALIDADE',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_prorata_mensalidade),0,1,'R');
  }

  if ($total_prorata_aditivo <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'PRO-RATA TAXAS',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_prorata_aditivo),0,1,'R');
  }

  if ($total_aditivo <> 0) {
    $sql_aditivos = new Query();
    $txt_aditivos = "SELECT CDESCTXMEN,SUM(NVALOADTPG - NVL(NPRORADTPG,0)) NVALOADTPG
                       FROM HSSADTPG,HSSTXMEN
                      WHERE HSSADTPG.NNUMEPAGA = :id_pagamento
                        AND HSSADTPG.NNUMETXMEN = HSSTXMEN.NNUMETXMEN
                      GROUP BY CDESCTXMEN
                      ORDER BY CDESCTXMEN ";
    $sql_aditivos->addParam(":id_pagamento",$id_pagamento);
    $sql_aditivos->executeQuery($txt_aditivos);
    
    while (!$sql_aditivos->eof()) {
      $pdf->Cell(10,5,'',0,0);    
      $pdf->Cell(60,5,'TAXA '.$sql_aditivos->result("CDESCTXMEN"),0,0);
      $pdf->Cell(20,5,$formata->formataNumero($sql_aditivos->result("NVALOADTPG")),0,1,'R');
    
      $sql_aditivos->next();
    }      
  }

  if ($total_conta <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'DESPESA SAUDE',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_conta),0,1,'R');
  }

  if ($total_inss <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'INSS PRESTADOR',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_inss),0,1,'R');
  }

  if ($total_carteirinhas <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'CARTEIRINHAS',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_carteirinhas),0,1,'R');
  }

  if ($total_falta <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'FALTAS COBRADAS',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_falta),0,1,'R');
  }
  
  if ($total_acrescimos_descontos <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'ACRESCIMO/DESCONTO',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_acrescimos_descontos),0,1,'R');
  }
  
  if ($total_faturamento_minimo <> 0) {    
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'FATURAMENTO MINIMO',0,0);
    $pdf->Cell(20,5,$formata->formataNumero($total_faturamento_minimo),0,1,'R');
  }
     
  $sqln = new Query();
  $txt =  "SELECT NIR__NOTAF, NISS_NOTAF FROM HSSCNOTA,HSSNOTAF
            WHERE HSSCNOTA.NNUMEPAGA = :id_pagamento
              AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF
              AND CSITUNOTAF = 'A' ";
  $sqln->addParam(":id_pagamento" ,$id_pagamento);
  $sqln->executeQuery($txt);
  
  If ($sqln->result("NIR__NOTAF") <> 0) {
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'IMPOSTO DE RENDA',0,0);
    $pdf->Cell(20,3,$formata->formataNumero($sqln->result("NIR__NOTAF")),'',1,'R');
  }

  If ($sqln->result("NISS_NOTAF") <> 0) {
    $pdf->Cell(10,5,'',0,0);    
    $pdf->Cell(60,5,'ISS',0,0);
    $pdf->Cell(20,3,$formata->formataNumero($sqln->result("NISS_NOTAF")),'',1,'R');
  }

  $pdf->Cell(10,5,'',0,0);    
  $pdf->Cell(60,5,'TOTAL GERAL',0,0);
  $pdf->Cell(20,5,$formata->formataNumero($sql4->result("NVENCPAGA")),0,1,'R');

?>
