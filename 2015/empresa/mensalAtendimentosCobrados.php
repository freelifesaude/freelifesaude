<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once('../comum/autoload.php'); 
  
  $bd = new Oracle();
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "EXTRATO DE MENSALIDADES E ATENDIMENTOS COBRADOS";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","mensalAtendimentosCobrados.htm");
    
  $tpl->MASCARA   = $_SESSION['mascara'];  
  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  if (isset($_POST['datainicial']))
    $datainicial = htmlentities($_POST['datainicial']);  
  else
    $datainicial = '';    
  
  if (isset($_POST['datafinal']))
    $datafinal = htmlentities($_POST['datafinal']);  
  else
    $datafinal = '';   
  
  if (isset($_POST['codigo']))
    $codigo = htmlentities($_POST['codigo']);  
  else
    $codigo = '';   
  
  if (isset($_POST['listar_todos']))
    $todos_usuarios = htmlentities($_POST['listar_todos']);  
  else
    $todos_usuarios = '';   
  
  if (isset($_POST['quebrar_pagina']))
    $quebrar = htmlentities($_POST['quebrar_pagina']);  
  else
    $quebrar = '';  
  
  if (isset($_POST['listar_titulares']))
    $titulares = htmlentities($_POST['listar_titulares']);  
  else
    $titulares = '';  
  
  if (isset($_POST['listar_dependentes']))
    $dependentes = htmlentities($_POST['listar_dependentes']);  
  else
    $dependentes = '';  
  
  if (isset($_POST['listar_mensalidades']))
    $mensalidades = htmlentities($_POST['listar_mensalidades']);  
  else
    $mensalidades = '';  
  
  if (isset($_POST['listar_atendimentos']))
    $atendimentos = htmlentities($_POST['listar_atendimentos']);  
  else
    $atendimentos = '';  
  
        
  if ((isset($_POST['imprimir'])) or (isset($_POST['arquivo']))) {
  
    if (($codigo <> '') or ($todos_usuarios == 'S') or ($titulares == 'S') or ($dependentes == 'S')) {  
  
      $sql = new Query($bd);    
      $id_locacao = $_SESSION['id_locacao'];
      $id_contrato = $_SESSION['id_contrato'];

      if ($_SESSION['apelido_operadora'] == 'vitallis') {
        $txt_congenere = "SELECT NNUMECONGE FROM HSSTITU
                           WHERE NNUMETITU = :contrato
                             AND HSSTITU.NNUMECONGE = 2040382163 ";
        $sql_conge = new Query($bd);                         
        $sql_conge->addParam(":contrato",$id_contrato);
        $sql_conge->executeQuery($txt_congenere);

        if ($sql_conge->count() > 0) {
          $f_at1 = "   AND HSSCONT.CAMBUCONT <> '6' ";
          $f_at2 = "   AND HSSGUIA.CTIPOGUIA <> '6' ";
        }
      } else {
        $f_at1 = "";
        $f_at2 = "";
      }
    
      if ($todos_usuarios == 'S')
        $f_codigo .= "";
      else if ($titulares == 'S')
	      $f_codigo .= "   AND HSSUSUA.CTIPOUSUA = 'T' ";
	    else if ($dependentes == 'S')
	      $f_codigo .= "   AND HSSUSUA.CTIPOUSUA <> 'T' AND HSSUSUA.CTIPOUSUA <> 'F' ";
	    else {
        $f_codigo .= "   AND HSSUSUA.CCODIUSUA LIKE :codigo";
        $codigo2 = $codigo."%";
        $sql->addParam(":codigo",$codigo2);        
      }
        
      if ($id_locacao > 0) 
        $f_locacao .= "   AND TITU.NNUMESETOR = :id_locacao "; 
      else      
        $f_locacao .= "";
		
      if ($mensalidades == 'S') {
		    $txt = "SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,0,NULL,NULL,NULL,
		  	               HSSUSUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'M' TIPO,0,
                       NULL,NADESUSUPG,0,HSSUSUA.NNUMEPLAN, 					 
                       IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                       '3' COBRANCA, NVLIQUSUPG COBRADO, TITU.C_CPFUSUA, TITU.CCHAPUSUA, TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') COMPETENCIA, DVENCPAGA, HSSUSUA.CTIPOUSUA 
                  FROM HSSPAGA,HSSUSUPG,HSSUSUA,HSSUSUA TITU,HSSPLAN
                 WHERE HSSPAGA.DVENCPAGA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                   AND HSSPAGA.DVENCPAGA <  TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                   AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA".
               $f_codigo.                 
               $f_at2. 
               $f_locacao.             
               "  AND HSSUSUPG.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                  AND HSSUSUA.CTIPOUSUA <> 'F'
                  AND HSSPAGA.CFLAGPAGA not in ('F','P')
                  AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
                  AND TITU.NNUMETITU = :id_contrato
                  AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";   			   
               if (isset($_POST['imprimir']))                     
                 $txt = $txt." ORDER BY 8,2,19 DESC,4,3";
               else
                 $txt = $txt." ORDER BY 8,2,24,19 DESC,4,3"; 
	    }
	    else if ($atendimentos == 'S') {
        $txt = "SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,HSSCONT.NNUMECONT,HSSCONT.DATENCONT,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                       HSSUSUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'C' TIPO,HSSCONT.NFRANCONT, 
                       TO_CHAR(HSSCONT.DATENCONT,'DD/MM/YY') DATA, TOTAL_CONTA_PLANO(HSSCONT.NNUMECONT) CONTA,HSSCONT.NNUMECONT,HSSUSUA.NNUMEPLAN, 
                       IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                       '1' COBRANCA, NVALOPACON COBRADO, TITU.C_CPFUSUA, TITU.CCHAPUSUA, TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') COMPETENCIA, DVENCPAGA, HSSUSUA.CTIPOUSUA
                  FROM HSSPAGA,HSSPACON,HSSCONT,HSSUSUA,HSSUSUA TITU,FINPRES,FINPRES SOLI,HSSPLAN
                 WHERE HSSPAGA.DVENCPAGA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                   AND HSSPAGA.DVENCPAGA <  TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                   AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
                   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                   AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA ".
               $f_codigo.
               $f_at1.
               $f_locacao.
               "   AND HSSUSUA.CTIPOUSUA <> 'F'
                   AND HSSPAGA.CFLAGPAGA not in ('F','P')
                   AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
                   AND TITU.NNUMETITU = :id_contrato
                   AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+) 
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                 UNION ALL
                SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,HSSGUIA.DEMISGUIA,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                       HSSUSUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'G' TIPO,0, 
                       TO_CHAR(HSSGUIA.DEMISGUIA,'DD/MM/YY') DATA, TOTAL_GUIA(HSSGUIA.NNUMEGUIA) CONTA,HSSGUIA.NNUMEGUIA,HSSUSUA.NNUMEPLAN, 
                       IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                       '2' COBRANCA, NVALOGPAGA COBRADO, TITU.C_CPFUSUA, TITU.CCHAPUSUA, TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') COMPETENCIA, DVENCPAGA, HSSUSUA.CTIPOUSUA
                  FROM HSSPAGA,HSSGPAGA,HSSGUIA,HSSUSUA,HSSUSUA TITU,FINPRES,FINPRES SOLI,HSSPLAN
                 WHERE HSSPAGA.DVENCPAGA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                   AND HSSPAGA.DVENCPAGA <  TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                   AND HSSPAGA.NNUMEPAGA = HSSGPAGA.NNUMEPAGA
                   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA ".
               $f_codigo.                 
               $f_at2. 
               $f_locacao.             
               "   AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                   AND HSSUSUA.CTIPOUSUA <> 'F'
                   AND HSSPAGA.CFLAGPAGA not in ('F','P')
                   AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
                   AND TITU.NNUMETITU = :id_contrato
                   AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+) 
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";
               if (isset($_POST['imprimir']))                     
                 $txt = $txt." ORDER BY 8,2,19 DESC,4,3";
               else
                 $txt = $txt." ORDER BY 8,2,24,19 DESC,4,3"; 			   
	    }
	    else {
        $txt = "SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,HSSCONT.NNUMECONT,HSSCONT.DATENCONT,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                       HSSUSUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'C' TIPO,HSSCONT.NFRANCONT, 
                       TO_CHAR(HSSCONT.DATENCONT,'DD/MM/YY') DATA, TOTAL_CONTA_PLANO(HSSCONT.NNUMECONT) CONTA,HSSCONT.NNUMECONT,HSSUSUA.NNUMEPLAN, 
                       IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                       '1' COBRANCA, NVALOPACON COBRADO, TITU.C_CPFUSUA, TITU.CCHAPUSUA, TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') COMPETENCIA, DVENCPAGA, HSSUSUA.CTIPOUSUA
                  FROM HSSPAGA,HSSPACON,HSSCONT,HSSUSUA,HSSUSUA TITU,FINPRES,FINPRES SOLI,HSSPLAN
                 WHERE HSSPAGA.DVENCPAGA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                   AND HSSPAGA.DVENCPAGA <  TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                   AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
                   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                   AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA ".
               $f_codigo.
               $f_at1.
               $f_locacao.
               "   AND HSSUSUA.CTIPOUSUA <> 'F'
                   AND HSSPAGA.CFLAGPAGA not in ('F','P')
                   AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
                   AND TITU.NNUMETITU = :id_contrato
                   AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+) 
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                 UNION ALL
                SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,HSSGUIA.DEMISGUIA,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                       HSSUSUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'G' TIPO,0, 
                       TO_CHAR(HSSGUIA.DEMISGUIA,'DD/MM/YY') DATA, TOTAL_GUIA(HSSGUIA.NNUMEGUIA) CONTA,HSSGUIA.NNUMEGUIA,HSSUSUA.NNUMEPLAN, 
                       IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                       '2' COBRANCA, NVALOGPAGA COBRADO, TITU.C_CPFUSUA, TITU.CCHAPUSUA, TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') COMPETENCIA, DVENCPAGA, HSSUSUA.CTIPOUSUA
                  FROM HSSPAGA,HSSGPAGA,HSSGUIA,HSSUSUA,HSSUSUA TITU,FINPRES,FINPRES SOLI,HSSPLAN
                 WHERE HSSPAGA.DVENCPAGA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                   AND HSSPAGA.DVENCPAGA <  TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                   AND HSSPAGA.NNUMEPAGA = HSSGPAGA.NNUMEPAGA
                   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA ".
               $f_codigo.                 
               $f_at2. 
               $f_locacao.             
               "   AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                   AND HSSUSUA.CTIPOUSUA <> 'F'
                   AND HSSPAGA.CFLAGPAGA not in ('F','P')
                   AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
                   AND TITU.NNUMETITU = :id_contrato
                   AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+) 
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN   
                 UNION ALL
		  	        SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,0,NULL,NULL,NULL,
		  	               HSSUSUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'M' TIPO,0,
                       NULL,NADESUSUPG,0,HSSUSUA.NNUMEPLAN, 					 
                       IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                       '3' COBRANCA, NVLIQUSUPG, TITU.C_CPFUSUA, TITU.CCHAPUSUA, TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') COMPETENCIA, DVENCPAGA, HSSUSUA.CTIPOUSUA 
                  FROM HSSPAGA,HSSUSUPG,HSSUSUA,HSSUSUA TITU,HSSPLAN
                 WHERE HSSPAGA.DVENCPAGA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                   AND HSSPAGA.DVENCPAGA <  TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                   AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA".
               $f_codigo.                 
               $f_at2. 
               $f_locacao.             
               "  AND HSSUSUPG.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                  AND HSSUSUA.CTIPOUSUA <> 'F'
                  AND HSSPAGA.CFLAGPAGA not in ('F','P')
                  AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
                  AND TITU.NNUMETITU = :id_contrato
                  AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN";
               
               if (isset($_POST['imprimir']))                     
                 $txt = $txt." ORDER BY 8,2,19 DESC,24 DESC,4,3";
               else
                 $txt = $txt." ORDER BY 8,2,24,19 DESC,4,3";                   
      }                                 
      $sql->addParam(":datainicial",$datainicial);   
      $sql->addParam(":datafinal",$datafinal);   
      $sql->addParam(":id_contrato",$_SESSION['id_contrato']);      
       
      if ($id_locacao > 0)   
        $sql->addParam(":id_locacao",$id_locacao);  
           
      $sql->executeQuery($txt);   

      if ($sql->count() > 0) {
        if (isset($_POST['imprimir']))
          require_once("mensalAtendimentosCobradosImp.php");
        else if (isset($_POST['arquivo']))
          require_once("mensalAtendimentosCobradosArq.php");            
      }  
      else {
        $tpl->CLASSE = "";
        $tpl->MSG = "N�o existe nenhum atendimento para o per�odo informado.";
        $tpl->block("ERRO");
      }
    } else {
      $tpl->CLASSE = "";
      $tpl->MSG = "Selecione a forma de listagem de benefici�rios.";
      $tpl->block("ERRO");      
    }
    
    $tpl->CODIGO       = $codigo;    
    $tpl->DATA_INICIAL = $datainicial;
    $tpl->DATA_FINAL   = $datafinal;
    
    if ($todos_usuarios == 'S')
      $tpl->TODOS = 'checked';
    else
      $tpl->TODOS = '';
      
    if ($quebrar == 'S')
      $tpl->QUEBRAR = 'checked';
    else
      $tpl->QUEBRAR = '';
	  
    if ($titulares  == 'S')
      $tpl->TITULARES = 'checked';
    else
      $tpl->TITULARES = '';
	
    if ($dependentes == 'S')
      $tpl->DEPENDENTES = 'checked';
    else
      $tpl->DEPENDENTES = '';
	  
	if ($mensalidades == 'S')
      $tpl->MENSALIDADES = 'checked';
    else
      $tpl->MENSALIDADES = '';
	  
	if ($atendimentos == 'S')	  
      $tpl->ATENDIMENTOS = 'checked';
    else
      $tpl->ATENDIMENTOS = '';   

	if ($todos_usuarios == 'S') {
      $tpl->TITULARES = '';	 
      $tpl->DEPENDENTES = '';
    }	  
  }
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     

?>