<?php
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();  
  
  require_once("../comum/layoutJanela.php"); 
  $tpl->addFile("JANELA_CONTEUDO","../comum/localizaProfissao.htm"); 
  $tpl->block("MOSTRA_LOGO");
  
  $nome       = '';  
  $qtde       = 20;
    
  //$local = $_GET['local'];
    
  if (isset($_POST['localizar'])) {
    $nome = $seg->antiInjection(strtoupper($_POST['nome']));
    $qtde = $seg->antiInjection($_POST['qtde']);
        
    if ($nome <> '') {
      $sql = new Query($bd);
      $txt = "SELECT * FROM (
                SELECT NNUMEPROF,CDESCPROF
                  FROM HSSPROF ";
          
      if ($nome <> '') {
        $nome2 = $nome;
        $nome2 = $formata->somenteCaracteres($nome2)."%"; 
        $txt .= " WHERE HSSPROF.CDESCPROF LIKE :nome";
        $sql->addParam(":nome",$nome2);
      }
         
      $txt .= " ORDER BY 2 ";
                                                                
      $txt .= ") WHERE ROWNUM <= :qtde
                ORDER BY 2 ";            

      $sql->addParam(":qtde",$qtde);                    
      $sql->executeQuery($txt);

      while (!$sql->eof()) {         
        $tpl->PROFISSAO_ID   = $sql->result('NNUMEPROF');
        $tpl->PROFISSAO_NOME = $formata->initCap($sql->result('CDESCPROF'));
        $tpl->block("PROFISSAO");
        $sql->next();
      }
    }
    
    $tpl->block("RESULTADO");     
  }
    
  $tpl->NOME = $nome;
  $tpl->QTDE = $qtde;
  
  $bd->close();
  $tpl->show();  
?>