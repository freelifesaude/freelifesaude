<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $bd      = new Oracle();
  $func    = new Funcao();
  $formata = new Formata();
  $util    = new Util();
  
  $id_pagamento = $_POST['id_pagamento'];
  
  $sql_contrato = new Query();
  $txt_contrato = "SELECT CCODITITU,CRAZACONGE,CCUOPCONGE,CAGRPCONGE,HSSTITU.NNUMECONGE,TO_CHAR(DVENCPAGA,'DD/MM/YYYY') VENCIMENTO,
                          NOME_TITULAR_BOLETO(HSSPAGA.NNUMEPAGA) TITULAR,HSSPAGA.NNUMETITU
                     FROM HSSPAGA,HSSTITU,HSSCONGE 
                    WHERE NNUMEPAGA = :id_pagamento
                      AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU 
                      AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE ";
  $sql_contrato->addParam(":id_pagamento",$id_pagamento);
  $sql_contrato->executeQuery($txt_contrato);
  
  $codigo           = $sql_contrato->result("CCODITITU");
  $congenere        = $sql_contrato->result("CRAZACONGE");
  $razao_social     = $sql_contrato->result("TITULAR");
  $calcula_custo_op = '2';$sql_contrato->result("CCUOPCONGE");
  $id_congenere     = $sql_contrato->result("NNUMECONGE");
  $id_contrato      = $sql_contrato->result("NNUMETITU");
  $agrupa_congenere = $sql_contrato->result("CAGRPCONGE");  
  $vencimento       = $sql_contrato->result("VENCIMENTO");  
  
  if (($calcula_custo_op == '2') and ($agrupa_congenere == 'S')) {
    $sql_mens = new Query();
    $txt_mens = "SELECT NNUMEPAGA FROM HSSTITU,HSSPAGA
                  WHERE HSSTITU.NNUMECONGE = :congenere
                    AND HSSTITU.NNUMETITU = HSSPAGA.NNUMETITU
                    AND TO_CHAR(HSSPAGA.DVENCPAGA,'MM/YYYY') = :mes ";
    $sql_mens->addParam(":congenere",$congenere);
    $sql_mens->addParam(":mes",substr($vencimento,3,7));
    $sql_mens->executeQuery($txt_mens);
    
    while (!$sql_mens->eof()) {
      $ids .= $ids.$sql_mens->result("NNUMEPAGA").",";
      $sql_mens->next();
    }
    
    $ids = substr($ids,-1);
  } else
    $ids = $id_pagamento;
    
  $arquivo_populacao = 'Populacao_'.$congenere.'_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_populacao, "w");

  $sql_usu = new Query();
  $txt_usu = "SELECT DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,USUA.CCODIUSUA CODI_USUA,TITU.CCODIUSUA CODI_TITU,USUA.CNOMEUSUA NOME_USUA, TITU.CNOMEUSUA NOME_TITU,
                     HSSTITU.CCODITITU,NVL(HSSSETOR.CCGC_SETOR,HSSEMPR.C_CGCEMPR) C_CGCEMPR,DECODE(USUA.CTIPOUSUA,'T','1','F','1','2') CATEGORIA,
                     SUBSTR(DECODE(USUA.CGRAUUSUA,'F','Filho (a)' ,'E','Esposo (a)' ,'P','Pai'      ,'M','Mae'        ,'I','Irm�o (�)',
                                                  'A','Avo (�)'   ,'T','Tio (a)'    ,'R','Primo (a)','C','Cunhado (a)','G','Genro/Nora',
                                                  'N','Neto(a)'   ,'B','Sobrinho(a)','S','Sogro(a)' ,'O','Outro'      ,'D','Agregado(a)',
                                                  'H','Enteado(a)','J','Companh(a) ','X','Titular','K','Ex-Conjug�','L','Homoafet(a)'),1,11) GRAU,USUA.CNMAEUSUA,USUA.CCHAPUSUA,TO_CHAR(USUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,
                     USUA.C_CPFUSUA, USUA.C__RGUSUA,USUA.CPIS_USUA,TO_CHAR(USUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,DECODE(USUA.CSEXOUSUA,'M','1','2') SEXO,CNOMESETOR,
                     USUA.NNUMEUSUA, CRAZAEMPR, TO_CHAR(USUA.DEXRGUSUA,'DD/MM/YYYY') DEXRGUSUA, USUA.CORRGUSUA,NVL(CCODISTAT,DECODE(USUA.CTIPOUSUA,'T','0','F','0','1')) STATUS,TITU.NNUMESETOR
                FROM HSSUSUPG,HSSUSUA USUA,HSSUSUA TITU,HSSSETOR,HSSTITU,HSSCONGE,HSSEMPR,HSSPAGA,HSSPLAN,HSSSTAT
               WHERE HSSUSUPG.NNUMEPAGA IN (".$ids.")
                 AND HSSUSUPG.NNUMEUSUA = USUA.NNUMEUSUA
                 AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                 AND TITU.NNUMESETOR    = HSSSETOR.NNUMESETOR(+)
                 AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                 AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
                 AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR(+)
                 AND HSSUSUPG.NNUMEPAGA = HSSPAGA.NNUMEPAGA
                 AND USUA.NNUMEPLAN     = HSSPLAN.NNUMEPLAN
                 AND USUA.NNUMESTAT     = HSSSTAT.NNUMESTAT(+) ";

  if ($calcula_custo_op == '2')
    $txt_usu .= " ORDER BY TITU.NNUMESETOR,5,1,4 ";
  else
    $txt_usu .= " ORDER BY CRAZAEMPR,5,1,4 ";

  $sql_usu->executeQuery($txt_usu);
  
  if ($sql_usu->count() > 0) {
    while (!$sql_usu->eof()) {
    
      $situacao = $sql_usu->result("STATUS");

      if ($sql_usu->result("DEXRGUSUA") == '')
        $dataexpedicao = '          ';
      else
        $dataexpedicao = $sql_usu->result("DEXRGUSUA");
      
      $endereco = $func->enderecoUsuario($bd,$sql_usu->result("NNUMEUSUA"));
      
      if ($_SESSION['apelido_operadora'] == 'promedmg')
        $buffer = $formata->acrescentaZeros(substr($formata->somenteNumeros($sql_usu->result("CODI_USUA")),0,9),9).
                  $formata->acrescentaZeros(substr($formata->somenteNumeros($sql_usu->result("CODI_TITU")),0,9),9);
      else
        $buffer = $formata->acrescentaZeros(substr($sql_usu->result("CODI_USUA"),0,6),6).
                  $formata->acrescentaZeros(substr($sql_usu->result("CODI_TITU"),0,6),6);
                  
      $buffer .= $formata->acrescentaBrancos(substr($sql_usu->result("NOME_USUA"),0,40),40,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($sql_usu->result("NOME_TITU"),0,40),40,'D',chr(32)).
                 $formata->acrescentaZeros($formata->somenteNumeros($sql_usu->result("C_CGCEMPR")),14).
                 $sql_usu->result("CATEGORIA").
                 $formata->acrescentaBrancos($sql_usu->result("GRAU"),15,'D',chr(32)).
                 $formata->acrescentaZeros($formata->somenteNumeros($sql_usu->result("C_CPFUSUA")),11).
                 $formata->acrescentaZeros($formata->somenteNumeros($sql_usu->result("C__RGUSUA")),15).
                 $formata->acrescentaBrancos(substr($sql_usu->result("CORRGUSUA"),0,15),15,'D',chr(32)).
                 $dataexpedicao.
                 $sql_usu->result("DNASCUSUA").
                 $sql_usu->result("SEXO").
                 $formata->acrescentaBrancos(substr($sql_usu->result("CNMAEUSUA"),0,30),30,'D',chr(32)).
                 $sql_usu->result("DINCLUSUA").
                 $formata->acrescentaBrancos(substr($sql_usu->result("CNOMESETOR"),0,30),30,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($endereco['logradouro'],0,40),40,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($endereco['bairro'],0,15),15,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($formata->somenteNumeros($endereco['cep']),0,2),2,'D',chr(32)).".".
                 $formata->acrescentaBrancos(substr($formata->somenteNumeros($endereco['cep']),2,3),3,'D',chr(32))."-".
                 $formata->acrescentaBrancos(substr($formata->somenteNumeros($endereco['cep']),5,3),3,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($endereco['cidade'],0,20),20,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($endereco['estado'],0,2),2,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($sql_usu->result("CCHAPUSUA"),0,10),10,'D',chr(32)).
                 $formata->acrescentaBrancos(substr($situacao,0,20),20,'D',chr(32));
                 
      fwrite($ponteiro,$buffer.chr(13).chr(10));                   
      $sql_usu->next();
    }
      
    fclose ($ponteiro);      
  }
  
  $arquivo_taxa_adm = 'Taxa_Administrativa_'.$congenere.'_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_taxa_adm, "w");
  
  $sql_adm = new Query();
  $txt_adm = "SELECT DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,CRAZAEMPR,CNOMESETOR,USUA.CNOMEUSUA,CCODIPLAN,USUA.NNUMEUSUA,TITU.CNOMEUSUA TITULAR,
                     USUA.NTITUUSUA,TITU.CCHAPUSUA,USUA.DINCLUSUA,USUA.DNASCUSUA,HSSTITU.CCODITITU,
                     USUA.CTIPOUSUA,IDADE(USUA.DNASCUSUA,DVENCPAGA) IDADE,TITU.NNUMESETOR,VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,7) VALOR,VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,8) ADESAO
                FROM HSSUSUPG,HSSUSUA USUA,HSSUSUA TITU,HSSSETOR,HSSTITU,HSSEMPR,HSSPAGA,HSSPLAN
               WHERE HSSUSUPG.NNUMEPAGA IN (".$ids.")
                 AND HSSUSUPG.NNUMEUSUA = USUA.NNUMEUSUA
                 AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                 AND TITU.NNUMESETOR    = HSSSETOR.NNUMESETOR(+)
                 AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                 AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR(+)
                 AND HSSUSUPG.NNUMEPAGA = HSSPAGA.NNUMEPAGA
                 AND USUA.NNUMEPLAN     = HSSPLAN.NNUMEPLAN
              ORDER BY CRAZAEMPR,CNOMESETOR,7,1,USUA.NNUMEUSUA ";
  $sql_adm->executeQuery($txt_adm);
 
  if ($sql_adm->count() > 0) { 
    fwrite($ponteiro,$_SESSION['nome_operadora']." ANS N� ".$_SESSION['registro_ans'].chr(13).chr(10));                   
    fwrite($ponteiro,"FATURA PLANO EMPRESA - ".$codigo." - ".$razao_social.chr(13).chr(10));                   
    fwrite($ponteiro,"TAXA ADMINISTRATIVA");    
    fwrite($ponteiro,"Vencimento : ".$vencimento.chr(13).chr(10));                   
    fwrite($ponteiro,"Matricula  Usuario                      Plano   Tipo do usuario  Nascimento Idade Inclusao   Adesao   Valor".chr(13).chr(10));                   
    fwrite($ponteiro,str_repeat("_",112).chr(13).chr(10));                   
    
    //Matricula  Usuario                      Plano   Tipo do usuario  Nascimento Idade Inclusao   Adesao   Valor
    //1234567890 1234567890123456789012345678 1234567 1234567890123456 1234567890 12345 1234567890 12345678 12345678
      
    $titular = $sql_adm->result("NTITUUSUA");
    $empresa = '%%%%';
    $id_setor = -1;
    
    $total_setor_tit = 0;
    $total_geral_tit = 0;
    $total_empr_tit  = 0;

    $num_tit_geral   = 0;
    $num_tit_setor   = 0;
    $num_tit_empr    = 0;
    $total_titular   = 0;
    $total_geral_dep = 0;
    $total_geral_agr = 0;
    $inss_setor      = 0;
    $vl_contas_setor = 0;
    $despesas_setor  = 0;
    $copartsetor     = 0;
    
    while (!$sql_adm->eof()) {
      if ($empresa <> $sql_adm->result("CCODITITU").'-'.$sql_adm->result("CRAZAEMPR")) {
        $empresa = $sql_adm->result("CCODITITU").'-'.$sql_adm->result("CRAZAEMPR");
        $id_setor = $sql_adm->result("NNUMESETOR");

        $total_setor_tit = 0;
        $total_setor_dep = 0;
        $total_setor_agr = 0;

        $num_tit_setor = 0;
        $num_dep_setor = 0;
        $num_agr_setor = 0;

        $total_empr_tit = 0;
        $total_empr_dep = 0;
        $total_empr_agr = 0;

        $num_tit_empr = 0;
        $num_dep_empr = 0;
        $num_agr_empr = 0;

        fwrite($ponteiro,chr(13).chr(10));      
        fwrite($ponteiro,"Empresa: ".$empresa.chr(13).chr(10));      
        fwrite($ponteiro,chr(13).chr(10));
        fwrite($ponteiro,"Loca��o: ".$sql_adm->result("CNOMESETOR").chr(13).chr(10));      
        fwrite($ponteiro,chr(13).chr(10));
      }
      
      if ($id_setor <> $sql_adm->result("NNUMESETOR")) {
        $id_setor = $sql_adm->result("NNUMESETOR");

        $total_setor_tit = 0;
        $total_setor_dep = 0;
        $total_setor_agr = 0;
        $num_tit_setor = 0;
        $num_dep_setor = 0;
        $num_agr_setor = 0;

        fwrite($ponteiro,chr(13).chr(10));
        fwrite($ponteiro,"Loca��o: ".$sql_adm->result("CNOMESETOR").chr(13).chr(10));      
        fwrite($ponteiro,chr(13).chr(10));
      }
      
      if (($sql_adm->result("CTIPOUSUA") == 'T') or ($sql_adm->result("CTIPOUSUA") == 'F')) {
        $buffer = $formata->acrescentaBrancos($sql_adm->result("CCHAPUSUA"),10,"D"," ").' '.
                  $formata->acrescentaBrancos(substr($sql_adm->result("CNOMEUSUA"),0,28),28,'D',' ').' ';
      } else {
        $buffer = str_repeat(' ',14).
                  $formata->acrescentaBrancos(substr($sql_adm->result("CNOMEUSUA"),1,25),25,'D',' ').' ';    
      }
      
      $vl_valor_usuario = $sql_adm->result("VALOR");
      $vl_adesao_usuario = $sql_adm->result("ADESAO");

      switch ($sql_adm->result("CTIPOUSUA")) {
        case 'T' : $tipo_usuario = 'Titular         '; break;
        case 'F' : $tipo_usuario = 'Tit. financeiro '; break;
        case 'D' : $tipo_usuario = 'Dependente      '; break;
        case 'U' : $tipo_usuario = 'Depe. universit.'; break;
        case 'E' : $tipo_usuario = 'Depe. deficiente'; break;
        case 'L' : $tipo_usuario = 'Dependente legal'; break;
        case 'A' : $tipo_usuario = 'Agregado        '; break;
      }
      
      $buffer .= $formata->acrescentaBrancos($sql_adm->result("CCODIPLAN"),7,'D',' ').' '.
                 $tipo_usuario.' '.
                 $formata->acrescentaBrancos($sql_adm->result("DNASCUSUA"),10,'D',' ').' '.
                 $formata->acrescentaBrancos($sql_adm->result("IDADE"),5,'D',' ').' '.
                 $formata->acrescentaBrancos($sql_adm->result("DINCLUSUA"),10,'D',' ').' '.
                 $formata->acrescentaBrancos($formata->formataNumero($vl_adesao_usuario,2),8,'E',' ').' '.
                 $formata->acrescentaBrancos($formata->formataNumero($vl_valor_usuario,2),8,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10));
     
      if (($sql_adm->result("CTIPOUSUA") == 'T') or ($sql_adm->result("CTIPOUSUA") == 'F')) {
        $total_setor_tit += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        $total_geral_tit += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        $total_empr_tit += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));

        $num_tit_geral++;
        $num_tit_setor++;
        $num_tit_empr++;
      } else if ($sql_adm->result("CTIPOUSUA") == 'A') {
        $total_setor_agr += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        $total_geral_agr += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        $total_empr_agr += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));

        $num_agr_geral++;
        $num_agr_setor++;
        $num_agr_empr++;
      } else {
        $total_setor_dep += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        $total_geral_dep += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        $total_empr_dep += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));

        $num_dep_geral++;
        $num_dep_setor++;
        $num_dep_empr++;
      }

      $total_titular += (str_replace(',','.',$vl_valor_usuario) + str_replace(',','.',$vl_adesao_usuario));
        
      $sql_adm->next();
      
      if (($sql_adm->result("NTITUUSUA") <> $titular) or ($sql_adm->eof())) {
        $titular = $sql_adm->result("NTITUUSUA");

        $buffer = str_repeat(' ',69).'      Total da familia: '.$formata->acrescentaBrancos($formata->formataNumero($total_titular,2),17,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10));
        fwrite($ponteiro,chr(13).chr(10));
        $total_titular = 0;
      }
      
      if (($id_setor <> $sql_adm->result("NNUMESETOR")) or ($sql_adm->eof())) {
        $total_setor = str_replace(',','.',$total_setor_tit) +
                       str_replace(',','.',$total_setor_dep) +
                       str_replace(',','.',$total_setor_agr);
                         
        if (($total_setor > 0) and ($id_setor >= 0)) {
          fwrite($ponteiro,chr(13).chr(10));

          //Titulares: 123456789012            Dependentes: 123456789012            Agregados: 123456789012
          //    Total: 123456789012                  Total: 123456789012                Total: 123456789012
          //                                                           Total geral da locacao: 123456789012

          $buffer = '  Titulares: '.$formata->acrescentaBrancos($num_tit_setor,12,'E',' ').'     '.
                    'Dependentes: '.$formata->acrescentaBrancos($num_dep_setor,12,'E',' ').'     '.
                    '  Agregados: '.$formata->acrescentaBrancos($num_agr_setor,12,'E',' ');
          fwrite($ponteiro,$buffer.chr(13).chr(10));
          $buffer = '      Total: '.$formata->acrescentaBrancos($formata->formataNumero($total_setor_tit,2),12,'E',' ').'     '.
                    '      Total: '.$formata->acrescentaBrancos($formata->formataNumero($total_setor_dep,2),12,'E',' ').'     '.
                    '      Total: '.$formata->acrescentaBrancos($formata->formataNumero($total_setor_agr,2),12,'E',' ');
          fwrite($ponteiro,$buffer.chr(13).chr(10));
          fwrite($ponteiro,chr(13).chr(10));
          fwrite($ponteiro,str_repeat(" ",69).'Totaliza��o da locacao: '.$formata->acrescentaBrancos($formata->formataNumero($total_setor,2),17,'E',' ').chr(13).chr(10));
          fwrite($ponteiro,str_repeat("_",112).chr(13).chr(10));
          
        }
      }  
      
      if (($empresa <> $sql_adm->result("CCODITITU").'-'.$sql_adm->result("CRAZAEMPR")) or ($sql_adm->eof())) {
        $total_empresa = str_replace(',','.',$total_empr_tit) +
                         str_replace(',','.',$total_empr_dep) +
                         str_replace(',','.',$total_empr_agr);
                       
        $buffer = '  Titulares: '.$formata->acrescentaBrancos($num_tit_empr,12,'E',' ').'     '.
                  'Dependentes: '.$formata->acrescentaBrancos($num_dep_empr,12,'E',' ').'     '.
                  '  Agregados: '.$formata->acrescentaBrancos($num_agr_empr,12,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10));    

        $buffer = '      Total: '.$formata->acrescentaBrancos($formata->formataNumero($total_empr_tit,2),12,'E',' ').'     '.
                  '      Total: '.$formata->acrescentaBrancos($formata->formataNumero($total_empr_dep,2),12,'E',' ').'     '.
                  '      Total: '.$formata->acrescentaBrancos($formata->formataNumero($total_empr_agr,2),12,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10));
        fwrite($ponteiro,chr(13).chr(10));
        fwrite($ponteiro,str_repeat(" ",69).'Totaliza��o da empresa: '.$formata->acrescentaBrancos($formata->formataNumero($total_empresa,2),17,'E',' ').chr(13).chr(10));
        fwrite($ponteiro,str_repeat("_",112).chr(13).chr(10));
      }     
    }
    
    $total_geral = str_replace(',','.',$total_geral_tit) +
                   str_replace(',','.',$total_geral_dep) +
                   str_replace(',','.',$total_geral_agr);
                         
    fwrite($ponteiro,chr(13).chr(10));
    fwrite($ponteiro,chr(13).chr(10));
    fwrite($ponteiro,str_repeat(" ",69).'     Totaliza��o geral: '.$formata->acrescentaBrancos($formata->formataNumero($total_geral,2),17,'E',' ').chr(13).chr(10));
        
    fclose ($ponteiro);      
  }
  
  $arquivo_custo_operacional = 'Custo_Operacional_'.$congenere.'_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_custo_operacional, "w");
  
  $sql_desp = new Query();
  $txt_desp = "SELECT CRAZAEMPR,CNOMESETOR,USUA.NTITUUSUA,USUA.CCODIUSUA,DATENCONT,CNOMEPRES,
                      USUA.CNOMEUSUA,TITU.CNOMEUSUA TITULAR,HSSCONT.NNUMECONT,
                      'C' TIPO,USUA.CGRAUUSUA,
                      SUM(VALOR_TOTAL_FATURADO(HSSPACON.NNUMEPAGA,HSSCONT.NNUMEUSUA,3,HSSCONT.NNUMECONT)) NVALOPACON,SUM(NCONTPACON) NCONTPACON,SUM(NTAXAPACON) NTAXAPACON,
                      SUM(NPIS_PACON) NPIS_PACON,SUM(NCOFIPACON) NCOFIPACON,SUM(NCPMFPACON) NCPMFPACON,
                      SUM(NINSSPACON) NINSSPACON,SUM(NISS_PACON) NISS_PACON,HSSSETOR.NNUMESETOR,HSSTITU.CCODITITU
                 FROM HSSPACON,HSSCONT,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                WHERE HSSPACON.NNUMEPAGA IN (".$ids.")
                  AND HSSPACON.NVALOPACON > 0
                  AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                  AND HSSCONT.NNUMEPRES  = FINPRES.NNUMEPRES
                  AND HSSCONT.NNUMEUSUA  = USUA.NNUMEUSUA
                  AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                  AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                  AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                  AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)
                GROUP BY CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA,USUA.CGRAUUSUA,USUA.NTITUUSUA,USUA.CNOMEUSUA,USUA.CCODIUSUA,DATENCONT,CNOMEPRES,HSSCONT.NNUMECONT,HSSSETOR.NNUMESETOR,HSSTITU.CCODITITU
               UNION ALL
               SELECT CRAZAEMPR,CNOMESETOR,USUA.NTITUUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,
                      USUA.CNOMEUSUA,TITU.CNOMEUSUA TITULAR,HSSGUIA.NNUMEGUIA,
                      'G' TIPO,USUA.CGRAUUSUA,
                      SUM(VALOR_TOTAL_FATURADO(HSSGPAGA.NNUMEPAGA,HSSGUIA.NNUMEUSUA,2,HSSGUIA.NNUMEGUIA)),SUM(NGUIAGPAGA),SUM(NTAXAGPAGA),0 NPIS_PACON,0 NCOFIPACON,0 NCPMFPACON,0 NINSSPACON,0 NISS_PACON,HSSSETOR.NNUMESETOR,HSSTITU.CCODITITU
                 FROM HSSGPAGA,HSSGUIA,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                WHERE HSSGPAGA.NNUMEPAGA IN (".$ids.")
                  AND HSSGPAGA.NVALOGPAGA > 0
                  AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                  AND HSSGUIA.NNUMEPRES  = FINPRES.NNUMEPRES
                  AND HSSGUIA.NNUMEUSUA  = USUA.NNUMEUSUA
                  AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                  AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                  AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                  AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)
                GROUP BY CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA,USUA.CGRAUUSUA,USUA.NTITUUSUA,USUA.CNOMEUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,HSSGUIA.NNUMEGUIA,HSSSETOR.NNUMESETOR,HSSTITU.CCODITITU";
                
  if ($calcula_custo_op == '2')
    $txt_desp .= " ORDER BY 1,2,8,11,5 DESC";
  else
    $txt_desp .= " ORDER BY 1,8,11,5 DESC";
    
  $sql_desp->executeQuery($txt_desp);
  
  fwrite($ponteiro,$_SESSION['nome_operadora']." ANS N� ".$_SESSION['registro_ans'].chr(13).chr(10));                   
  fwrite($ponteiro,"FATURA PLANO EMPRESA - ".$codigo." - ".$razao_social.chr(13).chr(10));                   
  fwrite($ponteiro,"CUSTO OPERACIONAL");
  fwrite($ponteiro,"Vencimento : ".$vencimento.chr(13).chr(10));                   
  fwrite($ponteiro,"Codigo       Titular                       Paciente                      Conta    Data       Prestador do servico   Valor      INSS           Total".chr(13).chr(10));                   
  fwrite($ponteiro,str_repeat("_",180).chr(13).chr(10));                   
  
  //Codigo       Titular                       Paciente                      Conta    Data  Prestador do servico   Valor      INSS           Total
  //123456789012 12345678901234567890123456789 12345678901234567890123456789 12345678 12345 1234567890123456789012 1234567890 123456789 1234567890
    
  $despesas = 0;
  $titular = $sql_desp->result("NTITUUSUA");    
  
  if ($sql_desp->count() > 0) {
    fwrite($ponteiro,chr(13).chr(10));     
    
    $empresa = "";

    $despesas_titular = 0;
    $inss_titular = 0;
    $vl_contas_titular = 0;

    $despesas = 0;
    $inss = 0;
    $vl_contas = 0;
    $inss_empr = 0;
    $vl_contas_empr = 0;
    $despesas_empr = 0;    
    
    while (!$sql_desp->eof()) {
    
      if ($empresa <> $sql_desp->result("CRAZAEMPR")) {
        $empresa = $sql_desp->result("CRAZAEMPR");

        $inss_empr = 0;
        $vl_contas_empr = 0;
        $despesas_empr = 0;

        fwrite($ponteiro,chr(13).chr(10)); 
        fwrite($ponteiro,"Empresa: ".$empresa.chr(13).chr(10));         
        fwrite($ponteiro,chr(13).chr(10)); 
      }
      
      if (($calcula_custo_op == '2') and ($id_setor <> $sql_desp->result("NNUMESETOR"))) {
        $id_setor = $sql_desp->result("NNUMESETOR");

        $inss_setor = 0;
        $vl_contas_setor = 0;
        $despesas_setor = 0;

        fwrite($ponteiro,chr(13).chr(10)); 
        fwrite($ponteiro,"Loca��o: ".$sql_desp->result("CNOMESETOR").chr(13).chr(10));
        fwrite($ponteiro,chr(13).chr(10)); 
      }      
    
      $buffer = $formata->acrescentaBrancos(substr($sql_desp->result("CCODIUSUA"),0,12),12,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("TITULAR"),0,29),29,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("CNOMEUSUA"),0,29),29,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("NNUMECONT"),0,8),8,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("DATENCONT"),0,10),10,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("CNOMEPRES"),0,22),22,'D',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_desp->result("NCONTPACON"),2),10,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_desp->result("NINSSPACON"),2),9,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_desp->result("NVALOPACON"),2),10,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10)); 
          
      $inss += str_replace(',','.',$sql_desp->result("NINSSPACON"));
      $vl_contas += str_replace(',','.',$sql_desp->result("NCONTPACON"));
      $despesas += str_replace(',','.',$sql_desp->result("NVALOPACON"));

      $inss_titular += str_replace(',','.',$sql_desp->result("NINSSPACON"));
      $vl_contas_titular += str_replace(',','.',$sql_desp->result("NCONTPACON"));
      $despesas_titular += str_replace(',','.',$sql_desp->result("NVALOPACON"));

      $inss_empr += str_replace(',','.',$sql_desp->result("NINSSPACON"));
      $vl_contas_empr += str_replace(',','.',$sql_desp->result("NCONTPACON"));
      $despesas_empr += str_replace(',','.',$sql_desp->result("NVALOPACON"));

      $inss_setor += str_replace(',','.',$sql_desp->result("NINSSPACON"));
      $vl_contas_setor += str_replace(',','.',$sql_desp->result("NCONTPACON"));
      $despesas_setor += str_replace(',','.',$sql_desp->result("NVALOPACON"));
         
      $sql_proc = new Query();         
      $txt_proc = "SELECT HSSPCON.CCODIPMED, CNOMEPMED, NQUANPCON
                     FROM HSSPCON, HSSPMED 
                    WHERE HSSPCON.NNUMECONT = :conta
                      AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                    UNION ALL 
                   SELECT HSSPGUI.CCODIPMED, CNOMEPMED, NQUANPGUI
                     FROM HSSPGUI, HSSPMED
                    WHERE HSSPGUI.NNUMEGUIA = :guia
                      AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                    UNION ALL
                   SELECT HSSTAXA.CCODITAXA, CDESCTAXA, NQUANTCON
                     FROM HSSTCON, HSSTAXA
                    WHERE HSSTCON.NNUMECONT = :conta
                      AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                    ORDER BY 1,2,3";
                    
      if ($sql_desp->result("TIPO") == 'C') {
        $sql_proc->addParam(":conta",$sql_desp->result("NNUMECONT"));
        $sql_proc->addParam(":guia",0);
      } else {
        $sql_proc->addParam(":conta",0);
        $sql_proc->addParam(":guia",$sql_desp->result("NNUMECONT"));
      }
      
      $sql_proc->executeQuery($txt_proc);

      while (!$sql_proc->eof()) {
        $buffer = str_repeat(' ',20).
                  $formata->acrescentaBrancos($sql_proc->result("CCODIPMED"),8,'D',' ').' '.
                  $formata->acrescentaBrancos($sql_proc->result("CNOMEPMED"),60,'D',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($sql_proc->result("NQUANPCON"),2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        $sql_proc->next();
      }
          
      $sql_desp->next();
  
      if (($sql_desp->result("NTITUUSUA") <> $titular) or ($sql_desp->eof())) {
        $titular = $sql_desp->result("NTITUUSUA");
        $buffer = str_repeat(' ',71).
                  '                           Total da familia: '.
                  $formata->acrescentaBrancos($formata->formataNumero($vl_contas_titular,2),10,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($inss_titular,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($despesas_titular,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 

        $despesas_titular = 0;
        $inss_titular = 0;
        $vl_contas_titular = 0;
      }

      if (($calcula_custo_op == "2") and (($id_setor <> $sql_desp->result("NNUMESETOR")) or ($sql_desp->eof()))) {

        fwrite($ponteiro,chr(13).chr(10)); 

        $buffer = str_repeat(' ',71).
                  '      Total de custo operacional da Loca��o: '.
                  $formata->acrescentaBrancos($formata->formataNumero($vl_contas_setor,2),10,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($inss_setor,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($despesas_setor,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
      }

      if (($sql_desp->result("CRAZAEMPR") <> $empresa) or ($sql_desp->eof())) {
        $titular = $sql_desp->result("NTITUUSUA");
        fwrite($ponteiro,chr(13).chr(10)); 

        $buffer = str_repeat(' ',71).
                  '      Total de custo operacional da empresa: '.
                  $formata->acrescentaBrancos($formata->formataNumero($vl_contas_empr,2),10,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($inss_empr,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($despesas_empr,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 

        $despesas_empr = 0;
        $inss_empr = 0;
        $vl_contas_empr = 0;
      }      
    }
    
    fwrite($ponteiro,chr(13).chr(10)); 
    $buffer = str_repeat(' ',71).
              '       Sub-total geral de custo operacional: '.
              $formata->acrescentaBrancos($formata->formataNumero($vl_contas,2),10,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($inss,2),9,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($despesas,2),10,'E',' ');
    fwrite($ponteiro,$buffer.chr(13).chr(10)); 
    fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 

    fwrite($ponteiro,chr(13).chr(10)); 
    fwrite($ponteiro,chr(13).chr(10)); 
    fwrite($ponteiro,chr(13).chr(10)); 
  } else {
    $despesas = 0;
  }
  
  $sql_cart = new Query();
  $txt_cart = "SELECT SUM(NVALOCPAGA) TOTAL FROM HSSCPAGA
                WHERE NNUMEPAGA IN (".$ids.")";
  $sql_cart->executeQuery($txt_cart);
  
  $sql_paga = new Query();
  $txt_paga = "SELECT SUM(NVL(NVMENPAGA,0)) NVMENPAGA, SUM(NVL(NACDEPAGA,0)) NACDEPAGA,SUM(NVL(NDESPPAGA,0)) NDESPPAGA,SUM(NVL(NIMPOPAGA,0)) NIMPOPAGA,
                      SUM(NVL(NTXSSPAGA,0)) NTXSSPAGA,SUM(NVL(NTXRMPAGA,0)) NTXRMPAGA FROM HSSPAGA
                WHERE NNUMEPAGA IN (SELECT NNUMEPAGA FROM HSSPAGA
                                     WHERE NNUMEPAGA IN (".$ids.")
                                     UNION ALL
                                    SELECT NDESTPAGA FROM HSSPARCE
                                     WHERE NNUMEPAGA IN (".$ids.")
                                     UNION ALL
                                    SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2
                                     WHERE PARCE1.NDESTPAGA IN (".$ids.")
                                       AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ";
  $sql_paga->executeQuery($txt_paga);
  
  $custo = str_replace(',','.',$sql_paga->result("NTXSSPAGA")) +
           str_replace(',','.',$sql_paga->result("NTXSSPAGA")) +
           str_replace(',','.',$despesas);
           
  fwrite($ponteiro,$formata->acrescentaBrancos('SUB-TOTAL CUSTO OPERACIONAL',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($despesas,2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('TAXA DE SAUDE SUPLEMENTAR (ANS)',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_paga->result("NTXSSPAGA"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('TAXA DE COBERTURA DE REMOCAO AEREA CONFORME CONTRATO',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_paga->result("NTXRMPAGA"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('CUSTO OPERACIONAL',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($custo,2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('SEGUNDA VIA DE CARTEIRA',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_cart->result("TOTAL"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('IMPOSTOS',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_paga->result("NIMPOPAGA"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('TOTAL DE DESPESAS',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_paga->result("NDESPPAGA"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,chr(13).chr(10)); 
  fwrite($ponteiro,chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('TOTAL MENSALIDADES',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_paga->result("NVMENPAGA"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('TOTAL DE DESPESAS',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_paga->result("NDESPPAGA"),2),10,'E',' ').chr(13).chr(10)); 

  $sql_acde = new Query();
  $txt_acde = "SELECT CMOTIADPAG,VALOR_TOTAL_FATURADO(NNUMEPAGA,NNUMEADPR,4) NVALOADPAG FROM HSSADPAG
                WHERE HSSADPAG.NNUMEPAGA IN (".$ids.")";
  $sql_acde->executeQuery($txt_acde);
  
  $sql_nota = new Query();
  $txt_nota = "SELECT NIR__NOTAF,NISS_NOTAF FROM HSSCNOTA,HSSNOTAF
                WHERE HSSCNOTA.NNUMEPAGA IN (".$ids.")
                  AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF
                  AND CSITUNOTAF = 'A' ";
                  
  $sql_nota->executeQuery($txt_nota);  
  
  while (!$sql_acde->eof()) {
    $buffer = $formata->acrescentaBrancos(substr($sql_acde->result("CMOTIADPAG"),1,60),60,'D',' ').
              $formata->acrescentaBrancos($formata->formataNumero($sql_acde->result("NVALOADPAG"),2),10,'E',' ');
    fwrite($ponteiro,$buffer.chr(13).chr(10)); 
    $sql_acde->next();
  }
  
  fwrite($ponteiro,$formata->acrescentaBrancos('IMPOSTO DE RENDA',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_nota->result("NIR__NOTAF"),2),10,'E',' ').chr(13).chr(10)); 
  fwrite($ponteiro,$formata->acrescentaBrancos('ISS',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($sql_nota->result("NISS_NOTAF"),2),10,'E',' ').chr(13).chr(10)); 
  
  $total_geral = (str_replace(',','.',$sql_paga->result("NVMENPAGA")) +  
                  str_replace(',','.',$sql_paga->result("NACDEPAGA")) +
                  str_replace(',','.',$sql_paga->result("NDESPPAGA"))) -
                 (str_replace(',','.',$sql_paga->result("NIR__NOTAF")) +
                  str_replace(',','.',$sql_paga->result("NISS_NOTAF")));

  fwrite($ponteiro,$formata->acrescentaBrancos('TOTAL GERAL',60,'D',' ').$formata->acrescentaBrancos($formata->formataNumero($total_geral,2),10,'E',' ').chr(13).chr(10)); 
  
  fclose ($ponteiro);   
  
  $arquivo_copart_dep = 'Copart_Dependentes_'.$congenere.'_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_copart_dep, "w");

  $sql_desp = new Query();
  $txt_desp = " SELECT CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA TITULAR,DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,USUA.CNOMEUSUA,USUA.NTITUUSUA,USUA.CCODIUSUA,DATENCONT,CNOMEPRES,
                       HSSCONT.NNUMECONT,
                       USUA.CGRAUUSUA,NFRANCONT,HSSSETOR.NNUMESETOR
                  FROM HSSPACON,HSSCONT,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                 WHERE HSSPACON.NNUMEPAGA IN (".$ids.")
                   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                   AND NFRANCONT > 0
                   AND HSSCONT.NNUMEPRES  = FINPRES.NNUMEPRES
                   AND HSSCONT.NNUMEUSUA  = USUA.NNUMEUSUA
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                   AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)
                UNION ALL
                SELECT CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA TITULAR,DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,USUA.CNOMEUSUA,USUA.NTITUUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,
                       HSSGUIA.NNUMEGUIA,
                       USUA.CGRAUUSUA,NFRANPGUI,HSSSETOR.NNUMESETOR
                  FROM HSSGPAGA,HSSGUIA,HSSPGUI,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                 WHERE HSSGPAGA.NNUMEPAGA IN (".$ids.")
                   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                   AND HSSGUIA.NNUMEGUIA  = HSSPGUI.NNUMEGUIA
                   AND NFRANPGUI > 0
                   AND HSSGUIA.NNUMEPRES  = FINPRES.NNUMEPRES
                   AND HSSGUIA.NNUMEUSUA  = USUA.NNUMEUSUA
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                   AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)
                UNION ALL
                SELECT CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA TITULAR,DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,USUA.CNOMEUSUA,USUA.NTITUUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,
                       HSSGUIA.NNUMEGUIA,
                       USUA.CGRAUUSUA,NCOPADGUI,HSSSETOR.NNUMESETOR
                  FROM HSSGPAGA,HSSGUIA,HSSDGUI,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                 WHERE HSSGPAGA.NNUMEPAGA IN (".$ids.")
                   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                   AND HSSGUIA.NNUMEGUIA  = HSSDGUI.NNUMEGUIA
                   AND NCOPADGUI > 0
                   AND HSSGUIA.NNUMEPRES  = FINPRES.NNUMEPRES
                   AND HSSGUIA.NNUMEUSUA  = USUA.NNUMEUSUA
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                   AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)
                UNION ALL
                SELECT CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA TITULAR,DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,USUA.CNOMEUSUA,USUA.NTITUUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,
                       HSSGUIA.NNUMEGUIA,
                       USUA.CGRAUUSUA,NCOPAPCGUI,HSSSETOR.NNUMESETOR
                  FROM HSSGPAGA,HSSGUIA,HSSPCGUI,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                 WHERE HSSGPAGA.NNUMEPAGA IN (".$ids.")
                   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                   AND HSSGUIA.NNUMEGUIA  = HSSPCGUI.NNUMEGUIA
                   AND NCOPAPCGUI > 0
                   AND HSSGUIA.NNUMEPRES  = FINPRES.NNUMEPRES
                   AND HSSGUIA.NNUMEUSUA  = USUA.NNUMEUSUA
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                   AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)";
                
  if ($calcula_custo_op == '2')
    $txt_desp .= " ORDER BY 1,2,3,4,5";
  else
    $txt_desp .= " ORDER BY 1,3,4,5";
    
  $sql_desp->executeQuery($txt_desp);
  
  fwrite($ponteiro,$_SESSION['nome_operadora']." ANS N� ".$_SESSION['registro_ans'].chr(13).chr(10));                   
  fwrite($ponteiro,"FATURA PLANO EMPRESA - ".$codigo." - ".$razao_social.chr(13).chr(10));                   
  fwrite($ponteiro,"COPARTICIPACOES DE DEPENDENTES");
  fwrite($ponteiro,"Vencimento : ".$vencimento.chr(13).chr(10));                   
  fwrite($ponteiro,"Codigo       Titular                        Paciente                                 Conta    Data       Prestador do servico        Valor      INSS       Total".chr(13).chr(10));                   
  fwrite($ponteiro,str_repeat("_",180).chr(13).chr(10));  

  //Codigo       Titular                        Paciente                                 Conta    Data       Prestador do servico        Valor      INSS       Total
  //123456789012 123456789012345678901234567890 1234567890123456789012345678901234567890 12345678 1234567890 123456789012345678901234567 1234567890 1234567890 1234567890

  fwrite($ponteiro,chr(13).chr(10)); 

  $totalcopartdependentes = 0;
  $coparttitular = 0;
  $copartempresa = 0;
  $empresa = '';
      
  while (!$sql_desp->eof()) {
  
    if ($empresa <> $sql_desp->result("CRAZAEMPR")) {
      $empresa = $sql_desp->result("CRAZAEMPR");
      $copartempresa = 0;

      fwrite($ponteiro,chr(13).chr(10)); 
      fwrite($ponteiro,"Empresa: ".$empresa.chr(13).chr(10)); 
      fwrite($ponteiro,chr(13).chr(10)); 
    }

    if (($calcula_custo_op == "2") and ($id_setor <> $sql_desp->result("NNUMESETOR"))) {
      $id_setor = $sql_desp->result("NNUMESETOR");
      $copartsetor = 0;

      fwrite($ponteiro,$buffer.chr(13).chr(10)); 
      fwrite($ponteiro,"Loca��o: ".$sql_desp->result("CNOMESETOR").chr(13).chr(10)); 
      fwrite($ponteiro,chr(13).chr(10)); 
      fwrite($ponteiro,chr(13).chr(10)); 
    }

    $buffer = $formata->acrescentaBrancos(substr($sql_desp->result("CCODIUSUA"),0,12),12,'D',' ').' '.
              $formata->acrescentaBrancos(substr($sql_desp->result("TITULAR"),0,30),30,'D',' ').' '.
              $formata->acrescentaBrancos(substr($sql_desp->result("CNOMEUSUA"),0,40),40,'D',' ').' '.
              $formata->acrescentaBrancos(substr($sql_desp->result("NNUMECONT"),0,8),8,'D',' ').' '.
              $formata->acrescentaBrancos($sql_desp->result("DATENCONT"),10,'D',' ').' '.
              $formata->acrescentaBrancos(substr($sql_desp->result("CNOMEPRES"),0,27),27,'D',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($sql_desp->result("NFRANCONT"),2),10,'E',' ');
    fwrite($ponteiro,$buffer.chr(13).chr(10)); 

    $copartempresa += str_replace(',','.',$sql_desp->result("NFRANCONT"));
    $coparttitular += str_replace(',','.',$sql_desp->result("NFRANCONT"));
    $copartsetor += str_replace(',','.',$sql_desp->result("NFRANCONT"));
    $totalcopartdependentes += str_replace(',','.',$sql_desp->result("NFRANCONT"));

    $sql_desp->next();

    if (($sql_desp->result("NTITUUSUA") <> $titular) or ($sql_desp->eof())) {
      $titular = $sql_desp->result("NTITUUSUA");
      $buffer = str_repeat(' ',80).
                '                                   Total da familia: '.
                $formata->acrescentaBrancos($formata->formataNumero($coparttitular,2),10,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10)); 
      fwrite($ponteiro,chr(13).chr(10)); 
      $coparttitular = 0;
    }

    if (($calcula_custo_op == "2") and (($id_setor <> $sql_desp->result("NNUMESETOR")) or ($sql_desp->eof()))) {

      if (($copartsetor > 0) and ($id_setor >= 0)) {
        fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
        $buffer = str_repeat(' ',80).
                  '               Total de co-participa��es da Loca��o: '.
                  $formata->acrescentaBrancos($formata->formataNumero($copartsetor,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
      }

    }

    if (($empresa <> $sql_desp->result("CRAZAEMPR")) or ($sql_desp->eof())) {
      fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
      $buffer = str_repeat(' ',80).
                'Total de co-participa��es de dependentes da empresa: '.
                $formata->acrescentaBrancos($formata->formataNumero($copartempresa,2),10,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10)); 
      fwrite($ponteiro,chr(13).chr(10)); 
    }
  }
  
  fwrite($ponteiro,chr(13).chr(10)); 
  fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
  $buffer = str_repeat(' ',80).
            '     Total geral de co-participa��es de dependentes: '.
            $formata->acrescentaBrancos($formata->formataNumero($totalcopartdependentes,2),10,'E',' ');
  fwrite($ponteiro,$buffer.chr(13).chr(10)); 
  
  fclose ($ponteiro);   
  
  $arquivo_copart_canc = 'Copart_Dep_Cancelados_'.$congenere.'_Import_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_copart_canc, "w");

  $sql_desp = new Query();
  $txt_desp = " SELECT CRAZAEMPR,USUA.NTITUUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,
                       USUA.CNOMEUSUA,TITU.CNOMEUSUA TITULAR,HSSGUIA.NNUMEGUIA,
                       USUA.CGRAUUSUA,USUA.DSITUUSUA,SUM(NFRANPGUI) VALOR
                  FROM HSSTITU,HSSUSUA USUA,HSSGUIA,HSSPGUI,FINPRES,HSSUSUA TITU,HSSEMPR
                 WHERE HSSTITU.NNUMECONGE IN (SELECT NNUMECONGE FROM HSSCONGE
                                               WHERE NDEPETITU > 0
                                                 AND CCUOPCONGE = '0')
                   AND HSSTITU.NNUMETITU = USUA.NNUMETITU
                   AND USUA.CSITUUSUA <> 'A'
                   AND USUA.CTIPOUSUA NOT IN ('T','F')
                   AND USUA.DSITUUSUA >= FIRST_DAY(ADD_MONTHS(TO_DATE(:vencimento,'DD/MM/YYYY'),-1))
                   AND USUA.DSITUUSUA <= LAST_DAY(ADD_MONTHS(TO_DATE(:vencimento,'DD/MM/YYYY'),-1))
                   AND USUA.NNUMEUSUA = HSSGUIA.NNUMEUSUA
                   AND HSSGUIA.CSTATGUIA IS NULL
                   AND HSSGUIA.NNUMEGUIA NOT IN (SELECT NNUMEGUIA FROM HSSGPAGA
                                                  WHERE HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                                  UNION ALL
                                                 SELECT NNUMEGUIA FROM HSSGCON,HSSPACON
                                                  WHERE HSSGCON.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                                    AND HSSGCON.NNUMECONT = HSSPACON.NNUMECONT)
                   AND HSSGUIA.NNUMEGUIA  = HSSPGUI.NNUMEGUIA
                   AND NFRANPGUI > 0
                   AND HSSPGUI.CCOBRPGUI IS NULL
                   AND HSSGUIA.NNUMEPRES  = FINPRES.NNUMEPRES
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                 GROUP BY CRAZAEMPR,USUA.NTITUUSUA,USUA.CCODIUSUA,DEMISGUIA,CNOMEPRES,
                          USUA.CNOMEUSUA,TITU.CNOMEUSUA,HSSGUIA.NNUMEGUIA,
                          USUA.CGRAUUSUA,USUA.DSITUUSUA
                 ORDER BY 1,7,9,6 DESC";

  $sql_desp->addParam(":vencimento",$vencimento);
  $sql_desp->executeQuery($txt_desp);
  
  if ($sql_desp->count() > 0) {
    fwrite($ponteiro,$_SESSION['nome_operadora']." ANS N� ".$_SESSION['registro_ans'].chr(13).chr(10));                   
    fwrite($ponteiro,"FATURA PLANO EMPRESA - ".$codigo." - ".$razao_social.chr(13).chr(10));                   
    fwrite($ponteiro,"COPARTICIPACOES DE DEPENDENTES CANCELADOS");
    fwrite($ponteiro,"Vencimento : ".$vencimento.chr(13).chr(10));                   
    fwrite($ponteiro,"Codigo       Titular                        Paciente                                 Dt Cancelamento Guia      Data       Prestador do servico           Valor".chr(13).chr(10));
    fwrite($ponteiro,str_repeat("_",180).chr(13).chr(10));  

    //Codigo       Titular                        Paciente                                 Dt Cancelamento Guia      Data       Prestador do servico           Valor
    //123456789012 123456789012345678901234567890 1234567890123456789012345678901234567890 1234567890      123456789 1234567890 123456789012345678901234567890 1234567890

    fwrite($ponteiro,chr(13).chr(10)); 

    $totalcopartdependentes = 0;
    $coparttitular = 0;
    $copartempresa = 0;
    $empresa = '';
              
    while (!$sql_desp->eof()) {
  
      if ($empresa <> $sql_desp->result("CRAZAEMPR")) {
        $empresa = $sql_desp->result("CRAZAEMPR");
        $copartempresa = 0;

        fwrite($ponteiro,chr(13).chr(10)); 
        fwrite($ponteiro,"Empresa: ".$empresa.chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
      }

      $buffer = $formata->acrescentaBrancos(substr($sql_desp->result("CCODIUSUA"),0,12),12,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("TITULAR"),0,30),30,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("CNOMEUSUA"),0,40),40,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("DSITUUSUA"),0,15),15,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("NNUMEGUIA"),0,9),9,'D',' ').' '.
                $formata->acrescentaBrancos($sql_desp->result("DEMISGUIA"),10,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_desp->result("CNOMEPRES"),0,30),30,'D',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_desp->result("VALOR"),2),10,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10)); 

      $copartempresa += str_replace(',','.',$sql_desp->result("NFRANCONT"));
      $coparttitular += str_replace(',','.',$sql_desp->result("NFRANCONT"));
      $totalcopartdependentes += str_replace(',','.',$sql_desp->result("NFRANCONT"));

      $sql_desp->next();

      if (($sql_desp->result("NTITUUSUA") <> $titular) or ($sql_desp->eof())) {
        $titular = $sql_desp->result("NTITUUSUA");
        $buffer = str_repeat(' ',90).
                  '                                              Total da familia:'.
                  $formata->acrescentaBrancos($formata->formataNumero($coparttitular,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
        $coparttitular = 0;
      }

      if (($empresa <> $sql_desp->result("CRAZAEMPR")) or ($sql_desp->eof())) {
        fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
        $buffer = str_repeat(' ',90).
                  'Total de co-participa��es de dependentes cancelados da empresa:'.
                  $formata->acrescentaBrancos($formata->formataNumero($copartempresa,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
      }
    }
    
    fwrite($ponteiro,chr(13).chr(10)); 
    fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
    $buffer = str_repeat(' ',90).
              '     Total geral de co-participa��es de dependentes cancelados:'.
              $formata->acrescentaBrancos($formata->formataNumero($totalcopartdependentes,2),10,'E',' ');
    fwrite($ponteiro,$buffer.chr(13).chr(10)); 
  
    fclose ($ponteiro); 
  } else
    $arquivo_copart_canc = '';
    
  $arquivo_carteiras = 'Carteirinhas_'.$congenere.'_Import_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_carteiras, "w");

  $sql_cart = new Query();
  $txt_cart = " SELECT CRAZAEMPR,CNOMESETOR,TITU.CNOMEUSUA TITULAR,DECODE(USUA.NNUMEUSUA,TITU.NNUMEUSUA,1,2) ORDEM,USUA.CNOMEUSUA,
                       USUA.NTITUUSUA,USUA.CCODIUSUA,
                       USUA.CGRAUUSUA,NVALOCPAGA,HSSSETOR.NNUMESETOR
                  FROM HSSCPAGA,HSSCARTE,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR,HSSSETOR
                 WHERE HSSCPAGA.NNUMEPAGA IN (".$ids.")
                   AND HSSCPAGA.NNUMECARTE = HSSCARTE.NNUMECARTE
                   AND HSSCARTE.NNUMEUSUA  = USUA.NNUMEUSUA
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                   AND TITU.NNUMESETOR  = HSSSETOR.NNUMESETOR(+)";
  $sql_cart->executeQuery($txt_cart);
  
  if ($sql_cart->count() > 0) {
    fwrite($ponteiro,$_SESSION['nome_operadora']." ANS N� ".$_SESSION['registro_ans'].chr(13).chr(10));                   
    fwrite($ponteiro,"FATURA PLANO EMPRESA - ".$codigo." - ".$razao_social.chr(13).chr(10));                   
    fwrite($ponteiro,"CARTEIRINHAS COBRADAS");
    fwrite($ponteiro,"Vencimento : ".$vencimento.chr(13).chr(10));                   
    fwrite($ponteiro,"Codigo       Titular                        Beneficiario                             Valor".chr(13).chr(10));
    fwrite($ponteiro,str_repeat("_",180).chr(13).chr(10));  

    //Codigo       Titular                        Beneficiario                             Valor
    //123456789012 123456789012345678901234567890 1234567890123456789012345678901234567890 1234567890

    fwrite($ponteiro,chr(13).chr(10)); 

    $total = 0;
    $totalempr = 0;
    $empresa = '';
    $id_setor = 0;
    $copartsetor = 0;    
    
    while (!$sql_cart->eof()) {
  
      if ($empresa <> $sql_cart->result("CRAZAEMPR")) {
        $empresa = $sql_cart->result("CRAZAEMPR");
        $totalempr = 0;

        fwrite($ponteiro,chr(13).chr(10)); 
        fwrite($ponteiro,"Empresa: ".$empresa.chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
      }
	  
      if (($calcula_custo_op == "2") and ($id_setor <> $sql_cart->result("NNUMESETOR"))) {
        $id_setor = $sql_cart->result("NNUMESETOR");
        $copartsetor = 0;

        fwrite($ponteiro,chr(13).chr(10)); 
        fwrite($ponteiro,"Loca��o: ".$sql_cart->result("CNOMESETOR").chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
      }	  

      $buffer = $formata->acrescentaBrancos(substr($sql_cart->result("CCODIUSUA"),0,12),12,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_cart->result("TITULAR"),0,30),30,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_cart->result("CNOMEUSUA"),0,40),40,'D',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_cart->result("NVALOCPAGA"),2),10,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10)); 

      $totalempr += str_replace(',','.',$sql_cart->result("NVALOCPAGA"));
      $total += str_replace(',','.',$sql_cart->result("NVALOCPAGA"));
      $copartsetor += str_replace(',','.',$sql_cart->result("NVALOCPAGA"));

      $sql_cart->next();
      
      if (($calcula_custo_op == "2") and (($id_setor <> $sql_cart->result("NNUMESETOR")) or ($sql_cart->eof()))) {

        if (($copartsetor > 0) and ($id_setor >= 0)) {
          $buffer = str_repeat(' ',67).
                    'Total da Loca��o: '.
                    $formata->acrescentaBrancos($formata->formataNumero($copartsetor,2),10,'E',' ');
          fwrite($ponteiro,$buffer.chr(13).chr(10)); 
          fwrite($ponteiro,chr(13).chr(10)); 
        }
      }	      

      if (($empresa <> $sql_cart->result("CRAZAEMPR")) or ($sql_cart->eof())) {
        fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
        $buffer = str_repeat(' ',68).
                  'Total da empresa:'.
                  $formata->acrescentaBrancos($formata->formataNumero($totalempr,2),10,'E',' ');
        fwrite($ponteiro,$buffer.chr(13).chr(10)); 
        fwrite($ponteiro,chr(13).chr(10)); 
      }
    }
    
    fwrite($ponteiro,chr(13).chr(10)); 
    fwrite($ponteiro,str_repeat('_',180).chr(13).chr(10)); 
    $buffer = str_repeat(' ',79).
              'Total:'.
              $formata->acrescentaBrancos($formata->formataNumero($total,2),10,'E',' ');
    fwrite($ponteiro,$buffer.chr(13).chr(10)); 
  
    fclose ($ponteiro); 
  } else
    $arquivo_carteiras = '';    
  
  $arquivo_reembolso = 'Reembolsos_'.$congenere.'_Import_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_reembolso, "w");

  $sql_reem = new Query();
  $txt_reem = " SELECT CRAZAEMPR,USUA.NTITUUSUA,USUA.CCODIUSUA,DATENCONT,CNOMEPRES,
                       USUA.CNOMEUSUA,TITU.CNOMEUSUA TITULAR,HSSCONT.NNUMECONT,
                       'C' TIPO,USUA.CGRAUUSUA,
                       SUM(NVALOPACON) NVALOPACON,SUM(NCONTPACON) NCONTPACON,SUM(NTAXAPACON) NTAXAPACON,
                       SUM(NPIS_PACON) NPIS_PACON,SUM(NCOFIPACON) NCOFIPACON,SUM(NCPMFPACON) NCPMFPACON,
                       SUM(NINSSPACON) NINSSPACON,SUM(NISS_PACON) NISS_PACON
                  FROM HSSPACON,HSSCONT,HSSLOTE,FINPRES,HSSUSUA USUA,HSSUSUA TITU,HSSTITU,HSSEMPR
                 WHERE HSSPACON.NNUMEPAGA IN (".$ids.")
                   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                   AND HSSCONT.NNUMELOTE = HSSLOTE.NNUMELOTE
                   AND CGPAGLOTE = 'R' 
                   AND HSSCONT.NNUMEPRES  = FINPRES.NNUMEPRES
                   AND HSSCONT.NNUMEUSUA  = USUA.NNUMEUSUA
                   AND USUA.NTITUUSUA     = TITU.NNUMEUSUA
                   AND USUA.NNUMETITU     = HSSTITU.NNUMETITU
                   AND HSSTITU.NNUMEEMPR  = HSSEMPR.NNUMEEMPR
                 GROUP BY CRAZAEMPR,TITU.CNOMEUSUA,USUA.CGRAUUSUA,USUA.NTITUUSUA,USUA.CNOMEUSUA,USUA.CCODIUSUA,DATENCONT,CNOMEPRES,HSSCONT.NNUMECONT
                 ORDER BY 1,7,10,6 DESC";
                   
  $sql_reem->executeQuery($txt_reem);
  
  if ($sql_reem->count() > 0) {
    fwrite($ponteiro,$_SESSION['nome_operadora']." ANS N� ".$_SESSION['registro_ans'].chr(13).chr(10));                   
    fwrite($ponteiro,"FATURA PLANO EMPRESA - ".$codigo." - ".$razao_social.chr(13).chr(10));                   
    fwrite($ponteiro,"REEMBOLSOS COBRADOS NA FATURA");
    fwrite($ponteiro,"Essas contas j� est�o somadas na descricao de despesas anteriores, estao novamente listadas aqui apenas para demonstracao");
    fwrite($ponteiro,"Vencimento : ".$vencimento.chr(13).chr(10));                   
    fwrite($ponteiro,"Codigo       Titular                       Paciente                      Conta    Data  Prestador do servico   Valor      INSS      PIS      COFINS    ISS       CPMF     Total".chr(13).chr(10));
    fwrite($ponteiro,str_repeat("_",180).chr(13).chr(10));  
  
    //Codigo       Titular                       Paciente                      Conta    Data  Prestador do servico   Valor      INSS      PIS      COFINS    ISS       CPMF     Total
    //123456789012 12345678901234567890123456789 12345678901234567890123456789 12345678 12345 1234567890123456789012 1234567890 123456789 12345678 123456789 123456789 12345678 1234567890
    
    fwrite($ponteiro,chr(13).chr(10));  
    
    while (!$sql_reem->eof()) {
      if ($empresa <> $sql_reem->result("CRAZAEMPR")) {
        $empresa = $sql_reem->result("CRAZAEMPR");

        $inss_empr = 0;
        $vl_contas_empr = 0;
        $despesas_empr = 0;
        $pis_empr = 0;
        $cofins_empr = 0;
        $cpmf_empr = 0;
        $iss_empr = 0;

        fwrite($ponteiro,chr(13).chr(10));  
        fwrite($ponteiro,"Empresa: ".$empresa.chr(13).chr(10));  
        fwrite($ponteiro,chr(13).chr(10));  
      }
    
      $buffer = $formata->acrescentaBrancos(substr($sql_reem->result("CCODIUSUA"),0,12),12,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_reem->result("TITULAR"),0,29),29,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_reem->result("CNOMEUSUA"),0,29),29,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_reem->result("NNUMECONT"),0,8),8,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_reem->result("DATENCONT"),0,5),5,'D',' ').' '.
                $formata->acrescentaBrancos(substr($sql_reem->result("CNOMEPRES"),0,22),22,'D',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NCONTPACON"),2),10,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NINSSPACON"),2),9,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NPIS_PACON"),2),8,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NCOFIPACON"),2),9,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NISS_PACON"),2),9,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NCPMFPACON"),2),8,'E',' ').' '.
                $formata->acrescentaBrancos($formata->formataNumero($sql_reem->result("NVALOPACON"),2),10,'E',' ');
      fwrite($ponteiro,$buffer.chr(13).chr(10));  
        
      $iss += str_replace(',','.',$sql_reem->result("NISS_PACON"));
      $inss += str_replace(',','.',$sql_reem->result("NINSSPACON"));
      $pis += str_replace(',','.',$sql_reem->result("NPIS_PACON"));
      $cofins += str_replace(',','.',$sql_reem->result("NCOFIPACON"));
      $cpmf += str_replace(',','.',$sql_reem->result("NCPMFPACON"));
      $vl_contas += str_replace(',','.',$sql_reem->result("NCONTPACON"));
      $despesas += str_replace(',','.',$sql_reem->result("NVALOPACON"));

      $iss_titular += str_replace(',','.',$sql_reem->result("NISS_PACON"));
      $inss_titular += str_replace(',','.',$sql_reem->result("NINSSPACON"));
      $pis_titular += str_replace(',','.',$sql_reem->result("NPIS_PACON"));
      $cofins_titular += str_replace(',','.',$sql_reem->result("NCOFIPACON"));
      $cpmf_titular += str_replace(',','.',$sql_reem->result("NCPMFPACON"));
      $vl_contas_titular += str_replace(',','.',$sql_reem->result("NCONTPACON"));
      $despesas_titular += str_replace(',','.',$sql_reem->result("NVALOPACON"));

      $iss_empr += str_replace(',','.',$sql_reem->result("NISS_PACON"));
      $inss_empr += str_replace(',','.',$sql_reem->result("NINSSPACON"));
      $pis_empr += str_replace(',','.',$sql_reem->result("NPIS_PACON"));
      $cofins_empr += str_replace(',','.',$sql_reem->result("NCOFIPACON"));
      $cpmf_empr += str_replace(',','.',$sql_reem->result("NCPMFPACON"));
      $vl_contas_empr += str_replace(',','.',$sql_reem->result("NCONTPACON"));
      $despesas_empr += str_replace(',','.',$sql_reem->result("NVALOPACON"));
        
      $sql_reem->next();
      
      if (($sql_reem->result("NTITUUSUA") <> $titular) or ($sql_reem->eof())) {
        $titular = $sql_reem->result("NTITUUSUA");
        $buffer = str_repeat(' ',73).
                  '                    Total da familia: '.
                  $formata->acrescentaBrancos($formata->formataNumero($vl_contas_titular,2),10,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($inss_titular,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($pis_titular,2),8,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($cofins_titular,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($iss_titular,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($cpmf_titular,2),8,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($despesas_titular,2),10,'E',' ');                  
        fwrite($ponteiro,$buffer.chr(13).chr(10));  
 
        $despesas_titular = 0;
        $inss_titular = 0;
        $iss_titular = 0;
        $vl_contas_titular = 0;
        $pis_titular = 0;
        $cofins_titular = 0;
        $cpmf_titular = 0;
      }
      
      if (($sql_reem->result("CRAZAEMPR") <> $empresa) or ($sql_reem->eof())) {
        $titular = $sql_reem->result("NTITUUSUA");

        fwrite($ponteiro,chr(13).chr(10));  
        
        $buffer = str_repeat(' ',73).
                  '      Total de reembolsos da $empresa: '.
                  $formata->acrescentaBrancos($formata->formataNumero($vl_contas_empr,2),10,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($inss_empr,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($pis_empr,2),8,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($cofins_empr,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($iss_empr,2),9,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($cpmf_empr,2),8,'E',' ').' '.
                  $formata->acrescentaBrancos($formata->formataNumero($despesas_empr,2),10,'E',' ');                  
        fwrite($ponteiro,$buffer.chr(13).chr(10));  

        $despesas_empr = 0;
        $inss_empr = 0;
        $iss_empr = 0;
        $vl_contas_empr = 0;
        $pis_empr = 0;
        $cofins_empr = 0;
        $cpmf_empr = 0;
      }
    }
    
    fwrite($ponteiro,chr(13).chr(10));
    fwrite($ponteiro,str_repeat(' ',180).chr(13).chr(10));

    $buffer = str_repeat(' ',73).
              '           Total geral de reembolsos: '.
              $formata->acrescentaBrancos($formata->formataNumero($vl_contas,2),10,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($inss,2),9,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($pis,2),8,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($cofins,2),9,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($iss,2),9,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($cpmf,2),8,'E',' ').' '.
              $formata->acrescentaBrancos($formata->formataNumero($despesas,2),10,'E',' ');                
              
    fwrite($ponteiro,$buffer.chr(13).chr(10));
    fwrite($ponteiro,str_repeat(' ',180).chr(13).chr(10));  
  } else
    $arquivo_reembolso = '';
    
  fclose ($ponteiro); 
  
  $arquivo_utilizacao = 'Utilizacao_'.$congenere.'_Import_'.substr($vencimento,6,4).substr($vencimento,3,2).'.txt';
  $ponteiro = fopen ("../temp/".$arquivo_utilizacao, "w");

  $sql_util = new Query();
  
  if ($id_congenere == 2054705575) { // Grupo torres  
    $txt_util = " SELECT USUA.CCODIUSUA,USUA.CNOMEUSUA,TITU.CCODIUSUA CODIGO_TITULAR,NVL(CCGC_SETOR,C_CGCEMPR) C_CGCEMPR,
                         HSSCONT.NNUMECONT,TO_CHAR(DATENCONT,'DD/MM/YYYY') DATENCONT,FINPRES.CNOMEPRES,TITU.CNOMEUSUA TITULAR,
                         TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) + TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT) VALOR,
                         NINSSPACON,NPIS_PACON,NCOFIPACON,NISS_PACON,NCPMFPACON,VALOR_TOTAL_FATURADO(HSSPAGA.NNUMEPAGA,HSSCONT.NNUMEUSUA,3,HSSCONT.NNUMECONT) NVALOPACON,CAMBUCONT,LOCAL.CGRUPPRES,LOCAL.CREDEPRES
                    FROM HSSTITU,HSSEMPR,HSSPAGA,HSSPARCE,HSSPACON,HSSCONT,HSSUSUA USUA,HSSUSUA TITU,FINPRES,HSSSETOR,FINPRES LOCAL
                   WHERE HSSTITU.NNUMETITU = :contrato
                     AND HSSTITU.NNUMETITU = HSSPAGA.NNUMETITU
                     AND TO_CHAR(DVENCPAGA,'MM/YYYY') = :mes
                     AND HSSPAGA.NNUMEPAGA = HSSPARCE.NDESTPAGA(+)
                     AND (HSSPARCE.NNUMEPAGA IS NULL OR HSSPARCE.NNUMEPAGA = HSSPARCE.NDESTPAGA)
                     AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
                     AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
                     AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                     AND HSSCONT.NNUMEUSUA = USUA.NNUMEUSUA
                     AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                     AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
                     AND TITU.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
                     AND HSSCONT.NLOCAPRES = LOCAL.NNUMEPRES      
                   UNION ALL
                  SELECT USUA.CCODIUSUA,USUA.CNOMEUSUA,TITU.CCODIUSUA CODIGO_TITULAR,NVL(CCGC_SETOR,C_CGCEMPR) C_CGCEMPR,
                         HSSGUIA.NNUMEGUIA,TO_CHAR(DEMISGUIA,'DD/MM/YYYY') DEMISGUIA,FINPRES.CNOMEPRES,TITU.CNOMEUSUA TITULAR,
                         TOTAL_GUIA(HSSGUIA.NNUMEGUIA) VALOR,
                         0 NINSSPACON,0 NPIS_PACON,0 NCOFIPACON,0 NISS_PACON,0 NCPMFPACON,VALOR_TOTAL_FATURADO(HSSPAGA.NNUMEPAGA,HSSGUIA.NNUMEUSUA,2,HSSGUIA.NNUMEGUIA) NVALOGPAGA,
                         DECODE(CTIPOGUIA,'I','I','C','I','O','I','R','I','P','I','S','I','A') CAMBUCONT,LOCAL.CGRUPPRES,LOCAL.CREDEPRES
                    FROM HSSTITU,HSSEMPR,HSSPAGA,HSSPARCE,HSSGPAGA,HSSGUIA,HSSUSUA USUA,HSSUSUA TITU,FINPRES,HSSSETOR,FINPRES LOCAL
                   WHERE HSSTITU.NNUMETITU = :contrato
                     AND HSSTITU.NNUMETITU = HSSPAGA.NNUMETITU
                     AND TO_CHAR(DVENCPAGA,'MM/YYYY') = :mes
                     AND HSSPAGA.NNUMEPAGA = HSSPARCE.NDESTPAGA(+)
                     AND (HSSPARCE.NNUMEPAGA IS NULL OR HSSPARCE.NNUMEPAGA = HSSPARCE.NDESTPAGA)
                     AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
                     AND HSSPAGA.NNUMEPAGA = HSSGPAGA.NNUMEPAGA
                     AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                     AND HSSGUIA.NNUMEUSUA = USUA.NNUMEUSUA
                     AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                     AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES
                     AND TITU.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
                     AND HSSGUIA.NLOCAPRES = LOCAL.NNUMEPRES      
                  ORDER BY 4,8,5";
  } else if ($calcula_custo_op == '2') {
    $txt_util = " SELECT USUA.CCODIUSUA,USUA.CNOMEUSUA,TITU.CCODIUSUA CODIGO_TITULAR,NVL(CCGC_SETOR,C_CGCEMPR) C_CGCEMPR,
                         HSSCONT.NNUMECONT,TO_CHAR(DATENCONT,'DD/MM/YYYY') DATENCONT,FINPRES.CNOMEPRES,TITU.CNOMEUSUA TITULAR,
                         TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) + TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT) VALOR,
                         NINSSPACON,NPIS_PACON,NCOFIPACON,NISS_PACON,NCPMFPACON,VALOR_TOTAL_FATURADO(HSSPAGA.NNUMEPAGA,HSSCONT.NNUMEUSUA,3,HSSCONT.NNUMECONT) NVALOPACON,CAMBUCONT,LOCAL.CGRUPPRES,LOCAL.CREDEPRES
                    FROM HSSTITU,HSSEMPR,HSSPAGA,HSSPARCE,HSSPACON,HSSCONT,HSSUSUA USUA,HSSUSUA TITU,FINPRES,HSSSETOR,FINPRES LOCAL
                   WHERE HSSTITU.NNUMETITU = :contrato
                     AND HSSTITU.NNUMETITU = HSSPAGA.NNUMETITU
                     AND TO_CHAR(DVENCPAGA,'MM/YYYY') = :mes
                     AND HSSPAGA.NNUMEPAGA = HSSPARCE.NDESTPAGA(+)
                     AND (HSSPARCE.NNUMEPAGA IS NULL OR HSSPARCE.NNUMEPAGA = HSSPARCE.NDESTPAGA)
                     AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
                     AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
                     AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                     AND HSSCONT.NNUMEUSUA = USUA.NNUMEUSUA
                     AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                     AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
                     AND TITU.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
                     AND HSSCONT.NLOCAPRES = LOCAL.NNUMEPRES
                  ORDER BY 4,TITU.CNOMEUSUA,HSSCONT.NNUMECONT";  
  } else {
    $txt_util = " SELECT USUA.CCODIUSUA,USUA.CNOMEUSUA,TITU.CCODIUSUA CODIGO_TITULAR,C_CGCEMPR,
                         HSSCONT.NNUMECONT,TO_CHAR(DATENCONT,'DD/MM/YYYY') DATENCONT,FINPRES.CNOMEPRES,TITU.CNOMEUSUA TITULAR,
                         TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) + TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT) VALOR,
                         NINSSPACON,NPIS_PACON,NCOFIPACON,NISS_PACON,NCPMFPACON,VALOR_TOTAL_FATURADO(HSSPAGA.NNUMEPAGA,HSSCONT.NNUMEUSUA,3,HSSCONT.NNUMECONT) NVALOPACON,CAMBUCONT,LOCAL.CGRUPPRES,LOCAL.CREDEPRES
                    FROM HSSTITU,HSSPAGA,HSSPARCE,HSSPACON,HSSCONT,HSSUSUA USUA,HSSUSUA TITU,FINPRES,HSSTITU TITU2,HSSEMPR,FINPRES LOCAL
                   WHERE HSSTITU.NNUMETITU = :contrato
                     AND HSSTITU.NNUMETITU = HSSPAGA.NNUMETITU
                     AND TO_CHAR(DVENCPAGA,'MM/YYYY') = :mes
                     AND HSSPAGA.NNUMEPAGA = HSSPARCE.NDESTPAGA(+)
                     AND (HSSPARCE.NNUMEPAGA IS NULL OR HSSPARCE.NNUMEPAGA = HSSPARCE.NDESTPAGA)
                     AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
                     AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                     AND HSSCONT.NNUMEUSUA = USUA.NNUMEUSUA
                     AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                     AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
                     AND USUA.NNUMETITU = TITU2.NNUMETITU
                     AND TITU2.NNUMEEMPR = HSSEMPR.NNUMEEMPR
                     AND HSSCONT.NLOCAPRES = LOCAL.NNUMEPRES
                  ORDER BY C_CGCEMPR,TITU.CNOMEUSUA,HSSCONT.NNUMECONT";  
  }

  $sql_util->addParam(":contrato",$id_contrato);
  $sql_util->addParam(":mes",substr($vencimento,3,7));
  $sql_util->executeQuery($txt_util);  
  
  while (!$sql_util->eof()) {

    if ((str_replace(',','.',$sql_util->result("NPIS_PACON")) + 
         str_replace(',','.',$sql_util->result("NINSSPACON")) +
         str_replace(',','.',$sql_util->result("NCOFIPACON")) + 
         str_replace(',','.',$sql_util->result("NISS_PACON")) +
         str_replace(',','.',$sql_util->result("NCPMFPACON"))) > 0)
      // Quando um dos campos de impostos estiver preenchido significa que a mensalidade � da astromig,
      // entao nao tem coparticipacao
      $coparticipacao = 0;
    else
      $coparticipacao = $sql_util->result("NVALOPACON");

    if ($_SESSION['apelido_operadora'] == 'promedmg')
      $buffer = '1'.$formata->acrescentaZeros(substr($formata->somenteNumeros($sql_util->result("CCODIUSUA")),0,9),9).
                $formata->acrescentaZeros(substr($formata->somenteNumeros($sql_util->result("CODIGO_TITULAR")),0,9),9);
    else
      $buffer = '1'.$formata->acrescentaZeros(substr($sql_util->result("CCODIUSUA"),0,6),6).
                $formata->acrescentaZeros(substr($sql_util->result("CODIGO_TITULAR"),0,6),6);

    $buffer .= substr($formata->acrescentaBrancos($sql_util->result("TITULAR"),40,'D',' '),0,40).
               substr($formata->acrescentaBrancos($sql_util->result("CNOMEUSUA"),40,'D',' '),0,40).
               $formata->acrescentaZeros($formata->somenteNumeros($sql_util->result("C_CGCEMPR")),14).
               $formata->acrescentaZeros($sql_util->result("NNUMECONT"),7).
               substr($sql_util->result("DATENCONT"),0,10).
               substr($formata->acrescentaBrancos($sql_util->result("CNOMEPRES"),25,'D',' '),0,25).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("VALOR"),2)),10).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("NINSSPACON"),2)),10).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("NPIS_PACON"),2)),10).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("NCOFIPACON"),2)),10).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("NISS_PACON"),2)),10).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("NCPMFPACON"),2)),10).
               $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_util->result("NVALOPACON"),2)),10);
    fwrite($ponteiro,$buffer.chr(13).chr(10));

    $sql_proc = new Query();
    $txt_proc = " SELECT HSSPCON.CCODIPMED,CNOMEPMED,CNOMEPRES,CTIPOPMED,CPARTPMED,NFRANPCON,
                         NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) +
                         NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + NVL(NINSTHONO,0)+NVL(NCUOPPCON,0)+NVL(NVFILPCON,0) VALOR
                    FROM HSSPCON,HSSPMED,FINPRES
                   WHERE HSSPCON.NNUMECONT = :conta
                     AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                     AND NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) +
                         NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + NVL(NINSTHONO,0)+NVL(NCUOPPCON,0)+NVL(NVFILPCON,0) > 0
                     AND HSSPCON.NNUMEPRES = FINPRES.NNUMEPRES
                  UNION ALL
                  SELECT 'TAXA',CDESCTAXA,CNOMEPRES,NULL,NULL,NFRANTCON,ROUND(NQUANTCON * NVALOTCON,2)
                    FROM HSSCONT,HSSTCON,HSSTAXA,FINPRES
                   WHERE HSSCONT.NNUMECONT = :conta
                     AND HSSCONT.NNUMECONT = HSSTCON.NNUMECONT
                     AND ROUND(NQUANTCON * NVALOTCON,2) > 0
                     AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                     AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
                  UNION ALL
                  SELECT 'MAT/MED','MATERIAIS E MEDICAMENTOS',CNOMEPRES,NULL,NULL,VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT),
                         NVL(NMAQPCONT,0) + NVL(NMACPCONT,0) + NVL(NMDQPCONT,0) +
                         NVL(NMDCPCONT,0) + NVL(NMTOUCONT,0) + NVL(NMDOUCONT,0)
                    FROM HSSCONT,FINPRES
                   WHERE NNUMECONT = :conta
                     AND NVL(NMAQPCONT,0) + NVL(NMACPCONT,0) + NVL(NMDQPCONT,0) +
                         NVL(NMDCPCONT,0) + NVL(NMTOUCONT,0) + NVL(NMDOUCONT,0) > 0
                     AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES";
    $sql_proc->addParam(":conta",$sql_util->result("NNUMECONT"));
    $sql_proc->executeQuery($txt_proc);    
    
    while (!$sql_proc->eof()) {

      if ($sql_util->result("CGRUPPRES") == 'K')
        $tipo = '008';
      else if ($sql_proc->result("CPARTPMED") <> '')
        $tipo = '007';
      else if ($sql_util->result("CAMBUCONT") == 'I')
        $tipo = '006';
      else if ($sql_proc->result("CTIPOPMED") == 'C') {
        if ($sql_util->result("CREDEPRES") == 'S')
          $tipo = '003';
        else if (($sql_proc->result("CCODIPMED") == '00017777') or ($sql_proc->result("CCODIPMED") == '00018888'))
          $tipo = '003';
        else
          $tipo = '004';
      } else if (($sql_proc->result("CTIPOPMED") == 'N') or
                 ($sql_proc->result("CTIPOPMED") == 'H') or
                 ($sql_proc->result("CTIPOPMED") == 'R') or
                 ($sql_proc->result("CTIPOPMED") == 'O') or
                 ($sql_proc->result("CTIPOPMED") == 'E') or
                 ($sql_proc->result("CTIPOPMED") == 'K') or
                 ($sql_proc->result("CTIPOPMED") == 'V') or
                 ($sql_proc->result("CTIPOPMED") == 'X') or
                 ($sql_proc->result("CTIPOPMED") == 'Y') or
                 ($sql_proc->result("CTIPOPMED") == '*') or
                 ($sql_proc->result("CTIPOPMED") == 'Z') or
                 ($sql_proc->result("CTIPOPMED") == '&') or
                 ($sql_proc->result("CTIPOPMED") == '%') or
                 ($sql_proc->result("CTIPOPMED") == '@') or
                 ($sql_proc->result("CTIPOPMED") == 'B') or
                 ($sql_proc->result("CTIPOPMED") == '$') or
                 ($sql_proc->result("CTIPOPMED") == 'U') or
                 ($sql_proc->result("CTIPOPMED") == '#')) {
        if ($sql_proc->result("VALOR") > 100)
          $tipo = '002';
        else
          $tipo = '001';
      } else
        $tipo = '005';

      if ($_SESSION['apelido_operadora'] == 'promedmg')
        $buffer = '2'.$formata->acrescentaZeros(substr($formata->somenteNumeros($sql_util->result("CCODIUSUA")),0,9),9).
                  $formata->acrescentaZeros(substr($formata->somenteNumeros($sql_util->result("CODIGO_TITULAR")),0,9),9);
      else
        $buffer = '2'.$formata->acrescentaZeros(substr($sql_util->result("CCODIUSUA"),0,6),6).
                  $formata->acrescentaZeros(substr($sql_util->result("CODIGO_TITULAR"),0,6),6);
                
      $buffer .= substr($formata->acrescentaBrancos($sql_util->result("TITULAR"),40,'D',' '),0,40).
                 substr($formata->acrescentaBrancos($sql_util->result("CNOMEUSUA"),40,'D',' '),0,40).
                 $formata->acrescentaZeros($formata->somenteNumeros($sql_util->result("C_CGCEMPR")),14).
                 $formata->acrescentaZeros($sql_util->result("NNUMECONT"),7).
                 substr($sql_util->result("DATENCONT"),0,10).
                 substr($formata->acrescentaBrancos($sql_proc->result("CNOMEPRES"),25,'D',' '),0,25).
                 substr($formata->acrescentaBrancos($sql_proc->result("CCODIPMED"),8,'D',' '),0,8).
                 substr($formata->acrescentaBrancos($sql_proc->result("CNOMEPMED"),37,'D',' '),0,37).
                 $formata->acrescentaZeros($tipo,15).
                 $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_proc->result("NFRANPCON"),2)),10).
                 $formata->acrescentaZeros($formata->somenteNumeros($formata->formataNumero($sql_proc->result("VALOR"),2)),10);
      fwrite($ponteiro,$buffer.chr(13).chr(10));
                    
      $sql_proc->next();
    }

    $sql_util->next();
  }

  fclose ($ponteiro); 
  
  if (extension_loaded('zip')) {  
    $zip = new ZipArchive();
    
    $arquivo = str_replace(' ','_',$_SESSION['titular_contrato'])."_".date('Ymd');
    $arquivo = str_replace('/','',$arquivo);
 
    if (file_exists("../temp/".$arquivo.".zip"))
      unlink("../temp/".$arquivo.".zip");
      
    $zip->open("../temp/".$arquivo.".zip", ZipArchive::CREATE);
        
    $zip->addfile("../temp/".$arquivo_populacao,$arquivo_populacao);      
    $zip->addfile("../temp/".$arquivo_taxa_adm,$arquivo_taxa_adm);
    $zip->addfile("../temp/".$arquivo_custo_operacional,$arquivo_custo_operacional);
    $zip->addfile("../temp/".$arquivo_copart_dep,$arquivo_copart_dep);
    
    if ($arquivo_copart_canc <> '')
      $zip->addfile("../temp/".$arquivo_copart_canc,$arquivo_copart_canc);
      
    if ($arquivo_reembolso <> '')
      $zip->addfile("../temp/".$arquivo_reembolso,$arquivo_reembolso);

    $zip->addfile("../temp/".$arquivo_utilizacao,$arquivo_utilizacao);
      
    if ($arquivo_carteiras <> '')
      $zip->addfile("../temp/".$arquivo_carteiras,$arquivo_carteiras);
      
    $zip->close();
       
    $util->redireciona('../temp/'.$arquivo.'.zip','N');  
    
  } else {
    echo "Erro ao gerar arquivo. Entre em contato com a Operadora. (Erro: php_zip)";
  }

  $bd->close();
?>