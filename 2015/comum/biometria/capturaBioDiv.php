<?php

  /**** 
  
  Pela web optamos por cadastrar somente um unico dedo, isso porque no prestador o mesmo poder� cadastrar
  o dedo do paciente correto e um outro dedo de outra pessoa.
  
  ****/
  
  Session_start();
  
  //require_once('../sessao.php');
  require_once("../autoload.php");

  $bd = new Oracle();
  
  $status         = '';  
  $idBeneficiario = $_REQUEST["id_usuario"];
    
  $sql = new Query($bd);    
  $txt = " SELECT CDPOLUSUA,CDINDUSUA,CDMEDUSUA,CDANEUSUA,CDMINUSUA,
                  CDPO2USUA,CDIN2USUA,CDME2USUA,CDAN2USUA,CDMI2USUA,CCODIUSUA
             FROM HSSUSUA
            WHERE LOWER(MD5(NNUMEUSUA)) = :usuario";   
  $sql->addParam(":usuario",$idBeneficiario);
  $retorno = $sql->executeQuery($txt);    
     
  if (($sql->result("CDPOLUSUA") == '') and ($sql->result("CDINDUSUA") == '') and ($sql->result("CDMEDUSUA") == '') and 
     ($sql->result("CDANEUSUA") == '') and ($sql->result("CDMINUSUA") == '') and ($sql->result("CDPO2USUA") == '') and 
     ($sql->result("CDIN2USUA") == '') and ($sql->result("CDME2USUA") == '') and ($sql->result("CDAN2USUA") == '') and 
     ($sql->result("CDMI2USUA") == '')) {      
    
    $template      = '<param name="template1" id="" value="" />';
    $naoLocalizada = '<tr>
                        <td></td>
                        <td><input type="button" name="gravar" id="gravar" value="Salvar digital" class="botao_desabilitado" style="font-weight:normal;width:120px;"/></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><input type="button" name="limpar" id="limpar" value="Limpar digital" class="botao_desabilitado" style="font-weight:normal;width:120px;"/></td>
                      </tr>';
    /*
    $naoLocalizada = '<tr>
                        <td class="font2">M�o:</td>
                        <td>
                          <select name="select" id="mao" class="c1">
                            <option value="">Selecione</option>
                            <option value="D">Direita</option>
                            <option value="E">Esquerda</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td class="font2">Dedo:</td>
                        <td>
                          <select name="select2" id="dedo" class="c1">
                            <option value="P" >Polegar</option>
                            <option value="I" >Indicador</option>
                            <option value="M" >M�dio</option>
                            <option value="A" >Anular</option>
                            <option value="N" >M�nimo</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><input type="button" value="Capturar" id="capturar" name="capturar" class="c2" /></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><input type="button" value="Parar captura" id="pararBio" name="pararBio" class="c2" /></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><input type="button" value="Limpar digital " id="limpar" name="limpar" class="c2" /></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><input type="button" name="gravar" id="gravar" value=" Salvar digital" class="c2" /></td>
                      </tr>';
    */
    
    $divDedos      = "<div id='SD' style='z-index:1;position:absolute;left:511px;top:20px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
    
    $tem_bio       = "<param name='onInit'    value='setAppletColor(\"#8080FF\"); statusBio(8);' />
                      <param name='onStart'   value='VrBioApplet.applet.startCapture();' />
                      <param name='onPlaced'  value='setAppletColor(\"#FFFF00\");' />
                      <param name='onRemoved' value='setAppletColor(\"#00FF00\"); statusBio(1);' />";
  } 
  else {
    $dedos = array();
    
    if($sql->result("CDPOLUSUA")!='')
      $dedos['MD_P'] = $sql->result("CDPOLUSUA");   
  
    if($sql->result("CDINDUSUA")!='')
      $dedos['MD_I'] = $sql->result("CDINDUSUA");
    
    if($sql->result("CDMEDUSUA")!='')
      $dedos['MD_M'] = $sql->result("CDMEDUSUA"); 
    
    if($sql->result("CDANEUSUA")!='')
      $dedos['MD_A'] = $sql->result("CDANEUSUA"); 
    
    if($sql->result("CDMINUSUA")!='')
      $dedos['MD_N'] = $sql->result("CDMINUSUA");
    
    if($sql->result("CDPO2USUA")!='')
      $dedos['ME_P'] = $sql->result("CDPO2USUA");
    
    if($sql->result("CDIN2USUA")!='')
      $dedos['ME_I'] = $sql->result("CDIN2USUA");
    
    if($sql->result("CDME2USUA")!='')
      $dedos['ME_M'] = $sql->result("CDME2USUA");
    
    if($sql->result("CDAN2USUA")!='')
      $dedos['ME_A'] = $sql->result("CDAN2USUA");
    
    if($sql->result("CDMI2USUA")!='')
      $dedos['ME_N'] = $sql->result("CDMI2USUA");  
    
    $i        = 1;
    $direita  = 1;
    $esquerda = 1;
    $template = '';
    
    foreach ($dedos as $dedo=>$value) {
      $template .= '<param name="template'.$i.'" id="'.$dedo.'" value="'.$value.'" />';
      $i++;  
      
      $divDedos = "";
      
      if ($dedo == 'ME_N')
        $divDedos .= "<div id='miD' style='z-index:1;position:absolute;left:361px;top:45px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
             
      if ($dedo == 'ME_A')      
        $divDedos .= "<div id='anD' style='z-index:1;position:absolute;left:383px;top:20px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'ME_M')      
        $divDedos .= "<div id='meD' style='z-index:1;position:absolute;left:405px;top:13px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'ME_I')      
        $divDedos .= "<div id='inD' style='z-index:1;position:absolute;left:431px;top:20px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'ME_P')      
        $divDedos .= "<div id='poD' style='z-index:1;position:absolute;left:464px;top:63px;'><img src='../comum/img/bverme.gif' alt='' /></div>";

      if ($dedo == 'MD_P')      
        $divDedos .= "<div id='poE' style='z-index:1;position:absolute;left:478px;top:63px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'MD_I')      
        $divDedos .= "<div id='inE' style='z-index:1;position:absolute;left:511px;top:20px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'MD_M')      
        $divDedos .= "<div id='meE' style='z-index:1;position:absolute;left:535px;top:13px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'MD_A')      
        $divDedos .= "<div id='anE' style='z-index:1;position:absolute;left:559px;top:20px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
        
      if ($dedo == 'MD_N')      
        $divDedos .= "<div id='miE' style='z-index:1;position:absolute;left:582px;top:45px;'><img src='../comum/img/bverme.gif' alt='' /></div>";
      
    }
    
    $tem_bio       = "<param name='onInit'    value='setAppletColor(\"#8080FF\"); statusBio(6);' />
                      <param name='onStart'   value='VrBioApplet.applet.startCapture();' />
                      <param name='onPlaced'  value='setAppletColor(\"#FFFF00\");statusBio(7);' />
                      <param name='onRemoved' value='setAppletColor(\"#FF0000\");' />";
    $naoLocalizada = "";
  }
  
  $html = "<script type='text/javascript'>                                
            
            var updateDebug = function(){
            
                  var str = VrBioApplet.applet.getDebug();
                  while(str.indexOf('\\n') >= 0){
                    str = str.replace('\\n','<br/>');
                    
                    if (str.indexOf('Identified') >= 0) {
                      $('#digitalconfere').val('S');
                      $('#digitalconfere').change();
                      VrBioApplet.applet.stopCapture();
                      $( '#dialog-form-biometria' ).dialog( 'close' );
                      break;
                    }
                  }
                } 
                
            var deviceEvent = function(event){
              var scanner = VrBioApplet.applet.getScannerName();
              alert('Device ' + scanner + ' ' + event);
            }

            var setAppletColor = function(color){
              VrBioApplet.applet.style.borderColor = color;
            }
    
            $().ready(function(){ 
                              
              $('#mao').change(function(){
                $('#dedo').change();
              });

              $('#dedo').change(function(){
              
                if($('#mao').val() == 'E'){                                
                  if($(this).val() == 'N')
                    $('#SD').attr('style','z-index:1;position:absolute;left:361px;top:45px;');
                  else if($(this).val() == 'A')
                    $('#SD').attr('style','z-index:1;position:absolute;left:383px;top:20px;');
                  else if($(this).val() == 'M')
                    $('#SD').attr('style','z-index:1;position:absolute;left:405px;top:13px;');
                  else if($(this).val() == 'I')
                    $('#SD').attr('style','z-index:1;position:absolute;left:431px;top:20px;');
                  else if($(this).val() == 'P')
                    $('#SD').attr('style','z-index:1;position:absolute;left:464px;top:63px;');
                }
                else {
                  if($(this).val() == 'P')
                    $('#SD').attr('style','z-index:1;position:absolute;left:478px;top:63px;');
                  else if($(this).val() == 'I')
                    $('#SD').attr('style','z-index:1;position:absolute;left:511px;top:20px;');
                  else if($(this).val() == 'M')
                    $('#SD').attr('style','z-index:1;position:absolute;left:535px;top:13px;');
                  else if($(this).val() == 'A')
                    $('#SD').attr('style','z-index:1;position:absolute;left:559px;top:20px;');
                  else if($(this).val() == 'N')
                    $('#SD').attr('style','z-index:1;position:absolute;left:582px;top:45px;');
                }   
              });

              $('#capturar').click(function(){
                VrBioApplet.applet.startCapture();
                statusBio(8);
              });
              
              $('#gravar').click(function(){
                gravaDigital();
              });              

              $('#pararBio').click(function(){
                VrBioApplet.applet.stopCapture();
              }); 

              $('#limpar').click(function(){
                setAppletColor(\"#8080FF\");
                VrBioApplet.reset();
              });               
              
              $('#dedo').change();                        
            });
                        
            function statusBio(status){
              if(status == 0){
                $('#msg_digital').attr('class','font1');
                $('#msg_digital').html('Clique no bot�o \"Capturar\" para ler a digital.');
              }
              else if(status == 1){
                $('#msg_digital').attr('class','font1');
                $('#msg_digital').html('Clique em Salvar digital.');
                $('#gravar').attr('class','botao');
                $('#limpar').attr('class','botao');
              }
              else if(status == 3){
                $('#msg_digital').attr('class','sucesso1');
                $('#msg_digital').html('Digital salva com sucesso.');
              }
              else if(status == 4){
                $('#msg_digital').attr('class','erro1');
                $('#msg_digital').html('Erro ao gravar digital, tente novamente.');               
              }
              else if(status == 5){
                $('#msg_digital').attr('class','erro1');
                $('#msg_digital').html('Fa�a a leitura da digital antes de gravar.');             
              }
              else if(status == 6){
                $('#msg_digital').attr('class','font1');
                $('#msg_digital').html('Favor colocar dedo na leitora para captura da digital.');             
              }
              else if(status == 7){
                $('#msg_digital').attr('class','erro1');
                $('#msg_digital').html('Digital n�o encontrada. Tente novamente.');             
              }    
              else if(status == 8){
                $('#msg_digital').attr('class','font1');
                $('#msg_digital').html('Favor colocar o indicador direito na leitora para captura da digital.');             
              }              
            } 

            function gravaDigital(){
              if($('#digital').val() == ''){
                statusBio(5);
              }
              else{               
                $.post('../comum/biometria/gravaDigital.php',{
                    id_usuario : $('#numero_usuario').val(),
                    mao        : 'D',
                    dedo       : 'I',
                    digital    : $('#digital').val()
                },
              
                function(resposta){
                  if(resposta == 0){        
                    statusBio(3);
                    $('#digital').val('');
                    $('#digitalconfere').val('S');
                    $('#digitalconfere').change();
                    VrBioApplet.applet.stopCapture();
                    $( '#dialog-form-biometria' ).dialog( 'close' );                    
                  }
                  else{
                    statusBio(4);
                    alert(resposta);
                  }
                });
              }
            }
            
           </script>
  
           <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
              <td rowspan='2'>                           
                <applet id='BiometricApplet' name='BiometricApplet' 
                  code='veridis.biometric.samples.applet.AppletIdentificationSample' 
                  archive='../comum/biometria/veridisbiometric.jar'>
                  <!--        <param name='license' value='YOUR_LICENSE_HERE' /> -->
                  <param name='onCapture' value='VrBioApplet.onCapture();' />
                  <param name='onDebug' value='' />".
                  $tem_bio.                  
                 "<!-- 
                  Eventos in�teis de captura. Poderiam ser usados para criar uma UI simp�tica =)
                  <param name='onStart'          value='alert(\"Applet licenciado e iniciado\");' />
                  <param name='onPlug'           value='deviceEvent(\"Plugged\");' />
                  <param name='onUnplug'         value='deviceEvent(\"Unplugged\");' />
                  -->
                  
                  <param name='onBadVerify' value='updateDebug();' />
                  <param name='identifyTemplates' value='true' />
                  <param name='identifyThreshold' value='45' />".
                  
                  $template."
                  
                  <!--
                  PAR�METROS ADICIONAIS - Verifique documenta��o

                  <param name='onBadVerify' value='badTemplate();updateDebug();' />
                  <param name='verifyTemplateMinThreshold' value='45' />
                  <param name='verifyTemplates' value='true' />
                  <param name='template1' value='VALOR DO TEMPLATE 1 EM BASE64' />
                  <param name='template2' value='VALOR DO TEMPLATE 2 EM BASE64' />
                  -->
                </applet> 
              </td>
              <td valign='top'>
                <table width='135px' border='0' cellspacing='2' cellpadding='0'>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>".
                $naoLocalizada."
                </table>
              </td>
              <td>
                
                <div style='z-index:0'><img src='../comum/img/biometria/maos.jpg' alt='' width='241px' height='140' /></div>
                ".$divDedos."
              </td>
            </tr>
            <tr>
              <td colspan='2'>
                <table width='383px' border='0' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td colspan='2' align='center'></td>
                  </tr>
                  <tr>
                    <td width='50' height='30' align='right'><span class='font3'>Status</span>:</td>
                    <td width='300' height='30'><span id='msg_digital'></span></td>
                  </tr>
                  <tr>
                    <td height='30' align='right' id='debugBox'>&nbsp;</td>
                    <td height='30'>&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>";  
    
	echo json_encode(array('html'=>utf8_encode($html),
                         'status'=>$status));
    
  $bd->close(); 

?>