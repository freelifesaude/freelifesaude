<?php
  Session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");     
    
  if (isset($_POST['contrato'])) {
      $bd = new Oracle(); 
      $sql = new Query($bd);
	  
      $txt = " SELECT HSSTITU.NNUMETITU,CNOMEUSUA,NNUMEUSUA,HSSTITU.CSITUTITU FROM HSSTITU,HSSUSUA
                WHERE CCODITITU = :contrato
                  AND CTIPOUSUA IN ('T','F')
                  AND CSITUUSUA IN ('A','C','D')
                  AND CSITUTITU IN ('A','C','D')
                  AND HSSTITU.NNUMEEMPR IS NULL
                  AND HSSTITU.NNUMETITU = HSSUSUA.NNUMETITU ";              
      $sql->addParam(":contrato",$_POST['contrato']);                          
      $sql->executeQuery($txt);
	  
	  if($sql->count() > 0){
		  $retorno['contrato'] = $sql->result("NNUMETITU");
		  $retorno['id_titular'] = $sql->result("NNUMEUSUA");
		  $retorno['titular'] = $sql->result("CNOMEUSUA");
      $retorno['situacao'] = $sql->result("CSITUTITU");
	  }
	  else{
		  $retorno['contrato'] = 0;
		  $retorno['titular'] = '';		  
	  }
	  
	  echo json_encode($retorno);
      $bd->close();
  }
  
  
?>