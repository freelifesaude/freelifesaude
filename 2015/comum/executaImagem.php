<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();
        
  if (isset($_GET['pIdArquivo']))
    $pIdArquivo = $seg->antiInjection($_GET['pIdArquivo']);
  else
    $pIdArquivo = $seg->antiInjection($_POST['pIdArquivo']);
  
  $sql = new Query($bd);
  $txt = "SELECT HSSARQUI.CDESCARQUI,HSSARQNT.CARQUARQNT,HSSARQUI.CEXT_ARQUI EXTENSAO,
                 DECODE(HSSARQUI.CEXT_ARQUI,'PNG','S','JPG','S','JPEG','S','BMP','S','GIF','S','N') IMAGEM
            FROM HSSARQUI,HSSARQNT
           WHERE HSSARQUI.NNUMEARQUI = HSSARQNT.NNUMEARQUI
             AND HSSARQUI.NNUMEARQUI = :idArquivo";   
  $sql->addParam(":idArquivo",$pIdArquivo);
  $sql->executeQuery($txt);
    
  $img = $sql->result("CARQUARQNT");
 
  if ($sql->result("IMAGEM") == 'S')
    header('Content-Type: image/jpeg');
  else if ($sql->result("EXTENSAO") == 'PDF')
    header('Content-Type: application/pdf');
  else
    header('Content-Disposition: attachment; filename="'.$sql->result("CDESCARQUI").".".strtolower($sql->result("EXTENSAO")).'"');      
  
  echo $img;
  
  $bd->close();
?>