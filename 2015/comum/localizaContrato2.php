<?php
  Session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");     
    
  if (isset($_POST['contrato'])) {
      $bd = new Oracle(); 
      $sql = new Query($bd);
	  
      $txt = " SELECT HSSTITU.NNUMETITU,CNOMEUSUA FROM HSSTITU,HSSUSUA
	            WHERE CCODITITU = :contrato
				  AND CTIPOUSUA IN ('T','F')
				  AND CSITUUSUA = 'A'
				  AND CSITUTITU = 'A'
				  AND HSSTITU.NNUMEEMPR IS NULL
				  AND HSSTITU.NNUMETITU = HSSUSUA.NNUMETITU  ";              
      $sql->addParam(":contrato",$_POST['contrato']);                          
      $sql->executeQuery($txt);
	  
	  if($sql->count() > 0){
		  $retorno['contrato'] = $sql->result("NNUMETITU");
		  $retorno['titular'] = $sql->result("CNOMEUSUA");
	  }
	  else{
		  $retorno['contrato'] = 0;
		  $retorno['titular'] = '';		  
	  }
	  
	  echo json_encode($retorno);
      $bd->close();
  }
  
  
?>