<?php 

  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(230,5,$_SESSION['nome_operadora']." - ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(230,5,'EXTRATO DE ATENDIMENTOS COBRADOS',0,1);
      $this->SetFont('Arial','B',7);           
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(205,3,$_SESSION['logado'],0,1);      
      $this->SetFont('Arial','B',10);
      $this->Ln(3);
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    function cabecalho($cobranca) {
      $this->SetFont('Arial','B',8);
      $this->Ln(1);
      $this->SetFont('Arial','B',8);
      $this->Cell(5,3,'',0,0);   

      if (($cobranca == '2'))
        $this->Cell(18,3,'Guia',0,0);
      else
        $this->Cell(18,3,'Conta',0,0);
      
      $this->Cell(18,3,'Atendimento',0,0);
      $this->Cell(60,3,'Prestador',0,0);
      $this->Cell(18,3,'C�digo',0,0);
      $this->Cell(90,3,'Item',0,0);
      $this->Cell(10,3,'Qtde',0,0);
      $this->Cell(15,3,'Co-part',0,0);
      $this->Cell(15,3,'',0,1);
      $this->Cell(18,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(60,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(90,3,'',0,0);
      $this->Cell(10,3,'',0,0);
      $this->Cell(15,3,'',0,0);
      $this->Cell(12,3,'',0,0);
      $this->Ln(1);  
      $this->SetFont('Arial','',8);      
    }    
  }
  
  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  $usuario = "";
  $titular = "";
  $tipo = "";
  $conta = 0;
  $total_geral = 0;
  $desp_usuario = 0;
  $total_copart_titular = 0;
  $total_copart_titular_prestador = 0;
  $total_titular_reciprocidade = 0;
  $total_copart_usuario = 0;
  $total_copart_prestador = 0;
  $total_reciprocidade = 0;

  while (!$sql->eof()) {
     
      if ($titular <> $sql->result("TITULAR")) { 
      
        if (($quebrar == 'S') and ($titular <> ""))
          $pdf->AddPage();         
          
        $pdf->Cell(270,3,'Titular: '.$sql->result("TITULAR"),0,1);
        $pdf->Ln(3);        
        $titular = $sql->result("TITULAR");
        $total_copart_titular = 0;
        $total_copart_titular_prestador = 0;
        $total_titular_reciprocidade = 0;
        $tipo = "";
      }

      if ($usuario <> $sql->result("CNOMEUSUA")) {
        $pdf->SetTextColor(0,64,128);      
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(20,3,'Benefici�rio: ',0,0,'R');
        $pdf->Cell(100,3,$sql->result("CCODIUSUA").' - '.$sql->result("CNOMEUSUA"),0,0);      
        $pdf->Cell(20,3,'Idade: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("IDADE").' ANOS',0,0);
        $pdf->Cell(20,3,'Inclus�o: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("DINCLUSUA"),0,1);
        $pdf->Cell(20,3,'Plano: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("PLANO"),0,1);
        $pdf->Ln(3);        
        $usuario = $sql->result("CNOMEUSUA");
        $total_copart_usuario = 0;
        $total_copart_prestador = 0;
        $total_reciprocidade = 0;
        $tipo = "";
        $pdf->SetTextColor(0,0,0);        
      }
      
      if ($tipo <> $sql->result("COBRANCA")) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Ln(3);
        
        if ($sql->result("COBRANCA") == '1') 
          $pdf->Cell(50,3,'CONTAS',0,0);
        else
          $pdf->Cell(50,3,'GUIAS',0,0);
          
        $pdf->Ln(3);
        $pdf->cabecalho($sql->result("COBRANCA"));
        
        $tipo = $sql->result("COBRANCA");
      }      

      $pdf->Cell(5,3,'',0,0);      
      $pdf->Cell(18,3,$sql->result("NNUMECONT"),0,0);
      $pdf->Cell(18,3,$sql->result("DATA"),0,0);
      $pdf->Cell(60,3,$pdf->Copy($sql->result("CNOMEPRES"),59),0,0);

      if ($sql->result("TIPO") == "G") {
        $txt2 = "SELECT HSSPGUI.CCODIPMED,CNOMEPMED,NFRANPGUI COPART,NQINDPGUI VALOR,NQUANPGUI QTDE,
                        NCOPRPGUI COPART_PRES,0 RECIPROCIDADE
                   FROM HSSPGUI,HSSPMED
                  WHERE NNUMEGUIA = :numero
                    AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED";
      } else {
        $txt2 = "SELECT HSSPCON.CCODIPMED,CNOMEPMED,NFRANPCON COPART,TOTAL_CONTA_HONORARIOS_PCON(HSSPCON.NNUMEPCON) VALOR,NQUANPCON QTDE,
                        NCOPRPCON COPART_PRES,NVL(NRECIPCON,0)+NVL(NREC1PCON,0)+NVL(NREC2PCON,0)+NVL(NREC3PCON,0)+NVL(NREC4PCON,0) RECIPROCIDADE
                   FROM HSSPCON,HSSPMED
                  WHERE NNUMECONT = :numero
                    AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                    AND HSSPCON.NFRANPCON > 0
                  UNION ALL
                 SELECT 'TAXAS' CCODIPMED,CDESCTAXA CNOMEPMED,SUM(NFRANTCON) COPART,SUM(NVALOTCON) VALOR,SUM(NQUANTCON) QTDE,
                        0 COPART_PRES,SUM(NVL(NRECITCON,0)) RECIPROCIDADE
                   FROM HSSTCON,HSSTAXA
                  WHERE NNUMECONT = :numero
                    AND NVALOTCON > 0
                    AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                    AND HSSTCON.NFRANTCON > 0
                  GROUP BY CDESCTAXA
                  UNION ALL
                 SELECT 'MAT/MED' CCODIPMED,'MATERIAIS E MEDICAMENTOS' CNOMEPMED,VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT) COPART,TOTAL_FARMACIA(HSSCONT.NNUMECONT) VALOR, 1 QTDE,
                        0 COPART_PRES,0 RECIPROCIDADE
                   FROM HSSCONT
                  WHERE NNUMECONT = :numero
                    AND TOTAL_FARMACIA(HSSCONT.NNUMECONT) > 0 
                    AND VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT) > 0 ";
      }

      $sql3 = new Query($bd);
      $sql3->addParam(":numero",$sql->result("NNUMECONT"));
      $sql3->executeQuery($txt2);
      
      $qtde_proc = 0;

      while (!$sql3->eof()) {
        if ($qtde_proc > 0)
          $pdf->Cell(101,3,'',0,0);

        $pdf->Cell(18,3,$sql3->result("CCODIPMED"),0,0);
        $pdf->Cell(90,3,$pdf->Copy($sql3->result("CNOMEPMED"),89),0,0);
        $pdf->Cell(10,3,$sql3->result("QTDE"),0,0,'C');
        $pdf->Cell(15,3,$formata->formataNumero($sql3->result("COPART")),0,0,'R');	  
        $pdf->Cell(15,3,'',0,1,'R');

        $qtde_proc++;
        $total_copart_usuario = $total_copart_usuario + str_replace(',','.',$sql3->result("COPART"));
        $total_copart_prestador = $total_copart_prestador + str_replace(',','.',$sql3->result("COPART_PRES"));
        $total_reciprocidade = $total_reciprocidade + str_replace(',','.',$sql3->result("RECIPROCIDADE"));

        $total_copart_titular = $total_copart_titular + str_replace(',','.',$sql3->result("COPART"));
        $total_copart_titular_prestador = $total_copart_titular_prestador + str_replace(',','.',$sql3->result("COPART_PRES"));
        $total_titular_reciprocidade = $total_titular_reciprocidade + str_replace(',','.',$sql3->result("RECIPROCIDADE"));
        $sql3->next();
      }
      
      $valor_cobrado = str_replace(',','.',$sql->result("COBRADO"));
      $total_cobrado = $total_cobrado + str_replace(',','.',$sql->result("COBRADO"));
      
      $sql->next();
      
     if (($usuario <> $sql->result("CNOMEUSUA")) and ($_SESSION['apelido_operadora'] <> 'unimedBarra')) {
        $pdf->Ln(3);      
        $pdf->Cell(219,3,'Total do usu�rio: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_copart_usuario),0,0,'R');
        $pdf->Ln(3);
      }

      if (($valor_cobrado > 0) and ($usuario <> $sql->result("CNOMEUSUA")) and ($_SESSION['apelido_operadora'] == 'agemed')) {
        $pdf->Cell(219,3,'Valor cobrado: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($valor_cobrado),0,0,'R');
        $pdf->Ln(6);             
      }

      if ($titular <> $sql->result("TITULAR")) {
        $pdf->Cell(219,3,'Total do titular: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_copart_titular),0,0,'R');
        $pdf->Ln(3);
      }

      if ((($titular <> $sql->result("TITULAR")) and ($total_cobrado > 0)) and ($_SESSION['apelido_operadora'] == 'agemed')) {
        $pdf->Cell(219,3,'Total cobrado: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_cobrado),0,0,'R');
        $pdf->Ln(3);
      }      
  }

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";
?>
