<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
      
  if ((isset($_SESSION['arquivo_foto'])) and file_exists($_SESSION['arquivo_foto']))
    unlink($_SESSION['arquivo_foto']);
  
  if (isset($_POST['codigo']))
    $codigo = $_POST['codigo'];
  else
    $codigo = "";
    
  if (isset($_POST['usuario']))
    $usuario = $_POST['usuario'];
  else
    $usuario = "";
        
  if (($codigo <> "") or ($usuario > 0)) {
    $sql = new Query($bd);
    
    if ($codigo <> "") {      
      $txt = "SELECT CFOTOUSUA,NNUMEUSUA FROM HSSUSUA WHERE CCODIUSUA = :codigo ORDER BY CSITUUSUA";      
      $sql->addParam(":codigo",$_POST['codigo']);
    }
    else {
      $txt = "SELECT CFOTOUSUA,NNUMEUSUA FROM HSSUSUA WHERE NNUMEUSUA = :usuario ORDER BY CSITUUSUA";      
      $sql->addParam(":usuario",$_POST['usuario']);      
    }
    
    $sql->executeQuery($txt);

    $file='../temp/'.md5(uniqid(rand(), true)).'.jpg';    
     
    if ($sql->count() > 0) {
      $foto_usuario = $sql->result("CFOTOUSUA");      

      if ($foto_usuario <> '') {
        //header("Content-type: " . image_type_to_mime_type($foto_usuario));      
  
        $f=fopen($file,'wb'); 
        if(!$f)  
          $this->Error('Unable to create output file: '.$file);   
        fwrite($f,$foto_usuario,strlen($foto_usuario)); 
        fclose($f);

        $_SESSION['arquivo_foto'] = $file;
        echo $file;        
      } else {
        echo "2";
      }
    } else {
      echo "2";
    } 
  } else {
    echo "2";
  }        

  $bd->close();
  
?>