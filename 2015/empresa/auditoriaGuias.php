<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  include_once "../comum/autoload.php";  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $bd = new Oracle();
  
  $_SESSION['titulo'] = "AUDITORIA DE GUIAS";
  
  require_once("../comum/layout.php");
  $tpl->addFile("CONTEUDO","auditoriaGuias.htm");  

  $tpl->ID_SESSAO = $_GET['idSessao'];
  $tpl->MASCARA = $_SESSION['mascara'];  
  
  $tpl->TYPE = "text";
  $tpl->BOTAO_BUSCA_USUARIO = '<i class="icon-search"></i>';    
  $tpl->ID_BUSCA_USUARIO = "localizar";    
  $tpl->block("BUSCA_BENEFICIARIO");   
    
  $registros        = 15;  
  
  if (isset($_GET['pg']))
    $pg = $_GET['pg'];
	else
    $pg = 0;
  
	$inicio = $pg * $registros;
  
  $filtros = array();    
      
  if (isset($_POST['enviar'])) {   
    $filtros['auditor']          = htmlentities($_POST['auditor']);
    $filtros['filtro']           = htmlentities($_POST['filtro']);
    $filtros['data_inicial']     = htmlentities($_POST['data_inicial']);
    $filtros['data_final']       = htmlentities($_POST['data_final']);
    $filtros['guia']             = htmlentities($_POST['guia']);
    $filtros['senha']            = htmlentities($_POST['senha']);
    $filtros['executante']       = htmlentities($_POST['executante']);
    $filtros['regime']           = htmlentities($_POST['regime']);
    $filtros['tipo_autorizacao'] = htmlentities($_POST['tipo_autorizacao']);
    $filtros['ordenacao']        = htmlentities($_POST['ordenacao']);
    
    if (isset($_POST['codigo']))
      $filtros['codigo']           = htmlentities($_POST['codigo']);          
    else
      $filtros['codigo']           = '';
      
    $filtros['inicio']           = 0; 
    $pg                          = 0;
    
    $filtros['executa']          = 'N';       
    
    $_SESSION['filtrosAutorizacao'] = $filtros;     
  }
  else if (isset($_SESSION['filtrosAutorizacao'])) {
    $filtros                      = $_SESSION['filtrosAutorizacao'];
    $filtros['executa']           = 'S';       
    $filtros['inicio']            = $inicio;
  } 
  else {
    
    $operadoras = array('unimedJiParana','unimedBarra');
    if (in_array($_SESSION['apelido_operadora'],$operadoras))
      $filtros['auditor'] = $_SESSION['id_operador'];  
    else      
      $filtros['auditor']          = '';
      
    $filtros['filtro']           = '';
    $filtros['data_inicial']     = '';
    $filtros['data_final']       = '';
    $filtros['guia']             = '';
    $filtros['senha']            = '';
    $filtros['executante']       = '';
    $filtros['regime']           = '';
    $filtros['tipo_autorizacao'] = '4';
    $filtros['ordenacao']        = '';
    $filtros['codigo']           = '';     
    $filtros['inicio']           = 0; 
    $pg                          = 0;    
    $filtros['executa']          = 'N';  
  }

  $auditor                     = $filtros['auditor'];
  $filtro                      = $filtros['filtro'];
  $data_inicial                = $filtros['data_inicial'];       
  $data_final                  = $filtros['data_final'];   
  $guia                        = $filtros['guia'];  
  $senha                       = $filtros['senha'];
  $executante                  = $filtros['executante'];
  $regime                      = $filtros['regime'];
  $tipo_autorizacao            = $filtros['tipo_autorizacao'];     
  $ordenacao                   = $filtros['ordenacao'];    
  $codigo                      = $filtros['codigo'];  
  $inicio                      = $filtros['inicio'];   
    
  if ($filtro == '')
    $filtro = '3';
    
  if ($regime == '')
    $regime = '1';    
  
  

  $tpl->FILTRO_SELECIONADO = $filtro;  
  $tpl->DATA_INICIAL       = $data_inicial;
  $tpl->DATA_FINAL         = $data_final;
  $tpl->NUMERO_GUIA        = $guia;
  $tpl->SENHA              = $senha;
  $tpl->CODIGO             = $codigo;
  $tpl->REGIME             = $regime;
  $tpl->TIPO_AUTORIZACAO   = $tipo_autorizacao;  
  $tpl->block("MOSTRA_TIPO_AUTORIZACAO"); 
            
  $tpl->block("LEGENDA");

  $sql = new Query($bd);  
  
  if (isset($_POST['imprimir'])) 
    $inicio = -1;


      require_once('auditoriaSQL.php');

  if (isset($_POST['enviar']) or ($filtros['executa'] == 'S')) {
    if (($data_inicial == '' or $data_final == '') and $guia == '') {
    $msg->text = "Informe o per�odo ou o n�mero da autoriza��o.";             
    }
    else if ($data_inicial <> '' and $data_final <> '' and $guia == '') {

      if (($data->validaData($bd,$data_inicial) == false) or ($data->validaData($bd,$data_final) == false))
        $msg->text = "Data inv�lida informada no per�odo.";
      else if ($data->comparaData($bd,$data_inicial,$data_final,'>') == 1)
        $msg->text = "A data inicial n�o pode ser superior a data final.";
    }
    $sql->executeQuery($txt_guias); 
    if ($sql->count() > 0) {
    
      $sql_total->executeQuery($txt_total);    
      $paginas = ceil($sql_total->result("QTDE")/$registros);
      
      if ($paginas > 1) {
      
        $tpl->PAGINA = 0;
        $tpl->DESC_PAGINA = "&laquo;";

        if ($pg == 0)
          $tpl->PAGINA_CLASSE = "active";
        else
          $tpl->PAGINA_CLASSE = "";
          
        $tpl->block("LINHA_PAGINACAO");
        
        if ($paginas > 2) {
        
          for($i = 1;$i < $paginas;$i++) { 

            $tpl->PAGINA = $i;
            $tpl->DESC_PAGINA = $i;
            
            if ($pg == $i)
              $tpl->PAGINA_CLASSE = "active";
            else
              $tpl->PAGINA_CLASSE = "";
          
            $tpl->block("LINHA_PAGINACAO");
          }
        }
                 
        $tpl->PAGINA = $paginas-1;
        $tpl->DESC_PAGINA = "&raquo;";
        
        if ($pg == $paginas -1)
          $tpl->PAGINA_CLASSE = "active";
        else
          $tpl->PAGINA_CLASSE = "";
          
        $tpl->block("LINHA_PAGINACAO");          
      
        $tpl->block("PAGINACAO");
      }
      
       
      $quantidade = 0;
      $total = 0;
      while (!$sql->eof()) {
      
        $sql_faturada = new Query();
        $txt_faturada = "SELECT NNUMECONT FROM HSSGCON WHERE NNUMEGUIA = :guia ";
        $sql_faturada->addParam(":guia",$sql->result("NNUMEGUIA"));
        $sql_faturada->executeQuery($txt_faturada);
        
        if ($sql->result("CSTATGUIA") == "P")
          $tpl->COR_SITUACAO = "green";
        else if ($sql->result("CSTATGUIA") == "N")
          $tpl->COR_SITUACAO = "red";
        else
          $tpl->COR_SITUACAO = "#363636";     
             
        $tpl->GUIA = $sql->result("NNUMEGUIA");
        $tpl->DATA = $sql->result("DEMISGUIA");

        if ($_SESSION['apelido_operadora'] == 'vitallis')
          $tpl->CODIGO_BENEFICIARIO = acrescenta_zeros($sql->result("CCODIUSUA"),9);
        else
          $tpl->CODIGO_BENEFICIARIO = $sql->result("CCODIUSUA");
      
        $tpl->BENEFICIARIO = $sql->result("CNOMEUSUA");
        
        $tpl->TIPO_GUIA = $sql->result("TIPO");
        
       
        $tpl->block("BOTAO_IMPRIMIR");          
       
        if ($sql->result("CSTATGUIA") == "P"){
          $tpl->block("NEGA_AUDITORIA");
          $tpl->block("ACEITA_AUDITORIA");        
        }else{
          $tpl->block("GUIA_AUDITADA");
          if ($sql->result("CSTATGUIA") == "N"){
            $tpl->block("STATUS_AUDITADA");
            $tpl->STATUS = "NEGADA";
          }
        }       
        $tpl->block("LINHA");

        $quantidade++;
        $total += str_replace(',','.',$sql->result("VALOR"));
              
        $sql->next();
      }
      
      $tpl->QTDE = $quantidade;      
              
      $tpl->block("RESULTADO");        
      
    } else {
    
      $msg = new Dialog();    
      $msg->title = "Auditoria";      
      $msg->type = "alerta";
        
      if (($data_inicial == '' or $data_final == '') and $guia == '') {
        $msg->text = "Informe o per�odo ou o n�mero da autoriza��o.";             
      }
      else if ($data_inicial <> '' and $data_final <> '' and $guia == '') {
      
        if (($data->validaData($bd,$data_inicial) == false) or ($data->validaData($bd,$data_final) == false))
          $msg->text = "Data inv�lida informada no per�odo.";
        else if ($data->comparaData($bd,$data_inicial,$data_final,'>') == 1)
          $msg->text = "A data inicial n�o pode ser superior a data final.";
        else
          $msg->text = "N�o existe nenhuma autoriza��o para o per�odo informado.";
          
        $msg->type = "erro";
                
      } else if ($guia <> '') {
        $sql->clear();
        $txt = "SELECT NNUMEGUIA,CSTATGUIA FROM HSSGUIA WHERE NNUMEGUIA = :guia";
        $sql->addParam(":guia",$guia);
        $sql->executeQuery($txt);
        
        if ($sql->count() == 0) 
          $msg->text = "A guia informada n�o existe.";
        else
          $msg->text = "Guia n�o localizada.";
      }
           $filtros['guia'] = ''; 
      $msg->showTpl($tpl);      
    }          
  }
  
  $tpl->block("MOSTRA_MENU");     
  $bd->close();
  $tpl->show();     

?>