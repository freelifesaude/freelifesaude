<?php
  Session_start();
  
  require_once('../comum/sessao.php');
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  
  $bd   = new Oracle();
  $data = new Data();

  $atcad = $_POST['p'];

  $txt = "SELECT CNOMEATCAD,TO_CHAR(DDATAATCAD,'DD/MM/YYYY HH24:MI') DDATAATCAD,CSEXOATCAD,TO_CHAR(DNASCATCAD,'DD/MM/YYYY') DNASCATCAD,CCHAPATCAD,
                 C__RGATCAD,CORRGATCAD,C_CPFATCAD,CNMAEATCAD,CENDEATCAD,CBAIRATCAD,CCIDAATCAD,
                 CESTAATCAD,CCEP_ATCAD,NFATUTLOGR,CFONEATCAD,CTELRATCAD,CTELCATCAD,CDESCACOM,
                 NRGMSPLAN,CESTCATCAD,CDESCPROF,COPERATCAD,CIP__ATCAD
            FROM HSSATCAD,HSSPLAN,HSSACOM,HSSPROF
           WHERE NNUMEATCAD = :id
             AND HSSATCAD.NNUMEPLAN = HSSPLAN.NNUMEPLAN(+)
             AND HSSATCAD.NNUMEACOM = HSSACOM.NNUMEACOM(+)
             AND HSSATCAD.NNUMEPROF = HSSPROF.NNUMEPROF(+) ";
             
  $sql = new Query($bd);
  $sql->addParam(":id",$atcad);
  $sql->executeQuery($txt);  

  $pdf = new PDFSolus ('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','B',13);

  $pdf->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
  $pdf->Cell(15);
  $pdf->Cell(170,25,'Protocolo de movimenta��o',0,1,'C');
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(170,4,'Benefici�rio(a): ' . $sql->result("CNOMEATCAD"),0,1,'E');
  $pdf->Cell(170,4,'N�mero de protocolo: ' . $atcad,0,1,'E');
  $pdf->Cell(170,4,'Data de movimenta��o: ' . $sql->result("DDATAATCAD"),0,1,'E');
  
  if ($sql->result("COPERATCAD") == 'I') {
    $pdf->Cell(170,4,'Opera��o: Inclus�o',0,1,'E');
  } else if ($sql->result("COPERATCAD") == 'C'){
    $pdf->Cell(170,4,'Opera��o: Cancelamento',0,1,'E');
  } else if ($sql->result("COPERATCAD") == 'A') {
    $pdf->Cell(170,4,'Opera��o: Altera��o',0,1,'E');
  }

  if ($sql->result("CIP__ATCAD") <> '')
    $pdf->Cell(170,4,'IP: ' . $sql->result("CIP__ATCAD"),0,1,'E');
  
  
  if ($sql->result("CLOCAATCAD") == '') {
    $pdf->Cell(170,4,'Empresa: ' . $_SESSION['logado'],0,1,'E');
  } else {
    $pdf->Cell(170,4,'Loca��o: ' . $_SESSION['logado'],0,1,'E');
  }
  
  $pdf->Cell(190,1,'',0,1);  
  $pdf->SetFont('Arial','',6);  
  $pdf->Cell(190,3,'Solus Computa��o - www.solus.inf.br :: Emitido por '. 
             $_SESSION['operador'].' '.$data->dataAtual('DD/MM/YYYY HH24:mi'),0,1,'R');  
  
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";
  
  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
?>