<?php
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 

  $bd = new Oracle();  
   
  $_SESSION['titulo'] = "LOG DE OPERA��ES";
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/listaLogs.html");  
  
  $inicio = '';
  $fim    = '';
  
  if (isset($_POST['enviar'])) {
    $inicio = $seg->antiInjection($_POST['data_ini']);
    $fim    = $seg->antiInjection($_POST['data_fim']);

    if ($data->validaData($bd,$inicio) and $data->validaData($bd,$fim)) {  
      $sql = new Query($bd);

      if ($_SESSION['sistema'] == 'Prestador') {
        $txt = "SELECT NNUMELOGWE,TO_CHAR(DDATALOGWE,'DD/MM/YYYY') DATA,TO_CHAR(DDATALOGWE,'HH24:mi') HORA,NVL(CNCOMUSUA,CNOMEUSUA) CNOMEUSUA,
                       CIP__LOGWE,COPERLOGWE
                  FROM HSSLOGWE,SEGUSUA
                 WHERE HSSLOGWE.NID__LOGWE = :contratado
                   AND DDATALOGWE >= TO_DATE(:inicio,'DD/MM/YYYY')
                   AND DDATALOGWE < TO_DATE(:fim,'DD/MM/YYYY') + 1
                   AND HSSLOGWE.NOPERUSUA = SEGUSUA.NNUMEUSUA
                 ORDER BY NNUMELOGWE DESC";   
                 
        $sql->addParam(":inicio",$inicio);
        $sql->addParam(":fim",$fim);    
        $sql->addParam(":contratado",$_SESSION['id_contratado']);
        $sql->executeQuery($txt);                
      }
      
      if ($_SESSION['sistema'] == 'Empresa') {
        $txt = "SELECT NNUMELOGWE,TO_CHAR(DDATALOGWE,'DD/MM/YYYY') DATA,TO_CHAR(DDATALOGWE,'HH24:mi') HORA,NVL(CNCOMUSUA,CNOMEUSUA) CNOMEUSUA,
                     CIP__LOGWE,COPERLOGWE
                FROM HSSLOGWE,SEGUSUA
               WHERE HSSLOGWE.NID__LOGWE = :contratado
                 AND DDATALOGWE >= TO_DATE(:inicio,'DD/MM/YYYY')
                 AND DDATALOGWE < TO_DATE(:fim,'DD/MM/YYYY') + 1
                 AND HSSLOGWE.NOPERUSUA = SEGUSUA.NNUMEUSUA
                 AND NVL(CLOCALOGWE,'X') = 'EMPRESA'
               ORDER BY NNUMELOGWE DESC"; 

        $sql->addParam(":inicio",$inicio);
        $sql->addParam(":fim",$fim);    
        $sql->addParam(":contratado",$_SESSION['id_contrato']);
        $sql->executeQuery($txt);              
      }   

      if ($_SESSION['sistema'] == 'Comercial') {
        $txt = "SELECT NNUMELOGWE,TO_CHAR(DDATALOGWE,'DD/MM/YYYY') DATA,TO_CHAR(DDATALOGWE,'HH24:mi') HORA,NVL(CNCOMUSUA,CNOMEUSUA) CNOMEUSUA,
                       CIP__LOGWE,COPERLOGWE
                  FROM HSSLOGWE,SEGUSUA
                 WHERE HSSLOGWE.NID__LOGWE = :contratado
                   AND DDATALOGWE >= TO_DATE(:inicio,'DD/MM/YYYY')
                   AND DDATALOGWE < TO_DATE(:fim,'DD/MM/YYYY') + 1
                   AND HSSLOGWE.NOPERUSUA = SEGUSUA.NNUMEUSUA
                 ORDER BY NNUMELOGWE DESC";   
                 
        $sql->addParam(":inicio",$inicio);
        $sql->addParam(":fim",$fim);    
        $sql->addParam(":contratado",$_SESSION['id_vendedor']);
        $sql->executeQuery($txt); 

      }  
      
      while (!$sql->eof()) {           
        $tpl->PROTOCOLO = $sql->result("NNUMELOGWE");
        $tpl->DATA      = $sql->result("DATA");
        $tpl->HORA      = $sql->result("HORA");
        $tpl->OPERACAO  = $sql->result("COPERLOGWE");
        $tpl->OPERADOR  = $sql->result("CNOMEUSUA");
        $tpl->IP        = $sql->result("CIP__LOGWE");      
          
        $tpl->block("LINHA");
        $sql->next();
      }        
      
    }
    else {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG = "Data inicial ou final inv�lida";
      $tpl->block("MENSAGEM");
    }    
  }
  
  $tpl->DATA_INI = $inicio;
  $tpl->DATA_FIM = $fim;  
   
  $tpl->block("MOSTRA_MENU");
  $bd->close();  
  $tpl->show();     
  
?>