<?php
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  $bd      = new Oracle();
  $cidade = '';
 
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CDHATPRES,CCIDAPRES, CCODIPRES,CNOMEPRES FROM FINPRES
           WHERE CDHATPRES IS NOT NULL
           ORDER BY CCIDAPRES, CNOMEPRES";
  $sql->executeQuery($txt);  
  
  if ($sql->count() > 0) {
    class PDF extends PDFSolus {
      
      function Header() {
        //Logo
        $this->Image('../comum/img/logo_relatorio.jpg',10,8,30,22);
        $this->SetFont('Arial','B',10);
        $this->Cell(32,5,'');
        $this->Cell(32,5,'');
        $this->Cell(158,5,'Dias e horários de atendimento em consultório dos',0,1);
        $this->Cell(66,5,'');
        if($_SESSION['apelido_operadora'] == "UnimedLestePaulista")
          $this->Cell(158,5,' médicos cooperados da Unimed Leste Paulista',0,1);
		else
		  $this->Cell(158,5,' Médicos Credenciados da Operadora',0,1);
        $this->Ln(10);
        $this->Cell(190,1,' ','B',0);
        $this->Ln(2);      
      }

      function Footer()
      {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        //Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
      }
    }
    
    $pdf = new PDF('P','mm','A4');
    $pdf->AliasNbPages();

    $pdf->Open();
    $pdf->AddPage();  
    $pdf->SetFont('Arial','',8);  
    
    $primeira = true;
    while (!$sql->eof()) {   
      if ($cidade <> $sql->result("CCIDAPRES")) {
        if ($primeira == False) {
        $pdf->AddPage();
      }
        $primeira = false;
        $pdf->SetFont('Arial','B',12);
        $pdf->Ln(6); 
        $pdf->Cell(80,5,$sql->result("CCIDAPRES"),'',1);      
      }  
      $pdf->SetFont('Arial','',8);
      $pdf->Cell(80,5,$sql->result("CNOMEPRES"),'',1);  
      $pdf->Cell(80,5,$sql->result("CDHATPRES"),'',1);  
      $pdf->Ln(2); 
      $cidade = $sql->result("CCIDAPRES");
      $sql->next();
    }   

    $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
    $pdf->Output($file,'F');
    $bd->close();
    echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
  }
  else
    echo "<HTML><SCRIPT>close();</SCRIPT></HTML>";  
?>