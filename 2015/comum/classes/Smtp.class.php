<?php

class Smtp {
  var $conn;
  var $user;
  var $pass;
  var $debug;
  var $error;
  
  function Smtp ($host,$port) {
    try {
      $this->conn = fsockopen($host,$port,$errno,$errstr,15);      

    } catch (exception $e) {
      echo $e['message'];
      $this->conn = false;
    }
    $this->server_parse($this->conn, '220');
    
    if (!$this->conn) {
      $this->error = "Erro ao conectar: ".$host.":".$port;
      break;
    }
    else {
      $this->Put("EHLO $host");             
      $this->server_parse($this->conn, '250');      
    }
  }
  
  function Auth() {
    $this->Put("AUTH LOGIN");
    $this->server_parse($this->conn, '334');
    
    $this->Put(base64_encode($this->user));
    $this->server_parse($this->conn, '334');
    
    $this->Put(base64_encode($this->pass));    
    $this->server_parse($this->conn, '235');
    
  }
          
  function Send($to,$from,$subject,$msg,$att) {
  
    if ($this->user <> '')
      $this->Auth();
      
    $this->Put("MAIL FROM: <".$from.">");
    $this->server_parse($this->conn, '250');
    
    $this->Put("RCPT TO: <".$to.">");
    $this->server_parse($this->conn, '250');
    
    $this->Put("DATA");
    $this->server_parse($this->conn, '354');

    //Com anexos
    if (sizeOf($att) > 0) {
      $boundary = "XYZ-" . date("dmYis") . "-ZYX";
      
      $mens = "--$boundary\n";      
      $mens .= "Content-Transfer-Encoding: 8bits\n";
      $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"\n\n"; //plain
      $mens .= "$msg\n";
      $mens .= "--$boundary\n";
          
      for($i=0; $i < count($att); $i++) {
      
        if(file_exists($att[$i]["arquivo"])) {
          $fp = fopen($att[$i]["arquivo"],"rb");
          $attachment = fread($fp,filesize($att[$i]["arquivo"]));
          $attachment = base64_encode($attachment);
          fclose($fp);     
          $attachment = chunk_split($attachment);

          $mens .= "Content-Type: ".$att[$i]["type"]."\n name=\"".$att[$i]["name"]."\"\n";
          $mens .= "Content-Disposition: attachment; filename=\"".$att[$i]["name"]."\"\n";
          $mens .= "Content-Transfer-Encoding: base64\n\n";
          $mens .= "$attachment\n";
          
          if($i + 1 == count($att)) 
            $mens.= "--$boundary--"; 
          else 
            $mens.= "--$boundary\n";                  
        }
      } 

      $headers  = "MIME-Version: 1.0\n";
      $headers .= "Date: ".date("D, d M Y H:i:s O")."\n";  
      $headers .= "From: <".$from.">\r\n";
      $headers .= "To: <".$to."> \r\n";
      $headers .= "Return-Path: <".$to."> \r\n";
      $headers .= "Subject: ".$subject." \r\n";
      $headers .= "Date: ".date('D, d M Y H:i:s O')." \r\n";
      $headers .= "X-MSMail-Priority: High \r\n";  
      $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";      
    } else {
      $mens = $msg;
      
      $headers = "Message-Id: <".date('YmdHis').".".md5(microtime()).".".strtoupper($from)."> \r\n";
      $headers .= "From: <".$from."> \r\n";
      $headers .= "To: <".$to."> \r\n";
      $headers .= "Return-Path: <".$to."> \r\n";
      $headers .= "Subject: ".$subject." \r\n";
      $headers .= "Date: ".date('D, d M Y H:i:s O')." \r\n";
      $headers .= "X-MSMail-Priority: High \r\n";
      $headers .= "Content-Type: Text/HTML";    
    }
    
    $this->Put($headers);
    
    $this->Put("\r\n");
    $this->Put($mens);
    
    $this->Put(".");
    $this->server_parse($this->conn, '250');
    
    return $this->Close();
  }
  
  function Put($value) {
    return fputs($this->conn,$value."\r\n");
  }
  
  function Close() {
    $this->Put("QUIT");
    
    $erro = '';
    
    if ($this->debug == true) {
        
      while (!feof($this->conn)) {
        $msg = fgets($this->conn);
        
        if (strpos($msg,'5.7.8'))
          $erro .= "5.7.8 - Falha na autenticação.<br>";
        else if (strpos($msg,'5.5.2'))
          $erro .= "5.5.2 - Endereço de email rejeitado.<br>";
        else if (strpos($msg,'5.5.1'))
          $erro .= "5.5.1 - Endereço de email inválido.<br>";
        else if (strpos($msg,'5.1.3'))
          $erro .= "5.1.3 - Erro de sintaxe no destinatário.<br>";          
      }
    }

    fclose($this->conn);

    if ($erro <> '') {
      return false;
    } else
      return true;
  }
  
  function server_parse($socket, $expected_response) {
    
    $server_response = '';
    $contador        = 0;
    
    while (substr($server_response, 3, 1) != ' ') {
      $contador++;
      if (!($server_response = fgets($socket, 256))) {
        echo 'Couldn\'t get mail server response codes. Please contact the forum administrator.', __FILE__, __LINE__;
        exit;
      }
            
      if ($contador >= 100)
        exit;
    }
 
    //if (!(substr($server_response, 0, 3) == $expected_response))
    //  echo 'Unable to send e-mail. Please contact the forum administrator with the following error message reported by the SMTP server: "'.$server_response.'"', __FILE__, __LINE__;
  }  
}

?>