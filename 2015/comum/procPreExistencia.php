<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  $_SESSION['titulo'] = "PROCEDIMENTOS POR PR�-EXIST�NCIA";
  require_once("../comum/layoutJanela.php");
  $tpl->addFile("JANELA_CONTEUDO","../comum/procPreExistencia.htm");
  $tpl->block("MOSTRA_LOGO");
	
  if (isset($_POST['idPre']))
    $id_PreExistencia = $_POST['idPre'];
    
  if (isset($_POST['codBeneficiario']))
    $cod_usuario = $_POST['codBeneficiario'];
        
  $sql = new Query($bd);
  $sql->clear();    
  $txt = "SELECT CNOMEUSUA,DVENCPREEX,HSSPMED.CCODIPMED,CNOMEPMED FROM HSSPREEX, HSSUSUA, HSSPROPR, HSSPMED
					 WHERE HSSUSUA.CCODIUSUA = :beneficiario
						AND HSSPREEX.NNUMEPREEX = :id_PreExistencia
						AND HSSPREEX.NNUMEUSUA = HSSUSUA.NNUMEUSUA
						AND HSSPROPR.NNUMEPREEX = HSSPREEX.NNUMEPREEX
						AND HSSPROPR.CCODIPMED = HSSPMED.CCODIPMED"; 
						
  $sql->addParam(":beneficiario",$cod_usuario);
  $sql->addParam(":id_PreExistencia",$id_PreExistencia);  
  $sql->executeQuery($txt);

  $tpl->NOME = $sql->result("CNOMEUSUA");
  $tpl->VENCIMENTO = $sql->result("DVENCPREEX");
 
  if ($sql->count() > 0) { 
    while (!$sql->eof()) {
			$tpl->PROCEDIMENTO_CODIGO = $sql->result("CCODIPMED");
			$tpl->PROCEDIMENTO_NOME = $sql->result("CNOMEPMED");
			$tpl->block("LINHA");        
     
      $sql->next();     
    }
    $tpl->block("PROCEDIMENTOS");              
    $tpl->block("RESULTADO");   
  } else {
    $tpl->MSG = "** N�o existe procedimento para essa Pr�-Existencia **";
    $tpl->block("ERRO");
  }      
 
  $bd->close();
  $tpl->show();     

?>