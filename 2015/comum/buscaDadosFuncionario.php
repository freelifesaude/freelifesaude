<?php
  session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
  
    
  if (isset($_POST['usuario'])) {
	  
	//vem do menu GERAR ASO SEM AGENDAMENTO
    if($_POST['local_aso'] == 'R'){
	  $sql = new Query($bd);
	  $txt = "SELECT CCODIUSUA,CNOMEUSUA,DECODE(CSITUUSUA,'A','Ativo','C','Cancelado') SITUACAO,TO_CHAR(DNASCUSUA,'DD/MM/YYYY')DNASCUSUA,IDADE(DNASCUSUA) IDADE,
					TO_CHAR(DADMIUSUA,'DD/MM/YYYY') DADMIUSUA,C__RGUSUA,DEXRGUSUA,C_CPFUSUA,DECODE(CSEXOUSUA,'M','Masculino','F','Feminino') SEXO,					
					IDADE_DETALHADO2(DADMIUSUA) TEMPO_SERVICO,CNOMESETOR,CNOMEDEPAR,nvl(ultimaProfissaoUsuario(HSSUSUA.NNUMEUSUA,:data_base),CDESCPROF) CDESCPROF,CRAZAEMPR,HSSUSUA.NNUMEUSUA				
				FROM HSSUSUA,HSSPLAN,HSSPROF,HSSTITU,HSSEMPR,HSSDEPAR,HSSSETOR
			   WHERE (CCODIUSUA = :matricula OR CCHAPUSUA = :matricula)
				 AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
				 AND HSSUSUA.NNUMEPROF = HSSPROF.NNUMEPROF(+)
				 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
				 AND HSSUSUA.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
				 AND HSSUSUA.NNUMEDEPAR = HSSDEPAR.NNUMEDEPAR(+)
				 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR ";                            
	  $sql->addParam(":matricula",$_POST['usuario']);    
	  $sql->addParam(":data_base",$_POST['data_base']); 
	  $sql->executeQuery($txt);
		
	  $retorno = array();
	  $retorno["id"] = $sql->result("NNUMEUSUA");  
	  $retorno["codigo"] = utf8_encode($sql->result("CCODIUSUA"));  	  
	  $retorno["nome"] = utf8_encode($sql->result("CNOMEUSUA"));
	  $retorno["situacao"] = utf8_encode($sql->result("SITUACAO"));
	  $retorno["nascimento"] = utf8_encode($sql->result("DNASCUSUA"));	  
	  $retorno["idade"] = utf8_encode($sql->result("IDADE"));
	  $retorno["rg"] = utf8_encode($sql->result("C__RGUSUA"));
	  $retorno["admissao"] = utf8_encode($sql->result("DADMIUSUA"));
	  $retorno["tempo_servico"] = utf8_encode($sql->result("TEMPO_SERVICO"));
	  $retorno["sexo"] = utf8_encode($sql->result("SEXO"));
	  $retorno["empresa"] = utf8_encode($sql->result("CRAZAEMPR")); 	  
	  $retorno["unidade"] = utf8_encode($sql->result("CNOMESETOR"));
	  $retorno["departamento"] = utf8_encode($sql->result("CNOMEDEPAR"));
	  $retorno["profissao"] = utf8_encode($sql->result("CDESCPROF"));   	   	         
	}
	else{
	  $sql = new Query($bd);
	  $txt = "SELECT HSSUSUA.NNUMEUSUA,CNOMEUSUA,CDESCPROF,HSSPROF.NNUMEPROF,
		   		     NOME_DEPARTAMENTO_USUARIO(NNUMEDEPAR) DEPARTAMENTO,CRAZAEMPR
				FROM HSSUSUA,HSSPLAN,HSSPROF,HSSTITU,HSSEMPR 
			   WHERE (CCODIUSUA = :matricula OR CCHAPUSUA = :matricula)
				 AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
				 AND HSSUSUA.NNUMEPROF = HSSPROF.NNUMEPROF(+)
				 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
				 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR ";                            
	  $sql->addParam(":matricula",$_POST['usuario']);    
	  $sql->executeQuery($txt);
		
	  $retorno = array();
	  $retorno["id"] = $sql->result("NNUMEUSUA");   
	  $retorno["nome"] = utf8_encode($sql->result("CNOMEUSUA"));
	  $retorno["profissao"] = utf8_encode($sql->result("CDESCPROF"));   
	  $retorno["id_nova_profissao"] = $sql->result("NNUMEPROF");
	  $retorno["departamento"] = utf8_encode($sql->result("DEPARTAMENTO")); 
	  $retorno["empresa"] = utf8_encode($sql->result("CRAZAEMPR"));    
	}
  }
  echo json_encode($retorno);
  $bd->close();


?>