create or replace function ElegibilidadeWeb30 (usuario      in number,
                                               procedimento in varchar2,
                                               natureza     in varchar2,
                                               regime       in varchar2,
                                               prestador    in number) return varchar2 as
texto               varchar2(400);
cobranca_franquia   varchar2(1);
vencimentocarencia  date;
nomecarencia        varchar2(60);
valor_fixo          number;
percentual          number;
partecoberta        number;
id_cobertura        number;
id_usuario          number;
tabela_taxa         number;

begin
  texto := '';

  id_usuario := usuario; 
  id_cobertura := cobertura_usuario (usuario,procedimento,natureza,prestador,prestador,cobranca_franquia,sysdate,null,'2','0',0,'0');
                                       
  if id_cobertura > 0 then
    valor_copart (id_cobertura,'2',1,100,sysdate,prestador,null,valor_fixo,percentual,ParteCoberta,usuario,null,null,null,null,null,null,0,0,null,tabela_taxa);
  else
    id_cobertura := cobertura_aditivo (id_usuario,procedimento,cobranca_franquia,valor_fixo,percentual);
  end if;   
  
  esta_em_carencia (id_usuario,procedimento,vencimentocarencia,nomecarencia,null,regime,natureza);
              
  if id_cobertura <> 0 then   
    if nvl(valor_fixo,0) > 0 then
      texto := 'O paciente tem cobertura para o procedimento indicado com coparticipacao no valor de R$'||to_char(valor_fixo,'999999D99');
    elsif nvl(percentual,0) > 0 then
      texto := 'O paciente tem cobertura para o procedimento indicado com coparticipacao de '||to_char(percentual,'999999D99')||'%';
    else
      texto := 'O paciente tem cobertura para o procedimento indicado';    
    end if;
    
    if vencimentocarencia > sysdate then
      texto := texto||', porem esta em carencia ate '||to_char(vencimentocarencia,'DD/MM/YYYY')||'.';
    else
      texto := texto||'.';    
    end if;
  else
    texto := 'O paciente NAO tem cobertura para o procedimento indicado';
  end if;    
  
  return texto;
  
end;                                             
/

create or replace function RetornaIdPacote30 (pacote           in varchar2,
                                            prestador        in number,
                                            usuario          in number,
                                            localAtendimento in number default null) return number as

id_pacote number;

cursor cr_pacote(prestador in number,
                 usuario   in number,
                 pacote    in varchar2) is
                 
select nnumepacot from hsspacot
 where nnumepres = prestador
   and (cdescpacot = pacote or ccodipacot = pacote)
   and dvigepacot = (select max(pacot.dvigepacot) from hsspacot pacot 
                      where (pacot.cdescpacot = pacote or pacot.ccodipacot = pacote)  
                        and pacot.nnumepres = hsspacot.nnumepres 
                        and pacot.dvigepacot <= sysdate) 
   and nnumeredea is null
   and cstatpacot = 'A'
 union 
select nnumepacot from hsspacot
 where nnumepres = prestador
   and (cdescpacot = pacote or ccodipacot = pacote)
   and dvigepacot = (select max(pacot.dvigepacot) from hsspacot pacot 
                      where (pacot.cdescpacot = pacote or pacot.ccodipacot = pacote) 
                        and pacot.nnumepres = hsspacot.nnumepres 
                        and pacot.dvigepacot <= sysdate) 
   and nnumeredea in (select nnumeredea from hssusua, hssredco
                       where hssusua.nnumeusua = usuario
                         and hssusua.nnumetitu = hssredco.nnumetitu
                       union
                      select nnumeredea from hssusua, hssredpl
                       where hssusua.nnumeusua = usuario
                         and hssusua.nnumeplan = hssredpl.nnumeplan)
   and cstatpacot = 'A';

begin
  if (configuracao.regraAutorizacao() = 'L' or
      configuracao.cnpjOperadora() = '04083773000130' or
      configuracao.cnpjOperadora() = '00299149000113') and (localAtendimento > 0) then
      
    open cr_pacote(localAtendimento,usuario,pacote);
    fetch cr_pacote into id_pacote;
    close cr_pacote;      
  else
    open cr_pacote(prestador,usuario,pacote);
    fetch cr_pacote into id_pacote;
    close cr_pacote;        
  end if;
  
  return id_pacote;
end;
/

create or replace function RetornaIdDente30 (dente varchar2) return number as

id_dente number;

cursor cr_dente is
  select hssdente.nnumedente 
    from hssdente
   where ccodidente = dente;

begin
  open cr_dente;
  fetch cr_dente into id_dente;
  close cr_dente;
  return id_dente;
end;
/

create or replace function RetornaIdFace30 (face varchar2) return number as

id_face number;

cursor cr_face is
  select hssface.nnumeface
    from hssface
   where ccodiface = face;

begin
  open cr_face;
  fetch cr_face into id_face;
  close cr_face;
  return id_face;
end;
/

create or replace function valida_rede_atendimento30(usuario in number,
                                                   local   in number,
                                                   pagina  in number default 1,
                                                   procedimento in varchar2 default '',
                                                   especialidade in number default null) return number as
-- Retornos
-- 0 - N�o permitida
-- 1 - Permitida

id_prestador      number;
resultado         number;

TemRede           number;
validar_por_local varchar2(1);
carencia          number;

cursor cr_carencia_usuario is
select hsscrus.nnumecare 
  from hsscrus, hsscare, hsspcar
 where hsscrus.nnumeusua = usuario
   and hsscrus.nnumecare = hsscare.nnumecare
   and hsscare.nnumecare = hsspcar.nnumecare
   and hsspcar.ccodipmed = procedimento;

cursor cr_TemRede_por_carencia is
  select nnumeredea 
    from hssredec
   where nnumecare = carencia;    
   
cursor cr_Rede_por_Local is
select nnumeredea 
  from hssprede
 where nnumepres = local
intersect
select nnumeredea 
  from hssredec
 where nnumecare = carencia;    

cursor cr_rede (usuario   in number,
                local in number) is
  select 0 nnumepres
    from hssusua,hssredpl
   where hssusua.nnumeusua = usuario
     and hssusua.nnumeplan = hssredpl.nnumeplan
     and hssredpl.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
   union all
  select 0 nnumepres
    from hssusua,hssredco
   where hssusua.nnumeusua = usuario
     and hssusua.nnumetitu = hssredco.nnumetitu
     and hssredco.nnumeusua is null
     and hssredco.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
   union all
  select 0 nnumepres
    from hssredco
   where nnumeusua = usuario
    and hssredco.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
   union all   
  -- rede de atendimento no plano
  select nnumepres
    from hssusua,hssredpl,hssprede
   where hssusua.nnumeusua   = usuario
     and hssusua.nnumeplan   = hssredpl.nnumeplan
     and hssredpl.nnumeredea = hssprede.nnumeredea
     and hssprede.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
     and hssprede.nnumepres  = local
     and ((hssprede.nnumeespec is not null and hssprede.nnumeespec  = especialidade) or
          (hssprede.nnumeespec is null))
     and hssusua.nnumetitu not in (select hssredco.nnumetitu from hssredco
                                    where hssredco.nnumetitu = hssusua.nnumetitu
                                      and hssredco.nnumeusua is null
                                      and hssredco.nnumesetor is null)
     and hssusua.nnumeusua not in (select hssredco.nnumeusua from hssredco
                                    where hssredco.nnumeusua = hssusua.nnumeusua)
     and hssusua.nnumesetor not in (select hssredco.nnumesetor from hssredco
                                     where hssredco.nnumesetor = hssusua.nnumesetor)
  union all
  -- rede de atendimento no contrato com locacao informada
  select nnumepres
    from hssusua,hssredco,hssprede
   where hssusua.nnumeusua  = usuario
     and hssusua.nnumesetor  = hssredco.nnumesetor
     and hssredco.nnumeusua is null
     and hssredco.nnumesetor > 0
     and hssredco.nnumeredea = hssprede.nnumeredea
     and hssprede.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
     and hssprede.nnumepres = local
     and ((hssprede.nnumeespec is not null and hssprede.nnumeespec  = especialidade) or
          (hssprede.nnumeespec is null))
  union all
  -- rede de atendimento no contrato sem locacao informada
  select nnumepres
    from hssusua,hssredco,hssprede
   where hssusua.nnumeusua  = usuario
     and hssusua.nnumetitu  = hssredco.nnumetitu
     and hssredco.nnumeusua is null
     and hssredco.nnumesetor is null
     and hssredco.nnumeredea = hssprede.nnumeredea
     and hssprede.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
     and hssprede.nnumepres = local
     and ((hssprede.nnumeespec is not null and hssprede.nnumeespec  = especialidade) or
          (hssprede.nnumeespec is null))   
  union all
  -- rede de atendimento no usuario
  select nnumepres
    from hssredco,hssprede
   where hssredco.nnumeusua = usuario
     and hssredco.nnumeredea = hssprede.nnumeredea
     and hssprede.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A')
     and hssprede.nnumepres = local
     and ((hssprede.nnumeespec is not null and hssprede.nnumeespec  = especialidade) or
          (hssprede.nnumeespec is null))     
  order by 1 desc;

begin
  validar_por_local := '';
  select cvrcaconf
    into validar_por_local  
    from hssconf;

  if pagina = 2 and local = 0 then
    resultado := 1;
  else
    open cr_rede(usuario,local);
    fetch cr_rede into id_prestador;
    if cr_rede%found and nvl(id_prestador,0) = 0 then
      resultado := 0;
    else
      resultado := 1;
    end if;
    close cr_rede;
  end if;
  
 /*
   if validar_por_local = 'L' then
      open cr_carencia_usuario;
      fetch cr_carencia_usuario into carencia; 
      
    if local is null then
      resultado := 1;
    else
      open cr_TemRede_por_carencia;
      fetch cr_TemRede_por_carencia into TemRede;
      if cr_TemRede_por_carencia%notfound then
        resultado := 1;
      else    
        open cr_Rede_por_Local;
        fetch cr_Rede_por_Local into TemRede;
        if cr_Rede_por_Local%found then
          resultado := 0;
        else
          resultado := 1;
        end if;  
        close cr_Rede_por_Local;
      end if;
      close cr_TemRede_por_carencia;
    end if; 
    close cr_carencia_usuario;
  end if;  
  */
  return resultado;
end;
/

create or replace function TemCriticaNaGuia30 (guia   in number,
                                             motivo in number) return varchar2 as

temp number;

cursor cr_critica is
 select nnumeguia from hsscriti
  where nnumeguia = guia
    and nmoticriti = motivo;

begin
  open cr_critica;
  fetch cr_critica into temp;
  if cr_critica%found then
    return 'S';
  else
    return 'N';
  end if;
  close cr_critica;
end;
/

create or replace function PodeSerLiberadoWeb30(procedimento       in  varchar2,
                                               prestador          in  number,
                                               usuario            in  number,
                                               UsadoNaWeb         in  varchar2,
                                               Confirmacao        in  varchar2,
                                               codigo             out varchar2,
                                               tipo               out varchar2,
                                               qtde               out number,
                                               dias               out number,
                                               qtde_mes           out number,
                                               qtde_ano           out number,
                                               auditoria          out varchar2,
                                               natureza_limitacao out varchar2,
                                               regime_limitacao   out varchar2) return varchar2 as

temp            number;
PodeSerLiberado varchar2(1);
                                                
cursor cr_EspecificoPrestador is
  select count(*) from hssprweb
   where nnumepres = prestador;
   
cursor cr_EspecificoRede is
  select count(*) from hssprede,hssprord
   where hssprede.nnumepres = prestador
     and hssprede.nnumeredea = hssprord.nnumeredea
     and hssprede.nnumeredea in (select nnumeredea from hssredea where csituredea = 'A');   
                                                
cursor cr_prestador is
  select ccodipmed,ctipopmed,nmlibpmed,nvl(npconpmed,0),nautmpmed,nautapmed,caudipmed,cnlimpmed,crlimpmed
    from hsspmed,finpres
   where ccodipmed = procedimento
     and (clintpmed = 'S' or confirmacao = 'S' or UsadoNaWeb = 'N')
     and finpres.nnumepres = prestador
     and nvl(cprwepres,'A') = 'A' 
-- cprwepres significa que se for A, os procedimentos relacionados em HSSPRWEB sao adicionados aos que ja estao liberados no cadastro HSSPMED
--                         se for B, os procedimentos relacionados em HSSPRWEB sao exclusivamente liberados na web para o prestador sem considerar o que ta no cadastro HSSPMED
-- Portanto: a) se estiver A nao se deve olhar para HSSPMED
--           b) o que estiver HSSPRWEB sempre vai estar liberado
   union all     
  select hssprweb.ccodipmed,ctipopmed,nmlibpmed,nvl(npconpmed,0),nautmpmed,nautapmed,caudipmed,cnlimpmed,crlimpmed
    from hssprweb,hsspmed
   where nnumepres = prestador
     and hssprweb.ccodipmed = procedimento
     and hssprweb.ccodipmed = hsspmed.ccodipmed;
     
cursor cr_rede is                                                
  select hssprord.ccodipmed,ctipopmed,nmlibpmed,nvl(npconpmed,0),nautmpmed,nautapmed,caudipmed,cnlimpmed,crlimpmed
    from finpres,hssprede,RedesUsuario ru,hssprord,hsspmed
   where finpres.nnumepres = prestador
     and finpres.nnumepres = hssprede.nnumepres
     and ru.nnumeusua = usuario
     and hssprede.nnumeredea = ru.nnumeredea
     and hssprede.nnumeredea = hssprord.nnumeredea
     and hssprord.ccodipmed = procedimento
     and hssprord.ccodipmed = hsspmed.ccodipmed
  union all     
  select ccodipmed,ctipopmed,nmlibpmed,nvl(npconpmed,0),nautmpmed,nautapmed,caudipmed,cnlimpmed,crlimpmed
    from hssprede,hssredea,hsspmed,RedesUsuario ru
   where nnumepres = prestador
     and hssprede.nnumeredea = hssredea.nnumeredea
     and nvl(cprweredea,'A') = 'A' 
     and hsspmed.ccodipmed = procedimento
     and (clintpmed = 'S' or confirmacao = 'S' or UsadoNaWeb = 'N')
     and ru.nnumeusua = usuario
     and hssprede.nnumeredea = ru.nnumeredea;
     
cursor cr_cadastro is
  select ccodipmed,ctipopmed,nmlibpmed,nvl(npconpmed,0),nautmpmed,nautapmed,caudipmed,cnlimpmed,crlimpmed
    from hsspmed
   where ccodipmed = procedimento
     and (clintpmed = 'S' or confirmacao = 'S' or UsadoNaWeb = 'N');
   
cursor cr_congenere is
  select hsspmed.ccodipmed,ctipopmed,nmlibpmed,nvl(npconpmed,0),nautmpmed,nautapmed,caudipmed,cnlimpmed,crlimpmed
    from hsspmed, hssprocg, hsstitu, hssusua
   where hsspmed.ccodipmed = procedimento
     and hsspmed.ccodipmed = hssprocg.ccodipmed
     and nvl(clintprocg, 'N') = 'S'
   and hssprocg.nnumeconge = hsstitu.nnumeconge
   and hsstitu.nnumetitu = hssusua.nnumetitu
   and hssusua.nnumeusua = usuario;
     
begin
  PodeSerLiberado := 'N';
  open cr_EspecificoPrestador;
  fetch cr_EspecificoPrestador into temp;
  close cr_EspecificoPrestador;
  
  if nvl(temp,0) > 0 then
  -- Tem definicoes especificas para o prestador em questao
    open cr_prestador;
    fetch cr_prestador into codigo,tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,regime_limitacao;
    if cr_prestador%found then
      PodeSerLiberado := 'S';
    else
      PodeSerLiberado := 'N';
    end if;  
    close cr_prestador;
  else
    open cr_EspecificoRede;
    fetch cr_EspecificoRede into temp;
    close cr_EspecificoRede;
    if nvl(temp,0) > 0 then 
      open cr_rede;
      fetch cr_rede into codigo,tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,regime_limitacao;
      if cr_rede%found then
        PodeSerLiberado := 'S';
      else
        PodeSerLiberado := 'N';
      end if;  
      close cr_rede;
    else
      open cr_cadastro;
      fetch cr_cadastro into codigo,tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,regime_limitacao;
      if cr_cadastro%found then
        PodeSerLiberado := 'S';
      else
        PodeSerLiberado := 'N';
      end if;  
      close cr_cadastro;
    end if;
  end if;
  
  if (PodeSerLiberado = 'N') then -- se ainda n�o foi liberado pelas regras anteriores, pesquisa a congenere
    open cr_congenere;
    fetch cr_congenere into codigo,tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,regime_limitacao;
    if cr_congenere%found then  -- caso liberado para a congenere, sobrep�e as regras anteriores.
      PodeSerLiberado := 'S';
    else
      PodeSerLiberado := 'N';
    end if;  
    close cr_congenere;
  end if; 
  return PodeSerLiberado;
end;
/                                                

create or replace function valida_Procedimento_web30 (usuario           in     number,
                                                    procedimento      in     varchar2,
                                                    quantidade        in     number,
                                                    prestador         in     number,
                                                    dente             in     varchar2,
                                                    face              in     varchar2,
                                                    especialidade     in     number,
                                                    local             in     number default null,
                                                    urgencia          in     varchar2 default 'E',
                                                    natureza          in     varchar2 default 'A',
                                                    pagina            in     number default 1,
                                                    guia              in     number default 0,
                                                    confirmacao       in     varchar2 default 'N',
                                                    solicitante       in     number   default null,
                                                    UsadoNaWeb        in     varchar2 default 'S',
                                                    taxa              in     number default null,
                                                    cid               in     number default null,
                                                    database          in     date   default sysdate) return number as

-- O parametro pagina indica se a validacao vem da tela de autorizacao de procedimentos (0) ou autorizacao de consultas (1) ou se vem
-- da tela de pedido de exames(2). Caso venha da tela de pedido de exames, o prestador nao precisar� ser informado, entao o sistema nem
-- verificar� se o prestador est� autorizado a executar
-- (3) faturamento on-line, honorario individual

-- Retornos
-- 0  - Validacao OK
-- 1  - Prestador nao autorizado
-- 2  - Procedimento sem cobertura
-- 3  - Paciente em carencia
-- 4  - Retorno
-- 6  - Inadimplencia
-- 7  - N�o pode ser liberado pela internet
-- 8  - Combinacao Dente / Face nao permitida para o procedimento
-- 9  - idade incompativel
-- 10 - Sexo incompativel
-- 11 - Rede nao permitida
-- 12 - Numero de incidencias incompativel
-- 13 - Precisa de auditoria
-- 14 - Mais de uma guia de consulta no mesmo local de atendimento
-- 15 - Estouro de teto de producao
-- 16 - Procedimento n�o existe;
-- 17 - Procedimento nao pode ser liberado pela web por causa da cobertura (cweb_cober)
-- 18 - Procedimento de consulta n�o pode ser liberado na tela de procedimentos
-- 19 - Beneficiario com programacao de cancelamento com copart na guia
-- 20 - Beneficiario nao pode ser liberado pela web por causa da cobertura informar que a copart � no caixa
-- 21 - N�o usar
-- 22 - Guias no mesmo dia, especifico unihosp
-- 23 - Pacote inv�lido (tratado somente no php)
-- 24 - Procedimento cancelado
-- 25 - Bloqueia guias no mesmo dia com menos de 5 min entre uma guia e outra.
-- 26 - Natureza n�o permitida
-- 27 - Regime n�o permitido
-- 28 - Em car�ncia para taxa
-- 29 - Guia sendo enviada para a unimed origem (Intercambio on-line)
-- 30 - Puericultura

autorizado            varchar2(1);
cobertura             number;
cobr_copart           varchar2(1);
tipo                  varchar2(1);
carencia              varchar2(60);
vencimento            date;
retorno               number;
codigo                varchar2(9);
paga                  number;
d                     number;
f                     number;
qtde                  number;
dias                  number;
qtde_mes              number;
qtde_ano              number;
qtde_usada            number;
auditoria             varchar2(1);
id_usuario            number;
guia1                 number;
conta                 number;
vl_consumo            number;
qt_consumo            number;
teto                  number;
CoberturaWeb          varchar2(1);
regra_autorizacao     varchar2(1);
IdadeIncompativel     varchar2(1);
SexoIncompativel      varchar2(1);
PrecisaAuditoria      varchar2(1);
Inadimplencia         varchar2(1);
idade                 number;
sexo                  varchar2(1);
SituacaoUsuario       varchar2(1);
DataCancelamento      date;
Data                  date;
CopartAVista          varchar2(1);
RecebidoCaixa         varchar2(1);
id_critica            number;
tipo_procedimento     varchar2(1);
cnpj                  varchar2(18);
RetornoEncaminhamento varchar2(1);
TYPE CursorCurTyp     IS REF CURSOR;
cr_limite_prestador   CursorCurTyp;
txt                   varchar2(200);
limite                number;
id_limite             number;
tipo_limite           varchar2(1);
limite_quantidade     number;
limite_qtde_mensal    number;
limite_valor          number;
limite_vl_mensal      number;
id_indice             number;
AtitudeEstouroLimite  number;
PercAcresLimite       number;
data_inicio           date;
consumo               number;
situacao              varchar2(1);
natureza_procedimento varchar2(1);
regime_procedimento   varchar2(1);
natureza_limitacao    varchar2(1);
regime_limitacao      varchar2(1);
PodeSerLiberado       varchar2(1);
franquia_valor        number;
franquia              number;
flagPtu               varchar2(4);
ignorar_inadimplencia varchar2(1);
liberar               varchar2(1);
idCongenere           number;
localAtendimento      number;

cursor cr_usuario is
select trunc(months_between(database,dnascusua)/12),
       csexousua,csituusua,dsituusua,nnumeconge
  from hssusua, hsstitu
 where nnumeusua = usuario
   and hssusua.nnumetitu = hsstitu.nnumetitu;

cursor cr_existe is
  select ccodipmed,cpautconf,ctipopmed,ccgc_empr,csitupmed,cambupmed,curgepmed
    from hsspmed,hssconf,finempr
   where ccodipmed = procedimento;

cursor cr_dfpro1 (d in varchar2) is
  select hssdente.nnumedente
    from hssdente,hssdfpro
   where ccodidente = dente
     and hssdente.nnumedente = hssdfpro.nnumedente
     and ccodipmed = procedimento
     and nnumeface is null;

cursor cr_dfpro2 (d in varchar2,
                  f in varchar2) is
  select hssdente.nnumedente,hssdfpro.nnumeface
    from hssdente,hssface,hssdfpro
   where ccodidente = dente
     and hssdente.nnumedente = hssdfpro.nnumedente
     and ccodiface = face
     and hssface.nnumeface = hssdfpro.nnumeface
     and ccodipmed = procedimento;

cursor cr_inadimplencia is
  select hsspaga.nnumepaga
    from hssusua,hssusupg,hsspaga,hsstitu,hssplan,hsslopg,hssconf,hssinter
   where hssusua.nnumeusua = usuario
     and hssusua.ntituusua = hssusupg.nnumeusua
     and hssusupg.nnumepaga = hsspaga.nnumepaga
     and cpagopaga = 'N'
     and dcancpaga is null and dparcpaga is null
     and hsspaga.nnumetitu = hsstitu.nnumetitu
     and ((nvl(cinadtitu,'1') = '1') or (nvl(cinadtitu,'1') = '3'))
     and hssusua.nnumeplan = hssplan.nnumeplan
     and hsspaga.nnumelopg = hsslopg.nnumelopg
     and cflagpaga <> 'F' --Mensalidades da integra��o da farmacia em Barra do Gar�as
     and trunc(database) > vencimento_real( DECODE(CNEGOCONF,'N',HSSPAGA.DVENCPAGA + nvl(decode(hsstitu.nnumeempr,null,ndipfinter,ndipjinter),nvl(ntinatitu,nvl(ntolelopg,nvl(ntinaplan,0)))),NVL(HSSPAGA.DNEGOPAGA,HSSPAGA.DVENCPAGA + nvl(decode(hsstitu.nnumeempr,null,ndipfinter,ndipjinter),nvl(ntinatitu,nvl(ntolelopg,nvl(ntinaplan,0)))))))
     and ((nvl(ccinainter,'S') = 'N' and numero_dias_atraso_consecutivo(hsstitu.nnumetitu) >  ndatcplan and nvl(cciaginter,'N') = 'N') or (nvl(ccinainter,'N') = 'N' and nvl(cciaginter,'N') = 'N')) ;

cursor cr_idade_procedimento (procedimento in varchar2,
                                     idade in number,
                             especialidade in number) is
  select ccodipmed
    from hsspmed
   where hsspmed.ccodipmed = procedimento
     and nidaipmed is not null
     and nidafpmed is not null
     and (idade < nidaipmed or idade > nidafpmed)
union all 
SELECT CCODIPMED 
  FROM HSSAUTPR
 WHERE NNUMEPRES = prestador
   AND ( CCODIPMED = procedimento OR
         SUBSTR(CCODIPMED,1,7) = GRUPOPROCEDIMENTO(procedimento) OR
         SUBSTR(CCODIPMED,1,7) = ESPECIALIDADEPROCEDIMENTO(procedimento) )
   AND NIDAIAUTPR IS NOT NULL
   AND NIDAFAUTPR IS NOT NULL
   AND (idade < nidaiautpr or idade > nidafautpr);   

cursor cr_idade_especialidade (procedimento in varchar2,
                                      idade in number,
                              especialidade in number) is
  select 'X' ccodipmed
    from hssespec
   where hssespec.nnumeespec = especialidade
     and nidaiespec is not null
     and nidafespec is not null
     and (idade < nidaiespec or idade > nidafespec);

cursor cr_idade_prestador (procedimento in varchar2,
                                  idade in number,
                          especialidade in number) is
  select ccodipmed
    from hssautpr
   where hssautpr.nnumepres = prestador
     and (hssautpr.ccodipmed = procedimento or
          substr(hssautpr.ccodipmed,1,7) = grupoprocedimento(procedimento) or
          substr(hssautpr.ccodipmed,1,7) = especialidadeprocedimento(procedimento))
     and nidaiautpr is not null
     and nidafautpr is not null
     and (idade < nidaiautpr or idade > nidafautpr);

cursor cr_sexo (procedimento  in varchar2,
                especialidade in number,
                sexo          in varchar2) is
  select ccodipmed
    from hsspmed
   where hsspmed.ccodipmed = procedimento
     and ( (csexopmed = 'M' and sexo = 'F') or (csexopmed = 'F' and sexo = 'M') )
  union all
  select csexoespec
    from hssespec
   where hssespec.nnumeespec = especialidade
     and ( (csexoespec = 'M' and sexo = 'F') or (csexoespec = 'F' and sexo = 'M') );

cursor cr_incidencias_dia (usuario      in number,
                           procedimento in varchar2,
                           dias         in number,
                           nlim         in varchar2,
                           rlim         in varchar2) is
  select sum(nquanpgui)
    from hssguia,hsspgui
   where nnumeusua = usuario
     and demisguia >= trunc(database) - dias
     and demisguia <  trunc(database) + 1
     and cstatguia is null
     and (nlim = 'B' or ctipoguia = nlim)
     and (rlim = 'A' or curgeguia = rlim)
     and hssguia.nnumeguia = hsspgui.nnumeguia
     and cstatpgui is null
     and ccodipmed in (select ccodipmed from hsspmed
                        where ccodipmed = procedimento
            union all
             select chierpmed from hsspmed  
                        where ccodipmed = procedimento
            union all
             select ctusspmed from hsspmed  
                        where ccodipmed = procedimento)
     and (hssguia.nnumeguia <> nvl(guia,0));

cursor cr_incidencias_mes (usuario      in number,
                           procedimento in varchar2,
                           nlim         in varchar2,
                           rlim         in varchar2) is
  select sum(nquanpgui)
    from hssguia,hsspgui
   where nnumeusua = usuario
     and demisguia >= trunc(first_day(database))
     and demisguia < trunc(last_day(database)) + 1
     and cstatguia is null
     and (nlim = 'B' or ctipoguia = nlim)
     and (rlim = 'A' or curgeguia = rlim)
     and hssguia.nnumeguia = hsspgui.nnumeguia
     and cstatpgui is null
     and ccodipmed in (select ccodipmed from hsspmed
                        where ccodipmed = procedimento
            union all
             select chierpmed from hsspmed  
                        where ccodipmed = procedimento
            union all
             select ctusspmed from hsspmed  
                        where ccodipmed = procedimento)
     and (hssguia.nnumeguia <> nvl(guia,0));

cursor cr_incidencias_ano (usuario      in number,
                           procedimento in varchar2,
                           nlim         in varchar2,
                           rlim         in varchar2) is
  select sum(nquanpgui)
    from hssguia,hsspgui
   where nnumeusua = usuario
     and demisguia >= to_date('01/01/'||to_char(database,'YYYY'),'DD/MM/YYYY')
     and demisguia < to_date('31/12/'||to_char(database,'YYYY'),'DD/MM/YYYY') + 1
     and cstatguia is null
     and (nlim = 'B' or ctipoguia = nlim)
     and (rlim = 'A' or curgeguia = rlim)
     and hssguia.nnumeguia = hsspgui.nnumeguia
     and cstatpgui is null
     and ccodipmed in (select ccodipmed from hsspmed
                        where ccodipmed = procedimento
            union all
             select chierpmed from hsspmed  
                        where ccodipmed = procedimento
            union all
             select ctusspmed from hsspmed  
                        where ccodipmed = procedimento)
     and (hssguia.nnumeguia <> nvl(guia,0));

cursor cr_guias_mesmo_dia (usuario in number,
                           local   in number) is
  select count(*) from hssguia,hsspgui,hsspmed,hssinter
  where trunc(demisguia) = trunc(database)
    and nnumeusua = usuario
    and (nlocapres = local or local = 0)
    and cstatguia is null
    and hssguia.nnumeguia = hsspgui.nnumeguia
    and cstatpgui is null
    and hsspgui.ccodipmed = hsspmed.ccodipmed
    and ctipopmed = 'C'
    and nvl(cblcointer,'N') = 'S';

cursor cr_guias_mesmo_dia2 (usuario       in number,
                            procedimento  in varchar2,
                            especialidade in number,
                            local         in number) is
  select count(*)
   from hssguia,hsspgui
  where to_char(demisguia,'DD/MM/YYYY HH24:mi') between to_char(database-(5/1440),'DD/MM/YYYY HH24:mi') and to_char(database,'DD/MM/YYYY HH24:mi')
    and nnumeusua = usuario
    and (nlocapres = local or local = 0)
    and cstatguia is null
    and hssguia.nnumeguia = hsspgui.nnumeguia
    and hsspgui.ccodipmed = procedimento
    and hssguia.nnumeespec = especialidade
    and cstatpgui is null;

cursor cr_guias_mesmo_dia_proc (usuario      in number,
                                local        in number,
                                procedimento in varchar2) is
  select count(*) from hssguia,hsspgui,hsspmed,hssinter
  where trunc(demisguia) = trunc(database)
    and nnumeusua = usuario
    and (nlocapres = local or local = 0)
    and cstatguia is null
    and hssguia.nnumeguia = hsspgui.nnumeguia
    and cstatpgui is null
    and hsspgui.ccodipmed = procedimento
    and hsspgui.ccodipmed = hsspmed.ccodipmed
    and ctipopmed <> 'C'
    and nvl(cblprinter,'N') = 'S';

cursor cr_guias_consulta_unihosp (usuario in number,
                                  local   in number) is
  select count(*)
    from hssguia,hsspgui,hsspmed,hssinter
   where trunc(demisguia) = trunc(database)
     and nnumeusua = usuario
     and (nlocapres = local or local = 0)
     and cstatguia is null
     and hssguia.nnumeguia = hsspgui.nnumeguia
     and cstatpgui is null
     and hsspgui.ccodipmed = hsspmed.ccodipmed
     and ctipopmed = 'C';

cursor cr_guias_mesmo_dia_odonto (usuario      in number,
                                  local        in number,
                                  procedimento in varchar2,
                                  dente        in number,
                                  face         in number) is
  select count(*) from hssguia,hssdpgui,hsspmed,hssinter
  where trunc(demisguia) = trunc(database)
    and nnumeusua = usuario
    and nlocapres = local
    and hssguia.nnumeguia = hssdpgui.nnumeguia
    and hssdpgui.ccodipmed = procedimento
    and hssdpgui.nnumedente = dente
    and hssdpgui.nnumeface = face
    and hssdpgui.ccodipmed = hsspmed.ccodipmed
    and ctipopmed <> 'C'
    and nvl(cblprinter,'N') = 'S';

cursor cr_coberturaweb is
  select cweb_cober,ccobccober
    from hsscober
   where nnumecober = cobertura;

cursor cr_caixa is
  select 'S'
    from hssgcaix
   where nnumeguia = guia;

cursor cr_critica (guia         in number,
                   procedimento in varchar2,
                   id_critica   in number) is
  select nnumecriti from hsscriti
   where nnumeguia = guia
     and decode(procedimento,null,nnumeguia,ccodipmed) = nvl(procedimento,guia)
     and nmoticriti = id_critica;

cursor cr_ptu (usuario     in number,
               confirmacao in varchar2) is
  select cunimpsaud
    from hssusua,hssplan,hsstitu,hsspsaud,hssconf
   where hssusua.nnumeusua = usuario
     and hssusua.nnumeplan = hssplan.nnumeplan
     and cmodaplan = '2'
     and hssusua.nnumetitu = hsstitu.nnumetitu
     and hsstitu.nnumepsaud = hsspsaud.nnumepsaud
     and cunimpsaud is not null
     and cunimconf is not null
     and nvl(ciautconf,'N') = 'S'
     and confirmacao = 'N'
   and ccolotitu = 'N';

begin
  ignorar_inadimplencia := 'N';
  select ciipuconf into ignorar_inadimplencia from hssconf;
  if (ignorar_inadimplencia = 'S') and (urgencia = 'U') then
    return 0;
  end if;
  
  if procedimento = '10106146' then -- Puericultura
		if cnpj_operadora() = '85204279000188' then
			select puericultura(usuario,guia,quantidade,'S') into liberar from dual;
			if liberar = 'N' then
				return 30;
			end if;    		
		else
			select puericultura(usuario,guia,quantidade) into liberar from dual;
			if liberar = 'N' then
				return 30;
			end if;    
		end if;
  end if;
  
  -- feito paleativamente, depois irei refatorar essa rotina
  if confirmacao = 'S' then
    return valida_confirmacao_web30 (usuario,
                                    procedimento,
                                    quantidade,
                                    prestador,
                                    dente,
                                    face,
                                    especialidade,
                                    local,
                                    urgencia,
                                    natureza,
                                    pagina,
                                    guia,
                                    confirmacao,
                                    solicitante);
  else

    d := null;
    f := null;

    open cr_existe;
    fetch cr_existe into codigo,regra_autorizacao,tipo_procedimento,cnpj,situacao,natureza_procedimento,regime_procedimento;
    if cr_existe%found then
      if situacao = 'A' then
        if ((natureza = 'A' and natureza_procedimento = 'A') or
            (natureza in ('I','C','O','R','P','S') and natureza_procedimento = 'I') or
            (natureza_procedimento = 'B')) then

          if ((urgencia = 'E' and regime_procedimento = 'E') or
              (urgencia = 'U' and regime_procedimento = 'U') or
              (regime_procedimento = 'B')) then

            if nvl(regra_autorizacao,'P') = 'L' then
              PodeSerLiberado := PodeSerLiberadoWeb30 (procedimento,local,usuario,UsadoNaWeb,Confirmacao,codigo,
                                                     tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,
                                                     regime_limitacao);
            else
              PodeSerLiberado := PodeSerLiberadoWeb30 (procedimento,prestador,usuario,UsadoNaWeb,Confirmacao,codigo,
                                                     tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,
                                                     regime_limitacao);
            end if;

            if PodeSerLiberado = 'S' then
              if tipo in ('1','2','3','4','5','6','7','8','9') then
                if (length(dente) > 0 and length(face) > 0) then

                  if dente <> '-1' and face <> '-1' then

                    open cr_dfpro2(dente,face);
                    fetch cr_dfpro2 into d,f;
                    if cr_dfpro2%notfound then
                      return 8;
                    end if;
                    close cr_dfpro2;

                  end if;

                elsif (length(dente) > 0 and length(face) = 0) or
                      (length(dente) > 0 and face is null) then
                  open cr_dfpro1(dente);
                  fetch cr_dfpro1 into d;
                  if cr_dfpro1%notfound then
                    return 8;
                  end if;
                  close cr_dfpro1;
                elsif (length(dente) = 0 and length(face) = 0) or
                      (dente is null and face is null) then
                  select count(*) into d from hssdfpro
                   where ccodipmed = procedimento;
                  if d > 0 then
                    return 8;
                  end if;
                --elsif length(dente) = 0 or dente is null then
                --  return 8;
                end if;
              end if;

              open cr_inadimplencia;
              fetch cr_inadimplencia into paga;
              if cr_inadimplencia%found and TemCriticaNaGuia30(guia,10) = 'S' then
                Inadimplencia := 'N';
              elsif cr_inadimplencia%found then
                Inadimplencia := 'S';
              else
                Inadimplencia := 'N';
              end if;
              
              if (cgc_cliente = '12317012000123') and (urgencia = 'U') then -- asfal
                Inadimplencia := 'N';
              end if;
              
              if Inadimplencia = 'N' then

                open cr_usuario;
                fetch cr_usuario into idade,sexo,SituacaoUsuario,DataCancelamento,idCongenere;
                close cr_usuario;

                if pagina = 2 and prestador = 0 then
                 -- Este if significa que a funcao est� sendo solicitada na tela de pedido de exames e o prestador nao est� sendo informado
                  autorizado := 'S';
                elsif nvl(regra_autorizacao,'P') = 'L' then
                  autorizado := substr(Procedimento_autorizado(local,procedimento,nvl(natureza,'A'),database,urgencia,usuario,especialidade,'N','N',idCongenere,local),1,1);
                else
                  autorizado := substr(Procedimento_autorizado(prestador,procedimento,nvl(natureza,'A'),database,urgencia,usuario,especialidade,'N','N',idCongenere,local),1,1);
                end if;

                --if autorizado = 'N' and TemCriticaNaGuia30(guia,3) = 'S' then
                -- Critica 3 = Prestador nao autorizado
                  --autorizado := 'S';
                --end if;

                if autorizado = 'S' then
                  if nvl(regra_autorizacao,'P') = 'L' then
                    cobertura := cobertura_usuario (usuario,procedimento,nvl(natureza,'A')||urgencia,local,local,cobr_copart,database,especialidade,null,null,idade,'0');
                  else
                    cobertura := cobertura_usuario (usuario,procedimento,nvl(natureza,'A')||urgencia,prestador,local,cobr_copart,database,especialidade,null,null,idade,'0');
                  end if;

                  if cobertura = 0 then
                    cobertura := cobertura_aditivo (usuario,procedimento,cobr_copart,franquia_valor,franquia);
                  end if;

                  if cobertura = 0 and TemCriticaNaGuia30(guia,4) = 'S' then
                    cobertura := 1;
                  end if;

                  if ((cobertura > 0) or (cobertura = -1)) then
                    RecebidoCaixa := 'N';

                    if cobertura = -1 then
                      CoberturaWeb := 'S';
                    elsif confirmacao = 'N' then
                      -- Na tela de confirmacao de autorizacoes nao � necessario testar
                      -- se pode ser liberado na web porque ja foi.
                      open cr_coberturaweb;
                      fetch cr_coberturaweb into CoberturaWeb,cobr_copart;
                      close cr_coberturaweb;

                      if nvl(cobr_copart,'X') = 'C' then
                        open cr_caixa;
                        fetch cr_caixa into RecebidoCaixa;
                        if cr_caixa%notfound then
                          RecebidoCaixa := 'N';
                        end if;
                        close cr_caixa;
                      end if;

                    else
                      CoberturaWeb := 'S';
                    end if;

                    -- Tive que fazer esse remendo para a unimed franca. Nao encontrei forma de parametrizar.
                    -- Resumindo: Quando um atendimento de um contrato que teve copart a vista � solicitado na web menos de 24 horas depois de um outro
                    --            que j� foi autorizado, o sistema deve autorizar mesmo que seja para o contrato que tem copart a vista porque nesse caso
                    --            nao � cobrado copart. O mesmo acontece para casos de encaminhamentos.
                    if (cnpj = '45.309.606/0001-41' and tipo_procedimento = 'C' and urgencia = 'E' and nvl(cobr_copart,'X') = 'C' and RecebidoCaixa = 'N' and Confirmacao = 'N') or
                       (cnpj = '45.309.606/0001-41' and procedimento in ('00010014','00010073') and local = 1162 and  nvl(cobr_copart,'X') = 'C' and RecebidoCaixa = 'N' and Confirmacao = 'N') then
                      RetornoEncaminhamento := RetornoEncaminhamentoUnimed (usuario,guia,null,database,solicitante,local,tipo_procedimento,urgencia,procedimento);
                    else
                      RetornoEncaminhamento := 'N';
                    end if;

                    if nvl(cobr_copart,'X') = 'C' and RecebidoCaixa = 'N' and Confirmacao = 'N' and RetornoEncaminhamento = 'N' and UsadoNaWeb = 'S' then
                      return 20;
                    elsif (nvl(CoberturaWeb,'S') = 'N' and UsadoNaWeb = 'S') and RetornoEncaminhamento = 'N' then
                      return 17;
                    else
-- Par?tro CCRITICA_COPART criado para verificar se critica ou n?a copart de beneficiarios com programa? de cancelamento.
                      if SituacaoUsuario = 'C' and (parametro_string('CCRITICA_COPART') = 'S') and DataCancelamento >= trunc(sysdate) and cobr_copart is not null then

                        if cobr_copart = 'L' or cobr_copart = 'F' then -- ap�s extrapolar limite
                          id_limite := retorna_limite (usuario,Procedimento,tipo_limite,limite_quantidade,
                                                       limite_qtde_mensal,limite_valor,
                                                       limite_vl_mensal,id_indice,AtitudeEstouroLimite,especialidade,
                                                       PercAcresLimite,null,natureza,urgencia,local);
                          if nvl(id_limite,0) > 0 then

                            data_inicio := inicio_contagem_limites (usuario,id_limite,database,especialidade);
                            if Tipo_limite = 'L' then
                              consumo := nvl(total_consumo (id_limite,usuario,data_inicio,database,guia,procedimento,guia,especialidade),0);
                            else
                              consumo := nvl(total_consumo_intercambiavel(id_limite,usuario,data_inicio,database,especialidade),0);
                            end if;

                            if nvl(consumo,0) + quantidade > limite_quantidade then
                              CopartAVista := 'S';
                            else
                              CopartAVista := 'N';
                            end if;

                          else
                            CopartAVista := 'N';
                          end if;

                        else
                          CopartAVista := 'S';
                        end if;
                      else
                        CopartAVista := 'N';
                      end if;

                      if CopartAVista = 'S' then
                        return 19;
                      else
                        id_usuario := usuario;
                        esta_em_carencia (id_usuario,procedimento,vencimento,carencia,null,urgencia,natureza,null,taxa,prestador,local);

                        if nvl(vencimento,database) > database and TemCriticaNaGuia30(guia,5) = 'S' then
                          vencimento := database - 1;
                        end if;

                        if nvl(vencimento,database) <= database then
                          id_usuario := usuario;
                          tem_pre_existencia(id_usuario,procedimento,cid,vencimento,carencia);
                          if nvl(vencimento,database) > database and TemCriticaNaGuia30(guia,5) = 'S' then
                            vencimento := database - 1;
                          end if;
                        end if;

                        if nvl(vencimento,database) <= database then
                          guia1 := guia;
                          Data  := Trunc(database);                                                                           -- local     --0
                          retorno := retorno_procedimento (prestador,usuario,procedimento,Data,guia1,conta,d,f,especialidade,1, NULL, NULL, urgencia, local);

                          if retorno >= 0 and TemCriticaNaGuia30(guia,6) = 'S' then
                            retorno := -1;
                          end if;

                          if retorno < 0 then

                            IdadeIncompativel := 'N';

                            open cr_idade_prestador(procedimento, idade, especialidade);
                            fetch cr_idade_prestador into codigo;
                            if cr_idade_prestador%found then
                              IdadeIncompativel := 'S';
                            else
                              open cr_idade_especialidade(procedimento,idade,especialidade);
                              fetch cr_idade_especialidade into codigo;
                              if cr_idade_especialidade%found then
                                IdadeIncompativel := 'S';
                              else
                                open cr_idade_procedimento(procedimento,idade,especialidade);
                                fetch cr_idade_procedimento into codigo;
                                if cr_idade_procedimento%found then
                                  IdadeIncompativel := 'S';
                                else
                                  IdadeIncompativel := 'N';
                                end if;
                                close cr_idade_procedimento;
                              end if;
                              close cr_idade_especialidade;
                            end if;
                            close cr_idade_prestador;

                            if IdadeIncompativel = 'N' then
                              open cr_sexo (procedimento,especialidade,sexo);
                              fetch cr_sexo into codigo;

                              if cr_sexo%found and TemCriticaNaGuia30(guia,11) = 'S' then
                                SexoIncompativel := 'N';
                              elsif cr_sexo%found then
                                SexoIncompativel := 'S';
                              else
                                SexoIncompativel := 'N';
                              end if;

                              if SexoIncompativel = 'N' then
                                retorno := valida_rede_atendimento30(usuario,local,pagina, procedimento,especialidade);

                                if retorno = 0 and TemCriticaNaGuia30(guia,9) = 'S' then
                                  retorno := 1;
                                end if;

                                if retorno = 0 then
                                  return 11;
                                else
                                  teto := teto_producao(prestador,procedimento,urgencia,trunc(database),qt_consumo,vl_consumo);
                                  if teto <= 0 then

                                    open cr_critica (guia,procedimento,25);
                                    fetch cr_critica into id_critica;
                                    close cr_critica;

                                    if nvl(qtde_mes,0) > 0 and nvl(id_critica,0) = 0 then
                                      open cr_incidencias_mes (usuario,procedimento,natureza_limitacao,regime_limitacao);
                                      fetch cr_incidencias_mes into qtde_usada;
                                      close cr_incidencias_mes;
                                      if nvl(qtde_usada,0)+quantidade > nvl(qtde_mes,0) then
                                        return 12;
                                      end if;
                                    end if;

                                    if nvl(qtde_ano,0) > 0 and nvl(id_critica,0) = 0  then
                                      open cr_incidencias_ano (usuario,procedimento,natureza_limitacao,regime_limitacao);
                                      fetch cr_incidencias_ano into qtde_usada;
                                      close cr_incidencias_ano;
                                      if nvl(qtde_usada,0)+quantidade > nvl(qtde_ano,0) then
                                        return 12;
                                      end if;
                                    end if;

                                    if nvl(qtde,0) > 0 and nvl(dias,0) > 0 and nvl(id_critica,0) = 0  then
                                      open cr_incidencias_dia (usuario,procedimento,dias,natureza_limitacao,regime_limitacao);
                                      fetch cr_incidencias_dia into qtde_usada;
                                      close cr_incidencias_dia;
                                      if nvl(qtde_usada,0)+quantidade > nvl(qtde,0) then
                                        return 12;
                                      else

                                        if (nvl(auditoria,'N') = 'S') or
                                           (nvl(auditoria,'N') = 'A' and natureza = 'A') or
                                           (nvl(auditoria,'N') = 'I' and natureza = 'I') then
                                           PrecisaAuditoria := 'S';
                                        end if;

                                        if PrecisaAuditoria = 'S' and TemCriticaNaGuia30(guia,7) = 'S' then
                                          PrecisaAuditoria := 'N';
                                        end if;

                                        if PrecisaAuditoria = 'S' then
                                          return 13;
                                        else
                                          if nvl(tipo,'I') = 'C' then

                                            open cr_guias_mesmo_dia(usuario,nvl(local,prestador));
                                            fetch cr_guias_mesmo_dia into qtde_usada;
                                            close cr_guias_mesmo_dia;
                                            if nvl(qtde_usada,0) > 0 and TemCriticaNaGuia30(guia,6) = 'N' then
                                              return 14;
                                            else
                                              if cnpj_operadora() = '04083773000130' then
                                                txt := 'SELECT LIMITE FROM FINPRES WHERE NNUMEPRES = :1';
                                                open cr_limite_prestador for txt using nvl(local,prestador);
                                                fetch cr_limite_prestador into limite;
                                                close cr_limite_prestador;

                                                if limite is not null then
                                                  open cr_guias_consulta_unihosp(usuario,nvl(local,prestador));
                                                  fetch cr_guias_consulta_unihosp into qtde_usada;
                                                  close cr_guias_consulta_unihosp;
                                                  if nvl(qtde_usada,0) > limite then
                                                    return 22;
                                                  else
                                                    open cr_ptu (usuario,confirmacao);
                                                    fetch cr_ptu into flagPtu;
                                                    close cr_ptu;
                                                    if flagPtu is not null then
                                                      return 29;
                                                    else
                                                      return 0;
                                                    end if;
                                                  end if;
                                                else
                                                  open cr_ptu (usuario,confirmacao);
                                                  fetch cr_ptu into flagPtu;
                                                  close cr_ptu;
                                                  if flagPtu is not null then
                                                    return 29;
                                                  else
                                                    return 0;
                                                  end if;
                                                end if;
                                              else
                                                open cr_ptu (usuario,confirmacao);
                                                fetch cr_ptu into flagPtu;
                                                close cr_ptu;
                                                if flagPtu is not null then
                                                  return 29;
                                                else
                                                  return 0;
                                                end if;
                                              end if;
                                            end if;
                                          else
                                            open cr_guias_mesmo_dia_proc(usuario,nvl(local,prestador),procedimento);
                                            fetch cr_guias_mesmo_dia_proc into qtde_usada;
                                            close cr_guias_mesmo_dia_proc;
                                            if nvl(qtde_usada,0) > 0 and TemCriticaNaGuia30(guia,6) = 'N' then
                                              return 14;
                                            else
                                              open cr_ptu (usuario,confirmacao);
                                              fetch cr_ptu into flagPtu;
                                              close cr_ptu;
                                              if flagPtu is not null then
                                                return 29;
                                              else
                                                return 0;
                                              end if;
                                            end if;
                                          end if;
                                        end if;
                                      end if;
                                    else
                                      if (nvl(auditoria,'N') = 'S') or
                                         (nvl(auditoria,'N') = 'A' and natureza = 'A') or
                                         (nvl(auditoria,'N') = 'I' and natureza = 'I') then
                                        PrecisaAuditoria := 'S';
                                      end if;

                                      if PrecisaAuditoria = 'S' and TemCriticaNaGuia30(guia,7) = 'S' then
                                        PrecisaAuditoria := 'N';
                                      end if;

                                      if PrecisaAuditoria = 'S' then
                                        return 13;
                                      else
                                        -- Verifica se houve emissao de uma mesma guia nos ultimos 5 min
                                        -- evitando a libera��o de guia em duplicidade
                                        open cr_guias_mesmo_dia2(usuario,procedimento,especialidade,nvl(local,prestador));
                                        fetch cr_guias_mesmo_dia2 into qtde_usada;
                                        close cr_guias_mesmo_dia2;

                                        if nvl(qtde_usada,0) > 0 then
                                          return 25;
                                        else
                                          if nvl(tipo,'I') = 'C' then
                                            open cr_guias_mesmo_dia(usuario,nvl(local,prestador));
                                            fetch cr_guias_mesmo_dia into qtde_usada;
                                            close cr_guias_mesmo_dia;
                                            if nvl(qtde_usada,0) > 0 and TemCriticaNaGuia30(guia,6) = 'N' then
                                              return 14;
                                            else
                                              if cnpj_operadora() = '04083773000130' then
                                                txt := 'SELECT LIMITE FROM FINPRES WHERE NNUMEPRES = :1';
                                                open cr_limite_prestador for txt using nvl(local,prestador);
                                                fetch cr_limite_prestador into limite;
                                                close cr_limite_prestador;

                                                if limite is not null then
                                                  open cr_guias_consulta_unihosp(usuario,nvl(local,prestador));
                                                  fetch cr_guias_consulta_unihosp into qtde_usada;
                                                  close cr_guias_consulta_unihosp;

                                                  if nvl(qtde_usada,0) > limite then
                                                    return 22;
                                                  else
                                                    open cr_ptu (usuario,confirmacao);
                                                    fetch cr_ptu into flagPtu;
                                                    close cr_ptu;
                                                    if flagPtu is not null then
                                                      return 29;
                                                    else
                                                      return 0;
                                                    end if;
                                                  end if;
                                                else
                                                  open cr_ptu (usuario,confirmacao);
                                                  fetch cr_ptu into flagPtu;
                                                  close cr_ptu;
                                                  if flagPtu is not null then
                                                    return 29;
                                                  else
                                                    return 0;
                                                  end if;

                                                end if;
                                              else

                                                open cr_ptu (usuario,confirmacao);
                                                fetch cr_ptu into flagPtu;
                                                close cr_ptu;
                                                if flagPtu is not null then
                                                  return 29;
                                                else
                                                  return 0;
                                                end if;

                                              end if;
                                            end if;
                                          else
                                            if (d is not null) then
                                              open cr_guias_mesmo_dia_odonto(usuario,nvl(local,prestador),procedimento,d,f);
                                              fetch cr_guias_mesmo_dia_odonto into qtde_usada;
                                              close cr_guias_mesmo_dia_odonto;
                                            else
                                              open cr_guias_mesmo_dia_proc(usuario,nvl(local,prestador),procedimento);
                                              fetch cr_guias_mesmo_dia_proc into qtde_usada;
                                              close cr_guias_mesmo_dia_proc;
                                            end if;

                                            if nvl(qtde_usada,0) > 0 and TemCriticaNaGuia30(guia,6) = 'N' then
                                              return 14;
                                            else

                                               open cr_ptu (usuario,confirmacao);
                                              fetch cr_ptu into flagPtu;
                                              close cr_ptu;
                                              if flagPtu is not null then
                                                return 29;
                                              else
                                                return 0;
                                              end if;

                                            end if;
                                          end if;
                                        end if;
                                      end if;
                                    end if;
                                  else
                                    return 15;
                                  end if;
                                end if;
                              else
                                return 10;
                              end if;
                              close cr_sexo;
                            else
                              return 9;
                            end if;
                          else
                            return 4;
                          end if;
                        elsif nvl(vencimento,database) <= database and taxa > 0 then
                          return 28;
                        else
                          return 3;
                        end if;
                      end if;
                    end if;
                  else
                    return 2;
                  end if;
                else
                  return 1;
                end if;
              else
                return 6;
              end if;
              close cr_inadimplencia;
            else
              return 7;
            end if;
          else
            return 27;
          end if;
        else
          return 26;
        end if;
      else
        return 24;
      end if;
    else
      return 16;
    end if;
    close cr_existe;
  end if;
end;
/

create or replace function valida_confirmacao_web30 (usuario           in     number,
                                                   procedimento      in     varchar2,
                                                   quantidade        in     number,
                                                   prestador         in     number,
                                                   dente             in     varchar2,
                                                   face              in     varchar2,
                                                   especialidade     in     number,
                                                   local             in     number default null,
                                                   urgencia          in     varchar2 default 'E',
                                                   regime            in     varchar2 default 'A',
                                                   pagina            in     number default 1,
                                                   guia              in     number default 0,
                                                   confirmacao       in     varchar2 default 'N',
                                                   solicitante       in     number default null) return number as

-- Retornos 
-- 0  - Validacao OK
-- 1  - Prestador nao autorizado
-- 2  - Procedimento sem cobertura
-- 4  - Retorno
-- 11 - Rede nao permitida
-- 21 - N�o usar

autorizado            varchar2(1);
cobertura             number;
copart                number;
copart_valor          number;
cobr_copart           varchar2(1);
adm                   number;
cobr_adm              varchar2(1);
tipo                  varchar2(1);
carencia              varchar2(60);
vencimento            date;
validade              date;
retorno               number;
codigo                varchar2(9);
paga                  number;
d                     number;
f                     number;
qtde                  number;
dias                  number;
qtde_mes              number;
qtde_ano              number;
qtde_usada            number;
auditoria             varchar2(1);
id_usuario            number;
guia1                 number;
conta                 number;
vl_consumo            number;
qt_consumo            number;
teto                  number;
CoberturaWeb          varchar2(1);
regra_autorizacao     varchar2(1);
IdadeIncompativel     varchar2(1);
SexoIncompativel      varchar2(1);
PrecisaAuditoria      varchar2(1);
Inadimplencia         varchar2(1);
natureza              varchar2(1);
idade                 number;
sexo                  varchar2(1);
SituacaoUsuario       varchar2(1);
DataCancelamento      date;
Data                  date;
CopartAVista          varchar2(1);
RecebidoCaixa         varchar2(1);
id_critica            number;
tipo_procedimento     varchar2(1);
cnpj                  varchar2(18);
RetornoEncaminhamento varchar2(1);
TYPE CursorCurTyp     IS REF CURSOR;
cr_limite_prestador   CursorCurTyp;
txt                   varchar2(200);
limite                number;
id_limite             number;
tipo_limite           varchar2(1);
limite_quantidade     number;
limite_qtde_mensal    number;
limite_valor          number;
limite_vl_mensal      number;
id_indice             number;
AtitudeEstouroLimite  number;
PercAcresLimite       number;
data_inicio           date;
consumo               number;
situacao              varchar2(1);

cursor cr_usuario is
select trunc(months_between(sysdate,dnascusua)/12),
       csexousua,csituusua,dsituusua
  from hssusua
 where nnumeusua = usuario;

cursor cr_existe is
  select ccodipmed,cpautconf,ctipopmed,ccgc_empr,csitupmed
    from hsspmed,hssconf,finempr
   where ccodipmed = procedimento;

/* quando marcado para liberar pela internet, toda parametriza��o abaixo deve ser feita no procedimento */
cursor cr_internet is
  select ccodipmed,ctipopmed,nmlibpmed,npconpmed,nautmpmed,nautapmed,caudipmed from hsspmed
   where ccodipmed = procedimento
     and (clintpmed = 'S' or confirmacao = 'S');

cursor cr_coberturaweb is
  select cweb_cober,ccobccober from hsscober
   where nnumecober = cobertura;

cursor cr_caixa is
  select 'S'
    from hssgcaix  
   where nnumeguia = guia;

cursor cr_critica (guia         in number,
                   procedimento in varchar2,
                   id_critica   in number) is
  select nnumecriti from hsscriti
   where nnumeguia = guia
     and decode(procedimento,null,nnumeguia,ccodipmed) = nvl(procedimento,guia)
     and nmoticriti = id_critica;

cursor cr_validade is
  select dvaliguia
    from hssguia 
   where nnumeguia = guia;                  
   
begin
  select ccgc_empr into cnpj from finempr;
  
  d := null;
  f := null;
  natureza := 'A';
                    
  open cr_validade;
  fetch cr_validade into validade;
  close cr_validade;
 
  if Trunc(Sysdate)  > trunc(validade) then
    return 26;   
  else
  
    open cr_usuario;
    fetch cr_usuario into idade,sexo,SituacaoUsuario,DataCancelamento;
    close cr_usuario;          

    if pagina = 2 and prestador = 0 then
     -- Este if significa que a funcao est� sendo solicitada na tela de pedido de exames e o prestador nao est� sendo informado
      autorizado := 'S';
    elsif nvl(regra_autorizacao,'P') = 'L' then
      autorizado := substr(Procedimento_autorizado(local,procedimento,natureza,sysdate,urgencia,usuario,especialidade),1,1);
    else
      autorizado := substr(Procedimento_autorizado(prestador,procedimento,natureza,sysdate,urgencia,usuario,especialidade),1,1);
    end if;    

    if autorizado = 'N' and TemCriticaNaGuia30(guia,3) = 'S' then
    -- Critica 3 = Prestador nao autorizado
      autorizado := 'S';
    end if;

    if autorizado = 'S' then
      if nvl(regra_autorizacao,'P') = 'L' then
        cobertura := cobertura_usuario (usuario,procedimento,natureza||urgencia,local,local,cobr_copart,sysdate,especialidade,null,null,idade,'0');
      else
        cobertura := cobertura_usuario (usuario,procedimento,natureza||urgencia,prestador,local,cobr_copart,sysdate,especialidade,null,null,idade,'0');
      end if;  
      
      if (cobertura <= 0 and TemCriticaNaGuia30(guia,4) = 'S') or (cnpj = '45.309.606/0001-41' ) then
        cobertura := 1;
      end if;    
      
      if cobertura > 0 then                                                                                 --0  
        retorno := retorno_procedimento (prestador,usuario,procedimento, Data, guia1,conta,d,f,especialidade,local, NULL, NULL, urgencia);

        if retorno >= 0 and TemCriticaNaGuia30(guia,6) = 'S' then
          retorno := -1;
        end if;

        if retorno < 0 then

          retorno := valida_rede_atendimento30(usuario,local,pagina, procedimento,especialidade);

          if retorno = 0 and TemCriticaNaGuia30(guia,9) = 'S' then
            retorno := 1;
          end if;

          if retorno = 0 then
            return 11;
      else
        return 0;
          end if;
        else
          return 4;
        end if;
        
      else
        return 2;
      end if;        
    else
      return 1;
    end if;  
  end if;
end;
/

create or replace procedure insere_alteracao_web30 (operacao          in     varchar2,
                                                    usuario           in     number,
                                                    operador          in     number,
                                                    nome              in     varchar2,
                                                    categoria         in     varchar2,
                                                    nascimento        in     date,
                                                    estadonasc        in     varchar2,
                                                    cidadenasc        in     varchar2,
                                                    sexo              in     varchar2,
                                                    matricula         in     varchar2,
                                                    mae               in     varchar2,
                                                    pai               in     varchar2,
                                                    cpf               in     varchar2,
                                                    rg                in     varchar2,
                                                    orgao             in     varchar2,
                                                    pis               in     varchar2,
                                                    grau              in     varchar2,
                                                    locacao           in     number,
                                                    admissao          in     date,
                                                    endereco          in     varchar2,
                                                    bairro            in     varchar2,
                                                    cidade            in     varchar2,
                                                    estado            in     varchar2,
                                                    cep               in     varchar2,
                                                    ini_afast         in     date,
                                                    fim_afast         in     date,
                                                    motivo            in     varchar2,
                                                    plano             in     number,
                                                    acomodacao        in     number,
                                                    expedicao         in     date,
                                                    numero            in     varchar2,
                                                    tlogr             in     number,
                                                    compl             in     varchar2,                           
                                                    email             in     varchar2,
                                                    telefone          in     varchar2, 
                                                    salario           in     number,
                                                    motivo_canc       in     number,
                                                    dt_canc           in     date,
                                                    estado_civil      in     varchar2,
                                                    profissao         in     number,
                                                    setor             in     number,
                                                    local             in     varchar2,
                                                    vendedor          in     number,
                                                    demissao          in     date,
                                                    csus              in     varchar2,
                                                    ndnv              in     number,
                                                    idatcad           in     number,
                                                    re                in     number,                                                   
                                                    retorno           out    number,
                                                    uniao             in     date,
                                                    centro_custo      in     number,                                                   
                                                    ip                in     varchar2,
                                                    peso              in     number,
                                                    altura            in     number,
                                                    cidadeatendimento in     varchar2,
                                                    data_plano_prog   in     date default sysdate,
                                                    tel_comercial     in     varchar2 default null,
                                                    tel_celular       in     varchar2 default null) as
-- retornos                                                                     
-- -1 - n�o � poss�vel cadastrar usuario com nome vazio                                                         
-- -3 - n�o � poss�vel cadastrar usuario sem sexo                                              
-- -4 - n�o � poss�vel cadastrar usuario sem o nome da m�e ou pis                                                   
                                                   
nomeA              varchar2(60);
categoriaA         varchar2(1);
nascimentoA        date;
estadonascA        varchar2(60);
cidadenascA        varchar2(60);
sexoA              varchar2(1);
matriculaA         varchar2(20);
maeA               varchar2(60);
paiA               varchar2(60);
cpfA               varchar2(14);
rgA                varchar2(14);
pisA               varchar2(11);
grauA              varchar2(1);
orgaoA             varchar2(10);
admissaoA          date;
demissaoA          date;
locacaoA           number;
contrato           number;
titular            number;
enderecoA          varchar2(60);
bairroA            varchar2(60);
cidadeA            varchar2(60);
estadoA            varchar2(2);
cepA               varchar2(8);
cgc                varchar2(18);
ini_afastA         date;
fim_afastA         date;
motivoA            varchar2(1);
planoA             number;
acomodacaoA        number;
expedicaoA         date;
numeroA            varchar2(10);
tlogrA             number;
complA             varchar2(60);
emailA             varchar2(100);
telefoneA          varchar2(60);
salarioA           number;
id                 number;
estado_civilA      varchar2(1);
profissaoA         number;
setorA             number;
id_locacao         number;
id_vendedor        number;
csusA              varchar2(20);
ndnvA              number;
uniaoA             date;
centro_custoA      number;
id_centro_custo    number;
pesoA              number;
alturaA            number;
cidadeatendimentoA varchar2(60);
inclusao_planoA    date; 
tel_comercialA     varchar2(20);
tel_celularA       varchar2(20);

contrato_de_pcmso varchar2(1);

  cursor cr_afastado is 
    select dinicafast,dfinaafast,cmotiafast
      from hssafast     
     where nnumeusua = usuario
       and dfinaafast is null;
       
  cursor cr_telefone is
  select cfonetlusu 
    from hsstlusu
   where cfonetlusu is not null
     and nnumeusua = usuario
   order by nnumetlusu desc;   
   
  cursor cr_telefone2 (tipo in varchar2,
                       usu  in number) is
  select somente_numeros_string(cfonetlusu) 
    from hsstlusu
   where cfonetlusu is not null
     and ctipotlusu = tipo
     and nnumeusua = usu;    
   
  cursor cr_cidade_repasse is
  select ccidapsaud 
    from hsspsaud
   where nnumepsaud in (select max(natenpsaud) from hssrepas 
                        where dcancrepas is null 
                        and ctiporepas = 'R' 
                        and nnumeusua = usuario);

   procedure insere_atualizacao (id        in number,
                                 tabela    in varchar2,
                                 campo     in varchar2,
                                 descricao in varchar2,
                                 novo      in varchar2,
                                 anterior  in varchar2,
                                 re        in number
                                 ) as
   rev          number;
   begin
     if (re = 1) then
       select count(*)
         into rev
         from hssatuca
        where nnumeatcad = id
          and ctabeatuca = tabela
          and ccampatuca = campo;
          
       if( rev > 0 and ( nvl(novo,' ') <> nvl(anterior,' ')) ) then
       
         update hssatuca 
            set cnovoatuca = Trim(novo), 
                canteatuca = Trim(anterior) 
          where nnumeatcad = id 
            and ctabeatuca = tabela 
            and ccampatuca = campo;
            
       else if nvl(novo,' ') <> nvl(anterior,' ') then
         insert into hssatuca (nnumeatcad,ctabeatuca,ccampatuca,cdescatuca,cnovoatuca,canteatuca)
             values (id,tabela,campo,descricao,Trim(novo),Trim(anterior));
         end if;
       end if;
     else
       if nvl(novo,' ') <> nvl(anterior,' ')  then 
             insert into hssatuca (nnumeatcad,ctabeatuca,ccampatuca,cdescatuca,cnovoatuca,canteatuca)
             values (id,tabela,campo,descricao,Trim(novo),Trim(anterior));
       end if;
     end if;
   end;

begin 
  altera_application_info(operador);

  select nvl(caso_titu,'N') into contrato_de_pcmso from hsstitu,hssusua
   where hssusua.nnumeusua = usuario
     and hsstitu.nnumetitu = hssusua.nnumetitu;
   
  if locacao > 0 then
    id_locacao := locacao;
  else
    id_locacao := null;
  end if;
  
  if vendedor > 0 then
    id_vendedor := vendedor;
  else
    id_vendedor := null;
  end if;  
  
  if centro_custo > 0 then
    id_centro_custo := centro_custo;
  else
    id_centro_custo := null;
  end if;    
  
  select cnomeusua,ctipousua,dnascusua,cestnusua,ccidnusua,csexousua,cchapusua,cnmaeusua,cnpaiusua,c_cpfusua,c__rgusua,
         cpis_usua,cgrauusua,hssusua.nnumesetor,hssusua.nnumetitu,ntituusua,corrgusua,
         dadmiusua,cendeusua,cbairusua,ccidausua,cestausua,ccep_usua,hssusua.nnumeplan,
         nvl(hssusua.nnumeacom,nvl(hsstitu.nnumeacom,hssplan.nnumeacom)) nnumeacom,dexrgusua,
         hssusua.cmailusua,nsalafunci,cestcusua,nnumeprof,nnumedepar,hssusua.ddemiusua,csus_usua,ndcnvusua,crefeusua,cnumeusua,
         hssusua.nfatutlogr,duestusua,nnumececus,npesousua,naltuusua
    into nomeA,categoriaA,nascimentoA,estadonascA,cidadenascA,sexoA,matriculaA,maeA,paiA,cpfA,rgA,pisA,grauA,locacaoA,contrato,titular,
         orgaoA,admissaoA,enderecoA,bairroA,cidadeA,estadoA,cepA,planoA,acomodacaoA,expedicaoA,
         emailA,salarioA,estado_civilA,profissaoA,setorA,demissaoA,csusA,ndnvA,complA,numeroA,
         tlogrA,uniaoA,centro_custoA,pesoA,alturaA
    from hssusua,hsstitu,hssplan 
   where nnumeusua = usuario
     and hssusua.nnumetitu = hsstitu.nnumetitu
     and hssusua.nnumeplan = hssplan.nnumeplan;
  
  --if operacao = 'C' then
  --  locacaoA := null;
    --  id_locacao := null;
  --end if;
  
  open cr_afastado;
  fetch cr_afastado into ini_afastA,fim_afastA,motivoA;
  if cr_afastado%notfound then
    ini_afastA := null;
    fim_afastA := null;
    motivoA := null;
  end if;
  close cr_afastado;
  
  open cr_cidade_repasse;
  fetch cr_cidade_repasse into cidadeatendimentoA;
  if cr_cidade_repasse%notfound then
    cidadeatendimentoA := null;
  end if;
  close cr_cidade_repasse;
  
  cgc := cnpj_operadora();
  
  tel_comercialA := null;
  tel_celularA := null;
  
  if (cgc = '73717639000166') then -- F�tima Sa�de devido a integra��o com o LIFE
    if length(somente_numeros(telefone)) > 0 then        
      open cr_telefone2('R',usuario);
      fetch cr_telefone2 into telefoneA;
      close cr_telefone2;   
      telefoneA := trim(formata_telefone(telefoneA));      
    end if;

    if length(somente_numeros(tel_comercial)) > 0 then        
      open cr_telefone2('T',usuario);
      fetch cr_telefone2 into tel_comercialA;
      close cr_telefone2;  
      tel_comercialA := trim(formata_telefone(tel_comercialA));      
    end if;
    
    if length(somente_numeros(tel_celular)) > 0 then        
      open cr_telefone2('C',usuario);
      fetch cr_telefone2 into tel_celularA;
      close cr_telefone2;  
      tel_celularA := trim(formata_telefone(tel_celularA));       
    end if; 
  
  else  
    open cr_telefone;
    fetch cr_telefone into telefoneA;
    close cr_telefone;     
  end if;    
  
  if nome is null then                                                      
    retorno := -1;                                                           
  elsif sexo is null then                                                   
    retorno := -3;                                                           
  elsif ((mae is null) and (pis is null) and (contrato_de_pcmso = 'N')) then                                                   
    retorno := -4;  
  elsif nvl(nome,' ')                 <> nvl(nomeA,' ')                   or 
     nvl(categoria,' ')               <> nvl(categoriaA,' ')              or 
     nvl(nascimento,trunc(sysdate))   <> nvl(nascimentoA,trunc(sysdate))  or
     nvl(estadonasc,' ')              <> nvl(estadonascA,' ')       or
     nvl(cidadenasc,' ')              <> nvl(cidadenascA,' ')       or
     nvl(sexo,' ')                    <> nvl(sexoA,' ')                   or 
     nvl(matricula,' ')               <> nvl(matriculaA,' ')              or
     nvl(pai,' ')                     <> nvl(paiA,' ')                    or
     nvl(mae,' ')                     <> nvl(maeA,' ')                    or
     nvl(cpf,' ')                     <> nvl(cpfA,' ')                    or 
     nvl(rg,' ')                      <> nvl(rgA,' ')                     or
     nvl(pis,' ')                     <> nvl(pisA,' ')                    or 
     nvl(grau,' ')                    <> nvl(grauA,' ')                   or
     nvl(id_locacao,0)                <> nvl(locacaoA,0)                  or
     nvl(orgao,' ')                   <> nvl(orgaoA,' ')                  or
     nvl(admissao,trunc(sysdate))     <> nvl(admissaoA,trunc(sysdate))    or
     nvl(demissao,trunc(sysdate))     <> nvl(demissaoA,trunc(sysdate))    or
     nvl(endereco,' ')                <> nvl(enderecoA,' ')               or
     nvl(bairro,' ')                  <> nvl(bairroA,' ')                 or
     nvl(cidade,' ')                  <> nvl(cidadeA,' ')                 or
     nvl(estado,' ')                  <> nvl(estadoA,' ')                 or
     nvl(cep,' ')                     <> nvl(cepA,' ')                    or
     nvl(ini_afast,trunc(sysdate))    <> nvl(ini_afastA,trunc(sysdate))   or 
     nvl(fim_afast,trunc(sysdate))    <> nvl(fim_afastA,trunc(sysdate))   or 
     nvl(motivo,' ')                  <> nvl(motivoA,' ')                 or
     nvl(acomodacao,0)                <> nvl(acomodacaoA,0)               or
     nvl(expedicao,trunc(sysdate))    <> nvl(expedicaoA,trunc(sysdate))   or
     nvl(compl,' ')                   <> nvl(complA,' ')                  or
     nvl(numero,' ')                  <> nvl(numeroA,' ')                 or
     nvl(tlogr,0)                     <> nvl(tlogrA,0)                    or
     nvl(plano,0)                     <> nvl(planoA,0)                    or
     nvl(email,' ')                   <> nvl(emailA,' ')                  or
     nvl(telefone,' ')                <> nvl(telefoneA,' ')               or
     nvl(tel_comercial,' ')           <> nvl(tel_comercialA,' ')          or
     nvl(tel_celular,' ')             <> nvl(tel_celularA,' ')            or     
     nvl(salario,0)                   <> nvl(salarioA,0)                  or
     nvl(motivo_canc,0)               <> 0                                or
     nvl(dt_canc,trunc(sysdate))      <> trunc(sysdate)                   or
     nvl(profissao,0)                 <> nvl(profissaoA,0)                or
     nvl(setor,0)                     <> nvl(setorA,0)                    or
     nvl(csus,' ')                    <> nvl(csusA,' ')                   or
     nvl(ndnv,0)                      <> nvl(ndnvA,0)                     or     
     nvl(peso,0)                      <> nvl(pesoA,0)                     or     
     nvl(altura,0)                    <> nvl(alturaA,0)                   or     
     nvl(estado_civil,' ')            <> nvl(estado_civilA,' ')           or 
     nvl(centro_custo,0)              <> nvl(centro_custoA,0)             or 
     nvl(cidadeatendimento,0)         <> nvl(cidadeatendimentoA,0)        or 
     nvl(uniao,trunc(sysdate))        <> nvl(uniaoA,trunc(sysdate))       then 

    if ( re = 0 and idatcad = 0) then 
      select seqatcad.nextval into id from dual;
      insert into hssatcad (nnumeatcad,nnumeusua,coperatcad,cnomeatcad,noperusua,ddataatcad,nnumetitu,ntituusua,nnumesetor,clocaatcad,nnumevend,cnmaeatcad,cnpaiatcad,dnascatcad,cestnatcad,ccidnatcad,
                            csexoatcad,cmailatcad,cestaatcad,cestcatcad,c_cpfatcad,c__rgatcad,cpis_atcad,dexrgatcad,corrgatcad,cbairatcad, ccidaatcad,cgrauatcad,ctipoatcad,
                            dadmiatcad,cchapatcad,ccep_atcad,cendeatcad,cnumeatcad,nnumeplan,nfatutlogr,nsalaatcad,nnumeprof,cfoneatcad,cmafaatcad, nnumeacom,
                            nnumemcanc,duestatcad,cip__atcad,ccloaatcad)
                    values (id,usuario,operacao,nome,operador,sysdate,contrato,titular,id_locacao,local,id_vendedor,mae,pai,nascimento,estadonasc,cidadenasc,sexo,email,estado,estado_civil,cpf,rg,
                            pis,expedicao,orgao,bairro,cidade,grau,categoria,admissao,matricula,cep,endereco,numero,plano,tlogr,salario,profissao,telefone,motivo,acomodacao,
                            motivo_canc,uniao,ip,cidadeatendimento);
    else
      update hssatcad set ddataatcad = sysdate where nnumeatcad = idatcad;
      id := idatcad;
    end if;
    insere_atualizacao(id,'HSSUSUA','CNOMEUSUA','Nome',nome,nomeA,re);
    insere_atualizacao(id,'HSSUSUA','CTIPOUSUA','Categoria',categoria,categoriaA,re);
    insere_atualizacao(id,'HSSUSUA','DNASCUSUA','Data de nascimento',to_char(nascimento,'DD/MM/YYYY'),to_char(nascimentoA,'DD/MM/YYYY'),re);    
    insere_atualizacao(id,'HSSUSUA','CESTNUSUA','Estado de nascimento',estadonasc,estadonascA,re);
    insere_atualizacao(id,'HSSUSUA','CCIDNUSUA','Cidade de nascimento',cidadenasc,cidadenascA,re);
    insere_atualizacao(id,'HSSUSUA','CSEXOUSUA','Sexo',sexo,sexoA,re);
    insere_atualizacao(id,'HSSUSUA','CCHAPUSUA','Matr�cula',matricula,matriculaA,re);
    insere_atualizacao(id,'HSSUSUA','CNMAEUSUA','Nome da m�e',mae,maeA,re);
    insere_atualizacao(id,'HSSUSUA','CNPAIUSUA','Nome do pai',pai,paiA,re);
    insere_atualizacao(id,'HSSUSUA','C_CPFUSUA','C.P.F.',cpf,cpfA,re);
    insere_atualizacao(id,'HSSUSUA','C__RGUSUA','R.G.',rg,rgA,re);
    insere_atualizacao(id,'HSSUSUA','CPIS_USUA','P.I.S',pis,pisA,re);
    insere_atualizacao(id,'HSSUSUA','CGRAUUSUA','Parentesco',grau,grauA,re);
    insere_atualizacao(id,'HSSUSUA','NNUMESETOR','Loca��o',to_char(id_locacao,'9999999999999999'),to_char(locacaoA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','CORRGUSUA','�rg�o expedidor do R.G.',orgao,orgaoA,re);
    insere_atualizacao(id,'HSSUSUA','DADMIUSUA','Data de admiss�o',to_char(admissao,'DD/MM/YYYY'),to_char(admissaoA,'DD/MM/YYYY'),re);
    insere_atualizacao(id,'HSSUSUA','DDEMIUSUA','Data de demiss�o',to_char(demissao,'DD/MM/YYYY'),to_char(demissaoA,'DD/MM/YYYY'),re);
    insere_atualizacao(id,'HSSUSUA','CENDEUSUA','Logradouro',endereco,enderecoA,re);
    insere_atualizacao(id,'HSSUSUA','CBAIRUSUA','Bairro',bairro,bairroA,re);
    insere_atualizacao(id,'HSSUSUA','CCIDAUSUA','Cidade',cidade,cidadeA,re);
    insere_atualizacao(id,'HSSUSUA','CESTAUSUA','Estado',estado,estadoA,re);
    insere_atualizacao(id,'HSSUSUA','CCEP_USUA','C.E.P.',cep,cepA,re);
    
    if operacao <> 'C' then    
      insere_atualizacao(id,'HSSAFAST','DINICAFAST','Data de in�cio do afastamento',to_char(ini_afast,'DD/MM/YYYY'),to_char(ini_afastA,'DD/MM/YYYY'),re);
      insere_atualizacao(id,'HSSAFAST','DFINAAFAST','Data final do afastamento',to_char(fim_afast,'DD/MM/YYYY'),to_char(fim_afastA,'DD/MM/YYYY'),re);
      insere_atualizacao(id,'HSSAFAST','CMOTIAFAST','Motivo do afastamento',motivo,motivoA,re);
      end if;
    
    if (acomodacao > 0) then
      insere_atualizacao(id,'HSSUSUA','NNUMEACOM','Acomoda��o',to_char(acomodacao,'9999999999999999'),to_char(acomodacaoA,'9999999999999999'),re);
    end if;
    
    insere_atualizacao(id,'HSSUSUA','CSUS_USUA','N�mero do cart�o SUS',csus,csusA,re);
    
    if ndnv > 0 then
      insere_atualizacao(id,'HSSUSUA','NDCNVUSUA','Declara��o de nascido vivo',ndnv,ndnvA,re);    
    end if;
    
    insere_atualizacao(id,'HSSUSUA','DEXRGUSUA','Data de expedi��o do R.G.',to_char(expedicao,'DD/MM/YYYY'),to_char(expedicaoA,'DD/MM/YYYY'),re);
    insere_atualizacao(id,'HSSUSUA','CREFEUSUA','Complemento',compl,complA,re);
    insere_atualizacao(id,'HSSUSUA','CNUMEUSUA','N�mero',numero,numeroA,re);
    insere_atualizacao(id,'HSSUSUA','NFATUTLOGR','Tipo do logradouro',to_char(tlogr,'9999999999999999'),to_char(tlogrA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','NNUMEPLAN','Plano',to_char(plano,'9999999999999999'),to_char(planoA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','CMAILUSUA','Email',email,emailA,re);
    insere_atualizacao(id,'HSSTLUSU','CFONETLUSU','Telefone',telefone,telefoneA,re);    
    insere_atualizacao(id,'HSSTLUSU','CFONETLUSU2','Telefone comercial',tel_comercial,tel_comercialA,re);
    insere_atualizacao(id,'HSSTLUSU','CFONETLUSU3','Telefone celular',tel_celular,tel_celularA,re);
    insere_atualizacao(id,'HSSUSUA','NSALAFUNCI','Sal�rio',to_char(salario,'9999999999999999'),to_char(salarioA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','NNUMEMCANC','Motivo de cancelamento',to_char(motivo_canc,'9999999999999999'),null,re);
    insere_atualizacao(id,'HSSUSUA','DSITUUSUA','Data de cancelamento',to_char(dt_canc,'DD/MM/YYYY'),null,re);
    insere_atualizacao(id,'HSSUSUA','CESTCUSUA','Estado civil',estado_civil,estado_civilA,re);
    insere_atualizacao(id,'HSSUSUA','NNUMEPROF','Funcao/Profissao',to_char(profissao,'9999999999999999'),to_char(profissaoA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','NNUMEDEPAR','Setor',to_char(setor,'9999999999999999'),to_char(setorA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','NNUMECECUS','Centro custo',to_char(centro_custo,'9999999999999999'),to_char(centro_custoA,'9999999999999999'),re);
    insere_atualizacao(id,'HSSUSUA','DUESTUSUA','Data de uni�o est�vel/casamento',to_char(uniao,'DD/MM/YYYY'),to_char(uniaoA,'DD/MM/YYYY'),re);
    insere_atualizacao(id,'HSSUSUA','NPESOUSUA','Peso',peso,pesoA,re);
    insere_atualizacao(id,'HSSUSUA','NALTUUSUA','Altura',altura,alturaA,re);
    
    
    if (data_plano_prog > sysdate) and (nvl(plano,0) <> nvl(planoA,0)) then -- Altera��o do plano com programa��o futura (caso que s� ocorrer� na integra��o do LIFE)    
      insere_atualizacao(id,'HSSHPLAN','DDATAHPLAN','Data de inclus�o do novo plano',to_char(data_plano_prog,'DD/MM/YYYY'),to_char(sysdate,'DD/MM/YYYY'),re);
    end if;  
    
    insere_atualizacao(id,'HSSREPAS','CCIDAREPAS','Cidade de Atendimento (informativo)',cidadeatendimento,cidadeatendimentoA,re);
    
    retorno := id;
  else
    retorno := 1;
  end if;
end;
/


create or replace procedure insere_usuario_pospag30 (contrato      in     number,
                                                   nome          in     varchar2,
                                                   codigo        in     varchar2,
                                                   categoria     in     varchar2,
                                                   nascimento    in     date,
                                                   sexo          in     varchar2,
                                                   cpf           in     varchar2,
                                                   telefone      in     varchar2,
                                                   validade      in     date,
                                                   id_locacao    in     number,
                                                   via           in     number,
                                                   id_usuario    out  number,
                           id_acomodacao in   number,
                           email         in     varchar2) as

id_plano        number;
id_tabela       number;
codigo_contrato varchar2(16);
id_locacao2     number;

begin
  if id_locacao > 0 then
    id_locacao2 := id_locacao;
  else
    id_locacao2 := null;
  end if;

  select sequsua.nextval into id_usuario from dual;

  select ccodititu,nnumeplan,nnumetpla into codigo_contrato,id_plano,id_tabela
    from hsstitu where nnumetitu = contrato;

  insert into hssusua (nnumeusua,cnomeusua,dnascusua,csexousua,c_cpfusua,nnumetitu,ntituusua,
                       ddigiusua,ctipousua,dinclusua,csituusua,nnumeplan,cans_usua,
                       nnumetpla,ccallusua,ccodiusua,cgrauusua,dpvalusua,nnumesetor,nviauusua,ccuniusua,nnumeacom,cmailusua) 
               values (id_usuario,nome,nascimento,sexo,cpf,contrato,id_usuario,
                       sysdate,categoria,trunc(sysdate),'A',id_plano,'N',
                       id_tabela,'S',codigo,'X',validade,id_locacao2,via,codigo,id_acomodacao,email);  

  if telefone <> '' then
    insert into hsstlusu (nnumeusua,cfonetlusu,cobsetlusu)
                  values (id_usuario,telefone,'contato');
  end if;

end;
/

create or replace procedure insere_usuario_web30 (contrato     in     number,
                                                  titular      in     number,
                                                  nome         in     varchar2,
                                                  categoria    in     varchar2,
                                                  nascimento   in     date,
                                                  estadonasc   in     varchar2,
                                                  cidadenasc   in     varchar2,
                                                  sexo         in     varchar2,
                                                  matricula    in     varchar2,
                                                  mae          in     varchar2,
                                                  pai          in     varchar2,
                                                  cpf          in     varchar2,
                                                  rg           in     varchar2,
                                                  orgao        in     varchar2,
                                                  pis          in     varchar2,
                                                  grau         in     varchar2,
                                                  locacao      in     number,
                                                  admissao     in     date,
                                                  endereco     in     varchar2,
                                                  bairro       in     varchar2,
                                                  cidade       in     varchar2,
                                                  estado       in     varchar2,
                                                  cep          in     varchar2,
                                                  plano        in     number,
                                                  acomodacao   in     number,
                                                  telefone     in     varchar2,
                                                  expedicao    in     date,
                                                  numero       in     varchar2,
                                                  tlogr        in     number,
                                                  compl        in     varchar2,
                                                  tel_com      in     varchar2,
                                                  tel_cel      in     varchar2,
                                                  email        in     varchar2,
                                                  salario      in     varchar2,
                                                  dt_incl      in     date,
                                                  estado_civ   in     varchar2,
                                                  profissao    in     number,
                                                  setor        in     number,
                                                  local        in     varchar2,
                                                  vendedor     in     number,
                                                  operador     in     number,
                                                  proposta     in     number,
                                                  valor        in     number,
                                                  csus         in     varchar2,
                                                  ndnv         in     number,
                                                  observacao   in     varchar2,
                                                  uniao        in     date,
                                                  centro_custo in     number,
                                                  atcadpai     in     number,
                                                  peso         in     number,
                                                  altura       in     number,
                                                  ip           in     varchar2,
                                                  id_usuario   in out number,
                                                  retorno         out number,
                                                  id_operadora_repasse in number default null,
                                                  cidade_atendimento   in varchar2 default null) as

-- retornos
-- -1 - n�o � poss�vel cadastrar usuario com nome vazio
-- -2 - n�o � poss�vel cadastrar usuario sem data de nascimento
-- -3 - n�o � poss�vel cadastrar usuario sem sexo
-- -4 - n�o � poss�vel cadastrar usuario sem o nome da m�e ou pis

id_atcadpai     number;
id_titular      number;
id_plano        number;
id_tabela       number;
id_locacao      number;
id_contrato     number;
id_proposta     number;
id_vendedor     number;
inclusao        date;
id_motivo       number;
id_centro_custo number;
vl_peso         number;
vl_altura       number;
cgc             varchar2(18);
codigo_usuario  varchar2(16);
codigo_contrato varchar2(16);
contrato_de_pcmso varchar2(1);
carteira        varchar2(20);
cidade_atend    varchar2(60);
situ_usu        varchar2(1);
ans_plano       varchar2(1);
titu_ans        varchar2(1);
codigo_titular  varchar2(16);


cursor cr_motivo is
  select nnumemincl from hssinter;
  
cursor cr_plano is
  select cans_plan from hssplan
   where nnumeplan = plano;  
   
cursor cr_codigo_titular is
  select ccodiusua from hssusua   
   where nnumeusua = id_titular;
   
begin
  altera_application_info(operador);
  contrato_de_pcmso := 'N';
  id_atcadpai := atcadpai;

  if contrato > 0 then
    select nvl(caso_titu,'N') into contrato_de_pcmso from hsstitu
     where nnumetitu = contrato;
  end if;

  if nvl(id_usuario,0) = 0 then
    select sequsua.nextval into id_usuario from dual;
  end if;

  if categoria = 'T' or categoria = 'F' then
    id_titular := id_usuario;
  else
    id_titular := titular;
  end if;
  
  cgc := cnpj_operadora();

  open cr_motivo;
  fetch cr_motivo into id_motivo;
  if cr_motivo%notfound then
    id_motivo := null;
  end if;
  close cr_motivo;

  if cgc = '02918461000173' then
    inclusao := last_day(trunc(sysdate));
  elsif dt_incl is not null then
    inclusao := dt_incl;
  else
    inclusao := trunc(sysdate);
  end if;

  if locacao > 0 then
    id_locacao := locacao;
  else
    id_locacao := null;
  end if;

  if vendedor > 0 then
    id_vendedor := vendedor;
  else
    id_vendedor := null;
  end if;

  if contrato > 0 then
    id_contrato := contrato;
  else
    id_contrato := null;
  end if;

  if proposta > 0 then
    id_proposta := proposta;
  else
    id_proposta := null;
  end if;

  if centro_custo > 0 then
    id_centro_custo := centro_custo;
  else
    id_centro_custo := null;
  end if;

  if peso > 0 then
    vl_peso := peso;
  else
    vl_peso := null;
  end if;

  if altura > 0 then
    vl_altura := altura;
  else
    vl_altura := null;
  end if;

  if nome is null then
    retorno := -1;
  elsif nascimento is null then
    retorno := -2;
  elsif sexo is null then
    retorno := -3;
  elsif ((mae is null) and (pis is null) and (contrato_de_pcmso = 'N')) then
    retorno := -4;
  elsif ((cgc = '03016500000100') and (contrato = 3277018)) then

    select ccodititu,nnumeplan,nnumetpla into codigo_contrato,id_plano,id_tabela
      from hsstitu where nnumetitu = contrato;

    codigo_usuario := atribuicao_codigo_usuario (categoria,'',codigo_contrato,id_plano,id_usuario,null);

    insert into hssusua (nnumeusua,cnomeusua,dnascusua,cestnusua,ccidnusua,csexousua,cchapusua,
                         cnmaeusua,cnpaiusua,c_cpfusua,c__rgusua,corrgusua,cpis_usua,nnumesetor,cgrauusua,
                         nnumetitu,ntituusua,ddigiusua,ctipousua,dadmiusua,cendeusua,cbairusua,
                         ccidausua,cestausua,ccep_usua,dinclusua,csituusua,nnumeplan,cans_usua,
                         nnumetpla,ccallusua,ccodiusua,nnumeacom,dexrgusua,cmailusua,nsalafunci,
                         cestcusua,nnumeprof,nnumedepar,csus_usua,ndcnvusua,nnumemincl,nnumececus,npesousua,naltuusua, ndestpsaud)
                 values (id_usuario,nome,nascimento,estadonasc,cidadenasc,sexo,matricula,
                         mae,pai,cpf,rg,orgao,pis,id_locacao,grau,
                         id_contrato,id_titular,sysdate,categoria,admissao,endereco,bairro,
                         cidade,estado,somente_numeros(cep),inclusao,'A',id_plano,'N',
                         id_tabela,'N',codigo_usuario,acomodacao,expedicao,email,salario,
                         estado_civ,profissao,setor,csus,ndnv,id_motivo,id_centro_custo,vl_peso,vl_altura,id_operadora_repasse);

  elsif (cgc = '73717639000166') and (substr(cidade_atendimento,1,1) = 'S') then -- Fatima Saude (Inclus�o Life direta sem passar pela confirma��o em alguns casos)
    select ccodititu into codigo_contrato
      from hsstitu where nnumetitu = contrato;

    select max(nnumetpla) into id_tabela from hsstpla where nnumeplan = plano and nnumetitu = id_contrato;

    if nvl(id_tabela,0) > 0 then
    
      if categoria = 'T' then
        carteira := atribuicao_codigo_usuario (categoria,'',codigo_contrato,plano,id_usuario,null);
        codigo_usuario :=  remove_zeros_esquerda(substr(carteira,8,8));        
      else    
        open cr_codigo_titular;
        fetch cr_codigo_titular into codigo_titular;
        close cr_codigo_titular;      
        carteira := atribuicao_codigo_usuario (categoria,codigo_titular,codigo_contrato,plano,id_usuario,null);
        codigo_usuario :=  remove_zeros_esquerda(substr(carteira,8,8));
        carteira := substr(carteira,1,7) || acrescenta_zeros_string(codigo_titular,8) || substr(carteira,16,2);
      end if;  
      
      carteira := remove_zeros_esquerda(carteira);
      
      if (substr(cidade_atendimento,2,1) = 'S') then -- Contratos com restri��o de documenta��o
        situ_usu := 'D';
      else
        situ_usu := 'A';     
      end if;
      
      open cr_plano;
      fetch cr_plano into ans_plano;
      close cr_plano;   

      if categoria = 'T' then
        titu_ans := 'S';
      else
        titu_ans := 'N';
      end if;      

      insert into hssusua (nnumeusua,cnomeusua,dnascusua,cestnusua,ccidnusua,csexousua,cchapusua,
                           cnmaeusua,cnpaiusua,c_cpfusua,c__rgusua,corrgusua,cpis_usua,nnumesetor,cgrauusua,
                           nnumetitu,ntituusua,ddigiusua,ctipousua,dadmiusua,cendeusua,cbairusua,
                           ccidausua,cestausua,ccep_usua,dinclusua,csituusua,nnumeplan,cans_usua,
                           nnumetpla,ccallusua,ccodiusua,nnumeacom,dexrgusua,cmailusua,nsalafunci,
                           cestcusua,nnumeprof,nnumedepar,csus_usua,ndcnvusua,nnumemincl,nnumececus,npesousua,naltuusua,ccartusua,
                           cnumeusua,nfatutlogr,crefeusua,ctansusua)
                   values (id_usuario,nome,nascimento,estadonasc,cidadenasc,sexo,matricula,
                           mae,pai,cpf,rg,orgao,pis,id_locacao,grau,
                           id_contrato,id_titular,sysdate,categoria,admissao,endereco,bairro,
                           cidade,estado,somente_numeros(cep),inclusao,situ_usu,plano,ans_plano,
                           id_tabela,'S',codigo_usuario,acomodacao,expedicao,email,salario,
                           estado_civ,profissao,setor,csus,ndnv,id_motivo,id_centro_custo,vl_peso,vl_altura,carteira,
                           numero,tlogr,compl,titu_ans);
                           
      insert into hsshplan (nnumeusua,nnumeplan,nnumetpla,ddatahplan,ctipohplan)
                    values (id_usuario,plano,id_tabela,inclusao,'0');
                    
      if nvl(length(somente_numeros(telefone)),0) > 0 then 
        insert into hsstlusu (nnumeusua,cfonetlusu,ctipotlusu)
                      values (id_usuario,somente_numeros(telefone),'R');
      end if;  
      
      if nvl(length(somente_numeros(tel_com)),0) > 0 then 
        insert into hsstlusu (nnumeusua,cfonetlusu,ctipotlusu)
                      values (id_usuario,somente_numeros(telefone),'T');
      end if;  
      
      if nvl(length(somente_numeros(tel_cel)),0) > 0 then
        insert into hsstlusu (nnumeusua,cfonetlusu,ctipotlusu) 
                      values (id_usuario,somente_numeros(telefone),'C');
      end if;       
                    
      retorno := carteira; 
    else 
      retorno := -98;
    end if;      
  else
    select seqatcad.nextval into retorno from dual;

    if id_atcadpai is null then
      id_atcadpai := retorno;
    else
      id_atcadpai := atcadpai;
    end if;
    
    if (cgc = '73717639000166') then
      cidade_atend := '';
    else  
      cidade_atend := cidade_atendimento;
    end if;

    insert into hssatcad (nnumeatcad,coperatcad,nnumeusua,cnomeatcad,dnascatcad,cestnatcad,ccidnatcad,csexoatcad,cchapatcad,
                          cnmaeatcad,cnpaiatcad,c_cpfatcad,c__rgatcad,corrgatcad,cpis_atcad,nnumesetor,cgrauatcad,
                          nnumetitu,ntituusua,ddataatcad,ctipoatcad,dadmiatcad,cendeatcad,cbairatcad,
                          ccidaatcad,cestaatcad,ccep_atcad,dinclatcad,nnumeplan,nnumeacom,cfoneatcad,dexrgatcad,
                          cnumeatcad,nfatutlogr,ccompatcad,ctelratcad,ctelcatcad,cmailatcad,nsalaatcad,
                          cestcatcad,nnumeprof,nnumedepar,clocaatcad,nnumevend,noperusua,nnumepprop,nvaloatcad,
                          csus_atcad,ndcnvatcad,nnumemincl,cobseatcad,duestatcad,cip__atcad,nnumececus,nnpaiatcad,npesoatcad,naltuatcad, ndestpsaud,ccloaatcad)
                  values (retorno,'I',id_usuario,nome,nascimento,estadonasc,cidadenasc,sexo,matricula,
                          mae,pai,cpf,rg,orgao,pis,id_locacao,grau,id_contrato,id_titular,sysdate,categoria,admissao,
                          endereco,bairro,cidade,estado,cep,inclusao,plano,acomodacao,telefone,expedicao,
                          substr(numero,0,10),tlogr,compl,tel_com,tel_cel,email,salario,
                          estado_civ,profissao,setor,local,id_vendedor,operador,id_proposta,valor,
                          csus,ndnv,id_motivo,observacao,uniao,ip,centro_custo,id_atcadpai,vl_peso,vl_altura,id_operadora_repasse,cidade_atend);
  end if;
end;
/

create or replace procedure insere_usuario_pre30 (contrato     in     number,
                                                titular      in     number,
                                                nome         in     varchar2,
                                                categoria    in     varchar2,
                                                nascimento   in     date,
                                                estadonasc   in     varchar2,
                                                cidadenasc   in     varchar2,
                                                sexo         in     varchar2,
                                                matricula    in     varchar2,
                                                mae          in     varchar2,
                                                pai          in     varchar2,
                                                cpf          in     varchar2,
                                                rg           in     varchar2,
                                                orgao        in     varchar2,
                                                pis          in     varchar2,
                                                grau         in     varchar2,
                                                locacao      in     number,
                                                admissao     in     date,
                                                endereco     in     varchar2,
                                                bairro       in     varchar2,
                                                cidade       in     varchar2,
                                                estado       in     varchar2,
                                                cep          in     varchar2,
                                                plano        in     number,
                                                acomodacao   in     number,
                                                telefone     in     varchar2,
                                                expedicao    in     date,
                                                numero       in     varchar2,
                                                tlogr        in     number,
                                                compl        in     varchar2,
                                                tel_com      in     varchar2,
                                                tel_cel      in     varchar2,
                                                email        in     varchar2,
                                                salario      in     varchar2,
                                                dt_incl      in     date,
                                                estado_civ   in     varchar2,
                                                profissao    in     number,
                                                setor        in     number,
                                                local        in     varchar2,
                                                vendedor     in     number,
                                                operador     in     number,
                                                proposta     in     number,
                                                valor        in     number,
                                                csus         in     varchar2,
                                                ndnv         in     number,
                                                observacao   in     varchar2,
                                                uniao        in     date,
                                                centro_custo in     number,                                               
                                                ip           in     varchar2,
                                                id_usuario   in out number,
                                                retorno         out number,
                                                status       in     varchar2,
                                                mensagem     in     varchar2,
                                                carencia     in     varchar2) as
                                                
-- retornos                                                                     
-- -1 - n�o � poss�vel cadastrar usuario com nome vazio                                                         
-- -2 - n�o � poss�vel cadastrar usuario sem data de nascimento                                                              
-- -3 - n�o � poss�vel cadastrar usuario sem sexo                                              
-- -4 - n�o � poss�vel cadastrar usuario sem o nome da m�e ou pis 
                                               
id_atcadpai     number;  
id_titular      number;
id_plano        number;
id_tabela       number;
id_locacao      number;
id_contrato     number;
id_proposta     number;
id_vendedor     number;
inclusao        date;
id_motivo       number;
id_centro_custo number;
codigo_titular  varchar2(16);
cgc             varchar2(18);
codigo_usuario  varchar2(16);
codigo_contrato varchar2(16);
contrato_de_pcmso varchar2(1);

cursor cr_empresa is
  select ccgc_empr from finempr;

cursor cr_motivo is
  select nnumemincl from hssinter;  
  
begin 
  altera_application_info(operador);
  contrato_de_pcmso := 'N';
  
  if contrato > 0 then
    select nvl(caso_titu,'N') into contrato_de_pcmso from hsstitu
     where nnumetitu = contrato;
  end if;
  
  if nvl(id_usuario,0) = 0 then   
    select sequsua.nextval into id_usuario from dual;
  end if;
  
  if categoria = 'T' or categoria = 'F' then
    id_titular := id_usuario;
  else
    id_titular := titular;
    select ccodiusua into codigo_titular from hssusua where nnumeusua = id_titular;
  end if;

  open cr_empresa;
  fetch cr_empresa into cgc;
  close cr_empresa;
  
  open cr_motivo;
  fetch cr_motivo into id_motivo;
  if cr_motivo%notfound then
    id_motivo := null;
  end if;  
  close cr_motivo;  

  if cgc = '02.918.461/0001-73' then
    inclusao := last_day(trunc(sysdate));
  elsif dt_incl is not null then 
    inclusao := dt_incl;
  else
    inclusao := trunc(sysdate);
  end if;
  
  if locacao > 0 then
    id_locacao := locacao;
  else
    id_locacao := null;
  end if;
  
  if vendedor > 0 then
    id_vendedor := vendedor;
  else
    id_vendedor := null;
  end if;  
  
  if contrato > 0 then
    id_contrato := contrato;
  else
    id_contrato := null;
  end if;  
  
  if proposta > 0 then
    id_proposta := proposta;
  else
    id_proposta := null;
  end if;   
  
  if centro_custo > 0 then
    id_centro_custo := centro_custo;
  else
    id_centro_custo := null;
  end if;  
  
  if nome is null then                                                      
    retorno := -1;     
  elsif nascimento is null then                                                   
    retorno := -2;                                                       
  elsif sexo is null then                                                   
    retorno := -3;                                                           
  elsif ((mae is null) and (pis is null) and (contrato_de_pcmso = 'N')) then                                                  
    retorno := -4;
  else

    select ccodititu,nnumeplan,nnumetpla into codigo_contrato,id_plano,id_tabela
      from hsstitu where nnumetitu = contrato;

  if categoria = 'T' or categoria = 'F' then
    codigo_usuario := atribuicao_codigo_usuario (categoria,'',codigo_contrato,id_plano,id_usuario,null,grau,sexo);
  else
    codigo_usuario := atribuicao_codigo_usuario (categoria,codigo_titular,codigo_contrato,id_plano,id_usuario,null,grau,sexo);        
  end if;
    insert into hssusua (nnumeusua,cnomeusua,dnascusua,cestnusua,ccidnusua,csexousua,cchapusua,cprecusua,
                         cnmaeusua,cnpaiusua,c_cpfusua,c__rgusua,corrgusua,cpis_usua,nnumesetor,cgrauusua,
                         nnumetitu,ntituusua,ddigiusua,ctipousua,dadmiusua,
                         dinclusua,csituusua,nnumeplan,cans_usua,
                         nnumetpla,ccallusua,ccodiusua,nnumeacom,dexrgusua,cmailusua,nsalafunci,
                         cestcusua,nnumeprof,nnumedepar,csus_usua,ndcnvusua,nnumemincl,nnumececus,nnumestat,dstatusua,CCAREUSUA,nnumevend) 
                 values (id_usuario,nome,nascimento,estadonasc,cidadenasc,sexo,matricula,'1',
                         mae,pai,cpf,rg,orgao,pis,id_locacao,grau,
                         id_contrato,id_titular,sysdate,categoria,admissao,
                         inclusao,'D',id_plano,'N',
                         id_tabela,'N',codigo_usuario,acomodacao,expedicao,email,salario,
                         estado_civ,profissao,setor,csus,ndnv,id_motivo,id_centro_custo,status,trunc(sysdate), carencia,id_vendedor); 
  
  if(telefone <> ' ') then
      insert into hsstlusu(nnumeusua,cfonetlusu,ctipotlusu)
                values(id_usuario,telefone,'R');
  end if;
  
  if(tel_com <> ' ') then
      insert into hsstlusu(nnumeusua,cfonetlusu,ctipotlusu)
                values(id_usuario,tel_com,'T');
  end if;
  
  if(tel_cel <> ' ') then
      insert into hsstlusu(nnumeusua,cfonetlusu,ctipotlusu)
                values(id_usuario,tel_cel,'C');
  end if;
  
    insert into hssmstat(nnumestat,nnumetitu,nnumeusua,ddatamstat,cmotimstat,ddigimstat)
          values(status,contrato,id_usuario,trunc(sysdate),mensagem,sysdate);             
  end if;          
end;
/

create or replace procedure cancela_operacao_web30 (id       in number, 
                                                  operador in number default null) as
begin 
  altera_application_info(operador);
  delete from hssatcad where nnumeatcad = id;
end;
/

create sequence seqatcad
/

create or replace trigger tri_hssatcad30
before insert on hssatcad for each row
begin
  if :new.nnumeatcad is null then
    select seqatcad.nextval into :new.nnumeatcad from dual;
  end if;
end;
/

create or replace function valida_pacote30(pacote                in varchar2,
                                         prestador             in number,
                                         local                 in number,
                                         critica_procedimentos in varchar2,
                                         usuario               in number,
                                         especialidade         in number,
                                         urgencia              in varchar2,
                                         natureza              in varchar2,
                                         pagina                in number,
                                         guia                  in number,
                                         confirmacao           in varchar2,
                                         solicitante           in number,
                                         id_pacote1            in number default 0) return number as
                                         
regra_autorizacao varchar2(1);
resultado         number;
id_pacote         number;

begin
  if id_pacote1 > 0 then
    id_pacote := id_pacote1;
  elsif ((regra_autorizacao = 'L') or (cnpj_operadora() = '04083773000130') or (cnpj_operadora() = '00299149000113')) then
    id_pacote := RetornaIdPacote30(pacote,local,usuario);
  else
    id_pacote := RetornaIdPacote30(pacote,prestador,usuario);
  end if;

  if id_pacote > 0 then

    if critica_procedimentos = 'S' then
      for reg in (select ccodipmed,nquanppaco,ccodidente,ccodiface
                    from hssppaco,hssdente,hssface
                   where nnumepacot = id_pacote
                     and dvigeppaco = (select max(ppaco.dvigeppaco) from hssppaco ppaco
                                        where ppaco.nnumepacot = id_pacote
                                        and ppaco.dvigeppaco <= sysdate)
                     and hssppaco.nnumedente = hssdente.nnumedente(+)
                     and hssppaco.nnumeface = hssface.nnumeface(+) ) loop
                                      
        resultado := valida_Procedimento_web30 (usuario,reg.ccodipmed,reg.nquanppaco,prestador,reg.ccodidente,reg.ccodiface,
                                              especialidade,local,urgencia,natureza,pagina,guia,confirmacao,solicitante,'S');
                                              
        if resultado > 0 then
        -- Quando der erro na valida_procedimento_web, eu inverto o sinal do erro para poder retornar ao php como negativo, 
        -- porque os ids dos pacotes sao positivos
          return -resultado;
        end if;        
      end loop;                                      
    end if;
    
    return id_pacote;
  else
    return -23;
  end if;
end;
/

create or replace function TipoProcedimentoGuia30 (Guia in number) return varchar2 as

natureza varchar2(1);
urgencia varchar2(1);
tipo     varchar2(15);
ordem    number;

cursor cr_guia is
select ctipoguia,curgeguia,decode(ctipopmed,'C','Consulta','Procedimento'),1 ordem
  from hssguia,hsspgui,hsspmed
 where hssguia.nnumeguia = guia
   and hssguia.nnumeguia = hsspgui.nnumeguia(+)
   and hsspgui.ccodipmed = hsspmed.ccodipmed(+)
union all
select ctipoguia,curgeguia,'Pacote',2
  from hssguia,hsspcgui
 where hssguia.nnumeguia = guia
   and hssguia.nnumeguia = hsspcgui.nnumeguia
order by 3,2,1;

begin
  open cr_guia;
  fetch cr_guia into natureza,urgencia,tipo,ordem;
  close cr_guia;

  if natureza in ('I','C','O','R','P','S') then
    return 'Internacao';
  elsif natureza = 'D' then
    return 'Or�amento Odontol�gico';
  elsif tipo = 'Consulta' and urgencia = 'U' then
    return 'SP/SADT';
  else
    return tipo;
  end if;
end;
/

create or replace procedure insere_prestador_web30 (contratado      in     number,
                                                  operador        in     number,
                                                  conselho        in     varchar2,
                                                  numero_conselho in     number,
                                                  uf_conselho     in     varchar2,
                                                  nome            in     varchar2,
                                                  nascimento      in     date,
                                                  sexo            in     varchar2,
                                                  cpf             in     varchar2,
                                                  especialidade   in     number,
                                                  cep             in     varchar2,
                                                  tipo_logradouro in     number,
                                                  logradouro      in     varchar2,
                                                  numero          in     varchar2,
                                                  complemento     in     varchar2,
                                                  bairro          in     varchar2,
                                                  estado          in     varchar2,
                                                  cidade          in     varchar2,
                                                  status          in     varchar2,
                                                  id                 out number,
                                                  telefone        in     varchar2,
                                                  enviaptu        in     varchar2,
                                                  residenciamec   in     varchar2,                                                  
                                                  divulgado       in     varchar2) as                                                  
                                                  
id_prestador     number;
status_corpo     varchar2(1);
cgc              number;
codigo_prestador number;
enderecoSeparado varchar2(1);
endereco         varchar2(100);
                                                  
begin
  -- insere com o ccredpres Z
  cgc := cnpj_operadora();
  select cendsconf into enderecoSeparado from finconf;
  
  if status = 'C' then
    status_corpo := 'A';
  else
    status_corpo := '1';  
  end if;
    
  select seqgeral.nextval into id_prestador from dual;
  
  if (cgc = 01045690000168) or (cgc = 76882612000117) then --Vitallis/Clinipam feito isto porque o codigo do prestador para eles � de no maximo 8 digitos.
    select seq_prestador.nextval into codigo_prestador from dual;
  else  
    codigo_prestador := to_char(id_prestador);
  end if;
  
  if enderecoSeparado = 'S' then
    endereco := logradouro;
  else
    endereco := logradouro || ', ' || numero;
  end if;  
  
  insert into finpres (nnumepres,ccodipres,cnomepres,cconspres,ncrm_pres,cufcrpres,dnascpres,csexopres,ccpf_pres,
                       ccep_pres,nnumetlogr,cendepres,crefepres,cbairpres,cestapres,ccidapres,cnendpres,
                       ccredpres,dcriapres,ninclusua,cpesspres,csolipres,cgerppres,catenpres,cgruppres,cinclpres,
                       cir__pres,cfonepres,cptu_pres,cresipres,cans_pres)
               values (id_prestador,to_char(codigo_prestador),nome,conselho,numero_conselho,uf_conselho,nascimento,sexo,cpf,
                       cep,tipo_logradouro,endereco,complemento,bairro,estado,cidade,numero,
                       status,sysdate,operador,'F','S','S','1','M','2',
                       'N',telefone,enviaptu,residenciamec,divulgado);                     
                       
  insert into hssespre (nnumepres,nnumeespec,cpadrespre,cpublespre,clwebespre,clibeespre,cnatuespre,curgeespre,
                        cdocuespre)
                values (id_prestador,especialidade,'S','S','N','N','T','T','N');
                       
  if (contratado > 0) then -- ativa o corpo clinico ap�s confirmacao da operadora
    insert into hsscclin (nhosppres,nnumepres,nnumeespec,cstatcclin,cpublcclin,cformcclin)
                  values (contratado,id_prestador,especialidade,status_corpo,'S','P');
  end if;
  
  if cgc = 76882612000117 then --Clinipam faz uma inser��o padr�o de procedimentos autorizados. 
    --padr�o para eletivo 
  insert into hssautpr (nnumepres,ccodipmed,dvigeautpr,cnatuautpr,CURGEAUTPR,cpublautpr,cguiaautpr) 
                values (id_prestador,'00010014',trunc(sysdate),'T','E','S','S'); 
  --padr�o para urg�ncia 
  insert into hssautpr (nnumepres,ccodipmed,dvigeautpr,nperiautpr,cespeautpr,cnatuautpr,curgeautpr,cpublautpr,cguiaautpr) 
                values (id_prestador,'00010014',trunc(sysdate),0,'5','T','U','S','S'); 
  --padr�o para retorno 
  insert into hssautpr (nnumepres,ccodipmed,dvigeautpr,cespeautpr,cnatuautpr,curgeautpr,cpublautpr,cguiaautpr) 
                values (id_prestador,'00010015',trunc(sysdate),'1','T','B','N','S'); 
  end if;
  
  id := id_prestador;
end;
/

create or replace procedure apresenta_guia30 (guia          in     number,
                                            prestador     in     number,
                                            data          in     date,
                                            retorno          out number,
                                            material      in     number,
                                            medicamento   in     number,
                                            operador      in     number,
                                            inicio        in     varchar2,
                                            fim           in     varchar2,
                                            ncid1         in     varchar2 default null,
                                            ncid2         in     varchar2 default null,
                                            ncid3         in     varchar2 default null,
                                            ncid4         in     varchar2 default null,
                                            tipo_saida    in     varchar2 default null,
                                            tipo_atend    in     varchar2 default null,
                                            tipo_consulta in     varchar2 default null) as

  -- retornos                                                                     
  -- 1 - guia ja faturada                                                         
  -- 2 - guia negada                                                              
  -- 3 - guia aguardando confirmacao                                              
  -- 4 - guia aguardando autorizacao                                              
  -- 5 - guia sob auditoria
  -- 6 - nao usar
  -- 7 - guia inexistente                                                         
  -- 8 - guia faturada com sucesso  
  -- 9 - guia ja apresentada
  -- 10 - guia negada na unimed de origem
  
cursor cr_tipo_guia is
  select nvl(caso_guia,'G') caso_guia
    from hssguia
   where nnumeguia = guia;
   
cursor cr_guia is
  select cstatguia,nnumecid_ from hssguia
   where nnumeguia = guia
     and (nnumepres = prestador or nlocapres = prestador);

cursor cr_apresentado is
  select hssgint.nnumeguia from hssgint
   where hssgint.nnumeguia = guia;

cursor cr_faturada is
  select hssguia.nnumeguia from hssguia,hssgcon
   where hssguia.nnumeguia = guia
     and (nnumepres = prestador or nlocapres = prestador)
     and hssguia.nnumeguia = hssgcon.nnumeguia;

cursor cr_cid_guia (cid in number) is 
  select nnumeguia
    from hsscidg
   where nnumecid_ = cid
     and nnumeguia = guia;

numero_guia number;
status      varchar2(1);
cidguia     number;
eh_aso      varchar2(1);

begin
  --Se caso_guia = 'G' � guia
  --Se caso_guia = 'A' ent�o � um aso e n�o pode ser faturado
  open cr_tipo_guia;
  fetch cr_tipo_guia into eh_aso;
  close cr_tipo_guia;
  
  open cr_guia;
  fetch cr_guia into status,cidguia;
  if ((cr_guia%found) and (eh_aso = 'G')) then
    open cr_faturada;
    fetch cr_faturada into numero_guia;
    if cr_faturada%notfound then
      open cr_apresentado;
      fetch cr_apresentado into numero_guia;
      if cr_apresentado%notfound then
        if status = 'A' then                                                      
          retorno := 5;                                                           
        elsif status = 'C' then                                                   
          retorno := 3;                                                           
        elsif status = 'P' then                                                   
          retorno := 4;                                                           
        elsif status = 'N' then                                                   
          retorno := 2;
        elsif status = 'D' then                                                   
          retorno := 10;          
        else
          insert into hssgint (nnumeguia,nnumepres,ddigigint,datengint,nmedigint,nmategint,noperusua,
                               chinigint,chfimgint,ncid1gint,ncid2gint,ncid3gint,ncid4gint,
                               ctpcogint,ctpatgint,ctpsagint)
                       values (guia,prestador,sysdate,data,medicamento,material,operador,
                               inicio,fim,ncid1,ncid2,ncid3,ncid4,
                               tipo_consulta,tipo_atend,tipo_saida); 

          if cidguia is null and ncid1 is not null then
            update hssguia set nnumecid_ = ncid1
             where nnumeguia = guia;
          elsif cidguia <> ncid1 and ncid1 is not null then
            open cr_cid_guia(ncid1);
            fetch cr_cid_guia into cidguia;
            if cr_cid_guia%notfound then
               insert into hsscidg (nnumeguia,nnumecid_)
                           values (guia,ncid1);
            end if;
            close cr_cid_guia;
          end if;     

          if ncid2 is not null then
            open cr_cid_guia(ncid2);
            fetch cr_cid_guia into cidguia;
            if cr_cid_guia%notfound then
               insert into hsscidg (nnumeguia,nnumecid_)
                           values (guia,ncid2);
            end if;
            close cr_cid_guia;
          end if;
      
          if ncid3 is not null then
            open cr_cid_guia(ncid3);
            fetch cr_cid_guia into cidguia;
            if cr_cid_guia%notfound then
               insert into hsscidg (nnumeguia,nnumecid_)
                           values (guia,ncid3);
            end if;
            close cr_cid_guia;
          end if;

          if ncid4 is not null then
            open cr_cid_guia(ncid4);
            fetch cr_cid_guia into cidguia;
            if cr_cid_guia%notfound then
               insert into hsscidg (nnumeguia,nnumecid_)
                           values (guia,ncid4);
            end if;
            close cr_cid_guia;
          end if;

          retorno := 8;         
        
        end if;
        
      else
        retorno := 9;
      end if;
      close cr_apresentado;
    else
      retorno := 1;
    end if;
    close cr_faturada;
  else
    retorno := 7;
  end if;  
  close cr_guia;
end;
/

create or replace procedure faturamento_web30 (guia          in     number,
                                               prestador     in     number,
                                               data          in     date,
                                               retorno          out number,
                                               lote             out number,
                                               conta            out number,
                                               material      in     number   default 0,
                                               medicamento   in     number   default 0,
                                               operador      in     number   default null,
                                               h_inicio      in     varchar2 default null,
                                               ncid1         in     number   default null,
                                               ncid2         in     number   default null,
                                               ncid3         in     number   default null,
                                               ncid4         in     number   default null,
                                               tipo_saida    in     varchar2 default null,
                                               tipo_atend    in     varchar2 default null,
                                               tipo_consulta in     varchar2 default null,
                                               tipo_fatu     in     varchar2 default null,
                                               dataSaida     in     date     default null,
                                               h_fim         in     varchar2 default null,
                                               motivo_saida  in     varchar2 default null,
                                               localFatu     in     varchar2 default null) as

  -- retornos
  -- 1 - guia ja faturada
  -- 2 - guia negada
  -- 3 - guia aguardando confirmacao
  -- 4 - guia aguardando autorizacao
  -- 5 - guia sob auditoria
  -- 6 - sem configuracao de datas
  -- 7 - guia inexistente
  -- 8 - guia faturada com sucesso
  -- 9 - nao usar
  -- 10 - guia negada na unimed de origem
  -- 11 - periodo n�o permitido para faturamento
  -- 12 - guia cancelada
  -- 13 - guia cancelada pela unimed origem 

  competencia              varchar2(7);
  data_pagamento           date;
  data_inicio              date;
  data_final               date;
  protocolo                number;
  conta_bancaria           number;
  evento                   number;
  descricao                varchar2(60);
  nome_prestador           varchar2(60);
  forma_pagamento          varchar2(1);
  glosa_prestador          varchar2(1);
  negociacao               varchar2(1);
  status                   varchar2(1);
  numero_guia              number;
  emissao                  date;
  hora                     varchar2(5);
  motivo                   number;
  estabelecimento          number;
  tipo                     varchar2(1);
  comp_fechada             date;
  cgc                      varchar2(18);
  classificacao_lote       number;
  classificacao_prestador  number;
  tipo_guia                varchar2(1);
  mes_lote                 varchar2(1);
  data_recebimento         date;
  competencia_atend        varchar2(7);
  eve_hono                 number;
  Conta_Complementar       number;
  Complementares_Principal number;
  data_alta_complementar   date;
  hora_alta_complementar   varchar2(8);
  eve_custo                number; 
  eve_matmed               number;
  eve_diaria               number;
  congenere                number;
  separaCongenere          number;
  idCongenere              number;
  eh_aso                   varchar2(1);
  projeto90                varchar2(1);
  tipo_pag                 varchar2(1);
  existe_congenere_calend  boolean;
  codigo_prestador         varchar2(10);
  
  cursor cr_tipo_guia is
    select nvl(caso_guia,'G') caso_guia
    from hssguia
   where nnumeguia = guia;

  cursor cr_guia is
    select cstatguia,nvl(data,trunc(demisguia)),substr(TipoProcedimentoGuia30(nnumeguia),1,1) tipo,hsstitu.nnumeconge,to_char(demisguia,'HH24:MI') hora
      from hssguia,hssusua,hsstitu
     where nnumeguia = guia
       and hssguia.nnumeusua = hssusua.nnumeusua(+)
       and hssusua.nnumetitu = hsstitu.nnumetitu(+);
  
  cursor cr_guia_complementar is
    select hsscont.nnumecont Conta_complementar, daltacont data_alta_complementar, chaltcont hora_alta_complementar 
      from hssgcon,hsscont
     where nnumeguia in (select nvl(ncompguia,ncheqguia) 
                           from hssguia
                          where nnumeguia = guia)
       and hssgcon.nnumecont = hsscont.nnumecont; 
  
  cursor cr_complementares_principal is
    select nnumecont Complementares_Principal
      from hssgcon
     where nnumeguia in (select comp.nnumeguia
                           from hssguia orig,hssguia comp,segusua ocomp
                          where orig.nnumeguia = guia
                            and orig.nnumeguia = comp.ncompguia
                            and comp.noperusua = ocomp.nnumeusua(+)
                            and nvl(orig.ncompguia,0) <> guia
                          union all
                         select comp.nnumeguia
                           from hssguia orig,hssguia comp,segusua ocomp
                          where orig.nnumeguia = guia
                            and orig.nnumeguia = comp.npediguia
                            and comp.noperusua = ocomp.nnumeusua(+)
                            and nvl(orig.npediguia,0) <> guia );

  cursor cr_guia_faturada is
    select nnumeguia
      from hssgcon
     where nnumeguia = guia
       and tipo_fatu = '1';

  cursor cr_lote (pagamento in date) is
    select nnumelote,nprotlote
      from hsslote
     where ctipolote = 'W'
       and nnumepres = prestador
       and ((ctipglote = tipo_guia) or (nvl(tipo_guia,'A') = 'A' and ctipglote is null))
       and ( (nnumeconge = idCongenere) or ((nnumeconge is null) and (idCongenere is null)) )
       and diniclote = data_inicio
       and dfinalote = data_final
       and csitulote = 'A'
       and nvl(canallote,'1') in ('1','4')
       and dprevlote = pagamento;

  cursor cr_prestador is
    select nvl(ctpagpres,'C'),nvl(cglospres,'1'),cnomepres,nvl(cnegopres,'P'),ncm__even,hssconf.nnumecont,
           hssinter.nnumeestab, nnumeclapr,nvl(cpp90pres,'N') cpp90pres,ccodipres
      from finpres,hssconf,hssinter
     where nnumepres = prestador;

  cursor cr_motivo_alta (tipo in varchar2) is
    select nnumemotal
      from hssmotal
     where ccodimotal = tipo
        or ccod2motal = tipo;

  cursor cr_competencia_fechada (comp in varchar2) is
    select trunc(dlotalpweb) dlotflpweb from hsslpweb
     where ccomplpweb = comp;
   
  cursor cr_configuracao is
    select cwecoconf,nhonoeven,ncopeeven,nmmedeven,ntaxaeven
    from hssconf;    

  cursor cr_competencia_atendimento(comp in varchar2) is
    select ccatecompe from hsscompe
     where ccompcompe = comp;
     
begin
  --Se = 'G' e Ctipoguia in '1','2','3','4','5','7','8' ent�o � uma guia gerada para um aso, ent�o deve ser faturado
  --Se = 'A' e Ctipoguia in '1','2','3','4','5','7','8' ent�o � um ASO e n�o pode ser faturado
  open cr_tipo_guia;
  fetch cr_tipo_guia into eh_aso;
  close cr_tipo_guia;
  
  select ccgc_empr into cgc from finempr;

  open cr_guia;
  fetch cr_guia into status,emissao,tipo_guia,congenere,hora;
  if ( ((cr_guia%found) and (eh_aso = 'G')) or (cgc = '37.652.765/0001-16') or (cgc = '37.436.920/0001-67')) then 
    open cr_guia_faturada;
    fetch cr_guia_faturada into numero_guia;
    if cr_guia_faturada%notfound then

      if status = 'A' then
        retorno := 5;
      elsif status = 'C' then
        retorno := 3;
      elsif status = 'P' then
        retorno := 4;
      elsif status = 'N' then
        retorno := 2;
      elsif status = 'D' then
        retorno := 10;
      elsif status = 'X' then
        retorno := 12; 
      elsif status = 'E' then
        retorno := 13;      
      else
        existe_congenere_calend := retorna_congenere_calendario(prestador,sysdate,sysdate,'W',tipo_guia,congenere);
 
        calcula_periodo_calendario(prestador,data_pagamento,data_inicio,data_final,sysdate,sysdate,'W',tipo_guia,congenere);
          
        -- usado para ver se vai separar ou n�o o lote pela congenere baseado no calendario.
        if existe_congenere_calend then
          idCongenere := congenere;
        else 
          idCongenere := null;
        end if;
    
        open cr_configuracao;
        fetch cr_configuracao into mes_lote,eve_hono,eve_custo,eve_matmed,eve_diaria;
        close cr_configuracao;
        
        if mes_lote = 'S' then
          competencia := to_char(data_final,'MM/YYYY');
          data_recebimento := data_final;
        else      
          competencia := to_char(sysdate,'MM/YYYY');
          data_recebimento := sysdate;
        end if;   

    
        open cr_competencia_atendimento(competencia);
        fetch cr_competencia_atendimento into competencia_atend;
        close cr_competencia_atendimento; 

        select ccgc_empr into cgc from finempr;

        -- Na Agemed e Nossa Saude se a compet�ncia da web estiver fechada, eu pego a pr�xima.  TEM QUE ESTAR BLOQUEADO LOTES ABERTOS
        if ((cgc = '02.933.220/0001-01') or (cgc = '02.862.447/0001-03'))then
          open cr_competencia_fechada(competencia);
          fetch cr_competencia_fechada into comp_fechada;
          close cr_competencia_fechada;

          if comp_fechada is null then
            select lpad(to_char(sysdate,'MM')+01||'/'||to_char(sysdate,'YYYY'),7,0) into competencia from dual ;
          end if;
        end if;

        if (cgc = '02.862.447/0001-03') then --Nossa Saude
          competencia_atend := to_char(add_months(data_pagamento,-2),'MM/YYYY');
        end if;
        
        if (data_pagamento is not null) then
          open cr_lote (data_pagamento);
          fetch cr_lote into lote,protocolo;
          if cr_lote%notfound then
            open cr_prestador;
            fetch cr_prestador into forma_pagamento,glosa_prestador,nome_prestador,negociacao,evento,conta_bancaria,estabelecimento,classificacao_prestador,projeto90,codigo_prestador;
            close cr_prestador;
                
            if (cgc = '02.933.220/0001-01') and (projeto90 = 'S') then 
              calcula_data_previsao(prestador,data_pagamento,tipo_pag,sysdate,competencia,'C');
              competencia       := substr(add_months(data_pagamento,-1),4,7);
              competencia_atend := substr(add_months(data_pagamento,-2),4,7);
              data_pagamento    := add_months(data_pagamento,-1);
              data_recebimento  := First_day(add_months(data_pagamento,-1));
              
            elsif (cnpj_operadora = '53678264000165') then -- Unimed Leste Paulista (Medida paleativa)
              competencia       := to_char(add_months(data_pagamento,-1),'MM/YYYY');
              competencia_atend := to_char(add_months(data_pagamento,-2),'MM/YYYY');
              
             /* if competencia_atend = to_char(data_recebimento,'MM/YYYY') then
                data_recebimento := First_day(add_months(data_pagamento,-1));
                data_final       := First_day(add_months(data_pagamento,-1));
              end if;*/
            end if;           

            select seqgeral.nextval into lote from dual;
            select seq_identificacao_lote.nextval into protocolo from dual;

            if tipo_guia = 'C' then
              descricao := 'WEB - CONSULTAS - ' || to_char(sysdate,'DD/MM/YYYY') || ' ' || substr(nome_prestador,0,31);
            elsif tipo_guia = 'S' then
              descricao := 'WEB - SP/SADT - ' || to_char(sysdate,'DD/MM/YYYY') || ' ' || substr(nome_prestador,0,33);
            elsif tipo_guia = 'I' then
              descricao := 'WEB - INTERNACOES - ' || to_char(sysdate,'DD/MM/YYYY') || ' ' || substr(nome_prestador,0,29); 
            elsif cgc = '00.361.325/0001-08' then -- Humana
              descricao := codigo_prestador || ' - ' || substr(nome_prestador,0,34) || ' - ' || competencia || ' - WEB';
            else
              descricao := 'WEB - ' || to_char(sysdate,'DD/MM/YYYY') || ' ' || substr(nome_prestador,0,43);
            end if;

            classificacao_lote := null;
            -- Vitallis
            if (cnpj_operadora = '01045690000168') then      -- Na Vitallis o lote ser� classificado, caso o prestador tenha uma classifica��o correspondente
              if (classificacao_prestador = 2124904533) then -- classificacao = Consultas pagamentos
                classificacao_lote := 2124067345;            -- classificacao = PJ PAGAMENTO DE CONSULTAS
              end if;
            end if;
            
            -- Unimed Salto
            if (cnpj_operadora = '58837188000107') then
              classificacao_lote := 4046876;
            end if;            

            insert into hsslote (nnumelote,nnumepres,nprotlote,drecelote,dcrialote,noperusua,ncriausua,ccomplote,ccatelote,
                                 csitulote,nvalolote,cgpaglote,ctipclote,chosplote,ctpaglote,cglosalote,nnumecont,nnumeeven,
                                 cdesclote,ctipolote,cnegolote,dprevlote,cweb_lote,canallote,nnumeestab,
                                 diniclote,dfinalote, nnumeclalt,nhonoeven,ncopeeven,nmmedeven,ntaxaeven,nnumeconge,
                                 ctipglote)
                         values (lote,prestador,protocolo,data_recebimento,sysdate,operador,operador,competencia,competencia_atend,
                                 'A',0,'S','T','N',forma_pagamento,glosa_prestador,conta_bancaria,evento,
                                 descricao,'W',negociacao,data_pagamento,'S','4',estabelecimento,
                                 data_inicio,data_final, classificacao_lote,eve_hono,eve_custo,eve_matmed,eve_diaria,idCongenere,
                                 decode(tipo_guia,'A',null,tipo_guia));
          end if;
          close cr_lote;

          if nvl(motivo_saida,' ') <> ' ' then
            open cr_motivo_alta(motivo_saida);
            fetch cr_motivo_alta into motivo;
              if cr_motivo_alta%notfound then
                motivo := null;
            end if;
            close cr_motivo_alta;
          end if;

          if (lote > 0) then
            proximo_numero_conta (conta);
            insert into hsscont (nnumecont,nnumepres,nnumeusua,datencont,curgecont,cambucont,
                                 cclincont,ctipocont,nnumelote,ccortcont,
                                 nmaqpcont,nmdqpcont,nlocapres,nsolipres,
                                 nnumeespec,chintcont,nnumeacom,noperusua,ccobrcont,cdespcont,nnumeprof,
                                 ctpcocont,ctpatcont,nnumemotal,cmodacont,cffatcont,canalcont,
                                 cregicont,daltacont,chaltcont,ctpsacont,catrncont)
                         (select conta,decode(localFatu,'9',NVL(prestador,hssguia.nnumepres),hssguia.nnumepres),nutilusua,emissao,
                                 nvl(curgeguia,'E'),decode(ctipoguia,'I','I','C','I','O','I','R','I','P','I','6','6','1','1','2','2','3','3','A'),
                                 'M','N',lote,decode(hssguia.nnumenusua,null,ccortguia,decode(catrnguia,'S',ccortguia,cnomenusua)),
                                 nvl(material,0),nvl(medicamento,0),decode(localFatu,'9',NVL(prestador,hssguia.nlocapres),hssguia.nlocapres),
                                 nsolipres,nnumeespec,nvl(h_inicio,hora),nnumeacom,operador,'X',nvl(cdespguia,'1'),nnumeprof,
                                 tipo_consulta,tipo_atend,motivo,ccalcguia,'1','1',
                                 cregiguia,dataSaida,decode(NVL(dataSaida,emissao),null,null,h_fim),tipo_saida,catrnguia
                            from hssguia,hssnusua
                           where nnumeguia = guia
                             and hssguia.nnumenusua = hssnusua.nnumenusua(+));

            insert into hssgcon (nnumeguia,nnumecont,dfatugcon,ctipogcon) values (guia,conta,sysdate,localFatu);

            open cr_guia_complementar;
            fetch cr_guia_complementar into Conta_Complementar, data_alta_complementar, hora_alta_complementar;
              if cr_guia_complementar%found then
                update hsscont set ncompcont = Conta_Complementar, 
                                   daltacont = data_alta_complementar, 
                                   chaltcont = hora_alta_complementar
                 where nnumecont = conta;
              end if;
            close cr_guia_complementar; 	
  
            open cr_complementares_principal;
            fetch cr_complementares_principal into Complementares_Principal;
              while cr_complementares_principal%found loop
                update hsscont set ncompcont = conta, 
                                   daltacont = dataSaida, 
                                   chaltcont = decode(NVL(dataSaida,emissao),null,null,h_fim)
                 where nnumecont = Complementares_Principal;
                fetch cr_complementares_principal into Complementares_Principal;
              end loop;
            close cr_complementares_principal;
  
            insere_cid_conta(conta,ncid1);
            insere_cid_conta(conta,ncid2);
            insere_cid_conta(conta,ncid3);
            insere_cid_conta(conta,ncid4);

            retorno := 8;
          else
            retorno := 6;
          end if;
 
        elsif data_inicio is not null then
          retorno := 11;
        else
          retorno := 6;
        end if;
 
      end if;
 
    else
      retorno := 1;
    end if;
 
    close cr_guia_faturada;
 
  else
    retorno := 7;
  end if;
 
  close cr_guia;
end;
/

create or replace procedure fatura_simplificado_web30 (conta         in number,
                                                       guia          in number) as

numero_procedimento     number;
desconto_contraste      number;
valorDiferentePrincipal number;
valorDiferenteCO        number;
valorDiferenteFilme     number;
ValorDiferentePrestador varchar2(1);
dataAtendimento         date;
prestador               number;

cursor cr_guia is
  select demisguia,nnumepres
    from hssguia
   where nnumeguia = guia;

cursor cr_DescontoContraste(prestador in number,
                            data      in date) is
  select nvl(ndconlinea,0)
    from hsslinea
   where hsslinea.nnumepres = prestador
     and dvigelinea = (select max(linea.dvigelinea) from hsslinea linea
                        where linea.nnumepres = hsslinea.nnumepres
                          and dvigelinea <= data
                          and (linea.ddvallinea >= data or linea.ddvallinea is null));

begin
  open cr_guia;
  fetch cr_guia into dataAtendimento,prestador;
  close cr_guia;
  
  open cr_DescontoContraste(prestador,dataAtendimento);
  fetch cr_DescontoContraste into desconto_contraste;
  close cr_DescontoContraste;  

  -- Procedimentos
  for reg in (select distinct hsspgui.ccodipmed,nquanpgui,
                     nvl(porte_procedimento(prestador,hsspgui.ccodipmed,trunc(dataAtendimento)),0) porte,
                     hsspgui.nnumeguia,ncontpgui,nqindpgui,hssdpgui.nnumedente,hssdpgui.nnumeface,
                     npercpgui,nper1pgui,nper2pgui,nper3pgui,nper4pgui,cgestpgui,
                     ncoprpgui,nmcdfpgui,nnumecober,nfranpgui,nvemppgui,cpgprpgui,cvidepgui,nvdprpgui,nvdcopgui,nvdfipgui,cmdprpgui
                from hsspgui,hssdpgui
               where hsspgui.nnumeguia = guia
                 and cstatpgui is null
                 and hsspgui.nnumeguia = hssdpgui.nnumeguia(+)
                 and hsspgui.ccodipmed = hssdpgui.ccodipmed(+)) loop

    ValorDiferentePrestador := 'N';
    if (reg.cmdprpgui is not null and (reg.nvdprpgui is not null or reg.nvdcopgui is not null or reg.nvdfipgui is not null)) or
       (nvl(reg.nvdprpgui,0) + nvl(reg.nvdcopgui,0) + nvl(reg.nvdfipgui,0) > 0 ) then
    -- Se tem motivo de pagamento diferenciado cadastrado e ao menos um dos campos de valor preenchido, significa que foi cadastrado valor diferenciado
    -- Fiz dessa forma para evitar problemas com valores cadastrados como zero
      valorDiferentePrincipal := nvl(reg.nvdprpgui,0);
      valorDiferenteCO        := nvl(reg.nvdcopgui,0);
      valorDiferenteFilme     := nvl(reg.nvdfipgui,0);
      ValorDiferentePrestador := 'S';
    end if;

    proximo_procedimento_conta (numero_procedimento);
    insert into hsspcon (nnumepcon,ccodipmed,nnumepres,nnumecont,drealpcon,nvia_pcon,nequipcon,
                         nquanpcon,nportpcon,cpagopcon,ncontpcon,nptxspcon,chinipcon,chfimpcon,
                         nnumedente,nnumeface,
                         npercpcon,nper1pcon,nper2pcon,nper3pcon,nper4pcon,cgestpcon,ncoprpcon,
                         nmcobpcon,nnumecober,nfranpcon,nvemppcon,cformpcon,cvidepcon,nciruhono,ncuoppcon,nvfilpcon)
                 values (numero_procedimento,reg.ccodipmed,prestador,conta,trunc(dataAtendimento),1,1,
                         reg.nquanpgui,reg.porte,'P',nvl(reg.ncontpgui,0) / nvl((100-desconto_contraste)/100,1),
                         reg.porte,null,null,
                         reg.nnumedente,reg.nnumeface,
                         reg.npercpgui,reg.nper1pgui,reg.nper2pgui,reg.nper3pgui,reg.nper4pgui,reg.cgestpgui,
                         reg.ncoprpgui,reg.nmcdfpgui,reg.nnumecober,reg.nfranpgui,reg.nvemppgui,
                         decode(valorDiferentePrestador,'S','M',decode(reg.cpgprpgui,'S','M',decode(reg.npercpgui,null,'A','X'))),
                         reg.cvidepgui,
                         decode(valorDiferentePrestador,'S',nvl(reg.nvdprpgui,0),decode(reg.cpgprpgui,'S',reg.nfranpgui,reg.nqindpgui)),
                         decode(valorDiferentePrestador,'S',nvl(reg.nvdcopgui,0),0),
                         decode(valorDiferentePrestador,'S',nvl(reg.nvdfipgui,0),0)  );

  end loop;

  -- Pacote
  for reg in (select nnumepacot,nvl(nquanpcgui,1) nquanpcgui
                from hsspcgui
               where hsspcgui.nnumeguia = guia
                 and hsspcgui.nnumepacot not in (select nnumepacot from hsspctcm where nnumecont = conta)) loop

    insert into hsspctcm (nnumecont,nnumepacot,nquanpctcm)
                  values (conta,reg.nnumepacot,reg.nquanpcgui);
    insere_pacote (conta);

  end loop;

  -- Taxas
  for reg in (select nnumetaxa,nvalodgui,nquandgui
                from hssdgui
               where nnumeguia = guia
                 and cstatdgui is null ) loop

    insert into hsstcon (nnumecont,nnumetaxa,nvalotcon,nquantcon)
                 values (conta,reg.nnumetaxa,reg.nvalodgui,reg.nquandgui);

  end loop;

  -- Mat/Med/OPME
  for reg in (select nnumeprodu,ctipommgui,nnumetabmm,nquanmmgui,nvpagmmgui
                from hssmmgui
               where nnumeguia = guia
                 and cstatmmgui = 'L' ) loop

    if reg.ctipommgui = 'M' then
      update hsscont set cmdetcont = 'S' where nnumecont = conta;
      
      insert into hssmcon (nnumecont,nnumeprodu,ddatamcon,clocamcon,naquamcon,npquamcon,navalmcon,npvalmcon,nnumetabmm)
                   values (conta,reg.nnumeprodu,trunc(dataAtendimento),'Q',reg.nquanmmgui,reg.nquanmmgui,reg.nvpagmmgui,reg.nvpagmmgui,reg.nnumetabmm);
    else
      insert into hssmmcon (nnumecont,nnumeprodu,ddatammcon,cfornmmcon,naquammcon,npquammcon,navalmmcon,npvalmmcon,nnumetabmm)
                    values (conta,reg.nnumeprodu,NVL(trunc(dataAtendimento),sysdate),'N',reg.nquanmmgui,reg.nquanmmgui,reg.nvpagmmgui,reg.nvpagmmgui,reg.nnumetabmm);
    end if;
  end loop;
  
  atualizaMatMedOPMEConta(conta);  
end;
/                                                    

create or replace procedure fatura_procedimento_web30 (conta        in number,
                                                       guia         in number,
                                                       prestador    in number,
                                                       procedimento in varchar2,
                                                       qtde         in number,
                                                       funcao       in varchar2,
                                                       valor_inst   in number,
                                                       data         in date,
                                                       hora_ini     in varchar2,
                                                       hora_fim     in varchar2,
                                                       via          in number   default 1,
                                                       equipe       in number   default 1,
                                                       idAnest      in number   default null,
                                                       idPAux       in number   default null,
                                                       idSAux       in number   default null,
                                                       idTAux       in number   default null,
                                                       idQAux       in number   default null,
                                                       idInst       in number   default null,
                                                       grau         in varchar2 default null) as

numero_procedimento     number;
desconto_contraste      number;
id_anestesista          number;
id_paux                 number;
id_saux                 number;
id_taux                 number;
id_qaux                 number;
id_inst                 number;
id_cirurgiao            number;
data_atendimento        date;
procedimento2           varchar2(8);
valorDiferentePrincipal number;
valorDiferenteCO        number;
valorDiferenteFilme     number;
ValorDiferentePrestador varchar2(1);
valorDiferenteMotivo    varchar2(100);
procedimentoSeriado     varchar2(1);
congenere               number;
data_guia               date;
acomodacao              number;
tipo_guia               varchar2(1);
local                   number;
id_pacote               number;
descricao_pacote        varchar2(60);
ordem                   number;
valorPagoPrestador      number;

cursor cr_DescontoContraste is
  select nvl(ndconlinea,0)
    from hsslinea
   where hsslinea.nnumepres = prestador
     and dvigelinea = (select max(linea.dvigelinea) from hsslinea linea
                        where linea.nnumepres = hsslinea.nnumepres
                          and dvigelinea <= data
                          and (linea.ddvallinea >= data or linea.ddvallinea is null));

cursor cr_conta is
  select datencont from hsscont
   where nnumecont = conta;
   
cursor cr_ValorDiferenciado is
  select nvdprpgui,nvdcopgui,nvdfipgui,cmdprpgui,ncoprpgui/nquanpgui from hsspgui
   where nnumeguia = guia
     and ccodipmed = procedimento;   

cursor cr_seriado is
  select nvl(cseripmed,'N') from hsspmed
   where ccodipmed = procedimento;  

cursor cr_dados_guia is   
  select nnumeconge, demisguia, hssguia.nnumeacom, ctipoguia, nlocapres 
   from hssguia, hssusua, hsstitu
  where hssguia.nnumeguia = guia
   and hssguia.nnumeusua = hssusua.nnumeusua
   and hssusua.nnumetitu = hsstitu.nnumetitu;   
   
cursor cr_pacotes is
  select hssppaco.nnumepacot,cdescpacot,'1' ordem
   from hssppaco,hsspacot
   where ccodipmed = procedimento
     and dvigeppaco = (select max(ppaco.dvigeppaco) from hssppaco ppaco
                        where ppaco.nnumepacot = hssppaco.nnumepacot
                          and ppaco.ccodipmed = hssppaco.ccodipmed)
     and hssppaco.nnumepacot = hsspacot.nnumepacot
     and nnumepres = prestador
     and nvl(cstatpacot,'A') = 'A'
     and dvigepacot = (select max(dvigepacot) from hsspacot pacot
                        where pacot.nnumepacot = hsspacot.nnumepacot
                          and dvigepacot <= sysdate)
     and (cnatupacot = tipo_guia or cnatupacot = 'B')
     and (nlocapres = local or nlocapres is null)
     and (congenere in (select nnumeconge from hsscopac where nnumepres = hsspacot.nnumepres and nnumepacot = hsspacot.nnumepacot))
     and (nnumeacom = acomodacao or nnumeacom is null)          
   union all
   select hssppaco.nnumepacot,cdescpacot,'2'
   from hssppaco,hsspacot
   where ccodipmed = procedimento
     and dvigeppaco = (select max(ppaco.dvigeppaco) from hssppaco ppaco
                       where ppaco.nnumepacot = hssppaco.nnumepacot
                         and ppaco.ccodipmed = hssppaco.ccodipmed)
     and hssppaco.nnumepacot = hsspacot.nnumepacot
     and nnumepres = prestador
     and nvl(cstatpacot,'A') = 'A'
     and dvigepacot = (select max(dvigepacot) from hsspacot pacot
                       where pacot.nnumepacot = hsspacot.nnumepacot
                         and dvigepacot <= sysdate)
     and (cnatupacot = tipo_guia or cnatupacot = 'B')
     and (nlocapres = local or nlocapres is null)
     and (nnumepres not in (select nnumeconge from hsscopac where nnumepres = hsspacot.nnumepres))
     and (nnumeacom = acomodacao or nnumeacom is null)                     
   order by 3;
   
begin
  data_atendimento := data;

  if data_atendimento is null then
    open cr_conta;
    fetch cr_conta into data_atendimento;
    close cr_conta;
  end if;

  -- se for informado o procedimento, significa que � faturamento de honor�rio individual
  
  if ((procedimento = '') or (procedimento is null)) then
    procedimento2 := null;
  else
    procedimento2 := procedimento;
  end if;
    
  if (nvl(procedimento2,' ') <> ' ') then

    if (nvl(funcao,' ') = ' ') then
      id_cirurgiao   := prestador;
      id_anestesista := idAnest;
      id_paux        := idPAux;
      id_saux        := idSAux;
      id_taux        := idTAux;
      id_qaux        := idQAux;
      id_inst        := idInst;    
    else
      if (funcao = '06') then
        id_anestesista := prestador;
      elsif (funcao = '01') then
        id_paux := prestador;
      elsif (funcao = '02') then
        id_saux := prestador;
      elsif (funcao = '03') then
        id_taux := prestador;
      elsif (funcao = '04') then
        id_qaux := prestador;
      elsif (funcao = '05') then
        id_inst := prestador;
      else
        id_cirurgiao := prestador;
      end if;
    end if;
    
    open cr_ValorDiferenciado;
    fetch cr_ValorDiferenciado into valorDiferentePrincipal,valorDiferenteCO,valorDiferenteFilme,valorDiferenteMotivo,valorPagoPrestador;
    close cr_ValorDiferenciado;
    
    if (valorDiferenteMotivo is not null and (valorDiferentePrincipal is not null or valorDiferenteCO is not null or valorDiferenteFilme is not null)) or
      (nvl(valorDiferentePrincipal,0) + nvl(valorDiferenteCO,0) + nvl(valorDiferenteFilme,0) > 0 ) then
      ValorDiferentePrestador := 'M';
    end if; 
 
    open cr_seriado;
    fetch cr_seriado into procedimentoSeriado;
    close cr_seriado; 
        
    if (parametro_string('CCAPS_WEB') = '1') and (procedimentoSeriado = 'S') then
    
      open cr_dados_guia;
      fetch cr_dados_guia into congenere,data_guia,acomodacao,tipo_guia,local;
      close cr_dados_guia;     
        
      if ((tipo_guia = 'I') or (tipo_guia = 'C') or (tipo_guia = 'O') or (tipo_guia = 'R') or (tipo_guia = 'P') or (tipo_guia = 'S')) then
        tipo_guia := 'I';  
      end if;  

      for procedimentos in (select ccodipmed,nquanpgui from hsspgui
                            where hsspgui.nnumeguia = guia) loop 

        open cr_pacotes;
        fetch cr_pacotes into id_pacote,descricao_pacote,ordem;
          
        if cr_pacotes%found then
          insert into hsspctcm (nnumecont,nnumepacot,nquanpctcm)
                        values (conta,id_pacote,qtde);                  
          insere_pacote (conta);
        else
          proximo_procedimento_conta (numero_procedimento);
          insert into hsspcon (nnumepcon,ccodipmed,nnumecont,drealpcon,nvia_pcon,nequipcon,
                               nquanpcon,nportpcon,cpagopcon,ncontpcon,nptxspcon,chinipcon,chfimpcon,
                               cformpcon,nnumepres,nanespres,
                               npauxpres,nsauxpres,ntauxpres,nqauxpres,ninstpres,ninsthono,nciruhono,ncuoppcon,nvfilpcon,cgraupcon)
                       values (numero_procedimento,procedimento2,conta,trunc(data_atendimento),via,equipe,
                               qtde,nvl(porte_procedimento(prestador,procedimento2,data),0),'P',0,nvl(porte_procedimento(prestador,procedimento2,data),0),hora_ini,hora_fim,
                               nvl(ValorDiferentePrestador,'A'),id_cirurgiao,id_anestesista,
                               id_paux,id_saux,id_taux,id_qaux,id_inst,valor_inst,nvl(valorDiferentePrincipal,0),nvl(valorDiferenteCO,0),nvl(valorDiferenteFilme,0),grau);    
        end if;
        close cr_pacotes;
      end loop;
      
    else
      proximo_procedimento_conta (numero_procedimento);
      insert into hsspcon (nnumepcon,ccodipmed,nnumecont,drealpcon,nvia_pcon,nequipcon,
                           nquanpcon,nportpcon,cpagopcon,ncontpcon,nptxspcon,chinipcon,chfimpcon,
                           cformpcon,nnumepres,nanespres,
                           npauxpres,nsauxpres,ntauxpres,nqauxpres,ninstpres,ninsthono,nciruhono,ncuoppcon,nvfilpcon,cgraupcon,ncoprpcon)
                   values (numero_procedimento,procedimento2,conta,trunc(data_atendimento),via,equipe,
                           qtde,nvl(porte_procedimento(prestador,procedimento2,data),0),'P',0,nvl(porte_procedimento(prestador,procedimento2,data),0),hora_ini,hora_fim,
                           nvl(ValorDiferentePrestador,'A'),id_cirurgiao,id_anestesista,
                           id_paux,id_saux,id_taux,id_qaux,id_inst,valor_inst,nvl(valorDiferentePrincipal,0),nvl(valorDiferenteCO,0),nvl(valorDiferenteFilme,0),grau,valorPagoPrestador*qtde);
    end if;
  else
    open cr_DescontoContraste;
    fetch cr_DescontoContraste into desconto_contraste;
    close cr_DescontoContraste;

    for reg in (select distinct hsspgui.ccodipmed,nquanpgui,hssguia.nnumepres,
                       nvl(porte_procedimento(hssguia.nnumepres,hsspgui.ccodipmed,trunc(data_atendimento)),0) porte,
                       hsspgui.nnumeguia,ncontpgui,nqindpgui,hssdpgui.nnumedente,hssdpgui.nnumeface,
                       npercpgui,nper1pgui,nper2pgui,nper3pgui,nper4pgui,cgestpgui,
                       ncoprpgui,nmcdfpgui,nnumecober,nfranpgui,nvemppgui,cpgprpgui,cvidepgui,nvdprpgui,nvdcopgui,nvdfipgui,cmdprpgui
                  from hssguia,hsspgui,hssdpgui
                 where hssguia.nnumeguia = guia
                   and hssguia.nnumeguia = hsspgui.nnumeguia
                   and cstatpgui is null
                   and hsspgui.nnumeguia = hssdpgui.nnumeguia(+)
                   and hsspgui.ccodipmed = hssdpgui.ccodipmed(+)) loop
                   
      ValorDiferentePrestador := 'N';
      if (reg.cmdprpgui is not null and (reg.nvdprpgui is not null or reg.nvdcopgui is not null or reg.nvdfipgui is not null)) or
         (nvl(reg.nvdprpgui,0) + nvl(reg.nvdcopgui,0) + nvl(reg.nvdfipgui,0) > 0 ) then
      -- Se tem motivo de pagamento diferenciado cadastrado e ao menos um dos campos de valor preenchido, significa que foi cadastrado valor diferenciado
      -- Fiz dessa forma para evitar problemas com valores cadastrados como zero
        valorDiferentePrincipal := nvl(reg.nvdprpgui,0);
        valorDiferenteCO := nvl(reg.nvdcopgui,0);
        valorDiferenteFilme := nvl(reg.nvdfipgui,0);
        ValorDiferentePrestador := 'S';
      end if;                  

      proximo_procedimento_conta (numero_procedimento);
      insert into hsspcon (nnumepcon,ccodipmed,nnumepres,nnumecont,drealpcon,nvia_pcon,nequipcon,
                           nquanpcon,nportpcon,cpagopcon,ncontpcon,nptxspcon,chinipcon,chfimpcon,
                           nnumedente,nnumeface,
                           npercpcon,nper1pcon,nper2pcon,nper3pcon,nper4pcon,cgestpcon,ncoprpcon,
                           nmcobpcon,nnumecober,nfranpcon,nvemppcon,cformpcon,cvidepcon,nciruhono,ncuoppcon,nvfilpcon)
                   values (numero_procedimento,reg.ccodipmed,reg.nnumepres,conta,trunc(data_atendimento),1,1,
                           reg.nquanpgui,reg.porte,'P',reg.ncontpgui / nvl((100-desconto_contraste)/100,1),
                           reg.porte,hora_ini,hora_fim,
                           reg.nnumedente,reg.nnumeface,
                           reg.npercpgui,reg.nper1pgui,reg.nper2pgui,reg.nper3pgui,reg.nper4pgui,reg.cgestpgui,
                           reg.ncoprpgui,reg.nmcdfpgui,reg.nnumecober,reg.nfranpgui,reg.nvemppgui,
                           decode(valorDiferentePrestador,'S','M',decode(reg.cpgprpgui,'S','M',decode(reg.npercpgui,null,'A','X'))),
                           reg.cvidepgui,
                           decode(valorDiferentePrestador,'S',nvl(reg.nvdprpgui,0),decode(reg.cpgprpgui,'S',reg.nfranpgui,reg.nqindpgui)),
                           decode(valorDiferentePrestador,'S',nvl(reg.nvdcopgui,0),null),
                           decode(valorDiferentePrestador,'S',nvl(reg.nvdfipgui,0),null)  );

    end loop;
    

    
  end if;  

  for reg in (select nnumepacot,nvl(nquanpcgui,1) nquanpcgui
                from hssguia,hsspcgui
               where hssguia.nnumeguia = guia
                 and hssguia.nnumeguia = hsspcgui.nnumeguia
                 and hsspcgui.nnumepacot not in (select nvl(nnumepacot,0) from hsspctcm where nnumecont = conta)) loop
   
    insert into hsspctcm (nnumecont,nnumepacot,nquanpctcm)
                  values (conta,reg.nnumepacot,reg.nquanpcgui);                  
    insere_pacote (conta);
    
  end loop;    
  
end;
/

create or replace procedure fatura_taxa_web30 (conta        in number,
                                             taxa         in number,
                                             quantidade   in number) as
valor     number;
prestador number;
usuario   number;
contrato  number;
data      date;
congenere number;

cursor cr_conta is
  select nlocapres,hsscont.nnumeusua,hssusua.nnumetitu,datencont,hsstitu.nnumeconge
    from hsscont,hssusua,hsstitu
   where hsscont.nnumecont = conta
     and hsscont.nnumeusua = hssusua.nnumeusua
     and hssusua.nnumetitu = hsstitu.nnumetitu;     

begin

  open cr_conta;
  fetch cr_conta into prestador,usuario,contrato,data,congenere;
  close cr_conta;

  valor := preco_taxa(taxa,prestador,contrato,data,usuario,congenere);

  insert into hsstcon (nnumecont,nnumetaxa,nvalotcon,nquantcon)
               values (conta,taxa,valor,quantidade);

end;
/

create or replace procedure fatura_matmed_web30 (conta           in number,
                                                 produto         in number,
                                                 quantidade      in number,
                                                 tipoProduto     in varchar2,
                                                 valorInformado  in number default null,
                                                 tabelaInformada in number default null,
                                                 refatura        in varchar2 default null) as
valor             number;
prestador         number;
usuario           number;
contrato          number;
congenere         number;
data              date;
idTabelaUsada     number;
produtoValorizado number;
plano             number;
valorGlosado      number;
valorApresentado  number;
valorTabela       number;
valorPago         number;
idMatMed          number;
justificativa     number;
pacote            number;
executante        number;

cursor cr_conta is
  select nlocapres,hsscont.nnumeusua,hssusua.nnumetitu,datencont,hsstitu.nnumeconge,hsscont.nnumepres
    from hsscont,hssusua,hsstitu
   where hsscont.nnumecont = conta
     and hsscont.nnumeusua = hssusua.nnumeusua
     and hssusua.nnumetitu = hsstitu.nnumetitu;

cursor cr_plano is
  select nnumeplan 
    from hssusua
   where nnumeusua = usuario;
   
cursor cr_matmed is
  select nnumemcon 
    from hssmcon
   where nnumecont = conta
     and nnumeprodu = produto;

cursor cr_justificativa is
  select nnumejusti
    from hssjusti
   where  cans_justi='1705';   

cursor cr_pacote is
  select hsspctcm.nnumepacot 
    from hsspctcm, hssmpaco   
  where nnumecont = conta
    and hsspctcm.nnumepacot = hssmpaco.nnumepacot
    and nnumeprodu = produto;   

begin
  -- ao inserir um item na hssmcon, altero o campo cmdetcont para Sim e chamo a funcao atualizamatmedopmeconta
  update hsscont
     set cmdetcont = 'S'
   where nnumecont = conta;
     
  open cr_conta;
  fetch cr_conta into prestador,usuario,contrato,data,congenere,executante;
  close cr_conta;

  --pega o plano do usuario
  open  cr_plano;
  fetch cr_plano into plano;
  close cr_plano;   

  
  if executante = 972 and cnpj_operadora = '50480953000172' then -- Prestador JJ e Unimed Limeira 
    valor := preco_produto_plano(produto,executante,sysdate,congenere,contrato,produtoValorizado,0,idTabelaUsada,plano);
  else 
    valor := preco_produto_plano(produto,prestador,sysdate,congenere,contrato,produtoValorizado,0,idTabelaUsada,plano);
  end if;  

  if valorInformado is not null then
    valorTabela := valor;
    valor := valorInformado;
  end if;
  
  
  if tabelaInformada is not null then
    idTabelaUsada := tabelaInformada;
  end if;
  
  open  cr_pacote;
  fetch cr_pacote into pacote;
  close cr_pacote;     

  
  if tipoProduto = 'M' then
  
    if refatura is null then
    
      insert into hssmcon (nnumecont,nnumeprodu,ddatamcon,clocamcon,naquamcon,npquamcon,navalmcon,npvalmcon,nnumetabmm)
                   values (conta,produto,data,'Q',quantidade,quantidade,valor,valor,idTabelaUsada);
        
      if (valorInformado > valorTabela) and (nvl(pacote,0) = 0) then
        valorApresentado := quantidade * valorInformado;
        valorPago := quantidade * valorTabela;
        valorGlosado := valorApresentado - valorPago;
        
        --pega o numero mat/med na hssmcon
        open  cr_matmed;
        fetch cr_matmed into idMatMed;
        close cr_matmed;   
        
        open  cr_justificativa;
        fetch cr_justificativa into justificativa;
        close cr_justificativa;
        
        insert into hssglosa(nnumecont,citemglosa,nvaloglosa,napreglosa,npagoglosa,nnumejusti,nquanglosa,nnumemcon,cjustglosa,cformglosa)
                      values(conta,'M',valorGlosado,valorApresentado,valorPago,justificativa,quantidade,idMatMed,'Cobran�a incorreta/indevida de Mat/Med','A');
        
        update hssmcon set navalmcon=valorInformado, npvalmcon=valorTabela where nnumecont=conta and nnumeprodu=produto;
           
      end if;
      
   else
      update hssmmcon set npquammcon = quantidade, naquammcon = quantidade,navalmmcon = decode(valor,null,navalmmcon,valor), npvalmmcon = decode(valor,null,npvalmmcon,valor)
                            where nnumecont = conta and nnumeprodu = produto;
                            
       update hssmcon set npquamcon = quantidade, naquamcon = quantidade where nnumecont=conta and nnumeprodu=produto;                      
        
      if (valorInformado > valorTabela) and (nvl(pacote,0) = 0) then
        valorApresentado := quantidade * valorInformado;
        valorPago := quantidade * valorTabela;
        valorGlosado := valorApresentado - valorPago;
        
        --pega o numero mat/med na hssmcon
        open  cr_matmed;
        fetch cr_matmed into idMatMed;
        close cr_matmed;   

        delete hssglosa where nnumecont=conta and nnumemcon=idMatMed;
        
        open  cr_justificativa;
        fetch cr_justificativa into justificativa;
        close cr_justificativa;

        insert into hssglosa(nnumecont,citemglosa,nvaloglosa,napreglosa,npagoglosa,nnumejusti,nquanglosa,nnumemcon,cjustglosa,cformglosa)
                      values(conta,'M',valorGlosado,valorApresentado,valorPago,justificativa,quantidade,idMatMed,'Cobran�a incorreta/indevida de Mat/Med','A');
        
        update hssmcon set navalmcon=valorInformado, npvalmcon=valorTabela where nnumecont=conta and nnumeprodu=produto;
      end if;
   end if;
                 
  else
    insert into hssmmcon (nnumecont,nnumeprodu,ddatammcon,cfornmmcon,naquammcon,npquammcon,navalmmcon,npvalmmcon,nnumetabmm)
                  values (conta,produto,data,'N',quantidade,quantidade,valor,valor,idTabelaUsada);
  end if;

  atualizaMatMedOPMEConta(conta);
end;
/

create or replace procedure atualiza_valor_lote30 (lote number) as

total number;
Adiantamentos number;

cursor cr_hssadian is
  select sum(nvalodocu)
    from hssadian, findocu
   where hssadian.nnumelote = lote
     and hssadian.nnumedocu = findocu.nnumedocu;

begin
  select sum(total_conta_plano(nnumecont)) into total
    from hsscont
   where nnumelote = lote;

  open cr_hssadian;
  fetch cr_hssadian into adiantamentos;
  close cr_hssadian;

  adiantamentos := nvl(adiantamentos,0);

  if Adiantamentos > Total then
    Total := Adiantamentos;
  end if;

  update hsslote set nvalolote = total
   where nnumelote = lote;
end;
/

create or replace procedure cancela_faturamento_web30 (guia    in     number,
                                                       retorno    out number)  as

contratado          number;
dia                 number;
mes                 number;
competencia         varchar2(7);
dia_inicio          number;
dia_fim             number;
dia_pagamento       number;
ordem               number;
lote                number;
lote_guia           number;
situacao_lote       varchar2(1);
conta               number;
data_pagamento      date;

cursor cr_guia is
  select nlocapres
    from hssguia
   where nnumeguia = guia;

cursor cr_lote (pagamento in date) is
  select nnumelote
    from hsslote
   where ctipolote = 'W'
     and nnumepres = contratado
     and csitulote = 'A'
     and dprevlote = pagamento
     and nnumelote = lote_guia;

cursor cr_datas (dia in number) is
  select 0 ordem, ndiniclpag,ndfimclpag,ndiapclpag,nmespclpag                                 
    from hssclpag                                                                
   where nnumepres = contratado                                                  
     and (
          ( ndiniclpag < ndfimclpag and 
            ndiniclpag <= to_number(to_char(sysdate,'DD')) and 
            ndfimclpag >= to_number(to_char(sysdate,'DD')) ) or
            
          ( ndiniclpag > ndfimclpag and 
            to_number(to_char(sysdate,'MM') || acrescenta_zeros(ndiniclpag,2)) <= to_number(to_char(sysdate,'MMDD')) and 
            to_number(to_char(add_months(sysdate,1),'MM') || acrescenta_zeros(ndfimclpag,2)) >= to_number(to_char(sysdate,'MMDD')) ) or
            
          ( ndiniclpag > ndfimclpag and 
            to_number(to_char(add_months(sysdate,-1),'MM') || acrescenta_zeros(ndiniclpag,2)) <= to_number(to_char(sysdate,'MMDD')) and 
            to_number(to_char(sysdate,'MM') || acrescenta_zeros(ndfimclpag,2)) >= to_number(to_char(sysdate,'MMDD')) ) 
         )
     and clocaclpag in ('W','A')     
     and dvigeclpag = (select max(dvigeclpag) from hssclpag
                        where nnumepres = contratado
                          and dvigeclpag <= sysdate)
   union                                                                        
  select 1 ordem, 1,31,10,0                              
    from dual                                                               
   order by 1;    

cursor cr_guia_faturada is
  select hssgcon.nnumecont,hsscont.nnumelote,csitulote
    from hssgcon,hsscont,hsslote
   where hssgcon.nnumeguia = guia
     and hssgcon.nnumecont = hsscont.nnumecont
     and hsscont.nnumelote = hsslote.nnumelote;

-- 0 - Guia removida com sucesso
-- 1 - Guia inexistente
-- 2 - Lote fechado
-- 3 - Lote fora do periodo de movimenta��o
-- 4 - Guia n�o faturada
-- 6 - Datas n�o configuradas

begin
  open cr_guia;
  fetch cr_guia into contratado;
  if cr_guia%found then

    open cr_guia_faturada;
    fetch cr_guia_faturada into conta,lote_guia,situacao_lote;
    close cr_guia_faturada;

    if situacao_lote = 'F' then
      retorno := 2;
    elsif conta > 0 then
      dia := to_number(to_char(sysdate,'DD'));
      competencia := to_char(sysdate,'MM/YYYY');

      open cr_datas(dia);
      fetch cr_datas into ordem,dia_inicio,dia_fim,dia_pagamento,mes;
      if cr_datas%found then

        if dia_fim > dia_pagamento and mes = 0 then
          mes := 1;
        end if;
        
        data_pagamento := add_months(MontaData(dia_pagamento,to_number(substr(competencia,1,2)),to_number(substr(competencia,4,4))),mes);
        
        open cr_lote (data_pagamento);
        fetch cr_lote into lote;
        close cr_lote;

        if lote = lote_guia then
          delete from hsscont where nnumecont = conta;
          retorno := 0;

          atualiza_valor_lote30(lote_guia);
        else
          retorno := 3;
        end if;
      else
        retorno := 6;
      end if;
      close cr_datas;
    else
      retorno := 4;
    end if;
  else
    retorno := 1;
  end if;

  close cr_guia;
end;
/

create or replace procedure insere_protocolo_web30 (tipo      in     varchar2,
                                                  historico in     varchar2,
                                                  operador  in     number,
                                                  id        in     number,
                                                  operacao  in     varchar2,
                                                  protocolo    out number,
                                                  assunto   in varchar2 default null) as
begin
-- tipo 
-- C - Corpo clinico
-- G - Faturamento de guias
-- A - Apresenta??o de produ??o
-- U - upload empresa
-- P - upload prestador
-- E - email boleto comercial
-- P - atualiza��o do cadastro do prestador
-- L - Fechamento de lote Produ??o /Opera??o F

  select seqprotw.nextval into protocolo from dual;

  insert into hssprotw (nnumeprotw,ctipoprotw,ddataprotw,noperusua,cdescprotw,nid__protw,coperprotw,cassuprotw)
                values (protocolo,tipo,sysdate,operador,historico,id,operacao,assunto);
end;
/

create or replace procedure insere_log_web30 (id         in     number,
                                              descricao  in     varchar2,
                                              operador   in     number,
                                              ip         in     varchar2,
                                              protocolo  out    number,
                                              local_log in      varchar2 default null) as
begin
  select seqlogwe.nextval into protocolo from dual;

  insert into hsslogwe (nnumelogwe,nid__logwe,coperlogwe,noperusua,cip__logwe,ddatalogwe,clocalogwe)
                values (protocolo,id,descricao,operador,ip,sysdate,local_log);
end;
/

create or replace procedure insere_item_protocolo_web30 (protocolo in number, 
                                                       id        in number,
                                                       operacao  in varchar2) as

codigo         varchar2(20);
nome           varchar2(60);
cpf            varchar2(15);
lote           number;
conta          number;
historico      varchar2(200);
valor          number;
tipo           varchar2(1);
executante     varchar2(60);

cursor cr_tipo is 
  select ctipoprotw
    from hssprotw
   where nnumeprotw = protocolo;

cursor cr_dados is
  select ccodiusua,nvl(cnomeusua,ccortguia),nprotlote,hsscont.nnumecont,total_conta_plano(hsscont.nnumecont),cnomepres
    from hssguia,finpres,hssgcon,hsscont,hsslote,hssusua
   where hssguia.nnumeguia = id
     and hssguia.nnumepres = finpres.nnumepres
     and hssguia.nnumeguia = hssgcon.nnumeguia
     and hssgcon.nnumecont = hsscont.nnumecont
     and hsscont.nnumelote = hsslote.nnumelote
     and hssguia.nnumeusua = hssusua.nnumeusua(+)
   order by hsscont.nnumecont desc;         
     
cursor cr_dados_guia is
  select hssusua.ccodiusua,nvl(hssusua.cnomeusua,ccortguia),total_guia(hssguia.nnumeguia),cnomepres
    from hssguia,finpres,hssusua
   where hssguia.nnumeguia = id
     and hssguia.nnumepres = finpres.nnumepres
     and hssguia.nnumeusua = hssusua.nnumeusua(+);
     
cursor cr_dados_prestador is
  select cconspres || '-' || cufcrpres || ' ' || to_char(ncrm_pres),cnomepres,ccpf_pres
    from finpres  
   where nnumepres = id;

begin
  open cr_tipo;
  fetch cr_tipo into tipo;
  close cr_tipo;
    
  -- faturamento de guia
  if tipo = 'G' then
    open cr_dados;
    fetch cr_dados into codigo,nome,lote,conta,valor,executante;

    historico := lpad(id,12,' ') || ' ' ||
                 lpad(conta,12,' ') || ' ' ||
                 lpad(lote,12,' ') || ' ' ||
                 rpad(codigo,16,' ') || ' ' ||
                 rpad(nome,60,' ') || ' ' ||
                 lpad(valor,12,' ') || ' ' ||
                 rpad(executante,60);

    close cr_dados;
    
  elsif tipo = 'A' then
  
    open cr_dados_guia;
    fetch cr_dados_guia into codigo,nome,valor,executante;

    historico := lpad(id,12,' ') || ' ' ||
                 rpad(codigo,16,' ') || ' ' ||
                 rpad(nome,60,' ') || ' ' ||
                 lpad(valor,12,' ') || ' ' ||
                 rpad(executante,60,' ');

    close cr_dados_guia;    

  elsif tipo = 'C' then 
  
    open cr_dados_prestador;
    fetch cr_dados_prestador into codigo,nome,cpf;

    historico := rpad(codigo,15,' ') || ' ' ||
                 rpad(nome,60,' ') || ' ' ||
                 rpad(cpf,15,' ');

    close cr_dados_prestador;  
  
  end if;
  
  insert into hssitprw (nnumeitprw,nnumeprotw,chistitprw,coperitprw)
                values (seqitprw.nextval,protocolo,historico,operacao);                  
  
end;
/

create or replace function data_apresentacao_guia30(guia in number) return varchar2 as

data_apresentacao varchar2(10);

cursor cr_data is
  select to_char(trunc(ddigigint))
    from hssgint
   where nnumeguia = guia;

begin
  open cr_data;
  fetch cr_data into data_apresentacao;
  if cr_data%found then
    return data_apresentacao;
  else
    return '';
  end if;
end;
/

create or replace procedure atualiza_prestador30 (id_prestador      in     number,
                                                natureza_juridica in     varchar2,
                                                nome              in     varchar2,
                                                fantasia          in     varchar2,
                                                cnpj              in     varchar2,
                                                cpf               in     varchar2,
                                                nascimento        in     date,
                                                sexo              in     varchar2,
                                                rg                in     varchar2,
                                                pis               in     varchar2,
                                                cep               in     varchar2,
                                                tipo_logradouro   in     number,
                                                endereco          in     varchar2,
                                                numero            in     varchar2,
                                                bairro            in     varchar2,
                                                compl             in     varchar2,                           
                                                cidade            in     varchar2,
                                                estado            in     varchar2,    
                                                contato           in     varchar2,
                                                fax               in     varchar2,
                                                telefone          in     varchar2,
                                                banco             in     number,
                                                tipoconta         in     varchar2,
                                                agencia           in     varchar2,
                                                conta             in     varchar2,
                                                operador          in     number,
                                                retorno              out number) as

nomeA              varchar2(60);
fantasiaA          varchar2(60);
cnpjA              varchar2(18);
cpfA               varchar2(14);
nascimentoA        date;
sexoA              varchar2(1);
rgA                varchar2(14);
pisA               varchar2(11);
cepA               varchar2(9);
tipo_logradouroA   number;
enderecoA          varchar2(60);
numeroA            varchar2(5);
bairroA            varchar2(60);
complA             varchar2(60);                           
cidadeA            varchar2(60);
estadoA            varchar2(2);
contatoA           varchar2(15);
faxA               varchar2(15);
telefoneA          varchar2(15);
contaA             varchar2(15);
agenciaA           varchar2(10);
tipocontaA         varchar2(1);
bancoA             number;

id_tipo_logradouro number;
id_banco           number;

begin 
  altera_application_info(operador);
  
  if tipo_logradouro > 0 then
    id_tipo_logradouro := tipo_logradouro;
  else
    id_tipo_logradouro := null;
  end if;
  
  if banco > 0 then
    id_banco := banco;
  else
    id_banco := null;
  end if;  
 
  select cnomepres,cfantpres,ccgc_pres,ccpf_pres,dnascpres,csexopres,crg__pres,cpis_pres,ccep_pres,nnumetlogr,cendepres,
         cnendpres,cbairpres,crefepres,ccidapres,cestapres,ccon1pres,cfax_pres,cfonepres,nnumecdba,ctipcpres,cagenpres,ccc__pres
  
    into nomeA,fantasiaA,cnpjA,cpfA,nascimentoA,sexoA,rgA,pisA,cepA,tipo_logradouroA,enderecoA,
         numeroA,bairroA,complA,cidadeA,estadoA,contatoA,faxA,telefoneA,bancoA,tipocontaA,agenciaA,contaA
    from finpres
   where nnumepres = id_prestador;
  
  if nvl(nome,' ')                   <> nvl(nomeA,' ')                  or 
     nvl(fantasia,' ')               <> nvl(fantasiaA,' ')              or 
     nvl(cnpj,' ')                   <> nvl(cnpjA,' ')                  or
     nvl(cpf,' ')                    <> nvl(cpfA,' ')                   or 
     nvl(nascimento,trunc(sysdate))  <> nvl(nascimentoA,trunc(sysdate)) or
     nvl(sexo,' ')                   <> nvl(sexoA,' ')                  or
     nvl(rg,' ')                     <> nvl(rgA,' ')                    or 
     nvl(pis,' ')                    <> nvl(pisA,' ')                   or
     nvl(cep,' ')                    <> nvl(cepA,' ')                   or 
     nvl(id_tipo_logradouro,0)       <> nvl(tipo_logradouroA,0)         or
     nvl(id_banco,0)                 <> nvl(bancoA,0)                   or
     nvl(endereco,' ')               <> nvl(enderecoA,' ')              or
     nvl(numero,' ')                 <> nvl(numeroA,' ')                or
     nvl(bairro,' ')                 <> nvl(bairroA,' ')                or
     nvl(compl,' ')                  <> nvl(complA,' ')                 or
     nvl(cidade,' ')                 <> nvl(cidadeA,' ')                or
     nvl(estado,' ')                 <> nvl(estadoA,' ')                or
     nvl(contato,' ')                <> nvl(contatoA,' ')               or
     nvl(telefone,' ')               <> nvl(telefoneA,' ')              or
     nvl(conta,' ')                  <> nvl(contaA,' ')                 or
     nvl(agencia,' ')                <> nvl(agenciaA,' ')               or
     nvl(tipoconta,' ')              <> nvl(tipocontaA,' ')             or
     nvl(fax,' ')                    <> nvl(faxA,' ')                   then 
     
     update finpres 
        set cnomepres = nome,
            cfantpres = fantasia, 
            ccgc_pres = cnpj,
            ccpf_pres = cpf,
            dnascpres = nascimento,
            csexopres = sexo,
            crg__pres = rg,
            cpis_pres = pis,
            ccep_pres = cep,
            nnumetlogr = id_tipo_logradouro,
            cendepres = endereco,
            cnendpres = numero,
            crefepres = compl,
            cbairpres = bairro,
            cestapres = estado,
            ccidapres = cidade,
            ccon1pres = contato,
            cfax_pres = fax,
            cfonepres = telefone,
            nnumecdba = id_banco,
            cagenpres = agencia,
            ccc__pres = conta,
            ctipcpres = tipoconta,
            noperusua = operador
      where nnumepres = id_prestador;
      
    retorno := 0;
  else
    retorno := 1;
  end if;
end;
/

create or replace procedure GERA_PLANEJAMENTO_AGENDA30( prestador    in     number,
                                                        ambulatorio  in     number,
                                                        operador     in     number,
                                                        agendaweb    in     varchar2,
                                                        localagenda  in     number default 2,
                                                        inicio       in     date,
                                                        fim          in     date,
                                                        gerado          out varchar2) as 
                                                      
-- prestador id do prestador
-- ambulatorio id do ambulatorio
-- localagenda  1    = Agendado pelo beneficiario na web
--              2    = Agendado pela web
--              3    = Agendado pela medicina ocupacional
--              nulo = Agendado pela central/ambula

-- agendaweb = Agenda criada pela web? (Sim | N�o)
                
data           date;
horario        varchar2(5);
hora           number;
minuto         number;
intervalo_zero varchar2(1);
agenda         number;
acumulado      number;
hora_tempo     number;
minuto_tempo   number;
valida         number;
 
cursor cr_agenda is
  select nnumeagend
    from HSSAGEND
   where nnumepres = prestador
     and nambupres = ambulatorio
     and ddataagend = data
     and choraagend = horario;

begin
  gerado := '';
  valida := 0;
  intervalo_zero := 'N';
  
  delete from hssagend
   where nambupres = ambulatorio
     and nnumepres = prestador
     and ddataagend >= inicio
     and ddataagend <= fim
     and nnumeusua is null
     and ccortagend is null
     and nnumenusua is null
     and nnumeagend not in (select nnumeagend from hssagpac);
     
  for reg in (select ndia_pagen, chinipagen, chfinpagen, cintepagen,nnumeespec,ctipopagen, ctatepagen, nnumeconve
                from hsspagen
               where nnumepres = prestador
                 and nambupres = ambulatorio) loop

    data         := inicio;
    hora_tempo   := substr(reg.cintepagen,1,2);
    minuto_tempo := substr(reg.cintepagen,4,2);

    if minuto_tempo = 0 then
      intervalo_zero :='S';
    end if;
    
    loop

      if to_char(data,'D') = reg.ndia_pagen then
        horario := reg.chinipagen;
        hora    := substr(reg.chinipagen,1,2);
        minuto  := substr(reg.chinipagen,4,2);

        loop
          open cr_agenda;
          fetch cr_agenda into agenda;
            if cr_agenda%notfound then
              insert into hssagend (nnumepres,nambupres, ddataagend,choraagend, csituagend,cagewagend, clocaagend,
                                    dcriaagend,ngerausua,nnumeespec,ctipoagend,ctateagend,nnumeconve)
                            values (prestador,ambulatorio, data,horario, 'A', agendaweb,localagenda,
                                    sysdate,operador,reg.nnumeespec,reg.ctipopagen,reg.ctatepagen,reg.nnumeconve);
              valida:=1;
            end if;
          close cr_agenda;

          hora := hora + hora_tempo;
          acumulado := (minuto + minuto_tempo);

          if (acumulado >= 60) or (acumulado = 0 and intervalo_zero <> 'S') then
            minuto := acumulado - 60;
            hora   := hora + 1;
          else
            minuto := acumulado;
          end if;
          horario := acrescenta_zeros(hora,2) ||':'|| acrescenta_zeros(minuto,2);
          exit when horario > reg.chfinpagen;
        end loop;
      end if;
      
      data := data + 1;
      exit when data > fim;
    end loop;
  end loop;

  if valida = 0 then
    gerado :=0; --'N�o foi poss�vel gerar planejamento para este periodo.';
  else
    gerado :=1;--'Planejamento gerado com sucesso.';  
  end if;  
end;
/

create sequence seqprotw start with 1 nocache
/

create sequence seqitprw start with 1 nocache
/

declare
  cgc     varchar2(18);
  apelido varchar2(30);
begin
  select somente_numeros_string(ccgc_empr) into cgc 
    from finempr;
    
  if (cgc = '00299149000113') then
    apelido := 'affego';
  elsif (cgc = '20146064000102') then
    apelido := 'amc';
  elsif (cgc = '04966545000108') then 
    apelido := 'amazoniaSaude';   
  elsif (cgc = '00200720000109') then 
    apelido := 'apaspp';
  elsif (cgc = '00665690000106') then 
    apelido := 'apasDracena';
  elsif (cgc = '71565659000133') then
    apelido := 'apasItapetininga';
  elsif (cgc = '03716044000100') then 
    apelido := 'asl';
  elsif (cgc = '12317012000123') then
    apelido := 'asfal';      
  elsif (cgc = '03016500000100') then
    apelido := 'assimedica';
  elsif (cgc = '08883265000197') then 
    apelido := 'coamo';    
    elsif (cgc = '01052203000194') then 
    apelido := 'caurj';
  elsif (cgc = '02906583000140') then 
    apelido := 'consaude';   
  elsif (cgc = '02774736000142') then 
    apelido := 'garantia';   
  elsif (cgc = '02704835000158') then 
    apelido := 'genoveva';            
  elsif (cgc = '04101252000168') then   
    apelido := 'global';
  elsif (cgc = '78613841000323') then 
    apelido := 'hospitalar';   
  elsif (cgc = '03516381000154') then 
    apelido := 'idealSaude';
  elsif (cgc = '00637500000139') then 
    apelido := 'lifeSul';
  elsif (cgc = '02026403000135') then 
    apelido := 'multiSaude';
  elsif (cgc = '02862447000103') then 
    apelido := 'nossaSaude';
  elsif (cgc = '77790228000157') then 
    apelido := 'novaplam';
  elsif (cgc = '02881039000190') then 
    apelido := 'plamheg';
  elsif (cgc = '37035441000139') then 
    apelido := 'planmed';
  elsif (cgc = '14349740000142') then 
    apelido := 'plansul';       
  elsif (cgc = '02896924000143') then 
    apelido := 'poliSaude';
  elsif (cgc = '89890172000191') then 
    apelido := 'portoAlegreClinicas';
  elsif (cgc = '00558356000145') then 
    apelido := 'promedmg';
  elsif (cgc = '04467112000108') then 
    apelido := 'rnMetropolitan';
  elsif (cgc = '01273549000112') then 
    apelido := 'samedh';
  elsif (cgc = '02403281000159') then 
    apelido := 'sampes';
  elsif (cgc = '02562406000193') then
    apelido := 'sampmg';
  elsif (cgc = '22669931000110') then 
    apelido := 'santaCasaMontesClaros';
  elsif (cgc = '45098787000104') then 
    apelido := 'santaEdwirgesPlano';
  elsif (cgc = '95642179000197') then 
    apelido := 'santaRita';   
  elsif (cgc = '25104902000107') then
    apelido := 'santaRosalia';
  elsif (cgc = '02298080000139') then 
    apelido := 'saoLucas';
  elsif (cgc = '02918461000173') then 
    apelido := 'saudemed';
  elsif (cgc = '81564346000114') then 
    apelido := 'sempreVida';
  elsif (cgc = '85204279000188') then 
    apelido := 'servmed';
  elsif (cgc = '78311800000110') then 
    apelido := 'sinamed';
  elsif (cgc = '40336323000175') then 
    apelido := 'sinpacel';
  elsif (cgc = '37135365000133') then 
    apelido := 'smile';
  elsif (cgc = '04083773000130') then 
    apelido := 'unihosp';
  elsif (cgc = '45309606000141') then 
    apelido := 'unimedFranca';
  elsif (cgc = '37652765000116') then 
    apelido := 'unimedJatai';    
  elsif (cgc = '25064148000110') then 
    apelido := 'unimedAraguaina';        
  elsif (cgc = '62550256000120') then 
    apelido := 'universal';
  elsif (cgc = '01045690000168') then 
    apelido := 'vitallis';
  elsif (cgc = '76882612000117') then 
    apelido := 'clinipam';
  elsif (cgc = '72281561000117') then 
    apelido := 'saudeescolar';
  elsif (cgc = '79447603000196') then 
    apelido := 'odontopam';
  elsif (cgc = '04197511000104') then 
    apelido := 'cassind';    
  elsif (cgc = '01432102000149') then 
    apelido := 'camboriuSaude'; 
  elsif (cgc = '04574626000162') then    
    apelido := 'unisaudeMS';
  elsif (cgc = '04204285000133') then    
    apelido := 'casf';
  elsif (cgc = '00697509000135') then    
    apelido := 'unimedJiParana';
  elsif (cgc = '37436920000167') then    
    apelido := 'unimedBarra';
  elsif (cgc = '10311779000110') then
    apelido := 'YesOdonto'; 
  elsif (cgc = '53678264000165') then
    apelido := 'UnimedLestePaulista';
  elsif (cgc = '03098226000165') then
	 apelido := 'hgu';
  elsif (cgc = '73717639000166') then
   apelido := 'fatimaSaude';
  end if;
  
  update finempr set capelempr = apelido
   where capelempr is null;
  commit;   
end;
/

-- usado somente na insere_procedimento_aso
create or replace procedure insere_procedimento_web30 (guia         in     number,
                                                     procedimento in     varchar2,
                                                     quantidade   in     number,
                                                     dente        in     varchar2,
                                                     face         in     varchar2,
                                                     retorno         out number) as

valor_tabela      number;
valor_copart      number;
valor_coparthon   number;
valor_copartco    number;
valor_copartfilme number;
valor_adm         number;
valor_usuario     number;
contraste         number;
id_limite         number;
decisao           number;
cobranca          varchar2(1);
CopartPrestador   number;
id_dente          number;
id_face           number;
temp              number;
regraglosa        number;
local             number;
regra_autorizacao varchar2(1);
usuario           number;
prestador         number;
especialidade     number;
cobr_copart       varchar2(1);
cobertura         number;
natureza          varchar2(1);
regime            varchar2(1);
Idade             number;
franquia_valor    number;
franquia          number;

cursor cr_TemNaGuia is 
  select nnumeguia
    from hsspgui
   where nnumeguia = guia
     and ccodipmed = procedimento;

cursor cr_local is
  select nlocapres,nnumepres,cpautconf,nutilusua,nnumeespec,curgeguia,ctipoguia
    from hssguia,hssconf
   where nnumeguia = guia;
   
cursor cr_usuario is
select trunc(months_between(sysdate,dnascusua)/12)
  from hssusua
 where nnumeusua = usuario;

begin
  retorno := 0;
  
  open cr_local;
  fetch cr_local into local,prestador,regra_autorizacao,usuario,especialidade,regime,natureza;
  close cr_local;

  processa_guia (guia,procedimento,quantidade,valor_tabela,valor_copart,valor_adm,valor_usuario,
                 contraste,id_limite,decisao,CopartPrestador,valor_coparthon,valor_copartco,valor_copartfilme);

  regraglosa := RegrasGlosasGuias(guia,procedimento,local);

  if regraglosa > 0 then
    retorno := -5;
  else
    if nvl(id_limite,0) = 0 then
      id_limite := null;
    end if;

    if decisao = 0 then
      cobranca := null;
    elsif decisao = 1 then
      cobranca := 'C';
    elsif decisao = 2 then
      cobranca := 'F';
    elsif decisao = 3 then
      cobranca := 'M';
    elsif decisao = 5 then
      cobranca := 'B';
    elsif decisao = 6 then
      cobranca := 'I';
    elsif decisao = 9 then
      update hssguia set cstatguia = 'A' where nnumeguia = guia; -- estouro de limite com envio para auditoria
      retorno := 0;
    end if;

    if decisao = 4 then  -- Estourou limite
      insert into hsspgui (nnumeguia,ccodipmed,nquanpgui,nqindpgui,nfranpgui,nusuapgui,ntadmpgui,nnumelimi,
                       ccobrpgui,ncoprpgui,nnumedente,nnumeface,nnumecober,nhonopgui,ncuoppgui,nvfilpgui)
               values (guia,procedimento,quantidade,valor_tabela,valor_copart,valor_usuario,valor_adm,NULL,
                       cobranca,CopartPrestador,id_dente,id_face,cobertura,valor_coparthon,valor_copartco,valor_copartfilme);
      retorno := -4;
    else
      id_dente := RetornaIdDente30(dente);
      id_face := RetornaIdFace30(face);

      open cr_usuario;
      fetch cr_usuario into idade;
      close cr_usuario;      

      if nvl(regra_autorizacao,'P') = 'L' then
        cobertura := cobertura_usuario (usuario,procedimento,natureza||regime,local,local,cobr_copart,sysdate,especialidade,null,null,idade);
      else
        cobertura := cobertura_usuario (usuario,procedimento,natureza||regime,prestador,local,cobr_copart,sysdate,especialidade,null,null,idade);
      end if;
      
      if cobertura = 0 then 
        cobertura := cobertura_aditivo (usuario,procedimento,cobr_copart,franquia_valor,franquia);
      end if;         
      
      if cobertura = -1 then
        cobertura := null;
      elsif cobertura = 0 then
        cobertura := null;
        retorno := 4;
      end if;

      if id_dente > 0 and id_face > 0 then
 
        open cr_TemNaGuia;
        fetch cr_TemNaGuia into temp;
        if cr_TemNaGuia%notfound then
          insert into hsspgui (nnumeguia,ccodipmed,nquanpgui,nqindpgui,nfranpgui,nusuapgui,ntadmpgui,nnumelimi,
                               ccobrpgui,ncoprpgui,nnumedente,nnumeface,nnumecober,nhonopgui,ncuoppgui,nvfilpgui)
                       values (guia,procedimento,quantidade,valor_tabela,valor_copart,valor_usuario,valor_adm,id_limite,
                               cobranca,CopartPrestador,id_dente,id_face,cobertura,valor_coparthon,valor_copartco,valor_copartfilme);
        else
          update hsspgui 
              set nquanpgui = nquanpgui + 1, 
                  nqindpgui = nqindpgui + valor_tabela, 
                  nfranpgui = nfranpgui + valor_copart,
                  nhonopgui = nhonopgui + valor_coparthon,
                  ncuoppgui = ncuoppgui + valor_copartco,
                  nvfilpgui = nvfilpgui + valor_copartfilme
            where nnumeguia = guia
              and ccodipmed = procedimento;            
        end if;

        insert into hssdpgui (nnumeguia,ccodipmed,nnumedente,nnumeface,drealdpgui)
                      values (guia,procedimento,id_dente,id_face,sysdate);

        close cr_TemNaGuia;

      else
        insert into hsspgui (nnumeguia,ccodipmed,nquanpgui,nqindpgui,nfranpgui,nusuapgui,ntadmpgui,nnumelimi,
                             ccobrpgui,ncoprpgui,nnumedente,nnumeface,nnumecober,nhonopgui,ncuoppgui,nvfilpgui)
                     values (guia,procedimento,quantidade,valor_tabela,valor_copart,valor_usuario,valor_adm,id_limite,
                             cobranca,CopartPrestador,id_dente,id_face,cobertura,valor_coparthon,valor_copartco,valor_copartfilme);
      end if;
    end if;
  end if;
end;
/


create or replace function RetornaIdAcomIntercambio30 (codigo        in varchar2,
                                                     contrato      in number ) return number as

acomodacao    varchar2(1);
idacomodacao  number;
cursor cr_plano (acomodacao in varchar2)  is
  select hssacom.nnumeacom from hsstitu,hssplan,hssacpla,hssacom
   where hsstitu.nnumetitu = contrato     
     and hsstitu.nnumeplan = hssplan.nnumeplan       
   and hssplan.nnumeplan = hssacpla.nnumeplan
   and hssacpla.nnumeacom = hssacom.nnumeacom
   and cptu_acom = acomodacao;    

begin
  if (codigo = '014') or (codigo = '012') or (codigo = '013') or
     (codigo = '018') or (codigo = '202') or (codigo = '205') or
     (codigo = '206') or (codigo = '208') or (codigo = '021') or
     (codigo = '024') or (codigo = '025') or (codigo = '027') or
     (codigo = '029') or (codigo = '212') or (codigo = '215') or
     (codigo = '216') or (codigo = '218') or (codigo = '219') or
     (codigo = '221') or (codigo = '032') or (codigo = '045') or
     (codigo = '046') or (codigo = '047') or (codigo = '048') or
     (codigo = '052') or (codigo = '053') or (codigo = '054') or
     (codigo = '055') or (codigo = '061') or (codigo = '062') or
     (codigo = '063') or (codigo = '064') or (codigo = '065') or
     (codigo = '066') or (codigo = '067') or (codigo = '068') or
     (codigo = '074') or (codigo = '075') or (codigo = '076') or
     (codigo = '077') or (codigo = '081') or (codigo = '082') or
     (codigo = '083') or (codigo = '084') or (codigo = '087') or
     (codigo = '088') or (codigo = '089') or (codigo = '091') or
     (codigo = '094') or (codigo = '095') or (codigo = '096') or
     (codigo = '097') or (codigo = '301') or (codigo = '302') then
   
   Acomodacao := 'B';

  elsif (codigo = '011') or (codigo = '015') or (codigo = '016') or
        (codigo = '017') or (codigo = '201') or (codigo = '203') or
        (codigo = '204') or (codigo = '207') or (codigo = '019') or
        (codigo = '022') or (codigo = '023') or (codigo = '026') or
        (codigo = '211') or (codigo = '213') or (codigo = '214') or
        (codigo = '217') or (codigo = '031') or (codigo = '041') or
        (codigo = '042') or (codigo = '043') or (codigo = '044') or
        (codigo = '049') or (codigo = '051') or (codigo = '056') or
        (codigo = '057') or (codigo = '058') or (codigo = '059') or
        (codigo = '069') or (codigo = '071') or (codigo = '072') or
        (codigo = '073') or (codigo = '078') or (codigo = '079') or
        (codigo = '085') or (codigo = '086') or (codigo = '092') or
        (codigo = '093') or (codigo = '098') or (codigo = '300') then
    
    Acomodacao := 'A';
  
  else  
  Acomodacao := 'A';
  end if;      
  
  open cr_plano(Acomodacao);
  fetch cr_plano into idacomodacao;
  if cr_plano%found then
    return idacomodacao;
  end if;  
  
  return idacomodacao;
end;
/


create or replace function CodigoTabelaPrestadorWeb30 (pres   in number,
                                             procedimento in varchar2) return varchar2 as

temp varchar2(10);

cursor cr_codigoTabela is
  select hsstabe.ccoditabe
    from hsslinea,hsstabe,hssvpro
   where hsslinea.nnumepres = pres
     and hssvpro.ccodipmed = procedimento
     and hsslinea.nnumetabe = hsstabe.nnumetabe
     and hsstabe.nnumetabe = hssvpro.nnumetabe
   order by hsslinea.dvigelinea desc;

begin
  open cr_codigoTabela;
  fetch cr_codigoTabela into temp;
  if cr_codigoTabela%found then
    return temp;
  else
    return 'N';
  end if;
  close cr_codigoTabela;
end;
/


create or replace function procedimento_proc_audit (procedimento in varchar2) return varchar2 is
Processo       varchar2(1);
Procedi        varchar2(9);

cursor cr_qtde is
  select ccodipmed from hsspproa
   where ccodipmed = procedimento;

begin

  open cr_qtde;
  fetch cr_qtde into Procedi;
  if cr_qtde%found then 
    Processo := 'S';
  else
    Processo := 'N';
  end if;
  return processo;
end;
/

create or replace procedure insereTextoNoticia( idNoticia in number, texto in varchar2 ) as
l_clob clob;
begin
  delete from hsstxnot where nnumenotic = idnoticia;
  
  insert into HSSTXNOT (NNUMENOTIC,CTEXTTXNOT) values ( idNoticia, empty_clob() ) returning CTEXTTXNOT into l_clob;
  dbms_lob.write( l_clob, length(texto), 1, texto );
end;
/

