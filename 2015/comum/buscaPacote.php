<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
     
  if (isset($_POST['codigo'])) {
        
    if ($_POST['codigo'] <> "") {
      $sql = new Query($bd);
      $txt = "SELECT CDESCPACOT FROM HSSPACOT,HSSPCPRE
               WHERE CCODIPACOT = :codigo 
                 AND HSSPACOT.NNUMEPACOT = HSSPCPRE.NNUMEPACOT
                 AND HSSPCPRE.NNUMEPRES = :prestador ";

      $sql->clear();
      $sql->addParam(":codigo",$_POST['codigo']);
      
      if ($_POST['prestador'] > 0 ) {
        $prestador = $_POST['prestador'];
      }
      
      if (($_SESSION['regra_autorizacao'] == 'L') or 
          ($_SESSION['apelido_operadora'] == "unihosp") or
          ($_SESSION['apelido_operadora'] == "affego")){
        $prestador = $_SESSION['id_contratado'];
      }
      
      $sql->addParam(":prestador",$prestador);
      
      if ($_POST['id_pacote']>0){
        $txt .= " AND HSSPACOT.NNUMEPACOT = :id_pacote";
        $sql->addParam(":id_pacote",$_POST['id_pacote']);
      }

      $sql->executeQuery($txt);
      
      if ($sql->count() > 0)
        $json['nome']           = utf8_encode($sql->result("CDESCPACOT"));
      else
        $json['nome']           = utf8_encode('Pacote n�o localizado!');      
    }
  }
  
  echo json_encode($json);
  
  $bd->close();
  
?>