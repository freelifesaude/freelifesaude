<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
     
  if (isset($_POST['codigo'])) {
        
    if ($_POST['codigo'] <> "") {
      $sql = new Query($bd);
      $txt = "SELECT NVL(CNPCMPMED,CNOMEPMED) CNOMEPMED,REPLACE(REPLACE(COBAUPMED,CHR(13),'<BR>'),CHR(10),'') COBAUPMED,
                     REPLACE(REPLACE(CMENSPMED,CHR(13),'<BR>'),CHR(10),'') CMENSPMED,'TESTE' CAUDIPMED      
                FROM HSSPMED
               WHERE CCODIPMED = :codigo 
                 AND CSITUPMED = 'A'";      

      $sql->clear();
      $sql->addParam(":codigo",$_POST['codigo']);
      $sql->executeQuery($txt);
      
      if ((isset($_POST['usuario'])) and ($_SESSION['apelido_operadora'] == "unimedLimeira")) {
        $sql2 = new Query($bd);
        $txt2 = " SELECT TO_CHAR(PEDIDO.DEMISGUIA,'DD/MM/YYYY') DATA_SOLICITACAO, TO_CHAR(HSSGUIA.DEMISGUIA,'DD/MM/YYYY') DATA_REALIZACAO  
                    FROM HSSUSUA, HSSGUIA, HSSPGUI, HSSGUIA PEDIDO
                   WHERE NVL(HSSGUIA.NPEDIGUIA,HSSGUIA.NNUMEGUIA) = PEDIDO.NNUMEGUIA
                     AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
                     AND CCODIUSUA = :usuario   
                     AND CCODIPMED = :codigo                    
                     AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                     AND PEDIDO.DEMISGUIA - 365 <= SYSDATE 
                   ORDER BY TO_DATE(PEDIDO.DEMISGUIA,'DD/MM/YYYY') DESC";      
  
        $sql2->clear();
        $sql2->addParam(":usuario",$_POST['usuario']);
        $sql2->addParam(":codigo",$_POST['codigo']);
        $sql2->executeQuery($txt2);        
        
      }  
            
      if ($sql->count() > 0) {
        $json['nome']           = utf8_encode($sql->result("CNOMEPMED"));
        $json['auditoria']      = utf8_encode($sql->result("COBAUPMED"));
        $json['observacao']     = utf8_encode($sql->result("CMENSPMED"));
        $json['procAuditoria']  = utf8_encode($sql->result("CAUDIPMED"));
        if ($_SESSION['apelido_operadora'] == "unimedLimeira") {
          if ($sql2->count() > 0) 
            $json['autRecente']   = utf8_encode("Data solicita��o: ".$sql2->result("DATA_SOLICITACAO")."   Data de Execu��o: ".$sql2->result("DATA_REALIZACAO"));
          else
            $json['autRecente']   = '';
        }
        else 
          $json['autRecente']   = '';
      } else {
        $json['nome']           = utf8_encode('Procedimento n�o localizado!');
        $json['auditoria']      = '';
        $json['observacao']     = '';        
        $json['procAuditoria']  = '';
        $json['autRecente']     = '';
      }   
    }
  }
        
  echo json_encode($json);
  
 $bd->close();
  
?>