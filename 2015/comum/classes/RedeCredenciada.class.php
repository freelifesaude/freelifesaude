<?php

  /**
   * Classes armazenar os dados dos prestadores
   *
   * @author Augusto
   */

  include_once("classes/Prestador.class.php");

  class RedeCredenciada {
    /* Private */

    private $sql_principal;
    private $txtPrincipal;
    private $bd;


    /* Public */
    public $plano;
    public $nome_prestador;
    public $tipo_prestador;
    public $tipoRede;
    public $estado;
    public $cidade;
    public $bairro;
    public $graduacao;
    public $especialidade;
    public $area_prestador;
    public $indicavel;
    public $total_registros;
    public $pagina_inicial;
    public $registros_por_pagina;
    public $listarSubstitutos;

    /* Filtros */
    private $f_plano;
    private $f_nome;
    private $f_nome2;
    private $f_tipo;
    private $f_rede;
    private $f_estado;
    private $f_cidade;
    private $f_bairro;
    private $f_graduacao;
    private $f_especialidade;
    private $f_area;
    private $f_indicavel;
    private $f_substituto;

    public $grupo01 = array();
    public $grupo02 = array();

    public function __construct() {
      $this->listarSubstitutos    = 0;
      $this->pagina_inicial       = 0;
      $this->registros_por_pagina = 100000;
      $this->bd                   = new Oracle();
      $this->sql_principal        = new Query($this->bd);
    }

    function __call($func, $arq) {
      if (isset($arq)) {

      }
      if (!method_exists(get_class($this), $func)) {
        throw new Exception(" O metodo \"$func\" nao existe");
      }
    }

    function __destroy() {
      $this->bd->close();
    }

    private function preencheParametros() {

      $this->f_credenciado   = '';
      $this->f_plano         = '';
      $this->f_nome          = '';
      $this->f_tipo          = '';
      $this->f_redea         = '';
      $this->f_estado        = '';
      $this->f_cidade        = '';
      $this->f_bairro        = '';
      $this->f_graduacao     = '';
      $this->f_especialidade = '';
      $this->f_area          = '';
      $this->f_indicavel     = '';
      $this->f_rede          = '';
      $this->f_substituto    = '';

      $this->tem_rede        = $this->verificaTemRede();

      if ($_SESSION['apelido_operadora'] == 'affego')
        $this->f_credenciado = "   AND (FINPRES.CCREDPRES IN ('S','C') OR (FINPRES.DCANCPRES >= (sysdate - 180) )) ";
      else if ($_SESSION['apelido_operadora'] == 'saudemed')
        $this->f_credenciado = "   AND (FINPRES.CCREDPRES IN ('S','O') OR ((FINPRES.DCANCPRES >= (sysdate - 180) AND FINPRES.NNUMECLAPR <> 2335668 ))) ";
      else
        $this->f_credenciado = "   AND (FINPRES.CCREDPRES IN ('S','O') OR (FINPRES.DCANCPRES >= (sysdate - 180) )) ";

      if (($this->plano > 0) and ($this->tem_rede == 'plano')) {
        $this->f_plano = "   AND FINPRES.NNUMEPRES IN (SELECT HSSPREDE.NNUMEPRES
                                                         FROM HSSPLAN, HSSREDPL,HSSPREDE,FINPRES
                                                        WHERE HSSPLAN.NNUMEPLAN = :id_plano
                                                          AND HSSPLAN.NNUMEPLAN = HSSREDPL.NNUMEPLAN
                                                          AND HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA
                                                          AND HSSPREDE.CPWEBPREDE = 'S'
                                                          AND HSSPREDE.NNUMEPRES = FINPRES.NNUMEPRES)";
        $this->sql_principal->addParam(":id_plano", $this->plano);
      }

      if (($this->nome_prestador <> '') and ($this->listarSubstitutos == 0)) {
        $this->f_nome  = '   AND (FINPRES.CNOMEPRES LIKE UPPER(:nome) OR FINPRES.CFANTPRES LIKE UPPER(:nome))';
        $this->sql_principal->addParam(":nome", $this->nome_prestador."%");
      }

      if ($this->tipo_prestador <> '') {
        $this->f_tipo = "   AND FINPRES.CGRUPPRES = :tipo ";
        $this->sql_principal->addParam(":tipo", $this->tipo_prestador);
      }
      else if ($_SESSION['apelido_operadora'] == 'universal')
        $this->f_tipo = "   AND FINPRES.CGRUPPRES IN ('C','A','E','H','L','M','P','N') ";
      else if ($_SESSION['apelido_operadora'] == 'vitallis')
        $this->f_tipo = "   AND FINPRES.CGRUPPRES IN ('C','A','H','X','1','2','3','S','L','M','J','Q','T','U','8') ";

      if ($this->tipoRede > 0) {
        $this->f_rede = "   AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSPREDE WHERE HSSPREDE.NNUMEREDEA = :rede )";
        $this->sql_principal->addParam(":rede", $this->tipoRede);
      }

      if ($this->estado <> '') {
        $this->f_estado = "   AND FINPRES.CESTAPRES = :estado ";
        $this->sql_principal->addParam(":estado", $this->estado);
      }

      if ($this->cidade <> '') {
        $this->f_cidade = "   AND FINPRES.CCIDAPRES = :cidade ";
        $this->sql_principal->addParam(":cidade", $this->cidade);
      }

      if ($this->bairro <> '') {
        $this->f_bairro = "   AND FINPRES.CBAIRPRES = :bairro ";
        $this->sql_principal->addParam(":bairro", $this->bairro);
      }

      if ($this->graduacao <> '') {
        $this->f_graduacao = "   AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSPGRAD WHERE HSSPGRAD.CDESCPGRAD = :graduacao) ";
        $this->sql_principal->addParam(":graduacao", $this->graduacao);
      }

      if ($this->especialidade <> '') {
        $this->f_especialidade = "   AND HSSESPEC.NNUMEESPEC = :especialidade ";
        $this->sql_principal->addParam(":especialidade", $this->especialidade);
      }

      if ($this->area_prestador <> '') {
        $this->f_area = "   AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSAAPRE WHERE NNUMEAATUA = :area) ";
        $this->sql_principal->addParam(":area", $this->area_prestador);
      }

      if ($this->indicavel <> '') {
        $this->f_indicavel = "   AND FINPRES.CINDIPRES = :indicavel ";
        $this->sql_principal->addParam(":indicavel", $this->indicavel);
      }

      if ($this->tem_rede == "usuario") {
        $this->f_rede = "   AND FINPRES.NNUMEPRES IN (SELECT HSSPREDE.NNUMEPRES
                                                        FROM HSSREDCO,HSSPREDE
                                                       WHERE HSSREDCO.NNUMEUSUA = :usuario
                                                         AND HSSPREDE.CPWEBPREDE <> 'N'
                                                         AND HSSREDCO.NNUMEREDEA = HSSPREDE.NNUMEREDEA) ";
        $this->sql_principal->addParam(":usuario", $_SESSION['id_titular']);
      }
      else if ($this->tem_rede == "contrato") {
        $this->f_rede = "   AND NNUMEPRES IN (SELECT HSSPREDE.NNUMEPRES
                                                FROM HSSREDCO,HSSPREDE
                                               WHERE HSSREDCO.NNUMETITU = :contrato
                                                 AND HSSPREDE.CPWEBPREDE != 'N'
                                                 AND HSSREDCO.NNUMEREDEA = HSSPREDE.NNUMEREDEA) ";
        $this->sql_principal->addParam(":contrato", $_SESSION['id_contrato']);
      }
      else if ($this->tem_rede == "usuario_plano") {
        $this->f_rede = "   AND FINPRES.NNUMEPRES IN (SELECT HSSPREDE.NNUMEPRES
                                                        FROM HSSUSUA,HSSREDPL,HSSPREDE
                                                       WHERE HSSUSUA.NNUMEUSUA = :usuario
                                                         AND HSSPREDE.CPWEBPREDE <> 'N'
                                                         AND HSSUSUA.NNUMEPLAN = HSSREDPL.NNUMEPLAN
                                                         AND HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA) ";
        $this->sql_principal->addParam(":usuario", $_SESSION['id_titular']);
      }
      else if ($this->tem_rede == "contrato_plano") {
        $this->f_rede = "   AND FINPRES.NNUMEPRES IN (SELECT HSSPREDE.NNUMEPRES
                                                        FROM HSSTITU,HSSREDPL,HSSPREDE
                                                       WHERE HSSTITU.NNUMETITU = :contrato
                                                         AND HSSPREDE.CPWEBPREDE != 'N'
                                                         AND HSSTITU.NNUMEPLAN = HSSREDPL.NNUMEPLAN
                                                         AND HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA) ";
        $this->sql_principal->addParam(":contrato", $_SESSION['id_contrato']);
      }

      if (($this->tem_rede == "plano") and ( $this->plano > 0)) {
        $sql->addParam(":id_plano", $this->plano);
      }

      if ($this->listarSubstitutos > 0) {
        $this->f_substituto = "   AND FINPRES.NNUMEPRES IN (SELECT NSUBSPRES FROM HSSSUBPR WHERE NNUMEPRES = :substituto) ";
        $this->sql_principal->addParam(':substituto', $this->listarSubstitutos);
      }
    }

    public function verificaTemRede() {

      $sql = new Query($this->bd);
      $txt = "SELECT NNUMEREDEA FROM HSSREDEA WHERE ROWNUM = 1 ";
      $sql->executeQuery($txt);

      if ($sql->count() > 0) {

        $sql->clear();
        $txt = "SELECT HSSUSUA.NNUMEPLAN
                  FROM HSSREDCO, HSSUSUA
                 WHERE HSSREDCO.NNUMEUSUA = :usuario
                   AND HSSREDCO.NNUMEUSUA = HSSUSUA.NNUMEUSUA ";

        if (isset($_SESSION['id_titular']))
          $sql->addParam(":usuario", $_SESSION['id_titular']);
        else
          $sql->addParam(":usuario", 0);

        $sql->executeQuery($txt);

        if ($sql->count() > 0)
          $this->tem_rede = "usuario";
        else {
          $sql->clear();
          $txt = "SELECT HSSTITU.NNUMEPLAN
                    FROM HSSREDCO, HSSTITU
                   WHERE HSSREDCO.NNUMETITU = :contrato
                     AND NVL(HSSREDCO.NNUMESETOR, -1) = :locacao
                     AND HSSREDCO.NNUMETITU = HSSTITU.NNUMETITU ";
          $sql->addParam(":contrato", $_SESSION['id_contrato']);
          $sql->addParam(":locacao", $_SESSION['id_locacao']);
          $sql->executeQuery($txt);

          if ($sql->count() > 0)
            $tem_rede = "contrato";
          else {
            $sql->clear();
            $txt = "SELECT HSSREDPL.NNUMEPLAN
                      FROM HSSUSUA, HSSREDPL
                     WHERE HSSUSUA.NNUMEUSUA = :usuario
                       AND HSSUSUA.NNUMEPLAN = HSSREDPL.NNUMEPLAN";

            if (isset($_SESSION['id_titular']))
              $sql->addParam(":usuario", $_SESSION['id_titular']);
            else
              $sql->addParam(":usuario", 0);

            $sql->executeQuery($txt);

            if ($sql->count() > 0)
              $tem_rede = "usuario_plano";
            else {
              $sql->clear();
              $txt = "SELECT HSSTITU.NNUMEPLAN FROM HSSTITU, HSSREDPL
                       WHERE HSSTITU.NNUMETITU = :contrato
                         AND HSSTITU.NNUMEPLAN = HSSREDPL.NNUMEPLAN";

              $sql->addParam(":contrato", $_SESSION['id_contrato']);
              $sql->executeQuery($txt);

              if ($sql->count() > 0)
                $tem_rede = "contrato_plano";
              else {
                $sql->clear();
                $txt = "SELECT DISTINCT HSSREDPL.NNUMEPLAN
                          FROM HSSREDPL, HSSPREDE
                         WHERE HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA";
                $sql->executeQuery($txt);

                if ($sql->count() > 0)
                  $tem_rede = "plano";
              }
            }
          }
        }
      } else
        $tem_rede = "sem restricao";

      return $tem_rede;
    }

    private function processaDadosEspecialidade() {

      $this->preencheParametros();

      $this->txtPrincipal = "SELECT DISTINCT HSSESPEC.CNOMEESPEC,'P' TIPO,FINPRES.NNUMEPRES,HSSESPEC.NNUMEESPEC
                               FROM FINPRES,HSSESPRE,HSSESPEC,HSSEPRES
                              WHERE FINPRES.NNUMEPRES > 0".
                            $this->f_credenciado.
                            $this->f_plano.
                            $this->f_nome.
                            $this->f_tipo.
                            $this->f_redea.
                            $this->f_estado.
                            $this->f_cidade.
                            $this->f_bairro.
                            $this->f_graduacao.
                            $this->f_especialidade.
                            $this->f_area.
                            $this->f_indicavel.
                            $this->f_rede.
                            $this->f_substituto.
                            "   AND FINPRES.NNUMEPRES = HSSESPRE.NNUMEPRES(+)
                                AND HSSESPRE.NNUMEEPRES IS NULL
                                AND HSSESPRE.CPUBLESPRE = 'S'
                                AND HSSESPRE.NNUMEESPEC = HSSESPEC.NNUMEESPEC(+)
                                AND FINPRES.NNUMEPRES = HSSEPRES.NNUMEPRES(+)
                              UNION ALL
                             SELECT HSSESPEC.CNOMEESPEC,'C' TIPO,HOSP.NNUMEPRES,HSSESPEC.NNUMEESPEC
                               FROM HSSCCLIN,FINPRES,FINPRES HOSP,HSSESPEC
                              WHERE HSSCCLIN.NHOSPPRES = HOSP.NNUMEPRES
                                AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                                AND HOSP.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                                        WHERE NNUMEPRES = HOSP.NNUMEPRES
                                                          AND CPUBLESPRE = 'S'
                                                          AND NNUMEESPEC = HSSCCLIN.NNUMEESPEC
                                                          AND NNUMEEPRES IS NULL)
                                AND HSSCCLIN.CPUBLCCLIN='S'".
                            $this->f_credenciado.
                            $this->f_plano.
                            $this->f_nome.
                            $this->f_tipo.
                            $this->f_redea.
                            $this->f_estado.
                            $this->f_cidade.
                            $this->f_bairro.
                            $this->f_graduacao.
                            $this->f_especialidade.
                            $this->f_area.
                            $this->f_indicavel.
                            $this->f_rede.
                            $this->f_substituto.
                            "   AND HSSCCLIN.NNUMEESPEC = HSSESPEC.NNUMEESPEC(+)
                             ORDER BY 1,2 DESC";

      $txt = "SELECT COUNT(*) QTDE FROM (".
             $this->txtPrincipal.
             ")";
      $this->sql_principal->executeQuery($txt);

      $this->total_registros = $this->sql_principal->result("QTDE");

      $txt = "SELECT * FROM (
             SELECT ROWNUM REGISTRO, TABELA.* FROM ( ".
             $this->txtPrincipal.
             ") TABELA )
              WHERE REGISTRO BETWEEN :inicio + 1 AND :inicio + :qtde";
      $this->sql_principal->addParam(":inicio",$this->pagina_inicial * $this->registros_por_pagina);
      $this->sql_principal->addParam(":qtde"  ,$this->registros_por_pagina);
      $this->sql_principal->executeQuery($txt);
    }

    private function porEspecialidade() {

      $this->processaDadosEspecialidade();

      $nome_especialidade    = '';
      $this->grupo02         = array();

      while (!$this->sql_principal->eof()) {

        if ($this->sql_principal->result("CNOMEESPEC") <> $nome_especialidade) {
          $grupo = new Grupo();
          $grupo->nome = $this->sql_principal->result("CNOMEESPEC");
          $nome_especialidade = $this->sql_principal->result("CNOMEESPEC");
        }

        $prestador = new Prestador($this->sql_principal->result("NNUMEPRES"));
        $prestador->processaGraduacao($this->sql_principal->result("NNUMEESPEC"));
        $grupo->registros[] = $prestador;

        $this->sql_principal->next();

        if ($this->sql_principal->result("CNOMEESPEC") <> $nome_especialidade) {
          $this->grupo02[] = $grupo;
        }
      }
    }

    private function processaDadosPrestador() {

      $this->preencheParametros();

      $this->txtPrincipal = "SELECT FINPRES.CNOMEPRES,'P' TIPO,FINPRES.NNUMEPRES
                               FROM FINPRES
                              WHERE FINPRES.NNUMEPRES > 0".
                            $this->f_credenciado.
                            $this->f_plano.
                            $this->f_nome.
                            $this->f_tipo.
                            $this->f_redea.
                            $this->f_estado.
                            $this->f_cidade.
                            $this->f_bairro.
                            $this->f_graduacao.
                            $this->f_especialidade.
                            $this->f_area.
                            $this->f_indicavel.
                            $this->f_rede.
                            $this->f_substituto.
                            "   AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                                           WHERE NNUMEPRES = FINPRES.NNUMEPRES
                                                             AND CPUBLESPRE = 'S'
                                                             AND NNUMEEPRES IS NULL)
                              UNION ALL
                             SELECT HOSP.CNOMEPRES,'C' TIPO,HOSP.NNUMEPRES
                               FROM HSSCCLIN,FINPRES,FINPRES HOSP
                              WHERE HSSCCLIN.NHOSPPRES = HOSP.NNUMEPRES
                                AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                                AND HOSP.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                                        WHERE NNUMEPRES = HOSP.NNUMEPRES
                                                          AND CPUBLESPRE = 'S'
                                                          AND NNUMEESPEC = HSSCCLIN.NNUMEESPEC
                                                          AND NNUMEEPRES IS NULL)
                                AND HSSCCLIN.CPUBLCCLIN='S'".
                            $this->f_credenciado.
                            $this->f_plano.
                            $this->f_nome.
                            $this->f_tipo.
                            $this->f_redea.
                            $this->f_estado.
                            $this->f_cidade.
                            $this->f_bairro.
                            $this->f_graduacao.
                            $this->f_especialidade.
                            $this->f_area.
                            $this->f_indicavel.
                            $this->f_rede.
                            $this->f_substituto.
                            "ORDER BY 1,2 DESC";

      $txt = "SELECT COUNT(*) QTDE FROM (".
             $this->txtPrincipal.
             ")";
      $this->sql_principal->executeQuery($txt);

      $this->total_registros = $this->sql_principal->result("QTDE");

      $txt = "SELECT * FROM (
             SELECT ROWNUM REGISTRO, TABELA.* FROM ( ".
             $this->txtPrincipal.
             ") TABELA )
              WHERE REGISTRO BETWEEN :inicio + 1 AND :inicio + :qtde";
      $this->sql_principal->addParam(":inicio",$this->pagina_inicial * $this->registros_por_pagina);
      $this->sql_principal->addParam(":qtde"  ,$this->registros_por_pagina);
      $this->sql_principal->executeQuery($txt);
    }

    private function porPrestador() {

      $this->processaDadosPrestador();

      $this->grupo02         = array();

      $grupo = new Grupo();
      $grupo->nome = '';

      while (!$this->sql_principal->eof()) {

        $prestador = new Prestador($this->sql_principal->result("NNUMEPRES"));
        $prestador->processaGraduacao($this->sql_principal->result("NNUMEESPEC"));
        $grupo->registros[] = $prestador;

        $this->sql_principal->next();
      }

      $this->grupo02[] = $grupo;
    }

    public function processaRedeCredenciada($paginado) {
      ini_set("max_execution_time", 120);

      if ($paginado === false) {
        $this->pagina_inicial       = 0;
        $this->registros_por_pagina = 100000;
      }

      if ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista')
        $this->porEspecialidade();
      else
        $this->porPrestador();
        
      ini_set("max_execution_time", 30);
    }
  }

  class Grupo {
    public $nome      = '';
    public $registros = array();
  }
?>