<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
    
  $bd = new Oracle();  
  
  $diretorio = $seg->antiInjection($_POST['diretorio']);
        
  //Raiz
  $sql = new Query($bd);
  $txt = "SELECT CDESCARQUI,NVL(NSUPEARQUI,0) NSUPEARQUI,NNUMEARQUI
            FROM HSSARQUI
           START WITH NNUMEARQUI = :diretorio
         CONNECT BY PRIOR NSUPEARQUI = NNUMEARQUI
           ORDER BY NSUPEARQUI";   
  $sql->addParam(":diretorio",$diretorio);           
  $sql->executeQuery($txt);
    
  $caminho = '';
  
  while (!$sql->eof()) {
    $caminho .= '&nbsp;<i class="fa fa-caret-right"></i>'.
                '&nbsp;<a href="#" onclick="abreDiretorio(\''.$sql->result("NNUMEARQUI").'\');">'.$sql->result("CDESCARQUI").'</a>';
    $sql->next();              
  }
           
  echo utf8_encode($caminho);
  
  $bd->close();
?>