<?php
 
  $f_periodo = "";
  $f_situacao = "";
  $quantidade = 0;
  
  // Filtro por periodo

  if (($guia == '') and ($senha == '')) {
      $f_periodo =	"   AND (HSSGUIA.DEMISGUIA >= TO_DATE(:datainicial,'DD/MM/YYYY') AND ".
                        "         HSSGUIA.DEMISGUIA < TO_DATE(:datafinal,'DD/MM/YYYY') + 1) ";
   
    $sql->addParam(":datainicial",$data_inicial);
    $sql->addParam(":datafinal",$data_final);    
  }
  else  {
    if ($guia == '')
      $guia = 0;

    $guia = $formata->somenteNumeros($guia);
    
    $f_periodo = "   AND (HSSGUIA.NNUMEGUIA = :guia OR HSSGUIA.CSENHGUIA = UPPER(:senha))";
    
    $sql->addParam(":guia",$guia);
    $sql->addParam(":senha",$senha);     
  }

  // Filtro por codigo do usu�rio
  $f_usuario = '';
  
  if ($codigo <> '') {
    $f_usuario = "   AND HSSUSUA.CCODIUSUA = :codigo ";
    $sql->addParam(":codigo",$codigo);     
  }

  // Filtro situa��o
  $f_situacao = "   AND HSSGUIA.CSTATGUIA IS NULL ";
    

  // Ordenacao  
  $f_ordenacao = " ORDER BY CNOMEUSUA,NNUMEGUIA,1";


  $txt_guias = "SELECT * FROM (
                SELECT A.*,ROWNUM REGISTRO FROM (
                SELECT 1 ORDEM,HSSGUIA.NNUMEGUIA,DEMISGUIA DEMISGUIA1,TO_CHAR(HSSGUIA.DEMISGUIA,'DD/MM/YY HH24:MI') DEMISGUIA, 
                       NVL(HSSUSUA.CCODIUSUA,HSSNUSUA.CCODINUSUA) CCODIUSUA, NVL(NVL(HSSUSUA.CNOMEUSUA,HSSNUSUA.CNOMENUSUA),CCORTGUIA) CNOMEUSUA, PRES.CNOMEPRES PRESTADOR, 
                       TOTAL_GUIA(HSSGUIA.NNUMEGUIA,'1') VALOR, NVL(TITULAR.CNOMEUSUA,TITULAR2.CNOMENUSUA) TITULAR,
                       HSSGUIA.CSTATGUIA, HSSGUIA.CATRAGUIA, HSSGUIA.CLOCAGUIA, 
                       TO_CHAR(NVL(DVALIGUIA,TRUNC(SYSDATE)),'DD/MM/YYYY') DVALIGUIA,TIPOPROCEDIMENTOGUIA30(HSSGUIA.NNUMEGUIA) TIPO,
                       SOLI.CNOMEPRES SOLICITANTE,LOCA.CNOMEPRES LOCAL,
                       DECODE(PRES.CPESSPRES,'F',PRES.CCPF_PRES,PRES.CCGC_PRES) CPF_CGC_PRES,
                       DECODE(SOLI.CPESSPRES,'F',SOLI.CCPF_PRES,SOLI.CCGC_PRES) CPF_CGC_SOLI,                       
                       DECODE(LOCA.CPESSPRES,'F',LOCA.CCPF_PRES,PRES.CCGC_PRES) CPF_CGC_LOCA,
                       HSSESPEC.CNOMEESPEC,RETORNA_NOME_REDE_ATENDIMENTO(REDE_ATENDIMENTO_USUARIO(HSSGUIA.NNUMEUSUA)) REDE,CURGEGUIA,
                       DECODE(CSTATGUIA,NULL,'Liberada','A','Sob auditoria','C','Aguard. confirma��o','N','Negada','P','Aguard. Autoriza��o') STATUS,
					   CDESCMOTLN,TO_CHAR(DEMISGUIA,'HH24:mi') HORA, DATA_APRESENTACAO_GUIA30(HSSGUIA.NNUMEGUIA) DDIGIGINT,NPTU_GUIA 
                  FROM HSSGUIA,HSSUSUA,HSSUSUA TITULAR,HSSNUSUA,HSSNUSUA TITULAR2,HSSTITU,FINPRES PRES,FINPRES SOLI,FINPRES LOCA,HSSESPEC,HSSMOTLN 
                  WHERE HSSUSUA.NNUMETITU = :contrato".
               $f_situacao.  
               $f_periodo.
               "   AND HSSGUIA.NNUMEUSUA  = HSSUSUA.NNUMEUSUA(+) 
				           AND HSSUSUA.NTITUUSUA  = TITULAR.NNUMEUSUA(+)
				           AND HSSGUIA.NNUMENUSUA = HSSNUSUA.NNUMENUSUA(+)
                   AND HSSNUSUA.NTITUNUSUA = TITULAR2.NNUMENUSUA(+) ".			   
               $f_usuario.
               "   AND HSSUSUA.NNUMETITU  = HSSTITU.NNUMETITU(+) 
                   AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
                   AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)                
                   AND HSSGUIA.NLOCAPRES = LOCA.NNUMEPRES(+)   
                   AND HSSGUIA.NNUMEESPEC = HSSESPEC.NNUMEESPEC(+)
				           AND HSSGUIA.NNUMEMOTLN = HSSMOTLN.NNUMEMOTLN(+) ".
               $f_ordenacao.			   
                ") A) ";
               
  if ($inicio >= 0) {
    $txt_guias .= " WHERE REGISTRO BETWEEN TO_NUMBER(:inicio) + 1 AND TO_NUMBER(:inicio) + 100";
    $sql->addParam(":inicio",$inicio); 
  }
  
  $sql->addParam(":contrato",$_SESSION['id_contrato']);  
  
  $txt_copart = "SELECT HSSPGUI.CCODIPMED, CNOMEPMED, NQUANPGUI, NQINDPGUI,
                        CCODIDENTE||' '||CDESCDENTE DENTE, CCODIFACE||' '||CDESCFACE FACE,CSTATPGUI
                   FROM HSSPGUI,HSSPMED,HSSDENTE,HSSFACE
                  WHERE HSSPGUI.NNUMEGUIA = :guia
                    AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                    AND HSSPGUI.NNUMEDENTE = HSSDENTE.NNUMEDENTE(+)
                    AND HSSPGUI.NNUMEFACE = HSSFACE.NNUMEFACE(+)
                    AND (HSSPGUI.CCODIPMED, HSSPGUI.NNUMEGUIA) NOT IN (SELECT CCODIPMED,NNUMEGUIA FROM HSSDPGUI
                                                                        WHERE NNUMEGUIA = HSSPGUI.NNUMEGUIA)
                  UNION ALL
                 SELECT HSSPGUI.CCODIPMED, CNOMEPMED, DECODE(HSSPGUI.NNUMEDENTE,NULL,NQUANPGUI,DECODE(HSSDENTE.CCODIDENTE,'00',NQUANPGUI,1)) NQUANPGUI, DECODE(HSSPGUI.NNUMEDENTE,NULL,NQINDPGUI,DECODE(HSSDENTE.CCODIDENTE,'00',NQINDPGUI,(NQINDPGUI/NQUANPGUI))) NQINDPGUI,
                        CCODIDENTE||' '||CDESCDENTE DENTE, CCODIFACE||' '||CDESCFACE FACE,TO_CHAR(NULL)
                   FROM HSSPGUI,HSSDPGUI,HSSPMED,HSSDENTE,HSSFACE
                  WHERE HSSPGUI.NNUMEGUIA = :guia
                    AND HSSPGUI.NNUMEGUIA = HSSDPGUI.NNUMEGUIA
                    AND HSSPGUI.CCODIPMED = HSSDPGUI.CCODIPMED
                    AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                    AND HSSDPGUI.NNUMEDENTE = HSSDENTE.NNUMEDENTE
                    AND HSSDPGUI.NNUMEFACE = HSSFACE.NNUMEFACE
                  UNION ALL
                 SELECT CCODITAXA, CDESCTAXA, NQUANDGUI, NVALODGUI,
                        '' DENTE, '' FACE,TO_CHAR(NULL)
                   FROM HSSDGUI,HSSTAXA
                  WHERE HSSDGUI.NNUMEGUIA = :guia
                    AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                  UNION ALL
                 SELECT CCODIPRODU, CNOMEPRODU, NQORIITMOV, NPRECITMOV,
                        '' DENTE, '' FACE,TO_CHAR(NULL)
                   FROM HSSMGUI,ESTITMOV,ESTPRODU
                  WHERE HSSMGUI.NNUMEGUIA = :guia
                    AND HSSMGUI.NNUMEMOVIM = ESTITMOV.NNUMEMOVIM
                    AND ESTITMOV.NSOLIPRODU = ESTPRODU.NNUMEPRODU
                  UNION ALL
                 SELECT CCODIPACOT, CDESCPACOT,NQUANPCGUI,NVALOPCGUI,
                        '' DENTE, '' FACE,TO_CHAR(NULL)
                   FROM HSSPCGUI,HSSPACOT
                  WHERE HSSPCGUI.NNUMEGUIA = :guia
                    AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT ";  
  
?>                     