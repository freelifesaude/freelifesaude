/*****************************************************************************************/
/* Leitura biometrica                                                                    */
/*****************************************************************************************/
function capture() {
  var err;
  var result = false;
  
  // Check ID is not NULL
  if ( document.getElementById('codigo').value == '' ) {
    alert('Por favor identifique o paciente primeiramente!');
    result = false;
  } else if ( document.getElementById('digitalconfere').value == 'S' ) {
    result = true;
  } else {
     
    try { // Exception handling
    
      DEVICE_AUTO_DETECT  = 255;
    
      var objNBioBSP = new ActiveXObject('NBioBSPCOM.NBioBSP.1');
      var objDevice = objNBioBSP.Device;
      var objExtraction = objNBioBSP.Extraction;
      var objMatching = objNBioBSP.Matching;

      // Open device. [AUTO_DETECT]
      // You must open device before capture.
      objDevice.Open(DEVICE_AUTO_DETECT);

      err = objDevice.ErrorCode;  // Get error code  
      if ( err != 0 ) {    // Device open failed
        alert('Device open failed !');
        result = false;      
      } else { 
        objExtraction.Capture();
        err = objExtraction.ErrorCode;  // Get error code
      
        if ( err != 0 ) {
          alert('Problemas na captura da digital! Erro n�mero : [' + err + ']');
          result = false;        
        } else {
          if (document.getElementById('digital').value == '') { 
              alert ('Beneficiario n�o possui digital cadastrada.');
              result = false;
          }else{
            objMatching.VerifyMatch(document.getElementById('digital').value,objExtraction.TextEncodeFIR);
            if (objMatching.MatchingResult == '1') {
              alert ('Digital confere.');
              document.getElementById('digitalconfere').value = 'S';
              document.getElementById('fotoDigital').style.border = "2px solid lime";
              document.getElementById('fotoDigital').style.width = "100px"; 
              document.getElementById('fotoDigital').style.height = "111px"; 
              document.getElementById('fotoDigital').style.marginRight  = "5px"; 
              result = true;
            } else {
              alert ('A digital n�o confere');
              document.getElementById('digitalconfere').value = 'N';
              result = false;
            }
          }
          objDevice.Close(DEVICE_AUTO_DETECT);
        }
      }
          
      objExtraction = 0;
      objDevice = 0;    
      objNBioBSP = 0;
      objMatching = 0;
    } catch(e) {
      alert('Erro' + e.message);
      result = false;
    }
  }
    
  return (result);
}