<?php
  
class Guia {
  private $bd;
  private $idGuia;  
  private $apelidoOperadora;  
  private $idAcomodacao;  
  private $diaVencimento;    
  private $procedimentos    = array();
  private $procedimentosAso = array();
  private $pacotes          = array();
  private $taxas            = array();
  private $matmeds          = array();
  private $OPME             = array();
  private $erros            = array();
  private $criticas         = array();
  private $errosFatal       = array();  
  private $anexos           = array();
  private $agentes          = array();
  private $aux              = array();
  
  public $senhaBeneficiario;
  public $nascimentoBeneficiario;
  public $codigoBeneficiario;
  public $modalidade;
  public $idCid;
  public $idCid2;
  public $idCid3;
  public $idCid4;
  public $digital;
  public $motivoDigital;
  public $idMotivoDigital;
  public $idBeneficiario;
  public $idSolicitante;
  public $idExecutante;
  public $idLocalAtendimento;
  public $regime;
  public $natureza;
  public $tipoAtendimento;
  public $tipoSaidaSadt;
  public $idEspecialidade;
  public $hipoteseDiagnostica;
  public $indicadorDeAcidente;
  public $data_internacao;
  public $hora_internacao;
  public $data_prev_alta;
  public $hora_prev_alta;
  public $leito;  
  public $tipoDeDoenca;
  public $tempoDaDoenca;
  public $unidadeDeTempoDaDoenca;
  public $status;
  public $localEmissao;
  public $tipoConsulta;
  public $regimeInternacao;
  public $idGuiaPrincipal;  
  public $idOperador;
  public $idGuiaPedido;
  public $tipoGuia;
  public $ptuGuia;
  public $autorizacaoOrigem;
  public $indicadorAcidente;
  public $tipoDoenca;
  public $tempoDoenca;
  public $unidadeTempoDoenca;
  public $observacoes;
  public $atendimentoRn;
  public $asoOuGuia;
  public $nascimentoFilho;
  public $nomeFilho;
  public $cortesia;
  
  public $peso;
  public $altura;
  public $superficie;
  public $nomeSoliOnco;
  public $telefoneSoliOnco;
  public $emailSoliOnco;
  public $dataDiagnostico;
  public $estadiamento;
  public $tipoQuimioterapia;
  public $finalidade;
  public $ecog;
  public $planoTerapeutico;
  public $diagnosticoCitoHisto;
  public $informRelevantes;
  public $numCiclos;
  public $cicloAtual;
  public $interCiclos;
  public $dataSoliOnco;
  public $cirurgia;
  public $dataRealizacao;
  public $areaIrradiada;
  public $dataAplicacao;
  public $diagnosticoImagem;
  public $numCampos;
  public $doseDia;
  public $doseTotal;
  public $numDias;
  public $previsao;
  public $observacao;
  public $justificativaTecnica;
  public $especificacaoMat;
  public $procedi_realizado;
  public $observacaoOPME; 

  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  }  
    
  public function __construct($bd) {
    $this->bd = $bd;
  }
  
  public function getIdGuia() {
    return $this->idGuia;
  }
    
  public function getStatusGuia($confirmacao = 'N') {
    $sql = new Query($this->bd);
    $txt = "SELECT CSTATGUIA FROM HSSGUIA
             WHERE NNUMEGUIA = :guia";
    $sql->addParam(":guia",$this->idGuia);
    $sql->executeQuery($txt);  
    $status = $sql->result("CSTATGUIA");

    if (($status == '') or ($status == 'P')) {
      $sql = new Query($this->bd);    
      $txt = "SELECT CUNIMPSAUD
                FROM HSSGUIA,HSSUSUA,HSSPLAN,HSSTITU,HSSPSAUD,HSSCONF
               WHERE HSSGUIA.NNUMEGUIA = :guia
                 AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                 AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                 AND CMODAPLAN = '2'
                 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                 AND HSSTITU.NNUMEPSAUD = HSSPSAUD.NNUMEPSAUD
                 AND CUNIMPSAUD IS NOT NULL
                 AND CUNIMCONF IS NOT NULL
                 AND NVL(CIAUTCONF,'N') = 'S'
                 AND :confirmacao = 'N'
                 AND CCOLOTITU = 'N'";
                 
      $sql->addParam(":guia",$this->idGuia);
      $sql->addParam(":confirmacao",$confirmacao);
      $sql->executeQuery($txt);  
      
      if ($sql->result("CUNIMPSAUD") <> '')
        $status = "29";      
    }        
    
    return $status;
  }
  
  public function setStatusGuia($value) {
    $sql = new Query($this->bd);
    $txt = "UPDATE HSSGUIA SET CSTATGUIA = :status
             WHERE NNUMEGUIA = :guia";
    $sql->addParam(":guia",$this->idGuia);
    $sql->addParam(":status",$value);
    $sql->executeSQL($txt);      
  }  
  
  public function adicionarCritica($motivo) {
    $sql = new Query($this->bd);      
    $txt = "INSERT INTO HSSCRITI (NNUMEGUIA,NNUMEUSUA,NMOTICRITI)
                          VALUES (:guia,:operador,:motivo)";

    $sql->addParam(":guia",$this->idGuia);
    
    if ($_SESSION['id_operador'] > 0)
      $sql->addParam(":operador",$_SESSION['id_operador']);
    else
      $sql->addParam(":operador",'');
    
    $sql->addParam(":motivo",$motivo);      
    $sql->executeSQL($txt);        
  }    
  
  public function adicionaProcedimento($codigo,$quantidade,$dente,$face) {
    array_push($this->procedimentos,array($codigo,$quantidade,$dente,$face));
  }    
  
  public function adicionaProcedimentoAso($codigo,$data) {
    array_push($this->procedimentosAso,array($codigo,$data));
  }  
    
  public function adicionaTaxa($codigo,$quantidade,$id) {
    array_push($this->taxas,array($codigo,$quantidade,$id));
  }  
  
  public function adicionaPacote($codigo,$quantidade,$id) {
    array_push($this->pacotes,array($codigo,$quantidade,$id));
  }  

  public function adicionaMatMed($codigo,$quantidade,$tabela,$previsao,$dose,$via,$frequencia) {
    array_push($this->matmeds,array($codigo,$quantidade,$tabela,$previsao,$dose,$via,$frequencia));
  }  

  public function adicionaOPME($codigo,$quantidade,$tabela,$precounitario) {
    array_push($this->OPME,array($codigo,$quantidade,$tabela,$precounitario));
  } 

  public function adicionaAgente($codigo,$quantidade) {
    array_push($this->agentes,array($codigo,$quantidade));
  } 
  
  public function adicionaAnexo($name,$temp,$size) {
    array_push($this->anexos,array($name,$temp,$size));
  }    
  
  public function adicionaCID($codigo,$num = 1) {
    $func = new Funcao();
    
    if ($num == 1) {
      $this->idCid = $func->validaCid($this->bd,$codigo);
    
      if ($this->idCid == 0) {
        $this->idCid = '';
        $this->adicionaErro($codigo.'- CID Principal inv�lido!');
      }
    }

    if ($num == 2) {
      $this->idCid2 = $func->validaCid($this->bd,$codigo);
    
      if ($this->idCid2 == 0) {
        $this->idCid2 = '';
        $this->adicionaErro($codigo.'- CID 2 inv�lido!');
      }
    }  

    if ($num == 3) {
      $this->idCid3 = $func->validaCid($this->bd,$codigo);
    
      if ($this->idCid3 == 0) {
        $this->idCid3 = '';
        $this->adicionaErro($codigo.'- CID 3 inv�lido!');
      }
    }  

    if ($num == 4) {
      $this->idCid4 = $func->validaCid($this->bd,$codigo);
    
      if ($this->idCid4 == 0) {
        $this->idCid4 = '';
        $this->adicionaErro($codigo.'- CID 4 inv�lido!');
      }
    }     
    
  }  
  
  public function adicionaCritica($descricao) {
    array_push($this->criticas,array("* ".$descricao));
  }
  
  public function adicionaErro($descricao) {
    array_push($this->erros,array("* ".$descricao));
  }   
  
  public function adicionaErroFatal($descricao) {
    array_push($this->errosFatal,array($descricao));
  }   
  
  public function qtdeProcedimentos() {
    return sizeOf($this->procedimentos);
  }
  
  public function getProcedimentos() {
    return $this->procedimentos;
  }  
  
  public function qtdeErros() {
    return sizeOf($this->erros);
  }  
  
  public function getErros() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->erros);$b++) {      
      $erro = array();
      $erro = $this->erros[$b];
      
      $desc_erros .= $erro[0]."<br>";      
    } 
    
    return $desc_erros;
  }  
  
  public function getCriticas() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->criticas);$b++) {      
      $erro = array();
      $erro = $this->criticas[$b];
      
      $desc_erros .= $erro[0]."<br>";      
    } 
    
    return $desc_erros;
  }  
  
  public function qtdeErrosFatal() {
    return sizeOf($this->errosFatal);
  } 

  public function getErrosFatal($quebra = '<br>') {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->errosFatal);$b++) {      
      $erro = array();
      $erro = $this->errosFatal[$b];
      
      $desc_erros .= $erro[0].$quebra;      
    } 
    
    return $desc_erros;
  }   
  
  public function teste2() {
    for ($p = 0;$p < sizeof($this->matmeds);$p++) {        
      $item      = array();
      $item      = $this->matmeds[$p];          
      $id_matmed = valida_matmed($this->bd,$item[0],$this->idLocalAtendimento,'',$item[2],'1');
      
      if ($id_matmed > 0) {   
        $this->matmeds[$p] = array($item[0],$item[1],$item[2],$id_matmed);
      
/*        $sql_proc = new Query($this->bd);
        $txt_proc = "INSERT INTO HSSMMGUI (NNUMEGUIA,nnumeprodu,NQUANMMGUI,NQSOLMMGUI,NVPROMMGUI,NVPAGMMGUI,
                                           NCOPAMMGUI,CFORNMMGUI,CCOBRMMGUI,CSTATMMGUI,CTIPOMMGUI,CMANUMMGUI) 
                                   VALUES (:idGuia,:idProduto,:qtde,:qtde,0,0,
                                           0,'N','X','L','M','A')";
                                   
        $sql_proc->addParam(":idGuia"      ,$this->idGuia);
        $sql_proc->addParam(":idProduto"   ,$id_matmed);
        $sql_proc->addParam(":qtde"        ,$item[1]);  
        $erro = $sql_proc->executeSQL($txt_proc);                      */
      } 
      else
        $this->adicionaErro($item[0]." - C�digo de material/medicamento n�o existe ou n�o autorizado.");            
    }   

    for ($b = 0;$b < sizeof($this->matmeds);$b++) {      
      $procedimento = array();
      $procedimento = $this->matmeds[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]." = ".$procedimento[3]." *<br/>";
    }     
  }
  
  public function teste() {
    for ($b = 0;$b < sizeof($this->procedimentos);$b++) {      
      $procedimento = array();
      $procedimento = $this->procedimentos[$b];      
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]." = ".$procedimento[3]."<br/>";
    }    
    
    for ($b = 0;$b < sizeof($this->taxas);$b++) {      
      $procedimento = array();
      $procedimento = $this->taxas[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }  

    for ($b = 0;$b < sizeof($this->pacotes);$b++) {      
      $procedimento = array();
      $procedimento = $this->pacotes[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }     
    
    for ($b = 0;$b < sizeof($this->matmeds);$b++) {      
      $procedimento = array();
      $procedimento = $this->matmeds[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }  

    for ($b = 0;$b < sizeof($this->OPME);$b++) {      
      $procedimento = array();
      $procedimento = $this->OPME[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }      
  }
  
  /***
    Esta fun��o insere o registro da guia, sem efetuar qualquer valoriza��o ou verifica��o
  **/
  public function save() {
            
    $seg     = new Seguranca();
    $formata = new Formata();
    $util    = new Util();
    $data    = new Data();
    
    $this->preValidar();

    if ($this->qtdeErros() == 0) {
      $seg->alteraOperador($this->bd,$this->idOperador);
      
      $sql_conf = new Query($this->bd);  
      $txt_conf = "SELECT NVL(NVALACONF,30) DIA_AMB,NVL(NVALICONF,30) DIA_INT, CUNIMCONF CODIGO_UNIMED, NVL(PARAMETRO_NUMBER('NVGCONGUIA'),0) DIA_CONS FROM HSSCONF";
      $sql_conf->executeQuery($txt_conf); 
    
      if (($this->tipoGuia == 'guiaConsulta') and ($sql_conf->result("DIA_CONS") > 0))
        $this->diaVencimento = $sql_conf->result("DIA_CONS");
      else if ($this->natureza == 'A')
        $this->diaVencimento = $sql_conf->result("DIA_AMB");
      else
        $this->diaVencimento = $sql_conf->result("DIA_INT");       

      if ($this->idBeneficiario > 0) {
        if ($this->tipoGuia == 'guiaPedido') {
          if ($_SESSION['GuiaGeradaPedidoExame'] == 'L' and $this->executante > 0)
            $this->status = '';
          else if ($_SESSION['GuiaGeradaPedidoExame'] == '1')
            $this->status = '1';
          else
            $this->status = 'C';
        } 
        else {
		  if ($this->tipoGuia == 'guiaOdonto')
		    $this->status = 'A';
          elseif ($_SESSION['StatusLiberacao'] == 'L') 
            $this->status = '';
          elseif ($_SESSION['StatusLiberacao'] == 'C')
            $this->status = 'C';
          elseif ($_SESSION['StatusLiberacao'] == 'A')
            $this->status = 'A';
          elseif (($_SESSION['StatusLiberacao'] == '1') and ($this->tipoGuia <> 'guiaConsulta'))
            $this->status = 'C';            
          else
            $this->status = '';
        }
        
        if($this->modalidade == ''){
          $sql_contrato = new Query($this->bd);  
          $txt_contrato = "SELECT NVL(CMODATITU,'1') MODALIDADE,CUNIMPSAUD CODIGO_UNIMED_ORIGEM
                             FROM HSSUSUA,HSSTITU,HSSPSAUD
                            WHERE HSSUSUA.NNUMEUSUA = :idBeneficiario
                              AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                              AND HSSTITU.NNUMEPSAUD = HSSPSAUD.NNUMEPSAUD(+)";
          $sql_contrato->addParam(":idBeneficiario",$this->idBeneficiario);                    
          $sql_contrato->executeQuery($txt_contrato); 
              
          if ($sql_contrato->result("MODALIDADE") == '1')
            $this->modalidade = '1';
          else
            $this->modalidade = 'C';
        }
    
        if (($this->apelidoOperadora == 'assimedica') and ($this->natureza == 'I'))
          $this->status = 'A';        
        else if ($this->modalidade == '2' and $sql_conf->result("CODIGO_UNIMED") <> '' and $sql_contrato->result("CODIGO_UNIMED_ORIGEM") <> '')
          $this->status = '';
      }
	  
	    if ($this->idBeneficiario > 0) {
	      $sql_cancelado = new Query($this->bd);
		    $txt_cancelado = "SELECT DSITUUSUA - trunc(SYSDATE) DIAS
                            FROM HSSUSUA
                           WHERE NNUMEUSUA = :idBeneficiario";
        $sql_cancelado->addParam(":idBeneficiario",$this->idBeneficiario);
        $sql_cancelado->executeQuery($txt_cancelado); 
		  
		    if (($sql_cancelado->result("DIAS") > 0) and ($sql_cancelado->result("DIAS") <= 30)) {
		      $this->diaVencimento = $sql_cancelado->result("DIAS");
		    }	
	    }
      
      $naturezas_guia = array("I", "C", "O", "R", "P", "S");
 
      if ($this->motivoDigital <> '')
        $this->observacoes = $this->motivoDigital." - ".$this->observacoes;
        
      if ($this->atendimentoRn == 'S')
        $this->observacoes = 'Atendimento ao Recem Nascido'.$this->nomeFilho.'-'.$this->nascimentoFilho." - ".$this->observacoes;          
      else
        $this->observacoes =  $this->observacoes; 
        
      if (in_array($this->natureza, $naturezas_guia)) {
        $txt_acom = "SELECT CDESCACOM, 1 ORDEM,HSSACOM.NNUMEACOM
                       FROM HSSUSUA, HSSTITU, HSSPLAN,HSSACOM
                      WHERE HSSUSUA.NNUMEUSUA = :usuario
                        AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                        AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                        AND HSSUSUA.NNUMEACOM = HSSACOM.NNUMEACOM
                      UNION ALL
                     SELECT CDESCACOM, 2 ORDEM,HSSACOM.NNUMEACOM
                       FROM HSSUSUA, HSSTITU, HSSPLAN,HSSACOM
                      WHERE HSSUSUA.NNUMEUSUA = :usuario
                        AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                        AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                        AND HSSTITU.NNUMEACOM = HSSACOM.NNUMEACOM
                        AND HSSUSUA.NNUMEACOM IS NULL
                      UNION ALL
                     SELECT CDESCACOM, 3 ORDEM,HSSACOM.NNUMEACOM
                       FROM HSSUSUA, HSSTITU, HSSPLAN,HSSACOM,HSSACPLA
                      WHERE HSSUSUA.NNUMEUSUA = :usuario
                        AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                        AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                        AND HSSPLAN.NNUMEPLAN = HSSACPLA.NNUMEPLAN
                        AND HSSACPLA.NNUMEACOM = HSSACOM.NNUMEACOM
                        AND HSSUSUA.NNUMEACOM IS NULL
                        AND HSSTITU.NNUMEACOM IS NULL
                      ORDER BY 2,1 ";
        $sql_acom = new Query($this->bd);
        $sql_acom->addParam(":usuario",$this->idBeneficiario);
        $sql_acom->executeQuery($txt_acom);
        $this->idAcomodacao = $sql_acom->result("NNUMEACOM");
      }
    
      $sql_guia = new Query($this->bd);  
      $txt_guia = "SELECT SEQGUIA.NEXTVAL PROXIMO FROM DUAL";
      $erro = $sql_guia->executeQuery($txt_guia); 
    
      $this->idGuia = $sql_guia->result("PROXIMO");
      
      if ($this->idGuia > 0) {
              
        $sql = new Query($this->bd);
                           
        $txt = "INSERT INTO HSSGUIA (NNUMEGUIA,DEMISGUIA,NNUMEUSUA,NUTILUSUA,NNUMEPRES,
                                     NSOLIPRES,NLOCAPRES,NOPERUSUA,
                                     CTIPOGUIA,CTATEGUIA,CCALCGUIA,CLOCAGUIA,CURGEGUIA,CTPCOGUIA,
                                     NNUMEESPEC,NNUMECID_,CHIPOGUIA,DVALIGUIA,CIACIGUIA,
                                     CTPDOGUIA,NTPDOGUIA,CTTPDGUIA,CSTATGUIA,CDINDGUIA,NNUMEACOM,
                                     COBSEGUIA,NNUMEMOTLB,NCOMPGUIA,
                                     CREGIGUIA,NPEDIGUIA,NPTU_GUIA,CAUTOGUIA,CTSAIGUIA,CATRNGUIA,
                                     CASO_GUIA,DNARNGUIA,CCORTGUIA,DINTEGUIA,DPALTGUIA,CHALTGUIA,
                                     CLEITGUIA,DALTAGUIA,CSENHGUIA,DRECEGUIA,NPESOGUIA,NALTUGUIA,NSUPCGUIA,CPUOPGUIA,CPUQMGUIA,
                                     CJTOPGUIA,CDMATGUIA,COOPMGUIA,CPROCGUIA)
                             VALUES (:idGuia,SYSDATE,:idBeneficiario,:idBeneficiario,decode(:executante,0,null,:executante),
                                     decode(nvl(:solicitante,:executante),0,null,:solicitante),decode(:localAtendimento,0,null,:localAtendimento),:operador,
                                     :natureza,:tipoAtendimento,:modalidade,:localEmissao,:regime,:tipoConsulta,
                                     :especialidade,decode(:idCid,0,null,:idCid),:hipoteseDiagnostica,TRUNC(SYSDATE + :diaVencimento),:indicadorAcidente,
                                     :tipoDoenca,nvl(:tempoDoenca,0),:unidadeTempoDoenca,:status,:digital,decode(:acomodacao,0,null,:acomodacao),
                                     :observacoes,decode(:idMotivoDigital,0,null,:idMotivoDigital),decode(:guiaPrincipal,0,null,:guiaPrincipal),
                                     :regimeInternacao,:guiaPedido,:ptuGuia,:autorizacaoOrigem,:tipoSaidaSadt,:atendimentoRn,
                                     :asoOuGuia,:nascimentoFilho,:cortesia,TO_DATE(:data_internacao,'DD/MM/YYYY HH24:MI'),TO_DATE(:data_prev_alta,'DD/MM/YYYY'),:hora_prev_alta,
                                     :leito,:alta,:idGuia,:dataSolicitacao,:peso,:altura,:superficie,:previsaoOpme,:previsaoQuimio,
                                     :justificativaTecnica,:especificacaoMat,:observacaoOPME,:procedi_realizado)";
        
        /**      
        localEmissao
        - 5 - Web prestador
        - 6 - Web Benefici�rio (Affego)
        - 7 - Web prestador com biometria
        - 9 - Web prestador, pedido de exames
        **/
                      
        $sql->addParam(":idGuia"             ,$this->idGuia);
        $sql->addParam(":idBeneficiario"     ,$this->idBeneficiario);
        $sql->addParam(":executante"         ,$this->idExecutante);
        $sql->addParam(":solicitante"        ,$this->idSolicitante);        
        $sql->addParam(":localAtendimento"   ,$this->idLocalAtendimento);
        $sql->addParam(":operador"           ,$this->idOperador);
        $sql->addParam(":natureza"           ,$this->natureza);
        $sql->addParam(":tipoAtendimento"    ,$this->tipoAtendimento);
        $sql->addParam(":modalidade"         ,$this->modalidade);   
        $sql->addParam(":localEmissao"       ,$this->localEmissao);                     
        $sql->addParam(":regime"             ,$this->regime);        
        $sql->addParam(":tipoConsulta"       ,$this->tipoConsulta);             
        $sql->addParam(":especialidade"      ,$this->idEspecialidade);    
        $sql->addParam(":idCid"              ,$this->idCid);
        $sql->addParam(":hipoteseDiagnostica",$this->hipoteseDiagnostica);
        $sql->addParam(":diaVencimento"      ,$this->diaVencimento);
        $sql->addParam(":indicadorAcidente"  ,$this->indicadorAcidente);
        $sql->addParam(":tipoDoenca"         ,$this->tipoDoenca);
        $sql->addParam(":tempoDoenca"        ,$this->tempoDoenca);
        $sql->addParam(":unidadeTempoDoenca" ,$this->unidadeTempoDoenca);            
        $sql->addParam(":status"             ,$this->status);       
        $sql->addParam(":digital"            ,$this->digital);   
        $sql->addParam(":acomodacao"         ,$this->idAcomodacao);    
        $sql->addParam(":observacoes"        ,$this->observacoes);    
        $sql->addParam(":idMotivoDigital"    ,$this->idMotivoDigital);    
        $sql->addParam(":guiaPrincipal"      ,$this->idGuiaPrincipal);  
        $sql->addParam(":regimeInternacao"   ,$this->regimeInternacao);
        $sql->addParam(":guiaPedido"         ,$this->idGuiaPedido);  
        $sql->addParam(":ptuGuia"            ,$this->ptuGuia);  
        $sql->addParam(":autorizacaoOrigem"  ,$this->autorizacaoOrigem);  
        $sql->addParam(":tipoSaidaSadt"      ,$this->tipoSaidaSadt);
        $sql->addParam(":atendimentoRn"      ,$this->atendimentoRn);
        $sql->addParam(":asoOuGuia"          ,$this->asoOuGuia);
        $sql->addParam(":nascimentoFilho"    ,$this->nascimentoFilho);        
        $sql->addParam(":previsaoOpme"       ,$this->previsaoOpme);
        $sql->addParam(":previsaoQuimio"     ,$this->previsaoQuimio);
        $sql->addParam(":dataSolicitacao"    ,$this->dataSoliOnco);
        $sql->addParam(":peso"               ,$this->peso);
        $sql->addParam(":procedi_realizado"  ,$this->procedi_realizado);
        $sql->addParam(":altura"             ,str_replace('.',',',$this->altura));
        $sql->addParam(":superficie"         ,str_replace('.',',',$this->superficie));
        
        if ($this->nomeFilho <> '') 
          $sql->addParam(":cortesia"         ,$this->nomeFilho);
        else
          $sql->addParam(":cortesia"         ,$this->cortesia);
        
        if ($this->hora_internacao == '')
          $this->hora_internacao = '00:00';
          
        if ($this->data_internacao <> '')
          $internacao = $this->data_internacao." ".$this->hora_internacao;
        else
          $internacao = '';
          
        $sql->addParam(":data_internacao"    ,$internacao);                
        $sql->addParam(":hora_prev_alta"     ,$this->hora_prev_alta);
        $sql->addParam(":leito"              ,$this->leito);       
    
        if ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista') {
          $sql->addParam(":data_prev_alta"     ,$this->data_prev_alta);
          $sql->addParam(":alta"               ,'');  
        }
        else {        
          $alta = '';
          if (($internacao <> '') and sizeof($this->taxas) > 0 and ($_SESSION['apelido_operadora'] == 'unimedJiParana')) {

            $diarias = 0;
            
            $sql_proc = new Query($this->bd);
            $txt_proc = "SELECT SUM(NQUANDGUI) QTDE FROM HSSDGUI 
                          WHERE NNUMEGUIA IN ( SELECT NNUMEGUIA FROM HSSGUIA
                                                START WITH NNUMEGUIA = :idGuia
                                              CONNECT BY PRIOR NNUMEGUIA = NCOMPGUIA)";                                        
            $sql_proc->addParam(":idGuia" ,$this->idGuiaPrincipal);
            $erro = $sql_proc->executeQuery($txt_proc);
                            
            $diarias = $sql_proc->result("QTDE");
            
            for ($p = 0;$p < sizeof($this->taxas);$p++) {        
              $item = array();
              $item = $this->taxas[$p];
              
              $sql_proc = new Query($this->bd);
              $txt_proc = "SELECT NNUMETAXA FROM HSSTAXA WHERE NNUMETAXA = :idTaxa AND CTIPOTAXA = 'D'";                                        
              $sql_proc->addParam(":idTaxa" ,$item[2]);
              $erro = $sql_proc->executeQuery($txt_proc);     

              if ($sql_proc->count() > 0) {
                $diarias += $item[1]; 
              }
            } 

            if ($diarias > 0) {
              $alta = date('d/m/Y',$data->incrementaData($this->data_internacao,$diarias-1,'D'));
            }           
          }
          else
            $alta = $this->data_prev_alta;
                
          $sql->addParam(":data_prev_alta"     ,'');                
          $sql->addParam(":alta"               ,$alta);  
        }

        $sql->addParam(":justificativaTecnica" ,$this->justificativaTecnica);  
        $sql->addParam(":especificacaoMat"     ,$this->especificacaoMat);  
        $sql->addParam(":observacaoOPME"       ,$this->observacaoOPME);       

        $erro = $sql->executeSQL($txt);   
        
        $tem_na_guia = new Query($this->bd);
        $txt_tem_na_guia = "SELECT NNUMEGUIA FROM HSSPGUI
                            WHERE NNUMEGUIA = :guia
                             AND CCODIPMED = :procedimento ";
               
        /** CIDs **/
        if ($erro == '') {
          if ($this->idCid2 > 0) {
            $sql_cid = new Query($this->bd);
            $txt_cid = "INSERT INTO HSSCIDG (NNUMEGUIA,NNUMECID_) VALUES (:idGuia,:idCid)";
            $sql_cid->addParam(":idGuia",$this->idGuia);
            $sql_cid->addParam(":idCid",$this->idCid2);
            $sql_cid->executeSQL($txt_cid);
          }

          if ($this->idCid3 > 0) {
            $sql_cid = new Query($this->bd);
            $txt_cid = "INSERT INTO HSSCIDG (NNUMEGUIA,NNUMECID_) VALUES (:idGuia,:idCid)";
            $sql_cid->addParam(":idGuia",$this->idGuia);
            $sql_cid->addParam(":idCid",$this->idCid3);
            $sql_cid->executeSQL($txt_cid);
          }

          if ($this->idCid4 > 0) {
            $sql_cid = new Query($this->bd);
            $txt_cid = "INSERT INTO HSSCIDG (NNUMEGUIA,NNUMECID_) VALUES (:idGuia,:idCid)";
            $sql_cid->addParam(":idGuia",$this->idGuia);
            $sql_cid->addParam(":idCid",$this->idCid4);
            $sql_cid->executeSQL($txt_cid);
          }
        }
        
        /** Quimioterapia **/
        if ($erro == '') {
          if (($this->tipoGuia == 'guiaQuimioterapia') or ($this->tipoGuia == 'guiaRadioterapia')) {
                        
            $sql_quimio = new Query($this->bd);
            $txt_quimio = "INSERT INTO HSSONCO (NNUMEGUIA,CTIPOONCO,DDIAGONCO,CDIAIONCO,CESTAONCO,CTIPQONCO,CFINAONCO,CECOGONCO,CPLATONCO,
                                                CDIAGONCO,CINFOONCO,NCICPONCO,NCICAONCO,NINTCONCO,CCIRUONCO,DCIRUONCO,CTEXTONCO,DAPLIONCO,
                                                NCAMPONCO,NDOSDONCO,NDOSTONCO,NDIASONCO,DPREVONCO,COBSEONCO,CNSOLONCO,CFSOLONCO,CESOLONCO)
                                        VALUES (:idGuia,:tipoOnco,TO_DATE(:dataDiagnostico,'DD/MM/YYYY'),:diagnosticoImagem,:estadiamento,:tipoQuimio,:finalidade,:ecog,:planoTerapeutico,
                                                :diagnosticoCitoHisto,:informRelevantes,:numCiclos,:cicloAtual,:interCiclos,:cirurgia,TO_DATE(:dataRealizacao,'DD/MM/YYYY'),:areaIrradiada,TO_DATE(:dataAplicacao,'DD/MM/YYYY'),
                                                :numCampos,:doseDia,:doseTotal,:numDias,TO_DATE(:previsao,'DD/MM/YYYY'),:observacao,:solicitante,:telefone,:email)";
                                                
            $sql_quimio->addParam(":idGuia"               ,$this->idGuia);  
            
            if ($this->tipoGuia == 'guiaQuimioterapia')
              $sql_quimio->addParam(":tipoOnco"             ,'Q');  
            else
              $sql_quimio->addParam(":tipoOnco"             ,'R');  
              
            $sql_quimio->addParam(":dataDiagnostico"      ,$this->dataDiagnostico);  
            $sql_quimio->addParam(":diagnosticoImagem"    ,$this->diagnosticoImagem);  
            $sql_quimio->addParam(":estadiamento"         ,$this->estadiamento);  
            $sql_quimio->addParam(":tipoQuimio"           ,$this->tipoQuimioterapia);  
            $sql_quimio->addParam(":finalidade"           ,$this->finalidade);  
            $sql_quimio->addParam(":ecog"                 ,$this->ecog);  
            $sql_quimio->addParam(":planoTerapeutico"     ,$this->planoTerapeutico);  
            $sql_quimio->addParam(":diagnosticoCitoHisto" ,$this->diagnosticoCitoHisto);  
            $sql_quimio->addParam(":informRelevantes"     ,$this->informRelevantes);  
            $sql_quimio->addParam(":numCiclos"            ,$this->numCiclos);  
            $sql_quimio->addParam(":cicloAtual"           ,$this->cicloAtual);  
            $sql_quimio->addParam(":interCiclos"          ,$this->interCiclos);  
            $sql_quimio->addParam(":cirurgia"             ,$this->cirurgia);  
            $sql_quimio->addParam(":dataRealizacao"       ,$this->dataRealizacao);  
            $sql_quimio->addParam(":areaIrradiada"        ,$this->areaIrradiada);  
            $sql_quimio->addParam(":dataAplicacao"        ,$this->dataAplicacao);  
            $sql_quimio->addParam(":numCampos"            ,$this->numCampos);  
            $sql_quimio->addParam(":doseDia"              ,$this->doseDia);  
            $sql_quimio->addParam(":doseTotal"            ,$this->doseTotal);  
            $sql_quimio->addParam(":numDias"              ,$this->numDias);  
            $sql_quimio->addParam(":previsao"             ,$this->previsao);  
            $sql_quimio->addParam(":observacao"           ,$this->observacao);  
            $sql_quimio->addParam(":solicitante"          ,$this->nomeSoliOnco);  
            $sql_quimio->addParam(":telefone"             ,$this->telefoneSoliOnco);  
            $sql_quimio->addParam(":email"                ,$this->emailSoliOnco); 
            $erro = $sql_quimio->executeSQL($txt_quimio);           
          }        
        }
        
        /** Procedimentos **/
        if ($erro == '') {
          for ($p = 0;$p < sizeof($this->procedimentos);$p++) {        
            $item = array();
            $item = $this->procedimentos[$p];
            
            $tem_na_guia->clear();
            $tem_na_guia->addParam(":guia",$this->idGuia);
            $tem_na_guia->addParam(":procedimento",$item[0]);
            $tem_na_guia->executeQuery($txt_tem_na_guia);
            
          
            if($tem_na_guia->count() == 0) {
                    $sql_proc = new Query($this->bd);
                    $txt_proc = "INSERT INTO HSSPGUI (NNUMEGUIA,CCODIPMED,NQUANPGUI,NQINDPGUI,NUSUAPGUI,CPSOLPMED,NQSOLPGUI,CCOBRPGUI)
                                              VALUES (:idGuia,:procedimento,:qtde,0,0,:procedimento,:qtde,:cobra)";
                                               
                    $sql_proc->addParam(":idGuia"      ,$this->idGuia);
                    $sql_proc->addParam(":procedimento",$item[0]);
                    $sql_proc->addParam(":qtde"        ,$item[1]);  
             if($this->tipoGuia == 'guiaOdonto')
               $sql_proc->addParam(":cobra","X");
             else
               $sql_proc->addParam(":cobra",'');
                    $erro = $sql_proc->executeSQL($txt_proc); 
            }
            
            //Tem dente e face
            if ($item[2] <> '' and $item[3] <> '') {
              $sql_proc = new Query($this->bd);
              $txt_proc = "INSERT INTO HSSDPGUI (NNUMEGUIA,CCODIPMED,NNUMEDENTE,NNUMEFACE)
                                        VALUES (:idGuia,:procedimento,RetornaIdDente30(:dente),RetornaIdFace30(:face))";
                                         
              $sql_proc->addParam(":idGuia"      ,$this->idGuia);
              $sql_proc->addParam(":procedimento",$item[0]);
              $sql_proc->addParam(":dente"       ,$item[2]);  
              $sql_proc->addParam(":face"        ,$item[3]);  
              $erro = $sql_proc->executeSQL($txt_proc); 

              if ($_SESSION['apelido_operadora'] == 'saudemed' and $erro == '' ){
                $sql3 = new Query($this->bd);
                $txt = "SELECT COUNT(HSSDPGUI.CCODIPMED)QTDE 
                          FROM HSSPGUI,HSSDPGUI
                         WHERE HSSPGUI.CCODIPMED=HSSDPGUI.CCODIPMED
                           AND HSSPGUI.NNUMEGUIA=HSSDPGUI.NNUMEGUIA
                           AND HSSPGUI.NNUMEGUIA = :guia
                           AND HSSPGUI.CCODIPMED = :procedimento
                         GROUP BY HSSDPGUI.CCODIPMED";
                                 
                $sql3->clear();
                $sql3->addParam(":guia"        ,$this->idGuia);
                $sql3->addParam(":procedimento",$item[0]);
                $sql3->executeQuery($txt);

                  if ($sql3->result("QTDE") > 0){
                    $sql_at = new Query($this->bd);
                    $txt_at = "UPDATE HSSPGUI SET NQUANPGUI = :qtde_p  
                               WHERE NNUMEGUIA = :idGuia  
                                 AND CCODIPMED = :procedimento ";

                  $sql_at->addParam(":idGuia"      ,$this->idGuia);
                  $sql_at->addParam(":procedimento",$item[0]);
                  $sql_at->addParam(":qtde_p"      ,$sql3->result("QTDE"));	
                  $sql_at->executeSQL($txt_at);
                }
              }
            }
          }
        }
   
        /** Procedimento ASO **/
        if ($erro == '') {
          $i = 0;
          for ($p = 0;$p < sizeof($this->procedimentosAso);$p++) {  
                             
            $item = array();
            $item = $this->procedimentosAso[$p];
            
            //Esse la�o garante que que n�o exista procedimentos repetidos.
            $achou = false;
            foreach($this->aux as $v){
                if($v == $item[0])
                  $achou = true;
            }
            if($achou == false){
               $this->aux[$i] = $item[0];
              $sql_proc = new Query($this->bd);
              $txt_proc = "INSERT INTO HSSPGUI (NNUMEGUIA,CCODIPMED,NQUANPGUI,NQINDPGUI,NUSUAPGUI,CPSOLPMED,NQSOLPGUI,DEXAMPGUI)
                                        VALUES (:idGuia,:procedimento,1,0,0,:procedimento,1,:realizacao)";
                                         
              $sql_proc->addParam(":idGuia"      ,$this->idGuia);
              $sql_proc->addParam(":procedimento",$item[0]);
              $sql_proc->addParam(":realizacao"  ,$item[1]);  
              $erro = $sql_proc->executeSQL($txt_proc);
            }
              
            $i++;
          }
        }          

        /** Pacotes **/
        if ($erro == '') {
          for ($p = 0;$p < sizeof($this->pacotes);$p++) {        
            $item = array();
            $item = $this->pacotes[$p];

            if ($item[2] > 0) 
              $id_pacote = $item[2];
            else {
              $sql_id = new Query($this->bd);
              $txt_id = "SELECT RETORNAIDPACOTE30(:pacote,:executante,:usuario,:locaAtendimento) ID_PACOTE FROM DUAL";
              $sql_id->addParam(":pacote",$item[0]);
              $sql_id->addParam(":executante",$this->idExecutante);  
              $sql_id->addParam(":usuario",$this->idBeneficiario);  
              $sql_id->addParam(":locaAtendimento",$this->idLocalAtendimento);  
              $erro = $sql_id->executeQuery($txt_id); 
              $id_pacote = $sql_id->result("ID_PACOTE");
            }                      
            
            $sql_proc = new Query($this->bd);			
						  
            $sql_valor = new Query($this->bd);
            $txt_valor = "SELECT VL_PACOTE(:pacote,SYSDATE) VALOR_PACOTE FROM DUAL";
            $sql_valor->addParam(":pacote",$id_pacote);
            $erro = $sql_valor->executeQuery($txt_valor); 
            $vl_pacote = $sql_valor->result("VALOR_PACOTE");
      
            $txt_proc = "INSERT INTO HSSPCGUI (NNUMEGUIA,NNUMEPACOT,NQUANPCGUI,NVALOPCGUI) 
                                       VALUES (:idGuia,:idPacote,:qtde,:Valor)";
                                         
            $sql_proc->addParam(":idGuia"  ,$this->idGuia);
            $sql_proc->addParam(":idPacote",$id_pacote);
            $sql_proc->addParam(":qtde"    ,$item[1]);  
            $sql_proc->addParam(":Valor"   ,$vl_pacote);  
			
            $erro = $sql_proc->executeSQL($txt_proc);                      
          }       
        }    


        /** Taxas **/
        if ($erro == '') {
          for ($p = 0;$p < sizeof($this->taxas);$p++) {        
            $item = array();
            $item = $this->taxas[$p];
            
            $sql_proc = new Query($this->bd);
            $txt_proc = "INSERT INTO HSSDGUI (NNUMEGUIA,NNUMETAXA,NQUANDGUI,NVALODGUI) 
                                      VALUES (:idGuia,:idTaxa,:qtde,0)";
                                         
            $sql_proc->addParam(":idGuia" ,$this->idGuia);
            $sql_proc->addParam(":idTaxa" ,$item[2]);
            $sql_proc->addParam(":qtde"   ,$item[1]);  
            $erro = $sql_proc->executeSQL($txt_proc);                      
          }       
        }       
 
        /** Materiais e Medicamentos **/
        if ($erro == '') {
  
          for ($p = 0;$p < sizeof($this->matmeds);$p++) {        
            $item      = array();
            $item      = $this->matmeds[$p];
            
            $sql_proc = new Query($this->bd);
            $txt_proc = "INSERT INTO HSSMMGUI (NNUMEGUIA,NNUMEPRODU,NSOLIPRODU,NQUANMMGUI,NQSOLMMGUI,NVPROMMGUI,NVPAGMMGUI,
                                               NCOPAMMGUI,CFORNMMGUI,CCOBRMMGUI,CSTATMMGUI,CTIPOMMGUI,CMANUMMGUI,NNUMETABMM,
                                               DPREVMMGUI,NDOSEMMGUI,NFREQMMGUI,NNUMEVIAME,CTPANMMGUI) 
                                       VALUES (:idGuia,:idProduto,:idProduto,:qtde,:qtde,0,0,
                                               0,:fornecido,'X','L','M','A',:idTabela,
                                               :previsao,:dose,:frequencia,:via,:tpAnexo)";
                                       
            $sql_proc->addParam(":idGuia"      ,$this->idGuia);
            
            if ($_SESSION['apelido_operadora'] == 'unimedJiParana')
              $sql_proc->addParam(":fornecido"   ,'S');
            else
              $sql_proc->addParam(":fornecido"   ,'N');
            
            if ($this->tipoGuia == 'guiaQuimioterapia') // tpAnexo = 1 Obrigat�rio Quimio
              $tpAnexo = '1';
            else
              $tpAnexo = '';            
            
            $sql_proc->addParam(":idProduto"   ,$item[3]);
            $sql_proc->addParam(":idTabela"    ,$item[4]);
            $sql_proc->addParam(":qtde"        ,$item[1]);  
            $sql_proc->addParam(":previsao"    ,$item[5]);  
            $sql_proc->addParam(":dose"        ,$item[6]);  
            $sql_proc->addParam(":frequencia"  ,$item[7]);  
            $sql_proc->addParam(":via"         ,$item[8]);  
            $sql_proc->addParam(":tpAnexo"     ,$tpAnexo);
            $erro = $sql_proc->executeSQL($txt_proc);                      
          } 
         
        }    

        /** OPME **/        
        if ($erro == '') {
        
          for ($p = 0;$p < sizeof($this->OPME);$p++) {        
            $item    = array();
            $item    = $this->OPME[$p];
          
            $sql_proc = new Query($this->bd);
            $txt_proc = "INSERT INTO HSSMMGUI (NNUMEGUIA,NNUMEPRODU,NSOLIPRODU,NQUANMMGUI,NQSOLMMGUI,NVPROMMGUI,NVPAGMMGUI,
                                               NCOPAMMGUI,CFORNMMGUI,CCOBRMMGUI,CSTATMMGUI,CTIPOMMGUI,CMANUMMGUI,NNUMETABMM) 
                                       VALUES (:idGuia,:idProduto,:idProduto,:qtde,:qtde,0,(:valor * :qtde),
                                               0,'N','X',:status,'O',:manual,:idTabela)";
                                           
            $sql_proc->addParam(":idGuia"      ,$this->idGuia);
            $sql_proc->addParam(":idProduto"   ,$item[4]);
            $sql_proc->addParam(":idTabela"    ,$item[5]);
            $sql_proc->addParam(":qtde"        ,$item[1]);  
            
            
            if ($formata->somenteNumeros($item[3]) <> "") {
              $sql_proc->addParam(":valor"       ,str_replace('.',',',$item[3]));                
              $sql_proc->addParam(":manual"      ,'M');
              $sql_proc->addParam(":status"      ,'A');
            }
            else {
              $sql_proc->addParam(":valor"       ,0);
              $sql_proc->addParam(":manual"      ,'A');
              if(($this->apelidoOperadora == 'unimedBarra') or ($this->apelidoOperadora == 'unimedJiParana')){
                $sql_proc->addParam(":status"      ,'A');
                $this->setStatusGuia('A');
              }else{
                $sql_proc->addParam(":status"      ,'L');
              }
            }
            
            $erro = $sql_proc->executeSQL($txt_proc);                      
          }       
        } 
        
    
        //agentes
        if ($erro == '') {
          for ($p = 0;$p < sizeof($this->agentes);$p++) {        
            $item = array();
            $item = $this->agentes[$p];
            
            $sql_proc = new Query($this->bd);
            $txt_proc = "INSERT INTO HSSAGGUI (NNUMEGUIA,NNUMEAGENT,NQUANAGGUI)
                                      VALUES (:idGuia,:agente,:qtde)";
                                         
            $sql_proc->addParam(":idGuia"      ,$this->idGuia);
            $sql_proc->addParam(":agente",$item[0]);
            $sql_proc->addParam(":qtde"        ,$item[1]);  
            $erro = $sql_proc->executeSQL($txt_proc);                      
          }
    }
        /** ANEXOS **/
        $path = getcwd(); //$_SERVER['DOCUMENT_ROOT']."/solusweb_2_1/prestador/";
        $dir  = $util->criaDiretorio("../anexos");
        //$dir  = $util->criaDiretorio("arquivos");
        $dir  = $util->criaDiretorio("guias")."/";
                              
        for ($b = 0;$b < sizeof($this->anexos);$b++) {            
          $anexo = array();
          $anexo = $this->anexos[$b];
          
          $nome_arquivo = $anexo[0];     
          $tamanho      = $anexo[2];
          $nome_arquivo = $util->corrigeNomeArquivo($nome_arquivo);
          $file =  str_replace('//','/',$dir) . $this->idGuia. "_".$nome_arquivo;      

          if (move_uploaded_file($anexo[1], $file )) {

            $sql = new Query($this->bd);
            $txt = "SELECT SEQANEXO.NEXTVAL PROXIMO FROM DUAL";
            $sql->executeQuery($txt);
            $idAnexo = $sql->result("PROXIMO");
          
            $sql = new Query($this->bd);
            $txt = "INSERT INTO HSSANEXO (NNUMEANEXO,CNOMEANEXO,DDATAANEXO,NSIZEANEXO)
                                  VALUES (:idAnexo,:nomeArquivo,sysdate,:tamanho)";
            $sql->addParam(":idAnexo",$idAnexo);
            $sql->addParam(":nomeArquivo",$this->idGuia."_".$nome_arquivo);
            $sql->addParam(":tamanho",$tamanho);
            $sql->executeSQL($txt);
            
            $sql = new Query($this->bd);
            $txt = "INSERT INTO HSSGANEX (NNUMEANEXO,NNUMEGUIA)
                                  VALUES (:idAnexo,:idGuia)";
            $sql->addParam(":idAnexo",$idAnexo);
            $sql->addParam(":idGuia",$this->idGuia);
            $sql->executeSQL($txt); 
          
          } 
          else 
            $erro = "Erro ao anexar arquivo.";                   
        } 
        
        chdir($path);        
     
        if ($erro <> '') {
        
          /* Se ocorreu erro n�o estiver em transa��o, apago a guia */
          if (!$this->bd->inTransaction()) {
           // $delete = new Query();
            //$txt_delete = "DELETE FROM HSSGUIA WHERE NNUMEGUIA = :guia";         
           // $delete->addParam(":guia",$this->idGuia);
           // $delete->executeSQL($txt_delete);
          }
          
          return $erro;        
        }
        else
          return '';
      }
      else
        return $erro;
    }

  }
  
  /***
    Esta fun��o avalia os dados da guia, cobertura, carencia....
  **/  
  function validarGuia($codigo) {

    $func = new Funcao();
    $sql  = new Query($this->bd);
    $txt = "BEGIN ".
           "  GUIA.VALIDAR(:guia,:operador,:localEmissao,'N',:tipoGuia);".
           "END;";          
    $sql->addParam(":guia",$this->idGuia);
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":localEmissao",$this->localEmissao);
    $sql->addParam(":tipoGuia",$this->tipoGuia);
    $sql->executeSQL($txt);  

    if($_SESSION['apelido_operadora'] == 'clinipam'){
      $data=date('d/m/Y');    
      $sql2 = new Query($bd);
      $txt2="SELECT * from hssguia,hsspgui
        WHERE DEMISGUIA BETWEEN (SYSDATE-(2880/1440) /*48h*/) AND (SYSDATE)
          AND NNUMEUSUA = (SELECT NNUMEUSUA FROM HSSUSUA WHERE CCODIUSUA = :codigo)
          --AND (NLOCAPRES = localAtendimento or localAtendimento = 0)
          AND CSTATGUIA IS NULL
          AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
           AND HSSGUIA.NNUMEGUIA <> NVL(:guia,0)
  --        AND HSSPGUI.CCODIPMED = procedimento
          --AND HSSGUIA.NNUMEESPEC = especialidade";
      
      
      $sql2->addParam(":dataEmissao",$data);
      $sql2->addParam(":codigo",$codigo);
      $sql2->addParam(":guia", $this->idGuia);
      $sql2->executeQuery($txt2);   
     
      if(($sql2->result("NNUMEGUIA") > 0) and ($_SESSION['verifica'] != $codigo)){
        $_SESSION['valida_prazo'] = 1;
        $_SESSION['verifica'] = $codigo;
      }else{
        $_SESSION['valida_prazo'] = 2;
      }
    }
 
    $this->listarCriticas();
  }
  
  function listarCriticas() {
    $sql = new Query($this->bd);
    $txt = "SELECT CCODIPMED || ' - ' || GUIA.descricaoCritica(NMOTICRITI) MOTIVO ,NMOTICRITI,CCODIPMED
              FROM HSSCRITI
             WHERE NNUMEGUIA = :guia
               AND NNUMETAXA  IS NULL
               AND NNUMEDENTE IS NULL
               AND NNUMEFACE  IS NULL
             UNION ALL
            SELECT CCODITAXA || ' - ' || GUIA.descricaoCritica(NMOTICRITI) MOTIVO,NMOTICRITI,CCODIPMED 
              FROM HSSCRITI,HSSTAXA
             WHERE NNUMEGUIA = :guia
               AND HSSCRITI.NNUMETAXA = HSSTAXA.NNUMETAXA
             UNION ALL
            SELECT CCODIPMED || ' - (Dente: ' || CCODIDENTE || '/Face: ' || CCODIFACE || ')' || GUIA.descricaoCritica(NMOTICRITI) MOTIVO ,NMOTICRITI,CCODIPMED
              FROM HSSCRITI,HSSDENTE,HSSFACE
             WHERE NNUMEGUIA = :guia
               AND HSSCRITI.NNUMEDENTE = HSSDENTE.NNUMEDENTE
               AND HSSCRITI.NNUMEFACE  = HSSFACE.NNUMEFACE(+) ";
    $sql->addParam(":guia",$this->idGuia);
    $sql->executeQuery($txt);  
    
    while (!$sql->eof()) {
      // Usuario inadimplente
      if ($sql->result("NMOTICRITI") == 10) {
        $this->adicionaCritica('N�o foi poss�vel emitir esta autoriza��o. Gentileza entrar em contato com a Operadora.');
        $this->adicionaErro('N�o foi poss�vel emitir esta autoriza��o. Gentileza entrar em contato com a Operadora.');
      }
      else {
        $this->adicionaCritica($sql->result("MOTIVO"));
      
        if ($sql->result("NMOTICRITI") <> 35) {
          if ( ( ($sql->result("NMOTICRITI") <> 25) and ($sql->result("NMOTICRITI") <> 1) and ($_SESSION['apelido_operadora'] == 'unimedJiParana') ) or
             (($sql->result("NMOTICRITI") <> 25) and ($_SESSION['apelido_operadora'] <> 'unimedJiParana')) or
             (($sql->result("NMOTICRITI") == 25) and ($_SESSION['gera_negada'] == "S") and ($_SESSION['apelido_operadora'] <> 'unimedJiParana')) ) {
          
            if ($_SESSION['auditoria_guias'] == '3') {
            if ($sql->result("NMOTICRITI") <> 7)
              $this->adicionaErro($sql->result("MOTIVO"));
            } else 
            $this->adicionaErro($sql->result("MOTIVO"));
          } else
              if(($sql->result("NMOTICRITI") == 25) and ($_SESSION['gera_negada'] == "N"))
                $this->adicionaErro($sql->result("MOTIVO"));
		    }
      }
        
      if ($sql->result("NMOTICRITI") == 6 and ($_SESSION['apelido_operadora'] <> 'saudemed'))        
        $this->adicionaErro($func->listaRetorno($this->bd,$this->idBeneficiario,$this->idExecutante,$sql->result("CCODIPMED"),'',$this->idGuia));
        
      $sql->next();
    }  
  }
  
  /***
    Esta fun��o calcula os valores dos itens e suas respectivas co-participacoes
  **/    
  function calcularGuia() {
    $sql = new Query($this->bd);
    $txt = "BEGIN ".
           "  GUIA.CALCULARGUIA(:guia);".
           "END;";          
    $sql->addParam(":guia",$this->idGuia);
    $sql->executeSQL($txt);
  }  
  
  /***
    Esta fun��o faz avalia��o de integridade dos itens
  **/   
  function preValidar() {
    $func = new Funcao();
    
    // Verificar vari�veis obrigat�rias
    if ($this->tipoGuia == '')
      $this->adicionaErroFatal("vari�vel tipoGuia n�o informada.");      
    
    if ($this->idGuiaPrincipal > 0)	   
      if ($func->validaGuiaPrincipal($this->idGuiaPrincipal,$this->idBeneficiario,$this->natureza) <= 0)
        $this->adicionaErro("Guia principal informada inv�lida,cancelada ou n�o pertence a este benefici�rio.");      
    
    
    /** Procedimentos **/
    for ($p = 0;$p < sizeof($this->procedimentos);$p++) {        
      $item = array();
      $item = $this->procedimentos[$p];
      
      $sql_proc = new Query($this->bd);
      $txt_proc = "SELECT CCODIPMED,CSITUPMED FROM HSSPMED WHERE CCODIPMED = :procedimento";                                   
      $sql_proc->addParam(":procedimento",$item[0]);
      $erro = $sql_proc->executeQuery($txt_proc);
      
      if ($sql_proc->result("CCODIPMED") == '')
        $this->adicionaErro($item[0]." - C�digo do procedimento n�o existe.");
      else if ($sql_proc->result("CSITUPMED") <> 'A')
        $this->adicionaErro($item[0]." - C�digo do procedimento inativo.");
    } 
    
    /** Taxas **/
    for ($p = 0;$p < sizeof($this->taxas);$p++) {        
      $item    = array();
      $item    = $this->taxas[$p];
      $id_taxa = $func->validaTaxa($this->bd,$item[0],$_SESSION['id_contratado']);
      
      if ($id_taxa > 0)          
        $this->taxas[$p] = array($item[0],$item[1],$id_taxa);
      else {
        $this->adicionaErro($item[0]." - C�digo de taxa n�o existe ou n�o autorizado.");
      }                     
    }     
          
    /** Pacotes **/
    for ($p = 0;$p < sizeof($this->pacotes);$p++) {        
      $item = array();
      $item = $this->pacotes[$p];

      if ($item[2] > 0) 
        $id_pacote = $item[2];
      else {
        $sql_id = new Query($this->bd);
        $txt_id = "SELECT RETORNAIDPACOTE30(:pacote,:executante,:usuario,:locaAtendimento) ID_PACOTE FROM DUAL";
        $sql_id->addParam(":pacote",$item[0]);
        $sql_id->addParam(":executante",$this->idExecutante); 
        $sql_id->addParam(":usuario",$this->idBeneficiario);        
        $sql_id->addParam(":locaAtendimento",$this->idLocalAtendimento);  
        $erro = $sql_id->executeQuery($txt_id); 
        $id_pacote = $sql_id->result("ID_PACOTE");
      }                      
      
      if ($id_pacote > 0)
        $this->pacotes[$p] = array($item[0],$item[1],$id_pacote);        
      else
        $this->adicionaErro($item[0]." - C�digo do pacote n�o existe.");
    }  
        
    /** Materiais e Medicamentos **/
    for ($p = 0;$p < sizeof($this->matmeds);$p++) {        
      $item      = array();
      $item      = $this->matmeds[$p];          
      $retorno = array();
      $retorno = $func->validaMatmed2($this->bd,$item[0],$this->idLocalAtendimento,'',$item[2],'1',$this->idBeneficiario);
      
      if ($retorno['produto'] > 0)
        $this->matmeds[$p] = array($item[0],$item[1],$item[2],$retorno['produto'],$retorno['tabela'],$item[3],$item[4],$item[6],$item[5]);
      else
        $this->adicionaErro($item[0]." - C�digo de material/medicamento n�o existe ou n�o autorizado.");            
    }    
    
    /** OPMEs **/
    for ($p = 0;$p < sizeof($this->OPME);$p++) {        
      $item      = array();
      $item      = $this->OPME[$p];          
      $retorno = array();
      $retorno = $func->validaMatmed2($this->bd,$item[0],$this->idLocalAtendimento,'',$item[2],'O',$this->idBeneficiario);
                
      if ($retorno['produto'] > 0)
        $this->OPME[$p] = array($item[0],$item[1],$item[2],$item[3],$retorno['produto'],$retorno['tabela']);
      else 
        $this->adicionaErro($item[0]." - C�digo do OPME n�o existe ou n�o autorizado.");              
    }    
    
    for ($p = 0;$p < sizeof($this->agentes);$p++) {
      $item = array();
      $item = $this->agentes[$p];
      
      $sql_proc = new Query($this->bd);
      $txt_proc = "SELECT NNUMEAGENT FROM HSSAGENT WHERE NNUMEAGENT = :agente";                                   
      $sql_proc->addParam(":agente",$item[0]);
      $erro = $sql_proc->executeQuery($txt_proc);
      
      if ($sql_proc->result("NNUMEAGENT") == '')
        $this->adicionaErro($item[0]." - O agente " . $item[0] . " n�o existe.");
    }
  }
  
  /***
    Esta fun��o exclui a guia
  **/   
  function excluir() {  
    $delete = new Query();
    $txt_delete = "DELETE FROM HSSGUIA WHERE NNUMEGUIA = :guia";         
    $delete->addParam(":guia",$this->idGuia);
    $delete->executeSQL($txt_delete);

    $this->idGuia = 0;
  }
  /*
  function gerarGuia() {
    $func = new Funcao();
    
    $beneficiario = new Beneficiario($this->bd,$this->codigoBeneficiario);      
    $beneficiario->leitorCartao = $_SESSION['tem_leitora'];
    $beneficiario->validaBeneficiarioGuia($this->nascimentoBeneficiario,$this->senhaBeneficiario,$this->regime,$this->natureza,$this->idGuiaPrincipal);
    
    if ($beneficiario->getQtdeErrosFatal() == 0) {
            
      $this->idBeneficiario = $beneficiario->idBeneficiario;
      $this->save();
      
      if ($this->qtdeErrosFatal() > 0)
        return utf8_encode($this->getErrosFatal('\n'));
      else if ($this->qtdeErros() > 0) {
        return utf8_encode($this->getErros('\n'));
        $this->excluir();
      } 
      else {      
        $this->calcularGuia();
       
        if ($beneficiario->getQtdeErros() > 0) {
         
          $this->setStatusGuia('N');
         
          $erros = array();
          $erros = $beneficiario->getArrayErros();  
          
          for ($b = 0;$b < sizeof($erros);$b++) {      
            $erro = array();
            $erro = $erros[$b];            
            
            if ($erro[2] <> '')
              $this->adicionarCritica($erro[2]);
          }

          if (($func->parametroString($bd,"CEXGUICAR_WEB") == "2") and 
              ($func->possuiCriticaGuia($bd,$this->getIdGuia(),5) == "T")) { // Regra espec�fica para car�ncia gerar guia negada
            $status     = "negada";
            $excluir    = "N";
            $msgRetorno = "Autoriza��o n�o concedida, protocolo n�mero: ".$this->getIdGuia().
                          ".<br><br>".
                          $beneficiario->getErros().
                          $this->getErros();              
          }            
          else if ($_SESSION['gera_negada'] == "S") {
            $status     = "negada";
            $excluir    = "N";          
            $msgRetorno = "Autoriza��o n�o concedida, protocolo n�mero: ".$this->getIdGuia().
                          ".<br><br>".
                          $beneficiario->getErros().
                          $this->getErros();              
          }
          else if ($_SESSION['gera_negada'] == "A") {
            $status     = "auditoria";
            $excluir    = "N";          
            $this->setStatusGuia('A');
            
            $msgRetorno = "Autoriza��o n�o concedida, por�m enviada para auditoria m�dica, protocolo n�mero: ".$this->getIdGuia().
                          ".<br><br>".
                          $this->getErros();          
          }
          else {
            $status     = "negada";
            $excluir    = "S";
            $msgRetorno = "Autoriza��o n�o concedida.<br><br>".
                          $beneficiario->getErros().
                          $this->getErros();         
            $this->excluir();  
          }        

          if ($msgRetorno <> '')
            return utf8_encode($msgRetorno);        
                    
        }
        else if ($this->qtdeErros() > 0) {
        
          $sql_proc = new Query($bd);
          $txt_proc = "SELECT SUM(LIBERADO) QTDE,SUM(TOTAL) TOTAL FROM (
                       SELECT NNUMEGUIA,DECODE(CSTATPGUI,NULL,NQUANPGUI,0) LIBERADO,NQUANPGUI TOTAL FROM HSSPGUI WHERE NNUMEGUIA = :idGuia
                        UNION ALL
                       SELECT NNUMEGUIA,DECODE(CSTATPCGUI,NULL,NQUANPCGUI,0) LIBERADO,NQUANPCGUI TOTAL FROM HSSPCGUI WHERE NNUMEGUIA = :idGuia
                        UNION ALL
                       SELECT NNUMEGUIA,DECODE(CSTATDGUI,NULL,NQUANDGUI,0) LIBERADO,NQUANDGUI TOTAL FROM HSSDGUI WHERE NNUMEGUIA = :idGuia
                        UNION ALL
                       SELECT NNUMEGUIA,DECODE(CSTATMMGUI,'L',NQUANMMGUI,0) LIBERADO,NQUANMMGUI FROM HSSMMGUI WHERE NNUMEGUIA = :idGuia)";
          $sql_proc->addParam(":idGuia",$this->getIdGuia());
          $sql_proc->executeQuery($txt_proc);
        
          if (($func->parametroString($bd,"AUTORIZA_WEB_PARCIAL") == "S") and ($sql_proc->result("QTDE") > 0) and ($sql_proc->result("QTDE") < $sql_proc->result("TOTAL"))) {
            $status     = "liberada";
            $excluir    = "N";
            $msgRetorno = "Guia <b>".$this->getIdGuia()."</b> gerada parcialmente com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>. ";
          }
          else {
            $this->setStatusGuia('N');
          
            if (($func->parametroString($bd,"CGNRP_WEBP") == "1") and ($func->possuiCriticaGuia($bd,$this->getIdGuia(),39) == "T")) {
              $status     = "negada";
              $excluir    = "N";              
              $msgRetorno = "Autoriza��o n�o concedida, protocolo n�mero: ".$this->getIdGuia().".<br><br>".$this->getErros();          
            } 
            else if ($func->possuiCriticaGuia($bd,$this->getIdGuia(),55) == "T") { // Procedimento autogerado
              $status     = "negada";
              $excluir    = "N";              
              $msgRetorno = "Autoriza��o n�o concedida, protocolo n�mero: ".$this->getIdGuia().".<br><br>".$this->getErros();
            }  
            else  if ($_SESSION['gera_negada'] == "S") {
              $status     = "negada";
              $excluir    = "N";              
              $msgRetorno = "Autoriza��o n�o concedida, protocolo n�mero: ".$this->getIdGuia().".<br><br>".$this->getErros();              
            }
            else if ($_SESSION['gera_negada'] == "A") {
              $this->setStatusGuia('A');
              
              if ($func->parametroString($bd,"CSMSAUDIT") == "S") 
                $tpl->RESULT = "<script> mensagemAuditoria('".$this->getIdGuia()."','".$pagina."');</script>";
              else {
                $status     = "auditoria";
                $excluir    = "N";              
                $msgRetorno = "Autoriza��o n�o concedida, por�m enviada para auditoria m�dica, protocolo n�mero: ".$this->getIdGuia().".<br><br>".$guia->getErros();
              }
            } 
            else if ($_SESSION['valida_prazo'] == 1) {
              $status     = "negada";
              $excluir    = "N";              
              $msgRetorno = "Aten��o, j� existe esta mesma guia em um prazo de 48 horas.";
              $_SESSION['valida_prazo'] = 2;
            }
            else {
              $status     = "negada";
              $excluir    = "S";              
              $msgRetorno = "Autoriza��o n�o concedida.<br><br>".$this->getErros();              
              $this->excluir();              
            }          
          }   

          if ($msgRetorno <> '')
            return utf8_encode($msgRetorno);        
          
        }   
        else {
          $this->calcularGuia();         

          if ((($this->tipoGuia == 'guiaQuimioterapia') or ($this->tipoGuia == 'guiaRadioterapia')) and 
              ($func->parametroString($bd,"CEAUDQRWEB") == "S")) {
            $this->->setStatusGuia('A');  
          }
          
          $msg = new Dialog();
          $msg->type = "confirmacao";
          $msg->title = "Guia gerada";
                      
          $txt = "SELECT SUM(NFRANPGUI) NFRANPGUI,SUM(NCOPRPGUI) NCOPRPGUI 
                    FROM HSSPGUI
                   WHERE HSSPGUI.NNUMEGUIA = :guia";
          $sql_copart = new Query($bd);
          $sql_copart->addParam(":guia",$this->getIdGuia());
          $sql_copart->executeQuery($txt);          
              
          /* Verifica se h� necessidade de autorizacao no contrato */
      /*    $txt = "SELECT NVL(CAUTOTITU,'N') CAUTOTITU
                    FROM HSSTITU,HSSUSUA,HSSGUIA
                   WHERE HSSGUIA.NNUMEGUIA = :guia
                     AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                     AND HSSTITU.NNUMETITU = HSSUSUA.NNUMETITU";
          $sql2 = new Query($bd);
          $sql2->addParam(":guia",$this->getIdGuia());
          $sql2->executeQuery($txt);
          
          if ($sql2->result("CAUTOTITU") <> "N") {
            $this->setStatusGuia('P');
            
            $msg->text  = "Esta libera��o N�O FOI CONCEDIDA. Ela necessita de autoriza��o da empresa.<br/> 
                           Acompanhe pelo menu Emiss�o de guias/Rela��o de guias emitidas, o processo n� ".$this->getIdGuia();                            
          }       
          else {                          
            $sql_msg = new Query($db);
            $txt_msg = "SELECT STAT_USUA.CWEB_STAT, STAT_USUA.CMENWSTAT, STAT_USUA.CMSNWSTAT,
                               DECODE(NVL(STAT_USUA.NNUMESTAT,0),0,STAT_TITU.CMSNWSTAT,STAT_USUA.CMSNWSTAT) MENSAGEM
                          FROM HSSUSUA USUA,HSSTITU,HSSUSUA TITU,HSSSTAT STAT_USUA,HSSSTAT STAT_TITU
                         WHERE USUA.NNUMEUSUA = :id
                           AND USUA.NNUMETITU    = HSSTITU.NNUMETITU
                           AND USUA.NTITUUSUA    = TITU.NNUMEUSUA
                           AND USUA.NNUMESTAT    = STAT_USUA.NNUMESTAT(+) 
                           AND HSSTITU.NNUMESTAT = STAT_TITU.NNUMESTAT(+) ";
            $sql_msg->addParam(":id",$this->idBeneficiario);        
            $sql_msg->executeQuery($txt_msg);
      
            if ( ($sql_msg->result("MENSAGEM")   <> '' ) and 
                 ($sql_msg->result("CWEB_STAT")  == "S") and 
                 ($_SESSION['apelido_operadora'] == "unimedJiParana") )  {
              $msgAviso = $sql_msg->result("MENSAGEM");
            }  
             
            if (($this->tipoGuia == 'guiaPedido') and ($this->getStatusGuia() <> '29')) { //Verifica se � intercambio
              
              $msg->title = "Pedido gerado";
              
              if ($this->getStatusGuia() == 'A') {
              
                $msg->text = "Pedido <b>".$this->getIdGuia()."</b> gerado com sucesso, por�m enviada para Auditoria M�dica.
                              Por favor aguarde a libera��o ou entre em contato com a Operadora.";
              }                     
              else if ($func->parametroString($bd,"CIMPPED_WEB") == "S") {
                $filtros = array();
                $filtros['guia']     = $this->getIdGuia();
                $filtros['pagina']   = $tipoGuia;
                $_SESSION['filtros'] = $filtros; 
        
                $msg->text = "Pedido de exame n� <b>".$this->getIdGuia()."</b> gerado com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>.";
                $msg->addButton("Imprimir","popup","imprimeGuia.php?idSessao=".$_GET['idSessao'],"procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
              }
              else {
                $msg->text = "Pedido de exame n� <b>".$this->getIdGuia()."</b> gerado com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>".$msgAviso."</b>.";       
              }
            }
            else if ($this->tipoGuia == 'guiaOdonto') {   
              $msg->text = "Or�amento odontol�gico n� <b>".$this->getIdGuia()."</b> gerado com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>".$msgAviso."</b>.";                                         
            }        
            else {
              if ($this->getStatusGuia() == 'A'){
                $msg->text = "Solicita��o <b>".$this->getIdGuia()."</b> gerada com sucesso, por�m enviada para Auditoria M�dica. 
                              Por favor aguarde a libera��o ou entre em contato com a Operadora. ".$msgAviso;
              }                
              else if (($this->getStatusGuia() == '29') and ($_SESSION['apelido_operadora'] <> 'UnimedLestePaulista'))
                if ($tipoGuia == 'guiaPedido') {
                  $msg->title = "Pedido gerado";
                  $msg->text = "Pedido <b>".$this->getIdGuia()."</b> gerado com sucesso, por�m est� sendo encaminhada para a unimed 
                                origem em virtude de se tratar de um paciente de interc�mbio. 
                                Acompanhe o andamento da mesma em janela ao lado.";
                }else
                  $msg->text = "Guia <b>".$this->getIdGuia()."</b> gerada com sucesso, por�m est� sendo encaminhada para a unimed 
                                origem em virtude de se tratar de um paciente de interc�mbio. 
                                Acompanhe o andamento da mesma em janela ao lado.";  
                                
              else if (($this->getStatusGuia() == '29') and ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista'))
                $msg->text = "Guia <b>".$this->getIdGuia()."</b> foi <b>encaminhada</b> � Unimed do benefici�rio para ser autorizada. <br /><br />
                              <span style=\"color:#000099\">(*) Acompanhe o <b>andamento</b> da guia na janela ao lado.</span>";                                 

              else if ($this->getStatusGuia() == '') {
              
                $filtros = array();
                $filtros['guia']     = $this->getIdGuia();
                $filtros['pagina']   = $tipoGuia;
                $_SESSION['filtros'] = $filtros; 
                
                if ( ($_SESSION['apelido_operadora'] == 'sempreVida') and ($sql_copart->result("NFRANPGUI") > 0 or $sql_copart->result("NCOPRPGUI") > 0)) {
                  $msg->text = "Guia <b>".$this->getIdGuia()."</b> gerada com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>.";
                  $msg->text .= strtoupper("<br /><b><span style=\"color:#FF0000\">".'Existe uma coparticipa��o para esta guia'."</span></b>");
                  $msg->text .= " Aviso ao beneficiario: ".$msgAviso;
                  $msg->addButton("Imprimir","popup","imprimeGuia.php?idSessao=".$_GET['idSessao'],"procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
                }
                else if ( ($_SESSION['apelido_operadora'] == 'fatimaSaude') and ($sql_copart->result("NCOPRPGUI") > 0)) {
                  $msg->text = "Guia <b>".$this->getIdGuia()."</b> gerada com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>.";
                  $msg->text .= strtoupper("<br /><b><span style=\"color:#FF0000\">".'Existe uma coparticipa��o para esta guia R$ - '.$formata->formataNumero($sql_copart->result("NCOPRPGUI"))."</span></b>");
                  if ($msgAviso <> '') 
                    $msg->text .= " Aviso ao beneficiario: ".$msgAviso;
                  $msg->addButton("Imprimir","popup","imprimeGuia.php?idSessao=".$_GET['idSessao'],"procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
                }
                else{
                  $msg->text = "Guia <b>".$this->getIdGuia()."</b> gerada com sucesso, para o c�digo <b>".$this->codigoBeneficiario."</b>. ".$msgAviso;
                  $msg->addButton("Imprimir","popup","imprimeGuia.php?idSessao=".$_GET['idSessao'],"procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
                } 
                
                if ($_SESSION['apelido_operadora'] == 'sindifiscope')
                  $func->avisoGuiaEmail($bd,$this->getIdGuia());                
              }
              else if ($this->getStatusGuia() == 'P')
                $msg->text = "Solicita��o <b>".$this->getIdGuia()."</b> gerada com sucesso, por�m Aguardando autoriza��o da Operadora. ".$msgAviso;
              else if ($this->getStatusGuia() == 'C')
                $msg->text = "Solicita��o <b>".$this->getIdGuia()."</b> gerada com sucesso, por�m Aguardando confirma��o da Operadora. ".$msgAviso;               
              else
                $msg->text = "Guia n�o liberada, favor entrar em contato com a operadora.".$this->getIdGuia()."com o status.".$this->getStatusGuia()."</b>.";
            }
          }

          if ($this->tipoGuia == 'guiaPedido') 
            $msg->addButton("Nova Autoriza��o","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina."&codigo=".$this->codigoBeneficiario);
          
          $msg->addButton("OK","redirect","procedimento.php?idSessao=".$_GET['idSessao']."&pagina=".$pagina);
          $msg->setWidth(500);
          
          if ($msg->text <> '')
            return $msg->show();                
        }
      }      
    } 
    else { 
      $msg = new Dialog();
      $msg->type = "erro";
      $msg->title = "Emiss�o de guia";
      $msg->text = $beneficiario->getErrosFatal();          
      $msg->setWidth(500);
      return $msg->show();
    }  

    return false;
  } */
}
?>