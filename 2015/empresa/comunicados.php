<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  ini_set("memory_limit","12M");
   
 
  $atcad = 0;
    
  $_SESSION['titulo'] = "COMUNICADOS AGEMED";    

  require_once("../comum/layout.php");  
  $tpl->addFile("CONTEUDO","comunicados.html");  
 
  $tpl->ID_SESSAO = $_GET['idSessao'];
  
    $sql = new Query($bd);
    $txt = "SELECT * FROM HSSTITU WHERE NNUMETITU = :contrato"; 
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
	
	$sql2 = new Query($bd);
	$txt2 = "SELECT * FROM HSSCOMWE WHERE TRUNC(SYSDATE) >= DVIGECOMWE AND 
			CTIPOCOMWE = 'E'
			AND NNUMEEMPR IS NULL OR NNUMEEMPR = :NNUMEEMPR";
	$sql2->addParam(":NNUMEEMPR",$sql->result('NNUMEEMPR'));
    $sql2->executeQuery($txt2);
	
	
    $i = 1;
   
    while (!$sql2->eof()) {
    
	  if ($i==1)
        $tpl->COR_LINHA = '';
      else
        $tpl->COR_LINHA = '#EEEEEE';
        
      $i = $i * (-1);
           
      $tpl->ATCAD  = $sql2->result("NNUMECOMWE");      
	  $tpl->block("BOTAO_MAIS");	        
      
      $tpl->DATA = $sql2->result("DDATACOMWE");
      $tpl->TITUL = $sql2->result("CTITUCOMWE");
      
          
	  $tpl->MENSAGEM    = $sql2->result("CMENSCOMWE");         
      $tpl->block("ALTERACAO");          
          
      $tpl->block("REGISTRO");
      $sql2->next();
      
	  $tpl->block("BLOCO");  
    }     
  $tpl->block("REGISTROS");  
  $tpl->block("MOSTRA_MENU");     
  $bd->close();
  $tpl->show();     

?>