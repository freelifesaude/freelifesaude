<?php   
  session_start();
  require_once("../comum/autoload.php");

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $bd      = new Oracle();
  $func    = new Funcao();
  $formata = new Formata();
  $seg     = new Seguranca();
  
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,30,15);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(230,5,$_SESSION['nome_operadora']." - ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(230,5,'EXTRATO DE GUIA PARA SER AUDITADA PELA EMPRESA',0,1);
      $this->SetFont('Arial','B',7);           
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(205,3,$_SESSION['logado'],0,1);      
      $this->SetFont('Arial','B',10);
      $this->Ln(3);
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    function cabecalho($cobranca) {
      $this->SetFont('Arial','B',8);
      $this->Ln(1);
      $this->SetFont('Arial','B',8);
      $this->Cell(5,3,'',0,0);   

      if (($cobranca == '2'))
        $this->Cell(18,3,'Guia',0,0);
      
      $this->Cell(18,3,'Atendimento',0,0);
      $this->Cell(60,3,'Prestador',0,0);
      $this->Cell(18,3,'C�digo',0,0);
      $this->Cell(90,3,'Item',0,0);
      $this->Cell(10,3,'Qtde',0,0);
      $this->Cell(15,3,'Co-part',0,0);
      $this->Cell(15,3,'',0,1);
      $this->Cell(18,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(60,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(90,3,'',0,0);
      $this->Cell(10,3,'',0,0);
      $this->Cell(15,3,'',0,0);
      $this->Cell(12,3,'',0,0);
      $this->Ln(1);  
      $this->SetFont('Arial','',8);      
    }    
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  $usuario = "";
  $titular = "";
  $tipo = "";
  $conta = 0;
  $total_geral = 0;
  $desp_usuario = 0;
  $total_copart_titular = 0;
  $total_copart_titular_prestador = 0;
  $total_titular_reciprocidade = 0;
  $total_copart_usuario = 0;
  $total_copart_prestador = 0;
  $total_reciprocidade = 0;
  

                   
  $txt = "SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,DEMISGUIA,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                 USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'G',0,
                 TO_CHAR(DEMISGUIA,'DD/MM/YY') DATA, TOTAL_GUIA(HSSGUIA.NNUMEGUIA) CONTA,HSSGUIA.NNUMEGUIA,USUA.NNUMEPLAN,
                 IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                 '2' COBRANCA
            FROM HSSUSUA TITU,HSSUSUA USUA,HSSGUIA,FINPRES,FINPRES SOLI,HSSPLAN
           WHERE TITU.NNUMETITU = :id_contrato 
             AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
             AND USUA.NNUMEUSUA = HSSGUIA.NUTILUSUA
             AND USUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
             AND HSSGUIA.NNUMEGUIA = :guia
             AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES
             AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES
             AND HSSGUIA.NNUMEGUIA NOT IN (SELECT NNUMEGUIA FROM HSSGCON WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                            UNION ALL
                                           SELECT NNUMEGUIA FROM HSSGPAGA WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA)                              
           ORDER BY 8,2,19,4, 3";            
         
  $sql = new Query($bd);

  $sql->addParam(":id_contrato",$_SESSION['id_contrato']);
  $sql->addParam(":guia",$_POST['guia']);  

       
  $sql->executeQuery($txt);    

  if ($sql->count() > 0) {
  
    while (!$sql->eof()) {
   
      if ($titular <> $sql->result("TITULAR")) {   

        if (($quebrar == 'S') and ($titular <> ""))
          $pdf->AddPage();    
          
        $pdf->Cell(270,3,'Titular: '.$sql->result("TITULAR"),0,1);
        $pdf->Ln(3);        
        $titular                        = $sql->result("TITULAR");
        $total_copart_titular           = 0;
        $total_copart_titular_prestador = 0;
        $total_titular_reciprocidade    = 0;
        $tipo                           = "";
      }

      if ($usuario <> $sql->result("CNOMEUSUA")) {
        $pdf->SetTextColor(0,64,128);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(20,3,'Benefici�rio: ',0,0,'R');
        $pdf->Cell(100,3,$sql->result("CCODIUSUA").' - '.$sql->result("CNOMEUSUA"),0,0);      
        $pdf->Cell(20,3,'Idade: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("IDADE").' ANOS',0,0);
        $pdf->Cell(20,3,'Inclus�o: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("DINCLUSUA"),0,1);
        $pdf->Cell(20,3,'Plano: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("PLANO"),0,1);
        $pdf->Ln(3);        
        $usuario                = $sql->result("CNOMEUSUA");
        $codigo_usuario         = $sql->result("CCODIUSUA");
        $total_copart_usuario   = 0;
        $total_copart_prestador = 0;
        $total_reciprocidade    = 0;
        $tipo                   = "";
        $pdf->SetTextColor(0,0,0);
      }
      
      if ($tipo <> $sql->result("COBRANCA")) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Ln(3);
        $pdf->Cell(50,3,'GUIA AUDITADA',0,0);
          
        $pdf->Ln(3);
        $pdf->cabecalho($sql->result("COBRANCA"));
        
        $tipo = $sql->result("COBRANCA");
      }      

      $pdf->Cell(5,3,'',0,0);      
      $pdf->Cell(18,3,$sql->result("NNUMEGUIA"),0,0);
      $pdf->Cell(18,3,$sql->result("DATA"),0,0);
      $pdf->Cell(60,3,$pdf->Copy($sql->result("CNOMEPRES"),59),0,0);


      $txt2 = "SELECT HSSPGUI.CCODIPMED, CNOMEPMED, NFRANPGUI COPART,NQUANPGUI QTDE, NQINDPGUI VALOR,NCOPRPGUI COPART_PRES, CSTATPGUI,0 RECIPROCIDADE
                 FROM HSSPGUI,HSSPMED,HSSDENTE,HSSFACE
                WHERE HSSPGUI.NNUMEGUIA = :numero
                  AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                  AND HSSPGUI.NNUMEDENTE = HSSDENTE.NNUMEDENTE(+)
                  AND HSSPGUI.NNUMEFACE = HSSFACE.NNUMEFACE(+)
                  AND (HSSPGUI.CCODIPMED, HSSPGUI.NNUMEGUIA) NOT IN (SELECT CCODIPMED,NNUMEGUIA FROM HSSDPGUI
                                                                      WHERE NNUMEGUIA = HSSPGUI.NNUMEGUIA)
                UNION ALL
               SELECT CCODITAXA, CDESCTAXA,NVL(NCOPADGUI,0) COPART, NQUANDGUI, NVALODGUI, NVL(NVDPRDGUI,0),TO_CHAR(NULL),0 RECIPROCIDADE
                 FROM HSSDGUI,HSSTAXA
                WHERE HSSDGUI.NNUMEGUIA = :numero
                  AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                UNION ALL
               SELECT CCODIPRODU, CNOMEPRODU,NVL(NCOPAMMGUI,0) COPART, NQUANMMGUI, NVPAGMMGUI,NVL(NVAUTMMGUI,0),TO_CHAR(NULL),0 RECIPROCIDADE
                 FROM HSSMMGUI,ESTPRODU
                WHERE HSSMMGUI.NNUMEGUIA = :numero
                  AND HSSMMGUI.NSOLIPRODU = ESTPRODU.NNUMEPRODU";


      $sql3 = new Query($bd);
      $sql3->addParam(":numero",$_POST['guia']);
      $sql3->executeQuery($txt2);
      
      $qtde_proc = 0;

      while (!$sql3->eof()) {
   
        if ($qtde_proc > 0)
          $pdf->Cell(101,3,'',0,0);

        $pdf->Cell(18,3,$sql3->result("CCODIPMED"),0,0);
        $pdf->Cell(90,3,$pdf->Copy($sql3->result("CNOMEPMED"),89),0,0);
        $pdf->Cell(10,3,$sql3->result("QTDE"),0,0,'C');
        
        $pdf->Cell(15,3,$formata->formataNumero($sql3->result("COPART")),0,0,'R');	  
        $pdf->Cell(15,3,'',0,1,'R');

        $qtde_proc++;
        $total_copart_usuario   += str_replace(',','.',$sql3->result("COPART"));
        $total_copart_prestador += str_replace(',','.',$sql3->result("COPART_PRES"));
        $total_reciprocidade    += str_replace(',','.',$sql3->result("RECIPROCIDADE"));

        $total_copart_titular           += str_replace(',','.',$sql3->result("COPART"));
        $total_copart_titular_prestador += str_replace(',','.',$sql3->result("COPART_PRES"));
        $total_titular_reciprocidade    += str_replace(',','.',$sql3->result("RECIPROCIDADE"));
        $sql3->next();
          
      }

      $sql->next();
      
      if ($usuario <> $sql->result("CNOMEUSUA")) {
        $pdf->Ln(3);      
        $pdf->Cell(219,3,'Total do usu�rio: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_copart_usuario),0,0,'R');
        $pdf->Ln(6);  
      }


    }    
  $file='../temp/'.mt_rand().'.pdf';
  $pdf->Output($file,'F');
  
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 
  
  $bd->close();
    
  } else {
    $tpl->CLASSE = "";
    $tpl->MSG = "N�o existe nenhum atendimento de sa�de sem cobran�a.";
    $tpl->block("ERRO");
  }  
?>
