<?php

  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg', 10, 5, 40, 25);
      $this->SetFont('Arial', 'B', 10);
      $this->Cell(40, 5, '');
      $this->Cell(230, 5, $_SESSION['nome_operadora'] . " - " . $_SESSION['cnpj_operadora'], 0, 1);
      $this->Cell(40, 5, '');
      $this->Cell(230, 5, 'RELAT�RIO DE ATENDIMENTOS', 0, 1);
      $this->SetFont('Arial', 'B', 7);
      $this->Cell(40, 3, '');
      $this->Cell(25, 3, "Per�odo:", 0, 0, 'R');
      $this->Cell(205, 3, $this->getArray("Periodo"), 0, 1);
      $this->Cell(40, 3, '');
      $this->Cell(25, 3, 'Valores:', 0, 0, 'R');
      $this->Cell(205, 3, $this->getArray("Valores"), 0, 1);
      $this->SetFont('Arial', 'B', 8);
      $this->Cell(40, 3, '');      
      $this->Cell(205, 3, "Este relat�rio trata-se apenas de um hist�rico, sendo que o mesmo poder� n�o ser correspondente ao desconto em folha.");
      $this->Ln(3);
      $this->Cell(270, 1, ' ', 'B', 0);
      $this->Ln(2);

      $this->SetFont('Arial', 'B', 8);
      $this->Ln(1);
      $this->SetFont('Arial', 'B', 8);
      $this->Cell(18, 3, 'Conta/Guia', 0, 0);
      $this->Cell(18, 3, 'Atendimento', 0, 0);
      $this->Cell(60, 3, 'Prestador', 0, 0);
      $this->Cell(18, 3, 'C�digo', 0, 0);
      $this->Cell(90, 3, 'Item', 0, 0);
      $this->Cell(10, 3, 'Qtde', 0, 0);
      $this->Cell(15, 3, 'Co-part', 0, 0);
      $this->Cell(15, 3, 'Co-part', 0, 0);
      $this->Cell(12, 3, 'Valores', 0, 0);

      if ($_SESSION['apelido_operadora'] == 'affego')
          $this->Cell(15, 3, 'Reciprocidade', 0, 1);
      else
          $this->Cell(15, 3, '', 0, 1);

      $this->Cell(18, 3, '', 0, 0);
      $this->Cell(18, 3, '', 0, 0);
      $this->Cell(60, 3, '', 0, 0);
      $this->Cell(18, 3, '', 0, 0);
      $this->Cell(90, 3, '', 0, 0);
      $this->Cell(10, 3, '', 0, 0);
      $this->Cell(15, 3, '', 0, 0);
      $this->Cell(15, 3, 'Prestador', 0, 0);
      $this->Cell(12, 3, '', 0, 0);
      $this->Cell(15, 3, '', 0, 1);
      $this->Ln(1);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial', '', 8);
      //Page number
      $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
  }

  $pdf = new PDF('L', 'mm', 'A4');
  $pdf->AliasNbPages();

  if ($valores == '1')
    $valores_desc = "Todas";
  else if ($valores == '2')
    $valores_desc = "Somente valores n�o cobrados";
  else if ($valores == '3')
    $valores_desc = "Somente valores cobrados";

  $arr = array("Periodo" => ($datainicial . " a " . $datafinal),
               "Valores" => $valores_desc);
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220, 220, 200);
  $pdf->SetFont('Arial', '', 8);

  $usuario = "";
  $titular = "";
  $conta = 0;
  $total_geral = 0;
  $desp_usuario = 0;
  $total_copart_titular = 0;
  $total_copart_titular_prestador = 0;
  $total_titular_reciprocidade = 0;
  $total_copart_usuario = 0;
  $total_copart_prestador = 0;
  $total_reciprocidade = 0;
  $total_copart_usuario_pago = 0;
  $total_copart_usuario_npago = 0;

  while (!$sql->eof()) {
  
    if ($titular <> $sql->result("TITULAR")) {
        $pdf->Cell(270, 3, 'Titular: ' . $sql->result("TITULAR"), 0, 1);
        $pdf->Ln(3);
        $titular = $sql->result("TITULAR");
        $total_copart_titular = 0;
        $total_copart_titular_prestador = 0;
        $total_titular_reciprocidade = 0;
    }

    if ($usuario <> $sql->result("CNOMEUSUA")) {
        $pdf->Cell(100, 3, 'Usu�rio: ' . $sql->result("CCODIUSUA") . ' - ' . $sql->result("CNOMEUSUA"), 0, 0);
        $pdf->Cell(20, 3, $sql->result("IDADE") . ' ANOS', 0, 0);
        $pdf->Cell(20, 3, 'Inclus�o: ' . $sql->result("DINCLUSUA"), 0, 1);
        $pdf->Ln(3);
        $usuario = $sql->result("CNOMEUSUA");
        $total_copart_usuario = 0;
        $total_copart_prestador = 0;
        $total_reciprocidade = 0;
        $total_copart_usuario_pago = 0;
        $total_copart_usuario_npago = 0;
    }

    $pdf->Cell(18, 3, $sql->result("NNUMECONT"), 0, 0);
    $pdf->Cell(18, 3, $sql->result("DATA"), 0, 0);
    $pdf->Cell(60, 3, $pdf->Copy($sql->result("CNOMEPRES"), 59), 0, 0);

    if ($sql->result("TIPO") == "G") {
        $txt2 = "SELECT HSSPGUI.CCODIPMED,CNOMEPMED,NFRANPGUI COPART,NQINDPGUI VALOR,NQUANPGUI QTDE,
                    NCOPRPGUI COPART_PRES,0 RECIPROCIDADE
               FROM HSSPGUI,HSSPMED
              WHERE NNUMEGUIA = :numero
                AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED";
    } else {
        $txt2 = "SELECT HSSPCON.CCODIPMED,CNOMEPMED, SUM(NFRANPCON) COPART,SUM(TOTAL_CONTA_HONORARIOS_PCON(HSSPCON.NNUMEPCON)) VALOR, SUM(NQUANPCON) QTDE,
                        SUM(NCOPRPCON) COPART_PRES, SUM(NVL(NRECIPCON,0)+NVL(NREC1PCON,0)+NVL(NREC2PCON,0)+NVL(NREC3PCON,0)+NVL(NREC4PCON,0)) RECIPROCIDADE
               FROM HSSPCON,HSSPMED
              WHERE NNUMECONT = :numero
                AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                GROUP BY HSSPCON.CCODIPMED, CNOMEPMED
              UNION
             SELECT 'TAXAS' CCODIPMED,CDESCTAXA CNOMEPMED,SUM(NFRANTCON) COPART,SUM(NVALOTCON) VALOR,SUM(NQUANTCON) QTDE,
                    0 COPART_PRES,SUM(NVL(NRECITCON,0)) RECIPROCIDADE
               FROM HSSTCON,HSSTAXA
              WHERE NNUMECONT = :numero
                AND NVALOTCON > 0
                AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
              GROUP BY CDESCTAXA
              UNION
             SELECT 'MAT/MED' CCODIPMED,'MATERIAIS E MEDICAMENTOS' CNOMEPMED,VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT) COPART,TOTAL_FARMACIA(HSSCONT.NNUMECONT) VALOR, 1 QTDE,
                    0 COPART_PRES,0 RECIPROCIDADE
               FROM HSSCONT
              WHERE NNUMECONT = :numero
                AND TOTAL_FARMACIA(HSSCONT.NNUMECONT) > 0 ";
    }

    $sql3 = new Query($bd);
    $sql3->addParam(":numero", $sql->result("NNUMECONT"));
    $sql3->executeQuery($txt2);

    $qtde_proc = 0;

    while (!$sql3->eof()) {
        if ($qtde_proc > 0)
            $pdf->Cell(96, 3, '', 0, 0);

        $pdf->Cell(18, 3, $sql3->result("CCODIPMED"), 0, 0);
        $pdf->Cell(90, 3, $pdf->Copy($sql3->result("CNOMEPMED"), 89), 0, 0);
        $pdf->Cell(10, 3, $sql3->result("QTDE"), 0, 0, 'C');
        $pdf->Cell(15, 3, $formata->formataNumero($sql3->result("COPART")), 0, 0, 'R');
        $pdf->Cell(15, 3, $formata->formataNumero($sql3->result("COPART_PRES")), 0, 0, 'R');
        $pdf->Cell(10, 3, $sql->result("STATUS"), 0, 0);

        if ($_SESSION['apelido_operadora'] == 'affego')
            $pdf->Cell(15, 3, $formata->formataNumero($sql3->result("RECIPROCIDADE")), 0, 1, 'R');
        else
            $pdf->Cell(15, 3, '', 0, 1, 'R');

        $qtde_proc++;
        $total_copart_usuario = $total_copart_usuario + str_replace(',', '.', $sql3->result("COPART"));
        $total_copart_prestador = $total_copart_prestador + str_replace(',', '.', $sql3->result("COPART_PRES"));
        $total_reciprocidade = $total_reciprocidade + str_replace(',', '.', $sql3->result("RECIPROCIDADE"));
        
        if ($sql->result("STATUS") == 'Pago')
          $total_copart_usuario_pago = $total_copart_usuario_pago + str_replace(',', '.', $sql3->result("COPART"));
        else          
          $total_copart_usuario_npago = $total_copart_usuario_npago + str_replace(',', '.', $sql3->result("COPART"));

        $total_copart_titular = $total_copart_titular + str_replace(',', '.', $sql3->result("COPART"));
        $total_copart_titular_prestador = $total_copart_titular_prestador + str_replace(',', '.', $sql3->result("COPART_PRES"));
        $total_titular_reciprocidade = $total_titular_reciprocidade + str_replace(',', '.', $sql3->result("RECIPROCIDADE"));
        $sql3->next();
    }

    $pdf->Ln(3);

    $sql->next();
    
    if (($usuario <> $sql->result("CNOMEUSUA")) or $sql->eof()) {
      $pdf->Cell(214, 3, 'Total do usu�rio: ', 0, 0, 'R');
      $pdf->Cell(15, 3, $formata->formataNumero($total_copart_usuario), 0, 0, 'R');
      $pdf->Cell(15, 3, $formata->formataNumero($total_copart_prestador), 0, 0, 'R');

      if ($_SESSION['apelido_operadora'] == 'affego')
          $pdf->Cell(15, 3, $formata->formataNumero($total_reciprocidade), 0, 0, 'R');
      else
          $pdf->Cell(15, 3, '', 0, 0, 'R');

      if ($valores == '1') { // Somente mostrar quando o filtro valores for = todos
        $pdf->Ln(6);
        $pdf->Cell(214, 3, 'Total pago: ', 0, 0, 'R');
        $pdf->Cell(15, 3, $formata->formataNumero($total_copart_usuario_pago), 0, 0, 'R');
        $pdf->Ln(6);
        $pdf->Cell(214, 3, 'Total n�o pago: ', 0, 0, 'R');
        $pdf->Cell(15, 3, $formata->formataNumero($total_copart_usuario_npago), 0, 0, 'R');     
      }
      $pdf->Ln(6);
    }

    if (($titular <> $sql->result("TITULAR")) or $sql->eof()) {
      $pdf->Cell(214, 3, 'Total do titular: ', 0, 0, 'R');
      $pdf->Cell(15, 3, $formata->formataNumero($total_copart_titular), 0, 0, 'R');
      $pdf->Cell(15, 3, $formata->formataNumero($total_copart_titular_prestador), 0, 0, 'R');

      if ($_SESSION['apelido_operadora'] == 'affego')
          $pdf->Cell(15, 3, $formata->formataNumero($total_titular_reciprocidade), 0, 0, 'R');
      else
          $pdf->Cell(15, 3, '', 0, 0, 'R');

      $pdf->Ln(3);
    }
  }

  if ($faltas == 'S'){
    $txt3 = "SELECT DDATAAGEND, CHORAAGEND, CNOMEUSUA, CNOMEPRES,
                    DECODE(NFALTTITU, NULL, (SELECT NVALOESPFA
                                               FROM HSSESPFA
                                              WHERE NNUMETITU = HSSUSUA.NNUMETITU
                                                AND ((NNUMEPRES = HSSAGEND.NNUMEPRES AND NNUMEESPEC = HSSAGEND.NNUMEESPEC) OR
                                                     (NNUMEPRES = HSSAGEND.NNUMEPRES AND NNUMEESPEC IS NULL) OR
                                                     (NNUMEPRES IS NULL AND NNUMEESPEC = HSSAGEND.NNUMEESPEC))), NFALTTITU) VALOR_FALTA
               FROM HSSAPAGA, HSSAGEND, HSSUSUA, FINPRES, HSSTITU
              WHERE HSSAPAGA.NNUMEPAGA IS NULL -- falta n�o cobrada
                AND CSITUAGEND = 'F' AND NVL(HSSTITU.CFALTTITU, '1') <> '1'
								AND DDATAAGEND >= TO_DATE(:datainicial,'DD/MM/YYYY')
								AND DDATAAGEND < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                AND HSSAGEND.NNUMEAGEND = HSSAPAGA.NNUMEAGEND (+)
                AND HSSAGEND.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                AND HSSAGEND.NNUMEPRES = FINPRES.NNUMEPRES
                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                AND HSSTITU.NNUMETITU = :contrato
              ORDER BY DDATAAGEND,CHORAAGEND";
       
    $id_contrato = $_SESSION['id_contrato'];
    $sql4 = new Query($bd);
    $sql4->addParam(":contrato", $id_contrato);
    $sql4->addParam(":datainicial",$datainicial);   
    $sql4->addParam(":datafinal",$datafinal);   
    $sql4->executeQuery($txt3);

    if ($sql4->count() > 0) {
      $pdf->Ln(3);
      $pdf->SetFont('Arial', 'B', 8);
      $pdf->Cell(300, 3,'FALTAS N�O COBRADAS',0, 1, 'L');
      $pdf->Ln(1);
      $pdf->SetFont('Arial', '', 8);
      $pdf->Cell(30, 3,'Data','B', 0, 'L');
      $pdf->Cell(80, 3,'Usu�rio','B', 0, 'L');
      $pdf->Cell(80, 3,'Prestador','B', 0, 'L');
      $pdf->Cell(30, 3,'Valor','B', 1, 'L');
      $pdf->Ln(1);

      while (!$sql4->eof()) {
        $pdf->Cell(30, 3,$sql4->result("DDATAAGEND"). ' ' .$sql4->result("CHORAAGEND"),0, 0, 'L');
        $pdf->Cell(80, 3,$sql4->result("CNOMEUSUA"),0, 0, 'L');
        $pdf->Cell(80, 3,$sql4->result("CNOMEPRES"),0, 0, 'L');
        $pdf->Cell(30, 3,$formata->formataNumero($sql4->result("VALOR_FALTA")),0, 1, 'L');
        $sql4->next();
      }
    }
  }
	
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";
?>
