<?php

	class PreCadastro{
		
		function __call($func,$arq) {
		  if(!method_exists(get_class($this),$func)){
			  throw new Exception(" O metodo \"$func\" nao existe");
		  }
	  }

	  function insereUsuario ($bd,$id_contrato,$contrato,$nome,$categoria,$nascimento,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$titular,
                             $matricula,$salario,$admissao,$locacao,$plano,$acomodacao,$telefone1,$telefone2,$celular,$email,
                             $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,$inclusao,
                             $estado_civil,$profissao,$setor,$local,$vendedor,$id_proposta,$valor,$estabelecimento,$anexo1,$anexo2,$anexo3,$anexo4,$csus,$ndnv,$observacao,$uniao,
                             $aditivos,$centro_custo,$vencimento,$pai, $statuspre) {                           
      // Local E = Empresa C = Comercial  
      $formata = new Formata();
      $seg     = new Seguranca();
      $util    = new Util();
      $data    = new Data();
	  $func    = new Funcao();
	  
    if (isset($anexo1['tmp_name']))
      $tamanhoArquivo1 = filesize($anexo1['tmp_name'])/1024;
    else
      $tamanhoArquivo1 = 0;
      
    if (isset($anexo2['tmp_name']))
      $tamanhoArquivo2 = filesize($anexo2['tmp_name'])/1024;
    else
      $tamanhoArquivo2 = 0;
      
    if (isset($anexo3['tmp_name']))
      $tamanhoArquivo3 = filesize($anexo3['tmp_name'])/1024;
    else
      $tamanhoArquivo3 = 0;

    if (isset($anexo4['tmp_name']))
      $tamanhoArquivo4 = filesize($anexo4['tmp_name'])/1024;
    else
      $tamanhoArquivo4 = 0;  

    if (($tamanhoArquivo1 <= $func->convertBytes(ini_get("upload_max_filesize"))) or ($tamanhoArquivo2 <= $func->convertBytes(ini_get("upload_max_filesize"))) or
       ($tamanhoArquivo3 <= $func->convertBytes(ini_get("upload_max_filesize"))) or ($tamanhoArquivo4 <= $func->convertBytes(ini_get("upload_max_filesize")))) {
      if(($id_contrato == 0) || ($id_contrato == '')){
        $proximo = new Query($bd);
        $txt = " SELECT SEQTITU.NEXTVAL PROXIMO FROM DUAL ";
        $proximo->executeQuery($txt);
        $nnumetitu = $proximo->result("PROXIMO");
      }
      else{
        $nnumetitu = $id_contrato;
        $existeTitu = true;
      }
	  }
    
		if(($nnumetitu >0) and ($retorno['erro'] == '')){
		  $cpf  = $formata->somenteNumeros($cpf);
		  $pis  = $formata->somenteNumeros($pis);
		  $cep  = $formata->somenteNumeros($cep);
		  $csus = $formata->somenteNumeros($csus);
		  $ndnv = $formata->somenteNumeros($ndnv);
		  
		  if ($categoria == 'T' or $categoria == 'F')
			$grau = 'X';
			
		  $sqlcomp = new Query($bd);
		  
		  $retorno = array();
		  $retorno['erro'] = '';
   
		  $anoinclusao = substr($inclusao,7,4);
		  $mesinclusao = substr($inclusao,4,2);
  
		  $txt = "SELECT DCOBRCOMPE FROM HSSCOMPE
				   WHERE CCOMPCOMPE = :competencia
					 AND DCOBRCOMPE IS NOT NULL";
				 
		  $sqlcomp->clear();
		  $sqlcomp->addParam(":competencia",substr($inclusao,3,2).'/'.substr($inclusao,6,4));
		  $sqlcomp->executeQuery($txt);
		  
		  $sql = new Query($bd);
		  
  
		  $txt = "SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
				   WHERE C_CPFUSUA = :cpf
					 AND CTIPOUSUA = :categoria
					 AND CSITUUSUA in ('A','D')
					 AND NNUMETITU = :contrato
				   UNION ALL
				  SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
				   WHERE C_CPFUSUA = :cpf
					 AND NTITUUSUA = :titular
					 AND CSITUUSUA in ('A','D')
					 AND NNUMETITU = :contrato           
                   UNION ALL
                  SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
				   WHERE C_CPFUSUA = :cpf
                     AND CTIPOUSUA = :categoria
                     AND CSITUUSUA in ('A','D')
				   UNION ALL
				  SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
				   WHERE C_CPFUSUA = :cpf
                     AND NTITUUSUA = :titular
                     AND CSITUUSUA in ('A','D') 
				   UNION ALL
				  SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
				   WHERE C_CPFATCAD = :cpf
					 AND CTIPOATCAD = :categoria
					 AND CFLAGATCAD IS NULL
					 AND COPERATCAD = 'I'
					 AND NNUMETITU = :contrato
				   UNION ALL
				  SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
				   WHERE C_CPFATCAD = :cpf
					 AND NTITUUSUA = :titular
					 AND CFLAGATCAD IS NULL
					 AND COPERATCAD = 'I'
					 AND NNUMETITU = :contrato             
				   UNION ALL
				  SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
				   WHERE C_CPFATCAD = :cpf
					 AND CTIPOATCAD = :categoria
					 AND CFLAGATCAD IS NULL
					 AND COPERATCAD = 'I'
					 AND NNUMEPPROP = :proposta
				   UNION ALL
				  SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
				   WHERE C_CPFATCAD = :cpf
					 AND NTITUUSUA = :titular
					 AND CFLAGATCAD IS NULL
					 AND COPERATCAD = 'I'
					 AND NNUMEPPROP = :proposta ";
					
		  $sql->clear();
		  $sql->addParam(":cpf",$cpf);
		  $sql->addParam(":categoria",$categoria);
		  $sql->addParam(":titular",$titular);
		  $sql->addParam(":contrato",$nnumetitu);
		  $sql->addParam(":proposta",$id_proposta);
		  $sql->executeQuery($txt);
  
		  $duplicidade_cpf = $seg->permissaoOutros($bd,"WEBEMPRESACADASTRARCPFDUPLICADOPCMSO",false);
		
		  if ( ($sql->count() > 0) && ( $duplicidade_cpf == false ) )
			$retorno['erro'] = "Não é possível cadastrar esse novo usuário. CPF em duplicidade! (".$sql->result("CNOMEUSUA").").";
		  else if ($sqlcomp->count() > 0)
			$retorno['erro'] = "A competência para esta data de inclusão está fechada! (".$inclusao.").";
		  else if (!$util->validaCNS($csus))
			$retorno['erro'] = 'Número do Cartão do SUS inválido.';                
		  else {
			$txt = "SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
					 WHERE (CNOMEUSUA = :nome AND TO_CHAR(DNASCUSUA,'DD/MM/YYYY') = :nascimento)
					   AND CTIPOUSUA = :categoria
					   AND CSITUUSUA = 'A'
					   AND NNUMETITU = :contrato
					 UNION ALL
					SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
					 WHERE (CNOMEATCAD = :nome AND TO_CHAR(DNASCATCAD,'DD/MM/YYYY') = :nascimento)
					   AND CTIPOATCAD = :categoria
					   AND CFLAGATCAD IS NULL
					   AND COPERATCAD = 'I'
					   AND NNUMETITU = :contrato 
					 UNION ALL
					SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
					 WHERE (CNOMEATCAD = :nome AND TO_CHAR(DNASCATCAD,'DD/MM/YYYY') = :nascimento)
					   AND CTIPOATCAD = :categoria
					   AND CFLAGATCAD IS NULL
					   AND COPERATCAD = 'I'
					   AND NNUMEPPROP = :proposta ";               
					   
			$sql->clear();
			$sql->addParam(":nome",$nome);
			$sql->addParam(":nascimento",$nascimento);    
			$sql->addParam(":categoria",$categoria);
			$sql->addParam(":contrato",$nnumetitu);
			$sql->addParam(":proposta",$id_proposta);    
			$sql->executeQuery($txt);  
			   
			if ($sql->count() > 0) {
			  $retorno['erro'] = "Não é possível cadastrar esse novo usuário. Cadastro em duplicidade! (".$sql->result("CNOMEUSUA").").";
			} else { 
			  if (($categoria == 'T') or ($categoria == 'F')) {
			if ($locacao > 0)
			$txt1 = " AND  NNUMESETOR = :id_locacao";
			else	
				  $txt1 = ""; 
  
				$txt = "SELECT NNUMEUSUA FROM HSSUSUA
						 WHERE CTIPOUSUA = 'T'
						   AND CSITUUSUA = 'A'
						   AND NNUMETITU = :contrato
						   AND CCHAPUSUA = :matricula".
				 $txt1.
					   "  UNION ALL
						SELECT NNUMEUSUA FROM HSSATCAD
						 WHERE CTIPOATCAD = 'T'
						   AND CFLAGATCAD IS NULL
						   AND COPERATCAD = 'I'
						   AND NNUMETITU = :contrato
						   AND CCHAPATCAD = :matricula".
				 $txt1.
					   " UNION ALL
						SELECT NNUMEUSUA FROM HSSATCAD
						 WHERE CTIPOATCAD = 'T'
						   AND CFLAGATCAD IS NULL
						   AND COPERATCAD = 'I'
						   AND NNUMEPPROP = :proposta
						   AND CCHAPATCAD = :matricula".
				 $txt1;
						   
				$sql->clear();
				$sql->addParam(":matricula",$matricula);
				$sql->addParam(":contrato",$nnumetitu);
				$sql->addParam(":proposta",$id_proposta);
			if ($locacao > 0)
				  $sql->addParam(":id_locacao",$locacao);		  
				$sql->executeQuery($txt); 
					 
				if ($sql->count() > 0) {
				  $continua = false;
				} else {
				  $continua = true;
				}
			  } else {
				$continua = true;
			  }
			  
			  if (!$continua) {
				$retorno['erro'] = "Não é possível cadastrar esse novo usuário, pois já existe um usuário com a mesma matrícula.";
			  } else {  
				$txt = "SELECT HSSTITU.NNUMETITU
						  FROM HSSTITU,HSSPLAN
						 WHERE HSSTITU.NNUMETITU = :contrato
						   AND (
								(:categoria = 'D' AND :sexo = 'M' AND :grau <> 'E' AND :grau <> 'J' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMMTITU,NVL(NLDEMPLAN,999))) OR
								(:categoria = 'D' AND :sexo = 'F' AND :grau <> 'E' AND :grau <> 'J' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMFTITU,NVL(NLDEFPLAN,999))) OR
								(:categoria = 'U' AND :sexo = 'M' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMUTITU,NVL(NLDEUPLAN,999))) OR
								(:categoria = 'U' AND :sexo = 'F' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIUFTITU,NVL(NLDUFPLAN,999))) OR
								(:categoria = 'A' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMATITU,NVL(NLAGRPLAN,999)))
								)
						   AND HSSPLAN.NNUMEPLAN = :plano
						   AND NVL(DLIMIPLAN,SYSDATE) <= SYSDATE ";
						   
				$sql->clear();
				$sql->addParam(":categoria",$categoria);
				$sql->addParam(":sexo",$sexo);
				$sql->addParam(":grau",$grau);
				$sql->addParam(":nascimento",$nascimento);        
				$sql->addParam(":plano",$plano);         
				$sql->addParam(":contrato",$nnumetitu);
				$sql->executeQuery($txt); 
  
				if ($sql->count() > 0) {
				  $retorno['erro'] = "Este usuário ultrapassa a idade limite para este contrato.<br />
									  Verifique os campos de data de nascimento, categoria do usuário, grau de parentesco e sexo.";
				  $continua = false;                    
				} 
			
/*				if ($estabelecimento == 2){  
				  $txt = " SELECT COUNT(1) EXISTE FROM HSSATCAD
							WHERE CTIPOATCAD IN ('T','F') 
							  AND NNUMEPPROP = :proposta " ;
				  $sql->clear();
				  $sql->addParam(":proposta",31294539);
				  $sql->executeQuery($txt);
			  
				  if($sql->count() > 0){
					$retorno['erro'] = "Já existe um titular para esta proposta.";
					$continua = false;
				  } else
					$continua = true;                                 
				} else {   */         
				  if (($continua) and (($grau == "E") or ($grau == "J"))) {        
					$txt = "SELECT COUNT(1) QTDE
							  FROM (SELECT NNUMEUSUA FROM HSSUSUA
									 WHERE CSITUUSUA = 'A'
									   AND CGRAUUSUA IN ('E','J')
									   AND NTITUUSUA = :titular 
									 UNION
									SELECT NNUMEUSUA FROM HSSATCAD
									 WHERE CFLAGATCAD IS NULL
									   AND COPERATCAD = 'I'  
									   AND CGRAUATCAD IN ('E','J')
									   AND NTITUUSUA = :titular) ";
							   
					$sql->clear();
					$sql->addParam(":titular",$titular);
					$sql->executeQuery($txt);   
  
					if ($sql->result("QTDE") > 0) {
					  $retorno['erro'] = "Não é possível cadastrar esse novo usuário, pois já existe um dependente com o grau de parentesco \"Esposa(o)\".";
					  $continua = false;
					} else {   
					  $continua = true;
					}
				  }
         if(($continua) and ($existeTitu == true)){
            $txt_vendedor ="SELECT NNUMETITU QTDE
                              FROM HSSTITU
                             WHERE NNUMEVEND = :idVendedor
                               AND CCODITITU = :idTitu";

            $sql->clear();
            $sql->addParam(":idVendedor",$_SESSION['id_vendedor']);
            $sql->addParam(":idTitu",$contrato);
            $sql->executeQuery($txt_vendedor); 
            if ($sql->result("QTDE") > 0) {
              $continua = true;
            } else {
              $retorno['erro'] = "Não é possível cadastrar esse novo usuário, ele faz parte da carteira de outro vendedor.";
              $continua = false;
            }
          }
  
				    
				  if (($continua) and ($retorno['erro'] == '')) {	
				    if(($categoria == 'T') or ($categoria == 'F')){				  			    					
              $txt_vendedor ="SELECT NNUMECONGE, NNUMEUNCOM, NNUMEESTAB
                                FROM HSSVEND
                               WHERE NNUMEVEND = :idvendedor ";
                     
              $sql->clear();
              $sql->addParam(":idvendedor",$_SESSION['id_vendedor']);
              $sql->executeQuery($txt_vendedor);
					  $titu = new Query($bd);
					  $txt = "insert into hsstitu(nnumetitu,ccodititu,ndia_venci,nnumeplan,defettitu,nendetlogr,cendetitu,cbairtitu,ccidatitu,c_ceptitu,cnumetitu,cprectitu,
												  cobsetitu,cestatitu,csitutitu,dconttitu,cnatutitu,cbenetitu,cperititu,noperusua,cmailtitu,crefetitu,nnumestat,dstattitu,nnumevend,
                          nnumeconge,nnumeuncom,nnumeestab)
										   values(:nnumetitu,:ccodititu,:ndia_venci,:nnumeplan,sysdate,:nendetlogr,:cendetitu,:cbairtitu,:ccidatitu,:c_ceptitu,:cnumetitu,'1',
												  :obs,:cestatitu,'D',:datacontrato,'1','1','1',:noperusua,:cmailtitu,:crefetitu,:nnumestat,trunc(sysdate),:nnumevend,
                          :idcongenere,:idunidade,:estabelecimento)";
			  
		  
			  
					  $titu->addParam(":nnumetitu",$nnumetitu);
					  $titu->addParam(":ccodititu",$contrato);
					  $titu->addParam(":ndia_venci",$vencimento);
					  $titu->addParam(":nnumeplan",$plano);
					  $titu->addParam(":nendetlogr",$tipo_logradouro);
					  $titu->addParam(":cendetitu",$logradouro);
					  $titu->addParam(":cbairtitu",$bairro);
					  $titu->addParam(":ccidatitu",$cidade);
					  $titu->addParam(":c_ceptitu",$cep);
					  $titu->addParam(":cnumetitu",$numero);
					  $titu->addParam(":obs",$mensagem.' '.$observacao);
					  $titu->addParam(":cestatitu",$estado);		
					  $titu->addParam(":noperusua",$_SESSION['id_operador']);
					  $titu->addParam(":cmailtitu",$email);
					  $titu->addParam(":crefetitu",$complemento);
					  $titu->addParam(":nnumestat",$statuspre);					
					  $titu->addParam(":datacontrato",$inclusao);
            $titu->addParam(":nnumevend",$vendedor);            
            $titu->addParam(":idcongenere",$sql->result("NNUMECONGE"));
            $titu->addParam(":idunidade",$sql->result("NNUMEUNCOM"));
            $titu->addParam(":estabelecimento",$sql->result("NNUMEESTAB"));
  
					  $retorno['erro'] = $titu->executeSQL($txt);
					  
					  if($retorno['erro'] == ''){
						  $status = new Query($bd);
						  $txt = "INSERT INTO HSSMSTAT(NNUMESTAT,NNUMETITU,NOPERUSUA,DDATAMSTAT,CMOTIMSTAT,DDIGIMSTAT)
						                    VALUES(:nnumestat,:nnumetitu,:noperusua,trunc(sysdate),'Inclusão via web comercial',sysdate)";
						  $status->addParam(":nnumestat",$statuspre);
						  $status->addParam(":nnumetitu",$nnumetitu);
						  $status->addParam(":noperusua",$_SESSION['id_operador']);
						  $status->executeSQL($txt);
					  }
				    }
					
					  
					if(($retorno['erro'] == '') and (($categoria == 'T') or ($categoria == 'F'))){
			  		  $txt = " INSERT INTO HSSTLCON(NNUMETITU,CFONETLCON,NOPERUSUA,CTIPOTLCON)
			                                   VALUES(:nnumetitu,:cfonetlcon,:noperusua,:ctipotlcon)";
			  		  $fone = new Query($bd);
			  
			  		  if($telefone1 != ''){
				        $fone->clear();
						$fone->addParam(":nnumetitu",$nnumetitu);
						$fone->addParam(":cfonetlcon",$telefone1);
						$fone->addParam(":noperusua",$_SESSION['id_operador']);
						$fone->addParam(":ctipotlcon",'R');
						$fone->executeSQL($txt);
			  		  }
			  		  if($telefone2 != ''){
					    $fone->clear();
						$fone->addParam(":nnumetitu",$nnumetitu);
						$fone->addParam(":cfonetlcon",$telefone2);
						$fone->addParam(":noperusua",$_SESSION['id_operador']);
						$fone->addParam(":ctipotlcon",'O');
						$fone->executeSQL($txt);
			 		  }
			  		  if($celular != ''){
					    $fone->clear();
						$fone->addParam(":nnumetitu",$nnumetitu);
						$fone->addParam(":cfonetlcon",$celular);
						$fone->addParam(":noperusua",$_SESSION['id_operador']);
						$fone->addParam(":ctipotlcon",'C');
						$fone->executeSQL($txt);
					  }
					}  
					$mensagem = utf8_decode("INCLUSAO VIA WEB COMERCIAL - ");
					$sql_carencia_textual = new Query($bd);
					
						$txt_care = "SELECT CCAREPLAN FROM HSSTITU, HSSPLAN WHERE NNUMETITU = :titular
									 AND HSSPLAN.NNUMEPLAN = HSSTITU.NNUMEPLAN";
						$sql_carencia_textual->addParam(":titular",$nnumetitu);        
						$sql_carencia_textual->executeQuery($txt_care); 
						$carencia = $sql_carencia_textual->result("CCAREPLAN");				
				
								
					$sql->clear();          
					$txt = "BEGIN ".
						   "   INSERE_USUARIO_PRE30(:contrato,:titular,:nome,:categoria,TO_DATE(:nascimento,'DD/MM/YYYY'),:ufNasc,:cidadeNasc,:sexo,:matricula,".
						   "                     :mae,:pai,:cpf,:rg,:orgao,:pis,:grau,:locacao,TO_DATE(:admissao,'DD/MM/YYYY'),:endereco,:bairro, ".
						   "                     :cidade,:estado,:cep,:plano,:acomodacao,:telefone1,to_date(:expedicao,'DD/MM/YYYY'),:numero, ".
						   "                     :tipo_logradouro,:complemento,:telefone2,:celular,:email,:salario,:inclusao,:estado_civil, ".
						   "                     :profissao,:setor,:local,:vendedor,:operador,:proposta,:valor,:csus,:ndnv,:observacao,:uniao,".
						   "                     :centro_custo,:ip,:id_usuario,:retorno, :statuspre, :mensagem, :carencia); ".
						   "END; ";   
  
					$sql->addParam(":contrato",$nnumetitu);
					$sql->addParam(":titular",$titular);
					$sql->addParam(":nome",$nome);
					$sql->addParam(":categoria",$categoria);
					$sql->addParam(":nascimento",$nascimento);
					$sql->addParam(":ufNasc",'PR');
					$sql->addParam(":cidadeNasc",'');
					$sql->addParam(":sexo",$sexo);
					$sql->addParam(":matricula",$matricula);					
					$sql->addParam(":mae",$mae);
					$sql->addParam(":pai",$pai);
					$sql->addParam(":cpf",$cpf);
					$sql->addParam(":rg",$rg);
					$sql->addParam(":orgao",$orgao);
					$sql->addParam(":pis",$pis);
					$sql->addParam(":grau",$grau);
					$sql->addParam(":locacao",$locacao);
					$sql->addParam(":admissao",$admissao);
					$sql->addParam(":endereco",$logradouro);
					$sql->addParam(":bairro",$bairro);
					$sql->addParam(":cidade",$cidade);
					$sql->addParam(":estado",$estado);
					$sql->addParam(":cep",$cep);
					$sql->addParam(":plano",$plano);
					$sql->addParam(":acomodacao",$acomodacao);            
					$sql->addParam(":telefone1",$telefone1);            
					$sql->addParam(":expedicao",$expedicao);            
					$sql->addParam(":numero",$numero);            
					$sql->addParam(":tipo_logradouro",$tipo_logradouro);            
					$sql->addParam(":complemento",$complemento);            
					$sql->addParam(":telefone2",$telefone2);            
					$sql->addParam(":celular",$celular);            
					$sql->addParam(":email",$email);            
					$sql->addParam(":salario",$salario);            
					$sql->addParam(":inclusao",$inclusao);
					$sql->addParam(":estado_civil",$estado_civil);
					$sql->addParam(":profissao",$profissao);
					$sql->addParam(":setor",$setor);               
					$sql->addParam(":local",'Z');
					$sql->addParam(":vendedor",$vendedor);             
					$sql->addParam(":statuspre",$statuspre);             
					$sql->addParam(":mensagem",$mensagem);             
					$sql->addParam(":carencia",$carencia);             
					
					if ($_SESSION['sistema'] <> 'Usuario')
					  $sql->addParam(":operador",$_SESSION['id_operador']);              
					else
					  $sql->addParam(":operador",'');              
					  
					$sql->addParam(":proposta",$id_proposta);              
					$sql->addParam(":valor",$valor);              
					$sql->addParam(":id_usuario",0,12);
					$sql->addParam(":csus",$csus);
					$sql->addParam(":ndnv",$ndnv);              
					$sql->addParam(":observacao",$observacao);
					$sql->addParam(":uniao",$uniao);
					$sql->addParam(":centro_custo",$centro_custo);
					$sql->addParam(":ip",getenv("REMOTE_ADDR"));
					$sql->addParam(":retorno",0,12);
					$erro = $sql->executeSQL($txt);														  
  
					$retorno['erro'] = $erro;
					$id_atcad = $sql->getReturn(":retorno");
					$retorno['id'] = $id_atcad;
					$id_usuario = $sql->getReturn(":id_usuario");
					$retorno['id_usuario'] = $id_usuario;
  
  
  					if($retorno['erro'] != ''){
						$txt = " DELETE FROM HSSTITU WHERE NNUMETITU = :contrato";
						$deleta = new Query($bd);
						$deleta->addParam(":contrato",$nnumetitu);
						$deleta->executeSQL($txt);
					}
					//Verificação de erros retornados do INSERE_USUARIO_WEB30
					if($id_atcad == -1)
					  $retorno['erro']='Não é possível cadastrar usuario com nome vazio';
					if($id_atcad == -2)
					  $retorno['erro']='não é possível cadastrar usuario sem data de nascimento';
					if($id_atcad == -3)
					  $retorno['erro']='Não é possível cadastrar usuario sem sexo';
					if($id_atcad == -4)
					  $retorno['erro']='Não é possível cadastrar usuario sem o nome da mãe ou pis';
            					         
					if ($retorno['erro'] == '') {					  
            $todos_anexos_name = array($anexo1["name"],$anexo2["name"],$anexo3["name"],$anexo4["name"]);
            $todos_anexos_tmp = array($anexo1["tmp_name"],$anexo2["tmp_name"],$anexo3["tmp_name"],$anexo4["tmp_name"]);
            $i = 0;            
            while($i < 4){//Quantidade de anexos atualmente
              if ($todos_anexos_name[$i] <> null){//anexo 
                $path = getcwd();    
                $util->criaDiretorio("anexos");
                $util->criaDiretorio($data->dataAtual('YYYY'));
                $util->criaDiretorio($_SESSION['codigo_contrato'] );  
                $dir = getcwd();                
                      
                $nome_arquivo = $todos_anexos_name[$i];
                $nome_arquivo = $util->corrigeNomeArquivo($nome_arquivo);
						
                $file =  str_replace('//','/',$dir).$id_usuario. "_".$nome_arquivo;
  
                if (move_uploaded_file($todos_anexos_tmp[$i], $file )) {
					  
                  $txt = "INSERT INTO HSSANEXO (NNUMEUSUA,CNOMEANEXO,DDATAANEXO,NSIZEANEXO)
                          VALUES (:id_usuario,:novoarquivo,SYSDATE,:tamanho)";
                  $sql->clear();
                  $sql->addParam(":id_usuario",$id_usuario);
                  $sql->addParam(":novoarquivo",$id_usuario."_".$nome_arquivo);
                  $sql->addParam(":tamanho",filesize($file));
                  $sql->executeSQL($txt);
                }
						
                if ($path <> '')
                  chdir($path);                              
                  
              }  
              $i = $i + 1;
            }            
					}
				  }
			  }
			}
		  }
//		}
      } else
        $retorno['erro'] = 'O tamanho do arquivo não pode ser superior a '.ini_get("upload_max_filesize") . '.';
          
      
        return $retorno;
      }   
	
	}
?>