<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd      = new Oracle();
  $util    = new Util();
  $func    = new Funcao();
  $formata = new Formata();
    
  if ($_SESSION['id_locacao'] > 0) {
    $arquivo = str_replace(' ','_',$func->retornaNomeLocacao($bd,$_SESSION['id_locacao'])."_".date('Ymdhis'));
    $arquivo = str_replace('/','',$arquivo);
  }
  else {
    $arquivo = str_replace(' ','_',$_SESSION['titular_contrato'])."_".date('Ymdhis');
    $arquivo = str_replace('/','',$arquivo);
  } 
  
  $arquivo = $util->corrigeNomeArquivo($arquivo);  
  $util->excluiArquivo("../temp",$arquivo.".csv");
  $ponteiro = fopen ("../temp/".$arquivo.".csv", "w");
  $delimitador = ';';
  
  $id_pagamento = $_POST['id_pagamento'];
  $tipo         = $_POST['tipo'];  
  
  $sql = new Query($bd);
  $txt = "SELECT CLFATTITU,TO_CHAR(HSSPAGA.DVENCPAGA,'DD/MM/YYYY') DVENCPAGA
            FROM HSSPAGA, HSSTITU
           WHERE NNUMEPAGA = :id_pagamento
             AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU ";
  $sql->clear();
  $sql->addParam(":id_pagamento",$id_pagamento);
  $sql->executeQuery($txt);
  
  $vencimento = $sql->result("DVENCPAGA");
    
  // Fatura Agemed 
  if (($sql->result("CLFATTITU") == ",") or
      ($sql->result("CLFATTITU") == utf8_decode("�")) or
      ($sql->result("CLFATTITU") == "]") or
      ($sql->result("CLFATTITU") == "`") or
      ($sql->result("CLFATTITU") == "<")) {   
      
    $txt = "SELECT HSSUSUA.CCODIUSUA,HSSUSUA.CNOMEUSUA,TITULAR.CNOMEUSUA NOME_TITULAR,C_CGCEMPR,HSSUSUA.DNASCUSUA,HSSUSUA.CGRAUUSUA,
                   CCODITITU,CDESCPLAN,CCODIPLAN,NVL(HSSUSUA.CCHAPUSUA,TITULAR.CCHAPUSUA) CCHAPUSUA,HSSUSUA.CCARTUSUA,HSSUSUA.CNOMEUSUA,
                   DECODE(HSSUSUA.C_CPFUSUA,NULL,(CASE WHEN IDADE_USUARIO(HSSUSUA.NNUMEUSUA) < 18 THEN TITULAR.C_CPFUSUA ELSE HSSUSUA.C_CPFUSUA END),HSSUSUA.C_CPFUSUA) C_CPFUSUA,
                   HSSUSUA.C__RGUSUA,HSSUSUA.CNMAEUSUA,HSSUSUA.DINCLUSUA,HSSUSUA.DSITUUSUA,HSSUSUA.CSEXOUSUA,
                   HSSUSUA.CESTCUSUA,NVLIQUSUPG,NVLIQUSUPG * 100 NVLIQUSUPG2,NADESUSUPG,NADESUSUPG * 100 NADESUSUPG2,CRAZAEMPR,NDOCUPAGA,CCOMPPAGA,HSSUSUA.NNUMEUSUA,HSSUSUA.CTIPOUSUA,HSSTITU.NNUMETITU,HSSUSUA.NNUMEUSUA,HSSUSUA.NTITUUSUA,DECODE(HSSUSUA.CTIPOUSUA,'T',HSSUSUA.CTIPOUSUA,'D') CTIPOUSUA2
              FROM HSSPAGA,HSSUSUPG,HSSUSUA,HSSUSUA TITULAR,HSSTITU,HSSEMPR,HSSPLAN
             WHERE HSSPAGA.NNUMEPAGA = :id_pagamento
               AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
               AND HSSUSUA.NNUMEUSUA = HSSUSUPG.NNUMEUSUA
               AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
            UNION
            SELECT HSSUSUA.CCODIUSUA,HSSUSUA.CNOMEUSUA,TITULAR.CNOMEUSUA NOME_TITULAR,C_CGCEMPR,HSSUSUA.DNASCUSUA,HSSUSUA.CGRAUUSUA,
                   CCODITITU,CDESCPLAN,CCODIPLAN,NVL(HSSUSUA.CCHAPUSUA,TITULAR.CCHAPUSUA) CCHAPUSUA,HSSUSUA.CCARTUSUA,HSSUSUA.CNOMEUSUA,
                   DECODE(HSSUSUA.C_CPFUSUA,NULL,(CASE WHEN IDADE_USUARIO(HSSUSUA.NNUMEUSUA) < 18 THEN TITULAR.C_CPFUSUA ELSE HSSUSUA.C_CPFUSUA END),HSSUSUA.C_CPFUSUA) C_CPFUSUA, 
                   HSSUSUA.C__RGUSUA,HSSUSUA.CNMAEUSUA,HSSUSUA.DINCLUSUA,HSSUSUA.DSITUUSUA,HSSUSUA.CSEXOUSUA,
                   HSSUSUA.CESTCUSUA,NULL,NULL,NULL,NULL,CRAZAEMPR,NDOCUPAGA,CCOMPPAGA,HSSUSUA.NNUMEUSUA,HSSUSUA.CTIPOUSUA,HSSTITU.NNUMETITU,HSSUSUA.NNUMEUSUA,HSSUSUA.NTITUUSUA,DECODE(HSSUSUA.CTIPOUSUA,'T',HSSUSUA.CTIPOUSUA,'D') CTIPOUSUA2
              FROM HSSPAGA,HSSPACON,HSSUSUA,HSSUSUA TITULAR,HSSTITU,HSSEMPR,HSSPLAN,HSSCONT
             WHERE HSSPAGA.NNUMEPAGA = :id_pagamento
               AND HSSUSUA.NNUMEUSUA NOT IN (SELECT NNUMEUSUA FROM HSSUSUPG WHERE NNUMEPAGA = :id_pagamento)
               AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
               AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
               AND HSSUSUA.NNUMEUSUA = HSSCONT.NNUMEUSUA
               AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
             ORDER BY 3, 2, 1 ";
           
    $sql->clear();
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->executeQuery($txt);
    
    function CampoOpcional($campo,$contrato) {
      if (($contrato == 82719634) or ($contrato == 82726415) or ($contrato == 73569524) or ($contrato == 4463)) // Tecnofibras e Fortlev
        return $campo.';';
      else
        return '';
    }    
    
    function ColunaOpcional($coluna,$contrato) {
      if (($contrato == 82719634) or ($contrato == 82726415) or ($contrato == 73569524) or ($contrato == 4463))
        return $coluna.';';
      else
        return '';
    } 

    function PontoVirgulaOpcional($contrato) {
      if (($contrato == 82719634) or ($contrato == 82726415) or ($contrato == 73569524) or ($contrato == 4463))
        return ';';
      else
        return '';
    }    
    
    
    function CampoOpcional2($campo,$empresa) {
      if (substr($empresa,0,8) == 'MAX MOHR')
        return $campo.';';
      else
        return '';
    }    
    
    function ColunaOpcional2($coluna,$empresa) {
      if (substr($empresa,0,8) == 'MAX MOHR')
        return $coluna.';';
      else
        return '';
    } 

    function PontoVirgulaOpcional2($empresa) {
      if (substr($empresa,0,8) == 'MAX MOHR')
        return ';';
      else
        return '';
    }    
      
    $sql1 = new Query($bd);
    $sql2 = new Query($bd);
    $txtAditivo = "SELECT NVALOADTPG,CDESCTXMEN FROM HSSADTPG,HSSTXMEN
                    WHERE NNUMEPAGA = :id_pagamento
                      AND NNUMEUSUA = :id_usuario
                      AND NVALOADTPG <> 0
                      AND HSSADTPG.NNUMETXMEN = HSSTXMEN.NNUMETXMEN
                    UNION ALL
                   SELECT NVALOCPAGA,'CARTEIRINHA' FROM HSSCPAGA, HSSCARTE
                    WHERE NNUMEPAGA = :id_pagamento
                      AND NVALOCPAGA <> 0
                      AND HSSCPAGA.NNUMECARTE = HSSCARTE.NNUMECARTE
                      AND NNUMEUSUA = :id_usuario ";
    
    $txtDespesa = "SELECT HSSCONT.NNUMECONT,DATENCONT,HSSPCON.CCODIPMED,CNOMEPMED,NQUANPCON, 0 NNUMETAXA,
                          HSSCONT.NNUMEUSUA,PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRANPCON,CTIPOPMED,CAMBUCONT,CURGECONT,NINSSPACON,
                          NTHONPCON + NTCOPPCON + NTFILPCON + NTANEPCON + NTPRIPCON + NTSEGPCON + NTTERPCON + NTQUAPCON + NTINSPCON + NTSALPCON TAXA_ADM, NLOCAPRES LOCAL
                     FROM HSSPACON,HSSCONT,HSSPCON,HSSPMED,FINPRES PRES,FINPRES SOLI
                    WHERE HSSPACON.NNUMEPAGA = :id_pagamento
                      AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                      AND HSSCONT.NNUMEUSUA = :id_usuario
                      AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                      AND NFRANPCON <> 0
                      AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                      AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES(+)
                      AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
                    UNION ALL
                   SELECT HSSCONT.NNUMECONT,DATENCONT,CCODITAXA,CDESCTAXA,NQUANTCON, HSSTAXA.NNUMETAXA NNUMETAXA,
                          HSSCONT.NNUMEUSUA,PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRANTCON,'Taxas',CAMBUCONT,CURGECONT,NINSSPACON,
                          NTADMTCON, NLOCAPRES LOCAL
                     FROM HSSPACON,HSSCONT,HSSTCON,HSSTAXA,FINPRES PRES,FINPRES SOLI
                    WHERE NNUMEPAGA = :id_pagamento
                      AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                      AND HSSCONT.NNUMEUSUA = :id_usuario
                      AND HSSCONT.NNUMECONT = HSSTCON.NNUMECONT
                      AND NFRANTCON <> 0
                      AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                      AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES(+)
                      AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
                    UNION ALL
                   SELECT HSSCONT.NNUMECONT,DATENCONT,TO_CHAR(NULL),'MAT/MED',TO_NUMBER(NULL), 0 NNUMETAXA, HSSCONT.NNUMEUSUA,
                          PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRMMCONT,'Mat/Med',CAMBUCONT,CURGECONT,NINSSPACON,0, NLOCAPRES LOCAL
                     FROM HSSPACON,HSSCONT,FINPRES PRES,FINPRES SOLI
                    WHERE NNUMEPAGA = :id_pagamento
                      AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
                      AND HSSCONT.NNUMEUSUA = :id_usuario
                      AND NFRMMCONT <> 0
                      AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES(+)
                      AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
                    UNION ALL
                   SELECT HSSGUIA.NNUMEGUIA,TRUNC(DEMISGUIA),HSSPGUI.CCODIPMED,CNOMEPMED,NQUANPGUI, 0 NNUMETAXA, HSSGUIA.NNUMEUSUA,
                          PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRANPGUI,CTIPOPMED,CTIPOGUIA,CURGEGUIA,0, 0, NLOCAPRES LOCAL
                     FROM HSSGPAGA,HSSGUIA,HSSPGUI,HSSPMED,FINPRES PRES,FINPRES SOLI
                    WHERE NNUMEPAGA = :id_pagamento
                      AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                      AND HSSGUIA.NNUMEUSUA = :id_usuario
                      AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
                      AND NFRANPGUI <> 0
                      AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                      AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
                      AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)
                    UNION ALL
                   SELECT HSSGUIA.NNUMEGUIA,TRUNC(DEMISGUIA),CCODITAXA,CDESCTAXA,NQUANDGUI, HSSTAXA.NNUMETAXA, HSSGUIA.NNUMEUSUA,
                          PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NCOPADGUI,'Taxas',CTIPOGUIA,CURGEGUIA,0,0, NLOCAPRES LOCAL
                     FROM HSSGPAGA,HSSGUIA,HSSDGUI,HSSTAXA,FINPRES PRES,FINPRES SOLI
                    WHERE NNUMEPAGA = :id_pagamento
                      AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                      AND HSSGUIA.NNUMEUSUA = :id_usuario
                      AND HSSGUIA.NNUMEGUIA = HSSDGUI.NNUMEGUIA
                      AND NCOPADGUI <> 0
                      AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                      AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
                      AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)
                    UNION ALL
                   SELECT HSSGUIA.NNUMEGUIA,TRUNC(DEMISGUIA),TO_CHAR(NULL),CDESCPACOT,NQUANPCGUI,0 NNUMETAXA, HSSGUIA.NNUMEUSUA,
                          PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NCOPAPCGUI,'Pacotes',CTIPOGUIA,CURGEGUIA,0,0, HSSGUIA.NLOCAPRES LOCAL 
                     FROM HSSGPAGA,HSSGUIA,HSSPCGUI,HSSPACOT,FINPRES PRES,FINPRES SOLI
                    WHERE NNUMEPAGA = :id_pagamento
                      AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                      AND HSSGUIA.NNUMEUSUA = :id_usuario
                      AND HSSGUIA.NNUMEGUIA = HSSPCGUI.NNUMEGUIA
                      AND NCOPAPCGUI <> 0
                      AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT
                      AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
                      AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)
                    ORDER BY 1 ";
                  
    $txtAcrDesc = "SELECT NNUMEPAGA,CMOTIADPAG,NVALOADPAG FROM HSSADPAG
                    WHERE NNUMEPAGA = :id_pagamento ";
                   
    $linha = "CNPJ".$delimitador.
             "Contrato".$delimitador.
             "Plano � Descri��o".$delimitador.
             "Matricula".$delimitador.
             "Carteira".$delimitador.
             "Nome".$delimitador.
             ColunaOpcional("Id Usu�rio",$sql->result("NNUMETITU")).
             ColunaOpcional("Tipo Usu�rio",$sql->result("NNUMETITU")).             
             "Dt de Nasc".$delimitador.
             CampoOpcional2("Tipo Usu�rio",$sql->result("CRAZAEMPR")).
             CampoOpcional2("CPF do Titular",$sql->result("CRAZAEMPR")).             
             "CPF".$delimitador.
             "RG".$delimitador.
             "Titularidade".$delimitador.
             ColunaOpcional("Id Titular",$sql->result("NNUMETITU")).             
             "Nome da M�e".$delimitador.
             "Dt da Inclus�o".$delimitador.
             "Dt Exclus�o".$delimitador.
             "Sexo".$delimitador.
             "Estado Civil".$delimitador.
             "Vlr Mensalidade".$delimitador.
             "Tx Inscr".$delimitador.
             "Nr da Fatura".$delimitador.
             "Compet�ncia".$delimitador.
             "Executante".$delimitador.
             "Solicitante".$delimitador.
             "Dt da realiza��o".$delimitador.
             "N� Guia".$delimitador.
             "Cd Produto".$delimitador.
             "Descri��o".$delimitador.
             "Qtd".$delimitador.
             "Vl Benefici�rio".$delimitador.
             "Vl Empresa".$delimitador.
             "Vl tx sobre servi�o".$delimitador.
             "INSS Credenc. (PF 20%)".$delimitador.
             "Plano � C�digo".$delimitador.
             ColunaOpcional("Evento",$sql->result("NNUMETITU"));             
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
    while (!$sql->eof()) {
                         
      $linha = $sql->result("C_CGCEMPR").$delimitador. 
               $sql->result("CCODITITU").$delimitador. 
               $sql->result("CDESCPLAN").$delimitador. 
               $sql->result("CCHAPUSUA").$delimitador. 
               $sql->result("CCARTUSUA").$delimitador. 
               $sql->result("CNOMEUSUA").$delimitador. 
               CampoOpcional($sql->result("NNUMEUSUA"),$sql->result("NNUMETITU")).
               CampoOpcional($sql->result("CTIPOUSUA"),$sql->result("NNUMETITU")).               
               $sql->result("DNASCUSUA").$delimitador. 
               CampoOpcional2($sql->result("CTIPOUSUA2"),$sql->result("CRAZAEMPR")).
               CampoOpcional2($sql->result("CPF_TITU"),$sql->result("CRAZAEMPR")).               
               $sql->result("C_CPFUSUA").$delimitador. 
               $sql->result("C__RGUSUA").$delimitador. 
               $sql->result("NOME_TITULAR").$delimitador. 
               CampoOpcional($sql->result("NTITUUSUA"),$sql->result("NNUMETITU")).               
               $sql->result("CNMAEUSUA").$delimitador. 
               $sql->result("DINCLUSUA").$delimitador. 
               $sql->result("DSITUUSUA").$delimitador. 
               $sql->result("CSEXOUSUA").$delimitador. 
               $sql->result("CESTCUSUA").$delimitador; 
      if (($_SESSION['id_contrato'] == 87955601) or ($_SESSION['id_contrato'] == 87938935) or ($_SESSION['id_contrato'] == 87943075) or ($_SESSION['id_contrato'] == 87945797)) // COTEMINAS 			   
        $linha = $linha.$sql->result("NVLIQUSUPG2").$delimitador.
		             $sql->result("NADESUSUPG2").$delimitador;
      else		
		    $linha = $linha.$sql->result("NVLIQUSUPG").$delimitador.
		             $sql->result("NADESUSUPG").$delimitador;
                 
      $Vl_Valor_Usuario = $sql->result("NVLIQUSUPG");
      $Vl_Valor_Empresa = 0;
      $sql_perc_emp = new Query();
      $txt_perc_emp =  "SELECT PART_EMPR
                         FROM FATURA_EMPRESA
                        WHERE NNUMETITU = :Contrato
                         AND NNUMEUSUA = :usuario ";
      $sql_perc_emp->addParam(":contrato",$sql->result("NNUMETITU"));
      $sql_perc_emp->addParam(":usuario",$sql->result("NNUMEUSUA"));
      $sql_perc_emp->executeQuery($txt_perc_emp); 

      if ($sql_perc_emp->result("PART_EMPR") > 0) {
        $Vl_Valor_Empresa = (str_replace(',','.',$Vl_Valor_Usuario) * str_replace(',','.',$sql_perc_emp->result("PART_EMPR")));   //Valor Empresa
        $Vl_Valor_Empresa = $Vl_Valor_Empresa / 100; 
      }
      
      $Vl_Valor_Usuario = (str_replace(',','.',$Vl_Valor_Usuario) - str_replace(',','.',$Vl_Valor_Empresa));  //Valor real do Usuario                    
                 
                 
      $linha = $linha.                 
               $sql->result("NDOCUPAGA").$delimitador. 
               $sql->result("CCOMPPAGA").$delimitador. 
               "".$delimitador. 
               "".$delimitador. 
               "".$delimitador. 
               "".$delimitador. 
               "".$delimitador. 
               "".$delimitador. 
               "".$delimitador. 
               formata_numero($Vl_Valor_Usuario).$delimitador.
               formata_numero($Vl_Valor_Empresa).$delimitador.
               "".$delimitador. 
               "".$delimitador. 
               $sql->result("CCODIPLAN").$delimitador;      
      fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
      $sql1->clear();
      $sql1->addParam(":id_pagamento",$id_pagamento);
      $sql1->addParam(":id_usuario",$sql->result("NNUMEUSUA"));
      $sql1->executeQuery($txtAditivo);
      
      while (!$sql1->eof()) {
        $linha = $sql->result("C_CGCEMPR").$delimitador.
                 $sql->result("CCODITITU").$delimitador.
                 $sql->result("CDESCPLAN").$delimitador.
                 $sql->result("CCHAPUSUA").$delimitador.
                 $sql->result("CCARTUSUA").$delimitador.
                 $sql->result("CNOMEUSUA").$delimitador.
                 CampoOpcional($sql->result("NNUMEUSUA"),$sql->result("NNUMETITU")).
                 CampoOpcional($sql->result("CTIPOUSUA"),$sql->result("NNUMETITU")).                    
                 $sql->result("DNASCUSUA").$delimitador.
                 CampoOpcional2($sql->result("CTIPOUSUA2"),$sql->result("CRAZAEMPR")).
                 CampoOpcional2($sql->result("CPF_TITU"),$sql->result("CRAZAEMPR")).                   
                 $sql->result("C_CPFUSUA").$delimitador.
                 $sql->result("C__RGUSUA").$delimitador.
                 $sql->result("NOME_TITULAR").$delimitador.
                 CampoOpcional($sql->result("NTITUUSUA"),$sql->result("NNUMETITU")).                
                 $sql->result("CNMAEUSUA").$delimitador.
                 $sql->result("DINCLUSUA").$delimitador.
                 $sql->result("DSITUUSUA").$delimitador.
                 $sql->result("CSEXOUSUA").$delimitador.
                 $sql->result("CESTCUSUA").$delimitador.
                 "".$delimitador.
                 "".$delimitador.
                 $sql->result("NDOCUPAGA").$delimitador.
                 $sql->result("CCOMPPAGA").$delimitador.
                 "".$delimitador.
                 "".$delimitador.
                 "".$delimitador.
                 "".$delimitador.
                 "".$delimitador.
                 $sql1->result("CDESCTXMEN").$delimitador.
                 "1".$delimitador.
                 "0".$delimitador.
                 $sql1->result("NVALOADTPG").$delimitador.
                 "".$delimitador.
                 "".$delimitador.
                 $sql->result("CCODIPLAN").$delimitador;
        fwrite($ponteiro,$linha.chr(13).chr(10)); 
        
        $sql1->next();
      }
      
      $sql1->clear();
      $sql1->addParam(":id_pagamento",$id_pagamento);
      $sql1->addParam(":id_usuario",$sql->result("NNUMEUSUA"));
      $sql1->executeQuery($txtDespesa);
      while (!$sql1->eof()) {
        $txt1 = "SELECT PERCENTUALBENEFICIARIO(:Contrato, :Categoria, :Natureza, :Regime, :Tipo, :Taxa, :Codigo, :Nome, :Vencimento, :Local) PERC
                   FROM DUAL ";
        $sql2->clear();
        $sql2->addParam(":Contrato"   ,$sql->result("NNUMETITU"));
        $sql2->addParam(":Categoria"  ,$sql->result("CTIPOUSUA"));
        $sql2->addParam(":Natureza"   ,$sql1->result("CAMBUCONT"));
        $sql2->addParam(":Regime"     ,$sql1->result("CURGECONT"));
        $sql2->addParam(":Tipo"       ,$sql1->result("CTIPOPMED"));
        $sql2->addParam(":Taxa"       ,$sql1->result("NNUMETAXA"));
        $sql2->addParam(":Codigo"     ,$sql1->result("CCODIPMED"));
        $sql2->addParam(":Nome"       ,$sql1->result("CNOMEPMED"));
        $sql2->addParam(":Vencimento" ,$vencimento);   
        $sql2->addParam(":Local"      ,$sql1->result("LOCAL"));        
        $sql2->executeQuery($txt1);
        
        $PercentualFuncionario = 0;
        $PercentualFuncionario = $sql2->result("PERC");
                
        if (str_replace(',','.',$PercentualFuncionario) > 0) {
          $VlBeneficiario = (str_replace(',','.',$sql1->result("NFRANPCON")) * str_replace(',','.',$PercentualFuncionario));
          $VlBeneficiario = round($VlBeneficiario,2);
          $ValorEmpresa = (str_replace(',','.',$sql1->result("NFRANPCON")) - str_replace(',','.',$VlBeneficiario));
        }
        else {
          $VlBeneficiario = 0;
          $ValorEmpresa = $sql1->result("NFRANPCON");
        }
                  
        $linha = $sql->result("C_CGCEMPR").$delimitador.
                 $sql->result("CCODITITU").$delimitador.
                 $sql->result("CDESCPLAN").$delimitador.
                 $sql->result("CCHAPUSUA").$delimitador.
                 $sql->result("CCARTUSUA").$delimitador.
                 $sql->result("CNOMEUSUA").$delimitador.
                 CampoOpcional($sql->result("NNUMEUSUA"),$sql->result("NNUMETITU")).
                 CampoOpcional($sql->result("CTIPOUSUA"),$sql->result("NNUMETITU")).                 
                 $sql->result("DNASCUSUA").$delimitador.
                 CampoOpcional2($sql->result("CTIPOUSUA2"),$sql->result("CRAZAEMPR")).
                 CampoOpcional2($sql->result("CPF_TITU"),$sql->result("CRAZAEMPR")).                   
                 $sql->result("C_CPFUSUA").$delimitador.
                 $sql->result("C__RGUSUA").$delimitador.
                 $sql->result("NOME_TITULAR").$delimitador.
                 CampoOpcional($sql->result("NTITUUSUA"),$sql->result("NNUMETITU")).                 
                 $sql->result("CNMAEUSUA").$delimitador.
                 $sql->result("DINCLUSUA").$delimitador.
                 $sql->result("DSITUUSUA").$delimitador.
                 $sql->result("CSEXOUSUA").$delimitador.
                 $sql->result("CESTCUSUA").$delimitador.
                 $sql->result("").$delimitador.
                 $sql->result("").$delimitador.
                 $sql->result("NDOCUPAGA").$delimitador.
                 $sql->result("CCOMPPAGA").$delimitador.
                 $sql1->result("EXECUTANTE").$delimitador.
                 $sql1->result("SOLICITANTE").$delimitador.
                 $sql1->result("DATENCONT").$delimitador.
                 $sql1->result("NNUMECONT").$delimitador.
                 $sql1->result("CCODIPMED").$delimitador.
                 $sql1->result("CNOMEPMED").$delimitador.
                 "1".$delimitador.
                 $formata->formataNumero($VlBeneficiario).$delimitador.
                 $formata->formataNumero($ValorEmpresa).$delimitador.
                 $sql1->result("TAXA_ADM").$delimitador.
                 "0".$delimitador;
                 if (($sql->result("NNUMETITU") == 82719634) or ($sql->result("NNUMETITU") == 82726415) or ($contrato == 73569524) or ($contrato == 4463)) { // TecnoFibras e Fortlev
                   $linha = $linha.$sql->result("CCODIPLAN").$delimitador;
                   if ($sql1->result("CIACICONT") == '0') 
                      $linha = $linha."40".$delimitador; 
                   else {
                     if ($sql1->result("CTIPOPMED") == 'Taxas') 
                       $linha = $linha."50".$delimitador;
                     else if ($sql1->result("CTIPOPMED") == 'Mat/Med')  
                       $linha = $linha."70".$delimitador;
                     else {
                       $sql_cl = new Query($bd);
                       $txt_cl = "SELECT NSUBCCLPRO
                                 FROM HSSPMED, HSSCLPRO
                                WHERE HSSPMED.CCODIPMED = :Procedimento
                                  AND HSSPMED.NNUMECLPRO = HSSCLPRO.NNUMECLPRO(+) ";
                       $sql_cl->clear();
                       $sql_cl->addParam(":Procedimento",$sql1->result("CCODIPMED"));
                       $sql_cl->executeQuery($txt_cl);     

                       $linha = $linha.$sql_cl->result("NSUBCCLPRO").$delimitador;                       
                     } 
                   }                     
                 }
                 else
                   $linha = $linha.$sql->result("CCODIPLAN").$delimitador;
                   
        fwrite($ponteiro,$linha.chr(13).chr(10)); 
               
        $Conta = $sql1->result("NNUMECONT");
        $Inss  = $sql1->result("NINSSPACON");
        
        $inss_executante  = $sql1->result("EXECUTANTE");
        $inss_solicitante = $sql1->result("SOLICITANTE");
        $inss_data_conta  = $sql1->result("DATENCONT");
        $inss_conta       = $sql1->result("NNUMECONT");
                   
        $sql1->next();
        
        if ((($Conta <> $sql1->result("NNUMECONT")) or ($sql1->eof())) and (str_replace(',','.',$Inss) > 0)) {
          if ((str_replace(',','.',$PercentualFuncionario)) > 0) {
            $VlBeneficiario = (str_replace(',','.',$Inss) * str_replace(',','.',$PercentualFuncionario));
            $ValorEmpresa = (str_replace(',','.',$Inss) - str_replace(',','.',$VlBeneficiario));
          }
          else {
            $VlBeneficiario = 0;
            $ValorEmpresa = $Inss;
          }
        
          $linha = $sql->result("C_CGCEMPR").$delimitador.
                   $sql->result("CCODITITU").$delimitador.
                   $sql->result("CDESCPLAN").$delimitador.
                   $sql->result("CCHAPUSUA").$delimitador.
                   $sql->result("CCARTUSUA").$delimitador.
                   $sql->result("CNOMEUSUA").$delimitador.
                   CampoOpcional($sql->result("NNUMEUSUA"),$sql->result("NNUMETITU")).
                   CampoOpcional($sql->result("CTIPOUSUA"),$sql->result("NNUMETITU")).                     
                   $sql->result("DNASCUSUA").$delimitador.
                   CampoOpcional2($sql->result("CTIPOUSUA2"),$sql->result("CRAZAEMPR")).
                   CampoOpcional2($sql->result("CPF_TITU"),$sql->result("CRAZAEMPR")).                      
                   $sql->result("C_CPFUSUA").$delimitador.
                   $sql->result("C__RGUSUA").$delimitador.
                   $sql->result("NOME_TITULAR").$delimitador.
                   CampoOpcional($sql->result("NTITUUSUA"),$sql->result("NNUMETITU")).                   
                   $sql->result("CNMAEUSUA").$delimitador.
                   $sql->result("DINCLUSUA").$delimitador.
                   $sql->result("DSITUUSUA").$delimitador.
                   $sql->result("CSEXOUSUA").$delimitador.
                   $sql->result("CESTCUSUA").$delimitador.
                   "".$delimitador.
                   "".$delimitador.
                   $sql->result("NDOCUPAGA").$delimitador.
                   $sql->result("CCOMPPAGA").$delimitador.
                   $inss_executante.$delimitador.
                   $inss_solicitante.$delimitador.
                   $inss_data_conta.$delimitador.
                   $inss_conta.$delimitador.
                   "".$delimitador.
                   "INSS PRESTADOR".$delimitador.
                   "1".$delimitador.
                   $formata->formataNumero($VlBeneficiario).$delimitador.
                   $formata->formataNumero($ValorEmpresa).$delimitador.
                   "0".$delimitador.
                   $Inss.$delimitador.
                   $sql->result("CCODIPLAN").$delimitador;
          fwrite($ponteiro,$linha.chr(13).chr(10));           
        }
      }
     $sql->next();
    }

    $sql1->clear();
    $sql1->addParam(":id_pagamento",$id_pagamento);
    $sql1->executeQuery($txtAcrDesc);
    while (!$sql1->eof()) {
      $linha = $sql->result("C_CGCEMPR").$delimitador.
               $sql->result("CCODITITU").$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               PontoVirgulaOpcional($sql->result("NNUMETITU")).
               PontoVirgulaOpcional($sql->result("NNUMETITU")).
               "".$delimitador.
               PontoVirgulaOpcional2($sql->result("CRAZAEMPR")).
               PontoVirgulaOpcional2($sql->result("CRAZAEMPR")).               
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               PontoVirgulaOpcional($sql->result("NNUMETITU")).
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               $sql->result("NDOCUPAGA").$delimitador.
               $sql->result("CCOMPPAGA").$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               str_replace(';',' ',$sql1->result("CMOTIADPAG")).$delimitador.
               "1".$delimitador.
               $sql1->result("NVALOADPAG").$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador.
               "".$delimitador;
          
      fwrite($ponteiro,$linha.chr(13).chr(10));       
      $sql1->next();
    }
     
    fclose ($ponteiro);  
  }

  // Fatura Agemed Franke
  if ($sql->result("CLFATTITU") == ";") {   
  
    $txt = "SELECT HSSUSUA.CCODIUSUA,HSSUSUA.CNOMEUSUA,TITULAR.CNOMEUSUA NOME_TITULAR,C_CGCEMPR,HSSUSUA.DNASCUSUA,HSSUSUA.CGRAUUSUA,
				   CCODITITU,CDESCPLAN,CCODIPLAN,NVL(HSSUSUA.CCHAPUSUA,TITULAR.CCHAPUSUA) CCHAPUSUA,HSSUSUA.CCARTUSUA,HSSUSUA.CNOMEUSUA,
				   DECODE(HSSUSUA.C_CPFUSUA,NULL,(CASE WHEN IDADE_USUARIO(HSSUSUA.NNUMEUSUA) < 18 THEN TITULAR.C_CPFUSUA ELSE HSSUSUA.C_CPFUSUA END),HSSUSUA.C_CPFUSUA) C_CPFUSUA,
				   HSSUSUA.C__RGUSUA,HSSUSUA.CNMAEUSUA,HSSUSUA.DINCLUSUA,HSSUSUA.DSITUUSUA,HSSUSUA.CSEXOUSUA,
				   HSSUSUA.CESTCUSUA,NVLIQUSUPG,NADESUSUPG,CRAZAEMPR,NDOCUPAGA,CCOMPPAGA,HSSUSUA.NNUMEUSUA,HSSUSUA.CTIPOUSUA
			  FROM HSSPAGA,HSSUSUPG,HSSUSUA,HSSUSUA TITULAR,HSSTITU,HSSEMPR,HSSPLAN
			 WHERE HSSPAGA.NNUMEPAGA = :id_pagamento
			   AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
			   AND HSSUSUA.NNUMEUSUA = HSSUSUPG.NNUMEUSUA
			   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
			   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
			   AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
			   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
			 UNION
			SELECT HSSUSUA.CCODIUSUA,HSSUSUA.CNOMEUSUA,TITULAR.CNOMEUSUA NOME_TITULAR,C_CGCEMPR,HSSUSUA.DNASCUSUA,HSSUSUA.CGRAUUSUA,
				   CCODITITU,CDESCPLAN,CCODIPLAN,NVL(HSSUSUA.CCHAPUSUA,TITULAR.CCHAPUSUA) CCHAPUSUA,HSSUSUA.CCARTUSUA,HSSUSUA.CNOMEUSUA,
				   DECODE(HSSUSUA.C_CPFUSUA,NULL,(CASE WHEN IDADE_USUARIO(HSSUSUA.NNUMEUSUA) < 18 THEN TITULAR.C_CPFUSUA ELSE HSSUSUA.C_CPFUSUA END),HSSUSUA.C_CPFUSUA) C_CPFUSUA, 
				   HSSUSUA.C__RGUSUA,HSSUSUA.CNMAEUSUA,HSSUSUA.DINCLUSUA,HSSUSUA.DSITUUSUA,HSSUSUA.CSEXOUSUA,
				   HSSUSUA.CESTCUSUA,NULL,NULL,CRAZAEMPR,NDOCUPAGA,CCOMPPAGA,HSSUSUA.NNUMEUSUA,HSSUSUA.CTIPOUSUA
			  FROM HSSPAGA,HSSPACON,HSSUSUA,HSSUSUA TITULAR,HSSTITU,HSSEMPR,HSSPLAN,HSSCONT
			 WHERE HSSPAGA.NNUMEPAGA = :id_pagamento
			   AND HSSUSUA.NNUMEUSUA NOT IN (SELECT NNUMEUSUA FROM HSSUSUPG WHERE NNUMEPAGA = :id_pagamento)
			   AND HSSPAGA.NNUMEPAGA = HSSPACON.NNUMEPAGA
			   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
			   AND HSSUSUA.NNUMEUSUA = HSSCONT.NNUMEUSUA
			   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
			   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
			   AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
			   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
       ORDER BY 3, 2, 1 ";
           
    $sql->clear();
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->executeQuery($txt);
      
    $sql1 = new Query($bd);
    
    $txtDespesa =  "SELECT HSSCONT.NNUMECONT,DATENCONT,HSSPCON.CCODIPMED,CNOMEPMED,NQUANPCON, 0 NNUMETAXA,
						   HSSCONT.NNUMEUSUA,PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRANPCON,CTIPOPMED,CAMBUCONT,CURGECONT,NINSSPACON,
						   NTHONPCON + NTCOPPCON + NTFILPCON + NTANEPCON + NTPRIPCON + NTSEGPCON + NTTERPCON + NTQUAPCON + NTINSPCON + NTSALPCON TAXA_ADM
					  FROM HSSPACON,HSSCONT,HSSPCON,HSSPMED,FINPRES PRES,FINPRES SOLI
					 WHERE HSSPACON.NNUMEPAGA = :Id
					   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
					   AND HSSCONT.NNUMEUSUA = :Usuario
					   AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
					   AND NFRANPCON <> 0
					   AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
					   AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES(+)
					   AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
					 UNION ALL
					SELECT HSSCONT.NNUMECONT,DATENCONT,CCODITAXA,CDESCTAXA,NQUANTCON, HSSTAXA.NNUMETAXA NNUMETAXA,
						   HSSCONT.NNUMEUSUA,PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRANTCON,'Taxas',CAMBUCONT,CURGECONT,NINSSPACON,
						   NTADMTCON
					  FROM HSSPACON,HSSCONT,HSSTCON,HSSTAXA,FINPRES PRES,FINPRES SOLI
					 WHERE NNUMEPAGA = :Id
					   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
					   AND HSSCONT.NNUMEUSUA = :Usuario
					   AND HSSCONT.NNUMECONT = HSSTCON.NNUMECONT
					   AND NFRANTCON <> 0
					   AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
					   AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES(+)
					   AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
					 UNION ALL
					SELECT HSSCONT.NNUMECONT,DATENCONT,TO_CHAR(NULL),'MAT/MED',TO_NUMBER(NULL),HSSCONT.NNUMEUSUA, 0 NNUMETAXA,
						   PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRMMCONT,'Mat/Med',CAMBUCONT,CURGECONT,NINSSPACON,0
					  FROM HSSPACON,HSSCONT,FINPRES PRES,FINPRES SOLI
					 WHERE NNUMEPAGA = :Id
					   AND HSSPACON.NNUMECONT = HSSCONT.NNUMECONT
					   AND HSSCONT.NNUMEUSUA = :Usuario
					   AND NFRMMCONT <> 0
					   AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES(+)
					   AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES(+)
					 UNION ALL
					SELECT HSSGUIA.NNUMEGUIA,TRUNC(DEMISGUIA),HSSPGUI.CCODIPMED,CNOMEPMED,NQUANPGUI,HSSGUIA.NNUMEUSUA, 0 NNUMETAXA,
						   PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NFRANPGUI,CTIPOPMED,CTIPOGUIA,CURGEGUIA,0, 0
					  FROM HSSGPAGA,HSSGUIA,HSSPGUI,HSSPMED,FINPRES PRES,FINPRES SOLI
					 WHERE NNUMEPAGA = :Id
					   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
					   AND HSSGUIA.NNUMEUSUA = :Usuario
					   AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
					   AND NFRANPGUI <> 0
					   AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
					   AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
					   AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)
					 UNION ALL
					SELECT HSSGUIA.NNUMEGUIA,TRUNC(DEMISGUIA),CCODITAXA,CDESCTAXA,NQUANDGUI,HSSGUIA.NNUMEUSUA, HSSTAXA.NNUMETAXA,
						   PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NCOPADGUI,'Taxas',CTIPOGUIA,CURGEGUIA,0,0
					  FROM HSSGPAGA,HSSGUIA,HSSDGUI,HSSTAXA,FINPRES PRES,FINPRES SOLI
					 WHERE NNUMEPAGA = :Id
					   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
					   AND HSSGUIA.NNUMEUSUA = :Usuario
					   AND HSSGUIA.NNUMEGUIA = HSSDGUI.NNUMEGUIA
					   AND NCOPADGUI <> 0
					   AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
					   AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
					   AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)
					 UNION ALL
					SELECT HSSGUIA.NNUMEGUIA,TRUNC(DEMISGUIA),TO_CHAR(NULL),CDESCPACOT,NQUANPCGUI,HSSGUIA.NNUMEUSUA, 0 NNUMETAXA,
						   PRES.CNOMEPRES EXECUTANTE,SOLI.CNOMEPRES SOLICITANTE,NCOPAPCGUI,'Pacotes',CTIPOGUIA,CURGEGUIA,0,0 
					  FROM HSSGPAGA,HSSGUIA,HSSPCGUI,HSSPACOT,FINPRES PRES,FINPRES SOLI
					 WHERE NNUMEPAGA = :Id
					   AND HSSGPAGA.NNUMEGUIA = HSSGUIA.NNUMEGUIA
					   AND HSSGUIA.NNUMEUSUA = :Usuario
					   AND HSSGUIA.NNUMEGUIA = HSSPCGUI.NNUMEGUIA
					   AND NCOPAPCGUI <> 0
					   AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT
					   AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
					   AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES(+)
					   ORDER BY 1 ";
                                     
    $linha = "FUNCIONARIO".$delimitador.
             "CODREG".$delimitador.
             "NUMEMP".$delimitador.
             "TIPCOL".$delimitador.
             "NUMCAD".$delimitador.
             "CODCAL".$delimitador.
             "TABEVE".$delimitador.
             "CODEVE".$delimitador.
             "CODRAT".$delimitador.
             "SEQEVE".$delimitador.
             "REFEVE".$delimitador.
             "VALEVE".$delimitador.
             "VL_TOTAL".$delimitador;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
    while (!$sql->eof()) {
      $sql1->clear();
      $sql1->addParam(":Id",$id_pagamento);
      $sql1->addParam(":Usuario",$sql->result("NNUMEUSUA"));
      $sql1->executeQuery($txtDespesa);
       
	  while (!$sql1->eof()) {
		$linha =  $sql->result("NOME_TITULAR").$delimitador. 
				  '01'   .$delimitador. //CODREG
				  '0242' .$delimitador. //NUMEMP;
				  '1'    .$delimitador. //TIPCOL;
				  '1'    .$delimitador. //NUMCAD;
				  '00001'.$delimitador. //CODCAL;
				  '004'  .$delimitador; //TABEVE;
		
	    if ($sql1->result("CCODIPMED") == '9905060') {
		  $linha = $linha.'987'.$delimitador;
		}
	    else if ($sql1->result("CCODIPMED") == '9905070') {
		  $linha = $linha.'878'.$delimitador;
		}
	    else if ($sql1->result("CCODIPMED") == '9905071') {
		  $linha = $linha.'878'.$delimitador;
		}
	    else if ($sql1->result("CCODIPMED") == '9905050') {
		  $linha = $linha.'879'.$delimitador;
		}
	    else if ($sql1->result("CCODIPMED") == '9905063') {
		  $linha = $linha.'528'.$delimitador;
		}
	    else {
		  $linha = $linha.'969'.$delimitador;
		}
		
		$linha = $linha.
		         '  '.$delimitador.
				 '01'.$delimitador.
				 '  '.$delimitador.			     
			     $sql1->result("NFRANPCON").$delimitador. 
			     $sql1->result("NFRANPCON").$delimitador;
		   
		fwrite($ponteiro,$linha.chr(13).chr(10)); 
        $sql1->next();        
      }
      
     $sql->next();
    }
     
    fclose ($ponteiro);  
  }
	  
  else if ($_SESSION['apelido_operadora'] == 'sampes') {
          
    $txt = "SELECT HSSUSUA.CCHAPUSUA,HSSUSUA.CNOMEUSUA,TITULAR.CNOMEUSUA TITULAR,HSSUSUA.C_CPFUSUA,DECODE(HSSUSUA.NTITUUSUA,HSSUSUA.NNUMEUSUA,0,1) ORDEM,HSSUSUA.CTIPOUSUA,
                   HSSPLAN.CCODIPLAN,HSSUSUA.CCODIUSUA,HSSUSUA.NTITUUSUA,
                   SUM(DECODE(TIPO_DADO,'USUPG',VALOR,0)) MENSALIDADE,
                   SUM(DECODE(TIPO_DADO,'PACON',VALOR,'GPAGA',VALOR,0)) COPART,
                   SUM(DECODE(TIPO_DADO,'PACON',0,'GPAGA',0,'USUPG',0,VALOR)) OUTROS,
                   SUM(VALOR) TOTAL
              FROM COMPOSICAO_MENSALIDADE CM,HSSUSUA,HSSUSUA TITULAR,HSSPLAN
             WHERE CM.NNUMEPAGA = :id_pagamento
               AND CM.NNUMEUSUA = HSSUSUA.NNUMEUSUA
               AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
             GROUP BY HSSUSUA.CCHAPUSUA,HSSUSUA.CNOMEUSUA,TITULAR.CNOMEUSUA,HSSUSUA.C_CPFUSUA,DECODE(HSSUSUA.NTITUUSUA,HSSUSUA.NNUMEUSUA,0,1),HSSUSUA.CTIPOUSUA,
                   HSSPLAN.CCODIPLAN,HSSUSUA.CCODIUSUA,HSSUSUA.NTITUUSUA
             ORDER BY TITULAR.CNOMEUSUA,5"; 
           
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->executeQuery($txt);
    
    $linha = "Matricula".$delimitador.
             "C�digo".$delimitador.             
             "Benefici�rio".$delimitador.
             "CPF".$delimitador.             
             "Categoria".$delimitador.
             "Plano".$delimitador.
             "Mensalidade".$delimitador.
             "Coparticipacao".$delimitador.
             "Outros valores".$delimitador.
             "Total".$delimitador;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
    $id_titular = 0;
    
    $txt2 = "SELECT FE.VALOR NVALOPACON                   
              FROM FATURA_COMPOSICAO FE
             WHERE FE.NNUMEPAGA = :id_pagamento
               AND FE.TIPO = 'COPARTICIPACAO_SINTETICA'";
             
    $sql1 = new Query($bd);
    $sql1->clear();
    $sql1->addParam(":id_pagamento",$id_pagamento);
    $sql1->executeQuery($txt2);

    $total_geral_copart = 0;
    
    while (!$sql->eof()) {
      
      $id_titular = $sql->result("NTITUUSUA");
      
      $linha = $sql->result("CCHAPUSUA").$delimitador.
               $formata->somenteNumeros($sql->result("CCODIUSUA"))." ".$delimitador.                           
               $sql->result("CNOMEUSUA").$delimitador.
               $sql->result("C_CPFUSUA").$delimitador.  
               $func->categoriaUsuario($sql->result("CTIPOUSUA")).$delimitador.
               $sql->result("CCODIPLAN").$delimitador.
               $sql->result("MENSALIDADE").$delimitador.
               $sql->result("COPART").$delimitador.
               $sql->result("OUTROS").$delimitador.
               $sql->result("TOTAL").$delimitador;
                 
      fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
      $total_mensalidade += str_replace(',','.',$sql->result("MENSALIDADE"));      
      $total_copart += str_replace(',','.',$sql->result("COPART"));
      $total_outros += str_replace(',','.',$sql->result("OUTROS"));
      $total += str_replace(',','.',$sql->result("TOTAL"));

      $sql->next();
      
      $total_geral_copart += str_replace(',','.',$sql1->result("NVALOPACON"));
	  
      $sql1->next();
      
      if ($id_titular <> $sql->result("NTITUUSUA")) {
        $linha = $delimitador.
                 $delimitador.
                 $delimitador.
                 $delimitador.
                 $delimitador.                 
                 "Sub-total:".$delimitador.
                 $formata->formataNumero($total_mensalidade).$delimitador.
                 $formata->formataNumero($total_copart).$delimitador.
                 $formata->formataNumero($total_outros).$delimitador.
                 $formata->formataNumero($total).$delimitador; 
        fwrite($ponteiro,$linha.chr(13).chr(10));

        $total_mensalidade = 0;
        $total_copart = 0;
        $total_outros = 0;
        $total = 0;
      }
    }
    
    $linha = $delimitador.
             $delimitador.
             $delimitador.
             $delimitador.
             $delimitador.             
             "Copart Total:".$delimitador.
             $delimitador.
             $formata->formataNumero($total_geral_copart).$delimitador;
    fwrite($ponteiro,$linha.chr(13).chr(10));

    fclose ($ponteiro);  
    
  } 
  
  else if ($tipo == 1) {
    
    $txt = "SELECT NVL(FE.CCHAPUSUA,FE.CCODIUSUA) CCHAPUSUA, FE.CNOMEUSUA, DECODE(FE.CTIPOUSUPG,'D','DEP.','T','TIT.','F','FIN.','A','AGR.','U','UNI') TIPO, 
                   FE.CDESCPLAN, SUM(DECODE(FE.TIPO,'MENSALIDADE',FE.VALOR,0)) NVLIQUSUPG, SUM(DECODE(FE.TIPO,'COPARTICIPACAO_SINTETICA',FE.VALOR,0)) COPART, FE.DINCLUSUA INCLUSAO, IDADE(FE.DNASCUSUA,:vencimento) IDADE,
                   FE.C_CPFUSUA                   
              FROM FATURA_COMPOSICAO FE
             WHERE FE.NNUMEPAGA = :id_pagamento
               AND FE.TIPO IN ('MENSALIDADE','COPARTICIPACAO_SINTETICA')
             GROUP BY NVL(FE.CCHAPUSUA,FE.CCODIUSUA),FE.CNOMEUSUA,DECODE(FE.CTIPOUSUPG,'D','DEP.','T','TIT.','F','FIN.','A','AGR.','U','UNI'),
                      FE.CDESCPLAN,FE.DINCLUSUA,IDADE(FE.DNASCUSUA,:vencimento),FE.C_CPFUSUA
             ORDER BY 1, 3 DESC ";
                          
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->addParam(":vencimento",$vencimento);           
    $sql->executeQuery($txt);
          
    $linha = "Matricula".$delimitador.
             "Benefici�rio".$delimitador.
             "Categoria".$delimitador.
             "Plano".$delimitador.
             "Valor".$delimitador.
             "Copart".$delimitador.
             "Data Inclus�o".$delimitador.
             "Idade".$delimitador.
             "Cpf".$delimitador;;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
    while (!$sql->eof()) {
                         
      $linha = $sql->result("CCHAPUSUA").$delimitador.
               $sql->result("CNOMEUSUA").$delimitador.
               $sql->result("TIPO").$delimitador.
               $sql->result("CDESCPLAN").$delimitador.
               $sql->result("NVLIQUSUPG").$delimitador.
               $sql->result("COPART").$delimitador.
               $sql->result("INCLUSAO").$delimitador.
               $sql->result("IDADE").$delimitador.
               $sql->result("C_CPFUSUA").$delimitador;
                 
      fwrite($ponteiro,$linha.chr(13).chr(10)); 
                 
      $sql->next();
    }

    fclose ($ponteiro);  
  } 
  else if ($tipo == 2) {

    $txt = "SELECT FE.CCHAPUSUA,FE.TITULAR,
                   SUM(DECODE(FE.TIPO,'COPARTICIPACAO_SINTETICA',FE.VALOR,0)) COPART,
                   (SUM(DECODE(FE.TIPO,'MENSALIDADE',FE.VALOR,0)) + SUM(DECODE(FE.TIPO,'ADESAO',FE.VALOR,0)) + SUM(DECODE(FE.TIPO,'ADITIVO',FE.VALOR,0))) MENSALIDADE
              FROM FATURA_COMPOSICAO FE
             WHERE FE.NNUMEPAGA = :id_pagamento 
               AND FE.TIPO IN ('COPARTICIPACAO_SINTETICA','MENSALIDADE','ADESAO','ADITIVO')
             GROUP BY FE.CCHAPUSUA,FE.TITULAR
             ORDER BY 2 ";
                   
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->executeQuery($txt);
          
    $linha = "Matricula".$delimitador.
             "Titular".$delimitador.
             "Mensalidade".$delimitador.
             "Coparticipacao".$delimitador;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
    while (!$sql->eof()) {
                         
      $linha = $sql->result("CCHAPUSUA").$delimitador.
               $sql->result("TITULAR").$delimitador.
               $sql->result("MENSALIDADE").$delimitador.
               $sql->result("COPART").$delimitador;
                 
      fwrite($ponteiro,$linha.chr(13).chr(10)); 
                 
      $sql->next();
    }

    fclose ($ponteiro);
  }
  else if ($tipo == 4) {
//retorna_CPF_Titular
    $txt = "SELECT CCODITITU NUMERO_DO_CONTRATO,FE.CCHAPUSUA MATRICULA,FE.TITULAR,
                   NVL(retorna_CPF_Titular(FE.NTITUUSUA),FE.C_CPFUSUA) CPF_DO_TITULAR,FE.NDOCUPAGA NUMERO_DO_BOLETO,
                   SUM(DECODE(FE.TIPO,'COPARTICIPACAO_SINTETICA',FE.VALOR,0)) COPART,
                   (SUM(DECODE(FE.TIPO,'MENSALIDADE',FE.VALOR,0)) + SUM(DECODE(FE.TIPO,'ADESAO',FE.VALOR,0)) + SUM(DECODE(FE.TIPO,'ADITIVO',FE.VALOR,0))) MENSALIDADE
              FROM FATURA_COMPOSICAO FE,HSSPAGA,HSSTITU
             WHERE FE.NNUMEPAGA = :id_pagamento
               AND      FE.NNUMEPAGA = HSSPAGA.NNUMEPAGA
               AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
               AND FE.TIPO IN ('COPARTICIPACAO_SINTETICA','MENSALIDADE','ADESAO','ADITIVO')
             GROUP BY CCODITITU,FE.CCHAPUSUA,FE.TITULAR,NVL(retorna_CPF_Titular(FE.NTITUUSUA),FE.C_CPFUSUA),FE.NDOCUPAGA
             ORDER BY 2 ";
                   
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->executeQuery($txt);
          
    $linha = "Numero do contrato".$delimitador.
             "Matricula".$delimitador.
             "Titular".$delimitador.
             "Cpf do titular".$delimitador.
             "Numero do boleto".$delimitador.
             "Mensalidade".$delimitador.
             "Coparticipacao".$delimitador;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
      
    while (!$sql->eof()) {
                         
      $linha = $sql->result("NUMERO_DO_CONTRATO").$delimitador.
               $sql->result("CCHAPUSUA").$delimitador.
               $sql->result("TITULAR").$delimitador.
               $sql->result("CPF_DO_TITULAR").$delimitador.
               $sql->result("NUMERO_DO_BOLETO").$delimitador.
               $sql->result("MENSALIDADE").$delimitador.
               $sql->result("COPART").$delimitador;
                 
      fwrite($ponteiro,$linha.chr(13).chr(10)); 
                 
      $sql->next();
    }

    fclose ($ponteiro);
  }    
  else if ($tipo == 5) {
    $txt = "SELECT FE.TITULAR,FE.CCHAPUSUA MATRICULA,TO_CHAR(FE.EMISSAO,'DD/MM/YYYY') EMISSAO,
                   FE.PRESTADOR,FE.DESCRICAO_ITEM,SUM(FE.qtde_item) QTDE, SUM(FE.VALOR) VALOR
              FROM FATURA_COMPOSICAO FE
             WHERE FE.NNUMEPAGA = :id_pagamento 
             GROUP BY FE.TITULAR,FE.CCHAPUSUA,TO_CHAR(FE.EMISSAO,'DD/MM/YYYY'),
                      FE.PRESTADOR,FE.DESCRICAO_ITEM
             ORDER BY 1 ";
             
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->executeQuery($txt);
          
    $linha = "Titular".$delimitador.
             "Matricula".$delimitador.
             "Atendimento".$delimitador.
             "Prestador".$delimitador.             
             "Item".$delimitador.
             "Quant".$delimitador.
             "Valor".$delimitador;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
                          
    while (!$sql->eof()) {
            
      $linha = $sql->result("TITULAR").$delimitador.
               $sql->result("MATRICULA").$delimitador.
               $sql->result("EMISSAO").$delimitador.
               $sql->result("PRESTADOR").$delimitador.
               $sql->result("DESCRICAO_ITEM").$delimitador.             
               $sql->result("QTDE").$delimitador.
               $sql->result("VALOR").$delimitador;               
                 
      fwrite($ponteiro,$linha.chr(13).chr(10));          
          
      $sql->next();
    }

    fclose ($ponteiro);   
  }
  else {           

    $txt = "SELECT FE.TITULAR,DECODE(FE.NTITUUSUA,FE.NNUMEUSUA,0,1) ORDEM,0 ORDEM2,
                   NVL(FE.CCHAPUSUA,FE.CCODIUSUA) CCHAPUSUA,FE.CCODIUSUA,FE.CNOMEUSUA,FE.CDESCPLAN,
                   FE.CTIPOUSUPG CTIPOUSUA,TO_CHAR(FE.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,IDADE(FE.DNASCUSUA,:vencimento) IDADE,
                   TO_CHAR(FE.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,FE.VALOR,FE.TIPO,FE.DESCRICAO_ITEM,FE.CODIGO_ITEM,FE.PRESTADOR,
                   DECODE(TIPO,'COPARTICIPACAO',FE.ID,NULL) TIPO
              FROM FATURA_COMPOSICAO FE
             WHERE FE.NNUMEPAGA = :id_pagamento 
             ORDER BY 3,1,2 ";
             
    $sql->addParam(":id_pagamento",$id_pagamento);           
    $sql->addParam(":vencimento",$vencimento);
    $sql->executeQuery($txt);
          
    $linha = "Matricula".$delimitador.
             "C�digo".$delimitador.
             "Benefici�rio".$delimitador.
             "Titular".$delimitador.
             "Plano".$delimitador.             
             "Categoria".$delimitador.
             "Data de nascimento".$delimitador.
             "Idade".$delimitador.
             "Data Inclus�o".$delimitador.
             "Tipo".$delimitador.
             "Conta/Guia".$delimitador.
             "Data".$delimitador.
             "Prestador".$delimitador.
             "C�digo do item".$delimitador.
             "Descri��o do item".$delimitador.             
             "Valor".$delimitador;
               
    fwrite($ponteiro,$linha.chr(13).chr(10)); 
                          
    while (!$sql->eof()) {
            
      $linha = $sql->result("CCHAPUSUA").$delimitador.
               $sql->result("CCODIUSUA").$delimitador.
               $sql->result("CNOMEUSUA").$delimitador.
               $sql->result("TITULAR").$delimitador.
               $sql->result("CDESCPLAN").$delimitador.             
               $func->categoriaUsuario($sql->result("CTIPOUSUA")).$delimitador.
               $sql->result("DNASCUSUA").$delimitador.
               $sql->result("IDADE").$delimitador.
               $sql->result("DINCLUSUA").$delimitador.
               $sql->result("TIPO").$delimitador.
               $sql->result("ID").$delimitador.
               $sql->result("EMISSAO").$delimitador.
               $sql->result("PRESTADOR").$delimitador.
               $sql->result("CODIGO_ITEM").$delimitador.
               $sql->result("DESCRICAO_ITEM").$delimitador.             
               $sql->result("VALOR").$delimitador;               
                 
      fwrite($ponteiro,$linha.chr(13).chr(10));          
          
      $sql->next();
    }

    fclose ($ponteiro); 
  }

  if (extension_loaded('zip')) {  
    $zip = new ZipArchive();
    $zip->open("../temp/".$arquivo.".zip", ZipArchive::CREATE);
    $zip->addfile("../temp/".$arquivo.".csv",$arquivo.".csv" );
    $zip->close();
    
    $util->redireciona('../temp/'.$arquivo.'.zip','N');  
    
  } else {
    echo "Erro ao gerar arquivo. Entre em contato com a Operadora. (Erro: php_zip)";
  }

  $bd->close();
?>