<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  $bd = new Oracle();

  $_SESSION['titulo'] = "AUTORIZA��O PARA ATENDIMENTO";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","autorizacao.htm");    
    
  $aso         = '';
  $nome        = '';    
  $nascimento  = '';
  $sexo        = '';
  $cpf         = '';
  $rg          = '';
  $data_rg     = '';
  $orgao_rg    = '';
//  $matricula   = '';
  $profissao   = '';
//  $salario     = '';
//  $admissao    = '';
//  $locacao     = '';
  $obs         = '';
  $tpl->MASCARA = $_SESSION['mascara'];
     
  if (isset($_POST['enviar'])) {     
    $aso        = $seg->antiInjection(strtoupper($_POST['aso']));
    $nome       = $seg->antiInjection(strtoupper($_POST['nome'])); 
    $nascimento = $seg->antiInjection($_POST['nascimento']);
    $sexo       = $seg->antiInjection(strtoupper($_POST['sexo']));
    $cpf        = $func->somenteNumeros($seg->antiInjection($_POST['cpf']));
    $rg         = $seg->antiInjection($_POST['rg']);
    $data_rg    = $seg->antiInjection($_POST['expedicao']);
    $orgao_rg   = $seg->antiInjection($_POST['orgao']);
//    $matricula  = $seg->antiInjection($_POST['matricula']);
    $profissao  = $seg->antiInjection($_POST['profissao']);
//    $salario    = $seg->antiInjection($_POST['salario']);
//    $admissao   = $seg->antiInjection($_POST['admissao']);
//    $locacao    = $seg->antiInjection($_POST['locacao']);
	$obs        = $seg->antiInjection($_POST['observacao']);  	
    	
	$proximo = new Query($bd);
	$txt = " SELECT SEQUSUA.NEXTVAL PROXIMO FROM DUAL ";
	$proximo->executeQuery($txt);
	
	$sql = new Query($bd);
	$txt = " SELECT ATRIBUICAO_CODIGO_USUARIO('T','',:contrato,0,0,0) CODIGO_USUARIO FROM DUAL ";
	$sql->addParam(":contrato",$_SESSION['codigo_contrato']);
	$sql->executeQuery($txt);
	
  	$insere = new Query($bd);
	$txt = " INSERT INTO HSSUSUA(CCODIUSUA,NNUMETITU,NNUMEUSUA,NTITUUSUA,CANS_USUA,DINCLUSUA,CNOMEUSUA,
	                             CSITUUSUA,NOPERUSUA,NNUMESTAT,DNASCUSUA,CTIPOUSUA,CGRAUUSUA,CSEXOUSUA,
								 C_CPFUSUA,C__RGUSUA,DEXRGUSUA,CORRGUSUA,NNUMEPROF,NNUMEPLAN)
	                      VALUES(:codigo,:contrato,:usuario,:titular,'N',trunc(SYSDATE),:nome,
						         'P',:operador,44479,:nascimento,'T','X',:sexo,
								 :cpf,:rg,:data_rg,:orgao_rg,:profissao,3812166) ";
	         
    $insere->addParam(":codigo",$sql->result("CODIGO_USUARIO"));
	$insere->addParam(":contrato",$_SESSION['id_contrato']);
	$insere->addParam(":usuario",$proximo->result("PROXIMO"));
	$insere->addParam(":titular",$proximo->result("PROXIMO"));
	$insere->addParam(":nome",$nome);
	$insere->addParam(":operador",$_SESSION['id_operador']);
	$insere->addParam(":nascimento",$nascimento);
	$insere->addParam(":sexo",$sexo);
	$insere->addParam(":cpf",$cpf);
	$insere->addParam(":rg",$rg);
	$insere->addParam(":data_rg",$data_rg);
	$insere->addParam(":orgao_rg",$orgao_rg);
//	$insere->addParam(":matricula",$matricula);
	$insere->addParam(":profissao",$profissao);	
//	$insere->addParam(":salario",$salario);	
//	$insere->addParam(":admissao",$admissao);	
//	$insere->addParam(":locacao",$locacao);	
	$erro = $insere->executeSQL($txt);

    if ($erro <> '') {
      $tpl->MENSAGEM = $erro;
      $tpl->block("MSG");
    }else {
	  $txt = " BEGIN " .
	         "    configura_pcmso(:usuario,:profissao,trunc(sysdate)); " .
	         " END;  ";
				 
      $conf = new Query($bd);
      $conf->addParam(":usuario",$proximo->result("PROXIMO"));
	  $conf->addParam(":profissao",$profissao);	   
      $erro = $conf->executeSQL($txt);  
      
	  if ($erro <> '') {
      	$tpl->MENSAGEM = $erro;
      	$tpl->block("MSG");
      }else {
		
          $filtros = array();
          $filtros['op']   = 600;
         
          $_SESSION['filtrosConfirmacao'] = $filtros;	 
        
          $util->redireciona('confirmacao.php?idSessao='.$_GET['idSessao']);
	  }
    }
  }
  else if(isset($_POST['imprimir'])) { 
    $exames            = $_POST['ck_exames'];
    $id_funcionario    = $_POST['id_funcionario'];
	$id_nova_profissao = $_POST['id_nova_profissao'];
	$nome2             = $_POST['nome2'];
	$profissao2        = $_POST['profissao2'];
	$obs               = $_POST['observacao'];
	$prof              = $_POST['nova_prof'];
	
	if($_POST['aso'] == '1')
	  $tipo_aso = "Admissional";
	else if($_POST['aso'] == '2')
	  $tipo_aso = "Peri�dico";	  
	else if($_POST['aso'] == '3')
	  $tipo_aso = "Demissional";	  
	else if($_POST['aso'] == '4')
	  $tipo_aso = "Mudan�a de fun��o";	 
	else if($_POST['aso'] == '5')
	  $tipo_aso = "Retorno ao trabalho";
	  
	  	 	   
  	require_once("autorizacaoImp.php"); 
  }
  /* Montagem da Tela */
  
  $sql = new Query($bd);  
  $txt = "SELECT CDESCPROF,HSSPROF.NNUMEPROF
            FROM HSSPROF, HSSPROEM, HSSEMPR, HSSTITU
		   WHERE HSSTITU.NNUMETITU = :contrato
		     AND HSSPROF.NNUMEPROF = HSSPROEM.NNUMEPROF
			 AND HSSPROEM.NNUMEEMPR = HSSEMPR.NNUMEEMPR
			 AND HSSEMPR.NNUMEEMPR = HSSTITU.NNUMEEMPR
		   ORDER BY CDESCPROF";
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  while (!$sql->eof()) {
    $tpl->CADASTRO_PROFISSAO_ID        = $sql->result("NNUMEPROF");
    $tpl->CADASTRO_PROFISSAO_DESCRICAO = $sql->result("CDESCPROF");
    $tpl->block("CADASTRO_ITEM_PROFISSAO");
    $sql->next();
  }
  /* Select Categoria 
  $sql = new Query($bd);  
  $txt = "SELECT CTIPORCATE FROM HSSRCATE WHERE NNUMETITU = :contrato ";
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  if ($sql->count() > 0) {
        
    $tpl->CADASTRO_CATEGORIA_ID        = 'T';
    $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario('T');
    $tpl->block("CADASTRO_ITEM_CATEGORIA");
          
    while (!$sql->eof()) {
      $tpl->CADASTRO_CATEGORIA_ID        = $sql->result("CTIPORCATE");
      $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($sql->result("CTIPORCATE"));
      $tpl->block("CADASTRO_ITEM_CATEGORIA");
      $sql->next();
    }
  } 
  else {
    $categorias = array('T','F','D','A','U','E','L');
    
    foreach ($categorias as $c) {        
      $tpl->CADASTRO_CATEGORIA_ID        = $c;
      $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($c);
      $tpl->block("CADASTRO_ITEM_CATEGORIA");
    }     
  } 
*/
  
  /* Select Locacao */
  /*
  if ($_SESSION['id_locacao'] <= 0) {
    $txt = "SELECT CNOMESETOR,NNUMESETOR 
              FROM HSSTITU,HSSEMPR,HSSSETOR
             WHERE HSSTITU.NNUMETITU = :contrato
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
               AND HSSEMPR.NNUMEEMPR = HSSSETOR.NNUMEEMPR
               AND NVL(CSITUSETOR,'A') = 'A'";               
    $sql = new Query($bd);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);  
    
    if ($sql->count() > 0) {
      while (!$sql->eof()) {
        $tpl->CADASTRO_LOCACAO_ID        = $sql->result("NNUMESETOR");
        $tpl->CADASTRO_LOCACAO_DESCRICAO = $sql->result("CNOMESETOR");
        $tpl->block("CADASTRO_ITEM_LOCACAO");
        $sql->next();
      } 
    
//    	$tpl->block("CADASTRO_LOCACOES");	      
    }
  }  
  */
 
  
  /* Select Tipo logradouro 
  $txt = "SELECT NNUMETLOGR,INITCAP(CDESCTLOGR) CDESCTLOGR
            FROM HSSTLOGR 
           ORDER BY 2";  
  $sql = new Query($bd);
  $sql->executeQuery($txt);  
  
  while (!$sql->eof()) {
    $tpl->CADASTRO_TIPO_LOGRADOURO_ID        = $sql->result("NNUMETLOGR");
    $tpl->CADASTRO_TIPO_LOGRADOURO_DESCRICAO = $sql->result("CDESCTLOGR");
    $tpl->block("CADASTRO_ITEM_TIPO_LOGRADOURO");
    $sql->next();
  } 
  */
 
  $tpl->CADASTRO_NOME              = $nome;  
 $tpl->ID_SESSAO                     = $_GET['idSessao']; 
 
  $tpl->PAGINA_VOLTAR = "principal.php?idSessao=".$_GET['idSessao'];
  
  
  $tpl->block("MOSTRA_MENU");  
  $tpl->show();     
  $bd->close();
  
?>