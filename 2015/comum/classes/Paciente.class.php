<?php
  
class Paciente {

  private $bd;
  private $codigoErro;
  private $descricaoErro;
  private $erros      = array();
  private $errosFatal = array();
  
  public $idPaciente;
  public $situacao;
  public $nome;
  public $dataNascimento;
  public $sexo;
  public $cpf;
  public $nomeDaMae;
  public $nomeDoPai;
  public $rg;
  public $idProfissao;
  public $observacao;
  public $cep;
  public $idTipoLogradouro;
  public $logradouro;
  public $numero;
  public $complemento;
  public $bairro;
  public $uf;
  public $cidade;
  public $telefone1;
  public $telefone2;
  public $email1;
  public $email2; 
  public $idConsultorio;  
  public $cns;
  public $rn;
  
  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  } 
  
  public function __construct($bd) {
    $this->bd = $bd;
  }
  
  public function setIdPaciente($id) {
    $this->idPaciente = $id;
  }
  
  public function getIdPaciente() {
    return $this->idPaciente;
  }
  
  public function carregaDadosPaciente() {

    $formata = new Formata();
    
    if ($this->getErrosFatal() == 0) {
      
      $sql = new Query($this->bd);
                     
      $txt = "SELECT NNUMEPACIE,CNOMEPACIE,TO_CHAR(DNASCPACIE,'DD/MM/YYYY') DNASCPACIE,CSEXOPACIE,CCPF_PACIE,
                     CNMAEPACIE,CRG__PACIE,NNUMEPROF,COBSEPACIE,CCEP_PACIE,NNUMETLOGR,CENDEPACIE,CNUMEPACIE,
                     CCOMPPACIE,CBAIRPACIE,CUF__PACIE,CCIDAPACIE,CFON1PACIE,CFON2PACIE,CMAI1PACIE,CMAI2PACIE,
                     CCNS_PACIE,NNUMEPRES,TRUNC(SYSDATE - DNASCPACIE,0) IDADE_DIAS,CNPAIPACIE
                FROM HSSPACIE 
               WHERE NNUMEPACIE = :id ";
               
      $sql->addParam(":id",$this->idPaciente);
      $sql->executeQuery($txt); 

      if ($sql->result("NNUMEPACIE") == 0)
        $this->adicionaErroFatal('naoLocalizado','Paciente n�o localizado.');        
      else {                     
        $this->situacao           = 'A';
        $this->nome               = $sql->result("CNOMEPACIE");
        $this->dataNascimento     = $sql->result("DNASCPACIE");
        $this->sexo               = $sql->result("CSEXOPACIE");
        $this->cpf                = $formata->formataCPF($sql->result("CCPF_PACIE"));
        $this->nomeDaMae          = $sql->result("CNMAEPACIE");
        $this->nomeDoPai          = $sql->result("CNPAIPACIE");
        $this->rg                 = $sql->result("CRG__PACIE");
        $this->idProfissao        = $sql->result("NNUMEPROF");
        $this->observacao         = $sql->result("COBSEPACIE");
        $this->cep                = $formata->formataCEP($sql->result("CCEP_PACIE"));
        $this->idTipoLogradouro   = $sql->result("NNUMETLOGR");
        $this->logradouro         = $sql->result("CENDEPACIE");
        $this->numero             = $sql->result("CNUMEPACIE");
        $this->complemento        = $sql->result("CCOMPPACIE");
        $this->bairro             = $sql->result("CBAIRPACIE");
        $this->uf                 = $sql->result("CUF__PACIE");
        $this->cidade             = $sql->result("CCIDAPACIE");
        $this->telefone1          = $sql->result("CFON1PACIE");
        $this->telefone2          = $sql->result("CFON2PACIE");
        $this->email1             = $sql->result("CMAI1PACIE");
        $this->email2             = $sql->result("CMAI2PACIE");    
        $this->cns                = $formata->formataCNS($sql->result("CCNS_PACIE"));
        $this->idConsultorio      = $sql->result("NNUMEPRES"); 

        if ($sql->result("IDADE_DIAS") > 30)         
          $this->rn               = "N";
        else
          $this->rn               = "S";
      }
    }
  }
    
  public function adicionaErro($codigo,$descricao,$idCodigo='') {
    array_push($this->erros,array($codigo,$descricao,$idCodigo));
  }   
  
  public function adicionaErroFatal($codigo,$descricao) {
    array_push($this->errosFatal,array($codigo,$descricao));
  }     
    
  public function getQtdeErros() {
    return sizeOf($this->erros);
  }  
  
  public function getQtdeErrosFatal() {
    return sizeOf($this->errosFatal);
  }    
  
  public function getErros() {
    
    $desc_erros = "";
    for ($b = 0;$b < sizeof($this->erros);$b++) {      
      $erro = array();
      $erro = $this->erros[$b];
      
      if (sizeof($this->erros) > 1)
        $desc_erros .= "* ".$erro[1]."<br><br>";      
      else
        $desc_erros .= "* ".$erro[1]."<br>";
    } 
    
    return $desc_erros;
  }

  public function getArrayErros() {  
    return $this->erros;
  }  

  public function getErrosFatal() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->errosFatal);$b++) {      
      $erro = array();
      $erro = $this->errosFatal[$b];
      
      if (sizeof($this->errosFatal) > 1)
        $desc_erros .= ($b+1)." - ".$erro[1]."<br><br>";      
      else
        $desc_erros .= $erro[1]."<br>";
    } 
    
    return $desc_erros;
  }   
  
  public function getArrayFatalErros() {  
    return $this->errosFatal;
  } 
      
  public function jaExiste() {
    $sql = new Query($this->bd);
        
    $txt = "SELECT CNOMEPACIE,NNUMEPACIE,DNASCPACIE
              FROM HSSPACIE
             WHERE CCPF_PACIE = :cpf
               AND NNUMEPRES  = :consultorio
               AND NNUMEPACIE <> :idPaciente
             UNION ALL
            SELECT CNOMEPACIE,NNUMEPACIE,DNASCPACIE
              FROM HSSPACIE              
             WHERE CNMAEPACIE = :mae
               AND TO_CHAR(DNASCPACIE,'DD/MM/YYYY') = :nascimento
               AND CNOMEPACIE LIKE :nome
               AND NNUMEPACIE <> :idPaciente
               AND NNUMEPRES = :consultorio";
                     
    $sql->addParam(":cpf",$this->cpf);
    $sql->addParam(":consultorio",$this->idConsultorio);
    $sql->addParam(":nascimento",$this->dataNascimento);
    $sql->addParam(":idPaciente",$this->idPaciente);
    $sql->addParam(":mae",$this->nomeDaMae);
    
    $nomes = explode(" ",$this->nome);
    //Pega somente o primeiro nome
    $sql->addParam(":nome",$nome[0]."%");
    $sql->executeQuery($txt);

    if ($sql->result("NNUMEPACIE") > 0) {
      $this->setIdPaciente($sql->result("NNUMEPACIE"));
      $this->carregaDadosPaciente();
    }
  }  
  
  public function validar() {
    $util = new Util();
    $data = new Data();
    $formata = new Formata();
    
    $erros  = "";  
    $erros .= $util->validarCampo("O nome"                     ,$this->nome             ,array('obrigatorio','minimo=8','maximo=60','letras','abreviado'));  
    $erros .= $util->validarCampo("A data de nascimento"       ,$this->dataNascimento   ,array('obrigatorio','data'));
    $erros .= $util->validarCampo("O sexo"                     ,$this->sexo             ,array('obrigatorio'));
                                                                
    if ((($this->dataNascimento <> '') and ($data->idade($this->bd,$this->dataNascimento) > 18)) or ($this->dataNascimento == ''))
      $erros .= $util->validarCampo("O CPF"                    ,$this->cpf              ,array('obrigatorio','cpf'));
    else                                                        
      $erros .= $util->validarCampo("O CPF"                    ,$this->cpf              ,array('cpf'));
                                                                
    $erros .= $util->validarCampo("O RG"                       ,$this->rg               ,array('maximo=14'));
    $erros .= $util->validarCampo("O Cart�o Nacional de Sa�de" ,$formata->somenteNumeros($this->cns) ,array('cns'));    
    $erros .= $util->validarCampo("O nome da m�e"              ,$this->nomeDaMae        ,array('obrigatorio','minimo=8','maximo=60','letras','abreviado'));
    $erros .= $util->validarCampo("O nome do pai"              ,$this->nomeDoPai        ,array('maximo=60','letras','abreviado'));
    $erros .= $util->validarCampo("O CEP"                      ,$this->cep              ,array('obrigatorio'));
    $erros .= $util->validarCampo("O tipo do logradouro"       ,$this->idTipoLogradouro ,array('obrigatorio'));
    $erros .= $util->validarCampo("O logradouro"               ,$this->logradouro       ,array('obrigatorio'));
    $erros .= $util->validarCampo("O n�mero"                   ,$this->numero           ,array('obrigatorio','maximo=10'));
    $erros .= $util->validarCampo("O complemento"              ,$this->complemento      ,array('maximo=30'));
    $erros .= $util->validarCampo("O bairro"                   ,$this->bairro           ,array('obrigatorio'));
    $erros .= $util->validarCampo("O estado"                   ,$this->uf               ,array('obrigatorio'));
    $erros .= $util->validarCampo("A cidade"                   ,$this->cidade           ,array('obrigatorio'));
    $erros .= $util->validarCampo("O email 1"                  ,$this->email1           ,array('email'));
    $erros .= $util->validarCampo("O email 2"                  ,$this->email2           ,array('email'));  
    $erros .= $util->validarCampo("O telefone 1"               ,$this->telefone1        ,array('obrigatorio'));
    
    return $erros;
  }
    
  public function save() {
    $func = new Funcao();
    $sql  = new Query($this->bd);
    
    if ($this->idPaciente > 0) {
      $txt = "UPDATE HSSPACIE
                 SET CNOMEPACIE = :nome,
                     DNASCPACIE = :nascimento,
                     CSEXOPACIE = :sexo,
                     CCPF_PACIE = :cpf,
                     CNMAEPACIE = :nomemae,
                     CNPAIPACIE = :nomepai,
                     CRG__PACIE = :rg,
                     NNUMEPROF  = :idProfissao,
                     COBSEPACIE = :observacao,
                     CCEP_PACIE = :cep,
                     NNUMETLOGR = :idTipoLogradouro,
                     CENDEPACIE = :logradouro,
                     CNUMEPACIE = :numero,
                     CCOMPPACIE = :complemento,
                     CBAIRPACIE = :bairro,
                     CUF__PACIE = :uf,
                     CCIDAPACIE = :cidade,
                     CFON1PACIE = :telefone1,
                     CFON2PACIE = :telefone2,
                     CMAI1PACIE = :email1,
                     CMAI2PACIE = :email2,
                     NOPERUSUA  = :idOperador,
                     DATUAPACIE = SYSDATE,
                     CCNS_PACIE = :cns
               WHERE NNUMEPACIE = :idPaciente";    
    }
    else {
      $sql_proximo = new Query($this->bd);
      $txt_proximo = "SELECT SEQPACIE.NEXTVAL ULTIMO FROM DUAL ";
      $sql_proximo->executeQuery($txt_proximo);
            
      $this->idPaciente = $sql_proximo->result("ULTIMO");
            
      $txt = "INSERT INTO HSSPACIE (NNUMEPACIE,CNOMEPACIE,DNASCPACIE,CSEXOPACIE,CCPF_PACIE,CNMAEPACIE,CNPAIPACIE,CRG__PACIE,
                                    NNUMEPROF ,COBSEPACIE,CCEP_PACIE,NNUMETLOGR,CENDEPACIE,CNUMEPACIE,CCOMPPACIE,
                                    CBAIRPACIE,CUF__PACIE,CCIDAPACIE,CFON1PACIE,CFON2PACIE,CMAI1PACIE,CMAI2PACIE,
                                    CCNS_PACIE,NNUMEPRES,NOPERUSUA,DINCLPACIE,DATUAPACIE)
                            VALUES (:idPaciente,:nome,:nascimento,:sexo,:cpf,:nomemae,:nomepai,:rg,
                                    :idProfissao,:observacao,:cep,:idTipoLogradouro,:logradouro,:numero,:complemento,
                                    :bairro,:uf,:cidade,:telefone1,:telefone2,:email1,:email2,
                                    :cns,:idConsultorio,:idOperador,sysdate,sysdate)";
    }
    
    $sql->addParam(":idPaciente"       ,$this->idPaciente);
    $sql->addParam(":nome"             ,strToUpper($this->nome));
    $sql->addParam(":nascimento"       ,$this->dataNascimento);
    $sql->addParam(":sexo"             ,$this->sexo);
    $sql->addParam(":cpf"              ,$func->somenteNumeros($this->cpf));
    $sql->addParam(":nomemae"          ,strToUpper($this->nomeDaMae));
    $sql->addParam(":nomepai"          ,strToUpper($this->nomeDoPai));
    $sql->addParam(":rg"               ,$this->rg);
    $sql->addParam(":idProfissao"      ,$this->idProfissao);
    $sql->addParam(":observacao"       ,$this->observacao);
    $sql->addParam(":cep"              ,$func->somenteNumeros($this->cep));
    $sql->addParam(":idTipoLogradouro" ,$this->idTipoLogradouro);
    $sql->addParam(":logradouro"       ,strToUpper($this->logradouro));
    $sql->addParam(":numero"           ,$this->numero);
    $sql->addParam(":complemento"      ,strToUpper($this->complemento));
    $sql->addParam(":bairro"           ,strToUpper($this->bairro));
    $sql->addParam(":uf"               ,$this->uf);
    $sql->addParam(":cidade"           ,$this->cidade);
    $sql->addParam(":telefone1"        ,$this->telefone1);
    $sql->addParam(":telefone2"        ,$this->telefone2);
    $sql->addParam(":email1"           ,strToLower($this->email1));
    $sql->addParam(":email2"           ,strToLower($this->email2)); 
    $sql->addParam(":cns"              ,$func->somenteNumeros($this->cns)); 
    $sql->addParam(":idConsultorio"    ,$this->idConsultorio); 
    $sql->addParam(":idOperador"       ,$this->idOperador); 
    
    if ($this->idPaciente > 0)
      $sql->executeSQL($txt);
    
  }  
  
  public function excluir() {  
    $sql  = new Query($this->bd);    
    $txt = "DELETE FROM HSSPACIE WHERE NNUMEPACIE = :idPaciente AND CNOMEPACIE = :nome";    
    $sql->addParam(":idPaciente"       ,$this->idPaciente);
    $sql->addParam(":nome"             ,strToUpper($this->nome));
    $sql->executeSQL($txt);
  }
  
}

?>