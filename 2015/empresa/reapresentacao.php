<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
  $sql = new Query($bd);
  $_SESSION['titulo'] = "RE-APRESENTA��O DE CADASTRO REJEITADO";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/formulario.htm");

  $atcad                 = $seg->antiInjection($_POST['atcad']);
  $operacao              = $seg->antiInjection($_POST['op']);    
  $id                    = $seg->antiInjection($_POST['id']);    
  $nome                  = '';
  $categoria             = '';
  $data_inclusao         = '';
  $nascimento            = '';
  $estadonasc            = '';
  $cidadenasc            = '';  
  $sexo                  = '';
  $grau                  = '';
  $estado_civil          = '';
  $cpf                   = '';
  $rg                    = '';
  $expedicao             = '';
  $orgao                 = '';
  $pis                   = '';
  $csus                  = '';
  $ndnv                  = '';
  $mae                   = '';
  $pai                   = '';  
  $uniao                 = '';
  $titular               = '';
  $matricula             = '';
  $profissao             = '';
  $setor                 = '';
  $salario               = '';
  $admissao              = '';
  $demissao              = '';
  $locacao               = '';
  $centro_custo          = '';
  $ini_afast             = '';
  $motivo                = '';
  $fim_afast             = '';
  $plano                 = '';
  $acomodacao            = '';
  $aditivos_selecionados = array();
  $telefone1             = '';
  $telefone2             = '';
  $celular               = '';
  $email                 = '';
  $cep                   = '';
  $tipo_logradouro       = '';
  $logradouro            = '';
  $numero                = '';
  $complemento           = '';
  $bairro                = '';
  $estado                = '';
  $cidade                = '';
  $motivo_canc           = '';
  $data_cancelamento     = '';
  $anexo                 = array();
  $observacao1           = '';
  $melhor_dia_inclusao   = '';
  $continuar             = '';  
  $observacao            = '';
  $peso                  = '';
  $altura                = ''; 
  $cidadeatendimento     = '';  
  
  
  $pode_alterar_plano_dependente = $seg->permissaoOutros($bd,"WEBEMPRESAPODEALTERARPLANODODEPENDENTE",false);
  
  if (isset($_POST['enviar'])) {
    $nome                  = $seg->antiInjection(strtoupper($_POST['nome']));
    $categoria             = $seg->antiInjection($_POST['categoria']);
    
    if (isset($_POST['data_inclusao']))
      $data_inclusao       = $seg->antiInjection($_POST['data_inclusao']);
      
    $estadonasc            = $seg->antiInjection($_POST['estado_nasc']);    
    $cidadenasc            = $seg->antiInjection($_POST['cidade_nasc']);        
    
    $nascimento            = $seg->antiInjection($_POST['nascimento']);
    $sexo                  = $seg->antiInjection($_POST['sexo']);
    
    if ($categoria == 'T' or $categoria == 'F')
      $grau                = "X";
    else
      $grau                = $seg->antiInjection($_POST['grau']);
      
    $estado_civil          = $seg->antiInjection($_POST['estado_civil']);
    $cpf                   = $seg->antiInjection($_POST['cpf']);
    $rg                    = $seg->antiInjection($_POST['rg']);
    $expedicao             = $seg->antiInjection($_POST['expedicao']);
    $orgao                 = $seg->antiInjection(strtoupper($_POST['orgao']));
    $pis                   = $seg->antiInjection($_POST['pis']);
    $csus                  = $seg->antiInjection($_POST['csus']);
    $ndnv                  = $seg->antiInjection($_POST['ndnv']);
    $mae                   = $seg->antiInjection(strtoupper($_POST['mae']));
    $pai                   = $seg->antiInjection(strtoupper($_POST['pai']));       
    $uniao                 = $seg->antiInjection($_POST['uniao']);
    
    if (isset($_POST['titular']))
      $titular             = $seg->antiInjection($_POST['titular']);
      
    $matricula             = $seg->antiInjection($_POST['matricula']);
    
    if (isset($_POST['profissao']))    
      $profissao           = $seg->antiInjection($_POST['profissao']);
      
    if (isset($_POST['setor']))      
      $setor               = $seg->antiInjection($_POST['setor']);
      
    $salario               = $seg->antiInjection($_POST['salario']);
    $admissao              = $seg->antiInjection($_POST['admissao']);
    
    if (isset($_POST['demissao']))    
      $demissao            = $seg->antiInjection($_POST['demissao']);
    
    if ((isset($_SESSION['id_locacao'])) and ($_SESSION['id_locacao'] > 0))
      $locacao             = $_SESSION['id_locacao'];
    else if (isset($_POST['locacao']))
      $locacao             = $seg->antiInjection($_POST['locacao']);
      
    if (isset($_POST['centro_custo']))
      $centro_custo        = $seg->antiInjection($_POST['centro_custo']);
      
    if (isset($_POST['ini_afast']))
      $ini_afast           = $seg->antiInjection($_POST['ini_afast']);
      
    if (isset($_POST['motivo_afast']))
      $motivo              = $seg->antiInjection($_POST['motivo_afast']);
      
    if (isset($_POST['fim_afast']))
      $fim_afast           = $seg->antiInjection($_POST['fim_afast']);
    
    if (isset($_POST['plano']))    
      $plano               = $seg->antiInjection($_POST['plano']);
      
    if (isset($_POST['acomodacao']))      
      $acomodacao          = $seg->antiInjection($_POST['acomodacao']);
      
    $telefone1             = $seg->antiInjection($_POST['telefone1']);
    
    if (isset($_POST['telefone2']))    
      $telefone2           = $seg->antiInjection($_POST['telefone2']);
      
    if (isset($_POST['celular']))
      $celular             = $seg->antiInjection($_POST['celular']);
      
    $email                 = $seg->antiInjection($_POST['email']);
    $cep                   = $seg->antiInjection($_POST['cep']);
    $tipo_logradouro       = $seg->antiInjection($_POST['tipo_logradouro']);
    $logradouro            = $seg->antiInjection(strtoupper($_POST['logradouro']));
    $numero                = $seg->antiInjection($_POST['numero']);
    $complemento           = $seg->antiInjection(strtoupper($_POST['complemento']));
    $bairro                = $seg->antiInjection(strtoupper($_POST['bairro']));
    $estado                = $seg->antiInjection($_POST['estado']);
    $cidade                = $seg->antiInjection($_POST['cidade']);
    $cidadeatendimento     = $seg->antiInjection($_POST['cidadeatendimento']);
    
    if (isset($_POST['motivo_cancelamento']))     
      $motivo_canc         = $seg->antiInjection($_POST['motivo_cancelamento']);
    
    if (isset($_POST['data_cancelamento']))     
      $data_cancelamento   = $seg->antiInjection($_POST['data_cancelamento']);
      
    if (isset($_POST['observacao']))    
      $observacao1         = $seg->antiInjection($_POST['observacao']);
      
    $melhor_dia_inclusao   = $seg->antiInjection($_POST['melhor_dia_inclusao']);
    $continuar             = $seg->antiInjection($_POST['continuar']);
    $peso                  = $seg->antiInjection($_POST['peso']);
    $altura                = $seg->antiInjection($_POST['altura']);    
          
    if (($plano == "") or ($acomodacao == "")) {
      if ((!$pode_alterar_plano_dependente) and (($categoria <> 'T') and ($categoria <> 'F'))) {
        $txt = "SELECT HSSUSUA.NNUMEPLAN,RETORNA_ACOMODACAO_USUARIO(HSSUSUA.NNUMEUSUA) NNUMEACOM
                  FROM HSSUSUA,HSSTITU,HSSPLAN
                 WHERE NNUMEUSUA = :titular
                   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                 UNION ALL
                SELECT NNUMEPLAN,NNUMEACOM FROM HSSATCAD
                 WHERE NNUMEUSUA = :titular
                   AND COPERATCAD = 'I'
                   AND CFLAGATCAD IS NULL ";

        $sql->clear();
        $sql->addParam(":titular",$titular);      
        $sql->executeQuery($txt);
        
        $plano      = $sql->result("NNUMEPLAN");
        $acomodacao = $sql->result("NNUMEACOM");
      }
    }
             
    if (!$seg->permissaoOutros($bd,"WEBEMPRESAEFETUAAFASTAMENTO")) {     
      $sql->clear();    
      $txt = "SELECT TO_CHAR(DFAFAATCAD,'DD/MM/YYYY') DFINAAFAST, TO_CHAR(DIAFAATCAD,'DD/MM/YYYY') DINICAFAST, CMAFAATCAD CMOTIAFAST
                FROM HSSATCAD
               WHERE NNUMEATCAD = :id ";
      $sql->addParam(":id",$atcad);     
      $sql->executeQuery($txt);

      $ini_afast = $sql->result("DINICAFAST"); 
      $fim_afast = $sql->result("DFINAAFAST"); 
      $motivo    = $sql->result("CMOTIAFAST");        
    }

    $erro = '';

    if ($_FILES['upload1']['name'] <> '')
      $erro .= $util->validaExtensaoArquivo($_FILES['upload1']['name']);
          
    if ($erro == '') { 
    
      if ($operacao == 'C') {
        $retorno = $func->alteraUsuario ($bd,$_SESSION['id_contrato'],'C',$id,$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,
                                         $matricula,$salario,$admissao,'',$locacao,$plano,$acomodacao,
                                         $telefone1,$telefone2,$celular,$email,
                                         $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,
                                         $ini_afast,$fim_afast,$motivo,$motivo_canc,$data_cancelamento,$estado_civil,$profissao,$setor,'E','',$_FILES['upload1'],
                                         $csus,$ndnv,$uniao,'','',$atcad,1); 
      } 
      else if ($operacao == 'I') {
      
        $sql_pai = new Query($bd); 
        $txt = "SELECT NNPAIATCAD FROM HSSATCAD
                 WHERE NNUMEATCAD = :atcad ";
        $sql_pai->addParam(":atcad",$atcad);
        $sql_pai->executeQuery($txt);
      
        if ($sql_pai->result("NNPAIATCAD") > 0) {
          $atcadpai = $sql_pai->result("NNPAIATCAD");
        } else {
          $atcadpai = $atcad;
        }  
        
        $retorno = $func->insereUsuario ($bd,$_SESSION['id_contrato'],$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,$titular,
                                         $matricula,$salario,$admissao,$locacao,$plano,$acomodacao,
                                         $telefone1,$telefone2,$celular,$email,
                                         $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,$data_inclusao,
                                         $estado_civil,$profissao,$setor,'E',0,0,0,0,$_FILES['upload1'],$csus,$ndnv,$observacao1,$uniao,
                                         $aditivos_selecionados,$centro_custo,$atcadpai,$peso,$altura,'',$cidadeatendimento);
        //Repassando os anexos do usuario anterior para o novo uruario re-apresentado.
        if ($retorno['erro'] == '') {
          $sql_anexo = new Query();      
          $txt_anexo = "SELECT NNUMEANEXO,CNOMEANEXO,NNUMEUSUA,NSIZEANEXO,DDATAANEXO FROM HSSANEXO
                     WHERE NNUMEUSUA = :usuario";
          $sql_anexo->addParam(":usuario",$id);
          $sql_anexo->executeQuery($txt_anexo); 
          
          while (!$sql_anexo->eof()) {
            $sql_insere = new Query();
            $txt_insere_anexo = "INSERT INTO HSSANEXO (CNOMEANEXO,NNUMEUSUA,NSIZEANEXO,DDATAANEXO)
                                      VALUES (:nome,:usuario,:tamanho,:data)";
            $sql_insere->addParam(":nome",$sql_anexo->result("CNOMEANEXO"));
            $sql_insere->addParam(":usuario",$retorno['id_usuario']);
            $sql_insere->addParam(":tamanho",$sql_anexo->result("NSIZEANEXO"));
            $sql_insere->addParam(":data",$sql_anexo->result("DDATAANEXO"));
            $sql_insere->executeSQL($txt_insere_anexo); 
            $sql_anexo->next();
          }
        }
      } else {
        $retorno = $func->alteraUsuario ($bd,$_SESSION['id_contrato'],'A',$id,$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,
                                         $matricula,$salario,$admissao,'',$locacao,$plano,$acomodacao,
                                         $telefone1,$telefone2,$celular,$email,
                                         $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,
                                         $ini_afast,$fim_afast,$motivo,'','',$estado_civil,$profissao,$setor,'E','',$_FILES['upload1'],
                                         $csus,$ndnv,$uniao,'',$cidadeatendimento,$atcad,1);
      }                                  
    
      if ($retorno['erro'] <> '') {
        $tpl->CLASSE   = "alert-error";
        $tpl->MENSAGEM = $retorno['erro'];
        $tpl->block("MSG");
      } 
      else {
        if ($operacao <> 'I') {    
          $sql = new Query($bd); 
          $txt = "UPDATE HSSATCAD
                     SET CFLAGATCAD = NULL
                   WHERE NNUMEATCAD = :atcad ";
          $sql->addParam(":atcad",$atcad);
          $sql->executeSQL($txt);
        } else {
          $sql = new Query($bd);
          $txt = "UPDATE HSSATCAD
                     SET CREAPATCAD = 'N'
                   WHERE NNUMEATCAD = :atcad ";
          $sql->addParam(":atcad",$atcad);
          $sql->executeSQL($txt);    
        }
      
        $filtros = array();
        $filtros['op']   = 300;
        $filtros['prot'] = $retorno['id'];
        
        $_SESSION['filtrosConfirmacao'] = $filtros;
      
        $util->redireciona('confirmacao.php?idSessao='.$_GET['idSessao']);
      }     
    }
    else {
      $tpl->CLASSE   = "alert-error";
      $tpl->MENSAGEM = $erro;
      $tpl->block("MSG");    
    }
  } 
  else if ($id > 0) {
    if ($_SESSION['apelido_operadora'] <> 'saudemed')
      $estado = $_SESSION['uf_operadora']; 

    $sql_mat = new Query($bd);
    $sql_mat->clear();
    $txt_mat = "SELECT COBMTTITU
              FROM HSSTITU
             WHERE NNUMETITU = :contrato";
    $sql_mat->addParam(":contrato",$_SESSION['id_contrato']);
    $sql_mat->executeQuery($txt_mat); 	        
      
    $sql = new Query();

    if ($operacao == 'I') {
    
      $tpl->OBRIGATORIO_MATRICULA = $sql_mat->result("COBMTTITU");
      
      $txt = "SELECT CNOMEATCAD CNOMEUSUA,CCHAPATCAD CCHAPUSUA,TO_CHAR(DNASCATCAD,'DD/MM/YYYY') DNASCUSUA,DINCLATCAD DINCLUSUA,
                     CPIS_ATCAD CPIS_USUA,C_CPFATCAD C_CPFUSUA,C__RGATCAD C__RGUSUA,TO_CHAR(DADMIATCAD,'DD/MM/YYYY') DADMIUSUA,
                     CORRGATCAD CORRGUSUA,CGRAUATCAD CGRAUUSUA,CTIPOATCAD CTIPOUSUA,NTITUUSUA,NNUMEPLAN,
                     NNUMEACOM,
                     NNUMEUSUA,CNMAEATCAD CNMAEUSUA,CSEXOATCAD CSEXOUSUA,NNUMESETOR,CENDEATCAD CENDEUSUA,CBAIRATCAD CBAIRUSUA,
                     CCIDAATCAD CCIDAUSUA,CESTAATCAD CESTAUSUA,CCEP_ATCAD CCEP_USUA,TO_CHAR(DEXRGATCAD,'DD/MM/YYYY') DEXRGUSUA,
                     CCOMPATCAD CREFEUSUA,CNUMEATCAD CNUMEUSUA,NFATUTLOGR,CMAILATCAD CMAILUSUA,CFONEATCAD CFONETLUSU,
                     NSALAATCAD NSALAFUNCI,COPERATCAD,CESTCATCAD CESTCUSUA,NNUMEPROF,NNUMEFUNSE,CSUS_ATCAD CSUS_USUA,NDCNVATCAD NDCNVUSUA, 
                     CCLOAATCAD, COBSEATCAD, CCIDNATCAD CCIDNUSUA, CNPAIATCAD CNPAIUSUA,  CESTNATCAD CESTNUSUA
                FROM HSSATCAD
               WHERE HSSATCAD.NNUMEATCAD = :id ";
               
      $sql->addParam(":id",$atcad); 
    } 
    else {
    
      $tpl->OBRIGATORIO_MATRICULA = $sql_mat->result("COBMTTITU");
      
      $txt = "SELECT CNOMEUSUA,CCHAPUSUA,TO_CHAR(DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,DINCLUSUA, 
                     CPIS_USUA,C_CPFUSUA,C__RGUSUA,TO_CHAR(DADMIUSUA,'DD/MM/YYYY') DADMIUSUA, 
                     CORRGUSUA,CGRAUUSUA,CTIPOUSUA,NTITUUSUA,HSSUSUA.NNUMEPLAN,
                     RETORNA_ACOMODACAO_USUARIO(HSSUSUA.NNUMEUSUA) NNUMEACOM, 
                     HSSUSUA.NNUMEUSUA,CNMAEUSUA,CSEXOUSUA,HSSUSUA.NNUMESETOR,CENDEUSUA,CBAIRUSUA,
                     CCIDAUSUA,CESTAUSUA,CCEP_USUA,TO_CHAR(DEXRGUSUA,'DD/MM/YYYY') DEXRGUSUA,HSSUSUA.NNUMEMCANC,
                     CREFEUSUA,CNUMEUSUA,HSSUSUA.NFATUTLOGR,CMAILUSUA,acrescenta_zeros(somente_numeros_string(CFONETLUSU),10) CFONETLUSU,NSALAFUNCI,CESTCUSUA,NNUMEPROF,NNUMEFUNSE,
                     CSUS_USUA,NDCNVUSUA, '' CCLOAATCAD, '' COBSEATCAD, CCIDNUSUA, CNPAIUSUA, CESTNUSUA                   
                FROM HSSUSUA,HSSTITU,HSSPLAN, (SELECT NNUMEUSUA, CFONETLUSU FROM HSSTLUSU
                                                WHERE NNUMETLUSU = (SELECT MAX(TEL.NNUMETLUSU) FROM HSSTLUSU TEL
                                                                     WHERE TEL.CFONETLUSU IS NOT NULL
                                                                        AND TEL.NNUMEUSUA = HSSTLUSU.NNUMEUSUA)) FONE
               WHERE HSSUSUA.NNUMEUSUA = :id 
                 AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                 AND HSSUSUA.NNUMEUSUA = FONE.NNUMEUSUA(+) ";                   
      $sql->addParam(":id",$id);     
    }
    
    $sql->executeQuery($txt);
    
    $nome                 = $func->alteracaoAtcad($atcad,"HSSUSUA","CNOMEUSUA",$sql->result("CNOMEUSUA"));
    $categoria            = $func->alteracaoAtcad($atcad,"HSSUSUA","CTIPOUSUA",$sql->result("CTIPOUSUA"));
    $data_inclusao        = '';
    $nascimento           = $func->alteracaoAtcad($atcad,"HSSUSUA","DNASCUSUA",$sql->result("DNASCUSUA"));
    $sexo                 = $func->alteracaoAtcad($atcad,"HSSUSUA","CSEXOUSUA",$sql->result("CSEXOUSUA"));
    $grau                 = $func->alteracaoAtcad($atcad,"HSSUSUA","CGRAUUSUA",$sql->result("CGRAUUSUA"));
    $estado_civil         = $func->alteracaoAtcad($atcad,"HSSUSUA","CESTCUSUA",$sql->result("CESTCUSUA"));
    $cpf                  = $func->alteracaoAtcad($atcad,"HSSUSUA","C_CPFUSUA",$sql->result("C_CPFUSUA"));
    $rg                   = $func->alteracaoAtcad($atcad,"HSSUSUA","C__RGUSUA",$sql->result("C__RGUSUA"));
    $expedicao            = $func->alteracaoAtcad($atcad,"HSSUSUA","DEXRGUSUA",$sql->result("DEXRGUSUA"));    
    $orgao                = $func->alteracaoAtcad($atcad,"HSSUSUA","CORRGUSUA",$sql->result("CORRGUSUA"));
    $pis                  = $func->alteracaoAtcad($atcad,"HSSUSUA","CPIS_USUA",$sql->result("CPIS_USUA"));
    $csus                 = $func->alteracaoAtcad($atcad,"HSSUSUA","CSUS_USUA",$sql->result("CSUS_USUA"));
    $ndnv                 = $func->alteracaoAtcad($atcad,"HSSUSUA","NDCNVUSUA",$sql->result("NDCNVUSUA"));
    $mae                  = $func->alteracaoAtcad($atcad,"HSSUSUA","CNMAEUSUA",$sql->result("CNMAEUSUA"));
    $uniao                = $sql->result("DUESTUSUA"); 
    $titular              = $func->alteracaoAtcad($atcad,"HSSUSUA","NTITUUSUA",$sql->result("NTITUUSUA"));
    $matricula            = $func->alteracaoAtcad($atcad,"HSSUSUA","CCHAPUSUA",$sql->result("CCHAPUSUA"));
    $profissao            = $func->alteracaoAtcad($atcad,"HSSUSUA","NNUMEPROF",$sql->result("NNUMEPROF"));
    $setor                = $func->alteracaoAtcad($atcad,"HSSUSUA","NNUMEFUNSE",$sql->result("NNUMEFUNSE"));
    $salario              = $func->alteracaoAtcad($atcad,"HSSUSUA","NSALAFUNCI",$sql->result("NSALAFUNCI"));
    $admissao             = $func->alteracaoAtcad($atcad,"HSSUSUA","DADMIUSUA",$sql->result("DADMIUSUA"));
    $demissao             = '';
    $locacao              = $func->alteracaoAtcad($atcad,"HSSUSUA","NNUMESETOR",$sql->result("NNUMESETOR"));
    $centro_custo         = '';   
    $plano                = $func->alteracaoAtcad($atcad,"HSSUSUA","NNUMEPLAN",$sql->result("NNUMEPLAN"));
    $acomodacao           = $func->alteracaoAtcad($atcad,"HSSUSUA","NNUMEACOM",$sql->result("NNUMEACOM"));
    
    if ($sql->result("CFONETLUSU") <> '0000000000')
      $telefone1          = $formata->formataTelefone($func->alteracaoAtcad($atcad,"HSSTLUSU","CFONETLUSU",$sql->result("CFONETLUSU")));
    else             
      $telefone1          = "";
    
    $telefone2            = "";
    $celular              = "";
    $email                = $func->alteracaoAtcad($atcad,"HSSUSUA","CMAILUSUA",$sql->result("CMAILUSUA"));
    $cep                  = $func->alteracaoAtcad($atcad,"HSSUSUA","CCEP_USUA",$sql->result("CCEP_USUA"));
    $tipo_logradouro      = $func->alteracaoAtcad($atcad,"HSSUSUA","NFATUTLOGR",$sql->result("NFATUTLOGR"));
    $logradouro           = $func->alteracaoAtcad($atcad,"HSSUSUA","CENDEUSUA",$sql->result("CENDEUSUA"));
    $numero               = $func->alteracaoAtcad($atcad,"HSSUSUA","CNUMEUSUA",$sql->result("CNUMEUSUA"));
    $complemento          = $func->alteracaoAtcad($atcad,"HSSUSUA","CREFEUSUA",$sql->result("CREFEUSUA"));
    $bairro               = $func->alteracaoAtcad($atcad,"HSSUSUA","CBAIRUSUA",$sql->result("CBAIRUSUA"));
    $estado               = $func->alteracaoAtcad($atcad,"HSSUSUA","CESTAUSUA",$sql->result("CESTAUSUA"));
    $cidade               = $func->alteracaoAtcad($atcad,"HSSUSUA","CCIDAUSUA",$sql->result("CCIDAUSUA"));
    $motivo_canc          = $func->alteracaoAtcad($atcad,"HSSUSUA","NNUMEMCANC",$sql->result("NNUMEMCANC"));
    $data_cancelamento    = '';
    $observacao1          = $sql->result("COBSEATCAD"); 
    $melhor_dia_inclusao  = '';
    $continuar            = '';  
    $observacao           = '';
    $cidadeatendimento    = $sql->result("CCLOAATCAD");
    $cidadenasc           = $func->alteracaoAtcad($atcad,"HSSUSUA","CCIDNUSUA",$sql->result("CCIDNUSUA"));
    $estadonasc           = $func->alteracaoAtcad($atcad,"HSSUSUA","CESTNUSUA",$sql->result("CESTNUSUA"));
    $pai                  = $func->alteracaoAtcad($atcad,"HSSUSUA","CNPAIUSUA",$sql->result("CNPAIUSUA"));


    if ($seg->permissaoOutros($bd,"WEBEMPRESAEFETUAAFASTAMENTO")) {
      $sql = new Query($bd);
      $txt = "SELECT TO_CHAR(DFINAAFAST,'DD/MM/YYYY') DFINAAFAST, TO_CHAR(DINICAFAST,'DD/MM/YYYY') DINICAFAST, CMOTIAFAST
                FROM HSSAFAST
               WHERE NNUMEUSUA = :id
                 AND DFINAAFAST IS NULL
               ORDER BY DINICAFAST ";
      $sql->addParam(":id",$id);    
      $sql->executeQuery($txt);

      $ini_afast = $func->alteracaoAtcad($atcad,"HSSAFAST","DINICAFAST",$sql->result("DINICAFAST"));
      $fim_afast = $func->alteracaoAtcad($atcad,"HSSAFAST","DFINAAFAST",$sql->result("DFINAAFAST"));
      $motivo    = $func->alteracaoAtcad($atcad,"HSSAFAST","CMOTIAFAST",$sql->result("CMOTIAFAST"));      
      
      if ($ini_afast <> '')
        $tpl->block("CADASTRO_FIM_AFASTAMENTO");    
      
      $tpl->block("CADASTRO_AFASTAMENTO");      
    }
  }  
    
  /* Montagem da Tela */

  /* Select Categoria 
  
    Como � alteracao, se o usuario for titular deve-se mostrar a lista de categorias contendo
    apenas titular e titular financeiro
    Se o usuario n�o for titular a� sim mostra-se a relacao de categorias
    
  */

  if ($categoria == 'T' or $categoria == 'F') {
    $categorias = array('F','T');
    
    foreach ($categorias as $c) {        
      $tpl->CADASTRO_CATEGORIA_ID        = $c;
      $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($c);
      $tpl->block("CADASTRO_ITEM_CATEGORIA");
    }    
  } 
  else {
    $sql = new Query($bd);   
    $txt = "SELECT CTIPORCATE FROM HSSRCATE WHERE NNUMETITU = :contrato AND CTIPORCATE NOT IN ('T','F')";
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
    
    if ($sql->count() > 0) {
      while (!$sql->eof()) {
        $tpl->CADASTRO_CATEGORIA_ID        = $sql->result("CTIPORCATE");
        $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($sql->result("CTIPORCATE"));
        $tpl->block("CADASTRO_ITEM_CATEGORIA");
        $sql->next();
      }
    } 
    else {
    if (($_SESSION['apelido_operadora'] == 'clinipam') or
        ($_SESSION['apelido_operadora'] == 'odontopam') or
        ($_SESSION['apelido_operadora'] == 'saudeescolar')) {
      $categorias = array('T','D','A','U','E','L');
    } else {
	  $categorias = array('T','F','D','A','U','E','L');
    }
      
      foreach ($categorias as $c) {        
        $tpl->CADASTRO_CATEGORIA_ID        = $c;
        $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($c);
        $tpl->block("CADASTRO_ITEM_CATEGORIA");
      }     
    }
  }
  
  /* Select Grau */  
  $txt = "SELECT CGRAURPARE FROM HSSRPARE
           WHERE NNUMETITU = :contrato
           UNION ALL
          SELECT CGRAURPARE FROM HSSTITU,HSSRPARE
           WHERE HSSTITU.NNUMETITU = :contrato
             AND HSSTITU.NNUMEPLAN = HSSRPARE.NNUMEPLAN
             AND HSSRPARE.NNUMETITU IS NULL
             AND 0 = (SELECT COUNT(*) FROM HSSRPARE
                       WHERE NNUMETITU = :contrato)";
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  if ($sql->count() > 0) {
    while (!$sql->eof()) {
      $tpl->CADASTRO_GRAU_ID        = $sql->result("CGRAURPARE");
      $tpl->CADASTRO_GRAU_DESCRICAO = $func->grauDeParentesco($sql->result("CGRAURPARE"));
      $tpl->block("CADASTRO_ITEM_GRAU");
      $sql->next();
    }
  } else {
    $graus = array('F','E','P','M','I','A','T','R','C','G','N','B','S','O','D','H','J');
    
    foreach ($graus as $g) {        
      $tpl->CADASTRO_GRAU_ID        = $g;
      $tpl->CADASTRO_GRAU_DESCRICAO = $func->grauDeParentesco($g);
      $tpl->block("CADASTRO_ITEM_GRAU");
    }     
  }  

  /* Select estado civil */  
  $estados_civis = array('S','C','V','D','U','A','O');
    
  foreach ($estados_civis as $e) {        
    $tpl->CADASTRO_ESTADO_CIVIL_ID        = $e;
    $tpl->CADASTRO_ESTADO_CIVIL_DESCRICAO = $func->estadoCivil($e);
    $tpl->block("CADASTRO_ITEM_ESTADO_CIVIL");
  }
  
  if ($operacao == 'I') {
    /* Select Titular */
    $txt = "SELECT NNUMEUSUA,CNOMEUSUA,DECODE(CCHAPUSUA,NULL,NULL,' - ' || CCHAPUSUA) CCHAPUSUA
              FROM HSSUSUA
             WHERE NNUMETITU = :contrato
               AND CTIPOUSUA IN ('T','F') 
               AND CSITUUSUA = 'A'
             UNION ALL
            SELECT DISTINCT NNUMEUSUA,CNOMEATCAD,DECODE(CCHAPATCAD,NULL,NULL,' - ' || CCHAPATCAD) CCHAPUSUA
              FROM HSSATCAD
             WHERE NNUMETITU = :contrato
               AND COPERATCAD = 'I'
               AND CFLAGATCAD IS NULL
               AND NNUMEUSUA = NTITUUSUA
             ORDER BY 2 ";  
    $sql->clear();
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);  
    
    while (!$sql->eof()) {
      $tpl->CADASTRO_TITULAR_ID   = $sql->result("NNUMEUSUA");
      $tpl->CADASTRO_TITULAR_NOME = $sql->result("CNOMEUSUA").$sql->result("CCHAPUSUA");
      $tpl->block("CADASTRO_ITEM_TITULAR");
      $sql->next();
    }
  
    $tpl->block("LOCALIZA_TITULAR");  
    $tpl->block("TITULAR"); 
  }
  
  /* Select profissao */
  $txt = "SELECT NNUMEPROF,INITCAP(CDESCPROF) CDESCPROF
            FROM HSSPROF 
           ORDER BY 2";  
  $sql = new Query($bd);
  $sql->executeQuery($txt); 

  if ($sql->count() > 0) {  
  
    while (!$sql->eof()) {
      $tpl->CADASTRO_PROFISSAO_ID        = $sql->result("NNUMEPROF");
      $tpl->CADASTRO_PROFISSAO_DESCRICAO = $sql->result("CDESCPROF");
      $tpl->block("CADASTRO_ITEM_PROFISSAO");
      $sql->next();
    }
    
    $tpl->block("CADASTRO_PROFISSOES");
  } 

  /* Select setor */	   
  $txt = "SELECT HSSDEPAR.NNUMEDEPAR,INITCAP(CNOMEDEPAR) CNOMEDEPAR           
            FROM HSSDEPAR,HSSDEPEM,HSSEMPR,HSSTITU
		       WHERE HSSTITU.NNUMETITU = :contrato
			       AND HSSDEPAR.NNUMEDEPAR = HSSDEPEM.NNUMEDEPAR
     		     AND HSSDEPEM.NNUMEEMPR = HSSEMPR.NNUMEEMPR
		         AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR			
           ORDER BY 2"; 		   
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt); 

  if ($sql->count() > 0) {  
  
    while (!$sql->eof()) {
      $tpl->CADASTRO_SETOR_ID        = $sql->result("NNUMEDEPAR");
      $tpl->CADASTRO_SETOR_DESCRICAO = $sql->result("CNOMEDEPAR");
      $tpl->block("CADASTRO_ITEM_SETOR");
      $sql->next();
    }    
    $tpl->block("CADASTRO_SETORES");
  } 

  /* Select Locacao */
  $txt = "SELECT CNOMESETOR,NNUMESETOR 
            FROM HSSTITU,HSSEMPR,HSSSETOR
           WHERE HSSTITU.NNUMETITU = :contrato
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
             AND HSSEMPR.NNUMEEMPR = HSSSETOR.NNUMEEMPR
             AND NVL(CSITUSETOR,'A') = 'A' ";  
    
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);  
  
  if ($sql->count() > 0) {
    while (!$sql->eof()) {
      $tpl->CADASTRO_LOCACAO_ID        = $sql->result("NNUMESETOR");
      $tpl->CADASTRO_LOCACAO_DESCRICAO = $sql->result("CNOMESETOR");
      $tpl->block("CADASTRO_ITEM_LOCACAO");
      $sql->next();
    } 
    
    $tpl->block("CADASTRO_LOCACOES");
  }  

  /* Select Plano */
  $txt = "SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSPLTIT.NNUMEPLAN
            FROM HSSPLTIT,HSSPLAN
           WHERE HSSPLTIT.NNUMETITU = :contrato
             AND HSSPLTIT.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION
          SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSTITU.NNUMEPLAN
            FROM HSSTITU,HSSPLAN
           WHERE HSSTITU.NNUMETITU = :contrato
             AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN
           UNION
          SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSUSUA.NNUMEPLAN
            FROM HSSUSUA,HSSPLAN
           WHERE NNUMEUSUA = :usuario 
             AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";  
    
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->addParam(":usuario",$id);  
  $sql->executeQuery($txt);  
  
  while (!$sql->eof()) {
    $tpl->CADASTRO_PLANO_ID        = $sql->result("NNUMEPLAN");
    $tpl->CADASTRO_PLANO_DESCRICAO = $sql->result("CCODIPLAN")." - ".$sql->result("CDESCPLAN");
    $tpl->block("CADASTRO_ITEM_PLANO");
    $sql->next();
  }   
  
  /* Select Tipo logradouro */
  $txt = "SELECT NNUMETLOGR,INITCAP(CDESCTLOGR) CDESCTLOGR
            FROM HSSTLOGR 
           ORDER BY 2";  
  $sql = new Query($bd);
  $sql->executeQuery($txt);  
  
  while (!$sql->eof()) {
    $tpl->CADASTRO_TIPO_LOGRADOURO_ID        = $sql->result("NNUMETLOGR");
    $tpl->CADASTRO_TIPO_LOGRADOURO_DESCRICAO = $sql->result("CDESCTLOGR");
    $tpl->block("CADASTRO_ITEM_TIPO_LOGRADOURO");
    $sql->next();
  }
  
  /* Select Motivos de cancelamento */
  $txt = "SELECT NNUMEMCANC,CDESCMCANC
            FROM HSSMCANC
           WHERE (CLOCAMCANC = 'A' OR CLOCAMCANC = 'W')
           ORDER BY 2";  
    
  $sql = new Query($bd);
  $sql->executeQuery($txt);  
  
  while (!$sql->eof()) {
    $tpl->CADASTRO_MOTIVO_CANC_ID        = $sql->result("NNUMEMCANC");
    $tpl->CADASTRO_MOTIVO_CANC_DESCRICAO = $sql->result("CDESCMCANC");
      
    $tpl->block("CADASTRO_ITEM_MOTIVO_CANC");
    $sql->next();
  }   
  
  /* Direitos obriga��o de campos */
  $tpl->DIREITO_CPF                   = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOCPF",false);
  $tpl->DIREITO_CSUS                  = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOCARTOSUS",false);
  $tpl->DIREITO_NDNV                  = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTODECLARAONASCIDOVIVO",false);
  $tpl->DIREITO_ENDERECO              = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOENDEREOCOMPLETO",false);
  $tpl->DIREITO_TEL                   = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOTELEFONE",false);  
  $tpl->PF                            = 'N'; 
  $tpl->ID_SESSAO                     = $_GET['idSessao'];  
  $tpl->CADASTRO_OPERADORA            = $_SESSION['apelido_operadora'];    
  $tpl->CADASTRO_CONTRATO             = $_SESSION['id_contrato'];       

  if ($operacao == 'C') {
    $tpl->CANCELAMENTO = 'S';
    $tpl->OBRIGATORIO_MATRICULA = 'N';
  } else
    $tpl->CANCELAMENTO = 'N';  
  
  if ($pode_alterar_plano_dependente)
    $tpl->PODE_ALTERAR_PLANO_DEPENDENTE = 'S';
  else
    $tpl->PODE_ALTERAR_PLANO_DEPENDENTE = 'N'; 
    
  $tpl->block("ANEXOS"); 
  
  if ($seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOANEXONAREAPRESENTAO",false))    
    $tpl->ANEXO_OBRIGATORIO = 'S';
  else
    $tpl->ANEXO_OBRIGATORIO = 'N';  
  
  if (($_SESSION['apelido_operadora'] == 'agemed') or ($_SESSION['apelido_operadora'] == 'unimedSalto')) {
    $tpl->MSG_TAM_ARQUIVO = "OBS: Tamanho m�ximo do arquivo � de 5MB.";
    $tpl->block("MSG_TAMANHO_ARQUIVO");
  }      
  
  if ($_SESSION['apelido_operadora'] <> 'sampes')
    $tpl->block("MOSTRA_RG");	
    
  if ($_SESSION['apelido_operadora'] <> 'saudemed')
    $tpl->block("MOSTRA_ACOMODACAO");
     
  $tpl->block("OBSERVACOES");   
     
  if (($operacao == 'C') and ($seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOENDEREOCOMPLETO",false) == 1)) { 
    $cep             = '';
    $tipo_logradouro = '';    
    $logradouro      = '';    
    $numero          = '';   
    $complemento     = '';    
    $bairro          = ''; 
    $estado          = '';
    $cidade          = '';
  } 
  
  $tpl->CADASTRO_NOME                 = $nome;  
  $tpl->CADASTRO_CATEGORIA            = $categoria;    
  $tpl->CADASTRO_DATA_INCLUSAO        = $data_inclusao;  
  $tpl->CADASTRO_NASCIMENTO           = $nascimento;  
  $tpl->CADASTRO_ESTADO_NASC          = $estadonasc;  
  $tpl->CADASTRO_CIDADE_NASC          = $cidadenasc;    
  $tpl->CADASTRO_SEXO                 = $sexo;    
  $tpl->CADASTRO_GRAU                 = $grau;      
  $tpl->CADASTRO_ESTADO_CIVIL         = $estado_civil; 
  $tpl->CADASTRO_CPF                  = $cpf;  
  $tpl->CADASTRO_RG                   = $rg;  
  $tpl->CADASTRO_EXPEDICAO            = $expedicao;  
  $tpl->CADASTRO_ORGAO                = $orgao;  
  $tpl->CADASTRO_PIS                  = $pis;  
  $tpl->CADASTRO_CSUS                 = $csus;    
  $tpl->CADASTRO_NDNV                 = $ndnv;    
  $tpl->CADASTRO_MAE                  = $mae;  
  $tpl->CADASTRO_PAI                  = $pai;    
  $tpl->CADASTRO_UNIAO                = $uniao;    
  $tpl->CADASTRO_TITULAR              = $titular;  
  $tpl->CADASTRO_MATRICULA            = $matricula;  
  $tpl->CADASTRO_PROFISSAO            = $profissao;
  $tpl->CADASTRO_SETOR                = $setor;  
  $tpl->CADASTRO_SALARIO              = $salario;  
  $tpl->CADASTRO_ADMISSAO             = $admissao;
  $tpl->CADASTRO_DEMISSAO             = $demissao;    
  $tpl->CADASTRO_LOCACAO              = $locacao;      
  $tpl->CADASTRO_PLANO                = $plano;    
  $tpl->CADASTRO_ACOMODACAO           = $acomodacao;  
  $tpl->CADASTRO_TELEFONE1            = $telefone1;    
  $tpl->CADASTRO_TELEFONE2            = $telefone2;    
  $tpl->CADASTRO_CELULAR              = $celular;    
  $tpl->CADASTRO_EMAIL                = $email; 
  $tpl->CADASTRO_CEP                  = $cep;    
  $tpl->CADASTRO_TIPO_LOGRADOURO      = $tipo_logradouro;    
  $tpl->CADASTRO_LOGRADOURO           = $logradouro;    
  $tpl->CADASTRO_NUMERO               = $numero;    
  $tpl->CADASTRO_COMPLEMENTO          = $complemento;    
  $tpl->CADASTRO_BAIRRO               = $bairro; 
  $tpl->CADASTRO_ESTADO               = $estado;
  $tpl->CADASTRO_CIDADE               = $cidade;    
  $tpl->CADASTRO_OBSERVACAO1          = $observacao1;
  $tpl->CADASTRO_DATA_CANCELAMENTO    = $data_cancelamento;
  $tpl->CADASTRO_INI_AFAST            = $ini_afast;  
  $tpl->CADASTRO_FIM_AFAST            = $fim_afast;
  $tpl->CADASTRO_MOTIVO_AFAST         = $motivo;
  $tpl->CADASTRO_MOTIVO_CANC          = $motivo_canc;  
  $tpl->CADASTRO_ID                   = $id;      
  $tpl->CADASTRO_CENTRO_CUSTO         = $centro_custo;  
  $tpl->OBSERVACAO                    = $observacao;  
  $tpl->CADASTRO_OP                   = $operacao;
  $tpl->CADASTRO_ATCAD                = $atcad;
  $tpl->CADASTRO_PESO                 = $peso;
  $tpl->CADASTRO_ALTURA               = $altura;  
  $tpl->CIDADE_ATEND                  = $cidadeatendimento;
                        
  if ($observacao <> '')                        
    $tpl->block("MOSTRA_OBSERVACAO");  
  
  $tpl->BOTAO_ID                      = "enviar";      
  $tpl->CADASTRO_BOTAO                = "Re-apresentar";    
  
  $tpl->PAGINA_VOLTAR = "pendentes.php?idSessao=".$_GET['idSessao'];
  $tpl->block("BOTAO_VOLTAR");
  
  $tpl->block("MOSTRA_MENU");   
  $bd->close();
  $tpl->show();     
  
?>
  
  
  