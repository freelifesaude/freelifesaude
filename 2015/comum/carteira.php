<?php
  Session_start();
  require_once("../comum/autoload.php");  
  
  $bd = new Oracle();  
  
  $sql = new Query($bd);
    
  if ($_POST['acao'] == 'I') {
  
    if ($_SESSION['id_locacao'] > 0) {
      $txt = "INSERT INTO HSSCAWEB (NNUMEUSUA,CSITUCAWEB,NNUMETITU,NNUMESETOR) VALUES (:usuario,'I',:contrato,:locacao)";
      $sql->addParam(":locacao",$_SESSION['id_locacao']);   
    } else
      $txt = "INSERT INTO HSSCAWEB (NNUMEUSUA,CSITUCAWEB,NNUMETITU) VALUES (:usuario,'I',:contrato)";
      
    $sql->addParam(":usuario",$_POST['usuario']);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeSQL($txt);
  
  } else if ($_POST['acao'] == 'E') {
  
    $txt = "DELETE FROM HSSCAWEB
             WHERE NNUMEUSUA = :usuario
               AND NNUMETITU = :contrato
               AND NVL(NNUMESETOR,-1) = :locacao
               AND CSITUCAWEB = 'I'";  
    $sql->addParam(":locacao",$_SESSION['id_locacao']); 
    $sql->addParam(":usuario",$_POST['usuario']);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeSQL($txt);
  
  } else if ($_POST['acao'] == 'A') {    
    $txt = "SELECT TO_CHAR(SYSDATE,'YYYYMMDDHHMi') PROTOCOLO FROM DUAL";
    $sql->executeQuery($txt);    
    $protocolo = $sql->result("PROTOCOLO");
              
    for ($i=0;$i<sizeOf($_POST['carteira']);$i++) {
      $sql->clear();         
      $txt = "UPDATE HSSCAWEB SET CSITUCAWEB = 'P',
                                  CMOTICAWEB = UPPER(:obs),
                                  NNUMEMTCAR = :mtcar,
                                  CPROTCAWEB = :protocolo,
                                  CBO__CAWEB = UPPER(:bo)
               WHERE NNUMECAWEB = :carteira ";               
      $sql->addParam(":obs",$_POST['obs'][$i]); 
      $sql->addParam(":mtcar",$_POST['motivo'][$i]); 
      $sql->addParam(":protocolo",$protocolo);    
      $sql->addParam(":bo",$_POST['bo'][$i]);
      $sql->addParam(":carteira",$_POST['carteira'][$i]);
      $sql->executeSQL($txt);            
    }      
    
    $filtros = array();
    $filtros['op']   = 500;
    $filtros['prot'] = $protocolo;
    
    $_SESSION['filtrosConfirmacao'] = $filtros;
    
    $url = $_SESSION['diretorio'].'/confirmacao.php?idSessao='.$_GET['idSessao'];

    echo '<script type="text/javascript">
            window.location.href="'.$url.'";
            </script>
            <noscript>
              <meta http-equiv="refresh" content="0;url='.$url.'" />
            </noscript> ';  
  }
  
  $bd->close();
  
?>