<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  
  $seg  = new Seguranca();
  $bd   = new Oracle();
  $func = new Funcao();
  
  class PDF extends PDFSolus {
    
    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',11,11,40,13);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(125,5,'RELAT�RIO DE MOVIMENTA��O',0,0);
      
      if ($_SESSION['apelido_operadora'] <> 'saudemed') {
	  	$this->Cell(60,5);
        $this->SetFont('Arial','',6);
        $this->Cell(25,5,'carimbo da empresa',0,1);
        $this->Rect(230,5,30,25);        
      } else {
        $this->Cell(25,5,'',0,1);      
      }
      
      $this->SetFont('Arial','B',7);
      $this->Cell(40,5,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(130,3,$_SESSION['codigo_contrato'] . ' - ' . $_SESSION['titular_contrato']." - ".$_SESSION['cnpj_contrato'],0,1);
      
      if ($_SESSION['id_locacao'] > 0) {
        $this->Cell(40,3,'');      
        $this->Cell(25,3,"Loca��o:",0,0,'R');
        $this->Cell(205,3,$_SESSION['nome_locacao'],0,1);    
      }     
      
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Per�odo:",0,0,'R');
      $this->Cell(130,3,$this->getArray("Periodo"),0,1);
      $this->Cell(40,3,'');
      $this->Cell(25,3,'Situa��o:',0,0,'R');
      $this->Cell(130,3,$this->getArray("Situa��o"),0,1);
      $this->Ln(2);
      $this->Cell(40,3,'');
      $this->SetTextColor(255,0,0);
      $this->MultiCell(220,3,'Solicitamos a movimenta��o cadastral para os usu�rios relacionados abaixo, respeitando as informa��es lan�ados via sistema, bem como a data de inclus�o ou exclus�o dos usu�rios.',0,'J');
      $this->SetTextColor(0,0,0);
      $this->Ln(2);
      $this->Cell(275,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',7);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	  $this->Cell(0,10,'Impresso por: ' . $_SESSION['operador'] . '  em: ' . date('d/m/Y h:m'),0,0,'R');
    }
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  
  $datainicial = $_POST['di'];
  $datafinal   = $_POST['df'];
  $operacao    = $_POST['op'];
  $situacao    = $_POST['s'];
    
  if ($situacao == '1') 
    $situacao_desc = "Todas";
  else if ($situacao == '2') 
    $situacao_desc = "Pendentes";
  else if ($situacao == '3') 
    $situacao_desc = "Confirmadas";
  else
    $situacao_desc = '';

  $arr = array("Periodo" => ($datainicial." a ".$datafinal),
               "Situa��o" => $situacao_desc);
  $pdf->SetArray($arr);

  $contrato = $_SESSION['id_contrato'];
  
  if ($_SESSION['id_locacao'] > 0)
    $locacao = $_SESSION['id_locacao'];  
  else    
    $locacao = $_POST['lo'];
     
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);
 
  $sql_incl = new Query($bd);
  
  if ($seg->permissaoOutros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
    $f_operador = "   AND HSSATCAD.NOPERUSUA = :operador ";
    $operador = $_SESSION['id_operador'];
  } else {
    $operador = 0;
    $f_operador = "";  
  }
  
  if ($locacao > 0)
    $f_locacao =  "   AND NVL(HSSATCAD.NNUMESETOR,HSSUSUA.NNUMESETOR) = :locacao ";
  else
    $f_locacao = "";
  
  if ($situacao == '1')
    $f_situacao = "   AND HSSATCAD.CFLAGATCAD IS NULL";
  else if ($situacao == '2')
    $f_situacao = "   AND HSSATCAD.CFLAGATCAD = 'A'";
  else  
    $f_situacao = "";  
    
    
  // Inclusoes
  if ($operacao == '1') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD = 0";
    
  // Cancelamento
  } else if ($operacao == '2') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD = 0";
    
  // Alteracao
  } else if ($operacao == '3') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD > 0";
    
  // Cancelamento e Inclusao
  } else if ($operacao == '4') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD = 0";
  } else {
    $f_operacao  = "";      
    $f_operacao2 = "";      
    $f_operacao3 = "";      
  }
  
  $txt_incl = "SELECT NVL(RETORNA_NOME_USUARIO_ATCAD(HSSATCAD.NTITUUSUA),'Excluido') TITULAR, HSSATCAD.CNOMEATCAD CNOMEUSUA,HSSATCAD.C_CPFATCAD  C_CPFUSUA,TO_CHAR(HSSATCAD.DNASCATCAD,'DD/MM/YYYY') DNASCUSUA,
                      HSSATCAD.CNMAEATCAD CNMAEUSUA,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DDIGIUSUA,HSSUSUA.DINCLUSUA),HSSATCAD.DINCLATCAD),'DD/MM/YYYY')) DINCLUSUA,HSSATCAD.CTIPOATCAD CTIPOUSUA,
                      HSSATCAD.CGRAUATCAD CGRAUUSUA, 2 TIPO,HSSATCAD.NNUMEPLAN NNUMEPLAN,TO_CHAR(HSSATCAD.DDATAATCAD,'DD/MM/YYYY') DDATAATCAD,HSSUSUA.CCODIUSUA,
                      DECODE(CFLAGATCAD,'A',TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YYYY'),'R','Rejeitado','Pendente') DINCLUSUA,IDADE(DNASCATCAD,SYSDATE) IDADEUSUARIO,NDCNVATCAD,C__RGATCAD
                 FROM HSSATCAD,HSSUSUA
                WHERE HSSATCAD.NNUMETITU = :contrato
                  AND HSSATCAD.DDATAATCAD >= TO_DATE(:datainicial,'DD/MM/YYYY')
                  AND HSSATCAD.DDATAATCAD < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                  AND HSSATCAD.COPERATCAD = 'I' ".
              $f_situacao.
              $f_operacao.
              $f_operador.
              "   AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+) ".                              
              $f_locacao.
              "ORDER BY 1,8 desc,2";  

  $sql_incl->addParam(":contrato",$contrato); 
  $sql_incl->addParam(":datainicial",$datainicial); 
  $sql_incl->addParam(":datafinal",$datafinal);
    
  if ($locacao >0)
    $sql_incl->addParam(":locacao",$locacao);  

  if ($operador > 0)
    $sql_incl->addParam(":operador",$operador);  
  
  $sql_incl->executeQuery($txt_incl);
  
  if ($sql_incl->count() > 0) {
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'INCLUS�ES',0,1);  
    $pdf->Ln(1);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(42,3,'Nome',0,0);
    $pdf->Cell(18,3,'Categoria',0,0);
    $pdf->Cell(18,3,'D.N.V.',0,0);
	  $pdf->Cell(35,3,'Descri��o',0,0);
    $pdf->Cell(20,3,'Nascimento',0,0);
    $pdf->Cell(9,3,'Idade',0,0);    
    $pdf->Cell(46,3,'Titular',0,0);
	  $pdf->Cell(25,3,'CPF',0,0);
	  $pdf->Cell(18,3,'RG',0,0);
    $pdf->Cell(15,3,'Inclus�o',0,0);    
	  $pdf->Cell(30,3,'Nome da m�e',0,1);
    $pdf->Ln(1);

    $pdf->SetFont('Arial','',7);
    
    while (!$sql_incl->eof()) { 
      $pdf->Cell(42,3,$pdf->Copy($sql_incl->result("CNOMEUSUA"),41),0,0);
      $pdf->Cell(18,3,$func->categoriaUsuario($sql_incl->result("CTIPOUSUA")),0,0);
	    $pdf->Cell(18,3,$sql_incl->result("NDCNVATCAD"),0,0);
	    $pdf->Cell(35,3,$pdf->Copy($func->retornaNomePlano($bd,$sql_incl->result("NNUMEPLAN")),34),0,0);
      $pdf->Cell(20,3,$sql_incl->result("DNASCUSUA"),0,0);
      $pdf->Cell( 9,3,$sql_incl->result("IDADEUSUARIO"),0,0);
      $pdf->Cell(46,3,$pdf->Copy($sql_incl->result("TITULAR"),45),0,0);	  
  	  $pdf->Cell(25,3,$sql_incl->result("C_CPFUSUA"),0,0);
	    $pdf->Cell(18,3,$sql_incl->result("C__RGATCAD"),0,0);	  
      $pdf->Cell(15,3,$sql_incl->result("DINCLUSUA"),0,0);
	    $pdf->Cell(30,3,$pdf->Copy($sql_incl->result("CNMAEUSUA"),0,29),0,1);
      $sql_incl->next();
    }
  }

  $sql_canc = new Query($bd);  
  
  $txt_canc = "SELECT NVL(RETORNA_NOME_USUARIO_ATCAD(HSSATCAD.NTITUUSUA),'Excluido') TITULAR, HSSATCAD.CNOMEATCAD CNOMEUSUA,HSSATCAD.C_CPFATCAD C_CPFUSUA,TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,
                      HSSATCAD.CNMAEATCAD CNMAEUSUA,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DDIGIUSUA,HSSUSUA.DINCLUSUA),HSSATCAD.DINCLATCAD),'DD/MM/YYYY')) DINCLUSUA,HSSUSUA.CTIPOUSUA CTIPOUSUA,
                      HSSATCAD.CGRAUATCAD CGRAUUSUA, 2 TIPO,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DSITUUSUA,HSSUSUA.DALTEUSUA),HSSATCAD.DSITUATCAD),'DD/MM/YYYY')) DSITUUSUA,HSSUSUA.NNUMEPLAN NNUMEPLAN,TO_CHAR(HSSATCAD.DDATAATCAD,'DD/MM/YYYY') DDATAATCAD,HSSUSUA.CCODIUSUA,
                      DECODE(CFLAGATCAD,'A',TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY'),'R','Rejeitado','Pendente') DSITUUSUA,IDADE(DNASCATCAD,SYSDATE) IDADEUSUARIO,NDCNVATCAD,C__RGATCAD                      
                 FROM HSSATCAD,HSSUSUA
                WHERE HSSATCAD.NNUMETITU = :contrato
                  AND HSSATCAD.DDATAATCAD >= TO_DATE(:datainicial,'DD/MM/YYYY')
                  AND HSSATCAD.DDATAATCAD < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                  AND HSSATCAD.COPERATCAD = 'C' ".
              $f_situacao.
              $f_operacao2.
              $f_operador.
              "   AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+) ".                              
              $f_locacao.
              "ORDER BY 1,8 desc,2";
  
  $sql_canc->addParam(":contrato",$contrato); 
  $sql_canc->addParam(":datainicial",$datainicial); 
  $sql_canc->addParam(":datafinal",$datafinal); 

  if ($locacao > 0)
    $sql_canc->addParam(":locacao",$locacao); 

  if ($operador > 0)
    $sql_canc->addParam(":operador",$operador);  
    
  $sql_canc->executeQuery($txt_canc);
  
  $pdf->Ln(10);

  if ($sql_canc->count() > 0) {
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'CANCELAMENTOS',0,1);  
    $pdf->Ln(1);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(42,3,'Nome',0,0);
    $pdf->Cell(18,3,'Categoria',0,0);
	  $pdf->Cell(18,3,'D.N.V.',0,0);
	  $pdf->Cell(35,3,'Descri��o',0,0);
    $pdf->Cell(20,3,'Nascimento',0,0);
    $pdf->Cell(9,3,'Idade',0,0);     
    $pdf->Cell(46,3,'Titular',0,0);
	  $pdf->Cell(25,3,'CPF',0,0);
	  $pdf->Cell(18,3,'RG',0,0);	
    $pdf->Cell(15,3,'Cancel.',0,0);   
	  $pdf->Cell(30,3,'Nome da m�e',0,1); 
    $pdf->Ln(1);
	
    $pdf->SetFont('Arial','',7);
    
    while (!$sql_canc->eof()) {   
      $pdf->Cell(42,3,$pdf->Copy($sql_canc->result("CNOMEUSUA"),41),0,0);
      $pdf->Cell(18,3,$func->categoriaUsuario($sql_canc->result("CTIPOUSUA")),0,0);
	    $pdf->Cell(18,3,$sql_canc->result("NDCNVATCAD"),0,0);
	    $pdf->Cell(35,3,$pdf->Copy($func->retornaNomePlano($bd,$sql_canc->result("NNUMEPLAN")),34),0,0);
      $pdf->Cell(20,3,$sql_canc->result("DNASCUSUA"),0,0);
      $pdf->Cell(9,3,$sql_canc->result("IDADEUSUARIO"),0,0);      
      $pdf->Cell(46,3,$pdf->Copy($sql_canc->result("TITULAR"),45),0,0);
	    $pdf->Cell(25,3,$sql_canc->result("C_CPFUSUA"),0,0);
	    $pdf->Cell(18,3,$sql_canc->result("C__RGATCAD"),0,0);	
      $pdf->Cell(15,3,$sql_canc->result("DSITUUSUA"),0,0);
	    $pdf->Cell(30,3,$pdf->Copy($sql_canc->result("CNMAEUSUA"),29),0,1); 
      $sql_canc->next();	 
    }
  }
  
  $sql_alt = new Query($bd);
  
  $txt_alt = "SELECT NVL(RETORNA_NOME_USUARIO_ATCAD(HSSATCAD.NTITUUSUA),'Excluido') TITULAR, HSSATCAD.CNOMEATCAD CNOMEUSUA,HSSATCAD.C_CPFATCAD C_CPFUSUA,TO_CHAR(HSSATCAD.DNASCATCAD,'DD/MM/YYYY') DNASCUSUA,
                     HSSATCAD.CNMAEATCAD CNMAEUSUA,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DDIGIUSUA,HSSUSUA.DINCLUSUA),HSSATCAD.DINCLATCAD),'DD/MM/YYYY')) DINCLUSUA,CTIPOUSUA,
                     HSSATCAD.CGRAUATCAD CGRAUUSUA, 2 TIPO,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DSITUUSUA,HSSUSUA.DALTEUSUA),HSSATCAD.DSITUATCAD),'DD/MM/YYYY')) DSITUUSUA,HSSUSUA.NNUMEPLAN NNUMEPLAN,TO_CHAR(HSSATCAD.DDATAATCAD,'DD/MM/YYYY') DDATAATCAD,HSSUSUA.CCODIUSUA,
                     IDADE(DNASCATCAD,SYSDATE) IDADEUSUARIO                     
                FROM HSSATCAD,HSSUSUA
               WHERE HSSATCAD.NNUMETITU = :contrato
                 AND HSSATCAD.DDATAATCAD >= TO_DATE(:datainicial,'DD/MM/YYYY')
                 AND HSSATCAD.DDATAATCAD < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                 AND HSSATCAD.COPERATCAD = 'A' ".
             $f_situacao.
             $f_operacao3.
             $f_operador.
             "   AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+) ".                              
             $f_locacao.
             "ORDER BY 1,8 desc,2";
  
  $sql_alt->addParam(":contrato",$contrato); 
  $sql_alt->addParam(":datainicial",$datainicial); 
  $sql_alt->addParam(":datafinal",$datafinal); 

  if ($locacao > 0)
    $sql_alt->addParam(":locacao",$locacao); 

  if ($operador > 0)
    $sql_alt->addParam(":operador",$operador);  
    
  $sql_alt->executeQuery($txt_alt);
  
  $pdf->Ln(10);

  if ($sql_alt->count() > 0) {
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'ALTERA��ES',0,1);  
    $pdf->Ln(1);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,3,'Dt. Altera��o',0,0);
	  $pdf->Cell(30,3,'Codigo',0,0);
    $pdf->Cell(52,3,'Nome',0,0);
    $pdf->Cell(20,3,'Categoria',0,0);
    $pdf->Cell(11,3,'Idade',0,0);    
    $pdf->Cell(10,3,'Plano',0,0);
	  $pdf->Cell(50,3,'Descri��o',0,0);
    $pdf->Cell(50,3,'Titular',0,1);
    $pdf->Ln(1);

    $pdf->SetFont('Arial','',8);
    
    while (!$sql_alt->eof()) { 
      $pdf->Cell(20,3,$sql_alt->result("DDATAATCAD"),0,0);        
      $pdf->Cell(30,3,$sql_alt->result("CCODIUSUA"),0,0);    
      $pdf->Cell(52,3,$pdf->Copy($sql_alt->result("CNOMEUSUA"),51),0,0);
      $pdf->Cell(20,3,$func->categoriaUsuario($sql_alt->result("CTIPOUSUA")),0,0);
      $pdf->Cell(11,3,$sql_alt->result("IDADEUSUARIO"),0,0);            
      $pdf->Cell(10,3,$pdf->Copy($func->retornaCodigoPlano($bd,$sql_alt->result("NNUMEPLAN")),9),0,0);
	    $pdf->Cell(50,3,$pdf->Copy($func->retornaNomePlano($bd,$sql_alt->result("NNUMEPLAN")),49),0,0);
      $pdf->Cell(50,3,$pdf->Copy($sql_alt->result("TITULAR"),48),0,1);
      $sql_alt->next();
    }
  }  

  $pdf->Ln(10);
  $pdf->Cell(20,5,"Respons�vel:",0,0,'R');
  $pdf->Cell(100,5,'','B',1);
  $pdf->Ln(5);
  $pdf->Cell(20,3,'Recebido por:','',0);
  $pdf->Cell(100,3,'','B',0);
  $pdf->Cell(20,3,'Data: ___/___/______','',0);

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  
  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 

?>
