<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
  
  $bd  = new Oracle();
  
  $tpl = new Template("../comum/listaBoletos.htm");
  
  $mes             = $seg->antiInjection($_POST['mes']); 
  $ano             = $seg->antiInjection($_POST['ano']);
  
  if ($mes == '00') {
    $dataInicio    = "01/01/".$ano;  
    $dataFim       = "31/12/".$ano;  
  }
  else {
    $dataInicio    = "01/".$mes."/".$ano;  
    $dataFim       = "01/".$mes."/".$ano;  
  }
    
  $mostra_boleto   = $seg->antiInjection($_POST['mostra_boleto']); 
  $atualiza_boleto = $seg->antiInjection($_POST['atualiza_boleto']); 
  $boleto_vencido  = $seg->antiInjection($_POST['boleto_vencido']); 
  $boleto          = $seg->antiInjection($_POST['boleto']); 
 
  $sql = new Query($bd);
  
  if ($mostra_boleto == 'S') {
  
   
    
    if ($boleto == 'S')
      $tpl->block("TITULO_BOLETO");
      
    if ($seg->permissaoOutros($bd,"WEBEMPRESAGERAARQUIVOCSVDAFATURA",true))
      $tpl->block("TITULO_ARQUIVO_CSV");    
      
    if ($_SESSION['apelido_operadora'] == 'promedmg') {
      $tpl->block("TITULO_ARQUIVO");
      $tpl->block("TITULO_ARQUIVO_CSV2");      
    }
    
	  $tpl->block("TITULO_FATURA");
    
		if (($_SESSION["apelido_operadora"] == "clinipam") or ($_SESSION["apelido_operadora"] == "odontopam"))
			$txt_locacao = '';
    else    
	    $txt_locacao = " AND DECODE(:locacao,-1,-1,HSSPAGA.NNUMESETOR) = :locacao ";
	
    $sql_dados = new Query($bd);
    $txt_dados = "SELECT 1 ORDEM FROM SFPESTAB
                   WHERE CNFSEESTAB IN ('4','1','18')
                   UNION ALL
                  SELECT 2 ORDEM FROM FINEMPR
                   WHERE CNFSEEMPR IN ('4','1','18')";	  
    $sql_dados->executeQuery($txt_dados); 
  
    if($sql_dados->count() > 0)      
			$tpl->block("TITULO_NFSE");
    
    if($_SESSION["apelido_operadora"] == "clinipam") {	
      $txt = "SELECT TO_CHAR(DVENCPAGA,'DD/MM/YYYY') VENCIMENTO,CCOMPPAGA COMPETENCIA,
                     NVENCPAGA,
                     CPAGOPAGA,TO_CHAR(DPAGAPAGA,'DD/MM/YYYY') DPAGAPAGA,DVENCPAGA,CPAGOPAGA, NVL(NJUROPAGA,0) + NVL(NMULTPAGA,0) JUROS,
                     NDOCUPAGA,HSSPAGA.NNUMEPAGA,CLREMLOPG,CLFATTITU, NVL(CIMPBLOPG,'S') CIMPBLOPG,
                     DECODE(NVL(CNOTATITU,'N'),'S',DECODE(NNUMENOTAF,NULL,'N','S'),'S') NOTA, HSSTITU.NNUMESTAT,DPARCPAGA,
                     DECODE(NVL(NDEPETITU,0),0,CCUOPCONGE,'2') CCUOPCONGE,NNUMENOTAF,HSSPAGA.NNUMETITU,CDESCLOPG
                FROM HSSPAGA,HSSLOPG,HSSTITU,HSSCNOTA,HSSCONGE
               WHERE HSSTITU.NNUMETITU = :contrato ".
             $txt_locacao .
             "   AND HSSPAGA.DVENCPAGA < LAST_DAY(TO_DATE(:dataFim,'DD/MM/YYYY')) + 1
                 AND HSSPAGA.DVENCPAGA >= TO_DATE(:dataInicio,'DD/MM/YYYY')
                 AND HSSPAGA.DCANCPAGA IS NULL
                 AND HSSPAGA.NNUMEUSUA IS NULL
                 AND DPARCPAGA IS NULL
                 AND CFLAGPAGA <> 'F'
                 AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG
                 AND HSSPAGA.NNUMETITU = NVL(HSSTITU.NFATUTITU,HSSTITU.NNUMETITU)			   
                 AND HSSPAGA.NNUMEPAGA = HSSCNOTA.NNUMEPAGA(+)
                 AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
               ORDER BY DVENCPAGA DESC ";
    }
    else {
      $txt = "SELECT TO_CHAR(DVENCPAGA,'DD/MM/YYYY') VENCIMENTO,CCOMPPAGA COMPETENCIA,
                     NVENCPAGA,
                     CPAGOPAGA,DECODE(DPARCPAGA,NULL,TO_CHAR(DPAGAPAGA,'DD/MM/YYYY'),'Negociada') DPAGAPAGA,DVENCPAGA,CPAGOPAGA, NVL(NJUROPAGA,0) + NVL(NMULTPAGA,0) JUROS,
                     NDOCUPAGA,HSSPAGA.NNUMEPAGA,CLREMLOPG,CLFATTITU, NVL(CIMPBLOPG,'S') CIMPBLOPG,
                     DECODE(CFLAGPAGA,'P','S',DECODE(NVL(CNOTATITU,'N'),'S',DECODE(NNUMENOTAF,NULL,'N','S'),'S')) NOTA, HSSTITU.NNUMESTAT,DPARCPAGA,
                     DECODE(NVL(NDEPETITU,0),0,CCUOPCONGE,'2') CCUOPCONGE,NNUMENOTAF,HSSPAGA.NNUMETITU,CDESCLOPG,CORIGPAGA
                FROM HSSPAGA,HSSLOPG,HSSTITU,HSSCNOTA,HSSCONGE
               WHERE HSSPAGA.NNUMETITU = :contrato ".
             $txt_locacao .
             "   AND HSSPAGA.DVENCPAGA <= LAST_DAY(TO_DATE(:dataFim,'DD/MM/YYYY')) + 1
                 AND HSSPAGA.DVENCPAGA >= TO_DATE(:dataInicio,'DD/MM/YYYY')
                 AND HSSPAGA.DCANCPAGA IS NULL
                 AND HSSPAGA.NNUMEUSUA IS NULL
                 AND HSSPAGA.NVENCPAGA > 0
                 AND CFLAGPAGA <> 'F'
                 AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG
                 AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU			   
                 AND HSSPAGA.NNUMEPAGA = HSSCNOTA.NNUMEPAGA(+)
                 AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
                ORDER BY DVENCPAGA DESC ";		
    }
      
    $sql->clear();                           
    $sql->addParam(":dataInicio",$dataInicio);
    $sql->addParam(":dataFim",$dataFim);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    
	  if (($_SESSION["apelido_operadora"] <> "clinipam") and ($_SESSION["apelido_operadora"] <> "odontopam"))
      $sql->addParam(":locacao",$_SESSION['id_locacao']);  
      
    $sql->executeQuery($txt);
   
    $imprime_boleto = false;
    
    if($_SESSION['apelido_operadora'] == 'agemed') 
      $tpl->block("SEGVIA");     
              
    while (!$sql->eof()) {
    
      if ($atualiza_boleto == 'S') {
        $jpaga = new Query($bd);
        $txt_jpaga = "SELECT TO_CHAR(DVENCJPAGA,'DD/MM/YYYY') DVENCJPAGA, TO_CHAR(DVENCJPAGA,'MM/YYYY') COMPETENCIA, NVENCJPAGA,HSSJPAGA.NNUMEJPAGA,CLREMLOPG
                        FROM HSSMJPAG,HSSJPAGA,HSSLOPG
                       WHERE HSSMJPAG.NNUMEPAGA = :id
                         AND HSSJPAGA.DVENCJPAGA >= TRUNC(SYSDATE)                     
                         AND HSSMJPAG.NNUMEJPAGA = HSSJPAGA.NNUMEJPAGA 
                         AND HSSJPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG 
                       ORDER BY 3 DESC";
        $jpaga->addParam(":id",$sql->result("NNUMEPAGA"));
        $jpaga->executeQuery($txt_jpaga);
      
        if ($jpaga->count() > 0) {
          $vencimento = $jpaga->result("DVENCJPAGA");
          $competencia = $jpaga->result("COMPETENCIA");
          $valor      = $jpaga->result("NVENCJPAGA");
          $id_jpaga   = $jpaga->result("NNUMEJPAGA");
        } 
        else {
          $vencimento = $sql->result("VENCIMENTO");
          $competencia = $sql->result("COMPETENCIA");
          $valor      = $sql->result("NVENCPAGA");      
          $id_jpaga   = 0;
        }  
      } else {
        $vencimento   = $sql->result("VENCIMENTO");
        $competencia = $sql->result("COMPETENCIA");
        $valor = $formata->formataNumero($sql->result("NVENCPAGA"),2);
        $id_jpaga     = 0;      
      }
                
      $obj_boleto = new Boleto($sql->result("NNUMEPAGA"),$id_jpaga);
       
      if (($boleto == "S") and
          ($sql->result("CPAGOPAGA") <> "S") and
          ($obj_boleto->ImprimeBoleto(($boleto_vencido == 'S'))) and      
          (($sql->result("NOTA") == "S" ) or 
           (($sql->result("NOTA") == "N" ) and (($_SESSION['apelido_operadora'] == 'agemed') or ($_SESSION['apelido_operadora'] == 'nossaSaude') or $seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMIRBOLETOSEMPRECISARDENOTAFISCAL",true))) 
           )
		     ){
        $imprime_boleto = true;
      } else
        $imprime_boleto = false;

      $tpl->BOLETO_DOCUMENTO  = $sql->result("NDOCUPAGA");
      $tpl->BOLETO_VENCIMENTO = $vencimento;
      $tpl->BOLETO_COMPETENCIA = $competencia;
      $tpl->BOLETO_LOCAL      = $sql->result("CDESCLOPG");
      $tpl->BOLETO_ID         = $sql->result("NNUMEPAGA");

      if ((($imprime_boleto) and ($_SESSION['apelido_operadora'] <> 'saudemed')) or 
         (($_SESSION['apelido_operadora'] == 'saudemed') and ($imprime_boleto) and ( $sql->result("CORIGPAGA") <> 'N'))){
        $tpl->block("ATUALIZAR");            
      }   

      if (($_SESSION['apelido_operadora'] == 'agemed') and ($sql->result("CLREMLOPG") == 'C'))
        $tpl->block("ATUALIZA");
      else if (($_SESSION['apelido_operadora'] == 'agemed') and ($sql->result("CLREMLOPG") == 'H'))  
        $tpl->block("ATUALIZA2");
      else if (($_SESSION['apelido_operadora'] == 'agemed') and ($sql->result("CLREMLOPG") == 'SA'))  
        $tpl->block("ATUALIZA3");           

      $tpl->BOLETO_PAGAMENTO  = $sql->result("DPAGAPAGA");
      $tpl->BOLETO_VALOR      = $valor;   

      if ((($sql->result("NOTA") == "S" or 
            $sql->result("CPAGOPAGA") == "S" or
	          $_SESSION['apelido_operadora'] == 'agemed' or
            $_SESSION['apelido_operadora'] == 'nossaSaude' or 
			      $seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMIRBOLETOSEMPRECISARDENOTAFISCAL",false)) and 
            $_SESSION['apelido_operadora'] <> 'clinipam' ) or 
		      ((($sql->result("NOTA") == "S") or 
             $sql->result("CPAGOPAGA") == "S" or
 			       $seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMIRBOLETOSEMPRECISARDENOTAFISCAL",false)) and 
             ($_SESSION['apelido_operadora'] == 'clinipam' and $_SESSION['logando'] != 'PF' ))){
                if ($_SESSION['apelido_operadora'] == 'sampes')      
                  $tpl->block("FATURA_SAMP");
                else
                  $tpl->block("FATURA");
      }           

      if ($seg->permissaoOutros($bd,"WEBEMPRESAGERAARQUIVOCSVDAFATURA",true)) {

        if ($sql->result("CCUOPCONGE") == '2')
          $tpl->BOLETO_TIPO = '3';
        else if ($_SESSION['apelido_operadora'] == 'promedmg')
          $tpl->BOLETO_TIPO = '2';
        else if ($_SESSION['apelido_operadora'] == 'unimedFranca')
          $tpl->BOLETO_TIPO = '4';
        else
          $tpl->BOLETO_TIPO = '';
         
        if (($sql->result("NOTA") == "S") or ($sql->result("CPAGOPAGA") == "S") or ($_SESSION['apelido_operadora'] == 'agemed'))
          $tpl->block("ARQUIVO_CSV");      
      }
      
      if ($_SESSION['apelido_operadora'] == 'promedmg') {

        if ((($sql->result("CCUOPCONGE") == '2') or ($sql->result("NNUMETITU") == 30680879) or ($sql->result("NNUMETITU") == 30893837)) and ($sql->result("NOTA") == "S")) {
          $tpl->BOLETO_ARQUIVO = 'astromig.php';
          $tpl->block("ARQUIVO");
        } else {

          $dir = "download/".$_SESSION['codigo_contrato']."/*.rar";

          foreach (glob($dir,GLOB_NOCHECK ) as $filename) {
  
            if (file_exists($filename)) {
              if (substr(basename($filename),0,16) == $formata->acrescentaZeros($sql->result("NDOCUPAGA"),16)) {
                $tpl->BOLETO_ARQUIVO = $filename;
                $tpl->block("ARQUIVO");
              }
            }
          }
        }
        
        $tpl->block("ARQUIVO_CSV2");         
      }
      
	    $sql1 = new Query($bd);
	    $txt_nota = "SELECT NNOTANOTAF,CCDVENOTAF,NNUMEESTAB 
                     FROM HSSNOTAF,HSSNFRPS
	                  WHERE HSSNOTAF.NNUMENOTAF = :nota
					            AND HSSNOTAF.NNUMENOTAF = HSSNFRPS.NNUMENOTAF(+) 
					            AND CCDVENOTAF IS NOT NULL ";	  
	    $sql1->addParam(":nota",$sql->result("NNUMENOTAF"));
	    $sql1->executeQuery($txt_nota);
      	  
      $sql_dados = new Query($bd);
      $txt_dados = "SELECT SOMENTE_NUMEROS_STRING(CCGC_ESTAB) CCGC_EMPR,CINSMESTAB CINSMEMPR,CNFSEESTAB CNFSEEMPR, 1 ORDEM FROM SFPESTAB,HSSCONF
                     WHERE NNUMEESTAB = :estabelecimento
                     UNION ALL
                    SELECT SOMENTE_NUMEROS_STRING(CCGC_EMPR) CCGC_EMPR,CINSMEMPR,CNFSEEMPR, 2 ORDEM FROM FINEMPR,HSSCONF
                     ORDER BY 4";	  
      $sql_dados->addParam(":estabelecimento",$sql1->result("NNUMEESTAB"));
      $sql_dados->executeQuery($txt_dados); 
    
	    if($sql1->count() > 0){          
        if ($sql_dados->result("CNFSEEMPR") == '04') {
          $tpl->BOLETO_NOTA = 0;
          $tpl->IMAGEM = "nfse-web.jpg";
          $tpl->IMG_W = "25px";
          $tpl->IMG_H = "25px";
          
          if (($sql1->result("NNUMEESTAB")) <> 2313422) { 
            $tpl->LINK_NFSE = "http://isscuritiba.curitiba.pr.gov.br/portalnfse/Default.aspx?doc=".$_SESSION['cnpj_operadora']."&num=".$sql1->result("NNOTANOTAF")."&cod=".$sql1->result("CCDVENOTAF");
          } else if (($sql1->result("NNUMEESTAB")) == 2313422) { 
            $tpl->LINK_NFSE = "http://visualizar.ginfes.com.br/report/consultarNota?__report=nfs_ver4&cdVerificacao=".$sql1->result("CCDVENOTAF")."&numNota=".$sql1->result("NNOTANOTAF");
          } 
          $tpl->block("NFSE");	
          
        } 
        else if ($sql_dados->result("CNFSEEMPR") == '01') {
          $tpl->BOLETO_NOTA = $sql->result("NNUMENOTAF");
          $tpl->LINK_NFSE = "nfseBhImp.php";
          $tpl->IMAGEM = "nfse-web.jpg";
          $tpl->IMG_W = "25px";
          $tpl->IMG_H = "25px";
          $tpl->block("NFSE");	        
        }
        else if ($sql_dados->result("CNFSEEMPR") == '18') {
          $tpl->LINK_NFSE = "https://isse.maringa.pr.gov.br/print/nfse/cnpj/".$sql_dados->result("CCGC_EMPR")."/codval/".$sql1->result("CCDVENOTAF")."/numnfe/".$sql1->result("NNOTANOTAF")."/";        
          $tpl->BOLETO_NOTA = $sql->result("NNUMENOTAF");
          $tpl->IMAGEM = "nfse-web.jpg";
          $tpl->IMG_W = "25px";
          $tpl->IMG_H = "25px";
          $tpl->block("NFSE");	        
        }        
	    }
      
      $tpl->block("BOLETO");
      $sql->next();
    }
    $tpl->block("MOSTRA_BOLETOS");      
  } else {
    $tpl->block("MOSTRA_ERRO");  
  }
  
  echo utf8_encode($tpl->parse());   
  $bd->close();
  
?>
  
  
  