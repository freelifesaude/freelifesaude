<?php
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();

  $_SESSION['titulo'] = "INCLUS�O DE BENEFICI�RIOS";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/formulario.htm");
    
  $id                    = '';
  $nome                  = '';
  $categoria             = '';
  $data_inclusao         = '';
  $nascimento            = '';
  $estadonasc            = '';
  $cidadenasc            = '';
  $sexo                  = '';
  $grau                  = '';
  $estado_civil          = '';
  $cpf                   = '';
  $rg                    = '';
  $expedicao             = '';
  $orgao                 = '';
  $pis                   = '';
  $csus                  = '';
  $ndnv                  = '';
  $mae                   = '';
  $pai                   = '';
  $uniao                 = '';
  $titular               = '';
  $matricula             = '';
  $profissao             = '';
  $id_operadora_repasse  = '';
  $setor                 = '';
  $salario               = '';
  $admissao              = '';
  $demissao              = '';
  $locacao               = '';
  $centro_custo          = '';
  $ini_afast             = '';
  $motivo                = '';
  $fim_afast             = '';
  $plano                 = '';
  $acomodacao            = '';
  $aditivos_selecionados = array();
  $telefone1             = '';
  $telefone2             = '';
  $celular               = '';
  $email                 = '';
  $cep                   = '';
  $tipo_logradouro       = '';
  $logradouro            = '';
  $numero                = '';
  $complemento           = '';
  $bairro                = '';
  $estado                = '';
  $cidade                = '';
  $motivo_canc           = '';
  $data_cancelamento     = '';
  $anexo                 = array();
  $observacao1           = '';
  $melhor_dia_inclusao   = '';
  $continuar             = '';
  $observacao            = '';
  $peso                  = '';
  $altura                = '';
  $cidadeatendimento     = '';
    
  $pode_alterar_plano_dependente      = $seg->permissaoOutros($bd,"WEBEMPRESAPODEALTERARPLANODODEPENDENTE",false);
    
  if (isset($_POST['enviar'])) { 
    $nome                  = $seg->antiInjection(strtoupper($_POST['nome']));
    $categoria             = $seg->antiInjection($_POST['categoria']);
    
    if (isset($_POST['data_inclusao']))
      $data_inclusao       = $seg->antiInjection($_POST['data_inclusao']);
    
    $nascimento            = $seg->antiInjection($_POST['nascimento']);
    $estadonasc            = $seg->antiInjection($_POST['estado_nasc']);    
    $cidadenasc            = $seg->antiInjection($_POST['cidade_nasc']);    
    $sexo                  = $seg->antiInjection($_POST['sexo']);
    $grau                  = $seg->antiInjection($_POST['grau']);
    $estado_civil          = $seg->antiInjection($_POST['estado_civil']);
    $cpf                   = $seg->antiInjection($_POST['cpf']);
    $rg                    = $seg->antiInjection($_POST['rg']);
    $expedicao             = $seg->antiInjection($_POST['expedicao']);
    $orgao                 = $seg->antiInjection(strtoupper($_POST['orgao']));
    $pis                   = $seg->antiInjection($_POST['pis']);
    $csus                  = $seg->antiInjection($_POST['csus']);
    $ndnv                  = $seg->antiInjection($_POST['ndnv']);
    $mae                   = $seg->antiInjection(strtoupper($_POST['mae'])); 
    $pai                   = $seg->antiInjection(strtoupper($_POST['pai']));   
    $uniao                 = $seg->antiInjection($_POST['uniao']);
    $titular               = $seg->antiInjection($_POST['titular']);
    $matricula             = $seg->antiInjection($_POST['matricula']);
    $cidadeatendimento     = $seg->antiInjection($_POST['cidadeatendimento']);
    
    if (isset($_POST['profissao']))    
      $profissao           = $seg->antiInjection($_POST['profissao']);
	  
    if (isset($_POST['operadora_repasse']))    
      $id_operadora_repasse   = $seg->antiInjection($_POST['operadora_repasse']);	  
      
    if (isset($_POST['setor']))      
      $setor               = $seg->antiInjection($_POST['setor']);
      
    $salario               = $seg->antiInjection($_POST['salario']);
    $admissao              = $seg->antiInjection($_POST['admissao']);
    
    if (isset($_POST['demissao']))
      $demissao            = $seg->antiInjection($_POST['demissao']);
    
    if ((isset($_SESSION['id_locacao'])) and ($_SESSION['id_locacao'] > 0))
      $locacao             = $_SESSION['id_locacao'];
    else if (isset($_POST['locacao']))
      $locacao             = $seg->antiInjection($_POST['locacao']);
      
    if (isset($_POST['centro_custo']))
      $centro_custo        = $seg->antiInjection($_POST['centro_custo']);
      
    $ini_afast             = '';
    $motivo                = '';
    $fim_afast             = '';
    $plano                 = $seg->antiInjection($_POST['plano']);
    $acomodacao            = $seg->antiInjection($_POST['acomodacao']);
    $telefone1             = $seg->antiInjection($_POST['telefone1']);
    $telefone2             = $seg->antiInjection($_POST['telefone2']);
    $celular               = $seg->antiInjection($_POST['celular']);
    $email                 = $seg->antiInjection($_POST['email']);
    $cep                   = $seg->antiInjection($_POST['cep']);
    $tipo_logradouro       = $seg->antiInjection($_POST['tipo_logradouro']);
    $logradouro            = $seg->antiInjection(strtoupper($_POST['logradouro']));
    $numero                = $seg->antiInjection($_POST['numero']);
    $complemento           = $seg->antiInjection(strtoupper($_POST['complemento']));
    $bairro                = $seg->antiInjection(strtoupper($_POST['bairro']));
    $estado                = $seg->antiInjection($_POST['estado']);
    $cidade                = $seg->antiInjection($_POST['cidade']);
    $motivo_canc           = $seg->antiInjection($_POST['motivo_cancelamento']);
    $data_cancelamento     = '';
    $observacao1           = $seg->antiInjection($_POST['observacao']);
    $melhor_dia_inclusao   = $seg->antiInjection($_POST['melhor_dia_inclusao']);
    $continuar             = $seg->antiInjection($_POST['continuar']);
    $peso                  = $seg->antiInjection($_POST['peso']);
    $altura                = $seg->antiInjection($_POST['altura']);
      
    if (($plano == "") or ($acomodacao == "")) {
      if ((!$pode_alterar_plano_dependente) and (($categoria <> 'T') and ($categoria <> 'F'))) {
        $txt = "SELECT HSSUSUA.NNUMEPLAN,NVL(HSSUSUA.NNUMEACOM,NVL(HSSTITU.NNUMEACOM,HSSPLAN.NNUMEACOM)) NNUMEACOM
                  FROM HSSUSUA,HSSTITU,HSSPLAN
                 WHERE NNUMEUSUA = :titular
                   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                 UNION ALL
                SELECT NNUMEPLAN,NNUMEACOM FROM HSSATCAD
                 WHERE NNUMEUSUA = :titular
                   AND COPERATCAD = 'I'
                   AND CFLAGATCAD IS NULL ";

        $sql2 =  new Query($bd);
        $sql2->addParam(":titular",$titular);      
        $sql2->executeQuery($txt);
        
        $plano           = $sql2->result("NNUMEPLAN");
        $acomodacao      = $sql2->result("NNUMEACOM");
      }
    }
         
    if (isset($_POST["aditivos"])) {
      foreach($_POST["aditivos"] as $aditivo1) {
        
        if (isset($_POST["aditivo_".$aditivo1]))
          $id_aditivo = $_POST["aditivo_".$aditivo1];
        else
          $id_aditivo = 0;
        
        if ($id_aditivo > 0)
          $aditivos_selecionados[$id_aditivo] = $aditivo1;
        
        if (isset($_POST["aditivo2_".$aditivo1]))
          $id_aditivo2 = $_POST["aditivo2_".$aditivo1];
        else
          $id_aditivo2 = 0;        
          
        if ($id_aditivo2 > 0)
          $aditivos_selecionados[$id_aditivo2] = $aditivo1;                 
      }
    }       
    
    
    $erro = '';

    if ($_FILES['upload1']['name'] <> '')
      $erro .= $util->validaExtensaoArquivo($_FILES['upload1']['name']);
          
    if ($erro == '') {
	    if ((($_SESSION['apelido_operadora'] == 'clinipam') or 
           ($_SESSION['apelido_operadora'] == 'odontopam')) and ($continuar <> 'S')) {
  	    if(substr($melhor_dia_inclusao,0,2) <> substr($data_inclusao,0,2)){
	        $tpl->CADASTRO_MENSAGEM_ALERTA = "Prezado cliente, para o seu contrato, a melhor data para inclus�o � dia " . substr($melhor_dia_inclusao,0,2) . ". Para prosseguir clique novamente no bot�o incluir" ;
          $tpl->block("ALERTA");
		      $tpl->CONTINUAR = 'S';
	      } 
        else {	    
  	      $retorno = $func->insereUsuario ($bd,$_SESSION['id_contrato'],$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,$titular,
                                           $matricula,$salario,$admissao,$locacao,$plano,$acomodacao,
                                           $telefone1,$telefone2,$celular,$email,
                                           $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,$data_inclusao,
                                           $estado_civil,$profissao,$setor,'E',0,0,0,0,$_FILES['upload1'],$csus,$ndnv,$observacao1,$uniao,
                                           $aditivos_selecionados,$centro_custo,'',$peso,$altura,$id_operadora_repasse,$cidadeatendimento);
                             
          if ($retorno['erro'] <> '') {
            $tpl->MENSAGEM = $retorno['erro'];
            $tpl->block("MSG");
          } 
          else {
            $filtros = array();
            $filtros['op']   = 100;
            $filtros['prot'] = $retorno['id'];
          
            $_SESSION['filtrosConfirmacao'] = $filtros;
        
            $util->redireciona('confirmacao.php?idSessao='.$_GET['idSessao']);
		      }
	      }
	    } 
      else {        
        $retorno = $func->insereUsuario ($bd,$_SESSION['id_contrato'],$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,$titular,
                                         $matricula,$salario,$admissao,$locacao,$plano,$acomodacao,
                                         $telefone1,$telefone2,$celular,$email,
                                         $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,$data_inclusao,
                                         $estado_civil,$profissao,$setor,'E',0,0,0,0,$_FILES['upload1'],$csus,$ndnv,$observacao1,$uniao,
                                         $aditivos_selecionados,$centro_custo,'',$peso,$altura,$id_operadora_repasse,$cidadeatendimento);

        if ($retorno['erro'] <> '') {
          $tpl->MENSAGEM = $retorno['erro'];
          $tpl->block("MSG");
        } 
        else {
          $filtros = array();
          $filtros['op']   = 100;
          $filtros['prot'] = $retorno['id'];
          
          $_SESSION['filtrosConfirmacao'] = $filtros;
        
          $util->redireciona('confirmacao.php?idSessao='.$_GET['idSessao']);
        }
	    }
    }
    else {
      $tpl->CLASSE = "alert-error";    
      $tpl->MENSAGEM = $erro;
      $tpl->block("MSG");
    }          
  }
  else {   
    if (($func->nomeEstado($estado) == "") and ($_SESSION['apelido_operadora'] <> 'saudemed'))
      $estado = $_SESSION['uf_operadora'];  
  }
     
  /* Montagem da Tela */
  
  /* Select Categoria */
  $sql = new Query($bd);  
  $txt = "SELECT CTIPORCATE FROM HSSRCATE WHERE NNUMETITU = :contrato ";
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  if ($sql->count() > 0) {
        
    $tpl->CADASTRO_CATEGORIA_ID        = 'T';
    $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario('T');
    $tpl->block("CADASTRO_ITEM_CATEGORIA");
          
    while (!$sql->eof()) {
      $tpl->CADASTRO_CATEGORIA_ID        = $sql->result("CTIPORCATE");
      $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($sql->result("CTIPORCATE"));
      $tpl->block("CADASTRO_ITEM_CATEGORIA");
      $sql->next();
    }
  } 
  else {
    if (($_SESSION['apelido_operadora'] == 'clinipam') or
        ($_SESSION['apelido_operadora'] == 'odontopam') or
        ($_SESSION['apelido_operadora'] == 'saudeescolar')) {
      $categorias = array('T','D','A','U','E','L');
    } else if ($_SESSION['apelido_operadora'] == 'unimedSalto') {
	  $categorias = array('T','D','U','E','L');
    } else if ($_SESSION['apelido_operadora'] == 'unimedLimeira') {
	  $categorias = array('T','D','E');
	} else {
	  $categorias = array('T','F','D','A','U','E','L');	
    }
    
    foreach ($categorias as $c) {        
      $tpl->CADASTRO_CATEGORIA_ID        = $c;
      $tpl->CADASTRO_CATEGORIA_DESCRICAO = $func->categoriaUsuario($c);
      $tpl->block("CADASTRO_ITEM_CATEGORIA");
    }     
  } 

  if ($categoria == '')  
    $categoria = 'T';  
    
  /* Select Grau */  
  $txt = "SELECT CGRAURPARE FROM HSSRPARE
           WHERE NNUMETITU = :contrato
           UNION ALL
          SELECT CGRAURPARE FROM HSSTITU,HSSRPARE
           WHERE HSSTITU.NNUMETITU = :contrato
             AND HSSTITU.NNUMEPLAN = HSSRPARE.NNUMEPLAN
             AND HSSRPARE.NNUMETITU IS NULL
             AND 0 = (SELECT COUNT(*) FROM HSSRPARE
                       WHERE NNUMETITU = :contrato)";
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  if ($sql->count() > 0) {
    while (!$sql->eof()) {
      $tpl->CADASTRO_GRAU_ID        = $sql->result("CGRAURPARE");
      $tpl->CADASTRO_GRAU_DESCRICAO = $func->grauDeParentesco($sql->result("CGRAURPARE"));
      $tpl->block("CADASTRO_ITEM_GRAU");
      $sql->next();
    }
  } 
  else {
    $graus = array('F','E','P','M','I','A','T','R','C','G','N','B','S','O','D','H','J');
    
    foreach ($graus as $g) {        
      $tpl->CADASTRO_GRAU_ID        = $g;
      $tpl->CADASTRO_GRAU_DESCRICAO = $func->grauDeParentesco($g);
      $tpl->block("CADASTRO_ITEM_GRAU");
    }     
  }  
  
  /* Select estado civil */  
  $estados_civis = array('S','C','V','D','U','A','O');
    
  foreach ($estados_civis as $e) {        
    $tpl->CADASTRO_ESTADO_CIVIL_ID        = $e;
    $tpl->CADASTRO_ESTADO_CIVIL_DESCRICAO = $func->estadoCivil($e);
    $tpl->block("CADASTRO_ITEM_ESTADO_CIVIL");
  }
  
  /* Select Titular */
  $txt = "SELECT NNUMEUSUA,CNOMEUSUA,DECODE(CCHAPUSUA,NULL,NULL,' - ' || CCHAPUSUA) CCHAPUSUA
            FROM HSSUSUA
           WHERE NNUMETITU = :contrato
             AND CTIPOUSUA IN ('T','F') 
             AND CSITUUSUA = 'A'
           UNION ALL
          SELECT DISTINCT NNUMEUSUA,CNOMEATCAD,DECODE(CCHAPATCAD,NULL,NULL,' - ' || CCHAPATCAD) CCHAPUSUA
            FROM HSSATCAD
           WHERE NNUMETITU = :contrato
             AND COPERATCAD = 'I'
             AND CFLAGATCAD IS NULL
             AND NNUMEUSUA = NTITUUSUA
           ORDER BY 2 ";  
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);  
  
  while (!$sql->eof()) {
    $tpl->CADASTRO_TITULAR_ID   = $sql->result("NNUMEUSUA");
    $tpl->CADASTRO_TITULAR_NOME = $sql->result("CNOMEUSUA").$sql->result("CCHAPUSUA");
    $tpl->block("CADASTRO_ITEM_TITULAR");
    $sql->next();
  }
  
  $tpl->block("LOCALIZA_TITULAR");  
  $tpl->block("TITULAR");   
  
  /* Select profissao */
  if ($_SESSION['apelido_operadora'] <> 'sampes') {
  $txt = "SELECT NNUMEPROF,INITCAP(CDESCPROF) CDESCPROF
            FROM HSSPROF 
           ORDER BY 2";  
  $sql = new Query($bd);
  $sql->executeQuery($txt); 
  

  if ($sql->count() > 0) {  
  
    while (!$sql->eof()) {
      $tpl->CADASTRO_PROFISSAO_ID        = $sql->result("NNUMEPROF");
      $tpl->CADASTRO_PROFISSAO_DESCRICAO = $sql->result("CDESCPROF");
      $tpl->block("CADASTRO_ITEM_PROFISSAO");
      $sql->next();
    }
    
    $tpl->block("CADASTRO_PROFISSOES");
  }
  }
  

  /* Select operadoras de repasse */
  $txt = "SELECT NNUMEPSAUD, CNOMEPSAUD
            FROM HSSPSAUD 
           ORDER BY 2";  
    
  $sql = new Query($bd);
  $sql->executeQuery($txt); 

  if ($sql->count() > 0) {  
  
    while (!$sql->eof()) {
      $tpl->CADASTRO_OPERADORA_ID        = $sql->result("NNUMEPSAUD");
      $tpl->CADASTRO_OPERADORA_DESCRICAO = $sql->result("CNOMEPSAUD");
      $tpl->block("CADASTRO_ITEM_OPERADORA");
      $sql->next();
    }
    if ($seg->permissaoOutros($bd,"WEBEMPRESAMOSTRAOPERADORAREPASSENAINCLUSO", true))
      $tpl->block("OPERADORAS_REPASSE");

  }  
  
  /* Select setor */	   
  $txt = "SELECT HSSDEPAR.NNUMEDEPAR,INITCAP(CNOMEDEPAR) CNOMEDEPAR           
            FROM HSSDEPAR,HSSDEPEM,HSSEMPR,HSSTITU
		       WHERE HSSTITU.NNUMETITU = :contrato
			       AND HSSDEPAR.NNUMEDEPAR = HSSDEPEM.NNUMEDEPAR
     		     AND HSSDEPEM.NNUMEEMPR = HSSEMPR.NNUMEEMPR
		         AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR			
           ORDER BY 2"; 		   
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt); 

  if ($sql->count() > 0) {  
  
    while (!$sql->eof()) {
      $tpl->CADASTRO_SETOR_ID        = $sql->result("NNUMEDEPAR");
      $tpl->CADASTRO_SETOR_DESCRICAO = $sql->result("CNOMEDEPAR");
      $tpl->block("CADASTRO_ITEM_SETOR");
      $sql->next();
    }    
    $tpl->block("CADASTRO_SETORES");
  }  
  
  /* Select Locacao */
  if ($_SESSION['id_locacao'] <= 0) {
    $txt = "SELECT CNOMESETOR,NNUMESETOR 
              FROM HSSTITU,HSSEMPR,HSSSETOR
             WHERE HSSTITU.NNUMETITU = :contrato
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
               AND HSSEMPR.NNUMEEMPR = HSSSETOR.NNUMEEMPR
               AND NVL(CSITUSETOR,'A') = 'A'";               
    $sql = new Query($bd);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);  
    
    if ($sql->count() > 0) {
      while (!$sql->eof()) {
        $tpl->CADASTRO_LOCACAO_ID        = $sql->result("NNUMESETOR");
        $tpl->CADASTRO_LOCACAO_DESCRICAO = $sql->result("CNOMESETOR");
        $tpl->block("CADASTRO_ITEM_LOCACAO");
        $sql->next();
      } 
    
    	$tpl->block("CADASTRO_LOCACOES");	      
    }
  }  
  
  /* Select centro de custo */
  $txt = "SELECT NNUMECECUS,INITCAP(CCODICECUS || ' - ' || CDESCCECUS) CDESCCECUS
            FROM HSSCECUS,HSSEMPR,HSSTITU
           WHERE HSSCECUS.NNUMEEMPR = HSSEMPR.NNUMEEMPR
             AND HSSEMPR.NNUMEEMPR = HSSTITU.NNUMEEMPR
             AND HSSTITU.NNUMETITU = :contrato
           ORDER BY 2";  
  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt); 

  if ($sql->count() > 0) {  

    while (!$sql->eof()) {
      $tpl->CADASTRO_CENTRO_CUSTO_ID        = $sql->result("NNUMECECUS");
      $tpl->CADASTRO_CENTRO_CUSTO_DESCRICAO = $sql->result("CDESCCECUS");
      $tpl->block("CADASTRO_ITEM_CENTRO_CUSTO");
      $sql->next();
    }    
    $tpl->block("CADASTRO_CENTRO_CUSTOS");
  }  
  
  /* Select Plano */
  
  if ($_SESSION['id_locacao'] > 0) {
    $txt = "SELECT COUNT(*) QTDE
              FROM HSSPLTIT,HSSPLAN
             WHERE HSSPLTIT.NNUMETITU = :contrato
               AND HSSPLTIT.NNUMESETOR = :locacao
               AND HSSPLTIT.NNUMEPLAN = HSSPLAN.NNUMEPLAN
               AND HSSPLTIT.CMWEBPLTIT = 'S' ";
    $sql = new Query($bd);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->addParam(":locacao",$_SESSION['id_locacao']);
    $sql->executeQuery($txt); 
    
    $qtde = $sql->result("QTDE");
       
    $sql = new Query($bd);       
    if ($qtde > 0) {    
      $txt = "SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSPLTIT.NNUMEPLAN
                FROM HSSPLTIT,HSSPLAN
               WHERE HSSPLTIT.NNUMETITU = :contrato
                 AND HSSPLTIT.NNUMESETOR = :locacao
                 AND HSSPLTIT.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                 AND HSSPLTIT.CMWEBPLTIT = 'S' ";                        
      $sql->addParam(":locacao",$_SESSION['id_locacao']);
    } 
    else {
      $txt = "SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSPLTIT.NNUMEPLAN
              FROM HSSPLTIT,HSSPLAN
             WHERE HSSPLTIT.NNUMETITU = :contrato
               AND HSSPLTIT.NNUMEPLAN = HSSPLAN.NNUMEPLAN
               AND HSSPLTIT.NNUMESETOR IS NULL
               AND HSSPLTIT.CMWEBPLTIT = 'S'
             UNION
            SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSTITU.NNUMEPLAN
              FROM HSSTITU,HSSPLAN
             WHERE HSSTITU.NNUMETITU = :contrato
               AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";      
    }               
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);      
  } 
  else if ($_SESSION['apelido_operadora'] == 'consaude') {
    $txt = "SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,NNUMEPLAN
              FROM HSSPLAN";  
    $sql = new Query($bd);
    $sql->executeQuery($txt);  
  }
  else {
    $txt = "SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSPLTIT.NNUMEPLAN
              FROM HSSPLTIT,HSSPLAN
             WHERE HSSPLTIT.NNUMETITU = :contrato
               AND HSSPLTIT.NNUMEPLAN = HSSPLAN.NNUMEPLAN
               AND HSSPLTIT.CMWEBPLTIT = 'S'
             UNION
            SELECT CCODIPLAN,INITCAP(CDESCPLAN) CDESCPLAN,HSSTITU.NNUMEPLAN
              FROM HSSTITU,HSSPLAN
             WHERE HSSTITU.NNUMETITU = :contrato
               AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN";  
    $sql = new Query($bd);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);  
  }
  
  if ($sql->count() > 1 ) {
    $tpl->CADASTRO_PLANO_ID        = "";
    $tpl->CADASTRO_PLANO_DESCRICAO = "Selecione o plano";
  }
  
  while (!$sql->eof()) {
    if ($plano == '')
      $plano = $sql->result("NNUMEPLAN");
      
    $tpl->CADASTRO_PLANO_ID        = $sql->result("NNUMEPLAN");
    $tpl->CADASTRO_PLANO_DESCRICAO = $sql->result("CCODIPLAN")." - ".$sql->result("CDESCPLAN");
    $tpl->block("CADASTRO_ITEM_PLANO");
    $sql->next();
  }  
  
  /* Select aditivos */  
  $txt = "SELECT CDESCTXMEN,NNUMETXMEN
            FROM HSSTXMEN
           WHERE CCOMWTXMEN IN ('E','B')
           ORDER BY 1";               
  $sql = new Query($bd);
  $sql->executeQuery($txt);  
  
  if ($sql->count() > 0) {
    $col = 1;
    while (!$sql->eof()) {
      if ($col == 1) {
        $tpl->ID_ADITIVO   = $sql->result("NNUMETXMEN");
        $tpl->DESC_ADITIVO = $sql->result("CDESCTXMEN");
        
        if ((sizeOf($aditivos_selecionados) > 0) and ($aditivos_selecionados[$sql->result("NNUMETXMEN")] > 0))
          $tpl->ADITIVO_CHECKED = "checked";
        else
          $tpl->ADITIVO_CHECKED = "";
          
        $tpl->block("ADITIVO_COL1");
      } 
      else {
        $tpl->ID_ADITIVO2   = $sql->result("NNUMETXMEN");
        $tpl->DESC_ADITIVO2 = $sql->result("CDESCTXMEN");
        
        if ((sizeOf($aditivos_selecionados) > 0) and ($aditivos_selecionados[$sql->result("NNUMETXMEN")] > 0))
          $tpl->ADITIVO_CHECKED2 = "checked";
        else
          $tpl->ADITIVO_CHECKED2 = "";
        
        $tpl->block("ADITIVO_COL2");      
      }
      
      $col =  ($col * -1);
      
      $sql->next();
    }  

    $tpl->block("MOSTRA_ADITIVOS");
  }
  
  /* Select Tipo logradouro */
  $txt = "SELECT NNUMETLOGR,INITCAP(CDESCTLOGR) CDESCTLOGR
            FROM HSSTLOGR 
           ORDER BY 2";  
  $sql = new Query($bd);
  $sql->executeQuery($txt);  
  
  while (!$sql->eof()) {
    $tpl->CADASTRO_TIPO_LOGRADOURO_ID        = $sql->result("NNUMETLOGR");
    $tpl->CADASTRO_TIPO_LOGRADOURO_DESCRICAO = $sql->result("CDESCTLOGR");
    $tpl->block("CADASTRO_ITEM_TIPO_LOGRADOURO");
    $sql->next();
  } 
  
  /* Direitos obriga��o de campos */
  $tpl->DIREITO_CPF                   = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOCPF",false);
  $tpl->DIREITO_CSUS                  = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOCARTOSUS",false);
  $tpl->DIREITO_NDNV                  = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTODECLARAONASCIDOVIVO",false);
  $tpl->DIREITO_ENDERECO              = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOENDEREOCOMPLETO",false);
  $tpl->DIREITO_TEL                   = $seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOPREENCHIMENTOTELEFONE",false);  
  $tpl->PF                            = 'N'; 
  $tpl->ID_SESSAO                     = $_GET['idSessao'];  
  $tpl->CADASTRO_OPERADORA            = $_SESSION['apelido_operadora'];    
  $tpl->CADASTRO_CONTRATO             = $_SESSION['id_contrato'];       
  $tpl->CANCELAMENTO                  = 'N'; 
  
  if ($pode_alterar_plano_dependente)
    $tpl->PODE_ALTERAR_PLANO_DEPENDENTE = 'S';
  else
    $tpl->PODE_ALTERAR_PLANO_DEPENDENTE = 'N'; 
        
  if ($_SESSION['apelido_operadora'] <> 'saudemed')
    $tpl->block("MOSTRA_ACOMODACAO");
    
  if ($_SESSION['apelido_operadora'] <> 'sampes') {
    $tpl->block("MOSTRA_RG");
    $tpl->block("MOSTRA_PIS");
    $tpl->block("MOSTRA_UNIAO_ESTAVEL");
    $tpl->block("MOSTRA_PESO_ALTURA");
  }

  if ($_SESSION['apelido_operadora'] <> 'vitallis')
    $tpl->block("ANEXOS");    
	  
  if ($seg->permissaoOutros($bd,"WEBEMPRESAOBRIGATRIOANEXONAINCLUSO",false)) {    
    $tpl->ANEXO_OBRIGATORIO = 'S';
    if ($_SESSION['apelido_operadora'] == 'vitallis')
      $tpl->block("ANEXOS");  	  
  }
  else
    $tpl->ANEXO_OBRIGATORIO = 'N';
    
  $tpl->block("TELEFONES_ADICIONAIS");
  $tpl->block("OBSERVACOES");
  
  if (($_SESSION['apelido_operadora'] == 'agemed') or ($_SESSION['apelido_operadora'] == 'unimedSalto') or ($_SESSION['apelido_operadora'] == 'unimedLimeira')) {
    $tpl->MSG_TAM_ARQUIVO = "OBS: Tamanho m�ximo do arquivo � de 5MB.";
    $tpl->block("MSG_TAMANHO_ARQUIVO");
  }  

  //$tpl->MSG_ANEXOS = "Arquivos anexados :";
  //$tpl->block("MSG_ANEXOS");  
  
  if (($_SESSION['apelido_operadora'] == 'clinipam') or
      ($_SESSION['apelido_operadora'] == 'odontopam') or
      ($_SESSION['apelido_operadora'] == 'saudeescolar') or
      ($seg->permissaoOutros($bd,"WEBEMPRESAINFORMADATADEINCLUSOAOCADASTRAR",false))) 
    $tpl->block("DATA_INCLUSAO");    
  
  $sql = new Query($bd);
  $txt = "SELECT ACRESCENTA_ZEROS(HSSTITU.NDIA_VENCI,2) NDIA_VENCI,nvl(NLIMITITU,NLIMIVENCI) NLIMITITU,NVL(CPERITITU,CPERICONF) CPERITITU,NVL(CASO_TITU,'N') CASO_TITU,
                 NVL(NVL(HSSTITU.NDIWETITU,HSSEMPR.NDIWEEMPR),NDIWEGRFAT) NDIWETITU, NVL(CLOCAEMPR,'S') CLOCAEMPR,COBMTTITU
            FROM HSSTITU,HSSVENCI,HSSEMPR,HSSCONF,HSSGRFAT
           WHERE HSSTITU.NNUMETITU = :contrato
             AND HSSTITU.NDIA_VENCI = HSSVENCI.NDIA_VENCI 
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
			 AND HSSTITU.NNUMEGRFAT = HSSGRFAT.NNUMEGRFAT(+)";
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  $tpl->EH_CONTRATO_PCMSO   = $sql->result("CASO_TITU");
  $tpl->OBRIGATORIO_LOCACAO = $sql->result("CLOCAEMPR");
  $tpl->OBRIGATORIO_MATRICULA = $sql->result("COBMTTITU");

  $venc = $sql->result("NDIA_VENCI") . '/' . date('m/Y');
  $venc = $data->proximaDataValida($bd,$venc); /* por padrao vai decrementar a data se estiver errada, senao retorna ela mesma*/

  $sql2 = new Query($bd);
  $txt2 = " BEGIN " .
          "    CalculaDatasLimiteUti(:vencimento,:limite,:periodo,:inicio,:fim,sysdate);" .
		      " END; ";		
  $sql2->addParam(":vencimento",$venc);
  $sql2->addParam(":limite",$sql->result("NLIMITITU"));
  $sql2->addParam(":periodo",$sql->result("CPERITITU"));
  $sql2->addParam(":inicio",0,12);
  $sql2->addParam(":fim",0,12);
  $sql2->executeSQL($txt2);
  
  $tpl->MELHOR_DIA_INCLUSAO = $sql2->getReturn(":inicio");   
  
  if ($_SESSION['apelido_operadora'] <> 'vitallis') {
    
    if ($_SESSION['apelido_operadora'] == 'promedmg'){
      $observacao = "<b><font color='red'>* Para vencimento 15, a sua solicita��o ser� analisada/processada at� o dia 30 do m�s anterior.<br>".
                    "<font color='red'>* Lembrando que sua solicita��o no portal dever� ser realizada at� o dia 25 do m�s anterior. Favor aguardar!</font></b>";
    }else if ($_SESSION['apelido_operadora'] == 'unimedSalto') {
      $observacao = "<p><font color='red'>* Para vencimento 10 (Data de vencimento da fatura da empresa), a sua solicita��o ser� analisada/processada at� dia 30 do m�s anterior Lembrando que sua solicita��o no portal dever� ser realizada at� dia 20 do m�s anterior. Favor aguardar! ";
    }else if (($_SESSION['apelido_operadora'] <> 'vitallis') and ($_SESSION['apelido_operadora'] <> 'UnimedLondrina') and ($_SESSION['apelido_operadora'] <> 'unimedLimeira')) {
      $observacao = "<p><font color='red'>* Dia inicial do per�odo de utiliza��o: ".$sql->result("NLIMITITU");
    }
    
    if ($sql->result("NDIWETITU") <> '')
      $observacao = $observacao."<br>* �ltimo dia para movimenta��es de cadastro: ".$sql->result("NDIWETITU")."</font></p>";
    else
      $observacao = $observacao."</font></p>";
  
    if ($_SESSION['apelido_operadora'] == 'agemed') {
      $observacao .= "<p style='font-size:11px;'>
                       <b>Documentos obrigat�rios para a inclus�o do usu�rio (titular e dependentes):</b><br>
                       <ul>
                         <li><font color='red'>Proposta de ades�o Agemed, carta de orienta��o ao benefici�rio e declara��o de sa�de (devidamente preenchida, datada e assinada);</font></li>
                         <li><font color='red'>C�pia da ficha de registro (com foto, assinada e carimbada) ou RE atualizada;</font></li>
                         <li><font color='red'>C�pia da Certid�o de Nascimento (para dependentes menores de 18 anos);</font></li>
                         <li><font color='red'>C�pia da Certid�o de Casamento ou Declara��o de Uni�o Est�vel;</font></li>
                         <li><font color='navy'>Compacte todos os documentos em um �nico arquivo e anexe na inclus�o.</font></li>
                       </ul>
                     </p>
                     <p style='font-size:11px;'>
                       Dica: verifique diariamente o relat�rio de opera��es (Relat�rios => Cadastro => Opera��es de cadastro), para visualizar o andamento da sua movimenta��o.
                     </p>";
    }
  }
  
  $tpl->CADASTRO_NOME              = $nome;  
  $tpl->CADASTRO_CATEGORIA         = $categoria;    
  $tpl->CADASTRO_DATA_INCLUSAO     = $data_inclusao;  
  $tpl->CADASTRO_NASCIMENTO        = $nascimento;  
  $tpl->CADASTRO_ESTADO_NASC       = $estadonasc;  
  $tpl->CADASTRO_CIDADE_NASC       = $cidadenasc;  
  $tpl->CADASTRO_SEXO              = $sexo;    
  $tpl->CADASTRO_GRAU              = $grau;      
  $tpl->CADASTRO_ESTADO_CIVIL      = $estado_civil; 
  $tpl->CADASTRO_CPF               = $cpf;  
  $tpl->CADASTRO_RG                = $rg;  
  $tpl->CADASTRO_EXPEDICAO         = $expedicao;  
  $tpl->CADASTRO_ORGAO             = $orgao;  
  $tpl->CADASTRO_PIS               = $pis;  
  $tpl->CADASTRO_CSUS              = $csus;    
  $tpl->CADASTRO_NDNV              = $ndnv;    
  $tpl->CADASTRO_MAE               = $mae;  
  $tpl->CADASTRO_PAI               = $pai;  
  $tpl->CADASTRO_UNIAO             = $uniao;    
  $tpl->CADASTRO_TITULAR           = $titular;  
  $tpl->CADASTRO_MATRICULA         = $matricula;  
  $tpl->CADASTRO_PROFISSAO         = $profissao;
  $tpl->OPERADORA_REPASSE          = $id_operadora_repasse;
  $tpl->CADASTRO_SETOR             = $setor;  
  $tpl->CADASTRO_SALARIO           = $salario;  
  $tpl->CADASTRO_ADMISSAO          = $admissao;
  $tpl->CADASTRO_DEMISSAO          = $demissao;    
  $tpl->CADASTRO_LOCACAO           = $locacao;      
  $tpl->CADASTRO_PLANO             = $plano;    
  $tpl->CADASTRO_ACOMODACAO        = $acomodacao;  
  $tpl->CADASTRO_TELEFONE1         = $telefone1;    
  $tpl->CADASTRO_TELEFONE2         = $telefone2;    
  $tpl->CADASTRO_CELULAR           = $celular;    
  $tpl->CADASTRO_EMAIL             = $email; 
  $tpl->CADASTRO_CEP               = $cep;    
  $tpl->CADASTRO_TIPO_LOGRADOURO   = $tipo_logradouro;    
  $tpl->CADASTRO_LOGRADOURO        = $logradouro;    
  $tpl->CADASTRO_NUMERO            = $numero;    
  $tpl->CADASTRO_COMPLEMENTO       = $complemento;    
  $tpl->CADASTRO_BAIRRO            = $bairro; 
  $tpl->CADASTRO_ESTADO            = $estado;
  $tpl->CADASTRO_CIDADE            = $cidade;
  $tpl->CADASTRO_OBSERVACAO1       = $observacao1;
  $tpl->CADASTRO_DATA_CANCELAMENTO = $data_cancelamento;
  $tpl->CADASTRO_INI_AFAST         = $ini_afast;  
  $tpl->CADASTRO_FIM_AFAST         = $fim_afast;
  $tpl->CADASTRO_MOTIVO_AFAST      = $motivo;
  $tpl->CADASTRO_MOTIVO_CANC       = $motivo_canc;  
  $tpl->CADASTRO_ID                = $id;    
  $tpl->CADASTRO_CENTRO_CUSTO      = $centro_custo;
  $tpl->OBSERVACAO                 = $observacao;  
  $tpl->CADASTRO_PESO              = $peso;
  $tpl->CADASTRO_ALTURA            = $altura;
  $tpl->CIDADE_ATEND               = $cidadeatendimento;
   
  if ($observacao <> '')
    $tpl->block("MOSTRA_OBSERVACAO");  
   
  $tpl->BOTAO_ID                   = "enviar";      
  $tpl->CADASTRO_BOTAO             = "Incluir";   
  
  $tpl->PAGINA_VOLTAR = 'principal.php';  
  
  $tpl->block("MOSTRA_MENU");  
  $tpl->show();     
  $bd->close();
  
?>