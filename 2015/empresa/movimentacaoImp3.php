<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  
  $seg  = new Seguranca();
  $bd   = new Oracle();
  $func = new Funcao();
  
  class PDF extends PDFSolus {
    
    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(205,5,'RELAT�RIO DE MOVIMENTA��O',0,0);
      $this->SetFont('Arial','',6);
      $this->Cell(25,5,'carimbo da empresa',0,1);
      $this->Rect(250,5,30,25);        
     
      $this->SetFont('Arial','B',7);
      $this->Cell(40,5,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(130,3,$_SESSION['titular_contrato']." - ".$_SESSION['cnpj_contrato'],0,1);
      
      if ($_SESSION['id_locacao'] > 0) {
        $this->Cell(40,3,'');      
        $this->Cell(25,3,"Loca��o:",0,0,'R');
        $this->Cell(205,3,$_SESSION['nome_locacao'],0,1);    
      }     
      
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Per�odo:",0,0,'R');
      $this->Cell(130,3,$this->getArray("Periodo"),0,1);
      $this->Cell(40,3,'');
      $this->Cell(25,3,'Situa��o:',0,0,'R');
      $this->Cell(130,3,$this->getArray("Situa��o"),0,1);
      $this->Ln(4);      
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  
  $datainicial = $_POST['di'];
  $datafinal   = $_POST['df'];
  $operacao    = $_POST['op'];
  $situacao    = $_POST['s'];
    
  if ($situacao == '1') 
    $situacao_desc = "Todas";
  else if ($situacao == '2') 
    $situacao_desc = "Pendentes";
  else if ($situacao == '3') 
    $situacao_desc = "Confirmadas";
  else
    $situacao_desc = '';

  $arr = array("Periodo" => ($datainicial." a ".$datafinal),
               "Situa��o" => $situacao_desc);
  $pdf->SetArray($arr);

  $contrato = $_SESSION['id_contrato'];
  
  if ($_SESSION['id_locacao'] > 0)
    $locacao = $_SESSION['id_locacao'];  
  else    
    $locacao = $_POST['lo'];
    
  $tem_registro = false;  
     
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);
 
  $sql_incl = new Query($bd);
  
  if ($seg->permissaoOutros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
    $f_operador = "   AND HSSATCAD.NOPERUSUA = :operador ";
    $f_operador2 = "   AND HSSUSUA.NNUMEUSUA = 0 ";
    $operador = $_SESSION['id_operador'];
  } else {
    $operador = 0;
    $f_operador = "";  
    $f_operador2 = "";    
  }
  
  if ($locacao > 0) {
    $f_locacao =  "   AND NVL(HSSATCAD.NNUMESETOR,HSSUSUA.NNUMESETOR) = :locacao ";
    $f_locacao2 =  "   AND HSSUSUA.NNUMESETOR = :locacao ";
  } else {
    $f_locacao = "";
    $f_locacao2 = "";
  }
  
  if ($situacao == '1') {
    $f_situacao = "   AND HSSATCAD.CFLAGATCAD IS NULL";
    $f_situacao2 = "   AND HSSUSUA.NNUMEUSUA = 0 ";
  } else if ($situacao == '2') {
    $f_situacao = "   AND HSSATCAD.CFLAGATCAD = 'A'";
    $f_situacao2 = "";
  } else {
    $f_situacao = "";  
    $f_situacao2 = "";
  }
        
  // Inclusoes
  if ($operacao == '1') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD = 0";
    
    $f_operacaoA  = "";
    $f_operacao2A = "   AND HSSUSUA.NNUMEUSUA = 0";
    
  // Cancelamento
  } else if ($operacao == '2') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD = 0";
    
    $f_operacaoA  = "   AND HSSUSUA.NNUMEUSUA = 0";
    $f_operacao2A = "";
    
  // Alteracao
  } else if ($operacao == '3') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD = 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD > 0";
    
  // Cancelamento e Inclusao
  } else if ($operacao == '4') {
    $f_operacao  = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao2 = "   AND HSSATCAD.NNUMEATCAD > 0";
    $f_operacao3 = "   AND HSSATCAD.NNUMEATCAD = 0";
    
    $f_operacaoA  = "";
    $f_operacao2A = "";    
  } else {
    $f_operacao  = "";      
    $f_operacao2 = "";      
    $f_operacao3 = "";      
    
    $f_operacaoA  = "";
    $f_operacao2A = "";    
  }
  
  $txt_incl = "SELECT NVL(RETORNA_NOME_USUARIO_ATCAD(HSSATCAD.NTITUUSUA),'Excluido') TITULAR,HSSATCAD.CGRAUATCAD CGRAUUSUA, 
                      HSSATCAD.CNOMEATCAD CNOMEUSUA,HSSUSUA.CCODIUSUA,HSSATCAD.CTIPOATCAD CTIPOUSUA,HSSATCAD.NNUMEPLAN NNUMEPLAN,
                      TO_CHAR(HSSATCAD.DNASCATCAD,'DD/MM/YYYY') DNASCUSUA,IDADE(DNASCATCAD,SYSDATE) IDADEUSUARIO,
                      DECODE(CFLAGATCAD,'A',NVL(TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YYYY'),'Excluido'),'R','Rejeitado','Pendente') DINCLUSUA,'Web' LOCAL,
                      HSSATCAD.NNUMEATCAD PROTOCOLO
                 FROM HSSATCAD,HSSUSUA
                WHERE HSSATCAD.NNUMETITU = :contrato
                  AND HSSATCAD.DDATAATCAD >= TO_DATE(:datainicial,'DD/MM/YYYY')
                  AND HSSATCAD.DDATAATCAD < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                  AND HSSATCAD.COPERATCAD = 'I' ".
              $f_situacao.
              $f_operacao.
              $f_operador.
              "   AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+) ".                              
              $f_locacao.
              " UNION ALL
               SELECT TITULAR.CNOMEUSUA TITULAR,HSSUSUA.CGRAUUSUA, 
                      HSSUSUA.CNOMEUSUA,HSSUSUA.CCODIUSUA,HSSUSUA.CTIPOUSUA,HSSUSUA.NNUMEPLAN,
                      TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADEUSUARIO,
                      TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,'Operadora' LOCAL, TO_NUMBER(NULL) PROTOCOLO
                 FROM HSSUSUA,HSSUSUA TITULAR
                WHERE HSSUSUA.NNUMETITU = :contrato
                  AND HSSUSUA.DINCLUSUA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                  AND HSSUSUA.DINCLUSUA < TO_DATE(:datafinal,'DD/MM/YYYY') + 1 ".
              $f_situacao2.
              $f_operacaoA.
              $f_operador2.
              "   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA 
                  AND HSSUSUA.NNUMEUSUA NOT IN (SELECT NNUMEUSUA FROM HSSATCAD WHERE NNUMEUSUA = HSSUSUA.NNUMEUSUA) ".
              $f_locacao2.               
              "ORDER BY 1,2 desc,3";  
  
  $sql_incl->addParam(":contrato",$contrato); 
  $sql_incl->addParam(":datainicial",$datainicial); 
  $sql_incl->addParam(":datafinal",$datafinal);
    
  if ($locacao >0)
    $sql_incl->addParam(":locacao",$locacao);  

  if ($operador > 0)
    $sql_incl->addParam(":operador",$operador);  
  
  $sql_incl->executeQuery($txt_incl);
    
  if ($sql_incl->count() > 0) {
    $tem_registro = true;    
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(270,3,'INCLUS�ES',0,1);  
    $pdf->Ln(1);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(30,3,'Codigo',0,0);
    $pdf->Cell(80,3,'Nome',0,0);
    $pdf->Cell(20,3,'Categoria',0,0);
    $pdf->Cell(10,3,'Plano',0,0);
    $pdf->Cell(20,3,'Nascimento',0,0);
    $pdf->Cell(10,3,'Idade',0,0);    
    $pdf->Cell(40,3,'Titular',0,0);
    $pdf->Cell(20,3,'Inclus�o',0,0);    
    $pdf->Cell(20,3,'Local',0,0);    
    $pdf->Cell(20,3,'Protocolo',0,1);    
    $pdf->Ln(1);

    $pdf->SetFont('Arial','',8);
    
    while (!$sql_incl->eof()) { 
      $pdf->Cell(30,3,$sql_incl->result("CCODIUSUA"),0,0);
      $pdf->Cell(80,3,$pdf->Copy($sql_incl->result("CNOMEUSUA"),79),0,0);
      $pdf->Cell(20,3,$pdf->Copy($func->categoriaUsuario($sql_incl->result("CTIPOUSUA")),19),0,0);
      $pdf->Cell(10,3,$pdf->Copy($func->retornaCodigoPlano($bd,$sql_incl->result("NNUMEPLAN")),9),0,0);
      $pdf->Cell(20,3,$sql_incl->result("DNASCUSUA"),0,0);
      $pdf->Cell(10,3,$sql_incl->result("IDADEUSUARIO"),0,0);
      $pdf->Cell(40,3,$pdf->Copy($sql_incl->result("TITULAR"),39),0,0);
      $pdf->Cell(20,3,$sql_incl->result("DINCLUSUA"),0,0);
      $pdf->Cell(20,3,$sql_incl->result("LOCAL"),0,0);
      $pdf->Cell(20,3,$sql_incl->result("PROTOCOLO"),0,1,'R');
      $sql_incl->next();
    }
  }

  $sql_canc = new Query($bd);  
  
  $txt_canc = "SELECT NVL(RETORNA_NOME_USUARIO_ATCAD(HSSATCAD.NTITUUSUA),'Excluido') TITULAR, HSSATCAD.CGRAUATCAD CGRAUUSUA,
                      HSSATCAD.CNOMEATCAD CNOMEUSUA,HSSUSUA.CCODIUSUA,HSSUSUA.CTIPOUSUA CTIPOUSUA,HSSUSUA.NNUMEPLAN NNUMEPLAN,
                      TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,IDADE(DNASCATCAD,SYSDATE) IDADEUSUARIO,
                      DECODE(CFLAGATCAD,'A',TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY'),'R','Rejeitado','Pendente') DSITUUSUA,
                      CDESCMCANC MOTIVO,'Web' LOCAL,HSSATCAD.NNUMEATCAD PROTOCOLO
                 FROM HSSATCAD,HSSUSUA,HSSMCANC
                WHERE HSSATCAD.NNUMETITU = :contrato
                  AND HSSATCAD.DDATAATCAD >= TO_DATE(:datainicial,'DD/MM/YYYY')
                  AND HSSATCAD.DDATAATCAD < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                  AND HSSATCAD.COPERATCAD = 'C' ".
              $f_situacao.
              $f_operacao2.
              $f_operador.
              "   AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                  AND HSSUSUA.NNUMEMCANC = HSSMCANC.NNUMEMCANC(+) ".
              $f_locacao.
              " UNION ALL
               SELECT TITULAR.CNOMEUSUA TITULAR,HSSUSUA.CGRAUUSUA, 
                      HSSUSUA.CNOMEUSUA,HSSUSUA.CCODIUSUA,HSSUSUA.CTIPOUSUA,HSSUSUA.NNUMEPLAN,
                      TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADEUSUARIO,
                      TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY') DSITUUSUA,
                      CDESCMCANC MOTIVO,'Operadora' LOCAL, TO_NUMBER(NULL) PROTOCOLO
                 FROM HSSUSUA,HSSUSUA TITULAR,HSSMCANC
                WHERE HSSUSUA.NNUMETITU = :contrato
                  AND HSSUSUA.DSITUUSUA >= TO_DATE(:datainicial,'DD/MM/YYYY')
                  AND HSSUSUA.DSITUUSUA < TO_DATE(:datafinal,'DD/MM/YYYY') + 1 ".
              $f_situacao2.
              $f_operacao2A.
              $f_operador2.
              "   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA 
                  AND HSSUSUA.NNUMEUSUA NOT IN (SELECT NNUMEUSUA FROM HSSATCAD WHERE NNUMEUSUA = HSSUSUA.NNUMEUSUA)
                  AND HSSUSUA.NNUMEMCANC = HSSMCANC.NNUMEMCANC(+) ".
              $f_locacao2.                             
              "ORDER BY 1,2 desc,3";
                
  $sql_canc->addParam(":contrato",$contrato); 
  $sql_canc->addParam(":datainicial",$datainicial); 
  $sql_canc->addParam(":datafinal",$datafinal); 

  if ($locacao > 0)
    $sql_canc->addParam(":locacao",$locacao); 

  if ($operador > 0)
    $sql_canc->addParam(":operador",$operador);  
    
  $sql_canc->executeQuery($txt_canc);
  
  $pdf->Ln(10);

  if ($sql_canc->count() > 0) {
    $tem_registro = true;      
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'CANCELAMENTOS',0,1);  
    $pdf->Ln(1);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(30,3,'Codigo',0,0);
    $pdf->Cell(50,3,'Nome',0,0);
    $pdf->Cell(20,3,'Categoria',0,0);
    $pdf->Cell(10,3,'Plano',0,0);
    $pdf->Cell(20,3,'Nascimento',0,0);
    $pdf->Cell(10,3,'Idade',0,0);     
    $pdf->Cell(40,3,'Titular',0,0);
    $pdf->Cell(20,3,'Cancelamento',0,0);    
    $pdf->Cell(30,3,'Motivo de canc.',0,0);    
    $pdf->Cell(20,3,'Local',0,0);    
    $pdf->Cell(20,3,'Protocolo',0,1);      
    $pdf->Ln(1);

    $pdf->SetFont('Arial','',8);
    
    while (!$sql_canc->eof()) { 
      $pdf->Cell(30,3,$sql_canc->result("CCODIUSUA"),0,0);    
      $pdf->Cell(50,3,$pdf->Copy($sql_canc->result("CNOMEUSUA"),49),0,0);
      $pdf->Cell(20,3,$pdf->Copy($func->categoriaUsuario($sql_canc->result("CTIPOUSUA")),19),0,0);
      $pdf->Cell(10,3,$pdf->Copy($func->retornaCodigoPlano($bd,$sql_canc->result("NNUMEPLAN")),9),0,0);
      $pdf->Cell(20,3,$sql_canc->result("DNASCUSUA"),0,0);
      $pdf->Cell(10,3,$sql_canc->result("IDADEUSUARIO"),0,0);      
      $pdf->Cell(40,3,$pdf->Copy($sql_canc->result("TITULAR"),39),0,0);
      $pdf->Cell(20,3,$sql_canc->result("DSITUUSUA"),0,0);
      $pdf->Cell(30,3,$pdf->Copy($sql_canc->result("MOTIVO"),29),0,0);
      $pdf->Cell(20,3,$sql_canc->result("LOCAL"),0,0);
      $pdf->Cell(20,3,$sql_canc->result("PROTOCOLO"),0,1);
      $sql_canc->next();
    }
  }
  
  $sql_alt = new Query($bd);
  
  $txt_alt = "SELECT NVL(RETORNA_NOME_USUARIO_ATCAD(HSSATCAD.NTITUUSUA),'Excluido') TITULAR, HSSATCAD.CNOMEATCAD CNOMEUSUA,HSSATCAD.C_CPFATCAD C_CPFUSUA,TO_CHAR(HSSATCAD.DNASCATCAD,'DD/MM/YYYY') DNASCUSUA,
                     HSSATCAD.CNMAEATCAD CNMAEUSUA,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DDIGIUSUA,HSSUSUA.DINCLUSUA),HSSATCAD.DINCLATCAD),'DD/MM/YYYY')) DINCLUSUA,CTIPOUSUA,
                     HSSATCAD.CGRAUATCAD CGRAUUSUA, 2 TIPO,DECODE(HSSATCAD.CFLAGATCAD,NULL,'Pendente',TO_CHAR(NVL(GREATEST(HSSUSUA.DSITUUSUA,HSSUSUA.DALTEUSUA),HSSATCAD.DSITUATCAD),'DD/MM/YYYY')) DSITUUSUA,HSSUSUA.NNUMEPLAN NNUMEPLAN,TO_CHAR(HSSATCAD.DDATAATCAD,'DD/MM/YYYY') DDATAATCAD,HSSUSUA.CCODIUSUA,
                     IDADE(DNASCATCAD,SYSDATE) IDADEUSUARIO,'Web' LOCAL
                FROM HSSATCAD,HSSUSUA
               WHERE HSSATCAD.NNUMETITU = :contrato
                 AND HSSATCAD.DDATAATCAD >= TO_DATE(:datainicial,'DD/MM/YYYY')
                 AND HSSATCAD.DDATAATCAD < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                 AND HSSATCAD.COPERATCAD = 'A' ".
             $f_situacao.
             $f_operacao3.
             $f_operador.
             "   AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+) ".                              
             $f_locacao.
             "ORDER BY 1,8 desc,2";
  
  $sql_alt->addParam(":contrato",$contrato); 
  $sql_alt->addParam(":datainicial",$datainicial); 
  $sql_alt->addParam(":datafinal",$datafinal); 

  if ($locacao > 0)
    $sql_alt->addParam(":locacao",$locacao); 

  if ($operador > 0)
    $sql_alt->addParam(":operador",$operador);  
    
  $sql_alt->executeQuery($txt_alt);
  
  $pdf->Ln(10);

  if ($sql_alt->count() > 0) {
    $tem_registro = true;      
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'ALTERA��ES',0,1);  
    $pdf->Ln(1);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,3,'Dt. Altera��o',0,0);
    $pdf->Cell(30,3,'Codigo',0,0);
    $pdf->Cell(80,3,'Nome',0,0);
    $pdf->Cell(20,3,'Categoria',0,0);
    $pdf->Cell(10,3,'Idade',0,0);    
    $pdf->Cell(20,3,'Plano',0,0);
    $pdf->Cell(50,3,'Titular',0,0);
    $pdf->Cell(20,3,'Local',0,0);
    $pdf->Cell(20,3,'Protocolo',0,1);    
    $pdf->Ln(1);

    $pdf->SetFont('Arial','',8);
    
    while (!$sql_alt->eof()) { 
      $pdf->Cell(20,3,$sql_alt->result("DDATAATCAD"),0,0);        
      $pdf->Cell(30,3,$sql_alt->result("CCODIUSUA"),0,0);    
      $pdf->Cell(80,3,$pdf->Copy($sql_alt->result("CNOMEUSUA"),79),0,0);
      $pdf->Cell(20,3,$pdf->Copy($func->categoriaUsuario($sql_alt->result("CTIPOUSUA")),19),0,0);
      $pdf->Cell(10,3,$sql_alt->result("IDADEUSUARIO"),0,0);            
      $pdf->Cell(20,3,$pdf->Copy($func->retornaCodigoPlano($bd,$sql_alt->result("NNUMEPLAN")),9),0,0);
      $pdf->Cell(50,3,$pdf->Copy($sql_alt->result("TITULAR"),48),0,0);
      $pdf->Cell(20,3,$sql_alt->result("LOCAL"),0,0);
      $pdf->Cell(20,3,$sql_alt->result("PROTOCOLO"),0,1);
      $sql_alt->next();
    }
  }  

  if ($tem_registro == true) {
    $pdf->Ln(10);
    $pdf->Cell(20,5,"Respons�vel:",0,0,'R');
    $pdf->Cell(100,5,'','B',1);
    $pdf->Ln(5);
    $pdf->Cell(20,3,'Recebido por:','',0);
    $pdf->Cell(100,3,'','B',0);
    $pdf->Cell(20,3,'Data: ___/___/______','',0);
  }

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  
  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 

?>
