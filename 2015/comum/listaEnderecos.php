<?php
  require_once("../comum/autoload.php");
  //$seg->secureSessionStart();   
  //require_once('../comum/sessao.php'); 

  $bd  = new Oracle();
 
  $tpl = new Template("listaEnderecos.htm");

  $endereco   = $seg->antiInjection($_POST['logradouro']);
 
  $endereco = strToUpper($endereco);
  $endereco = str_replace(","," ",$endereco);
  $endereco = str_replace("+"," ",$endereco);
  $endereco = $formata->somenteCaracteres($endereco,"ABCDEFGHIJKLMNOPQRSTUVWXYZ ");
  $palavras = explode(" ",$endereco);
  $primeira = "";
      
  for ($i=0;$i<sizeOf($palavras);$i++) {
  
    if (($palavras[$i] == 'AC') or
        ($palavras[$i] == 'AM') or    
        ($palavras[$i] == 'AL') or    
        ($palavras[$i] == 'AP') or    
        ($palavras[$i] == 'BA') or    
        ($palavras[$i] == 'CE') or    
        ($palavras[$i] == 'DF') or    
        ($palavras[$i] == 'ES') or    
        ($palavras[$i] == 'GO') or    
        ($palavras[$i] == 'MA') or    
        ($palavras[$i] == 'MT') or    
        ($palavras[$i] == 'MS') or    
        ($palavras[$i] == 'MG') or    
        ($palavras[$i] == 'PA') or    
        ($palavras[$i] == 'PR') or    
        ($palavras[$i] == 'PB') or    
        ($palavras[$i] == 'PE') or    
        ($palavras[$i] == 'PI') or    
        ($palavras[$i] == 'RJ') or    
        ($palavras[$i] == 'RN') or    
        ($palavras[$i] == 'RS') or    
        ($palavras[$i] == 'RR') or    
        ($palavras[$i] == 'SC') or    
        ($palavras[$i] == 'SP') or    
        ($palavras[$i] == 'SE') or    
        ($palavras[$i] == 'TO'))    
      $f_filtro .= "   AND HSSCIDAD.CESTACIDAD = '".$palavras[$i]."'";
    else if (strLen($palavras[$i]) > 2) {
      if (($primeira == "") and
          ($palavras[$i] <> "AVENIDA") and
          ($palavras[$i] <> "RUA") and
          ($palavras[$i] <> "TRAVESSA") and
          ($palavras[$i] <> "RODOVIA"))
        $primeira = $palavras[$i];
          
      $f_filtro .= "   AND INSTR(HSSENDER.CLOGRENDER || HSSENDER.CLOGRENDER || HSSENDER.CNOMEENDER || HSSCIDAD.CNOMECIDAD || HSSCIDAD.CESTACIDAD,'".$palavras[$i]."') > 0";
    }
  }
                  
  if (strlen($endereco) >= 3) {      

    $sql = new Query($bd);
      
    $txt = "SELECT * FROM (
            SELECT ROWNUM NUMERO, HSSENDER.CLOGRENDER || ' ' || CNOMEENDER CNOMEENDER,CCEP_ENDER, 
                   HSSENDER.NNUMETLOGR,CNOMECIDAD,
                   CNOMEBAIRR,CESTACIDAD,CDESCTLOGR,
                   DECODE(INSTR(HSSENDER.CNOMEENDER,:palavra),0,999999,INSTR(HSSENDER.CNOMEENDER,:palavra)) ORDEM
              FROM HSSENDER,HSSCIDAD,HSSBAIRR,HSSTLOGR
             WHERE HSSENDER.NNUMECIDAD = HSSCIDAD.NNUMECIDAD(+)
               AND HSSENDER.NNUMEBAIRR = HSSBAIRR.NNUMEBAIRR(+)
               AND HSSENDER.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+) ".
             $f_filtro.
           "   ORDER BY 9,2,7,5 ) A 
             WHERE A.NUMERO <= 100 ";                   
             
    $sql->addParam(":palavra",$primeira);             
    $sql->executeQuery($txt);
    
    if ($sql->count() > 0) {
             
      while (!$sql->eof()) {      
      
        $tpl->ENDERECO_CEP        = $formata->formataCEP($sql->result("CCEP_ENDER"));
        
        $endereco = "";
        
        if ($sql->result("CLOGRENDER") <> '')
          $endereco = $sql->result("CLOGRENDER")." ";
        else if ($sql->result("CDESCTLOGR") <> '')
          $endereco = $sql->result("CDESCTLOGR")." ";
          
        $tpl->ENDERECO_LOGRADOURO = $endereco.$sql->result("CNOMEENDER");
        $tpl->ENDERECO_BAIRRO     = $sql->result('CNOMEBAIRR');
        $tpl->ENDERECO_CIDADE     = $sql->result("CNOMECIDAD");
        $tpl->ENDERECO_UF         = $sql->result("CESTACIDAD");
               
        $tpl->block("ENDERECO");
        $sql->next();
      }
      
      $tpl->block("RESULTADO");    
      
      echo utf8_encode($tpl->parse());   
      //$tpl->show();
      
    }
    else
          echo $txt;
      //echo "<br><div class='alert alert-error'>Endereço não localizado</div>";
  }
  else
    echo "<br><div class='alert alert-error'>Digite pelo menos as 3 primeiras letras do endereço.</div>";    
    
  $bd->close();
?>