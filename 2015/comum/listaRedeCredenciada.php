<?php

  require_once("../comum/autoload.php");
  $seg->secureSessionStart();

  $registros = 5;

  if (isset($_GET['pg']))
    $pg = $_GET['pg'];
  else
    $pg = 0;

  if (isset($_POST['listarSubstitutos']))
    $listarSubstitutos = $_POST['listarSubstitutos'];
  else
    $listarSubstitutos = 0;

  $filtros = array();

  if (count($_POST) >= 7) {
    if (isset($_POST['plano']))
      $filtros['plano']        = $seg->antiInjection($_POST['plano']);
    else
      $filtros['plano']        = '';

    $filtros['nome_prestador'] = $seg->antiInjection($_POST['nome_prestador']);
    $filtros['tipo_prestador'] = $seg->antiInjection($_POST['tipo_prestador']);
    $filtros['tipoRede']       = $seg->antiInjection($_POST['tipoRede']);
    $filtros['estado']         = $seg->antiInjection($_POST['estado']);
    $filtros['cidade']         = $seg->antiInjection($_POST['cidade']);
    $filtros['bairro']         = $seg->antiInjection($_POST['bairro']);
    $filtros['graduacao']      = $seg->antiInjection($_POST['graduacao']);
    $filtros['especialidade']  = $seg->antiInjection($_POST['especialidade']);
    $filtros['area']           = $seg->antiInjection($_POST['area']);

    if (isset($_POST['indicavel']))
      $filtros['indicavel']    = $seg->antiInjection($_POST['indicavel']);
    else
      $filtros['indicavel']    = '';

    $filtros['executa']        = 'N';
    $filtros['inicio']         = 0;

    $pg                        = 0;
    $_SESSION['filtrosRede']   = $filtros;
  }
  else if (isset($_SESSION['filtrosRede'])) {
    $filtros                   = $_SESSION['filtrosRede'];
    $filtros['executa']        = 'S';
    $filtros['inicio']         = $inicio;
  }
  else {
    $filtros['plano']          = '';
    $filtros['nome_prestador'] = '';
    $filtros['tipo_prestador'] = '';
    $filtros['tipoRede']       = '';
    $filtros['estado']         = '';
    $filtros['cidade']         = '';
    $filtros['bairro']         = '';
    $filtros['graduacao']      = '';
    $filtros['especialidade']  = '';
    $filtros['area']           = '';
    $filtros['indicavel']      = '';
    $filtros['executa']        = 'N';
    $filtros['inicio']         = 0;
  }

  $plano          = $filtros['plano'];
  $nome_prestador = $filtros['nome_prestador'];
  $tipo_prestador = $filtros['tipo_prestador'];
  $tipoRede       = $filtros['tipoRede'];
  $estado         = $filtros['estado'];
  $cidade         = $filtros['cidade'];
  $bairro         = $filtros['bairro'];
  $graduacao      = $filtros['graduacao'];
  $especialidade  = $filtros['especialidade'];
  $area_prestador = $filtros['area'];
  $indicavel      = $filtros['indicavel'];
  $inicio         = $filtros['inicio'];

  $tpl->ID_SESSAO = $_GET['idSessao'];

  $tpl = new Template("listaRedeCredenciada.html");

  $rede = new RedeCredenciada();

  $rede->plano                = $plano;
  $rede->nome_prestador       = $nome_prestador;
  $rede->tipo_prestador       = $tipo_prestador;
  $rede->tipoRede             = $tipoRede;
  $rede->estado               = $estado;
  $rede->cidade               = $cidade;
  $rede->bairro               = $bairro;
  $rede->graduacao            = $graduacao;
  $rede->especialidade        = $especialidade;
  $rede->area_prestador       = $area_prestador;
  $rede->indicavel            = $indicavel;
  $rede->pagina_inicial       = $pg;
  $rede->registros_por_pagina = $registros;
  $rede->listarSubstitutos    = $listarSubstitutos;

  $rede->processaRedeCredenciada(true);

  if ($rede->total_registros > 0) {
    if ($plano > 0) {

      $sql_plano = new Query();
      $txt_plano = "SELECT HSSPLAN.CDESCPLAN,NVL(TO_CHAR(HSSPLAN.NRGMSPLAN),HSSPLAN.CANTIPLAN) NRGMSPLAN,
                           HSSPLAN.CSITUPLAN
                      FROM HSSPLAN
                     WHERE NNUMEPLAN = :idPlano";
      $sql_plano->addParam(":idPlano",$plano);
      $sql_plano->executeQuery($txt_plano);

      if ($sql_plano->result("CSITUPLAN") == "C")
        $situ_plano = '<font color="red">Cancelado</font>';
      else if ($sql_plano->result("CSITUPLAN") == "S")
        $situ_plano = '<font color="navy">Ativo (comercializa��o suspensa)</font>';
      else
        $situ_plano = '<font color="navy">Ativo</font>';

      $desc_grupo = 'Plano: ' . $sql_plano->result("CDESCPLAN").
                    ' - Reg.: ' . $formata->formataRegistroAns($sql_plano->result("NRGMSPLAN")) . ' - ' .
                    $situ_plano;

      $tpl->DESCRICAO_GRUPO = $desc_grupo;
      $tpl->block("GRUPO");
    }

    foreach ($rede->grupo02 as $grupo02) {

      if ($grupo02->nome <> '') {
        $tpl->DESCRICAO_GRUPO = $grupo02->nome;
        $tpl->block("GRUPO");
      }

      foreach ($grupo02->registros as $reg) {
        $tpl->RESULTADO_ID                   = $reg->idPrestador;
        $tpl->RESULTADO_NOME_PRESTADOR       = $reg->nome." ".$reg->documento;

        if ($reg->fantasia <> '') {
          $tpl->RESULTADO_NOME_FANTASIA      = $reg->fantasia;
          $tpl->block("MOSTRA_NOME_FANTASIA");
        }

        $tpl->RESULTADO_TIPO_ESTABELECIMENTO = $func->tipoPrestador($reg->tipo_prestador);

        if ($reg->data_cancelamento <> '') {
          $tpl->RESULTADO_CANCELAMENTO         = '<br><b><font color="#F2281D">Cancelado em ' . $reg->data_cancelamento . '</font></b>';

          if ($reg->temSubstituto == 'S')
            $tpl->BOTAO_SUBSTITUTO               = '&nbsp;&nbsp;<a href="javascript:void(0);" onclick="buscaSubstituto(\''.$reg->idPrestador.'\',\''.$reg->nome.'\');" id="prestadorSubstituto" name="prestadorSubstituto">Substituto</a>';
          else
            $tpl->BOTAO_SUBSTITUTO               = '';

          $tpl->RESULTADO_INICIO_SUBSTITUTO      = '';
        }
        else {
          $tpl->RESULTADO_CANCELAMENTO         = '';
          $tpl->BOTAO_SUBSTITUTO               = '';

          $dinisub_prest = $reg->inicioSubstituicao($listarSubstitutos,$reg->idPrestador);

          if ($dinisub_prest <> '')
            $tpl->RESULTADO_INICIO_SUBSTITUTO  = '<br><b><font color="#3333FF">Inicio em ' . $dinisub_prest . '</font></b>';
          else
            $tpl->RESULTADO_INICIO_SUBSTITUTO  = '';
        }

        if ($reg->urgencia_emergencia == "S")
          $tpl->block("URGENCIA_EMERGENCIA");

        //Graduacao
        $tpl->TIPO_DOCUMENTO                 = '';
        foreach ($reg->lista_graduacoes as $graduacao) {
          $tpl->TIPO_DOCUMENTO              .= '<img src="'.$graduacao->imagem.'" title="'.$graduacao->descricao.'"/>&nbsp;';
        }

        if ($tpl->TIPO_DOCUMENTO <> '')
          $tpl->block("MOSTRA_GRADUACAO");

        // Homepage
        if ($reg->homepage <> '') {
          $tpl->RESULTADO_HOMEPAGE = $reg->homepage;
          $tpl->block("HOMEPAGE");
        }

        //Especialidades
        foreach ($reg->lista_especialidades as $especialidade) {
          $tpl->RESULTADO_ESPECIALIDADES = $especialidade->nome;

          if ($especialidade->num_certificacao <> '')
            $tpl->RESULTADO_ESPECIALIDADES .= " - RQE n� ".$especialidade->num_certificacao;

          if ($reg->tipo_pessoa == 'J') {
            $lista_corpo_clinico = $especialidade->lista_corpo_clinico;

            foreach ($lista_corpo_clinico as $corpo) {

              $prestador_corpo = new Prestador($corpo);
              $tpl->RESULTADO_MEDICO = $formata->initCap($prestador_corpo->nome) . " (" . $prestador_corpo->conselho . '-' . $prestador_corpo->uf_conselho . ' ' . $prestador_corpo->doc_conselho . ")";
              $tpl->block("CORPO_CLINICO");
            }
          }
          else {
            $lista_areas = $especialidade->lista_areas;
            foreach ($lista_areas as $area) {
              $tpl->RESULTADO_MEDICO = $area->nome;

              if ($area->num_certificacao <> '')
                $tpl->RESULTADO_MEDICO .= " - RQE n� ".$area->num_certificacao;

              $tpl->block("CORPO_CLINICO");
            }
          }

          $tpl->block("ESPECIALIDADE");
        }

        //Enderecos
        foreach ($reg->lista_enderecos as $endereco) {

          $tpl->RESULTADO_ENDERECO = $endereco->endereco;
          $tpl->RESULTADO_BAIRRO   = $endereco->bairro;
          $tpl->RESULTADO_CIDADE   = $endereco->cidade;
          $tpl->RESULTADO_ESTADO   = $endereco->estado;
          $tpl->RESULTADO_CEP      = $endereco->cep;
          $tpl->RESULTADO_TELEFONE = "Tel:&nbsp;".$endereco->telefone;

          $tpl->block("RESULTADO_LINHA_ENDERECO");
        }

        if ($reg->ultima_alteracao <> '')
          $tpl->RESULTADO_ULTIMA_ALTERACAO = $reg->ultima_alteracao;
        else
          $tpl->RESULTADO_ULTIMA_ALTERACAO = '';

        //Foto
        if ($reg->foto <> '') {
          $file = '../temp/' . md5(uniqid(rand(), true)) . '.jpg';
          $foto_usuario = $reg->foto;

          $f = fopen($file, 'wb');
          if (!$f)
            $this->Error('Unable to create output file: ' . $file);

          fwrite($f, $foto_usuario, strlen($foto_usuario));
          fclose($f);

          $tpl->FOTO = $file;
          $tpl->block("PHOTO");
        }

        if (!isset($_POST['imprimir']))
          $tpl->block("RESULTADO_LINHA");
      }
    }

    $paginas = ceil($rede->total_registros / $registros) ;

    if ($paginas > 1) {

      $tpl->PAGINA      = 0;
      $tpl->DESC_PAGINA = "&laquo;";

      if ($pg == 0)
        $tpl->PAGINA_CLASSE = "active";
      else
        $tpl->PAGINA_CLASSE = "";

      $tpl->block("LINHA_PAGINACAO");

      if ($paginas > 2) {

        if ($paginas > 10) {
          if ($pg <= 5) {
            $pg3 = 0;
            $pg2 = 10;
          }
          else {
            $pg3 = $pg - 4;
            if (($pg + 6) > $paginas)
              $pg2 = $pg + ($paginas - $pg + 1);
            else
              $pg2 = $pg + 6;
          }
        }
        else {
          $pg3 = 0;
          $pg2 = $paginas;
        }

        for ($i = $pg3; $i < $pg2; $i++) {

          $tpl->PAGINA      = $i;
          $tpl->DESC_PAGINA = $i + 1;

          if ($pg == $i)
            $tpl->PAGINA_CLASSE = "active";
          else
            $tpl->PAGINA_CLASSE = "";

          $tpl->block("LINHA_PAGINACAO");
        }
      }

      $tpl->PAGINA       = $paginas-1;
      $tpl->PAGINA_ATUAL = $pg + 1;
      $tpl->PAGINA_TOTAL = $paginas;
      $tpl->DESC_PAGINA  = "&raquo;";

      if ($pg == $paginas-1)
        $tpl->PAGINA_CLASSE = "active";
      else
        $tpl->PAGINA_CLASSE = "";

      $tpl->block("LINHA_PAGINACAO");

      $tpl->block("PAGINACAO");
    }
  }
  else {
    $tpl->MSG = "A pesquisa n�o retornou nenhum resultado";
    $tpl->block("ERRO");
  }

  echo utf8_encode($tpl->parse());
?>