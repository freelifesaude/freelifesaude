<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
  
  $tpl = new Template("listaDiretorio.htm");
  
  $bd = new Oracle();  
  
  $diretorio = $seg->antiInjection($_POST['diretorio']);
  $leitura   = $seg->antiInjection($_POST['leitura']);
        
  if (!$leitura) {
    $tpl->block("PERMITE_EDITAR");
    $tpl->block("PERMITE_EDITAR2");
  }
    
  //Raiz
  $sql2 = new Query($bd);
  $txt2 = "SELECT CDESCARQUI,NVL(NSUPEARQUI,0) NSUPEARQUI
             FROM HSSARQUI
            WHERE NNUMEARQUI = :diretorio";   
  $sql2->addParam(":diretorio",$diretorio);           
  $sql2->executeQuery($txt2);

  if ($sql2->count() > 0) {
  
    if (!$leitura) {
      $tpl->block("PERMITE_EDITAR3");
    }
    
    $tpl->DIRETORIO      = "S";
    $tpl->DIRETORIO_NOME = '...';        
    $tpl->IMAGEM_ICONE   = "diretorio.png";    
    $tpl->TIPO           = "Diretório Raiz";
    $tpl->PROTOCOLO      = $sql2->result("NSUPEARQUI");  
    $tpl->IMAGEM         = "N";
    $tpl->DATA_ENVIO     = "";
    $tpl->ASSUNTO        = "...";
    $tpl->block("LINHA");             
  }
    
  //Diretórios        
  $sql = new Query($bd);    
  $txt = "SELECT HSSARQUI.NNUMEARQUI,TO_CHAR(HSSARQUI.DDATAARQUI,'DD/MM/YYYY HH24:mi') DDATAARQUI,
                 HSSARQUI.CDESCARQUI,NVL(HSSARQUI.NSUPEARQUI,0) NSUPEARQUI,
                 DECODE(HSSARQUI.CEXT_ARQUI,'PNG','S','JPG','S','JPEG','S','BMP','S','GIF','S','N') IMAGEM,
                 DECODE(HSSARQUI.CTIPOARQUI,'D','S','N') DIRETORIO,
                 HSSARQUI.CEXT_ARQUI EXTENSAO,HSSARQUI.CNOMEARQUI
            FROM HSSARQUI,HSSARQNT
           WHERE HSSARQUI.NNUMEARQUI = HSSARQNT.NNUMEARQUI(+)
             AND HSSARQUI.CLOCAARQUI = 'NOSSACOOPERATIVA'
             AND NVL(HSSARQUI.NSUPEARQUI,0) = :diretorio 
             AND HSSARQUI.CTIPOARQUI = 'D'             
           ORDER BY HSSARQUI.DDATAARQUI DESC,HSSARQUI.CDESCARQUI,HSSARQUI.NNUMEARQUI DESC";   
  $sql->addParam(":diretorio",$diretorio);           
  $sql->executeQuery($txt);
      
  while (!$sql->eof()) {
           	              
    $tpl->DIRETORIO_NOME = $sql->result("CDESCARQUI");        
    $tpl->IMAGEM_ICONE   = "diretorio.png";    
    $tpl->TIPO           = "Pasta de arquivos";
    $tpl->DIRETORIO      = "S";    
    $tpl->PROTOCOLO      = $sql->result("NNUMEARQUI");
    $tpl->IMAGEM         = $sql->result("IMAGEM");
    $tpl->DATA_ENVIO     = $sql->result("DDATAARQUI");
    $tpl->ASSUNTO        = $sql->result("CDESCARQUI");
    
    if (!$leitura) {
      $tpl->block("PERMITE_EDITAR3");
    }
    
    $tpl->block("LINHA");        

    $sql->next();
  }
         
  //Arquivos
  $sql_arq = new Query($bd);   
  $txt_arq = "SELECT HSSARQUI.NNUMEARQUI,TO_CHAR(HSSARQUI.DDATAARQUI,'DD/MM/YYYY HH24:mi') DDATAARQUI,
                     HSSARQUI.CDESCARQUI,NVL(HSSARQUI.NSUPEARQUI,0) NSUPEARQUI,
                     DECODE(HSSARQUI.CEXT_ARQUI,'PNG','S','JPG','S','JPEG','S','BMP','S','GIF','S','N') IMAGEM,
                     DECODE(HSSARQUI.CTIPOARQUI,'D','S','N') DIRETORIO,
                     HSSARQUI.CEXT_ARQUI EXTENSAO,HSSARQUI.CNOMEARQUI
                FROM HSSARQUI,HSSARQNT
               WHERE HSSARQUI.NNUMEARQUI = HSSARQNT.NNUMEARQUI(+)
                 AND HSSARQUI.CLOCAARQUI = 'NOSSACOOPERATIVA'
                 AND NVL(HSSARQUI.NSUPEARQUI,0) = :diretorio 
                 AND HSSARQUI.CTIPOARQUI = 'A'         
               ORDER BY HSSARQUI.DDATAARQUI DESC,HSSARQUI.CDESCARQUI,HSSARQUI.NNUMEARQUI DESC";   
  $sql_arq->addParam(":diretorio",$diretorio);           
  $sql_arq->executeQuery($txt_arq);
      
  while (!$sql_arq->eof()) {
           	            
    $tpl->IMAGEM       = "N";
        
    if ($sql_arq->result("IMAGEM") == 'S') {
      $tpl->IMAGEM_ICONE   = "imagem.png";    
      $tpl->TIPO           = "Arquivo de imagem"; 
      $tpl->IMAGEM         = $sql_arq->result("IMAGEM");      
    }
    else {    
      if (in_array($sql_arq->result("EXTENSAO"),array('XLS','XLSX')))
        $tpl->IMAGEM_ICONE = "excel2.png";    
      else if (in_array($sql_arq->result("EXTENSAO"),array('DOC','DOCX','ODT')))
        $tpl->IMAGEM_ICONE = "word.png";    
      else if (in_array($sql_arq->result("EXTENSAO"),array('PDF'))) {
        $tpl->IMAGEM_ICONE = "pdf2.png";    
        $tpl->IMAGEM       = "S";
      }        
      else
        $tpl->IMAGEM_ICONE = "novo.png";    
    
      $tpl->TIPO         = "Arquivo ".$sql_arq->result("EXTENSAO");
    }    
      
    $tpl->DIRETORIO      = "N";
    $tpl->DIRETORIO_NOME = "";    
    $tpl->PROTOCOLO      = $sql_arq->result("NNUMEARQUI");
    $tpl->DATA_ENVIO     = $sql_arq->result("DDATAARQUI");
    $tpl->ASSUNTO        = $sql_arq->result("CDESCARQUI");
    
    if (!$leitura) {
      $tpl->block("PERMITE_EDITAR3");
    }
    
    $tpl->block("LINHA");        

    $sql_arq->next();
  }
  
  echo utf8_encode($tpl->parse());
  
  $bd->close();
?>