<?php
  Session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
  
  require_once("../comum/layoutJanela.php"); 
  $tpl->addFile("JANELA_CONTEUDO","../comum/localizaFuncionario.html"); 
  $tpl->block("MOSTRA_LOGO");
  
  $nome       = '';  
  $qtde       = 20;
  $matricula  = '';
  $nascimento = '';
  $local      = '';
  $aso        = $_GET['aso'];
    
 
  $tpl->block("NOME_USUARIO");
  $id_contrato = $_GET['id_contrato'];;
  $tpl->ID_CONTRATO = $id_contrato;
    
  if (isset($_POST['localizar'])) {
    $nome       = strtoupper($seg->antiInjection($_POST['nome'],false));      
    $matricula  = $seg->antiInjection($_POST['matricula'],false);
    $qtde       = $seg->antiInjection($_POST['qtde']);  
	$nascimento = $seg->antiInjection($_POST['nascimento']);	
              
    $sql = new Query($bd);
    $txt = "SELECT CCODIUSUA,REPLACE(CNOMEUSUA,'''','') CNOMEUSUA,
                   TO_CHAR(DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,CCARTUSUA,
                   DECODE(CSITUUSUA,'M','Migrado','C','Cancelado','P','Funcionário candidato','Ativo') CSITUUSUA,IDADE(DNASCUSUA,SYSDATE) IDADE,
                   CDINDUSUA,CCODIPLAN,TO_CHAR(NVL(DALIBUSUA,DSITUUSUA),'DD/MM/YYYY') DALIBUSUA, CNMAEUSUA,CDESCPROF,
				   NOME_DEPARTAMENTO_USUARIO(HSSUSUA.NNUMEDEPAR) DEPARTAMENTO,CRAZAEMPR
              FROM HSSUSUA,HSSPLAN,HSSTITU,HSSPROF,HSSEMPR
             WHERE HSSUSUA.NNUMEUSUA > 0 
			   AND (CASO_TITU = 'S' OR CPCMSUSUA = 'S')
			   AND HSSUSUA.NNUMEPROF = HSSPROF.NNUMEPROF(+)
			   AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR";                             
    
	if($id_contrato > 0){
		$txt .= " AND HSSTITU.NNUMETITU = :contrato";
		$sql->addParam(":contrato",$id_contrato);
	}	
	
    if ($nome <> '') {
      $nome2 = $nome;
      $nome2 = strtoupper($formata->removeAcento($bd,$nome2,false))."%"; 
      $txt .= "   AND REMOVE_ACENTO(HSSUSUA.CNOMEUSUA) LIKE :nome";
      $sql->addParam(":nome",$nome2);
    }        
            
    if ($matricula <> '') {
      $matricula2 = $matricula;
      $matricula2 = $matricula2."%";
      $txt .= "   AND (CCODIUSUA LIKE :matricula OR CCHAPUSUA LIKE :matricula) ";
      $sql->addParam(":matricula",$matricula2);      
    }
  
    if ($nascimento <> '') {
      $nascimento2 = $nascimento;
      $txt .= "   AND HSSUSUA.DNASCUSUA = :nascimento ";
      $sql->addParam(":nascimento",$nascimento2);      
    }
    
    if ($_SESSION['sistema'] == 'Saude_Ocupacional') { 
	  if(isset($_SESSION['id_empresa_selecionada'])){
	    $txt .= "  AND HSSUSUA.NNUMETITU = :contrato 
		           AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";
        $sql->addParam(":contrato",$_SESSION['id_empresa_selecionada']);	
	  }
	  else{
		$txt .= "  AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";
	  }
	}
	else{
      $txt .= "  AND CTIPOUSUA <> 'F' 
                 AND HSSUSUA.NNUMETITU = :contrato 
				 AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";
      $sql->addParam(":contrato",$_SESSION['id_contrato']);			   
	}
	
    if (isset($_SESSION['id_estabelecimento']) and $_SESSION['id_estabelecimento'] > 0 ) {
      $txt .= "   AND HSSTITU.NNUMEESTAB = :estabelecimento ";
      $sql->addParam(":estabelecimento",$_SESSION['id_estabelecimento']);                                  
    }
	
	if($aso == '1')
	  $txt .= " AND CSITUUSUA = 'P' ";
	else
	  $txt .= " AND (CSITUUSUA = 'A' OR (CSITUUSUA = 'C' AND TRUNC(SYSDATE) <= DALIBUSUA))";	  
                                  
    $txt .= "   AND ROWNUM <= :qtde
              ORDER BY CNOMEUSUA ";            

    $sql->addParam(":qtde",$qtde);  

    $sql->executeQuery($txt);
    
    if ($formata->somenteNumeros($sql->result("DNASCUSUA")) == substr($formata->somenteNumeros(date('d/m/Y')),0,4)){
      $aniv = "SELECT COUNT(*) QTD
                 FROM HSSGUIA
                WHERE (CLOCAGUIA = '5' OR CLOCAGUIA = '6')
                  AND DEMISGUIA = :hoje";
      $select->addParam(":hoje",date('d/m/Y'));
      $select-executeQuery($aniv);
      
      $parabens = "Parabéns pelos seus " . $sql->result("IDADE") . 
                  " anos devida, sr(a). " . $sql->result("CNOMEUSUA") . "!";
      
      if($select->result("QTD") == 0) {
        $tpl->MSG = "<p>".$_SESSION['mensagem_web_empresa_pop']."</p>";
      }
    
    }

    while (!$sql->eof()) {
      $situacao =$sql->result("CSITUUSUA");        
      
//      if (($situacao <> "Ativo") and (strtotime($data->dataInvertida($sql->result('DALIBUSUA'))) > strtotime(date('Y/m/d'))))
//        $situacao = "Ativo";

      if ($situacao == "Ativo")
        $tpl->USUARIO_COR = "#000000";
      else
        $tpl->USUARIO_COR = "#FF0000";
                                                           
      $tpl->USUARIO_ID         = $sql->result('CCODIUSUA');
      $tpl->USUARIO_NOME       = $formata->initCap($sql->result('CNOMEUSUA'));
	  $tpl->USUARIO_EMPRESA      = $sql->result("CRAZAEMPR");
	  $tpl->USUARIO_PROFISSAO = $sql->result("CDESCPROF");
	  $tpl->USUARIO_SETOR = $sql->result("DEPARTAMENTO");
      $tpl->USUARIO_CODIGO     = $sql->result("CCODIUSUA");      
      $tpl->USUARIO_NASCIMENTO = $sql->result("DNASCUSUA");
      $tpl->USUARIO_SITUACAO   = $situacao; 
      $tpl->block("USUARIO");
      $sql->next();
    }
    
    $tpl->block("RESULTADO");    
  }
  
  if ($_SESSION['sistema'] == 'Operadora') {
    $sql = new Query($bd);
    $txt = "SELECT NNUMEEMPR, CRAZAEMPR, CSITUEMPR
              FROM HSSEMPR
             ORDER BY CRAZAEMPR";			 
    $sql->executeQuery($txt);
    
    while (!$sql->eof()) {
      $tpl->EMPRESA_ID        = $sql->result("NNUMEEMPR");
      $tpl->EMPRESA_DESCRICAO = $sql->result("CRAZAEMPR");
      
      if ($sql->result("CSITUEMPR") <> 'A') 
        $tpl->EMPRESA_COR = "red";
      else
        $tpl->EMPRESA_COR = "";

      $tpl->block("LINHA_EMPRESA");
      
      $sql->next();
    }
    
    $tpl->block("MOSTRA_EMPRESA");    
  }
  
  $tpl->NOME = $nome;
  
    
  $tpl->matricula   = $matricula;
  $tpl->NASCIMENTO = $nascimento;
  $tpl->QTDE       = $qtde;
  
  $bd->close();
  $tpl->show();  
?>