<?php
  
class Beneficiario {

  private $bd;
  private $codigoErro;
  private $descricaoErro;
  private $atendimentoWeb;
  private $erros      = array();
  private $errosFatal = array();
  
  public $leitorCartao;
  public $situacao;
  public $naturezaJuridica;
  public $idBeneficiario;
  public $nome;
  public $idContrato;
  public $dataInclusao;
  public $dataCancelamento;
  public $codigoMigrado;
  public $dataStatusBeneficiario;
  public $dataStatusContrato;
  public $statuGuiaBeneficiario;
  public $statuGuiaContrato;
  public $dataAtual;
  public $dataNascimento;
  public $codigoBeneficiario;
  public $sexo;
  public $cpf;
  public $rg;
  public $cns;
  public $nomeDaMae;  
  public $nomeDoPai;
  public $intercambio;
    
  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  } 
  
  public function __construct($bd,$codigo,$id="") {
  
    if (($bd == '') or ($codigo == '' and $id == '')) {
      $this->adicionaErroFatal('parametros','Parametros inv�lidos.');
    }
    else {
      $this->bd                 = $bd;
          
      if ($id <> "")
        $this->idBeneficiario = $id;
      else
        $this->codigoBeneficiario = $codigo;
   
      if ($this->leitorCartao == 'S')
        $this->codigoBeneficiario = $this->decodificaCodigoCartao();
   
      $this->dadosBeneficiario();
    }
  }
  
  private function dadosBeneficiario() {

    if ($this->getErrosFatal() == 0) {
      
      $sql = new Query($this->bd);
      
      if ($this->idBeneficiario > 0) {                
        $f_where = " WHERE USUA.NNUMEUSUA = :id";
        $sql->addParam(":id",$this->idBeneficiario);
      }
      else {
        if ($_SESSION['apelido_operadora'] == 'casf')
          $f_where = " WHERE USUA.CCARTUSUA = :codigo";
        else
          $f_where = " WHERE USUA.CCODIUSUA = :codigo";
        $sql->addParam(":codigo",$this->codigoBeneficiario);
      }
      
      if (($_SESSION['apelido_operadora'] == 'carloschagas') or ($_SESSION['apelido_operadora'] == 'carloschagaspcmso'))
        $f_aso = "   AND NVL(CASO_TITU,'N') = 'N'";
      else
        $f_aso = "";              
      
      $txt = "SELECT USUA.CCODIUSUA,USUA.CNOMEUSUA,USUA.CSITUUSUA,USUA.NNUMEUSUA,USUA.NUPOSUSUA,
                     DECODE(HSSTITU.NNUMEEMPR,NULL,'PF',DECODE(HSSTITU.CBOLETITU,'S','PFF','PJ')) NJ,
                     USUA.CSENHUSUA SENHA_USUARIO,TITU.CSENHUSUA SENHA_TITULAR,
                     TO_CHAR(USUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,
                     TO_CHAR(USUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,
                     TO_CHAR(NVL(USUA.DPVALUSUA,SYSDATE),'YYYYMMDD') DPVALUSUA,
                     TO_CHAR(USUA.DSITUUSUA,'DD/MM/YYYY') DATA_CANCELAMENTO,
                     TO_CHAR(NVL(USUA.DALIBUSUA,USUA.DSITUUSUA),'YYYYMMDD') DALIBUSUA,
                     TO_CHAR(SYSDATE,'YYYYMMDD') HOJE, TO_CHAR(USUA.DINCLUSUA,'YYYYMMDD') DINCLUSUA2,
                     TO_CHAR(NVL(NVL(USUA.DVIGEUSUA,HSSTITU.DVIGETITU),SYSDATE),'YYYYMMDD') DVIGEUSUA, 
                     NVL(NVL(USUA.CLINTUSUA,HSSTITU.CLINTTITU),'S') CLINTTITU, HSSTITU.NNUMETITU,
                     TO_CHAR(USUA.DSTATUSUA,'YYYYMMDD') DSTATUSUA,USUA.CNMAEUSUA,USUA.CNPAIUSUA,
                     TO_CHAR(HSSTITU.DSTATTITU,'YYYYMMDD') DSTATTITU,HSSPLAN.CCODIPLAN,HSSPLAN.CDESCPLAN,
                     HSSTITU.NNUMECONGE,USUA.CSEXOUSUA,USUA.CSUS_USUA,USUA.C_CPFUSUA,USUA.C__RGUSUA,
                     STAT_USUA.CWEB_STAT SG1,STAT_TITU.CWEB_STAT SG2,
                     DECODE(NVL(STAT_USUA.NNUMESTAT,0),0,STAT_TITU.CMENWSTAT,STAT_USUA.CMENWSTAT) MENSAGEM
                FROM HSSUSUA USUA,HSSTITU,HSSUSUA TITU,HSSSTAT STAT_USUA,HSSSTAT STAT_TITU,HSSPLAN ".
             $f_where.                
             "   AND USUA.CTIPOUSUA <> 'F'
                 AND USUA.NNUMETITU = HSSTITU.NNUMETITU
                 AND USUA.NTITUUSUA = TITU.NNUMEUSUA ".
             $f_aso.
             "   AND USUA.NNUMESTAT = STAT_USUA.NNUMESTAT(+)
                 AND HSSTITU.NNUMESTAT = STAT_TITU.NNUMESTAT(+)
                 AND USUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
               ORDER BY USUA.CSITUUSUA ";
      $sql->executeQuery($txt); 

      if (($this->codigoBeneficiario == '') and ($this->idBeneficiario == '')) 
        $this->adicionaErroFatal('naoInformado','C�digo do benefici�rio n�o informado.');        
      else if ($sql->result("NNUMEUSUA") == 0)
        $this->adicionaErroFatal('naoLocalizado','Benefici�rio n�o localizado.');        
      else {                     
        $this->situacao           = $sql->result("CSITUUSUA");
        $this->naturezaJuridica   = $sql->result("NJ");
        $this->idBeneficiario     = $sql->result("NNUMEUSUA");
        $this->codigoBeneficiario = $sql->result("CCODIUSUA");
        $this->nome               = $sql->result("CNOMEUSUA");
        $this->idContrato         = $sql->result("NNUMETITU");
        $this->dataInclusao       = $sql->result("DINCLUSUA");
        $this->dataCancelamento   = $sql->result("DATA_CANCELAMENTO");    
        $this->atendimentoWeb     = $sql->result("CLINTTITU");
      
        if ($this->situacao == 'M') {
          $sql2 = new Query($this->bd);
          $txt = "SELECT CCODIUSUA, CNOMEUSUA FROM HSSUSUA WHERE NNUMEUSUA = :usuario ";
          $sql2->addParam(":usuario",$sql->result("NUPOSUSUA"));
          $sql2->executeQuery($txt); 
        
          $this->codigoMigrado = $sql2->result("CCODIUSUA");
        }
      
        $this->dataStatusBeneficiario = $sql->result("DSTATUSUA");
        $this->dataStatusContrato     = $sql->result("DSTATTITU");
        $this->statuGuiaBeneficiario  = $sql->result("SG1");
        $this->statuGuiaContrato      = $sql->result("SG2");
        $this->dataAtual              = $sql->result("HOJE");
        $this->mensagemStatus         = $sql->result("MENSAGEM");
        $this->senhaBeneficiario      = $sql->result("SENHA_USUARIO");
        $this->senhaTitular           = $sql->result("SENHA_TITULAR"); 
        $this->validadeCarteirinha    = $sql->result("DPVALUSUA"); 
        $this->dataInclusaoInvertida  = $sql->result("DINCLUSUA2"); 
        $this->vigencia               = $sql->result("DVIGEUSUA"); 
        $this->dataFinalAutorizacao   = $sql->result("DALIBUSUA");
        $this->codigoPlano            = $sql->result("CCODIPLAN");
        $this->descricaoPlano         = $sql->result("CDESCPLAN");
        $this->idCongenere            = $sql->result("NNUMECONGE");
        $this->dataNascimento         = $sql->result("DNASCUSUA");
        $this->sexo                   = $sql->result("CSEXOUSUA");
        $this->cpf                    = $sql->result("C_CPFUSUA");
        $this->rg                     = $sql->result("C__RGUSUA");
        $this->cns                    = $sql->result("CSUS_USUA");        
        $this->nomeDaMae              = $sql->result("CNMAEUSUA"); 
        $this->nomeDoPai              = $sql->result("CNPAIUSUA"); 
        $this->intercambio            = $this->verificaIntercambio($this->codigoBeneficiario);
      }
    }
  }
    
  public function adicionaErro($codigo,$descricao,$idCodigo='') {
  
    if (($this->intercambio == 'N') or ($codigo == 'bloqueioStatus'))
      array_push($this->erros,array($codigo,$descricao,$idCodigo));
  }   
  
  public function adicionaErroFatal($codigo,$descricao) {
    array_push($this->errosFatal,array($codigo,$descricao));
  }     
    
  public function getQtdeErros() {
    return sizeOf($this->erros);
  }  
  
  public function getQtdeErrosFatal() {
    return sizeOf($this->errosFatal);
  }    
  
  public function getErros() {
    
    $desc_erros = "";
    for ($b = 0;$b < sizeof($this->erros);$b++) {      
      $erro = array();
      $erro = $this->erros[$b];
      
      if (sizeof($this->erros) > 1)
        $desc_erros .= "* ".$erro[1]."<br><br>";      
      else
        $desc_erros .= "* ".$erro[1]."<br>";
    } 
    
    return $desc_erros;
  }

  public function getArrayErros() {  
    return $this->erros;
  }  

  public function getErrosFatal() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->errosFatal);$b++) {      
      $erro = array();
      $erro = $this->errosFatal[$b];
      
      if (sizeof($this->errosFatal) > 1)
        $desc_erros .= ($b+1)." - ".$erro[1]."<br><br>";      
      else
        $desc_erros .= $erro[1]."<br>";
    } 
    
    return $desc_erros;
  }   
  
  public function getArrayFatalErros() {  
    return $this->errosFatal;
  }   

  public function verificaIntercambio($codigo){

    $sql = new Query($this->bd);
	$txt = "select DECODE(ContratoIntercambioUnimed(:codigo),NULL,'N','S') INTERCAMBIO
		      from dual";
    $sql->addParam(":codigo",$codigo);
    $sql->executeQuery($txt);	

	$Intercambio = $sql->result("INTERCAMBIO");  
	return $Intercambio;
  }  
  
  public function validaBeneficiarioGuia($nascimento,$senha,$regime,$natureza,$guiaPrincipal) {
    
    $seg  = new Seguranca();
    $func = new Funcao();
    
    $senha = strtoupper($senha);
    if ($this->getErrosFatal() == 0) {
    
      if ($this->leitorCartao == 'S')
        $this->codigoBeneficiario = $this->decodificaCodigoCartao();
      
      if ($this->codigoBeneficiario == '')
        $this->adicionaErroFatal('naoInformado','C�digo do benefici�rio n�o informado.');        
      else if ($this->idBeneficiario == 0)
        $this->adicionaErroFatal('naoLocalizado','Benefici�rio n�o localizado.');        
      else {       
        
        if ($this->atendimentoWeb == "N") {
          
          if (($_SESSION['apelido_operadora'] == "promedmg") and ($this->idContrato == 2739)) // Coopeder
            $texto = 'Os usu�rios deste contrato devem solicitar a autoriza��o pr�via em sua regional. 
                      Autoriza��o pela web n�o permitida';
          else if (($_SESSION['apelido_operadora'] == "promedmg") and ($this->idContrato == 440238)) // TRF
            $texto = 'Benefici�rios deste contrato n�o necessitam autoriza��o pr�via para atendimento, '.
                     'ser� necess�rio autorizacao pela central de atendimento, apenas procedimentos acima '.
                     'de 800chs. Vide carteira do beneficiario';
          else
            $texto = 'Autoriza��es para este contrato/usu�rio n�o podem ser emitidas pela web.';

          $this->adicionaErro('atendimentoNaoPermitidoNaWeb',$texto);
            
        }

        if ($this->situacao <> "A" and $this->dataFinalAutorizacao < $this->dataAtual) {   	 
          
		  if ($this->situacao == "C" ){
			if ($_SESSION['apelido_operadora'] == 'saudemed')
              $this->adicionaErro('cancelado','N�o foi poss�vel emitir esta autoriza��o. Gentileza entrar em contato com a Operadora.',12);	
			else
			  $this->adicionaErro('cancelado','Contrato cancelado em '.$this->dataCancelamento.'.',12);
          } else if ($this->situacao == "M")
            $this->adicionaErro('migrado','Contrato migrado em '.$this->dataCancelamento.'. Novo c�digo '.$this->codigoMigrado.'.',12);
          else
            $this->adicionaErro('inativo','Benefici�rio inativo.',12);
        } 
        else {
               
          if (($this->dataStatusBeneficiario <= $this->dataAtual and $this->statuGuiaBeneficiario == "N") or
              ($this->dataStatusContrato     <= $this->dataAtual and $this->statuGuiaContrato     == "N") or
              ($this->dataStatusContrato     <= $this->dataAtual and $this->statuGuiaContrato     == "U" and $regime == "E")) {
            
            if ($this->mensagemStatus <> '')
              $texto = $this->mensagemStatus;
            else if ($_SESSION['apelido_operadora'] == 'unimedFranca') // Unimed franca status SUSPENSO
              $texto = 'Favor procurar a empresa associada.';
            else
              $texto = 'Favor procurar a operadora.';
              
            $this->adicionaErro('bloqueioStatus',$texto,27); //27-> Insere a critica (Benefici�rio com status que bloqueia atendimento)na hsscriti... Ver guia.sql
          }      
                   
          if ($_SESSION['senha_usuario'] == 'S') {
            
			
            if (($_SESSION["apelido_operadora"] == "nossaSaude") or                                                
                ($_SESSION['apelido_operadora'] == "santaRita" and $_SESSION['tem_leitor_biometrico'] == "N")) {
				
				
              if ($senha <> '') {
                if ($this->senhaTitular <> $senha and $this->senhaBeneficiario <> $senha)
                  $resultado_rita = "F";
                else
                  $resultado_rita = "T";

                if ($resultado_rita == "F" and $nascimento <> $this->dataNascimento)
                  $this->adicionaErro('senhaIncorreta','Senha incorreta.');
              }
              else if ($senha == '' and $nascimento <> $this->dataNascimento)
                $this->adicionaErro('senhaIncorreta','Senha incorreta.');
            }
            else if (($senha == '' or $this->senhaBeneficiario <> $senha))
              $this->adicionaErro('senhaIncorreta','Senha incorreta.');
          }
          
          $sql2 = new Query($this->bd);
          $txt = "SELECT NNUMEUSUA FROM HSSOBUSU 
                   WHERE NNUMEUSUA = :usuario 
                     AND CBLOQOBUSU = 'S' 
                     AND DLIMIOBUSU > SYSDATE";
          $sql2->addParam(":usuario",$this->idBeneficiario);
          $sql2->executeQuery($txt);
            
          if ($sql2->result("NNUMEUSUA") > 0 )
            $this->adicionaErro('bloqueioObservacao','N�o foi poss�vel emitir esta autoriza��o. Gentileza entrar em contato com a Operadora. (2)');
            
          if (($this->validadeCarteirinha < $this->dataAtual) and ($_SESSION['apelido_operadora'] <> 'santaRita'))
            $this->adicionaErro('carteirinhaVencida','Validade da carteirinha vencida. Favor solicitar ao benefici�rio que entre em contato com a operadora.',26);

          // Ativo porem com data de inclusao > que a data atual
          if ($this->dataInclusaoInvertida > $this->dataAtual)
            $this->adicionaErro('atendimentoNaoLiberado','Atendimento liberado ap�s '.$this->dataInclusao.'.');
 
          // Ativo com data de vig�ncia vencida
          if ($this->vigencia < $this->dataAtual)
            $this->adicionaErro('vigenciaVencida','Benefici�rio com data de vig�ncia vencida. Favor solicitar ao benefici�rio que entre em contato com a operadora.',26);
            
          if ((($natureza == 'A') or
              (($tipoGuia == 'guiaInternacao') and ($_SESSION['apelido_operadora'] == 'unimedJiParana'))) 
              and ($seg->permissaoOutros($this->bd,"WEBPRESTADORVERIFICAPACIENTEINTERNADO",false))) {
            if ($_SESSION['apelido_operadora'] == 'unimedJiParana'){
              $tabGuias = ", HSSDGUI";
              $tabGuias2 = "AND HSSGUIA.NCOMPGUIA IS NULL
                           AND HSSGUIA.NNUMEGUIA = HSSDGUI.NNUMEGUIA";
            }  
            else {
              $tabGuias  = "";
              $tabGuias2 = "";
            }

            if ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista')
              $status = " AND NVL(CSTATGUIA,' ') NOT IN ('X','N','C') ";
            else
              $status =  " AND NVL(CSTATGUIA,' ') NOT IN ('X','N') ";				
            
            $sql_int = new Query($this->bd);
            $txt_int = "SELECT HSSGUIA.NNUMEGUIA FROM HSSGUIA ".$tabGuias."
                         WHERE NNUMEUSUA = :usuario
                           AND CTIPOGUIA IN ('I','C','O','R','P','S')
                           AND (DALTAGUIA IS NULL OR (DALTAGUIA > SYSDATE AND DINTEGUIA <= SYSDATE))
                           AND NPTU_GUIA IS NULL ".
                          $status.
                           " AND NCOMPGUIA IS NULL".
                           $tabGuias2;
            $sql_int->addParam(":usuario",$this->idBeneficiario);
            $sql_int->executeQuery($txt_int);    
                 
            if ($sql_int->result("NNUMEGUIA") > 0)
              $this->adicionaErro('pacienteInternado','Este benefici�rio est� internado, n�o � permitido a gera��o de guias ambulatoriais.',51);
          }   

          if ($guiaPrincipal > 0){
            if ($func->validaGuiaPrincipal($guiaPrincipal,$this->idBeneficiario,$natureza) <= 0) 
              $this->adicionaErro('guiaPrincipalInvalida','Guia principal informada inv�lida ou n�o pertence a este benefici�rio.');
          }              
        }
      }
    }
  }
  
  public function decodificaCodigoCartao() {
    $codigo = trim($this-codigoBeneficiario);   
  
    if ($_SESSION['apelido_operadora'] == 'santaRita')
      $codigo = substr($codigo,1,12);
    
    return $codigo;
  }
  
  public function verificarInadimplencia() {
  
    $sql = new Query($this->bd);
    $txt = "SELECT USUARIOINADIMPLENTE(:usuario) RESULTADO FROM DUAL";
    $sql->addParam(":usuario",$this->idBeneficiario);
    $sql->executeQuery($txt);  
            
    if ($sql->result("RESULTADO") <> 'N')
      $this->adicionaErro('inadimplente','N�o foi possivel efetuar esta opera��o. Solicite ao benefici�rio para entrar em contato com a Operadora.',12);
  }
  
  public function verificarCarencia() {
  
    $sql = new Query($this->bd);
    $txt = "SELECT USUARIOINADIMPLENTE(:usuario) RESULTADO FROM DUAL";
    $sql->addParam(":usuario",$this->idBeneficiario);
    $sql->executeQuery($txt);  
            
    if ($sql->result("RESULTADO") <> 'N')
      $this->adicionaErro('inadimplente','N�o foi possivel efetuar esta opera��o. Solicite ao benefici�rio para entrar em contato com a Operadora.',12);
  }  
  
}

?>