<?php
  class Util {
  
    function abreArquivo ($url,$largura=500,$altura=100) {
       
      $txt = '';
      $txt .= '<script type="text/javascript">      
      
            var tam1 = (window.screen.availWidth / 2) - ('.$largura.' / 2) - (window.screen.availWidth * 0.01);
            var tam2 = (window.screen.availHeight / 2) - ('.$altura.'/ 2) - (window.screen.availHeight * 0.062);

            var win = open("'.$url.'","janela","top="+tam2+",left="+tam1+",width='.$largura.',height='.$altura.',status=yes,scrollbars=yes,resizable=yes");
            win.focus();
            ';
        
      $txt .= '</script>';
      
      return $txt;      
    }
    
    function redireciona ($url,$novapagina="N",$paginaatual="",$forma="0") {
       
      $txt = '';
      if ($novapagina == "S") {      
        $txt .= '<script type="text/javascript">      
        
              var tam1 = (window.screen.availWidth / 2) - (500 / 2) - (window.screen.availWidth * 0.01);
              var tam2 = (window.screen.availHeight / 2) - (600 / 2) - (window.screen.availHeight * 0.062);

              var win = open("'.$url.'","janela","top="+tam2+",left="+tam1+",width=500,height=600,status=yes,scrollbars=yes,resizable=yes");';              
                
        if ($paginaatual <> '')
          $txt .= ' window.location.href="'.$paginaatual.'";';
          
        $txt .= '</script>';
      } 
      else {
          $txt .= '<script type="text/javascript">
                  window.location.href="'.$url.'";
                </script>
                <noscript>
                  <meta http-equiv="refresh" content="0;url='.$url.'" />
                </noscript> ';
      }
      
      if ($forma == '1')
        return $txt;
      else
        echo $txt;
      
    }
    
    function redireciona2 ($url,$parametros = array(),$novapagina="N",$paginaatual="") {
      
      $txt = '';      
      $txt .= '<form id="formSubmit" name="formSubmit" method="post" action="" target="">';
      
      foreach ($parametros as $p) {
        $p1 = array();
        $p1 = $p;
        
        $txt .= '<input type="hidden" id="'.$p1['nome'].'" name="'.$p1['nome'].'" value="'.$p1['valor'].'">';
      }
              
      $txt .= '</form>';
  
      $txt .= '<script type="text/javascript" src="../comum/js/jquery-1.8.2.min.js"></script>
               <script type="text/javascript">';
            
      if ($novapagina == "S") {
        $titulo = "janela";
        
        $txt .= '
        
              var tam1 = (window.screen.availWidth / 2) - (500 / 2) - (window.screen.availWidth * 0.01);
              var tam2 = (window.screen.availHeight / 2) - (600 / 2) - (window.screen.availHeight * 0.062);

              var win = open("'.$url.'","'.$titulo.'","top="+tam2+",left="+tam1+",width=600,height=500,status=yes,scrollbars=yes,resizable=yes");';
      }
      else
        $titulo = "";
   /*  
      foreach ($parametros as $p) {
        $p1 = array();
        $p1 = $p;
        
        $txt .= '$("#'.$p1['nome'].'").val('.$p1['valor'].');';
      }
      */
      
      $txt .= '     
              $("#formSubmit").attr("target","'.$titulo.'");
              $("#formSubmit").attr("action","'.$url.'");
              $("#formSubmit").submit();
              $("#formSubmit").hide();
          
            </script>';
            
      return $txt;
    }          
    
    function insereLogOperacao($id,$descricao,$local = NULL) {
      $sql = new Query();
      $txt = "BEGIN ".
             "  INSERE_LOG_WEB30 (:id,:descricao,:operador,:ip,:protocolo,:local); ".
             "END; ";
      $sql->addParam(":id",$id);  
      $sql->addParam(":descricao",$descricao); 
      $sql->addParam(":operador",$_SESSION['id_operador']);                                                 
      $sql->addParam(":ip",getenv("REMOTE_ADDR"));
      $sql->addParam(":protocolo",0,12);  
      $sql->addParam(":local",$local); 
      $erro = $sql->executeSQL($txt);
      
      $retorno['erro'] = $erro;
      $retorno['id'] = $sql->getReturn(":protocolo");

      return $retorno;
    }
    
    function criaDiretorio($nome) {  
      if (!file_exists($nome)) {
        mkdir($nome,0777);
        chdir($nome);
        $nome = getcwd();
      } else {
        chdir($nome);  
        $nome = getcwd();    
      }
      return $nome;
    }    

    function excluiArquivo($dir,$arquivo) {
      $dir .= '/*';
      $contador = 1;

      foreach (glob($dir,GLOB_NOCHECK ) as $filename) { 

        if (is_dir($filename) <> 1) {
          if (basename($filename) == $arquivo) {
            unlink($filename);
            return 1;
          }
        }
        else {
          $this->excluiArquivo($filename,$arquivo);
        }
      }               
    }
    
    function corrigeNomeArquivo($arquivo) {
    
      $arquivo = strtolower($arquivo);
      //$arquivo = str_replace(".","",$arquivo);
      $arquivo = strtr($arquivo, "�������������������������� ","aaaaeeiooouucAAAAEEIOOOUUC_");
          
      return utf8_decode($arquivo);
    }  

    function validaExtensaoArquivo($arquivo,$extensoes=array()) {
      $extensao            = strtolower(pathinfo($arquivo, PATHINFO_EXTENSION));
      
      if (sizeOf($extensoes) > 0)
        $extensoesPermitidas = $extensoes;
      else
        $extensoesPermitidas = array("jpg","jpeg", "png", "pdf", "doc", "txt", "docx", "xlsx", "rar", "zip");       
      
      if (in_array($extensao,$extensoesPermitidas) == false)
        return '- N�o � permitido o envio de arquivos com extensao ".' . $extensao . '"<br>';
      else
        return '';
    }    
        
    function modulo11 ($numero) {
      $soma = 0;
      $fator = 1;

      for ($i = strlen($numero)-1;$i >= 0;$i--) {
        $fator++;
        if ($fator == 10)
          $fator = 2;
        $soma += $numero[$i] * $fator;
      }

      $resto = $soma % 11;
      $resto = 11 - $resto;
      if ($resto < 10)
        return $resto;
      else
        return 0;
    } 

    function nomeValido ($nome) {
      $formata = new Formata();
      
      if (strlen($formata->somenteCaracteres($nome)) < 6)
        return false;
      else if (substr_count($nome,' ') == 0)
        return false;
      else
        return true;

    }  

    function caracteresInvalidos ($variavel) {
      $indice = 0;
      $retorno = "";
      $caracteres = "*�./()_#-\`|:���~!@$%^&+=;?�[]{}��������������";
      while ($indice < strlen($variavel)) {
        if (substr_count($caracteres,substr($variavel,$indice,1)) > 0)
          $retorno = $retorno.substr($variavel,$indice,1);
          
        $indice = $indice + 1;
      }
      
      return $retorno;
    }

    function nomeAbreviado($nome) {
    
      $estaAbreviado = "N";      
      $i             = 1;   
      
      $nomes = explode(" ",strToUpper($nome));
      
      foreach ($nomes as $n) {
        if ((strLen($n) == 1) and ($estaAbreviado == "N") and ($n <> 'E'))
          $estaAbreviado = "S";
      }
      
      if ($estaAbreviado == "S")
        return true;
      else
        return false;
    } 
    
    function validaEmail($email) {
    
      if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        return false;
      else
        return true;
    }    
    
    function validarCampo($nome,$valor,$validacoes = array()) {
      
      $formata = new Formata();
      $datas   = new Data();
      
      $retorno = '';
      
      foreach ($validacoes as $regra) {      

        if ((substr($regra,0,11) == 'obrigatorio') and ($valor == '')) {
          $retorno .= $nome." � de preenchimento obrigat�rio<br>";
        }
        
        if (substr($regra,0,6) == 'minimo') {
          $tamanho = substr($regra,7);
          
          if (strlen($valor) < $tamanho)
            $retorno .= $nome." deve ter no m�nimo ".$tamanho." caracteres<br>";          
        }
        
        if (substr($regra,0,6) == 'maximo') {
          $tamanho = substr($regra,7);
          
          if (strlen($valor) > $tamanho)
            $retorno .= $nome." deve ter no m�ximo ".$tamanho." caracteres<br>";          
        }
        
        if ($valor <> '') {
          if (substr($regra,0,6) == 'letras')
            if ($formata->somenteCaracteres(strToUpper($valor)) <> strToUpper($valor))
              $retorno .= $nome." deve conter somente letras (a-z) sem acentua��o.<br>";    

          if (substr($regra,0,9) == 'abreviado')
            if ($this->nomeAbreviado($valor))
              $retorno .= $nome." n�o deve estar abreviado.<br>";    
              
          if (substr($regra,0,5) == 'email') 
            if (!$this->validaEmail($valor))
              $retorno .= $nome." informado n�o � v�lido (".$valor.")<br>";          

          if (substr($regra,0,3) == 'cpf')
            if (!$this->validaCPF($valor))
              $retorno .= $nome." informado n�o � v�lido (".$valor.")<br>";          

          if (substr($regra,0,3) == 'cns') 
            if (!$this->validaCNS($valor))
              $retorno .= $nome." informado n�o � v�lido (".$valor.")<br>";          
              
          if (substr($regra,0,3) == 'data') 
            if (!$datas->validaData($valor))
              $retorno .= $nome." informada n�o � v�lida (".$valor.")<br>";          
              
              
        }
      }
              
      return $retorno;
    } 
      
    function validaCNS ($parametro) {
      $sql = new Query();
      $txt = "SELECT VALIDA_CNS(:parametro) VALIDO FROM DUAL";              
      $sql->addParam(":parametro",$parametro);
      $sql->executeQuery($txt);
      
      if ($sql->result("VALIDO") == "T")
        return true;
      else
        return false;
    }
                
    function validaCPF ($CPF) {
      $sql = new Query();
      $txt = "SELECT VALIDA_CPF(:parametro) VALIDO FROM DUAL";              
      $sql->addParam(":parametro",$CPF);
      $sql->executeQuery($txt);
      
      if ($sql->result("VALIDO") == "T")
        return true;
      else
        return false;
    }
      
    function sdebug($var, $exit = false) { 
      echo "\n<pre>"; 
      if (is_array($var) || is_object($var)) { 
        echo htmlentities(print_r($var, true)); 
      } elseif (is_string($var)) { 
        echo "string(" . strlen($var) . ") \"" . htmlentities($var) . "\"\n"; 
      } else { 
        var_dump($var); 
      } 
      echo "</pre>"; 
      if ($exit) { 
        exit; 
      } 
    }
    
    function validaPIS ($parametro) {
      $sql = new Query();
      $txt = "SELECT VALIDA_PIS(:parametro) VALIDO FROM DUAL";              
      $sql->addParam(":parametro",$parametro);
      $sql->executeQuery($txt);
      
      if ($sql->result("VALIDO") == "T")
        return true;
      else
        return false;
    }
    
  }
?>