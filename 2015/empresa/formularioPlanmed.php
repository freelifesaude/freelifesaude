<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $bd      = new Oracle();
  $formata = new Formata();
  $data    = new Data();
  $func    = new Funcao();
  
  $atcad = $_GET['p'];
  
  $txt = "SELECT CNOMEATCAD USUARIO,CSEXOATCAD,TO_CHAR(DNASCATCAD,'DD/MM/YYYY') DNASCATCAD,CCHAPATCAD,
                 C__RGATCAD,CORRGATCAD,C_CPFATCAD,CNMAEATCAD,CENDEATCAD,CBAIRATCAD,CCIDAATCAD,
                 CESTAATCAD,CCEP_ATCAD,NFATUTLOGR,CFONEATCAD,CTELRATCAD,CTELCATCAD,CDESCACOM,DDATAATCAD,
                 NRGMSPLAN,CESTCATCAD,CDESCPROF,retorna_nome_usuario_atcad(NTITUUSUA) TITULAR,
								 DECODE(CTIPOATCAD,'D','Dependente','U','Dependente universit�rio','E','Dependente especial',
																	 'L','Dependente legal (determina��o judicial)','T','Titular','F','Titular financeiro',
																	 'A','Agregado','Outro') CTIPOATCAD
            FROM HSSATCAD,HSSPLAN,HSSACOM,HSSPROF
           WHERE NNUMEATCAD = :id
						 AND HSSATCAD.NNUMEPLAN = HSSPLAN.NNUMEPLAN(+)
						 AND HSSATCAD.NNUMEACOM = HSSACOM.NNUMEACOM(+)
						 AND HSSATCAD.NNUMEPROF = HSSPROF.NNUMEPROF(+)";
             
  $sql = new Query($bd);
  $sql->addParam(":id",$atcad);
  $sql->executeQuery($txt);  

  $pdf = new PDFSolus('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','B',13);

  $pdf->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);

  $pdf->Cell(55,1);
  $pdf->Cell(80,5,'FORMUL�RIO DE ADES�O',0,0,'C');
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(5,5,'',0,0,'C');  
  $pdf->Cell(50,5,'Matr�cula',0,0,'C');
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(190,5,'',0,1,'R');
  $pdf->Cell(55,1);  
  $pdf->SetFont('Arial','',8);  
  $pdf->Cell(80,5,'(PLANO EMPRESARIAL)',0,0,'C');
  $pdf->SetFont('Arial','',8);    
  $pdf->Cell(5,7,'',0,0,'C');    
  $pdf->Cell(50,7,' ',1,1,'R');
  $pdf->Cell(190,15,'',0,1);

  $pdf->Cell(190,1,'',0,1);

  $pdf->SetFont('Arial','',8);
  $pdf->Cell(190,3,'Dados do funcion�rio titular',0,1,'L',1);

  $pdf->Cell(190,1,'',0,1);
  
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(169,4,'Nome','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(20,4,'Sexo','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(169,5,$sql->result("USUARIO"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(20,5,$sql->result("CSEXOATCAD"),'LRB',1,'L');
    
  $pdf->Cell(190,1,'',0,1);

  $pdf->SetFont('Arial','',8);
  $pdf->Cell(50,4,'Categoria','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(112,4,'Titular','LTR',0,'L');
  $pdf->Cell(1);  
  $pdf->Cell(26,4,'Data de ades�o','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(50,5,$sql->result("CTIPOATCAD"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(112,5,$sql->result("TITULAR"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(26,5,$sql->result("DDATAATCAD"),'LRB',1,'L');

  $pdf->Cell(190,1,'',0,1);
	
	$pdf->SetFont('Arial','',8);
  $pdf->Cell(39,4,'Data de nascimento','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(89,4,'Estado civil','LTR',0,'E');
  $pdf->Cell(1);
  $pdf->Cell(29,4,'RG','LTR',0,'E');
  $pdf->Cell(1);  
  $pdf->Cell(30,4,'Org�o expedidor','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);  
  $pdf->Cell(39,5,$sql->result("DNASCATCAD"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(89,5,$func->estadoCivil($sql->result("CESTCATCAD")),'LRB',0,'E');
  $pdf->Cell(1);
  $pdf->Cell(29,5,$sql->result("C__RGATCAD"),'LRB',0,'E');
  $pdf->Cell(1);  
  $pdf->Cell(30,5,$sql->result("CORRGATCAD"),'LRB',1,'L');

  $pdf->Cell(190,1,'',0,1);

  $pdf->SetFont('Arial','',8);
  $pdf->Cell(60,4,'CPF','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(129,4,'Cargo','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(60,5,$sql->result("C_CPFATCAD"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(129,5,$sql->result("CDESCPROF"),'LRB',1,'L');

  $pdf->Cell(190,1,'',0,1);
  
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(190,4,'Nome da m�e','LTR',1,'L');
  
  $pdf->SetFont('Arial','B',8);  
  $pdf->Cell(190,5,$sql->result("CNMAEATCAD"),'LBR',1,'L');
  
  $pdf->Cell(190,1,'',0,1);
  
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(149,4,'Endere�o','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(40,4,'CEP','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(149,5,$sql->result("CENDEATCAD"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(40,5,$sql->result("CCEP_ATCAD"),'LRB',1,'L');  
  
  $pdf->Cell(190,1,'',0,1);
  
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(89,4,'Bairro','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(89,4,'Cidade','LTR',0,'L');
  $pdf->Cell(1);  
  $pdf->Cell(10,4,'UF','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(89,5,$sql->result("CBAIRATCAD"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(89,5,$sql->result("CCIDAATCAD"),'LRB',0,'L');
  $pdf->Cell(1);  
  $pdf->Cell(10,5,$sql->result("CESTAATCAD"),'LRB',1,'L');  
  
  $pdf->Cell(190,1,'',0,1);
  
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(63,4,'Telefone','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(63,4,'Celular','LTR',0,'L');
  $pdf->Cell(1);  
  $pdf->Cell(62,4,'Telefone para recado','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(63,5,$sql->result("CFONEATCAD"),'LRB',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(63,5,$sql->result("CTELCATCAD"),'LRB',0,'L');
  $pdf->Cell(1);  
  $pdf->Cell(62,5,$sql->result("CTELRATCAD"),'LRB',1,'L');   

  $pdf->Cell(190,1,'',0,1);
  
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(49,4,'C�digo do plano - ANS','LTR',0,'L');
  $pdf->Cell(1);
  $pdf->Cell(61,4,'Acomoda��o','LTR',0,'L');
  $pdf->Cell(1);  
  $pdf->Cell(41,3,'','LTR',0,'L');
  $pdf->Cell(1);    
  $pdf->Cell(36,3,'','LTR',1,'L');

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(49,5,$formata->formataRegistroAns($sql->result("NRGMSPLAN")),'LRB',0,'L');
  $pdf->Cell(1);
  
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(61,5,$pdf->Copy($sql->result("CDESCACOM"),60),'LBR',0,'L');
  $pdf->Cell(1);    
  
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(10,5,'|___|','LB',0,'R');
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(10,5,' Agravo','B',0,'L');
  $pdf->Cell(1,5,'','B');
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(10,5,'| X |','B',0,'R');
  $pdf->SetFont('Arial','',8);
  $pdf->Cell(10,5,' CPT','RB',0,'L');
  $pdf->Cell(1);     
  $pdf->Cell(36,5,' Valor R$ _________,____','LRB',1,'L');    
  
  $pdf->Cell(190,3,'',0,1);
  
  $pdf->SetFont('Arial','',9);

  $pdf->Cell(190,3,'','LTR',1);  
  
  $msg = 'Solicito a diretoria da empresa '.$_SESSION['titular_contrato'].', minha inclus�o e de meus dependentes,'.
         ' acima relacionados no Plano de Sa�de PLANMED, e autorizo que seja descontado mensalmente de meus vencimentos a import�ncia acima citada.';
  
  $pdf->MultiCell(190,3,$msg,'LR',1,'L');

  $pdf->Cell(190,6,'','LR',1);  
  
  $pdf->Cell(80,3,'Goi�nia-GO, '.$data->dataExtenso($_SESSION['hoje'],0),'L',0);  
  $pdf->Cell(110,3,'In�cio de utiliza��o do plano: _____ de _______________ de _____','R',1);  

  $pdf->Cell(190,10,'','LR',1);  

  $pdf->Cell(60,5,'______________________________','L',0,'C');  
  $pdf->Cell(60,5,'______________________________','',0,'C');  
  $pdf->Cell(70,5,'______________________________','R',1,'C');  
  
  $pdf->Cell(60,3,'PLANMED','L',0,'C');  
  $pdf->Cell(60,3,'Visto e carimbo da empresa','',0,'C');  
  $pdf->Cell(70,3,'Assinatura do funcion�rio','R',1,'C');   
  
  $pdf->Cell(190,3,'','LBR',1);  
  
  $pdf->Cell(190,1,'',0,1);  
  $pdf->SetFont('Arial','',6);  
  $pdf->Cell(190,3,'Solus Computa��o - www.solus.inf.br :: Emitido por '.$_SESSION['operador'].' '.$data->dataAtual('DD/MM/YYYY HH24:MI'),0,1,'R');  
  
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  
  $bd->close();
  
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
?>
