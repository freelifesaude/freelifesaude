<?php 
  Session_start(); 
  
  require_once("../comum/autoload.php");
  require_once("../comum/sessao.php");
  
  $bd   = new Oracle();  
  
  $_SESSION['titulo'] = "TABELA DE PRE�OS";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","tabelaPrecos.html");

  $tpl->ID_SESSAO = $_GET['idSessao'];  
	
  $erro = "Os pre�os sofrer�o reajuste no anivers�rio do contrato. Em casos de contratos agrupados, duvidas contatar a operadora." ;
  $tpl->CLASSE = "alert-error";    
  $tpl->MENSAGEM = $erro;
  
  $sql_plano = new Query($bd);
  $sql_plano->clear();
  $txt_plano = "SELECT DISTINCT HSSTPLA.NNUMEPLAN,HSSTPLA.NNUMETPLA,CDESCTPLA,CDESCPLAN
                  FROM HSSTPLA,HSSPLAN,HSSRDESC
                 WHERE HSSTPLA.NNUMETPLA IN (SELECT NNUMETPLA FROM HSSUSUA
                                              WHERE HSSUSUA.NNUMETITU = :CONTRATO
                                              UNION ALL
                                             SELECT NNUMETPLA FROM HSSTPLA
                                              WHERE NNUMETITU = :CONTRATO)
                   AND HSSTPLA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                   AND HSSTPLA.NNUMERDESC = HSSRDESC.NNUMERDESC(+)
                 ORDER BY 1"; 			
  $sql_plano->addParam(":CONTRATO",$_SESSION['id_contrato']);
  $sql_plano->executeQuery($txt_plano);    
	
  $sql_vigencia = new Query($bd);
  $sql_vigencia->clear();
  $txt_vigencia = "SELECT DISTINCT HSSFETA.NNUMETPLA,DVIGEMANU, SUBSTR(REAJUSTE_TABELA_VIGENCIA(HSSFETA.NNUMETPLA,HSSFETA.NNUMEPLAN,HSSMANU.DVIGEMANU),01,240) REAJUSTE
                  FROM HSSFETA,HSSMANU
                  WHERE NNUMEPLAN = :PLANO
                  AND NNUMETPLA = :TABELA
                  AND HSSFETA.NNUMEFETA = HSSMANU.NNUMEFETA
                  ORDER BY DVIGEMANU DESC";

  while(!$sql_plano->eof()){
    $tpl->TITULOTABELA = $sql_plano->result("CDESCTPLA") . ' - ' . $sql_plano->result("CDESCPLAN");    
    $sql_vigencia->addParam(":PLANO",$sql_plano->result("NNUMEPLAN"));
    $sql_vigencia->addParam(":TABELA",$sql_plano->result("NNUMETPLA"));
    $sql_vigencia->executeQuery($txt_vigencia);    
    $tpl->block("TITULO_TABELA_1");	
 
    $sql_tpreco = new Query($bd);
    $sql_tpreco->clear();
    $txt_tpreco = "SELECT NMINIFETA,NMAXIFETA,DVIGEMANU,HSSMANU.NNUMEFETA,NVALOMANU,NDEPEMANU,NAGREMANU,
                   NADETMANU,NADEDMANU,NADEAMANU,NINCLUSUA,NALTEUSUA,DINCLMANU,DALTEMANU,NQTDIMANU,NQTDFMANU,NADEFMANU
                   FROM HSSFETA,HSSMANU
                   WHERE HSSFETA.NNUMEPLAN = :PLANO
                   AND HSSFETA.NNUMETPLA = :TABELA
                   AND HSSFETA.NNUMEFETA = HSSMANU.NNUMEFETA
                   AND HSSMANU.DVIGEMANU = :VIGENCIA
                   ORDER BY NMINIFETA,NMAXIFETA";
                   
    $sql_tpreco->addParam(":PLANO",$sql_plano->result("NNUMEPLAN"));
    $sql_tpreco->addParam(":TABELA",$sql_plano->result("NNUMETPLA"));
    $sql_tpreco->addParam(":VIGENCIA",$sql_vigencia->result("DVIGEMANU"));
    $sql_tpreco->executeQuery($txt_tpreco);    
 
    if ($sql_tpreco->count() > 0) {		
			while (!$sql_tpreco->eof()){ 
				$tpl->IDADEMIN = $sql_tpreco->result("NMINIFETA");
				$tpl->IDADEMAX = $sql_tpreco->result("NMAXIFETA");
				$tpl->PRECOT   = $sql_tpreco->result("NVALOMANU");
				$tpl->PRECOD   = $sql_tpreco->result("NDEPEMANU");
				$tpl->PRECOA   = $sql_tpreco->result("NAGREMANU");
				$tpl->ADESAOT  = $sql_tpreco->result("NADETMANU");
				$tpl->ADESAOD  = $sql_tpreco->result("NADEDMANU");
				$tpl->ADESAOA  = $sql_tpreco->result("NADEAMANU");
				$tpl->ADESAOTF = $sql_tpreco->result("NADEFMANU");          
				$sql_tpreco->next();
				$tpl->block("TABELA_1");   
			}		  
			$tpl->block("TABELA");  
    }else{
      $tpl->MSG = 'N�o h� tabela de pre�o para este plano.';
      $tpl->block("ERRO");     
    }
    $sql_plano->next();
  }
 
  $tpl->block("TABELA");       
  $tpl->block("RESULTADO");       
 
  $tpl->block("MOSTRA_MENU");     
  $bd->close();
  $tpl->show();     
?>