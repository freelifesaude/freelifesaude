<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");

  $bd = new Oracle();  
  
  $_SESSION['titulo'] = "POPULA��O DE USU�RIOS";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","relTotalizacao.htm");

  $situacao     = 'A';  
  $periodo      = 'I';
  $formato      = '1';
  $locacao      = $_SESSION['id_locacao'];
  $status       = '';
  $datainicial  = '';
  $datafinal    = '';
  $idadeinicial = '';
  $idadefinal   = '';
    
  if (isset($_POST['situacao']))
    $situacao = htmlentities($_POST['situacao']);
    
  if (isset($_POST['periodo']))
    $periodo = htmlentities($_POST['periodo']);

  if (isset($_POST['formato']))
    $formato = htmlentities($_POST['formato']);
    
  if (isset($_POST['locacao']))
    $locacao = htmlentities($_POST['locacao']);  
  
  if (isset($_POST['status']))
    $status   = htmlentities($_POST['status']);  

  if (isset($_POST['datainicial']))
    $datainicial = htmlentities($_POST['datainicial']);    
    
  if (isset($_POST['datafinal']))
    $datafinal = htmlentities($_POST['datafinal']);
	
  if (isset($_POST['idadeinicial']))
    $idadeinicial = htmlentities($_POST['idadeinicial']);    
    
  if (isset($_POST['idadefinal']))
    $idadefinal = htmlentities($_POST['idadefinal']);	
  
  $sql = new Query($bd);
  $txt = "SELECT DISTINCT HSSSTAT.NNUMESTAT,CDESCSTAT
            FROM HSSUSUA,HSSSTAT
           WHERE HSSUSUA.NNUMETITU = :contrato
             AND HSSUSUA.NNUMESTAT = HSSSTAT.NNUMESTAT 
           ORDER BY 2 ";
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  if ($sql->count() > 0) {
    while (!$sql->eof()) {
      $tpl->ID_STATUS = $sql->result("NNUMESTAT");
      $tpl->STATUS = $sql->result("CDESCSTAT");
      $tpl->block("ITEM_STATUS");
      $sql->next();
    }
    
    $tpl->block("FILTRO_STATUS");
  }
  
  if ($_SESSION['id_locacao'] <= 0) {
    $sql = new Query($bd);
    $txt = "SELECT HSSSETOR.NNUMESETOR,CNOMESETOR
              FROM HSSTITU,HSSEMPR,HSSSETOR
             WHERE HSSTITU.NNUMETITU = :contrato
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
               AND HSSEMPR.NNUMEEMPR = HSSSETOR.NNUMEEMPR
             ORDER BY 2 ";
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
    
    if ($sql->count() > 0) {
      while (!$sql->eof()) {
        $tpl->ID_LOCACAO = $sql->result("NNUMESETOR");
        $tpl->DESC_LOCACAO = $sql->result("CNOMESETOR");
        $tpl->block("ITEM_LOCACAO");
        $sql->next();
      }
      
      $tpl->block("FILTRO_LOCACAO");
    }

  }  
   
  $tpl->{"PERIODO_".$periodo} = "selected";
  $tpl->{"SITUACAO_".$situacao} = "selected";
  $tpl->{"FORMATO_".$formato} = "selected";
  $tpl->DATAINICIAL  = $datainicial;
  $tpl->DATAFINAL    = $datafinal;
  $tpl->LOCACAO      = $locacao;  
  $tpl->IDADEINICIAL = $idadeinicial;
  $tpl->IDADEFINAL   = $idadefinal;

  if (isset($_POST['imprimir']) or (isset($_POST['arquivo']))) {
  
    $erro = '';
    
    if (($datainicial == '') or ($datafinal == ''))
      $erro = "- Informe o per�odo da pesquisa.";
    else {
      if ($data->validaData($bd,$datainicial) == false)
        $erro .= '- Data inicial inv�lida.';

      if ($data->validaData($bd,$datafinal) == false)
        $erro .= '- Data inicial inv�lida.';
        
      if ($data->comparaData($bd,$datainicial,$datafinal,'>') == 1) 
        $erro .= "- A data inicial n�o pode ser superior a data final.";
    }	
    
    if ($erro <> '') {
      $tpl->MSG = $erro;
      $tpl->block("ERRO"); 
    }
    else {      
      $parametros = array();
      
      if (isset($_POST['imprimir']))  {
        $parametros[0]['nome']  = 'tipo';
        $parametros[0]['valor'] = 'I';  
      }
      else if (isset($_POST['arquivo'])) {
        $parametros[0]['nome']  = 'tipo';
        $parametros[0]['valor'] = 'A';  
      }
        
      $parametros[1]['nome']  = 'l';
      $parametros[1]['valor'] = $locacao;
      $parametros[2]['nome']  = 'f';
      $parametros[2]['valor'] = $formato;
      $parametros[3]['nome']  = 's';
      $parametros[3]['valor'] = $situacao;
      $parametros[4]['nome']  = 'p';  
      $parametros[4]['valor'] = $periodo;
      $parametros[5]['nome']  = 'di';  
      $parametros[5]['valor'] = $datainicial;
      $parametros[6]['nome']  = 'df';  
      $parametros[6]['valor'] = $datafinal;
      $parametros[7]['nome']  = 't';  
      $parametros[7]['valor'] = $status;
      $parametros[8]['nome']  = 'ii';  
      $parametros[8]['valor'] = $idadeinicial;
      $parametros[9]['nome']  = 'if';  
      $parametros[9]['valor'] = $idadefinal;	  
	  
      
      $tpl->RESULT = $util->redireciona2('relTotalizacaoImp.php?idSessao='.$_GET['idSessao'],$parametros,'S');
    }
  }
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     
  
?>