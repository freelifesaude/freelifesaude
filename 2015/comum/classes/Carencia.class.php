<?php  
  session_start();
  
  class Carencia {
  
    private $bd;
    
    public function __construct() {
      
      $this->bd = new Oracle();       
    }
    
    public function free() {      
      $this->bd->close();       
    }    
    
    public function listaCarencias($idBeneficiario) {
      $tpl = new Template("classes/Carencia.htm");
      
      $sql = new Query($this->bd);
		  $txt = "SELECT CGRAUUSUA,CNOMEUSUA,CNOMECARE,DVENCCRUS,CCODIUSUA,HSSCRUS.NNUMECARE,HSSCRUS.NNUMEUSUA
                FROM HSSUSUA,HSSCRUS,HSSCARE
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NNUMEUSUA = HSSCRUS.NNUMEUSUA
                 AND HSSCRUS.NNUMECARE = HSSCARE.NNUMECARE
               ORDER BY 1 DESC,2,4 DESC,3";
		  $sql->addParam(":usuario",$idBeneficiario);
		  $sql->executeQuery($txt);
          
		  if ($sql->count() > 0) { 
        while (!$sql->eof()) {

          $tpl->CARENCIA_NOME       = $sql->result("CNOMECARE");
          $tpl->CARENCIA_VENCIMENTO = $sql->result("DVENCCRUS");
          $tpl->CARENCIA_ID         = $sql->result("NNUMECARE");
          $tpl->ID_USUARIO          = $sql->result("NNUMEUSUA");
          
          $tpl->block("LINHA");        
          $sql->next();     
        }
        
        $tpl->block("CARENCIAS");  
		  }
      else {
			  $tpl->MSG = "** Benefici�rio sem car�ncia **";
			  $tpl->block("ERRO");
		  }         
      
      $dialog = new Dialog();
      $dialog->width  = 500;
      $dialog->height = 400;
      $dialog->title  = "Car�ncias";
      $dialog->text   = $tpl->parse();
      echo utf8_encode($dialog->show());
    } 

    public function listaProcCarencias($idBeneficiario,$idCarencia) {
      $tpl2 = new Template("classes/ProcCarencia.htm");
            
      $sql = new Query($this->bd);
      $txt = "SELECT CNOMEUSUA,DVENCCRUS 
                FROM HSSUSUA,HSSCRUS
               WHERE HSSUSUA.NNUMEUSUA = :idBeneficiario
                 AND HSSUSUA.NNUMEUSUA = HSSCRUS.NNUMEUSUA
                 AND NNUMECARE = :idCarencia "; 
      $sql->addParam(":idCarencia",$idCarencia);
      $sql->addParam(":idBeneficiario",$idBeneficiario);  
      $sql->executeQuery($txt);

      $tpl2->NOME       = $sql->result("CNOMEUSUA");
      $tpl2->VENCIMENTO = $sql->result("DVENCCRUS");

      $sql->clear();  
      $txt = "SELECT CNOMEPMED,HSSPMED.CCODIPMED 
                FROM HSSPCAR,HSSPMED 
               WHERE HSSPCAR.NNUMECARE = :idCarencia
                 AND ((SUBSTR(HSSPCAR.CCODIPMED,5,3) <> '000' AND SUBSTR(HSSPCAR.CCODIPMED,3,2) <> '00' AND HSSPCAR.CCODIPMED = HSSPMED.CCODIPMED) OR
                      (SUBSTR(HSSPCAR.CCODIPMED,3,5) = '00000' AND SUBSTR(GRUPOPROCEDIMENTO(HSSPCAR.CCODIPMED),1,2) = SUBSTR(HSSPMED.CCODIPMED,1,2)) OR
                      (SUBSTR(HSSPCAR.CCODIPMED,5,3) = '000' AND SUBSTR(HSSPCAR.CCODIPMED,3,2) <> '00' AND SUBSTR(GRUPOPROCEDIMENTO(HSSPCAR.CCODIPMED),1,4) = SUBSTR(HSSPMED.CCODIPMED,1,4)))
               ORDER BY 2,1"; 

      $sql->addParam(":idCarencia",$idCarencia);
      $sql->executeQuery($txt);
           
      if ($sql->count() > 0) { 
        while (!$sql->eof()) {
          if (substr($sql->result("CNOMEPMED"),4,3) <> '000') {    
            $tpl2->PROCEDIMENTO_NOME   = $sql->result("CNOMEPMED");
            $tpl2->PROCEDIMENTO_CODIGO = $sql->result("CCODIPMED");
            $tpl2->block("LINHA");        
          }
          $sql->next();     
        }
        $tpl2->block("PROCEDIMENTOS");                      
      } else {
        $tpl2->MSG = "** N�o existe procedimento para essa car�ncia **";
        $tpl2->block("ERRO");
      }        
      
      $dialog2 = new Dialog();
      $dialog2->width  = 550;
      $dialog2->height = 400;
      $dialog2->title  = "Procedimentos da car�ncia";
      $dialog2->text   = $tpl2->parse();
      echo utf8_encode($dialog2->show());
    }     
  }

?>