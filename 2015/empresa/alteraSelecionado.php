<?php
  ini_set  ("display_errors","off");      
  ini_set  ("error_reporting","E_ALL & ~E_NOTICE");
  
  require_once("../comum/autoload.php");    
  $seg->secureSessionStart();  
  require_once("../comum/sessao.php");
  
  $bd     = new Oracle();

  if (isset($_POST['idSelecionado']))
    $id = $_POST['idSelecionado'];
  else if (isset($_SESSION['idSelecionado']))
    $id = $_SESSION['id_contrato'];
  else
    $id = 0;
    
  if (isset($_POST['locacaoSelecionado']))
    $locacao = $_POST['locacaoSelecionado']; 
  else if (isset($_SESSION['id_locacao']))
    $locacao = $_SESSION['id_locacao'];        
  else
    $locacao = -1;  
           
  $sql = new Query();  
  $txt = "SELECT HSSOPTIT.NNUMETITU NNUMETITU,HSSTITU.CCODITITU, 
                 TO_CHAR(HSSTITU.DCONTTITU,'DD/MM/YYYY') DCONTTITU,CRAZAEMPR,CATWETITU,C_CGCEMPR,
                 
                 NVL(HSSOPTIT.NNUMESETOR,-1) NNUMESETOR,CNOMESETOR,CASO_TITU,CASE WHEN CSITUTITU = 'A' THEN 'A' WHEN DSITUTITU > SYSDATE THEN 'A' ELSE 'C' END CSITUTITU
            FROM HSSOPTIT,HSSTITU,HSSEMPR,HSSSETOR 
           WHERE HSSOPTIT.NNUMEUSUA = :id_operador
             AND HSSOPTIT.NNUMETITU = HSSTITU.NNUMETITU 
             AND (HSSTITU.CSITUTITU = 'A' OR CSITUTITU = 'C' AND DSITUTITU + :dias > SYSDATE)
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR             
             AND HSSOPTIT.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND NVL(CSITUSETOR,'A') = 'A'";
                 
  if ($id > 0 ) {
    $txt .= "   AND HSSOPTIT.NNUMETITU = :contrato ";
    $sql->addParam(":contrato",$id);
  }
  
  if ($locacao > 0 ) {
    $txt .= "   AND HSSOPTIT.NNUMESETOR = :locacao ";  
    $sql->addParam(":locacao",$locacao);    
  } else if ($locacao == -1) {
    $txt .= "   AND HSSOPTIT.NNUMESETOR IS NULL ";  
  } 
  
  $sql->addParam(":id_operador",$_SESSION['id_operador']);   
  $sql->addParam(":dias",$_SESSION['qtde_dias_acesso_cancelado']);     
  $sql->executeQuery($txt);
    
  $_SESSION['id_contrato'] = $sql->result("NNUMETITU"); 
  $_SESSION['codigo_contrato'] = $sql->result("CCODITITU");    
  $id = $_SESSION['id_contrato'];
  
  $_SESSION['id_locacao'] = $sql->result("NNUMESETOR");        
  $locacao = $_SESSION['id_locacao'];
  
  $_SESSION['nome_locacao']      = $sql->result("CNOMESETOR");         
  $_SESSION['codigo_contrato']   = $sql->result("CCODITITU");
  $_SESSION['data_contrato']     = $sql->result("DCONTTITU");
  $_SESSION['titular_contrato']  = $sql->result("CRAZAEMPR");
  $_SESSION['contrato_pcmso']    = $sql->result("CASO_TITU");
  $_SESSION['situacao_contrato'] = $sql->result("CSITUTITU");
  
  if ((($_SESSION['apelido_operadora'] == 'promedmg' or $_SESSION['apelido_operadora'] == 'vitallis')) and
      (($sql->result("CCUOPCONGE") == '2') or ($sql->result("NNUMETITU") == 30680879) or ($sql->result("NNUMETITU") == 30893837)))
    $_SESSION['astromig'] = "S";
  else
    $_SESSION['astromig'] = "N";

  if ($sql->result("NNUMESETOR") > 0)
    $nome_locacao = " / Locação: ".$sql->result("CNOMESETOR");
  else
    $nome_locacao = ''; 
      
  $_SESSION['logado']               = $formata->initcap($sql->result("CRAZAEMPR").$nome_locacao);     
  $_SESSION['atendimento_contrato'] = $sql->result("CATWETITU");
  $_SESSION['cnpj_contrato']        = $formata->formataCNPJ($sql->result("C_CGCEMPR"));  
         
  $bd->close();
  
  echo $id." ".$locacao;
?>