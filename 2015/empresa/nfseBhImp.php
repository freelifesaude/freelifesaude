<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  
  $bd      = new Oracle();
  $formata = new Formata();
  $func    = new Funcao();

  $pdf=new PDFSolus ('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','B',12);

  
  $nota = $_POST['nota'];
  
  $sql = new Query();  
  $txt = "SELECT SUBSTR(NNOTANOTAF,1,4) || '/' || SUBSTR(NNOTANOTAF,5,11) NOTA,
                 'N�o informado' EMAIL_OPERADORA,
                 SUBSTR(EMISSAO_IMPRESSA,1,10) COMPETENCIA1,NFSE.*                 
            FROM NFSE WHERE ID = :nota";
  $sql->addParam(":nota",$nota);
  $sql->executeQuery($txt);
  
  if (file_exists('../comum/img/'.strtolower($sql->result("CRAZACONGE")).'.jpg'))
    $pdf->Image('../comum/img/'.strtolower($sql->result("CRAZACONGE")).'.jpg',11,30,40,25);
  else
    $pdf->Image('../comum/img/logo_relatorio.jpg',11,30,40,25);

  $pdf->Cell(190,5,'NFS-e NOTA FISCAL DE SERVI�OS ELETR�NICA','LR',1,'C');
  $pdf->Cell(190,5,'','LR',1,'C');
  
  $pdf->SetFont('Arial','',8);  
  $pdf->Cell(40,3,'','L',0,'C');
  $pdf->Cell(60,3,'Emitida em:','L',0);
  $pdf->Cell(30,3,'Compet�ncia:','L',0);
  $pdf->Cell(60,3,'C�digo de verifica��o:','LR',1);

  $pdf->SetFont('Arial','B',11);
  
  if (strlen($sql->result("NNOTANOTAF")) == 15)  
    $pdf->Cell(40,5,'N� '.$sql->result("NOTA"),'LB',0);
  else
    $pdf->Cell(40,5,'N� '.$sql->result("NNOTANOTAF"),'LB',0);
    
  $pdf->SetFont('Arial','B',10);      
  $pdf->Cell(60,5,$sql->result("EMISSAO_IMPRESSA"),'LB',0);
  $pdf->Cell(30,5,$sql->result("COMPETENCIA1"),'LB',0);
  $pdf->Cell(60,5,$sql->result("CCDVENOTAF"),'LRB',1);
  
  $pdf->Cell(190,3,'','LR',1,'C');
  $pdf->Cell(40,5,'','L',0); 
  $pdf->Cell(150,5,$sql->result("CNOMEEMPR"),'R',1); 
  $pdf->Cell(40,5,'','L',0); 
  $pdf->Cell(90,5,'CPF/CNPJ: '.$formata->formataCNPJ($sql->result("CNPJ")),0,0);
  $pdf->Cell(60,5,'Inscri��o municipal: '.$sql->result("INSC_MUNICIPAL"),'R',1);
  
  $pdf->SetFont('Arial','',10);      
  $pdf->Cell(40,5,'','L',0); 
  $pdf->Cell(150,5,$sql->result("ENDERECO_OPERADORA"),'R',1);
  $pdf->Cell(40,5,'','L',0); 
  $pdf->Cell(90,5,$sql->result("CIDADE_OPERADORA"),0,0);
  $pdf->Cell(60,5,$sql->result("UF_OPERADORA"),'R',1);
  $pdf->Cell(40,5,'','L',0); 
  $pdf->Cell(90,5,'Telefone: '.$sql->result("TEL_OPERADORA"),0,0);
  $pdf->Cell(60,5,'Email: '.$sql->result("EMAIL_OPERADORA"),'R',1);
  $pdf->Cell(190,5,'','LRB',1,'C');
  
  $pdf->SetFont('Arial','B',10);      
  $pdf->Cell(190,7,'Tomador do(s) Servi�o(s)','LR',1);
  
  
  $pdf->Cell(5,5,'','L',0); 
  $pdf->Cell(125,5,'CPF/CNPJ: '.$formata->formataCNPJ($sql->result("CPF_CNPJ")),0,0);
  $pdf->Cell(60,5,'Inscri��o municipal: '.$sql->result("INSCRICAO_MUNICIPAL"),'R',1);
  
  $pdf->Cell(5,5,'','L',0); 
  $pdf->Cell(185,5,$sql->result("CLIENTE"),'R',1);

  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(5,5,'','L',0); 
  $pdf->Cell(185,5,$sql->result("LOGRADOURO").','.$sql->result("BAIRRO").' - Cep: '.$sql->result("CEP"),'R',1);
  $pdf->Cell(5,5,'','L',0);   
  $pdf->Cell(125,5,$sql->result("CIDADE"),0,0);
  $pdf->Cell(60,5,$sql->result("UF"),'R',1);
  $pdf->Cell(5,5,'','L',0);   
  
  $email = trim($func->emailNota($bd,$sql->result("ID")));
  $telefone = trim($func->telefoneNota($bd,$sql->result("ID")));

  if ($telefone == '')
    $telefone = 'N�o informado';

  if ($email == '')
    $email = 'N�o informado';
    
  $pdf->Cell(125,5,'Telefone: '.$telefone,0,0);
  $pdf->Cell(60,5,'Email: '.$email,'R',1);
  $pdf->Cell(190,3,'','LRB',1,'C');

  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(190,7,'Discrimina��o do(s) Servi�o(s)','LR',1);
  
  $pdf->SetFont('Arial','',10);        
  
  $sql_mens = new Query();
  $txt_mens = "SELECT SUM(NVENCPAGA) NVENCPAGA, SUM(NDESPPAGA) NDESPPAGA
                 FROM HSSCNOTA, HSSPAGA
                WHERE HSSCNOTA.NNUMENOTAF = :nota
                  AND HSSCNOTA.NNUMEPAGA = HSSPAGA.NNUMEPAGA";
                  
  $sql_mens->addParam(":nota",$sql->result("ID"));
  $sql_mens->executeQuery($txt_mens);

  if ((str_replace(',','.',$sql_mens->result("NVENCPAGA")) - str_replace(',','.',$sql_mens->result("NDESPPAGA"))) <> 0) {
    if ($_SESSION['apelido_operadora'] == 'saudemed')
      $observacao = 'Servi�os de Plano Odontol�gico - R$ ' . $formata->formataNumero((str_replace(',','.',$sql_mens->result("NVENCPAGA")) - str_replace(',','.',$sql_mens->result("NDESPPAGA")))). chr(13);
    else
      $observacao = $sql->result("DISCRIMINACAO"). 'Servi�os de Plano de Sa�de - R$ ' . $formata->formataNumero((str_replace(',','.',$sql_mens->result("NVENCPAGA")) - str_replace(',','.',$sql_mens->result("NDESPPAGA")))). chr(13);
  }

  if ($sql_mens->result("NDESPPAGA") <> 0)
    $observacao .= 'Despesas/Co-participa��o - R$ ' . $formata->formataNumero(str_replace(',','.',$sql_mens->result("NVENCPAGA")))  . chr(13);

  $pedaco = $pdf->Copy($observacao,180);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  $pedaco = $pdf->Copy($observacao,180);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  $pedaco = $pdf->Copy($observacao,180);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  $pedaco = $pdf->Copy($observacao,180);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  $pedaco = $pdf->Copy($observacao,180);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  $pdf->Cell(5,3,'','L',0);
  $pdf->Cell(185,3,$pedaco,'R',1);
  $observacao = str_replace($pedaco,'',$observacao);
  
  
  $pdf->Cell(190,3,'','LRB',1,'C');
  
  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(190,7,'CNAE/BH','LR',1);
  
  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(5,5,'','L',0);
  $pdf->Cell(185,5,'0422-0/01-88 / Planos de medicina de grupo ou individual e conv�nios para presta��o de assit�ncia m�dica,','R',1);
  $pdf->Cell(5,5,'','L',0);
  $pdf->Cell(185,5,'hospitalar, odontol�gica e cong�neres.','R',1);
  $pdf->Cell(190,5,'','LRB',1,'C');

  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(190,7,'Subitem Lista de servi�os LC 116/03 / Descri��o:','LR',1);
  
  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(5,5,'','L',0);
  $pdf->Cell(185,5,'4.22 / Planos de medicina de grupo ou individual e conv�nios para presta��o de assit�ncia m�dica, hospitalar,','R',1);
  $pdf->Cell(5,5,'','L',0);
  $pdf->Cell(185,5,'odontol�gica e cong�neres.','R',1);
  $pdf->Cell(190,5,'','LRB',1,'C');

  $pdf->SetFont('Arial','B',10);      
  $pdf->Cell(100,7,'Cod/Munic�pio da presta��o do(s) Servi�o(s):','L',0);
  $pdf->Cell(90,7,'Natureza da opera��o:','R',1);

  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(5,5,'','L',0);
  $pdf->Cell(95,5,$func->codigoIbge($bd,$sql->result("UF_OPERADORA"),$sql->result("CIDADE_OPERADORA")).' / '. $sql->result("CIDADE_OPERADORA"),0,0);
  $pdf->Cell(5,3,'',0,0);
  
  if ($sql->result("NATUREZA_OPERACAO") == '1')
    $operacao = 'Tributa��o no municipio';
  else if ($sql->result("NATUREZA_OPERACAO") == '2')
    $operacao = 'Isen��o';
  else if ($sql->result("NATUREZA_OPERACAO") == '3')
    $operacao = 'Imune';
  else if ($sql->result("NATUREZA_OPERACAO") == '4')
    $operacao = 'Exigibilidade suspensa por decis�o judicial';
  else if ($sql->result("NATUREZA_OPERACAO") == '5')
    $operacao = 'Exigibilidade suspensa por procedimento administrativo';
    
  $pdf->Cell(85,5,$operacao,'R',1);

  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(100,5,'Regime Especial de Tributa��o:','L',0,'R');
  
  $pdf->SetFont('Arial','',10); 

  if ($sql->result("REGIME") == '1')
    $regime = 'Microempresa Municipal';
  else if ($sql->result("REGIME") == '2')
    $regime = 'Estimativa';
  else if ($sql->result("REGIME") == '3')
    $regime = 'Sociedade de Profissionais';
  else if ($sql->result("REGIME") == '4')
    $regime = 'Cooperativa';
  else if ($sql->result("REGIME") == '5')
    $regime = 'MEI do simples nacional';
  else if ($sql->result("REGIME") == '6')
    $regime = 'ME ou EPP do simples nacional';
      
  $pdf->Cell(90,5,$regime,'R',1);
  
  $pdf->Cell(190,5,'','LRB',1,'C');
  $pdf->Cell(190,3,'','LR',1,'C');

  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(70,5,'Valor dos servi�os:','L',0);
  $pdf->Cell(20,5,$formata->formataNumero($sql->result("VALOR_BRUTO")),0,0,'R');
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(70,5,'Valor dos servi�os:',0,0);
  $pdf->Cell(20,5,$formata->formataNumero($sql->result("VALOR_BRUTO")),'R',1,'R');

  $pdf->Cell(1,1,'','L',0);
  $pdf->Cell(89,1,'','B',0);
  $pdf->Cell(10,1,'',0,0);
  $pdf->Cell(89,1,'','B',0);
  $pdf->Cell(1,1,'','R',1);
  
  $pdf->Cell(190,2,'','LR',1,'C');
  
  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(70,7,'(-) Descontos: ','L',0);
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("DESCONTO")),0,0,'R');
  $pdf->Cell(10,7,'',0,0);
  $pdf->Cell(70,7,'',0,0);
  $pdf->Cell(20,7,'0,00','R',1,'R');
  
  $pdf->Cell(70,7,'(-) Reten��es Federais: ','L',0);
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("IR")),0,0,'R');
  $pdf->Cell(10,7,'',0,0);
  $pdf->Cell(70,7,'(-) Desconto incondicionado:',0,0);
  $pdf->Cell(20,7,'0,00','R',1,'R');
  
  $pdf->Cell(70,7,'(-) ISS Retido na Fonte: ','L',0);
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("ISS")),0,0,'R');
  $pdf->Cell(10,7,'',0,0);
  
  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(70,7,'(=) Base de C�lculo:',0,0);
  
  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("VALOR_BRUTO")),'R',1,'R');

  $pdf->SetFont('Arial','B',10);        
  $pdf->Cell(70,7,'Valor L�quido:','L',0);
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("NVALONOTAF")),0,0,'R');
  $pdf->Cell(10,7,'',0,0);
  
  $pdf->SetFont('Arial','',10);        
  $pdf->Cell(70,7,'(x) Al�quota:',0,0);
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("PERC_ISS")).'%','R',1,'R');
  
  $pdf->Cell(70,7,'','L',0);
  $pdf->Cell(20,7,'',0,0);
  $pdf->Cell(10,7,'',0,0);
  
  $pdf->SetFont('Arial','B',10);          
  $pdf->Cell(70,7,'(=) Valor do ISS:',0,0);
  $pdf->Cell(20,7,$formata->formataNumero($sql->result("ISSQN")),'R',1,'R');

  $pdf->Cell(190,3,'','LRB',1,'C');
  $pdf->Cell(190,2,'','LR',1,'C');

  $pdf->Image('../comum/img/nfse/pbh.jpg',12,243,15,18);
  
  $pdf->SetFont('Arial','B',10);          
  $pdf->Cell(20,5,'','L',0);
  $pdf->Cell(170,5,'Prefeitura de Belo Horizonte - Secretaria Municipal de Finan�as','R',1);
  
  $pdf->SetFont('Arial','',10);
  $pdf->Cell(20,5,'','L',0);
  $pdf->Cell(170,5,'Rua Esp�rito Santo, 605 - 2� andar - Centro - CEP: 30160-919 - Belo Horizonte MG','R',1);
  $pdf->Cell(20,5,'','L',0);
  $pdf->Cell(170,5,'Tel.: 31.3277-4000 Fax: 31.3224-3099','R',1);
  $pdf->Cell(20,5,'','L',0);
  $pdf->Cell(170,5,'E-mail: nfse@pbh.gov.br','R',1);
  $pdf->Cell(190,3,'','LRB',1,'C');
    
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');    
  
  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 
?>
