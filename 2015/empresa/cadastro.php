<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  $operacao = $_GET['op'];
  
  if ($operacao == 'C')
    $_SESSION['titulo'] = "CANCELAMENTO DE BENEFICI�RIOS";
  else
    $_SESSION['titulo'] = "ALTERA��O DE BENEFICI�RIOS";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/cadastro.htm");

  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  $nome     = '';
  $situacao = '';
  $cpf      = '';
  
  if ($operacao <> 'C')
    $tpl->block("FILTRO_SITUACAO");
   
  if ($seg->permissaoOutros($bd,"WEBEMPRESAPESQUISAUSURIOPORCPF"))
    $tpl->block("FILTRO_CPF");
  
  if (isset($_POST['localizar'])) { 
    $nome     = strtoupper($seg->antiInjection($_POST['nome']));
    $nome2    = $nome."%";
    
    if (isset($_POST['situacao']))
      $situacao = $seg->antiInjection($_POST['situacao']);
    
    if (isset($_POST['cpf']))
      $cpf      = $seg->antiInjection($_POST['cpf']);
	
    $sql =  new Query($bd);
    $sql->clear();
    
    $txt = "SELECT * FROM (
            SELECT ROWNUM NUMERO, A.* FROM (
            SELECT DISTINCT USUA.CNOMEUSUA,USUA.CCHAPUSUA,USUA.CCODIUSUA,TO_CHAR(USUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,CCODIPLAN, 
                   USUA.CGRAUUSUA,USUA.NTITUUSUA,USUA.CTIPOUSUA,TITU.CNOMEUSUA TITULAR,USUA.NNUMEUSUA, 
                   USUA.CSITUUSUA,TO_CHAR(USUA.DSITUUSUA,'DD/MM/YYYY') DSITUUSUA,USUA.NNUMESTAT
              FROM HSSTITU,HSSUSUA,HSSUSUA USUA,HSSUSUA TITU,HSSPLAN 
             WHERE HSSUSUA.NNUMETITU = :contrato
               AND HSSUSUA.CNOMEUSUA LIKE :nome";

    if (($situacao == 'A') or ($operacao == 'C'))
      $txt .= "   AND USUA.CSITUUSUA = 'A'";
    else if ($situacao == 'C')
      $txt .= "   AND USUA.CSITUUSUA IN ('C','M') ";
      
    if ($_SESSION['id_locacao'] > 0) {
      $txt .= "   AND TITU.NNUMESETOR = :locacao";
      $sql->addParam(":locacao",$_SESSION['id_locacao']);      
    }
	
    if (($seg->permissaoOutros($bd,"WEBEMPRESAPESQUISAUSURIOPORCPF")) and ($cpf <> '')) {
      $txt .= " AND HSSUSUA.C_CPFUSUA = :cpf ";
      $sql->addParam(":cpf",$formata->somenteNumeros($cpf));	
    }
	//O cancelamento s� pode ser feito pelo ASO DEMISSIONAL
	if ($operacao == 'C')
	  $txt .= " AND NVL(CASO_TITU,'N') <> 'S' ";

    $txt .= "  AND USUA.NNUMETITU = HSSTITU.NNUMETITU
	             AND HSSUSUA.NTITUUSUA = USUA.NTITUUSUA 
               AND USUA.NTITUUSUA = TITU.NNUMEUSUA 
               AND USUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
             ORDER BY TITU.CNOMEUSUA,USUA.CGRAUUSUA DESC,USUA.CNOMEUSUA ) A)
             WHERE NUMERO <= 500";
                                 
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->addParam(":nome",$nome2); 
    $sql->executeQuery($txt);

    $titular = 0;
    while (!$sql->eof()) {
    
      if ($titular <> $sql->result("NTITUUSUA")) {
        $titular = $sql->result("NTITUUSUA");
      }
           
      $tpl->CADASTRO_CODIGO = $sql->result("CCODIUSUA");
      $tpl->CADASTRO_MATRICULA = $sql->result("CCHAPUSUA");
      
      if (($sql->result("CTIPOUSUA") == 'T') or ($sql->result("CTIPOUSUA") == 'F'))
        $tpl->CADASTRO_NOME = $sql->result("CNOMEUSUA");
      else
        $tpl->CADASTRO_NOME = '&nbsp;&nbsp;&nbsp;'.$sql->result("CNOMEUSUA");
      
      if ($operacao == 'C') {
        $sql_atcad = new Query();
        $txt_atcad = "SELECT NNUMEATCAD FROM HSSATCAD
                       WHERE NNUMETITU = :contrato
                         AND CFLAGATCAD IS NULL
                         AND NNUMEUSUA = :usuario";
        $sql_atcad->addParam(":contrato",$_SESSION['id_contrato']);
        $sql_atcad->addParam(":usuario",$sql->result("NNUMEUSUA"));
        $sql_atcad->executeQuery($txt_atcad);

        if ($sql_atcad->result("NNUMEATCAD") > 0)
          $tpl->CADASTRO_LINK = "pendente('pendentes.php',".$sql_atcad->result("NNUMEATCAD").",'2');";
        else         
          $tpl->CADASTRO_LINK = "abrir('cancelamento.php',".$sql->result("NNUMEUSUA").");";
      }    
      else {          
        if ($sql->result("CSITUUSUA") == "A") {
        
          $sql_atcad = new Query();
          $txt_atcad = "SELECT NNUMEATCAD FROM HSSATCAD
                         WHERE NNUMETITU = :contrato
                           AND CFLAGATCAD IS NULL
                           AND NNUMEUSUA = :usuario";
          $sql_atcad->addParam(":contrato",$_SESSION['id_contrato']);
          $sql_atcad->addParam(":usuario",$sql->result("NNUMEUSUA"));
          $sql_atcad->executeQuery($txt_atcad);
    
          if ($sql_atcad->result("NNUMEATCAD") > 0)
            $tpl->CADASTRO_LINK = "pendente('pendentes.php',".$sql_atcad->result("NNUMEATCAD").",'1');";
          else
            $tpl->CADASTRO_LINK = "abrir('alteracao.php',".$sql->result("NNUMEUSUA").");";
        }
        else
          $tpl->CADASTRO_LINK = "abrir('dados.php',".$sql->result("NNUMEUSUA").");";          
      }
          
      $tpl->CADASTRO_NASCIMENTO = $sql->result("DNASCUSUA");
      $tpl->CADASTRO_ADESAO = $sql->result("DINCLUSUA");
      $tpl->CADASTRO_CATEGORIA = $func->categoriaUsuario($sql->result("CTIPOUSUA"));

      if ($sql->result("CSITUUSUA") == "A")      
        $tpl->COR_SITUACAO = "navy";
      else
        $tpl->COR_SITUACAO = "red";
      
      $tpl->CADASTRO_SITUACAO = $func->retornaStatusUsuario($bd,$sql->result("NNUMEUSUA"));
      $tpl->block("LINHA");
      $sql->next();
    }
    
    $tpl->block("CADASTRO_TITULO");
    
  }           
  
  $tpl->BUSCA_NOME = $nome;
  
  if ($seg->permissaoOutros($bd,"WEBEMPRESAPESQUISAUSURIOPORCPF"))
    $tpl->BUSCA_CPF  = $cpf;
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     
  
?>