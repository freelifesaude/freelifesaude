<?php
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd   =  new Oracle();  
  $func = new Funcao();
  
  $nome   = $_POST['nome'];
  
  if (isset($_POST['inicio']))
    $inicio = $_POST['inicio'];
  else    
    $inicio = 0;
  
  if ($nome <> "") {
    $sql =  new Query($bd);
    $nome2 = $nome;    
    $nome = strtoupper($nome)."%";

    $txt = "SELECT * FROM (
            SELECT ROWNUM REGISTRO, NNUMEUSUA,CNOMEUSUA,CCODIUSUA,
                   DECODE(HSSUSUA.CTIPOUSUA,'T','Titular   ','F','Tit Financ','A','Agregado  ','Dependente') CTIPOUSUA
              FROM HSSUSUA 
             WHERE CNOMEUSUA LIKE :nome 
               AND CSITUUSUA = 'A'
               AND NNUMETITU = :contrato ";
               
    if ($_SESSION['sistema'] == "Usuario") {               
      if (($_SESSION['logando'] == 'PPJ') or ($_SESSION['logando'] == 'PFF')) {
        $txt .= "   AND NTITUUSUA = :titular ";
        $sql->addParam(":titular",$_SESSION['id_titular']);
      }
    }

    $txt .= ")
             WHERE REGISTRO BETWEEN :inicio+1 AND :inicio + 5";
        
    $sql->addParam(":nome",$nome);
    $sql->addParam(":inicio",$inicio);    
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
     
    $res = '<table class="table table-hover table-bordered table-condensed">
            <tr>
              <th>C&oacute;digo</td>                      
              <th>Benefici&aacute;rio</td>
              <th>Categoria</td>
              <th>&nbsp;</td>
            </tr>';
     
    $anterior = $inicio - 5;
    $proximo = $inicio + 5;
      
    if ($sql->count() > 0) {
      while (!$sql->eof()) {
        $status = $func->statusCarteirinha($bd,$sql->result("NNUMEUSUA"));
        
        if ($status == '')
          $status = "<a href='javascript:seleciona(".$sql->result("NNUMEUSUA").");'>adicionar</a>";
        
        $res .= "<tr>
                <td>".$sql->result("CCODIUSUA")."</td>
                <td>".$sql->result("CNOMEUSUA")."</td>
                <td>".$sql->result("CTIPOUSUA")."</td>
                <td>".$status."</td>
              </tr>";
                
        $sql->next();
      }
      
      $res .= "<tr> ";
      
      if ($anterior >= 0) 
        $res .= "  <td><a href='javascript:proximo(\"".$nome2."\",".$anterior.");'>Anterior</a></td>";
      else
        $res .= "  <td></td>";
      
      $res .= "  <td></td>
              <td></td> ";
              
      if ($proximo > 0) 
        $res .= "  <td><a href='javascript:proximo(\"".$nome2."\",".$proximo.");'>Pr&oacute;ximo</a></td>";
      else
        $res .= "  <td></td>";
              
      $res .= "</tr>";      
    } else {
      $res = "<tr>
              <td></td>
              <td><font color='red'>Benefici&aacute;rio n&atilde;o encontrado!<font></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>"; 

      $res .= "<tr> ";
      
      if ($anterior >= 0) 
        $res .= "  <td><a href='javascript:proximo(\"".$nome2."\",".$anterior.");'>Anterior</a></td>";
      else
        $res .= "  <td></td>";
      
      $res .= "  <td></td>
              <td></td> ";
      $res .= "  <td></td>";            
    }
    
    echo utf8_encode($res);
  }

  $bd->close();
?>