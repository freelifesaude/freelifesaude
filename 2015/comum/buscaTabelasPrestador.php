<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
  
  if (isset($_POST['local'])) {
  
    if ($_POST['local'] <> "") {
      $sql_tabelas = new Query($bd);
      $txt_tabelas = "SELECT DISTINCT NNUMETABMM,CCODITABMM FROM (
                      SELECT TABELA.NNUMETABMM,ESTTABMM.CCODITABMM
                        FROM HSSLINEA,(SELECT NNUMETABMM,NNUMEPRES,DVIGELINEA, 1 ORDEM FROM HSSLINEA WHERE NNUMETABMM > 0
                                        UNION ALL
                                       SELECT NSEGUTABMM,NNUMEPRES,DVIGELINEA, 2 ORDEM FROM HSSLINEA WHERE NSEGUTABMM > 0
                                        UNION ALL
                                       SELECT NTERCTABMM,NNUMEPRES,DVIGELINEA, 3 ORDEM FROM HSSLINEA WHERE NTERCTABMM > 0
                                        UNION ALL
                                       SELECT NQUARTABMM,NNUMEPRES,DVIGELINEA, 4 ORDEM FROM HSSLINEA WHERE NQUARTABMM > 0) TABELA,ESTTABMM
                       WHERE HSSLINEA.NNUMEPRES = :prestador                                   
                         AND HSSLINEA.NNUMEPRES = TABELA.NNUMEPRES
                         AND HSSLINEA.DVIGELINEA = TABELA.DVIGELINEA
                         AND HSSLINEA.DVIGELINEA = (SELECT MAX(LINEA.DVIGELINEA) FROM HSSLINEA LINEA
                                                     WHERE LINEA.NNUMEPRES = :prestador
                                                       AND LINEA.DVIGELINEA <= SYSDATE) 
                         AND TABELA.NNUMETABMM = ESTTABMM.NNUMETABMM
                       ORDER BY TABELA.ORDEM)";  
                         
      $sql_tabelas->addParam(":prestador",$_POST['local']);
      $sql_tabelas->executeQuery($txt_tabelas);
        
      $opcoes_tabela = "";
      $sql_tabelas->first();      
      while (!$sql_tabelas->eof()) {

        if (array_key_exists($numero,$tabmatmed) and $tabmatmed[$numero] == $sql_tabelas->result("CCODITABMM"))
          $opcoes_tabela .= "<option value=".$sql_tabelas->result("CCODITABMM")." selected>".$sql_tabelas->result("CCODITABMM")."</option>";
        else
          $opcoes_tabela .= "<option value=".$sql_tabelas->result("CCODITABMM").">".$sql_tabelas->result("CCODITABMM")."</option>";
          
        $sql_tabelas->next();  
      }

      echo $opcoes_tabela;      
    }
  } 
 
  $bd->close(); 
?>