<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");  
  $bd = new Oracle();
  $formata = new Formata();
  
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(230,5,$_SESSION['nome_operadora']." - ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(230,5,'AUTORIZA��O DEBITO EM CONTA',0,1);
      $this->SetFont('Arial','B',7);
      $this->Cell(40,5,'',0,1);
      $this->Cell(40,5,'',0,1);
      $this->Cell(40,5,'',0,1);
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);      

    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',12);

  $id_titular  = $_SESSION['id_titular'];
  $id_contrato = $_SESSION['id_contrato'];
        
  $txt = "SELECT CRAZAEMPR,C_CGCEMPR, CRESPTITU,CCODITITU
            FROM HSSTITU,HSSEMPR
           WHERE NNUMETITU = :id_contrato
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR";

  $sql = new Query($bd);                         
  $sql->addParam(":id_contrato",$id_contrato);   
  $sql->executeQuery($txt);
  
  $empresa         = $sql->result("CRAZAEMPR");
  $cnpj            = $formata->formataCNPJ($sql->result("C_CGCEMPR"));
  $responsavel     = "";
  $codigo_contrato = $sql->result("CCODITITU"); 
  $nome_titu       = "";  
  $cpf_usuario     = "";
  $cep_usuario     = "";
  $rua_usuario     = "";
  $nume_usuario    = "";
  $bairro_usuario  = "";
  $cidade_usuario  = "";
  $esta_usuario    = "";
  $tel1_usuario    = "";
  $email_usuario   = "";
  
  $pdf->Ln(2);
  $pdf->Cell(190,10,'AUTORIZA��O DEBITO EM CONTA',0,1,'C');
  
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(170,5,"Prezado cliente, ",0,1);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(170,5,"Disponibilizamos o d�bito em conta corrente para sua maior comodidade. ",0,1);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(170,5,"Para fazer a sua ades�o, preencha o formul�rio abaixo.",0,1);
  
  $pdf->Ln(10);

  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Empresa: ",0,0,'R');
  $pdf->Cell(135,5,$empresa,'B',1);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * CNPJ: ",0,0,'R');
  $pdf->Cell(135,5,$cnpj,'B',1);
  
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Contrato: ",0,0,'R');
  $pdf->Cell(135,5,$codigo_contrato,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Respons�vel Legal: ",0,0,'R');
  $pdf->Cell(135,5,$responsavel,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Endere�o: ",0,0,'R');
  $pdf->Cell(105,5,$rua_usuario,'B',0);
  $pdf->Cell(15,5," * N�: ",0,0,'R');
  $pdf->Cell(15,5,$nume_usuario,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Bairro: ",0,0,'R');
  $pdf->Cell(135,5,$bairro_usuario,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Cidade: ",0,0,'R');
  $pdf->Cell(70,5,$cidade_usuario,'B',0);
  $pdf->Cell(5,5,' ',0,0);
  $pdf->Cell(15,5," * Estado: ",0,0,'R');
  $pdf->Cell(10,5,$esta_usuario,'B',0);
  $pdf->Cell(5,5,' ',0,0);
  $pdf->Cell(10,5," * CEP: ",0,0,'R');
  $pdf->Cell(20,5,$cep_usuario,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Telefone: ",0,0,'R');
  $pdf->Cell(135,5,$tel1_usuario,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * E-mail: ",0,0,'R');
  $pdf->Cell(135,5,$email_usuario,'B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Banco: ",0,0,'R');
  $pdf->Cell(135,5,'','B',1);
  $pdf->Ln(2);
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(35,5," * Ag�ncia: ",0,0,'R');
  $pdf->Cell(15,5,'','B',0);
  $pdf->Cell(20,5," * D�gito: ",0,0,'R');
  $pdf->Cell(10,5,' ','B',0);
  $pdf->Cell(40,5," * Conta Corrente: ",0,0,'R');
  $pdf->Cell(20,5,'','B',0);
  $pdf->Cell(20,5," * D�gito: ",0,0,'R');
  $pdf->Cell(10,5,' ','B',1);
  
  $pdf->Ln(10);
  
  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(10,5,'(   )',0,0);
  $pdf->MultiCell(140,5,"Sim, autorizo o d�bito autom�tico dos boletos mensais do Plano de Sa�de".
                        " Unimed Franca em minha conta corrente. Declaro estar ciente de que o banco ficar� isento de ".
                        "qualquer responsabilidade caso n�o haja saldo suficiente no dia do vencimento.");

  $pdf->Cell(10,5,'',0,0);
  $pdf->Cell(10,5,'( * )',0,0);
  $pdf->MultiCell(140,5,"� obrigat�rio o preenchimento de todos os campos do formul�rio.");
                        
  $pdf->Ln(10);

  $pdf->Cell(190,5,"Franca, ____ de __________ de __/__/____",0,1,'C');
  $pdf->Ln(10);
  
  $pdf->Cell(190,5,"__________________________________",0,1,'C');
  $pdf->Cell(190,7,"Contratante",0,1,'C');
  
  $pdf->Ln(5);

  $pdf->SetFont('Arial','',10);  
  $pdf->Cell(10,5,'',0,0);  
  $pdf->MultiCell(170,5,"Obs: Favor imprimir e encaminhar para a Asseroria Financeira da Unimed Franca situada na Rua General Carneiro, 1595 - Centro Franca/SP.");

  
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');

  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";

?>
