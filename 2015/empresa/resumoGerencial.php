<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd =  new Oracle();    

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "RESUMO GERENCIAL";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","resumoGerencial.htm");
  
  if (isset($_POST['datainicial']))
    $dt_inicio = htmlentities($_POST['datainicial']);  
  else
    $dt_inicio = '';     
  
  if (isset($_POST['datafinal']))
    $dt_fim = htmlentities($_POST['datafinal']);  
  else
    $dt_fim = '';  
  
  if (isset($_POST['datainicial']))
    $dt_inicio2 = htmlentities($_POST['datainicial']);  
  else
    $dt_inicio2 = '';  
  
  if (isset($_POST['datafinal']))
    $dt_fim2 = htmlentities($_POST['datafinal']);  
  else
    $dt_fim2 = '';  
  
  
  if (isset($_POST['imprimir']) ) {
    $tpl->DATA_INICIAL = $dt_inicio;
    $tpl->DATA_FINAL   = $dt_fim;
  
    $dt_inicio = '01/' . $dt_inicio;
    $dt_fim = '01/' . $dt_fim;
  
    $comp1 = substr($dt_inicio,6,4) . substr($dt_inicio,3,2);
    $comp2 = substr($dt_fim,6,4) . substr($dt_fim,3,2);
    
    while($comp1 <= $comp2){
      $competencia .= "'".substr($comp1,4,2) . "/" . substr($comp1,0,4) . "',";
      $comp1 = substr($comp1,0,4) . $formata->acrescentaZeros((substr($comp1,4,2) + 1),2);
      
      if( substr($comp1,4,2) == '13')
        $comp1 = (substr($comp1,0,4) + 1) . '01';
    }
    
    $competencia = substr($competencia,0,strlen($competencia)-1);
  
    $Q_Faturamento = new Query($bd);
    $txt_Faturamento = " SELECT SUM(VALOR) TOTAL_EMITIDO,
                                SUM(DECODE(NNUMEEMPR,NULL,VALOR,0)) PF,
                                SUM(DECODE(NNUMEEMPR,NULL,0,VALOR)) PJ
                           FROM COMPOSICAO_MENSALIDADE
                          WHERE CCOMPPAGA IN (" . $competencia . ")  
                            AND NNUMETITU = :contrato 
                            AND TIPO_RECEITA = '1' ";

    $Q_Faturamento->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Faturamento->executeQuery($txt_Faturamento);
  
    $Q_Total_Usuarios = new Query($bd);
    
    $txt_Total_Usuarios = " SELECT SUM(DECODE(NNUMEEMPR,NULL,TOTAL_USUARIOS_BOLETO(HSSPAGA.NNUMEPAGA),0)) QTDE_PF,
                                   ROUND(SUM(DECODE(NNUMEEMPR,NULL,TOTAL_USUARIOS_BOLETO(HSSPAGA.NNUMEPAGA),0))/COUNT(*),0) QTDE_PF_MEDIA,
                                   SUM(DECODE(NNUMEEMPR,NULL,0,1)) QTDE_PJ,
                                   ROUND(SUM(DECODE(NNUMEEMPR,NULL,0,TOTAL_USUARIOS_BOLETO(HSSPAGA.NNUMEPAGA)))/COUNT(*),0) QTDE_PJ_MEDIA
                              FROM HSSPAGA,HSSTITU,HSSPLAN,HSSUSUPG,HSSUSUA
                             WHERE CCOMPPAGA = '" . $dt_fim2 . "'
                               AND HSSTITU.NNUMETITU = :contrato
                               AND HSSPAGA.NNUMEESTAB = HSSTITU.NNUMEESTAB
                               AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
                               AND HSSUSUPG.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                               AND HSSPAGA.NNUMETITU  = HSSTITU.NNUMETITU
                               AND NVL(HSSUSUPG.NNUMEPLAN,HSSUSUA.NNUMEPLAN) = HSSPLAN.NNUMEPLAN ";
    $Q_Total_Usuarios->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Total_Usuarios->executeQuery($txt_Total_Usuarios);
  
    $Q_Eventos = new Query($bd);
    $txt_Eventos = " SELECT SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) +
                            TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT)) TOTAL,
                            SUM(DECODE(HSSCONT.NNUMEUSUA,NULL,0,DECODE(NNUMEEMPR,NULL,TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) +
                                                                       TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT),0))) PF,
                            SUM(DECODE(HSSCONT.NNUMEUSUA,NULL,0,DECODE(NNUMEEMPR,NULL,0,TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) +
                                                                       TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT)))) PJ,
                            SUM(DECODE(HSSCONT.NNUMEUSUA,NULL,TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) +
                                       TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT),0)) CORTESIA
                       FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                      WHERE CCOMPLOTE IN (" . $competencia . ")
                        AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                        AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                        AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                        AND HSSTITU.NNUMETITU = :contrato 
                        AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1') " ;
    $Q_Eventos->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Eventos->executeQuery($txt_Eventos); 
  
    $Q_Reembolso = new Query($bd);
    $txt_Reembolso = " SELECT SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) +
                              TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT)) TOTAL
                         FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                        WHERE CCOMPLOTE IN (" . $competencia . ")
                          AND CGPAGLOTE = 'R'
                          AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                          AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                          AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+) 
                          AND HSSTITU.NNUMETITU = :contrato 
                          AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1') ";
    $Q_Reembolso->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Reembolso->executeQuery($txt_Reembolso);
  
    $Q_EventosDetalhado = new Query($bd);
    $txt_EventosDetalhado = "-- Internacoes (procedimentos)
                             SELECT 'IP' CTIPOPMED,CURGECONT,CAMBUCONT,
                                    SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT = 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CURGECONT,CAMBUCONT
                              UNION ALL
                             -- Internacoes (mat/med)
                             SELECT 'IF' CTIPOPMED,CURGECONT,CAMBUCONT,
                                    SUM(TOTAL_FARMACIA(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT = 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CURGECONT,CAMBUCONT
                              UNION ALL
                             -- Internacoes (taxas)
                             SELECT 'IT' CTIPOPMED,CURGECONT,CAMBUCONT,
                                    SUM(TOTAL_TAXAS(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT = 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CURGECONT,CAMBUCONT
                              UNION ALL
                             -- Internacoes (opme)
                             SELECT 'IO' CTIPOPMED,CURGECONT,CAMBUCONT,
                                    SUM(TOTAL_MATERIAIS_FORNECIDOS(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT = 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CURGECONT,CAMBUCONT
                              UNION ALL
                             -- Consultas e exames
                             SELECT DECODE(CTIPOPMED,'C','C','E') CTIPOPMED,TO_CHAR(NULL),CAMBUCONT,
                                    SUM(NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) +
                                    NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSPCON,HSSPMED,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT <> 'I'
                                AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                                AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                                AND CTIPOPMED IN ('C','N','H','R','O','K','V','X','Y','*','Z','&','$','U','B','%','@','#','E','J','�')
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY DECODE(CTIPOPMED,'C','C','E'),CAMBUCONT
                              UNION ALL
                             -- Demais despesas
                             SELECT 'D',TO_CHAR(NULL),CAMBUCONT,
                                    SUM(NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) +
                                    NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSPCON,HSSPMED,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT <> 'I'
                                AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                                AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                                AND CTIPOPMED IN ('M','L','Q','G','P','S','F','T','[','A','D','1','2','3','4','5','6','7','8','9','I')
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CAMBUCONT
                              UNION ALL
                             -- Materiais e medicamentos
                             SELECT 'M',TO_CHAR(NULL),CAMBUCONT,
                                    SUM(TOTAL_FARMACIA(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT <> 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CAMBUCONT
                              UNION ALL
                             -- Taxas
                             SELECT 'T',TO_CHAR(NULL),CAMBUCONT,
                                    SUM(TOTAL_TAXAS(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT <> 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CAMBUCONT
                              UNION ALL
                             -- Opme
                             SELECT 'OPME',TO_CHAR(NULL),CAMBUCONT,
                                    SUM(TOTAL_MATERIAIS_FORNECIDOS(HSSCONT.NNUMECONT)) TOTAL
                               FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                              WHERE CCOMPLOTE IN (" . $competencia . ")
                                AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                AND HSSCONT.CAMBUCONT <> 'I'
                                AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                AND HSSTITU.NNUMETITU = :contrato
                                AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                              GROUP BY CAMBUCONT "; 
    $Q_EventosDetalhado->addParam(":contrato",$_SESSION['id_contrato']);                
    $Q_EventosDetalhado->executeQuery($txt_EventosDetalhado);
  
    $Q_Consultas = new Query($bd);
    $txt_Consultas = "SELECT CNOMEESPEC, SUM(NQUANPCON) QTDE,
                             SUM(NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) +
                             NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0) ) TOTAL
                        FROM HSSLOTE,HSSCONT,HSSPCON,HSSPMED,HSSUSUA,HSSTITU,HSSESPEC
                       WHERE CCOMPLOTE IN (" . $competencia . ")
                         AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                         AND CAMBUCONT = 'A'
                         AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                         AND NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + 
                             NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0) > 0
                         AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                         AND CTIPOPMED = 'C'
                         AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                         AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                         AND NVL(HSSCONT.NNUMEESPEC,ESPECIALIDADE_PADRAO(HSSPCON.NNUMEPRES)) = HSSESPEC.NNUMEESPEC
                         AND HSSTITU.NNUMETITU = :contrato
                         AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                       GROUP BY CNOMEESPEC
                       ORDER BY TOTAL DESC, CNOMEESPEC  ";
    $Q_Consultas->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Consultas->executeQuery($txt_Consultas);           

    $Q_Exames = new Query($bd);
    $txt_Exames = " SELECT CTIPOPMED,SUM(NQUANPCON) QTDE,SUM( NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) +
                           NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0)) TOTAL
                      FROM HSSLOTE,HSSCONT,HSSPCON,HSSPMED,HSSUSUA,HSSTITU
                     WHERE CCOMPLOTE IN (" . $competencia . ")
                       AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                       AND CAMBUCONT = 'A'
                       AND NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + 
                           NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0) > 0
                       AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                       AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                       AND CTIPOPMED IN ('N','H','R','O','K','V','X','Y','*','Z','&','$','U','B','%','@','#','E','J','�')
                       AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                       AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                       AND HSSTITU.NNUMETITU = :contrato
                       AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                     GROUP BY CTIPOPMED
                     ORDER BY TOTAL DESC, CTIPOPMED ";
    $Q_Exames->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Exames->executeQuery($txt_Exames);          

    $Q_Internacoes = new Query($bd);
    $txt_Internacoes = " SELECT CNOMEPRES,FINPRES.NNUMEPRES,
                                SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) + TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT)) TOTAL,
                                SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT,'H')) HONORARIOS,
                                SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT,'E')) EXAMES,
                                SUM(DECODE(NCOMPCONT,NULL,1,0)) QTDE,
                                SUM(TOTAL_FARMACIA(HSSCONT.NNUMECONT)) MATMED,
                                SUM(TOTAL_TAXAS(HSSCONT.NNUMECONT) + TOTAL_MATERIAIS_FORNECIDOS(HSSCONT.NNUMECONT)) TAXAS,
                                SUM(TOTAL_DIARIAS(HSSCONT.NNUMECONT,'C')) UTI,
                                SUM(TOTAL_DIARIAS(HSSCONT.NNUMECONT,'D')) DIARIAS
                           FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU,FINPRES
                          WHERE CCOMPLOTE IN (" . $competencia . ")
                            AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                            AND CAMBUCONT = 'I'
                            AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                            AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                            AND NVL(HSSCONT.NLOCAPRES,HSSCONT.NNUMEPRES) = FINPRES.NNUMEPRES
                            AND HSSTITU.NNUMETITU = :contrato
                            AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                          GROUP BY CNOMEPRES,FINPRES.NNUMEPRES
                          ORDER BY TOTAL DESC, CNOMEPRES "; 
    $Q_Internacoes->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Internacoes->executeQuery($txt_Internacoes);
  
    $Q_Usuarios = new Query($bd);
    $txt_Usuarios = " SELECT CNOMEUSUA,
                             SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT) + TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT)) TOTAL,
                             SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT,'H')) HONORARIOS,
                             SUM(TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT,'E')) EXAMES,
                             SUM(DECODE(NCOMPCONT,NULL,1,0)) QTDE,
                             SUM(TOTAL_FARMACIA(HSSCONT.NNUMECONT)) MATMED,
                             SUM(TOTAL_TAXAS(HSSCONT.NNUMECONT) + TOTAL_MATERIAIS_FORNECIDOS(HSSCONT.NNUMECONT)) TAXAS,
                             SUM(TOTAL_DIARIAS(HSSCONT.NNUMECONT,'C')) UTI,
                             SUM(TOTAL_DIARIAS(HSSCONT.NNUMECONT,'D')) DIARIAS
                        FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU
                       WHERE CCOMPLOTE IN (" . $competencia . ")
                         AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                         AND CAMBUCONT = 'I'
                         AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                         AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                         AND HSSTITU.NNUMETITU = :contrato
                         AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                       GROUP BY CNOMEUSUA
                       ORDER BY TOTAL DESC, CNOMEUSUA ";  
    $Q_Usuarios->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Usuarios->executeQuery($txt_Usuarios);  
    
    $Q_Especialidades = new Query($bd);
    $txt_Especialidades = " SELECT CNOMEESPEC,FINPRES.NNUMEPRES,CNOMEPRES,SUM(NQUANPCON) QTDE,SUM(NVL(NCIRUHONO,0) + NVL(NANESHONO,0) +
                                   NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + NVL(NQUARHONO,0) +
                                   NVL(NINSTHONO,0) + NVL(NSALAPCON,0)+ NVL(NCUOPPCON,0) + NVL(NVFILPCON,0)) TOTAL
                              FROM HSSLOTE,HSSCONT,HSSPCON,HSSPMED,HSSESPEC,HSSUSUA,HSSTITU,FINPRES
                             WHERE CCOMPLOTE IN (" . $competencia . ")
                               AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                               AND CAMBUCONT = 'A'
                               AND NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + 
                                   NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0) > 0
                               AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                               AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                               AND CTIPOPMED = 'C'
                               AND NVL(HSSCONT.NNUMEESPEC,ESPECIALIDADE_PADRAO(HSSPCON.NNUMEPRES)) = HSSESPEC.NNUMEESPEC
                               AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                               AND HSSPCON.NNUMEPRES = FINPRES.NNUMEPRES
                               AND HSSTITU.NNUMETITU = :contrato
                               AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                             GROUP BY CNOMEESPEC,FINPRES.NNUMEPRES,CNOMEPRES
                             ORDER BY 1,2 ";
    $Q_Especialidades->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Especialidades->executeQuery($txt_Especialidades);                                      

    $Q_Especialidades_Exames = new Query($bd);
    $txt_Especialidades_Exames = " SELECT CNOMEESPEC,FINPRES.NNUMEPRES,CNOMEPRES,SUM(NQUANPCON) QTDE,SUM(NVL(NCIRUHONO,0) + NVL(NANESHONO,0) +
                                          NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + NVL(NQUARHONO,0) + 
                                          NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0)) TOTAL
                                     FROM HSSLOTE,HSSCONT,HSSPCON,HSSPMED,HSSESPEC,HSSUSUA,HSSTITU,FINPRES
                                    WHERE CCOMPLOTE IN (" . $competencia . ")
                                      AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
                                      AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                                      AND CAMBUCONT = 'A'
                                      AND NVL(NCIRUHONO,0) + NVL(NANESHONO,0) + NVL(NPRIMHONO,0) + NVL(NSEGUHONO,0) + NVL(NTERCHONO,0) + 
                                          NVL(NQUARHONO,0) + NVL(NINSTHONO,0) + NVL(NSALAPCON,0) + NVL(NCUOPPCON,0) + NVL(NVFILPCON,0) > 0
                                      AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                                      AND CTIPOPMED IN ('N','H','R','O','K','V','X','Y','*','Z','&','$','U','B','%','@','#','E','J','�')
                                      AND NVL(HSSCONT.NNUMEESPEC,ESPECIALIDADE_PADRAO(HSSPCON.NNUMEPRES)) = HSSESPEC.NNUMEESPEC
                                      AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                                      AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU(+)
                                      AND HSSPCON.NNUMEPRES = FINPRES.NNUMEPRES
                                      AND HSSTITU.NNUMETITU = :contrato
                                      AND (HSSCONT.CDESPCONT  IS NULL OR HSSCONT.CDESPCONT = '1')
                                    GROUP BY CNOMEESPEC,FINPRES.NNUMEPRES,CNOMEPRES
                                    ORDER BY 1,2 ";
    $Q_Especialidades_Exames->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Especialidades_Exames->executeQuery($txt_Especialidades_Exames);  
 
    require_once("resumoGerencialImp.php");
  }
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     

?>