<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "RELACAO DE AUTORIZA��ES";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","autorizacoes.htm");

  $tpl->ID_SESSAO = $_GET['idSessao'];
  $tpl->MASCARA = $_SESSION['mascara'];   
  
  if (isset($_POST['enviar']) or
      isset($_POST['imprimir']) or
      isset($_POST['arquivo'])) {  
      
    $data_inicial = $_POST['data_inicial'];       
    $data_final   = $_POST['data_final'];
    $senha        = $_POST['senha'];
    $guia         = $_POST['guia'];  
    $codigo       = $_POST['codigo'];
    
    if (isset($_POST['enviar'])) 
      $inicio = 0;  
     else 
      $inicio = $_GET['ini'];   
  }
  else {
    
    if (isset($_GET['di']))
      $data_inicial = $_GET['di'];  
    else
      $data_inicial = '';  
    
    if (isset($_GET['df']))
      $data_final = $_GET['df'];  
    else
      $data_final = '';      
    
    if (isset($_GET['s']))
      $senha = $_GET['s'];  
    else
      $senha = '';    
    
    if (isset($_GET['g']))
      $guia = $_GET['g'];  
    else
      $guia = '';    
    
    if (isset($_GET['c']))
      $codigo = $_GET['c'];  
    else
      $codigo = '';  
    
    if (isset($_GET['ini']))
      $inicio = $_GET['ini'];  
    else
      $inicio = '';      
  }
    
  $tpl->DATA_INICIAL = $data_inicial;
  $tpl->DATA_FINAL   = $data_final;
  $tpl->SENHA        = $senha;
  $tpl->CODIGO       = $codigo;
  $tpl->NUMERO_GUIA2 = $guia;
  
  if ($inicio == '')
    $inicio = 0;
      
  if($_SESSION['apelido_operadora'] <> "sempreVida")
    $tpl->NUMERO_GUIA = $guia;

  if (isset($_POST['imprimir']) or isset($_POST['arquivo'])) 
    $inicio = -1;
 
  $sql = new Query($bd);  
  require_once('concedidasSQL.php');
  
  if (isset($_GET['ex']))
    $ex = $_GET['ex'];  
  else
    $ex = '';      

  if (isset($_POST['enviar']) or ($ex == '1')) {
    $sql->executeQuery($txt_guias);
    
    if ($sql->count() > 0) {
      if ($_SESSION['mostra_valor_procedimentos'] == "S")
        $tpl->block("TITULO_VALOR");
        

      $quantidade = 0;
      $total = 0;
      while (!$sql->eof()) {                   
        $tpl->GUIA = $sql->result("NNUMEGUIA");
        $tpl->DATA = $sql->result("DEMISGUIA");

        if ($_SESSION['apelido_operadora'] == 'vitallis')
          $tpl->CODIGO_BENEFICIARIO = $formata->acrescentaZeros($sql->result("CCODIUSUA"),9);
        else
          $tpl->CODIGO_BENEFICIARIO = $sql->result("CCODIUSUA");
      
        $tpl->BENEFICIARIO = $sql->result("CNOMEUSUA");
        $tpl->PRESTADOR = $sql->result("PRESTADOR");
        
        if ($_SESSION['mostra_valor_procedimentos'] == "S") {
          $tpl->VALOR = $formata->formataNumero($sql->result("VALOR"));              
          $tpl->block("MOSTRA_VALOR");
        }
        
        $tpl->TIPO_GUIA = $sql->result("TIPO");
        
        /*
          Liberada 
            - Imprime se estiver dentro da validade
            - Edita sempre
            - Excluir se tiver permiss�o e foi emitida pela web
          Negada ou Aguardando confima��o
            - N�o imprime
            - N�o edita
            - N�o exclui
          Sob auditoria
            - N�o imprime        
            - Edita sempre
            - Exclui se a mesma n�o foi editada e se tiver permiss�o e se foi emitida pela web
        */
        if ( (($sql->result("CSTATGUIA") == "") and ($sql->result("NPTU_GUIA") == "")) or
		       ( ($sql->result("CSTATGUIA") == "") and ($sql->result("NPTU_GUIA") <> "") and 
			     ( ($sql->result("NPTU_GUIA") == '6') or ($sql->result("NPTU_GUIA") == '102') or ($sql->result("NPTU_GUIA") == '9') or
				   ($sql->result("NPTU_GUIA") == '55') or ($sql->result("NPTU_GUIA") == '155') or ($sql->result("NPTU_GUIA") == '1044') or
  			       ($sql->result("NPTU_GUIA") == '103') or ($sql->result("NPTU_GUIA") == '12') or ($sql->result("NPTU_GUIA") == '104')  ) ) ) {
			  
          $datavalidade = explode("/",$sql->result("DVALIGUIA"));   
          $datavalidade = $datavalidade[2].$datavalidade[1].$datavalidade[0];

          $tpl->block("BOTAO_IMPRIMIR");  

        }        
                
        $tpl->block("LINHA");

        $quantidade++;
        $total += str_replace(',','.',$sql->result("VALOR"));
              
        $sql->next();
      }
      
      $tpl->QTDE = $quantidade;      
      
      if (($inicio - 100) >= 0) {
        $tpl->ANTERIOR = $inicio - 100;  
        $tpl->block("ANTERIORES");
      }
      
      if ((($inicio + 100) > 0) and ($quantidade >= 100)) {
        $tpl->PROXIMO = $inicio + 100;  
        $tpl->block("PROXIMOS");
      }       
      
      if ($_SESSION['mostra_valor_procedimentos'] == "S") {    
        $tpl->DESC_TOTAL = "Valor total:";
        $tpl->TOTAL = $formata->formataNumero($total);
      }      
           
      $tpl->block("RESULTADO");        
      
    } else {
      $tpl->CLASSE = "";
        
      if ($data_inicial == '' and $data_final == '' and $guia == '')      
        $tpl->MSG = "Informe o per�odo ou o n�mero da autoriza��o.";             
      else if ($data_inicial <> '' and $data_final <> '' and $guia == '') {
      
        if (($data->validaData($bd,$data_inicial) == false) or ($data->validaData($bd,$data_final) == false))
          $tpl->MSG = "Data inv�lida informada no per�odo.";
        else if ($data->comparaData($bd,$data_inicial,$data_final,'>') == 1)
          $tpl->MSG = "A data inicial n�o pode ser superior a data final.";
        else
          $tpl->MSG = "N�o existe nenhuma autoriza��o para o per�odo informado.";
          
        $tpl->CLASSE = "alert-error";
                
      } else if ($guia <> '') {
        $sql->clear();
        $txt = "SELECT NNUMEGUIA,CSTATGUIA FROM HSSGUIA WHERE NNUMEGUIA = :guia";
        $sql->addParam(":guia",$guia);
        $sql->executeQuery($txt);
        
        if ($sql->count() == 0) 
          $tpl->MSG = "A guia informada n�o existe.";
        else if ($sql->result("CSTATGUIA") == 'C') 
          $tpl->MSG = "A guia informada est� aguardando confirma��o.";
        else if ($sql->result("CSTATGUIA") == 'A') 
          $tpl->MSG = "A guia informada est� sob auditoria.";
        else if ($sql->result("CSTATGUIA") == 'P') 
          $tpl->MSG = "A guia informada est� aguardando autoriza��o.";
        else
          $tpl->MSG = "A guia informada pertencente a outro prestador.";
      }
      
      $tpl->block("ERRO");    
    }          
  }
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     

?>