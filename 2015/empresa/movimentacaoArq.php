<?php
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();

  $_SESSION['titulo'] = "MOVIMENTA��O DE USU�RIOS VIA ARQUIVO";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","movimentacaoArq.htm");
  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  $path = getcwd();

  if (isset($_POST['enviar'])) {
    $size         = 790;
    $arquivo      = $_FILES['upload1'];
    $nome_arquivo = $arquivo['name'];
    
    $retorno = $util->validaExtensaoArquivo($nome_arquivo,array('rem'));
    
    if ($retorno == '') {
      if (isset($arquivo)) {

        $dir = "upload";

        $dir = $util->criaDiretorio($dir);
        $dir = $util->criaDiretorio($_SESSION['codigo_contrato']);

        if ($_SESSION['id_locacao'] > 0) {
          $sql_setor = new Query($bd);
          $txt = "SELECT CNOMESETOR FROM HSSSETOR
                    WHERE NNUMESETOR = :Setor
                    GROUP BY CNOMESETOR";
                            
          $sql_setor->addParam(":Setor",$_SESSION['id_locacao']); 
          $sql_setor->executeQuery($txt);
  
          $dir = $util->criaDiretorio($sql_setor->result("CNOMESETOR"));
        }

        move_uploaded_file($arquivo['tmp_name'],$dir."/".$nome_arquivo);

        if ((file_exists($dir."/".$nome_arquivo)) and ($nome_arquivo <> '')) {
          $f = fopen($dir."/".$nome_arquivo,'r');
          $linha = 0;
          $erros = 0;

          $sql_id = new Query($bd);
          $txt_id = "SELECT SEQ_LIMPU.NEXTVAL ID_LOTE FROM DUAL";
          $sql_id->executeQuery($txt_id);

          $idLote = $sql_id->result("ID_LOTE");

          while (($data = fgetcsv($f, 3000, ";")) !== FALSE) {
          
            $linha++;
             
            if ((($data[0] == 'C') and (sizeOf($data) <> 38)) or
                (($data[0] <> 'C') and (sizeOf($data) <> 36 and sizeOf($data) <> 38))) {
            
              $sql_erro = new Query($bd);
              $txt_erro = " INSERT INTO TMP_ERRO_IMPORTACAO (ID_LOTE,ERRO,LINHA,VALOR,NOME) VALUES (:idLote,:erro1,:linha1,:valor1,:nome1)";
              $sql_erro->addParam(":idLote",$idLote);
              $sql_erro->addParam(":erro1","Linha inv�lida. Verifique a quantidade de colunas do layout do arquivo, para operacoes de cancelamento o arquivo deve conter 38 colunas e para as demais 36 colunas.");
              $sql_erro->addParam(":linha1",$linha);
              $sql_erro->addParam(":valor1","");
              $sql_erro->addParam(":nome1",$data[3]);
              $sql_erro->executeSQL($txt_erro);            
              $erros++;           
            }
            else {            
              $sql = new Query($bd);
              $txt = "INSERT INTO TMP_IMPORTAR_USUARIO (ID,ID_LOTE,LAYOUT,LINHA,OPERACAO,MATRICULADOUSUARIO,MATRICULADOTITULAR,NOME,CATEGORIA,GRAUDEPARENTESCO,
                                                        DATADENASCIMENTO,DATADEINCLUSAO,SEXO,ESTADOCIVIL,CPF,RG,DATAEXPEDICAORG,ORGAOEMISSORRG,
                                                        PIS,CNS,DNV,NOMEDAMAE,PROFISSAO,SALARIO,DATADEADMISSAO,DATADEDEMISSAO,LOCACAO,PLANO,
                                                        TELEFONE1,TELEFONE2,TELEFONE3,EMAIL,CEP,TIPOLOGRADOURO,ENDERECO,NUMERO,COMPLEMENTO,BAIRRO,
                                                        CIDADE,ESTADO,DATADOCANCELAMENTO,MOTIVODOCANCELAMENTO,OPERADOR,IDCONTRATO,LOCALATENDIMENTO)
                                                VALUES (SEQ_IMPUS.NEXTVAL,:id_lote,'SOLUS',:linha,:operacao,:codigodousuario,:codigodotitular,:nome,:categoria,:graudeparentesco,
                                                        :datadenascimento,:datadeinclusao,:sexo,:estadocivil,:cpf,:rg,:dataexpedicaorg,:orgaoemissorrg,
                                                        :pis,:cns,:dnv,:nomedamae,:profissao,:salario,:datadeadmissao,:datadedemissao,:locacao,:plano,
                                                        :telefone1,:telefone2,:telefone3,:email,:cep,:tipologradouro,:endereco,:numero,:complemento,:bairro,
                                                        :cidade,:estado,:datadocancelamento,:motivodocancelamento,:operador,:idContrato,:localatendimento)";

              $sql->addParam(":id_lote"             ,$idLote);
              $sql->addParam(":linha"               ,$linha);
              $sql->addParam("operacao"             ,substr(strtoupper($data[0]),0,1));
              $sql->addParam(":codigodousuario"     ,$data[1]);
              $sql->addParam(":codigodotitular"     ,$data[2]);
              $sql->addParam(":nome"                ,strtoupper($data[3]));
              $sql->addParam(":categoria"           ,$data[4]);
              $sql->addParam(":graudeparentesco"    ,$data[5]);
              $sql->addParam(":datadenascimento"    ,$data[6]);
              $sql->addParam(":datadeinclusao"      ,$data[7]);
              $sql->addParam(":sexo"                ,$data[8]);
              $sql->addParam(":estadocivil"         ,$data[9]);
              $sql->addParam(":cpf"                 ,$data[10]);
              $sql->addParam(":rg"                  ,$data[11]);
              $sql->addParam(":dataexpedicaorg"     ,$data[12]);
              $sql->addParam(":orgaoemissorrg"      ,$data[13]);
              $sql->addParam(":pis"                 ,$data[14]);
              $sql->addParam(":cns"                 ,$data[15]);
              $sql->addParam(":dnv"                 ,$data[16]);
              $sql->addParam(":nomedamae"           ,strtoupper($data[17]));
              $sql->addParam(":profissao"           ,$data[18]);
              $sql->addParam(":salario"             ,$data[19]);
              $sql->addParam(":datadeadmissao"      ,$data[20]);
              $sql->addParam(":datadedemissao"      ,$data[21]);
              $sql->addParam(":locacao"             ,strtoupper($data[22]));
              $sql->addParam(":plano"               ,strtoupper($data[23]));
              $sql->addParam(":telefone1"           ,$data[24]);
              $sql->addParam(":telefone2"           ,$data[25]);
              $sql->addParam(":telefone3"           ,$data[26]);
              $sql->addParam(":email"               ,strtolower($data[27]));
              $sql->addParam(":cep"                 ,$data[28]);
              $sql->addParam(":tipologradouro"      ,strtoupper($data[29]));
              $sql->addParam(":endereco"            ,strtoupper($data[30]));
              $sql->addParam(":numero"              ,$data[31]);
              $sql->addParam(":complemento"         ,strtoupper($data[32]));
              $sql->addParam(":bairro"              ,strtoupper($data[33]));
              $sql->addParam(":cidade"              ,strtoupper($data[34]));
              $sql->addParam(":estado"              ,substr(strtoupper($data[35]),0,2));  
              $sql->addParam(":datadocancelamento"  ,$data[36]);
              $sql->addParam(":motivodocancelamento",$formata->acrescentaZeros($data[37],2));
              $sql->addParam(":localatendimento"    ,strtoupper($data[38]));
              $sql->addParam(":operador"            ,$_SESSION['id_operador']);
              $sql->addParam(":idContrato"          ,$_SESSION['id_contrato']);
              $erro = $sql->executeSQL($txt);
            }
          }
          
          if ($erros == 0) {
            $sql_lote = new Query($bd);
            $txt_lote = "BEGIN
                           VALIDAIMPORTACAOUSUARIO (:idLote);
                         END;";
            $sql_lote->addParam(":idLote",$idLote);
            $sql_lote->executeSQL($txt_lote);
          }

          $sql_erros = new Query($bd);
          $txt_erros = "SELECT REMOVE_ACENTO(ERRO) ERRO, REMOVE_ACENTO(NOME) NOME, REMOVE_ACENTO(VALOR) VALOR, LINHA FROM TMP_ERRO_IMPORTACAO WHERE ID_LOTE = :idLote ORDER BY LINHA";
          $sql_erros->addParam(":idLote",$idLote);
          $sql_erros->executeQuery($txt_erros);

          if ($sql_erros->count() > 0) {

            libxml_use_internal_errors(true);

            $erros = new DOMDocument('1.0','UTF-8');
            $erros->preserveWhiteSpace = false;
            $erros->formatOutput       = true;
            $root = $erros->createElement('erros');
            $total_erros = 0;

            while (!$sql_erros->eof()) {

              $total_erros++;
              $erro1          = $erros->createElement('erro');
              $descricao_erro = $erros->createElement('descricao',utf8_encode($sql_erros->result("ERRO")));
              $valor_erro     = $erros->createElement('valor',utf8_encode($sql_erros->result("VALOR")));
              $linha_erro     = $erros->createElement('linha',$sql_erros->result("LINHA"));
              $nome_erro      = $erros->createElement('nome',utf8_encode($sql_erros->result("NOME")));

              $erro1->appendChild($descricao_erro);
              $erro1->appendChild($linha_erro);
              $erro1->appendChild($valor_erro);
              $erro1->appendChild($nome_erro);
              $root->appendChild($erro1);

              $sql_erros->next();
            }

            $erros->appendChild($root);
            chdir($path);
            $arquivo_erro = $util->corrigeNomeArquivo($nome_arquivo);
            $erros->save('../temp/erro_'.$arquivo_erro);
            libxml_clear_errors();

            if ($total_erros > 0) {
              $tpl->CLASSE = "alert-error";
              $tpl->MSG    = "Arquivo \"".$nome_arquivo."\" processado com erros. <a href=\"#\" onclick=\"relatorio('".$arquivo_erro."');\">Visualizar erros.</a>";
              $tpl->block("ERRO");
            }
          } else {
            $sql_lote = new Query($bd);
            $txt_lote = "INSERT INTO HSSLIMPU (NNUMELIMPU,CARQULIMPU,NOPERUSUA,DDATALIMPU,NNUMETITU)
                                       VALUES (:lote,:arquivo,:operador,sysdate,:idContrato) ";
            $sql_lote->addParam(":lote",$idLote);
            $sql_lote->addParam(":arquivo",$nome_arquivo);
            $sql_lote->addParam(":operador",$_SESSION['id_operador']);
            $sql_lote->addParam(":idContrato",$_SESSION['id_contrato']);
            $sql_lote->executeSQL($txt_lote);

            $sql_tmp = new Query($bd);

            $txt_tmp = "SELECT * FROM TMP_IMPORTAR_USUARIO WHERE ID_LOTE = :idLote
                         ORDER BY DECODE(OPERACAO,'I',1,'A',2,3),MATRICULADOTITULAR,DECODE(CATEGORIA,'T',1,'F',2,3)";

            $sql_tmp->addParam(":idLote",$idLote);
            $sql_tmp->executeQuery($txt_tmp);

            while (!$sql_tmp->eof()) {

              if ($sql_tmp->result("OPERACAO") == 'I') {
                $sql_atcad = new Query($bd);
                $txt_atcad = "BEGIN ".
                             "   INSERE_USUARIO_WEB30(:contrato,:titular,:nome,:categoria,TO_DATE(:nascimento,'DD/MM/YYYY'),:estadonasc,:cidadenasc,:sexo,:matricula, ".
                             "                     :mae,:pai,:cpf,:rg,:orgao,:pis,:grau,:locacao,TO_DATE(:admissao,'DD/MM/YYYY'),:endereco,:bairro, ".
                             "                     :cidade,:estado,:cep,:plano,:acomodacao,:telefone1,to_date(:expedicao,'DD/MM/YYYY'),:numero, ".
                             "                     :tipo_logradouro,:complemento,:telefone2,:celular,:email,:salario,:inclusao,:estado_civil, ".
                             "                     :profissao,:setor,:local,:vendedor,:operador,:proposta,:valor,:csus,:ndnv,:observacao,:uniao,".
                             "                     :centro_custo,:atcadpai,:peso,:altura,:ip,:id_usuario,:retorno,:operadorarepasse,:localatendimento); ".
                             "END; ";   

                $sql_atcad->addParam(":contrato"          ,$_SESSION['id_contrato']);
                $sql_atcad->addParam(":titular"           ,$sql_tmp->result("IDTITULAR"));
                $sql_atcad->addParam(":nome"              ,$sql_tmp->result("NOME"));
                $sql_atcad->addParam(":categoria"         ,$sql_tmp->result("CATEGORIA"));
                $sql_atcad->addParam(":nascimento"        ,$sql_tmp->result("DATADENASCIMENTO"));
                $sql_atcad->addParam(":estadonasc"        ,'');
                $sql_atcad->addParam(":cidadenasc"        ,'');                
                $sql_atcad->addParam(":sexo"              ,$sql_tmp->result("SEXO"));
                $sql_atcad->addParam(":matricula"         ,$sql_tmp->result("MATRICULADOUSUARIO"));
                $sql_atcad->addParam(":mae"               ,$sql_tmp->result("NOMEDAMAE"));
                $sql_atcad->addParam(":pai"               ,'');                
                $sql_atcad->addParam(":cpf"               ,$sql_tmp->result("CPF"));
                $sql_atcad->addParam(":rg"                ,$sql_tmp->result("RG"));
                $sql_atcad->addParam(":orgao"             ,$sql_tmp->result("ORGAOEMISSORRG"));
                $sql_atcad->addParam(":pis"               ,$sql_tmp->result("PIS"));
                $sql_atcad->addParam(":grau"              ,$sql_tmp->result("GRAUDEPARENTESCO"));
                $sql_atcad->addParam(":locacao"           ,$sql_tmp->result("IDLOCACAO"));
                $sql_atcad->addParam(":admissao"          ,$sql_tmp->result("DATADEADMISSAO"));
                $sql_atcad->addParam(":endereco"          ,$sql_tmp->result("ENDERECO"));
                $sql_atcad->addParam(":bairro"            ,$sql_tmp->result("BAIRRO"));
                $sql_atcad->addParam(":cidade"            ,$sql_tmp->result("CIDADE"));
                $sql_atcad->addParam(":estado"            ,$sql_tmp->result("ESTADO"));
                $sql_atcad->addParam(":cep"               ,$sql_tmp->result("CEP"));
                $sql_atcad->addParam(":plano"             ,$sql_tmp->result("IDPLANO"));
                $sql_atcad->addParam(":acomodacao"        ,'');
                $sql_atcad->addParam(":telefone1"         ,$sql_tmp->result("TELEFONE1"));
                $sql_atcad->addParam(":expedicao"         ,$sql_tmp->result("DATAEXPEDICAORG"));
                $sql_atcad->addParam(":numero"            ,$sql_tmp->result("NUMERO"));
                $sql_atcad->addParam(":tipo_logradouro"   ,$sql_tmp->result("IDTIPOLOGRADOURO"));
                $sql_atcad->addParam(":complemento"       ,$sql_tmp->result("COMPLEMENTO"));
                $sql_atcad->addParam(":telefone2"         ,$sql_tmp->result("TELEFONE2"));
                $sql_atcad->addParam(":celular"           ,$sql_tmp->result("TELEFONE3"));
                $sql_atcad->addParam(":email"             ,$sql_tmp->result("EMAIL"));
                $sql_atcad->addParam(":salario"           ,$sql_tmp->result("SALARIO"));
                $sql_atcad->addParam(":inclusao"          ,$sql_tmp->result("DATADEINCLUSAO"));
                $sql_atcad->addParam(":estado_civil"      ,$sql_tmp->result("ESTADOCIVIL"));
                $sql_atcad->addParam(":profissao"         ,$sql_tmp->result("IDPROFISSAO"));
                $sql_atcad->addParam(":setor"             ,'');
                $sql_atcad->addParam(":local"             ,'I');
                $sql_atcad->addParam(":vendedor"          ,'');
                $sql_atcad->addParam(":operador"          ,$_SESSION['id_operador']);
                $sql_atcad->addParam(":proposta"          ,'');
                $sql_atcad->addParam(":valor"             ,0);
                $sql_atcad->addParam(":csus"              ,$sql_tmp->result("CNS"));
                $sql_atcad->addParam(":ndnv"              ,$formata->somenteNumeros($sql_tmp->result("DNV")));
                $sql_atcad->addParam(":observacao"        ,'');
                $sql_atcad->addParam(":uniao"             ,'');
                $sql_atcad->addParam(":centro_custo"      ,'');
                $sql_atcad->addParam(":atcadpai"          ,'');              
                $sql_atcad->addParam(":peso"              ,'');              
                $sql_atcad->addParam(":altura"            ,'');              
                $sql_atcad->addParam(":id_usuario"        ,$sql_tmp->result("IDUSUARIO"),12);
                $sql_atcad->addParam(":ip"                ,getenv("REMOTE_ADDR"));
                $sql_atcad->addParam(":operadorarepasse"  ,'');
                $sql_atcad->addParam(":localatendimento"  ,$sql_tmp->result("LOCALATENDIMENTO"));
                $sql_atcad->addParam(":retorno"           ,0,12);

                $erro = $sql_atcad->executeSQL($txt_atcad);
              }
              else {
                $sql_atcad = new Query($bd);
                $txt_atcad = "BEGIN ".
                             "  INSERE_ALTERACAO_WEB30(:operacao,:usuario,:operador,:nome,:categoria,to_date(:nascimento,'DD/MM/YYYY'),:estadonasc,:cidadenasc,:sexo,:matricula,:mae,:pai,".
                             "                        :cpf,:rg,:orgao,:pis,:grau,:locacao,to_date(:admissao,'DD/MM/YYYY'),:endereco,:bairro,".
                             "                        :cidade,:estado,:cep,to_date(:ini_afast,'DD/MM/YYYY'),to_date(:fim_afast,'DD/MM/YYYY'),".
                             "                        :motivo,:plano,:acomodacao,to_date(:expedicao,'DD/MM/YYYY'),:numero,:tipo_logradouro,:complemento,".
                             "                        :email,:telefone,:salario,:motivo_canc,:data_canc,:estado_civil,:profissao,:setor,:local,:vendedor,".
                             "                        :demissao,:csus,:ndnv,:idatcad,:re,:retorno,to_date(:uniao,'DD/MM/YYYY'),:centro_custo,:ip,:peso,:altura ); ".
                             "END;";

                $sql_atcad->addParam(":operacao"          ,$sql_tmp->result("OPERACAO"));
                $sql_atcad->addParam(":usuario"           ,$sql_tmp->result("IDUSUARIO"));
                $sql_atcad->addParam(":operador"          ,$_SESSION['id_operador']);
                $sql_atcad->addParam(":nome"              ,$sql_tmp->result("NOME"));
                $sql_atcad->addParam(":categoria"         ,$sql_tmp->result("CATEGORIA"));
                $sql_atcad->addParam(":nascimento"        ,$sql_tmp->result("DATADENASCIMENTO"));
                $sql_atcad->addParam(":estadonasc"        ,'');
                $sql_atcad->addParam(":cidadenasc"        ,'');
                $sql_atcad->addParam(":sexo"              ,$sql_tmp->result("SEXO"));
                $sql_atcad->addParam(":matricula"         ,$sql_tmp->result("CODIGODOUSUARIO"));
                $sql_atcad->addParam(":mae"               ,$sql_tmp->result("NOMEDAMAE"));
                $sql_atcad->addParam(":pai"               ,'');
                $sql_atcad->addParam(":cpf"               ,$sql_tmp->result("CPF"));
                $sql_atcad->addParam(":rg"                ,$sql_tmp->result("RG"));
                $sql_atcad->addParam(":orgao"             ,$sql_tmp->result("ORGAOEMISSORRG"));
                $sql_atcad->addParam(":pis"               ,$sql_tmp->result("PIS"));
                $sql_atcad->addParam(":grau"              ,$sql_tmp->result("GRAUDEPARENTESCO"));
                $sql_atcad->addParam(":locacao"           ,$sql_tmp->result("IDLOCACAO"));
                $sql_atcad->addParam(":admissao"          ,$sql_tmp->result("DATADEADMISSAO"));
                $sql_atcad->addParam(":endereco"          ,$sql_tmp->result("ENDERECO"));
                $sql_atcad->addParam(":bairro"            ,$sql_tmp->result("BAIRRO"));
                $sql_atcad->addParam(":cidade"            ,$sql_tmp->result("CIDADE"));
                $sql_atcad->addParam(":estado"            ,$sql_tmp->result("ESTADO"));
                $sql_atcad->addParam(":cep"               ,$sql_tmp->result("CEP"));
                $sql_atcad->addParam(":ini_afast"         ,$ini_afast);
                $sql_atcad->addParam(":fim_afast"         ,$fim_afast);
                $sql_atcad->addParam(":motivo"            ,$motivo);
                $sql_atcad->addParam(":plano"             ,$sql_tmp->result("IDPLANO"));
                $sql_atcad->addParam(":acomodacao"        ,'');
                $sql_atcad->addParam(":expedicao"         ,$sql_tmp->result("DATAEXPEDICAORG"));
                $sql_atcad->addParam(":numero"            ,$sql_tmp->result("NUMERO"));
                $sql_atcad->addParam(":tipo_logradouro"   ,$sql_tmp->result("IDTIPOLOGRADOURO"));
                $sql_atcad->addParam(":complemento"       ,$sql_tmp->result("COMPLEMENTO"));
                $sql_atcad->addParam(":email"             ,$sql_tmp->result("EMAIL"));
                $sql_atcad->addParam(":telefone"          ,$sql_tmp->result("TELEFONE1"));
                $sql_atcad->addParam(":salario"           ,$sql_tmp->result("SALARIO"));
                $sql_atcad->addParam(":motivo_canc"       ,$sql_tmp->result("IDCANCELAMENTO"));
                $sql_atcad->addParam(":data_canc"         ,$sql_tmp->result("DATADOCANCELAMENTO"));
                $sql_atcad->addParam(":estado_civil"      ,$sql_tmp->result("ESTADOCIVIL"));
                $sql_atcad->addParam(":profissao"         ,$sql_tmp->result("IDPROFISSAO"));
                $sql_atcad->addParam(":setor"             ,'');
                $sql_atcad->addParam(":local"             ,'I');
                $sql_atcad->addParam(":vendedor"          ,$vendedor);
                $sql_atcad->addParam(":demissao"          ,$sql_tmp->result("DATADEDEMISSAO"));
                $sql_atcad->addParam(":csus"              ,$sql_tmp->result("CNS"));
                $sql_atcad->addParam(":ndnv"              ,$formata->somenteNumeros($sql_tmp->result("DNV")));
                $sql_atcad->addParam(":idatcad"           ,0);
                $sql_atcad->addParam(":re"                ,0);
                $sql_atcad->addParam(":retorno"           ,0,12);
                $sql_atcad->addParam(":uniao"             ,'');
                $sql_atcad->addParam(":centro_custo"      ,'');
                $sql_atcad->addParam(":peso"              ,'');              
                $sql_atcad->addParam(":altura"            ,'');                              
                $sql_atcad->addParam(":ip"                ,getenv("REMOTE_ADDR"));


                $erro = $sql_atcad->executeSQL($txt_atcad);
              }

              $sql_atcad2 = new Query($bd);
              $txt_atcad2 = "UPDATE HSSATCAD SET NNUMELIMPU = :idLote WHERE NNUMEATCAD = :idAtCad";
              $sql_atcad2->addParam(":idLote",$idLote);
              $sql_atcad2->addParam(":idAtCad",$sql_atcad->getReturn(":retorno"));
              $erro = $sql_atcad2->executeSQL($txt_atcad2);

              $sql_tmp->next();
            }

            if ($erro <> '') {
              $tpl->CLASSE = "alert-error";
              $tpl->MSG    = $erro;
              $tpl->block("ERRO");
            }
            else {
              $tpl->CLASSE = "alert-success";
              $tpl->MSG = "Arquivo importado com sucesso.";
              $tpl->block("ERRO");
            }

          }

          $sql_erros = new Query($bd);
          $txt_erros = "BEGIN ".
                       "  DELETE FROM TMP_IMPORTAR_USUARIO WHERE ID_LOTE = :idLote; ".
                       "  DELETE FROM TMP_ERRO_IMPORTACAO WHERE ID_LOTE = :idLote;  ".
                       "END;";
          $sql_erros->addParam(":idLote",$idLote);
          $sql_erros->executeSQL($txt_erros);

          fclose ($f);
        } else {
          $tpl->CLASSE = "alert-error";
          $tpl->MSG = "Erro na transmiss�o. O tamanho do arquivo deve possuir no m�ximo ".ini_get("upload_max_filesize").".";
          $tpl->block("ERRO");
        }
      }
    } 
    else {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG = "� permitido somente o envio de arquivos com a extens�o rem. ('.rem')";
      $tpl->block("ERRO");
    }
  }
  else if (($_POST['acao'] == 'E') and (isset($_POST['acao']))) {
    $sql = new Query($bd);
    $txt = "SELECT COUNT(*) QTDE FROM HSSLIMPU,HSSATCAD
             WHERE HSSLIMPU.NNUMELIMPU = :id
               AND HSSLIMPU.NNUMELIMPU = HSSATCAD.NNUMELIMPU
               AND HSSATCAD.CFLAGATCAD IS NOT NULL ";
    $id = $seg->antiInjection($_POST['id']);
    
    $sql->addParam(":id",$id);
    $sql->executeQuery($txt);

    if ($sql->result("QTDE") > 0) {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG = "N�o foi poss�vel excluir este lote, alguns registros j� foram aceitos pela operadora.";
      $tpl->block("ERRO");
    }
    else {
      
      $q_temp = new Query($bd);
      $txt = "SELECT NNUMELIMPU FROM HSSATCAD WHERE NNUMELIMPU = :id";
      $q_temp->addParam(":id",$id);
      $q_temp->executeQuery($txt);
      
      if(($q_temp->result('NNUMELIMPU')) != ''){ 
        $sql = new Query($bd);
        $txt = "DELETE FROM HSSATCAD WHERE NNUMELIMPU = :id ";
        $sql->addParam(":id",$id);
        $erro = $sql->executeSQL($txt);		
      }
  
      $sql = new Query($bd);
      $txt = "DELETE FROM HSSLIMPU WHERE NNUMELIMPU = :id ";
      $sql->addParam(":id",$id);
      $erro .= $sql->executeSQL($txt);
      
      if ($erro <> '') {
        $tpl->CLASSE = "alert-error";
        $tpl->MSG = "N�o foi poss�vel excluir este lote. Erro: ".$erro;
        $tpl->block("ERRO");            
      }
      else {
        $tpl->CLASSE = "alert-success";
        $tpl->MSG = "Lote excluido com sucesso.";
        $tpl->block("ERRO");      
      }
    }
  }

  $sql = new Query($bd);
  $txt = "SELECT NNUMELIMPU,CARQULIMPU,TO_CHAR(DDATALIMPU,'DD/MM/YYYY') DDATALIMPU,NOPERUSUA,DDATALIMPU ORDEM
            FROM HSSLIMPU
           WHERE NNUMETITU = :idContrato
             AND DDATALIMPU >= ADD_MONTHS(FIRST_DAY(SYSDATE),-12)
             AND DDATALIMPU < TRUNC(LAST_DAY(SYSDATE)) + 1
           ORDER BY ORDEM DESC";
  $sql->addParam(":idContrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);
  
  while (!$sql->eof()) {
    $sql_qtde = new Query($bd);
    $txt_qtde = "SELECT COUNT(*) QTDE
                   FROM HSSATCAD
                  WHERE NNUMETITU = :idContrato
                    AND NNUMELIMPU = :idLote
                    AND CFLAGATCAD IS NOT NULL";
    $sql_qtde->addParam(":idContrato",$_SESSION['id_contrato']);
    $sql_qtde->addParam(":idLote",$sql->result("NNUMELIMPU"));
    $sql_qtde->executeQuery($txt_qtde); 
  
    $tpl->PROTOCOLO  = $sql->result("NNUMELIMPU");
    $tpl->DATA_ENVIO = $sql->result("DDATALIMPU");
    $arquivo = $util->corrigeNomeArquivo($sql->result("CARQULIMPU"));
    $tpl->ARQUIVO    = $arquivo;
    $tpl->OPERADOR   = $seg->retornaNomeOperador($bd,$sql->result("NOPERUSUA"));

    if ($sql_qtde->result("QTDE") == 0)
      $tpl->block("BOTAO_EXCLUIR");

    $tpl->block("LINHA");
    $sql->next();
  }

  $tpl->block("MOSTRA_LOTES_ENVIADOS");

  chdir($path);
  
  $tpl->block("MOSTRA_MENU");

  $bd->close();
  $tpl->show();

?>