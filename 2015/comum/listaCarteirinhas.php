<?php
  Session_start();
  require_once("../comum/autoload.php"); 
  
  $bd = new Oracle();  
  
  $sql = new Query($bd);
  $txt = "SELECT HSSUSUA.NNUMEUSUA, CNOMEUSUA, CCODIUSUA,NNUMECAWEB,
                 DECODE(HSSUSUA.CTIPOUSUA,'T','Titular','F','Tit Financ','A','Agregado','Dependente') CTIPOUSUA,
                 NVL(CMOTICAWEB,'0') CMOTICAWEB,NVL(NNUMEMTCAR,0) NNUMEMTCAR,NVL(CBO__CAWEB,'0') CBO__CAWEB
            FROM HSSCAWEB,HSSUSUA
           WHERE HSSCAWEB.CSITUCAWEB = 'I'
             AND HSSCAWEB.NNUMEUSUA = HSSUSUA.NNUMEUSUA
             AND HSSUSUA.NNUMETITU = :contrato ";
             
  if ($_SESSION['sistema'] == "Usuario") {
    if (($_SESSION['logando'] == 'PPJ') or ($_SESSION['logando'] == 'PFF')) {
      $txt .= "   AND HSSUSUA.NTITUUSUA = :titular ";
      $sql->addParam(":titular",$_SESSION['id_titular']);
    }
  }
  
  $txt .= "   AND NVL(HSSCAWEB.NNUMESETOR,-1) = :locacao
            ORDER BY CNOMEUSUA ";
                      
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->addParam(":locacao",$_SESSION['id_locacao']);     
  $sql->executeQuery($txt);
  
  $sql2 = new Query($bd);
  $txt = "SELECT NNUMEMTCAR, INITCAP(CDESCMTCAR) CDESCMTCAR, NVL(CVBO_MTCAR,'N') CVBO_MTCAR, NVL(CVOBSMTCAR,'N') CVOBSMTCAR
            FROM HSSMTCAR
           WHERE CWEB_MTCAR = 'S'
           ORDER BY 2 ";           
  $sql2->executeQuery($txt); 
  
  $opcoes = "<option value='NN0'>Motivo</option>";
  
  while (!$sql2->eof()) {
    $opcoes .= "<option value='".$sql2->result("CVOBSMTCAR").$sql2->result("CVBO_MTCAR").$sql2->result("NNUMEMTCAR")."'";
    
    if ($sql->result("NNUMEMTCAR") == $sql2->result("NNUMEMTCAR"))
      $opcoes .= " selected ";
      
    $opcoes .= ">".utf8_encode($sql2->result("CDESCMTCAR"))."</option>";
    
    $sql2->next(); 
  }  
  
  echo '<table class="table table-hover">
          <caption>Carteirinhas selecionadas</caption>
          <tr>
            <th>Benefici&aacute;rio</tdh>
            <th>Motivo</th>
            <th>Observa&ccedil;&atilde;o</th>
            <th>B.O.</th>
            <th>&nbsp;</th>            
          </tr>';
          
  while (!$sql->eof()) {

    echo "<tr>
            <td>
              <input class='span4' type='text' id='nome[]' name='nome[]' disabled value='".$sql->result("CNOMEUSUA")."'/>
              <input type='hidden' id='id_carteira[]' name='id_carteira[]' value='".$sql->result("NNUMECAWEB")."'/>                
            </td>                      
            <td>
              <select class='span3' id='motivos[]' name='motivos[]' >".$opcoes."</select>            
            </td>
            <td>
              <input class='span3' type='text' id='observacao[]' name='observacao[]' value=''/>
            </td>
            <td>
              <input class='span1' type='text' id='bo[]' name='bo[]'/>
            </td>
            <td>
              <a href='javascript:remove(".$sql->result("NNUMEUSUA").");' title='remover'>
                <img src='../comum/img/lixeira.png' width='18' heigth='18' border='0' alt='remover'/>
              </a>            
            </td>            
          </tr>";
          
    $sql->next();                         
  }
  
  echo '</table>';   
  
  $bd->close();
?>