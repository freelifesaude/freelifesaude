<?php 
  session_start();

  require_once("../comum/autoload.php");

  $bd      = new Oracle();   
  $formata = new Formata();
  

  if (isset($_POST['carteira'])){

    if ($_POST['carteira'] <> '') {
      
      $sql = new Query($bd);
      
      if ($_SESSION['apelido_operadora'] == 'casf') {
        $carteira  = $formata->somenteNumeros(Trim($_POST['carteira']));
        $carteira1 = $formata->acrescentaZeros($carteira,8);
        
        $txt = "SELECT CCODIUSUA
                  FROM HSSUSUA
                 WHERE (somente_numeros(CCARTUSUA) = :carteira OR somente_numeros(CCARTUSUA) = :carteira1) ";
        $sql->addParam(":carteira1",$carteira1);
      }
      else {
        $carteira   = $_POST['carteira'];
        $carteira1  = $formata->somenteNumeros(Trim($_POST['carteira']));
        
        $txt = "SELECT CCODIUSUA
                  FROM HSSUSUA
                 WHERE (CCARTUSUA = :carteira OR CCODIUSUA = :carteira OR  CCARTUSUA = :carteira1 OR CCODIUSUA = :carteira1) ";      
        $sql->addParam(":carteira1",$carteira1);
      }
      
	  
      $sql->addParam(":carteira",$carteira);
      $sql->executeQuery($txt);

      if ($sql->count() > 0) {
        $json['codigo'] = $sql->result("CCODIUSUA");    
      } else {
        $json['codigo'] = '';
      }
    } else {
      $json['codigo'] = '';     
    }

    echo json_encode($json);         
  }
  
  $bd->close();
  
?>