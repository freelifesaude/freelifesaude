<?php
  session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
  
    
  if (isset($_POST['usuario'])) {
    $nome       = strtoupper($seg->antiInjection($_POST['nome']));      
    $matricula   = $seg->antiInjection($_POST['matricula']);
    $qtde       = $seg->antiInjection($_POST['qtde']);  
	$nascimento = $seg->antiInjection($_POST['nascimento']);	
              
    $sql = new Query($bd);   			   
	$label = '';
	
    if($_POST['tipo'] == '4'){
	  $tipo = "AND NVL(NADMIEXPRO,-1) >= 0 ";
	  // Exames demissionais da profissao anterior
      $txt = "SELECT HSSPMED.CCODIPMED,HSSPMED.CNOMEPMED
                FROM HSSEXPRO,HSSPMED
               WHERE NNUMEUSUA = :usuario
                 AND NNUMEPROF = :profissao
                 AND CDEMIEXPRO = 'S' 
				 AND HSSPMED.CCODIPMED = HSSEXPRO.CCODIPMED ";
        // Exames admissionais da nova profissao
      $txt .= "UNION 
              SELECT HSSPMED.CCODIPMED,HSSPMED.CNOMEPMED 
	            FROM HSSEXPRO, HSSPMED,HSSTITU, HSSEMPR
		       WHERE HSSTITU.NNUMETITU = :contrato 
			     AND HSSEXPRO.NNUMEPROF = :profissao_anterior
				 AND HSSEXPRO.NNUMEEMPR = HSSEMPR.NNUMEEMPR				 
			     AND HSSEXPRO.CCODIPMED = HSSPMED.CCODIPMED
                 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR               
                 AND NADMIEXPRO >= 0 ";
      $sql->addParam(":usuario",$_POST['usuario']);	  
	  $sql->addParam(":profissao",$_POST['profissao']);  
	  $sql->addParam(":contrato",$_SESSION['id_contrato']);
	  $sql->addParam(":profissao_anterior",$_POST['profissaoA']);   
      $sql->executeQuery($txt);				 
				   
	}else if(($_POST['tipo'] == '2') or (($_POST['tipo'] == '5') and ($_POST['vencidos'] == '')))
	  $tipo = "AND NVL(NPERIEXPRO,-1) >= 0 ";
	else if($_POST['tipo'] == '1')
	  $tipo = "AND NVL(NADMIEXPRO,-1) >= 0 ";		
	else if($_POST['tipo'] == '3')	  
	  $tipo = "AND CDEMIEXPRO  = 'S' ";		
	else
	  $tipo = " ";
			
	if(($_POST['empresa'] == 'S') and ($_POST['tipo'] != '4')){
	  $label = "todos os exames";	  	  				
		
      $txt = "SELECT HSSPMED.CCODIPMED,HSSPMED.CNOMEPMED 
	            FROM HSSEXPRO, HSSPMED,HSSTITU, HSSEMPR
		       WHERE HSSTITU.NNUMETITU = :contrato 
			     AND HSSEXPRO.NNUMEPROF = :profissao
				 AND HSSEXPRO.NNUMEEMPR = HSSEMPR.NNUMEEMPR				 
			     AND HSSEXPRO.CCODIPMED = HSSPMED.CCODIPMED
                 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR " .
		      $tipo ;
	  $sql->addParam(":profissao",3045);	
	  $sql->addParam(":contrato",$_SESSION['id_contrato']);	   //8846659  
      $sql->executeQuery($txt);		
	}else if(($_POST['vencidos'] == '')  and ($_POST['tipo'] != '4')){	
	  $label = "todos os exames";
      $txt = "SELECT HSSPMED.CCODIPMED,HSSPMED.CNOMEPMED 
	            FROM HSSEXPRO, HSSPMED
		       WHERE NNUMEUSUA = :usuario
				 AND NNUMEPROF = :profissao
			     AND HSSEXPRO.CCODIPMED = HSSPMED.CCODIPMED " .
				 $tipo ;	
      $sql->addParam(":usuario",$_POST['usuario']);	  
	  $sql->addParam(":profissao",$_POST['profissao']);    
      $sql->executeQuery($txt);
	}else if ($_POST['tipo'] != '4'){
  	  $label = "somente exames vencidos";
      $txt = "SELECT HSSPMED.CCODIPMED,HSSPMED.CNOMEPMED
                FROM HSSUSUA, HSSEXPRO,HSSPMED
         	   WHERE HSSUSUA.NNUMEUSUA = :Usuario
                 AND CSITUUSUA IN ('A','P')
                 AND HSSEXPRO.NPERIEXPRO IS NOT NULL
                 AND HSSUSUA.NNUMEPROF = HSSEXPRO.NNUMEPROF
                 AND HSSUSUA.NNUMEUSUA = HSSEXPRO.NNUMEUSUA " .
			     $tipo .
			 "	 AND HSSEXPRO.CCODIPMED = HSSPMED.CCODIPMED
                 AND NVL(ADD_MONTHS(ULTIMA_REALIZACAO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA, HSSEXPRO.CCODIPMED),
                                    DECODE(TIPO_ULTIMO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA, HSSEXPRO.CCODIPMED),
                                    '1', DECODE(PERIODICIDADE(HSSUSUA.NNUMEUSUA), 0, NADMIEXPRO, 24),
                                    '2', DECODE(PERIODICIDADE(HSSUSUA.NNUMEUSUA), 0, NPERIEXPRO, 24),
                                    '3', 0,
                                    '4', DECODE(PERIODICIDADE(HSSUSUA.NNUMEUSUA), 0, NADMIEXPRO, 24),
                                    '5', DECODE(PERIODICIDADE(HSSUSUA.NNUMEUSUA), 0, NPERIEXPRO, 24)) ),sysdate) <= LAST_DAY(sysdate)
               ORDER BY 1";
    
      $sql->addParam(":usuario",$_POST['usuario']);    
      $sql->executeQuery($txt);
	}
    if($sql->result("CCODIPMED") != ''){
		
	//	<input type=\"checkbox\" title=\"Marcar/Desmarcar todos\" id=\"todos\" name=\"todos\" onclick=\"marcardesmarcar(this);\" value=\"todos\" />
	  $linhas = '';
	  $tabela = "	  <div class=\"row-fluid\">
      <div class=\"span12\">  
        <table class=\"table table-hover table-bordered\">
          <thead>     
            <tr>
              <th colspan=\"3\">
              <h5>EXAMES (exibindo " . $label . ") </h5>
              </th>
            </tr>
            <tr>
              <th>&nbsp;</th>
              <th>Código</th>
              <th>Nome</th>        
            </tr>
          </thead>

           "; 
      while(!$sql->eof()){	
	    $linhas .= "<tr><td align=\"center\"><input type=\"checkbox\" checked=\"checked\" class=\"marcar\" onclick=\"marcado(this);\" id=\"ck_exames[]\" name=\"ck_exames[]\" value=\"" . $sql->result("CCODIPMED")."\" /></td>
                    <td>". $sql->result("CCODIPMED") . "</td>
                    <td>". $sql->result("CNOMEPMED") . "</td></tr>";
	    $sql->next();
	  }
	  $tabela .= $linhas ."     
        </table>
      </div>      
    </div>
		  <script type=\"text/javascript\">
function marcardesmarcar(ck){	
	if(ck.checked == true){
	  $('.marcar').each(
         function(){
            $(this).attr(\"checked\", true);
         }
      );
	}else{
	  $('.marcar').each(
         function(){
            $(this).attr(\"checked\", false);
         }
      );
	}
	 
	}
	
	function marcado(ck){
            $(ck).attr(\"checked\", true);
  
	}

</script>";
   }
/*   $retorno = array();
   $retorno["id"] = $sql->result("NNUMEUSUA");   
   $retorno["nome"] = $sql->result("CNOMEUSUA");
   $retorno["profissao"] = $sql->result("CDESCPROF");   
   $retorno["id_nova_profissao"] = $sql->result("NNUMEPROF");      
  
  	echo json_encode($retorno); */
	
	echo $tabela;
  	$bd->close();
  }

?>