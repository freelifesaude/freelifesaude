<?php 
  session_start();
  
  require_once("../comum/autoload.php");
 
  
  $bd = new Oracle();
  $desc = "";
  
  if (isset($_POST['codigo'])) {
        
    if ($_POST['codigo'] <> "") {
      $sql = new Query($bd);      
 
      $txt = "SELECT VALIDA_MATMED(:codigo,:prestador,SYSDATE,:tabela,:tipo,:beneficiario,:versaoTiss) RESULTADO FROM DUAL";
      $sql->clear();
      $sql->addParam(":codigo",$_POST['codigo']);
      $sql->addParam(":tabela",$_POST['tabela']);
      $sql->addParam(":tipo",$_POST['tipo']);
      $sql->addParam(":prestador",$_SESSION['id_contratado']);

      if (isset($_POST['id_beneficiario']))
        $sql->addParam(":beneficiario",$_POST['id_beneficiario']);
      else if (isset($_POST['usuario']))
        $sql->addParam(":beneficiario",$_POST['usuario']);
      else
        $sql->addParam(":beneficiario",0);
      
      $sql->addParam(":versaoTiss",$_SESSION['versaoTiss']);
      $sql->executeQuery($txt); 

      $id_produto = $sql->result("RESULTADO");
				
      if ($id_produto > 0) {
        $sql->clear();
        $txt = "SELECT RETORNA_NOME_MATERIAL(:id,SYSDATE,:versaoTiss) CNOMEPRODU FROM DUAL";
        $sql->clear();
        $sql->addParam(":id",$id_produto);
        $sql->addParam(":versaoTiss",$_SESSION['versaoTiss']);
        $sql->executeQuery($txt);      
      
        $desc = $sql->result("CNOMEPRODU");        
      }        
      
      if ($desc == '')
        $desc = "Produto n�o localizado!";  

      $json['nome']           = utf8_encode($desc);        
    }
  }
  
  echo json_encode($json);
  
  $bd->close();
  
?>