<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  if (isset($_GET['cep'])){
    if ($_GET['cep'] <> "") {
      $sql = new Query($bd);
      $txt = "SELECT CNOMEENDER,CNOMECIDAD,CNOMEBAIRR,CESTACIDAD,NNUMETLOGR,HSSCIDAD.NNUMECIDAD,CLOGRENDER
                FROM HSSENDER,HSSCIDAD,HSSBAIRR
               WHERE HSSENDER.CCEP_ENDER = :cep
                 AND HSSENDER.NNUMECIDAD = HSSCIDAD.NNUMECIDAD
                 AND HSSENDER.NNUMEBAIRR = HSSBAIRR.NNUMEBAIRR ";
      $sql->clear();
      $cep = str_replace("-","",$_GET['cep']);
      $sql->addParam(":cep",$cep);
      $sql->executeQuery($txt);
      
      if (($sql->result("CLOGRENDER") <> '') and ($sql->result("NNUMETLOGR") == '')) {
        $sql2 = new Query($bd);
        $txt2 = "SELECT NNUMETLOGR FROM HSSTLOGR
                 WHERE CDESCTLOGR = :tipo";
        $sql2->addParam(":tipo",$sql->result("CLOGRENDER"));
        $sql2->executeQuery($txt2);  

        $idTipoLogradouro = $sql2->result("NNUMETLOGR");
      }
      else
        $idTipoLogradouro = $sql->result("NNUMETLOGR");

      $json = "[{endereco: '".utf8_encode($sql->result("CNOMEENDER"))."',
                  bairro: '".utf8_encode($sql->result("CNOMEBAIRR"))."',      
                  estado: '".$sql->result("CESTACIDAD")."',      
                  tipo_logradouro: '".$idTipoLogradouro."',                          
                  cidade: '".$sql->result("CNOMECIDAD")."'}]";
    } else {
      $json = "[{endereco: '',
                  bairro: '',
                  estado: '',
                  tipo_logradouro: '',
                  cidade: ''}]";
    }
    
    echo json_encode($json);     
  }
  
  $bd->close();
  
?>