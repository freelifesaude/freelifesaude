<?php 
  session_start();  
  require_once("../comum/autoload.php");
  
  $func = new Funcao();
  $bd   = new Oracle();
  
  if (isset($_POST['codigo'])){
    $variavel = $_POST['codigo'];

    if ($_SESSION['apelido_operadora'] == "vitallis" or
        $_SESSION['apelido_operadora'] == "saudemed") {
      $posicao = strpos($variavel,"=100=1=");
      if ($posicao > 18) {
        $variavel = substr($variavel,$posicao-19,19);
        echo $func->retornaCodigoUsuario($bd,$variavel);
      }
      else
        echo "";
    } else if ($_SESSION["apelido_operadora"] == "nossaSaude") {
      $posicao = strpos($variavel,"=100=1=");
      if ($posicao > 7) {
        $variavel = substr($variavel,$posicao-8,8);
        echo substr($variavel,0,6).".".substr($variavel,6,2);
      }
      else
        echo "";
    } else if ($_SESSION['apelido_operadora'] == "sinamed") {
      $variavel = substr($variavel,2,13);
    } else if ($_SESSION['apelido_operadora'] == "smile") {
      $posicao = strpos($variavel,'{2');

      if ($posicao > 0) {
        $variavel = substr($variavel,19,16);
        echo $func->retornaCodigoUsuario($bd,$variavel);
      }
      else
        echo "";
    
    } else if ($_SESSION['apelido_operadora'] == "poliSaude") {
	  $variavel = utf8_decode($variavel);
      $variavel = str_replace(":�",":�",$variavel);      
      $variavel = str_replace(";",":�",$variavel);      
      $posicao = strpos($variavel,":�");


      if ($posicao > 0){      
        $variavel = substr($variavel,$posicao,14);      //Pega o c�digo de traz para frente
        $variavel = substr($variavel,2,3).".".     // php come�a com 0
                    substr($variavel,5,2).".".
                    substr($variavel,7,4).".".
                    substr($variavel,11,2)."-".
                    substr($variavel,13,1);
        echo utf8_encode($variavel);            
      }
      else
        echo "";    
      
	  } else if ($_SESSION['apelido_operadora'] == "unisaudeMS") {  
    
      $variavel = utf8_decode($variavel);
      $variavel2 = str_replace(":�",":�",$variavel);      
      $variavel2 = str_replace("?;",":�",$variavel);      //%CARLOS ALBERTO M PELLAT  ?;00784045000936000=011207=007805643000?
      
      $posicao1 = strpos($variavel2,"�");
      $posicao2 = strpos($variavel2,"=");
      
      //�00784045001424002=011207=007805643000:  //4045.001424.00-2
      //FERNANDA SOARES VASCONCELOS
      
      if ($posicao1 == 0 and $posicao2 == 18) {
        $variavel2 = substr($variavel2,5,13);
      }
      else {
        //%HANNA BEATRIZ O PELLAT   ?;00784045001404311=011207=007805643000?      
        //%HANNA BEATRIZ O PELLAT   :�00784045001404311=011207=007805643000?      
        $posicao1 = strpos($variavel2,":�");
        
        if ($posicao1 > 0)
          $variavel2 = substr($variavel,$posicao1+6,13);
        else
          $variavel2 = '';
      }
      
      if ($variavel2 <> '')
        $variavel = substr($variavel2,0,4).".".     
                    substr($variavel2,4,6).".".       
                    substr($variavel2,10,2)."-".          
                    substr($variavel2,12,1);
            
      echo utf8_encode($variavel); 
        
    } else if ($_SESSION['apelido_operadora'] == "novaPlam") {
       $variavel = substr($variavel,0,6).".".substr($variavel,6,4).".".substr($variavel,10,2);
      echo $variavel;
	} else if ($_SESSION['apelido_operadora'] == "apasSorocaba") { 
	  echo $variavel = $formata->somenteNumeros($variavel);	  
    } else if ($_SESSION['apelido_operadora'] == "casf") {
      $posicao = strpos($variavel,";");
      $variavel = substr($variavel,$posicao+1,strlen($variavel));
      $posicao = strpos($variavel,"=");
      $variavel = substr($variavel,0,$posicao);
      
      $variavel = substr($variavel,0,4).".".     
                  substr($variavel,4,4).".".       
                  substr($variavel,8,4).".".          
                  substr($variavel,12,4);
                  
      echo $variavel;
    } else if ($_SESSION['codigo_unimed'] <> '') {
      echo $variavel;  
    } else {
      $variavel = trim(substr($variavel,1,13));
      echo $variavel;
    }
  } 
 
  $bd->close(); 
?>