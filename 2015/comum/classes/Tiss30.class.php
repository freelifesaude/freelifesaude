<?php
  class Tiss30
  {
       
    function validaGuiasConsulta30($bd,$dom,$root,$xml,$tipo_guia,$id_lote) {

      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;
      $seg         = new Seguranca();
      $tiss        = new Tiss();
         
      foreach ($xml->prestadorParaOperadora->loteGuias->guiasTISS->guiaConsulta as $guia) {
        $numero_guia++;

        // Guia
		
		    $guia_operadora = $guia->numeroGuiaOperadora; 
		
        $retorno = $tiss->validaGuiaTiss($bd,
                                         $dom,
                                         $root,
                                         $numero_guia,
                                         $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                         $guia_operadora,
                                         $guia->dadosBeneficiario->nomeBeneficiario,                                 
                                         $guia->dadosAtendimento->dataAtendimento,
                                         $_SESSION['valida_guia_consulta'],
                                         '',
                                         $guia->dadosAtendimento->dataAtendimento,
                                         '',
                                         'C',
                                         '1');

        if ($retorno['erro'] == 1) {
          $erros += 1;
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia'];
        }else {
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia'];
          $id_guia = $retorno['id_guia'];        
        }
        
        if ($id_guia > 0) {      
          $retorno = $tiss->validaGuiaEletiva($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                              $guia_operadora,
                                              $guia->dadosBeneficiario->nomeBeneficiario,                                 
                                              $id_guia);
                             
          if ($retorno['erro'] == 1)
            $erros += 1;                                  
        }
                  
        $erros += $tiss->validaDataTiss($bd,
                                        $dom,
                                        $root,
                                        $guia->dadosAtendimento->dataAtendimento,
                                        'dataAtendimento',
                                        $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                        $guia_operadora,
                                        $guia->dadosBeneficiario->nomeBeneficiario);     
          
        // Benefici�rio
        $retorno = $tiss->validaBeneficiarioTiss($bd,
                                                 $dom,
                                                 $root,
                                                 $numero_guia,
                                                 $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                                 $guia_operadora,
                                                 $guia->dadosBeneficiario->nomeBeneficiario,
                                                 '',
                                                 $guia->dadosBeneficiario->numeroCarteira,
                                                 $guia->dadosBeneficiario->identificadorBeneficiario,
                                                 $guia->dadosBeneficiario->numeroCNS,
                                                 $id_usuario_guia,
                                                 $cortesia);
        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_usuario = $retorno['id_usuario'];      
        
        // Se tiver o direito, n�o ir� validar o executante                            
        if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {                                    
          // Prestador contratado
          $retorno = $tiss->validaPrestadorTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia,
                                                $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                                $guia_operadora,  
                                                $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                                $guia->contratadoExecutante->nomeContratado,
                                                'Prestador contratado',
                                                $guia->contratadoExecutante->cpfContratado,
                                                $guia->contratadoExecutante->cnpjContratado,
                                                $guia->contratadoExecutante->codigoPrestadorNaOperadora,
                                                $guia->contratadoExecutante->CNES,
                                                '',
                                                '',
                                                '',
                                                '3.02');
                                         
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_contratado = $retorno['id_prestador'];                                          
                                     
          // Profissional executante
          $retorno = $tiss->validaPrestadorTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia, 
                                                $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                                $guia_operadora,
                                                $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                $guia->profissionalExecutante->nomeProfissional,
                                                'Profissional executante',
                                                '',
                                                '',
                                                '',
                                                '',
                                                $guia->profissionalExecutante->conselhoProfissional,
                                                $guia->profissionalExecutante->numeroConselhoProfissional,
                                                $guia->profissionalExecutante->UF,
                                                '3.02');
                                          
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_executante = $retorno['id_prestador']; 
        }                                   
        
        $erros += $tiss->validaPrestadorGuia($bd,
                                             $dom,
                                             $root,
                                             $numero_guia,                                       
                                             $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                             $guia_operadora,
                                             $guia->dadosBeneficiario->nomeBeneficiario,
                                             $id_guia,
                                             $id_contratado,
                                             $id_executante);            
             
        // C�digo do procedimento e quantidade
        $erros += $tiss->validaCodigoProcedimentoTiss($bd,
                                                      $dom,
                                                      $root,
                                                      $numero_guia,
                                                      $guia->cabecalhoConsulta->numeroGuiaPrestador,
                                                      $guia_operadora, 
                                                      $guia->dadosBeneficiario->nomeBeneficiario,                                                 
                                                      $guia->dadosAtendimento->procedimento->codigoProcedimento,
                                                      '',
                                                      1,
                                                      $id_executante);
     
      }       

      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }    
    }

    function validaGuiasSadt30($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {

      $data1       = new Data();
      $seg         = new Seguranca();
      $tiss        = new Tiss();
      
      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;       
      $guia_operadora = '';
      foreach ($xml->prestadorParaOperadora->loteGuias->guiasTISS->{"guiaSP-SADT"} as $guia) {
        $numero_guia++;
        
        $total += str_replace(',','.',$guia->valorTotal->valorTotalGeral);      
        
        $guia_operadora = $guia->dadosAutorizacao->numeroGuiaOperadora;
        
        if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
          if ($guia->dadosAutorizacao->numeroGuiaOperadora <> '')
            $guia_principal = $guia->dadosAutorizacao->numeroGuiaOperadora;
          else
            $guia_principal = $guia->cabecalhoGuia->guiaPrincipal;          
        } 
        else if ($_SESSION['apelido_operadora'] == 'universal') {
          if ($guia->cabecalhoGuia->guiaPrincipal <> '')
            $guia_principal = $guia->cabecalhoGuia->guiaPrincipal;
          else
            $guia_principal = $guia->dadosAutorizacao->senha;	  
        } 
        else if ($_SESSION['apelido_operadora'] == 'vitallis') {
          if ($guia->dadosAutorizacao->numeroGuiaOperadora <> '')
            $guia_principal = $guia->dadosAutorizacao->numeroGuiaOperadora;
          else
            $guia_principal = $guia->cabecalhoGuia->guiaPrincipal;
			
          if ($guia->dadosAutorizacao->numeroGuiaOperadora <> '')
            $guia_operadora = $guia->dadosAutorizacao->numeroGuiaOperadora;
          else
            $guia_operadora = $guia->cabecalhoGuia->guiaPrincipal;
			
        }
        else {
          $guia_principal = $guia->dadosAutorizacao->numeroGuiaOperadora;
        }
        
        if ($erros <= 50) {
          // Guia
			    $retorno = $tiss->validaGuiaTiss($bd,
									                         $dom,
									                         $root,
									                         $numero_guia,
									                         $guia->cabecalhoGuia->numeroGuiaPrestador,
									                         $guia_principal,
									                         $guia->dadosBeneficiario->nomeBeneficiario,                                   
									                         $guia->dadosAutorizacao->dataAutorizacao,
									                         $_SESSION['valida_guia_sadt'],
									                         $guia->dadosAutorizacao->senha,
									                         $guia->dadosAutorizacao->dataAutorizacao,
									                         '',
									                         'S',
                                           $guia->dadosSolicitacao->caraterAtendimento);
                                     
          if ($retorno['erro'] == 1){
            $erros += 1;
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
          }else {
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
            $id_guia = $retorno['id_guia'];
          }                                   
          if ($guia->dataAutorizacao <> ''){
          $erros += $tiss->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $guia->dadosAutorizacao->dataAutorizacao,
                                          'dataEmissaoGuia',
                                          $guia->cabecalhoGuia->numeroGuiaPrestador,
                                          $guia_principal,
                                          $guia->dadosBeneficiario->nomeBeneficiario); 
										  
			    }
                                        
          // Senha autorizacao
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDARSENHANOENVIODOXML",false)) {    
          
            if ((count($guia->dadosAutorizacao->senha) > 0) and ($guia->dadosAutorizacao->senha <> '') and ($id_guia == '')) {
			
              $retorno = $tiss->validaGuiaTiss($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,
                                               '-2',
                                               $guia->dadosAutorizacao->senha,
                                               $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                               '',
                                               $_SESSION['valida_guia_sadt'],
                                               $guia->dadosAutorizacao->senha,
                                               '',
                                               '',
                                               'S',
                                               $guia->dadosSolicitacao->caraterAtendimento);
              if ($retorno['erro'] == 1){
                $erros += 1;
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }else {
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }                      
            }             
          }

          // Benefici�rio
          $retorno = $tiss->validaBeneficiarioTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                   $guia_operadora,                                          
                                                   $guia->dadosBeneficiario->nomeBeneficiario,
                                                   '',
                                                   $guia->dadosBeneficiario->numeroCarteira,
                                                   $guia->dadosBeneficiario->identificadorBeneficiario,
                                                   $guia->dadosBeneficiario->numeroCNS,
                                                   $id_usuario_guia,
                                                   $cortesia);
												   
			
                                      
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_usuario = $retorno['id_usuario']; 
          
          // Se tiver o direito, n�o ir� validar o solicitante                            
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDARSOLICITANTENOENVIODOXML",false)) {
            // Solicitante contratado
            $retorno = $tiss->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora,   
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $guia->dadosSolicitante->contratadoSolicitante->nomeContratado,
                                                  'Solicitante contratado',
                                                  $guia->dadosSolicitante->contratadoSolicitante->cpfContratado,
                                                  $guia->dadosSolicitante->contratadoSolicitante->cnpjContratado,
                                                  $guia->dadosSolicitante->contratadoSolicitante->codigoPrestadorNaOperadora,
                                                  $guia->dadosSolicitante->contratadoSolicitante->numeroCNES,
                                                  '',
                                                  '',
                                                  '',
                                                  '3.02');

            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_solicitante = $retorno['id_prestador'];  
            
            // Profissional solicitante
            $retorno = $tiss->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $guia->dadosSolicitante->profissionalSolicitante->nomeProfissional,
                                                  'Profissional solicitante',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  $guia->dadosSolicitante->profissionalSolicitante->conselhoProfissional,
                                                  $guia->dadosSolicitante->profissionalSolicitante->numeroConselhoProfissional,
                                                  $guia->dadosSolicitante->profissionalSolicitante->UF,
                                                  '3.02');
                                            
            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_solicitante = $retorno['id_prestador'];                                          
          }                                        
         
          // Se tiver o direito, n�o ir� validar o executante                            
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {  
            // Executante contratado
            $retorno = $tiss->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                  $guia->dadosExecutante->contratadoExecutante->nomeContratado,
                                                  'Executante contratado',
                                                  $guia->dadosExecutante->contratadoExecutante->cpfContratado,
                                                  $guia->dadosExecutante->contratadoExecutante->cnpjContratado,
                                                  $guia->dadosExecutante->contratadoExecutante->codigoPrestadorNaOperadora,
                                                  $guia->dadosExecutante->CNES,
                                                  '',
                                                  '',
                                                  '',
                                                  '3.02');

            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_prestador = $retorno['id_prestador'];                                  
          }
          
          $erros += $tiss->validaPrestadorGuia($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,                                       
                                               $guia->cabecalhoGuia->numeroGuiaPrestador,
                                               $guia_principal,
                                               $guia->dadosBeneficiario->nomeBeneficiario,
                                               $id_guia,
                                               $id_prestador,
                                               $id_prestador);
                        
          if (count($guia->procedimentosExecutados->procedimentoExecutado) > 0) {
            // Procedimentos
            foreach ($guia->procedimentosExecutados->procedimentoExecutado as $proc) {

              // C�digos dos procedimentos
              $erros += $tiss->validaCodigoProcedimentoTiss($bd,
                                                            $dom,
                                                            $root,
                                                            $numero_guia,
                                                            $guia->cabecalhoGuia->numeroGuiaPrestador, 
                                                            $guia_operadora,  
                                                            $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                                            $proc->procedimento->codigoProcedimento,
                                                            $proc->procedimento->descricaoProcedimento,
                                                            $proc->quantidadeExecutada,
                                                            $id_prestador);
                                                        
              $erros += $tiss->validaDataTiss($bd,
                                              $dom,
                                              $root,
                                              $proc->dataExecucao,
                                              'dataExecucao',
                                              $guia->cabecalhoGuia->numeroGuiaPrestador,
                                              $guia_principal,
                                              $guia->dadosBeneficiario->nomeBeneficiario);                                                       

              if (count($proc->equipeSadt) > 0) {
                foreach ($proc->equipeSadt as $membro) {

                  // Prestador da equipe
                  switch ($membro->grauPart) {
                    case '00' : $tipo_prestador = 'Cirurgi�o';                 break;
                    case '01' : $tipo_prestador = 'Primeiro auxiliar';         break;
                    case '02' : $tipo_prestador = 'Segundo auxiliar';          break;
                    case '03' : $tipo_prestador = 'Terceiro auxiliar';         break;
                    case '04' : $tipo_prestador = 'Quarto auxiliar';           break;
                    case '05' : $tipo_prestador = 'Instrumentador';            break;
                    case '06' : $tipo_prestador = 'Anestesista';               break;
                    case '07' : $tipo_prestador = 'Auxiliar anestesista';      break;
                    case '08' : $tipo_prestador = 'Consultor';                 break;
                    case '09' : $tipo_prestador = 'Perfusionista';             break;
                    case '10' : $tipo_prestador = 'Pediatra na sala de parto'; break;
                    case '11' : $tipo_prestador = 'Auxiliar SADT';             break;
                    case '12' : $tipo_prestador = 'Clinico';                   break;
                    case '13' : $tipo_prestador = 'Intensivista';              break;
                  }
                    
                  $cpf = $membro->codigoProfissional->cpfContratado;
                  
                  // Se tiver o direito, n�o ir� validar o executante                            
                  if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {                  
                    $retorno = $tiss->validaPrestadorTiss($bd,
                                                          $dom,
                                                          $root,
                                                          $numero_guia,
                                                          $guia->cabecalhoGuia->numeroGuiaPrestador, 
                                                          $guia_operadora,  
                                                          $guia->dadosBeneficiario->nomeBeneficiario,                                                   
                                                          $membro->nomeProf,
                                                          $tipo_prestador,
                                                          $cpf,
                                                          '',
                                                          $membro->codigoProfissional->codigoPrestadorNaOperadora,
                                                          '',
                                                          $membro->conselho,
                                                          $membro->numeroConselhoProfissional,
                                                          $membro->UF,
                                                          '3.02');

                    if ($retorno['erro'] == 1)
                      $erros += 1;
                    else
                      $id_pres_equipe = $retorno['id_prestador'];                                                
                  }
                }
              }
            }
          }

          if (count($guia->outrasDespesas->despesa) > 0) {
            // Despesas
            foreach ($guia->outrasDespesas->despesa as $despesas) {

              if (($despesas->codigoDespesa == '02') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Medicamento
                
                $erros += $tiss->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->servicosExecutados->codigoProcedimento,
                                                  $despesas->servicosExecutados->quantidadeExecutada,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->servicosExecutados->dataExecucao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->servicosExecutados->descricaoProcedimento,
                                                  '2101',
                                                  $despesas->servicosExecutados->codigoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '1');  

                $erros += $tiss->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->servicosExecutados->dataExecucao,
                                                'dataRealizacaoMedicamento',
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            

              } else if (($despesas->codigoDespesa == '03') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Material
                $erros += $tiss->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->servicosExecutados->codigoProcedimento,
                                                  $despesas->servicosExecutados->quantidadeExecutada,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->servicosExecutados->dataExecucao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->servicosExecutados->descricaoProcedimento,
                                                  '2001',
                                                  $despesas->servicosExecutados->codigoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '2');
                                             
                $erros += $tiss->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->servicosExecutados->dataExecucao,
                                                'dataRealizacaoMaterial',
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            
                                             
              } else if (($despesas->codigoDespesa <> '02') and			             
                         ($despesas->codigoDespesa <> '03') and
                         ($despesas->codigoDespesa <> '08')) {

                  // Taxas, Diarias
                  $erros += $tiss->validaTaxasTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                   $guia_operadora,
                                                   $guia->dadosBeneficiario->nomeBeneficiario,                                            
                                                   $despesas->servicosExecutados->codigoProcedimento,
                                                   $despesas->servicosExecutados->quantidadeExecutada,
                                                   $id_contratado,
                                                   $despesas->servicosExecutados->descricaoProcedimento,
                                                   $despesas->servicosExecutados->dataExecucao,
                                                   $id_usuario,
                                                   '2401');
                                              
                  $erros += $tiss->validaDataTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $despesas->servicosExecutados->dataExecucao,
                                                  'dataRealizacaoTaxas',
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario);                                             
              
              } 
              else if (($despesas->codigoDespesa == '08') and ($_SESSION['valida_mat_med'] == 'D')) {
                $erros += $tiss->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora, 
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                  $despesas->servicosExecutados->codigoProcedimento,
                                                  $despesas->servicosExecutados->quantidadeExecutada,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->servicosExecutados->dataExecucao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->servicosExecutados->descricaoProcedimento,
                                                  '2201',
                                                  $despesas->servicosExecutados->codigoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '3');            
                         
              }
              
            }
          }         
        }
      }
      
      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function validaGuiasInternacao30($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {

      $seg   = new Seguranca();
      $data1 = new Data();
      $tiss  = new Tiss();
      
      $numero_guia  = 0;
      $msg_erros    = '';
      $erros        = 0;
      $id_prestador = 0;

      foreach ($xml->prestadorParaOperadora->loteGuias->guiasTISS->guiaResumoInternacao as $guia) {
        $numero_guia++;      

        $total += str_replace(',','.',$guia->valorTotal->totalGeral);      
        
		    $guia_operadora = $guia->dadosAutorizacao->numeroGuiaOperadora;
		
        if ($erros <= 50) {
          // Guia
          if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
            if ($guia->dadosAutorizacao->numeroGuiaOperadora <> '')
              $guia_principal = $guia->dadosAutorizacao->numeroGuiaOperadora;
            else
              $guia_principal = $guia->numeroGuiaSolicitacaoInternacao;            
          } else if (($_SESSION['apelido_operadora'] == 'vitallis') or ($_SESSION['apelido_operadora'] == 'unimedSalto')) {
              if ($guia->dadosAutorizacao->numeroGuiaOperadora <> '')
                $guia_principal = $guia->dadosAutorizacao->numeroGuiaOperadora;
              else
                $guia_principal = $guia->numeroGuiaSolicitacaoInternacao;   

			        if ($guia->dadosAutorizacao->numeroGuiaOperadora <> '')
                $guia_operadora = $guia->dadosAutorizacao->numeroGuiaOperadora;
              else
                $guia_operadora = $guia->numeroGuiaSolicitacaoInternacao;  
          } else {
            $guia_principal = $guia->dadosAutorizacao->numeroGuiaOperadora;
          }      
       
          $retorno = $tiss->validaGuiaTiss($bd,
                                           $dom,
                                           $root,
                                           $numero_guia,
                                           $guia->cabecalhoGuia->numeroGuiaPrestador,
                                           $guia_principal,
                                           $guia->dadosBeneficiario->nomeBeneficiario,                                   
                                           $guia->dadosAutorizacao->dataAutorizacao,
                                           $_SESSION['valida_guia_internacao'],
                                           $guia->dadosAutorizacao->senha,
                                           $guia->dadosInternacao->dataInicioFaturamento,
                                           $guia->dadosInternacao->dataFinalFaturamento,
                                           'I',
                                           $guia->dadosInternacao->caraterAtendimento);
                                      
          if ($retorno['erro'] == 1){
            $erros += 1;
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
          }else {
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];               
            $id_guia = $retorno['id_guia'];
          }                                     

          $erros += $tiss->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $guia->dadosAutorizacao->dataAutorizacao,
                                          'dataEmissaoGuia',
                                          $guia->cabecalhoGuia->numeroGuiaPrestador,
                                          $guia_principal,
                                          $guia->dadosBeneficiario->nomeBeneficiario);         

          // Benefici�rio
          $retorno = $tiss->validaBeneficiarioTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                   $guia_operadora,  
                                                   $guia->dadosBeneficiario->nomeBeneficiario,
                                                   '',
                                                   $guia->dadosBeneficiario->numeroCarteira,
                                                   $guia->dadosBeneficiario->identificadorBeneficiario,
                                                   $guia->dadosBeneficiario->numeroCNS,
                                                   $id_usuario_guia,
                                                   $cortesia);

          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_usuario = $retorno['id_usuario']; 
            
          // Executante contratado
          $retorno = $tiss->validaPrestadorTiss($bd,
                                                $dom,
                                                $root,
                                                $numero_guia, 
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_operadora,  
                                                $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                $guia->dadosExecutante->nomeContratado,
                                                'Executante contratado',
                                                $guia->dadosExecutante->cpfContratado,
                                                $guia->dadosExecutante->cnpjContratado,
                                                $guia->dadosExecutante->codigoPrestadorNaOperadora,
                                                $guia->dadosExecutante->CNES,
                                                '',
                                                '',
                                                '',
                                                '3.02');

          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_prestador = $retorno['id_prestador']; 
              
          $erros += $tiss->validaPrestadorGuia($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,                                       
                                               $guia->cabecalhoGuia->numeroGuiaPrestador,
                                               $guia_operadora,  
                                               $guia->dadosBeneficiario->nomeBeneficiario, 
                                               $id_guia,
                                               $id_prestador,
                                               $id_prestador);
              
          $erros += $tiss->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $guia->dadosInternacao->dataInicioFaturamento,
                                          'dataInicioFaturamento',
                                          $guia->cabecalhoGuia->numeroGuiaPrestador,
                                          $guia_operadora,  
                                          $guia->dadosBeneficiario->nomeBeneficiario); 

            if (count($guia->dadosInternacao->dataFimFaturamento) > 0) {
            $erros += $tiss->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->dadosInternacao->dataFimFaturamento,
                                            'dataFimFaturamento',
                                            $guia->cabecalhoGuia->numeroGuiaPrestador,
                                            $guia_operadora,  
                                            $guia->dadosBeneficiario->nomeBeneficiario);
          }                                          
                    
          // Procedimentos
          if (count($guia->procedimentosExecutados->procedimentoExecutado) > 0) {      
            foreach ($guia->procedimentosExecutados->procedimentoExecutado as $proc) {

              // C�digos dos procedimentos
              $erros += $tiss->validaCodigoProcedimentoTiss($bd,
                                                            $dom,
                                                            $root,
                                                            $numero_guia,
                                                            $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                            $guia_operadora,  
                                                            $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                                            $proc->procedimento->codigoProcedimento,
                                                            $proc->procedimento->descricaoProcedimento,
                                                            $proc->quantidadeExecutada,
                                                            $id_prestador);
                                                        
              $erros += $tiss->validaDataTiss($bd,
                                              $dom,
                                              $root,
                                              $proc->dataExecucao,
                                              'dataProcedimento',
                                              $guia->cabecalhoGuia->numeroGuiaPrestador,
                                              $guia_operadora,  
                                              $guia->dadosBeneficiario->nomeBeneficiario);                                                       

              if (count($proc->identEquipe->identificacaoEquipe) > 0) {
                foreach ($proc->identEquipe->identificacaoEquipe as $membro) {

                  // Prestador da equipe
                  switch ($membro->grauPart) {
                    case '00' : $tipo_prestador = 'Cirurgi�o';                 break;
                    case '01' : $tipo_prestador = 'Primeiro auxiliar';         break;
                    case '02' : $tipo_prestador = 'Segundo auxiliar';          break;
                    case '03' : $tipo_prestador = 'Terceiro auxiliar';         break;
                    case '04' : $tipo_prestador = 'Quarto auxiliar';           break;
                    case '05' : $tipo_prestador = 'Instrumentador';            break;
                    case '06' : $tipo_prestador = 'Anestesista';               break;
                    case '07' : $tipo_prestador = 'Auxiliar anestesista';      break;
                    case '08' : $tipo_prestador = 'Consultor';                 break;
                    case '09' : $tipo_prestador = 'Perfusionista';             break;
                    case '10' : $tipo_prestador = 'Pediatra na sala de parto'; break;
                    case '11' : $tipo_prestador = 'Auxiliar SADT';             break;
                    case '12' : $tipo_prestador = 'Clinico';                   break;
                    case '13' : $tipo_prestador = 'Intensivista';              break;
                  }
                
                  $cpf = $membro->codProfissional->cpfContratado;
                  
                  // Se tiver o direito, n�o ir� validar o executante                            
                  if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {
                    $retorno = $tiss->validaPrestadorTiss($bd, 
                                                          $dom,
                                                          $root,
                                                          $numero_guia, 
                                                          $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                          $guia_operadora,  
                                                          $guia->dadosBeneficiario->nomeBeneficiario,                                                   
                                                          $membro->nomeProf,
                                                          $tipo_prestador,
                                                          $cpf,
                                                          '',
                                                          $membro->codProfissional->codigoPrestadorNaOperadora,
                                                          '',
                                                          $membro->conselho,
                                                          $membro->numeroConselhoProfissional,
                                                          $membro->UF,
                                                          '3.02');
                                                     
                    if ($retorno['erro'] == 1)
                      $erros += 1;
                    else
                      $id_prestador = $retorno['id_prestador'];                                                 
                  }
                }
              }
            }
          }
          
          // Despesas
          if (count($guia->outrasDespesas->despesa) > 0) {
            foreach ($guia->outrasDespesas->despesa as $despesas) {

              if (($despesas->codigoDespesa == '02') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Medicamento
                $erros += $tiss->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora,  
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->servicosExecutados->codigoProcedimento,
                                                  $despesas->servicosExecutados->quantidadeExecutada,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->servicosExecutados->dataExecucao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->servicosExecutados->descricaoProcedimento,
                                                  '2101',
                                                  $despesas->servicosExecutados->codigoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  $id_usuario,
                                                  '5');
                                             
                $erros += $tiss->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->servicosExecutados->dataExecucao,
                                                'dataRealizacaoMedicamento',
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);                                            
   
              } else if (($despesas->codigoDespesa == '03') and ($_SESSION['valida_mat_med'] == 'D')) {

                // Material
                $erros += $tiss->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora,  
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->servicosExecutados->codigoProcedimento,
                                                  $despesas->servicosExecutados->quantidadeExecutada,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->servicosExecutados->dataExecucao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->servicosExecutados->descricaoProcedimento,
                                                  '2001',
                                                  $despesas->servicosExecutados->codigoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  $id_usuario,
                                                  '6');
                                             
                $erros += $tiss->validaDataTiss($bd,
                                                $dom,
                                                $root,
                                                $despesas->servicosExecutados->dataExecucao,
                                                'dataRealizacaoMaterial',
                                                $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                $guia_principal,
                                                $guia->dadosBeneficiario->nomeBeneficiario);
                                          
              } else if (($despesas->codigoDespesa <> '02') and			             
                         ($despesas->codigoDespesa <> '03') and
                         ($despesas->codigoDespesa <> '08')) {

                  // Taxas, Diarias
                  $erros += $tiss->validaTaxasTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                   $guia_operadora,  
                                                   $guia->dadosBeneficiario->nomeBeneficiario,                                            
                                                   $despesas->servicosExecutados->codigoProcedimento,
                                                   $despesas->servicosExecutados->quantidadeExecutada,
                                                   $id_contratado,
                                                   $despesas->servicosExecutados->descricaoProcedimento,
                                                   $despesas->servicosExecutados->dataExecucao,
                                                   $id_usuario,
                                                   '2401');
                                              
                  $erros += $tiss->validaDataTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $despesas->servicosExecutados->dataExecucao,
                                                  'dataRealizacaoTaxas',
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_principal,
                                                  $guia->dadosBeneficiario->nomeBeneficiario);                                            
              }
              else if (($despesas->codigoDespesa == '08') and ($_SESSION['valida_mat_med'] == 'D')) {
                $erros += $tiss->validaMatMedTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                  $guia_operadora,  
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                           
                                                  $despesas->servicosExecutados->codigoProcedimento,
                                                  $despesas->servicosExecutados->quantidadeExecutada,
                                                  $id_contratado,
                                                  $data1->formataData($bd,$despesas->servicosExecutados->dataExecucao,'DD/MM/YYYY','YYYY-MM-DD'),
                                                  $despesas->servicosExecutados->descricaoProcedimento,
                                                  '2201',
                                                  $despesas->servicosExecutados->codigoTabela,
                                                  $id_guia,
                                                  $id_usuario,
                                                  '4');
              }              
            }
          }
        }
      }

      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function validaGuiasHonorario30($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {  

      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;
      $tiss        = new Tiss();
      
      foreach ($xml->prestadorParaOperadora->loteGuias->guiasTISS->guiaHonorarios as $guia) {
        $numero_guia++;      
        
        $total += str_replace(',','.',$guia->valorTotalHonorarios);      
        
		    $guia_operadora = $guia->numeroGuiaOperadora;
		
        if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
          if ($guia->numeroGuiaOperadora <> '')
            $guia_principal = $guia->numeroGuiaOperadora;
          else
            $guia_principal = $guia->guiaSolicInternacao;      
        }
        else if ($_SESSION['apelido_operadora'] == 'promedmg') {
          if ($guia->guiaSolicInternacao <> '')
            $guia_principal = $guia->guiaSolicInternacao;
          else
            $guia_principal = $guia->numeroGuiaOperadora;

        } else if ($_SESSION['apelido_operadora'] == 'vitallis') {
            if ($guia->numeroGuiaOperadora <> '')
              $guia_principal = $guia->numeroGuiaOperadora;
            else
              $guia_principal = $guia->guiaSolicInternacao;          
			  
            if ($guia->numeroGuiaOperadora <> '')
              $guia_operadora = $guia->numeroGuiaOperadora;
            else
              $guia_operadora = $guia->guiaSolicInternacao;          
			  
        } else {
          $guia_principal = $guia->numeroGuiaOperadora;
        } 
        
        // Guia
		
        $retorno = $tiss->validaGuiaTiss($bd,
                                         $dom,
                                         $root,
                                         $numero_guia,
                                         $guia->cabecalhoGuia->numeroGuiaPrestador,
                                         $guia_principal,
                                         $guia->beneficiario->nomeBeneficiario,                                 
                                         $guia->dataEmissaoGuia,
                                         $_SESSION['valida_guia_internacao'],
                                         '',
                                         $guia->procedimentosRealizados->procedimentoRealizado->dataExecucao,
                                         '',
                                         'H',
                                         '1');

        if ($retorno['erro'] == 1){
          $erros += 1;
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia'];
        }else {
          $id_usuario_guia = $retorno['id_usuario']; 
          $cortesia = $retorno['cortesia']; 
          $id_guia = $retorno['id_guia'];
        } 
        
        $erros += $tiss->validaDataTiss($bd,
                                        $dom,
                                        $root,
                                        $guia->dataEmissaoGuia,
                                        'dataEmissaoGuia',
                                        $guia->cabecalhoGuia->numeroGuiaPrestador,
                                        $guia_principal,
                                        $guia->beneficiario->nomeBeneficiario);      
                                         
        // Benefici�rio
        $retorno = $tiss->validaBeneficiarioTiss($bd,
                                                 $dom,
                                                 $root,
                                                 $numero_guia,
                                                 $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                 $guia_operadora,
                                                 $guia->beneficiario->nomeBeneficiario,                                          
                                                 '',
                                                 $guia->beneficiario->numeroCarteira,
                                                 $guia->beneficiario->identificadorBeneficiario,
                                                 $guia->beneficiario->numeroCNS,
                                                 $id_usuario_guia,
                                                 $cortesia);

        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_usuario = $retorno['id_usuario']; 

        // Contratado
        $retorno = $tiss->validaPrestadorTiss($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->cabecalhoGuia->numeroGuiaPrestador,
                                              $guia_operadora,
                                              $guia->beneficiario->nomeBeneficiario,                                       
                                              $guia->localContratado->nomeContratado,
                                              'Contratado',
                                              '',
                                              $guia->localContratado->codigoContratado->cnpjLocalExecutante,
                                              $guia->localContratado->codigoContratado->codigoNaOperadora,
                                              $guia->localContratado->cnes,
                                              '',
                                              '',
                                              '',
                                              '3.02');
                                        
        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_contratado = $retorno['id_prestador'];                                        

        // Contratado executante
        $retorno = $tiss->validaPrestadorTiss($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->cabecalhoGuia->numeroGuiaPrestador,
                                              $guia_operadora,
                                              $guia->beneficiario->nomeBeneficiario,                                       
                                              $guia->dadosContratadoExecutante->nomeContratadoExecutante,
                                              'Contratado executante',
                                              '',
                                              '',
                                              $guia->dadosContratadoExecutante->codigoNaOperadora,
                                              $guia->dadosContratadoExecutante->cnesContratadoExecutante,
                                              '',
                                              '',
                                              '',
                                              '3.02');
                                        
        if ($retorno['erro'] == 1)
          $erros += 1;
        else
          $id_prestador = $retorno['id_prestador'];  

        $erros += $tiss->validaPrestadorGuia($bd,
                                             $dom,
                                             $root,
                                             $numero_guia,                                       
                                             $guia->cabecalhoGuia->numeroGuiaPrestador,
                                             $guia_operadora,
                                             $guia->beneficiario->nomeBeneficiario,     
                                             $id_guia,
                                             $id_contratado,
                                             $id_prestador);         

        // Procedimentos
        foreach ($guia->procedimentosRealizados->procedimentoRealizado as $proc) {

          // C�digos dos procedimentos
          $erros += $tiss->validaCodigoProcedimentoTiss($bd,
                                                        $dom,
                                                        $root,
                                                        $numero_guia,
                                                        $guia->cabecalhoGuia->numeroGuiaPrestador,
                                                        $guia_operadora,
                                                        $guia->beneficiario->nomeBeneficiario,                                                  
                                                        $proc->procedimento->codigoProcedimento,
                                                        $proc->procedimento->descricaoProcedimento,
                                                        $proc->quantidadeExecutada,
                                                        $id_prestador);
                                                    
          $erros += $tiss->validaDataTiss($bd,
                                          $dom,
                                          $root,
                                          $proc->dataExecucao,
                                          'dataProcedimento',
                                          $guia->cabecalhoGuia->numeroGuiaPrestador,
                                          $guia_operadora,
                                          $guia->beneficiario->nomeBeneficiario);                                                  
        }
      }

      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }

    function validaGuiasOdontologia30($bd,$dom,$root,$xml,$tipo_guia,$id_lote,$id_contratado,&$total) {

      $seg         = new Seguranca();
      $data1       = new Data();
      $tiss        = new Tiss();
      
      $numero_guia = 0;
      $msg_erros   = '';
      $erros       = 0;

      foreach ($xml->prestadorParaOperadora->loteGuias->guiasTISS->guiaOdonto as $guia) {
        $numero_guia++;
             
	    $guia_operadora = $guia->numeroGuiaOperadora;
		
        if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
          if ($guia->numeroGuiaPrincipal <> '')
            $guia_principal = $guia->numeroGuiaPrincipal;
          else
            $guia_principal = $guia->numeroGuiaOperadora;
        } else {
          $guia_principal = $guia->numeroGuiaOperadora;
        }
        
        if ($erros <= 50) {
          // Guia
          
          $retorno = $tiss->validaGuiaTiss($bd,
                                           $dom,
                                           $root,
                                           $numero_guia,
                                           $guia->numeroGuiaPrestador,
                                           $guia_principal,
                                           $guia->dadosBeneficiario->nomeBeneficiario,                                   
                                           $guia->dataAutorizacao,
                                           $_SESSION['valida_guia_sadt'],
                                           $guia->senhaAutorizacao,
                                           $guia->dataAutorizacao,
                                           '',
                                           'O',
                                           $guia->tipoAtendimento);
                                     
          if ($retorno['erro'] == 1){
            $erros += 1;
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
          }else {
            $id_usuario_guia = $retorno['id_usuario']; 
            $cortesia = $retorno['cortesia'];
            $id_guia = $retorno['id_guia'];
          }                                   

          if (count($guia->dataAutorizacao) > 0) {
            $erros += $tiss->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->dataAutorizacao,
                                            'dataAutorizacao',
                                            $guia->numeroGuiaPrestador,
                                            $guia_principal,
                                            $guia->dadosBeneficiario->nomeBeneficiario);  
          }
                                        
          // Senha autorizacao
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDARSENHANOENVIODOXML",false)) {    
     
            if ((count($guia->senhaAutorizacao) > 0) and ($guia->senhaAutorizacao <> '') and ($id_guia == '')) {
			
              $retorno = $tiss->validaGuiaTiss($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,
                                               '-2',
                                               $guia->senhaAutorizacao,
                                               $guia->dadosBeneficiario->nomeBeneficiario,                                       
                                               '',
                                               $_SESSION['valida_guia_sadt'],
                                               $guia->senhaAutorizacao,
                                               '',
                                               '',
                                               'O',
                                               $guia->tipoAtendimento);
                                          
              if ($retorno['erro'] == 1){
                $erros += 1;
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }else {
                $id_usuario_guia = $retorno['id_usuario']; 
                $cortesia = $retorno['cortesia']; 
              }                      
            }             
          }

          // Benefici�rio
          $retorno = $tiss->validaBeneficiarioTiss($bd,
                                                   $dom,
                                                   $root,
                                                   $numero_guia,
                                                   $guia->numeroGuiaPrestador,
                                                   $guia_operadora,
                                                   $guia->dadosBeneficiario->nomeBeneficiario,
                                                   '',
                                                   $guia->dadosBeneficiario->numeroCarteira,
                                                   $guia->dadosBeneficiario->identificadorBeneficiario,
                                                   $guia->dadosBeneficiario->numeroCNS,
                                                   $id_usuario_guia,
                                                   $cortesia);
                                      
          if ($retorno['erro'] == 1)
            $erros += 1;
          else
            $id_usuario = $retorno['id_usuario']; 
                                                      
          // Se tiver o direito, n�o ir� validar o executante                            
          if (!$seg->permissaoOutros($bd,"WEBPRESTADORNOVALIDAREXECUTANTENOENVIODOXML",false)) {  
            $retorno = $tiss->validaPrestadorTiss($bd,
                                                  $dom,
                                                  $root,
                                                  $numero_guia,
                                                  $guia->numeroGuiaPrestador,
                                                  $guia_operadora,
                                                  $guia->dadosBeneficiario->nomeBeneficiario,                                         
                                                  $guia->dadosProfissionaisResponsaveis->nomeProfExec,
                                                  'Contratado',
                                                  '',
                                                  '',
                                                  '',
                                                  '',
                                                  'CRO',
                                                  $guia->dadosProfissionaisResponsaveis->croExec,
                                                  $guia->dadosProfissionaisResponsaveis->ufExec,
                                                  '3.02');

            if ($retorno['erro'] == 1)
              $erros += 1;
            else
              $id_prestador = $retorno['id_prestador'];          
                        
            // executante
            if (count($guia->dadosProfissionaisResponsaveis->croExec2) > 0 ) {
              $retorno = $tiss->validaPrestadorTiss($bd,
                                                    $dom,
                                                    $root,
                                                    $numero_guia,
                                                    $guia->numeroGuiaPrestador,
                                                    $guia_operadora,
                                                    $guia->dadosBeneficiario->nomeBeneficiario,                                             
                                                    $guia->dadosProfissionaisResponsaveis->nomeProfExec2,
                                                    'Executante',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    'CRO',
                                                    $guia->dadosProfissionaisResponsaveis->croExec2,
                                                    $guia->dadosProfissionaisResponsaveis->ufExec2,
                                                    '3.02');
                                                   
              if ($retorno['erro'] == 1)
                $erros += 1;
              else
                $id_prestador_compl = $retorno['id_prestador'];                                        
            }
        
            $erros += $tiss->validaPrestadorGuia($bd,
                                                 $dom,
                                                 $root,
                                                 $numero_guia,                                       
                                                 $guia->numeroGuiaPrestador,
                                                 $guia_principal,
                                                 $guia->dadosBeneficiario->nomeBeneficiario,
                                                 $id_guia,
                                                 $id_prestador,
                                                 $id_prestador);
        
          }
      
          if (count($guia->dataAutorizacao) > 0) {
            $erros += $tiss->validaDataTiss($bd,
                                            $dom,
                                            $root,
                                            $guia->dataAutorizacao,
                                            'dataAutorizacao',
                                            $guia->numeroGuiaPrestador,
                                            $guia_principal,
                                            $guia->dadosBeneficiario->nomeBeneficiario);
          }         
        
          if (count($guia->procedimentosExecutados) > 0) {
            // Procedimentos
            foreach ($guia->procedimentosExecutados as $proc) {

              // C�digos dos procedimentos
              $erros += $tiss->validaCodigoProcedimentoTiss($bd,
                                                            $dom,
                                                            $root,
                                                            $numero_guia,
                                                            $guia->numeroGuiaPrestador, 
                                                            $guia_operadora,
                                                            $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                                            $proc->procSolic->codigoProcedimento,
                                                            $proc->procSolic->descricaoProcedimento,
                                                            $proc->qtdProc,
                                                            $id_prestador);

              $erros += $tiss->validaDenteTiss($bd,
                                               $dom,
                                               $root,
                                               $numero_guia,
                                               $guia->numeroGuiaPrestador, 
                                               $guia_operadora,
                                               $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                               $proc->denteRegiao->codRegiao,
                                               $proc->denteRegiao->codDente,
                                               $id_prestador);

              $erros += $tiss->validaFaceTiss($bd,
                                              $dom,
                                              $root,
                                              $numero_guia,
                                              $guia->numeroGuiaPrestador, 
                                              $guia_operadora,
                                              $guia->dadosBeneficiario->nomeBeneficiario,                                                      
                                              $proc->denteFace,
                                              $id_prestador);                                                      
                                                        
              $erros += $tiss->validaDataTiss($bd,
                                              $dom,
                                              $root,
                                              $proc->dataRealizacao,
                                              'dataRealizacao',
                                              $guia->numeroGuiaPrestador,
                                              $guia_principal,
                                              $guia->dadosBeneficiario->nomeBeneficiario);                                                       
            }
          }        
        }
      }
      
      if ($erros > 0) {
        return $erros;
      } else {
        return 0;
      }  
    }
  }
?>