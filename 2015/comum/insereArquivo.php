<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
  
  require_once("../comum/layoutJanela.php"); 
  $tpl->addFile("JANELA_CONTEUDO","../comum/insereArquivo.htm"); 
      
  $bd = new Oracle();  
  
  $idSuperior = $seg->antiInjection($_POST['pIdArquivo']);
  $tipo       = $seg->antiInjection($_POST['pTipo']);
  $local      = $seg->antiInjection($_POST['pLocal']);
    
  if (isset($_POST['enviar'])) { 
    $nome    = $seg->antiInjection($_POST['nome']);
    $arquivo = $_FILES['arquivo'];
  
    if (isset($arquivo['tmp_name']) and ($tipo == 'A'))
      $tamanhoArquivo = filesize($arquivo['tmp_name'])/1024;
    else
      $tamanhoArquivo = 0;
      
    $obrigatorio = "";
    
    if (($nome == '') and ($tipo == 'A') and ($local == 'NOSSACOOPERATIVA'))
      $obrigatorio .= "- Informe o nome do arquivo.<br>";

    if (($nome == '') and ($tipo == 'D'))
      $obrigatorio .= "- Informe o nome da pasta.<br>";
      
    if (($arquivo['tmp_name'] == '') and ($tipo == 'A'))
      $obrigatorio .= "- Informe o arquivo.<br>";    
    
    if ($obrigatorio <> '') {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG = $obrigatorio;
      $tpl->block("MENSAGEM");
    }    
    else if ($tamanhoArquivo <= $func->convertBytes(ini_get("upload_max_filesize"))) {

      if ($tipo == 'A') {
        $extensao = strtoupper(pathinfo($arquivo['name'], PATHINFO_EXTENSION));
        $nomeArquivo = $arquivo['name'];
      }
      else
        $nomeArquivo = $nome;
        
      if ($local == 'RECURSOPROPRIO') {
        $nomeArquivo = 'recurso_proprio';
        $nome        = 'recurso_proprio';
        
        $sql_existe = new Query($bd);
        $txt_existe = "SELECT NNUMEARQUI FROM HSSARQUI
                        WHERE CLOCAARQUI = 'RECURSOPROPRIO'";
        $sql_existe->executeQuery($txt_existe);
        
        if ($sql_existe->result("NNUMEARQUI") > 0)
          $idArquivo = $sql_existe->result("NNUMEARQUI");
        else
          $idArquivo = 0;        
      }
      else if ($local == 'RELACAOBENEFICIOS') {
        $nomeArquivo = 'relacao_beneficio';
        $nome        = 'relacao_beneficio';
        
        $sql_existe = new Query($bd);
        $txt_existe = "SELECT NNUMEARQUI FROM HSSARQUI
                        WHERE CLOCAARQUI = 'RELACAOBENEFICIOS'";
        $sql_existe->executeQuery($txt_existe);
        
        if ($sql_existe->result("NNUMEARQUI") > 0)
          $idArquivo = $sql_existe->result("NNUMEARQUI");
        else
          $idArquivo = 0;        
      }      
      else
        $idArquivo = 0;
                        
      if ($idArquivo > 0) {
        $sql = new Query($bd);
        $txt = "UPDATE HSSARQUI 
                   SET CNOMEARQUI = :arquivo,
                       CDESCARQUI = :nome,
                       DDATAARQUI = SYSDATE,
                       NSUPEARQUI = :superior,
                       CTIPOARQUI = :tipo,
                       CEXT_ARQUI = :extensao,
                       CLOCAARQUI = :local
                 WHERE NNUMEARQUI = :idArquivo";
        $sql->addParam(":idArquivo",$idArquivo);
        $sql->addParam(":arquivo",$nomeArquivo);
        $sql->addParam(":nome",$nome);
        
        if ($idSuperior > 0)
          $sql->addParam(":superior",$idSuperior);
        else
          $sql->addParam(":superior","");
        
        $sql->addParam(":tipo",$tipo);
        $sql->addParam(":extensao",$extensao);
        $sql->addParam(":local",$local);
        $sql->executeSQL($txt);    

        if ($tipo == 'A') {       
          $imagem = file_get_contents($arquivo['tmp_name']);

          $sql2 = new Query($bd);
          $txt2 = "DELETE FROM HSSARQNT WHERE NNUMEARQUI = :idArquivo";
          $sql2->addParam(":idArquivo",$idArquivo);
          $sql2->executeSQL($txt2);
          
          $sql2 = new Query($bd);
          $txt2 = "INSERT INTO HSSARQNT (NNUMEARQUI,CARQUARQNT)
                                VALUES (:idArquivo,EMPTY_BLOB()) RETURNING CARQUARQNT INTO :imagem";
          $sql2->addParam(":idArquivo",$idArquivo);
          $sql2->addLob(":imagem",$imagem);
          $sql2->executeSQL($txt2);
        }        
      }
      else {
        $sql = new Query($bd);
        $txt = "SELECT SEQ_ARQUI.NEXTVAL PROXIMO FROM DUAL";
        $sql->executeQuery($txt);
        
        $idArquivo = $sql->result("PROXIMO");
      
        $sql = new Query($bd);
        $txt = "INSERT INTO HSSARQUI (NNUMEARQUI,CNOMEARQUI,CDESCARQUI,DDATAARQUI,NSUPEARQUI,CTIPOARQUI,CEXT_ARQUI,CLOCAARQUI)
                              VALUES (:idArquivo,:arquivo,:nome,SYSDATE,:superior,:tipo,:extensao,:local)";
        $sql->addParam(":idArquivo",$idArquivo);
        $sql->addParam(":arquivo",$nomeArquivo);
        $sql->addParam(":nome",$nome);
        
        if ($idSuperior > 0)
          $sql->addParam(":superior",$idSuperior);
        else
          $sql->addParam(":superior","");
        
        $sql->addParam(":tipo",$tipo);
        $sql->addParam(":extensao",$extensao);
        $sql->addParam(":local",$local);
        $sql->executeSQL($txt);
               
        if ($tipo == 'A') {
        
          if ($local == 'NOSSACOOPERATIVA') {
            $sql4 = new Query($bd);
            $txt4 = "SELECT SEQNOTIC.NEXTVAL PROXIMO FROM DUAL";
            $sql4->executeQuery($txt4);
            
            $idNoticia = $sql4->result("PROXIMO");
          
            $sql3 = new Query($bd);
            $txt3 = "INSERT INTO HSSNOTIC (NNUMENOTIC,CTITUNOTIC,CPUBLNOTIC,DDATANOTIC,CTIPONOTIC)
                                  VALUES (:idNoticia,:titulo,'S',SYSDATE,'N')";
            $sql3->addParam(":titulo","Incluso arquivo ".$nome);
            $sql3->addParam(":idNoticia",$idNoticia);
            $sql3->executeSQL($txt3);
          }
        
          $imagem = file_get_contents($arquivo['tmp_name']);
          
          $sql2 = new Query($bd);
          $txt2 = "INSERT INTO HSSARQNT (NNUMEARQUI,CARQUARQNT)
                                VALUES (:idArquivo,EMPTY_BLOB()) RETURNING CARQUARQNT INTO :imagem";
          $sql2->addParam(":idArquivo",$idArquivo);
          $sql2->addLob(":imagem",$imagem);
          $sql2->executeSQL($txt2);
        }        
      }  

      if ($local == 'NOSSACOOPERATIVA') {
        echo "<script>
                window.opener.abreDiretorio(".$idSuperior.");        
                window.close();        
             </script>";
      }
      else {
        echo "<script>                
                window.opener.location.reload();
                window.close();        
             </script>";
      }      
    }
    else {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG = 'O tamanho do arquivo n�o pode ser superior a '.ini_get("upload_max_filesize") . '.';
      $tpl->block("MENSAGEM");    
    }
  }

  if ($local == 'NOSSACOOPERATIVA')
    $tpl->block("MOSTRA_NOME");
    
  if ($tipo == 'A') {
    $tpl->BOTAO_NOME = "Enviar";
    $tpl->block("MOSTRA_ARQUIVO");
  }
  else
    $tpl->BOTAO_NOME = "Criar";  
  
  $tpl->ID_SUPERIOR = $idSuperior;
  $tpl->TIPO        = $tipo;
  $tpl->LOCAL       = $local;
    
  $tpl->show();  
  $bd->close();
?>