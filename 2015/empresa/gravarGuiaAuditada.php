<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
   
  $operador    = $_SESSION['id_operador'];
  $guia        = $_REQUEST['guia'];
  $acao        = $_REQUEST['acao'];
  
  $seg->alteraOperador($bd,$_SESSION['id_operador']);
  if ($acao == 'L') {
    $sql = new Query();  
    $txt = "UPDATE HSSPGUI 
               SET CSTATPGUI  = NULL,
                   NAUDIUSUA  = :operador,
                   DAUDIPGUI  = SYSDATE
             WHERE NNUMEGUIA  = :guia ";
    
         
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":guia",$guia);
    $erro = $sql->executeSQL($txt);
       
    $sql = new Query();  
    $txt = "UPDATE HSSDGUI 
               SET CSTATDGUI  = NULL,
                   NNUMEMOTLN = null,
                   NAUDIUSUA  = :operador
             WHERE NNUMEGUIA  = :guia ";
             
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":guia",$guia);
    $erro += $sql->executeSQL($txt);
        
    $sql = new Query();  
    $txt = "UPDATE HSSMMGUI 
               SET CSTATMMGUI= NULL,
                   NNUMEMOTLN = null, 
                   NAUDIUSUA  = :operador,
                   DAUDIMMGUI  = SYSDATE
                  WHERE NNUMEGUIA = :guia";
        
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":guia",$guia);
    $erro += $sql->executeSQL($txt);

    if ($erro == '') {
      $sql2 = new Query();  
      $txt2 = "UPDATE HSSGUIA
                 SET CSTATGUIA  = NULL
               WHERE NNUMEGUIA  = :guia
                 AND( NNUMEGUIA IN (SELECT NNUMEGUIA FROM HSSPGUI WHERE CSTATPGUI IS NULL AND NNUMEGUIA = HSSGUIA.NNUMEGUIA)
                 OR  NNUMEGUIA IN (SELECT NNUMEGUIA FROM HSSDGUI WHERE CSTATDGUI IS NULL AND NNUMEGUIA = HSSGUIA.NNUMEGUIA)
				 OR  NNUMEGUIA IN (SELECT NNUMEGUIA FROM HSSMMGUI WHERE CSTATMMGUI IS NULL AND NNUMEGUIA = HSSGUIA.NNUMEGUIA)) ";
               
      $sql2->addParam(":guia",$guia);
      $erro = $sql2->executeSQL($txt2);    
    }

  }
  else if ($acao == 'N') {
    $sql2 = new Query();  
    $txt2 = "UPDATE HSSGUIA
               SET CSTATGUIA  = 'N'
             WHERE NNUMEGUIA  = :guia
               AND NNUMEGUIA IN (SELECT NNUMEGUIA FROM HSSPGUI WHERE CSTATPGUI IS NULL AND NNUMEGUIA = HSSGUIA.NNUMEGUIA) ";
             
    $sql2->addParam(":guia",$guia);
    $erro = $sql2->executeSQL($txt2);    
  }
  
  if ($erro <> '')
    echo json_encode(array('sucesso'=>false,'msg'=>utf8_encode($erro)));  
  else
    echo json_encode(array('sucesso'=>true,'msg'=>''));
  
  $bd->close();
?>