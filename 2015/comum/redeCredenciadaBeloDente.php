<?php
  require_once("../comum/autoload.php");
    
  $seg->secureSessionStart();
  
  if ($_SESSION['sistema'] <> 'Rede') {
    require_once('../comum/sessao.php');   
  }
    
  $bd      =  new Oracle();    

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");

  $_SESSION['titulo'] = "REDE CREDENCIADA";

  if (!isset($_SESSION['id_contrato']))
    $_SESSION['id_contrato'] = 0;
    
  if (!isset($_SESSION['id_locacao']))
    $_SESSION['id_locacao'] = 0;

  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(150,5,'Rede Credenciada',0,1);      
      $this->Ln(10);
      $this->Cell(190,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/redeCredenciadaBeloDente.htm");
  
  if (isset($_GET['idSessao']))
    $tpl->ID_SESSAO = $_GET['idSessao'];       
  
  $registros        = 20;  
  
  if (isset($_GET['pg']))
    $pg = $_GET['pg'];
	else
    $pg = 0;
  
	$inicio = $pg * $registros;
  
  $filtros = array();   
  
  if (isset($_POST['listar']) or isset($_POST['imprimir'])) {       
    $filtros['tipo_prestador'] = $seg->antiInjection($_POST['tipo_prestador']);
    $filtros['estado']         = $seg->antiInjection($_POST['estado']);
    $filtros['cidade']         = $seg->antiInjection($_POST['cidade']);
    $filtros['especialidade']  = $seg->antiInjection($_POST['especialidade']);
    $filtros['area']           = $seg->antiInjection($_POST['area']);
    
    if (isset($_POST['indicavel']))
      $filtros['indicavel']    = $seg->antiInjection($_POST['indicavel']);          
    else
      $filtros['indicavel']    = '';
      
    $filtros['bairro']         = $seg->antiInjection($_POST['bairro']);
    
    if (isset($_POST['plano']))    
      $filtros['plano']        = $seg->antiInjection($_POST['plano']);
    else
      $filtros['plano']        = '';
    
    $filtros['nome_prestador'] = $seg->antiInjection($_POST['nome_prestador']);
    $filtros['inicio']         = 0; 
    $pg                        = 0;    
    $filtros['executa']        = 'N';           
    $_SESSION['filtrosRede']   = $filtros;     
  }
  else if (isset($_SESSION['filtrosRede'])) {
    $filtros = $_SESSION['filtrosRede'];
    $filtros['executa']          = 'S';       
    $filtros['inicio']           = $inicio;
  } 
  else {
    $filtros['tipo_prestador'] = '';
    $filtros['estado']         = '';   
    $filtros['cidade']         = '';
    $filtros['especialidade']  = '';
    $filtros['area']           = '';
    $filtros['indicavel']      = '';
    $filtros['bairro']         = '';
    $filtros['plano']          = '';
    $filtros['nome_prestador'] = '';
    $filtros['inicio']         = 0;  
    $filtros['executa']        = 'N';  
  }
  
  $tipo_prestador = $filtros['tipo_prestador'];
  $estado         = $filtros['estado'];
  $cidade         = $filtros['cidade'];
  $especialidade  = $filtros['especialidade'];
  $area_prestador = $filtros['area'];
  $indicavel      = $filtros['indicavel'];
  $bairro         = $filtros['bairro'];
  $plano          = $filtros['plano'];		
  $nome_prestador = $filtros['nome_prestador'];  
  $inicio         = $filtros['inicio'];  
     
  $tpl->block("MOSTRA_AREA");    
    
  $sql =  new Query($bd);
      
  if ($_SESSION['sistema'] == 'Rede') {
	
    /** Select Plano **/
    $txt = "SELECT DISTINCT HSSPLAN.NNUMEPLAN, CCODIPLAN || ' - ' || CDESCPLAN CDESCPLAN
              FROM HSSPLAN,HSSREDPL
             WHERE NVL(CSITUPLAN,'A') = 'A'
               AND HSSPLAN.NNUMEPLAN = HSSREDPL.NNUMEPLAN
             ORDER BY 2";	
    $sql->executeQuery($txt);
      
    if ($sql->count() > 0 ) {
      while (!$sql->eof()) {
        $tpl->PLANO_ID = $sql->result("NNUMEPLAN");
        $tpl->PLANO_NOME = ($sql->result("CDESCPLAN"));        
        $tpl-> block("ITEM_PLANO");	
        $sql->next();
      }
      $tpl->block("PLANO");
    } else
      $plano = -1;
  } else
    $plano = -1;
  
  /** Select estado **/
  $txt = "SELECT DISTINCT TRIM(CESTAPRES) CESTAPRES 
            FROM FINPRES 
           WHERE CCREDPRES IN ('S','O')           
              AND CESTAPRES IS NOT NULL
            UNION
           SELECT DISTINCT TRIM(CESTAEPRES) CESTAPRES 
             FROM HSSEPRES,FINPRES
            WHERE HSSEPRES.CSITUEPRES = 'S'
              AND HSSEPRES.CTIPOEPRES = 'A'
              AND HSSEPRES.CESTAEPRES IS NOT NULL
              AND HSSEPRES.NNUMEPRES = FINPRES.NNUMEPRES
              AND CCREDPRES IN ('S','O')
            ORDER BY 1";
             
  $sql->executeQuery($txt);
    
  while (!$sql->eof()) {
    $tpl->ESTADO_ID = $sql->result("CESTAPRES");
    $tpl->ESTADO_NOME = $func->nomeEstado($sql->result("CESTAPRES"));
    
    if ($estado == $sql->result("CESTAPRES"))
      $tpl->REDE_ESTADO = 'selected';
    else
      $tpl->REDE_ESTADO = '';
     
    $tpl-> block("REDE_ITEM_ESTADO");
    $sql->next();
  }  
   
  /** Select especialidade **/   
  $sql->clear();
  $txt = "SELECT CNOMEESPEC,NNUMEESPEC FROM HSSESPEC WHERE CSITUESPEC = 'A' AND CPUBLESPEC = 'S' ORDER BY CNOMEESPEC";
  $sql->executeQuery($txt);
  
  while (!$sql->eof()) {
    $tpl->ESPECIALIDADE_ID = $sql->result("NNUMEESPEC");
    $tpl->ESPECIALIDADE_DESCRICAO = $sql->result("CNOMEESPEC");
    
    if ($especialidade == $sql->result("NNUMEESPEC"))
      $tpl->REDE_ESPECIALIDADE = 'selected';
    else
      $tpl->REDE_ESPECIALIDADE = '';
     
    $tpl-> block("REDE_ITEM_ESPECIALIDADE");
    $sql->next();
  }
  
  $nome_prestador = strtoupper($nome_prestador);
  $tpl->SEL_NOME_PRESTADOR = $nome_prestador;
  
  $f_nome          = "";  
  $f_tipo          = "";  
  $f_cidade        = "";  
  $f_estado        = "";  
  $f_bairro        = "";  
  $f_indicavel     = "";  
  $f_cidade2       = "";  
  $f_estado2       = "";  
  $f_bairro2       = "";  
  $f_indicavel2    = "";  
  $f_especialidade = "";  
  $f_area          = "";  
  
  /** Lista ou imprime rede de atendimento **/
  if (isset($_POST['listar']) or isset($_POST['imprimir']) or ($filtros['executa'] == 'S')) {
  
    if ($plano == 0) {
      $tpl->MSG = "Informe o plano desejado e tente novamente.";
      $tpl->block("ERRO");    
    } else {

      ini_set  ("max_execution_time",60);  
      
      if (isset($_POST['imprimir'])) {
        $pdf = new PDF('P','mm','A4');
        $pdf->AliasNbPages();                      
        $pdf->Open();
        $pdf->AddPage();                
        $pdf->SetFont('Arial','',8);           
        
        $pdf->Cell(25,5,"Estado:",0,0,'R');
        
        if ($estado == '')
          $pdf->Cell(130,5,'Todos',0,1);
        else
          $pdf->Cell(130,5,$estado,0,1);
          
        $pdf->Cell(25,5,"Cidade:",0,0,'R');

        if ($cidade == '')
          $pdf->Cell(130,5,'Todas',0,1);
        else
          $pdf->Cell(130,5,$cidade,0,1);
        
        $pdf->Cell(25,5,"Bairro:",0,0,'R');

        if ($bairro == '')
          $pdf->Cell(130,5,'Todos',0,1);
        else
          $pdf->Cell(130,5,$bairro,0,1);
         
        $pdf->Cell(25,5,"Especialidade:",0,0,'R');
        
        if ($especialidade == '')
          $pdf->Cell(130,5,'Todas',0,1);
        else
          $pdf->Cell(130,5,$func->retornaNomeEspecialidade($bd,$especialidade),0,1);
                 
        $pdf->Cell(25,5,"�rea de atua��o:",0,0,'R');
        
        if ($area_prestador == '')
          $pdf->Cell(130,5,'Todas',0,1);
        else
          $pdf->Cell(130,5,$func->retornaNomeArea($bd,$area_prestador),0,1);         
                
        $pdf->Cell(190,3,' ','B',1);
        
      }
      
      $sql->clear();
      $txt = "SELECT NNUMEREDEA FROM HSSREDEA WHERE ROWNUM = 1 ";
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0 ) {
      
        $sql->clear();    
        $txt = "SELECT HSSUSUA.NNUMEPLAN FROM HSSREDCO,HSSUSUA
                 WHERE HSSREDCO.NNUMEUSUA = :usuario 
                   AND HSSREDCO.NNUMEUSUA = HSSUSUA.NNUMEUSUA ";  

        if (isset($_SESSION['id_titular']))                   
          $sql->addParam(":usuario",$_SESSION['id_titular']);
        else          
          $sql->addParam(":usuario",0);
          
        $sql->executeQuery($txt);

        if ($sql->count() > 0) {
          $tem_rede = "usuario";        
        }
        else {            
          $sql->clear();    
          $txt = "SELECT HSSTITU.NNUMEPLAN
                    FROM HSSREDCO,HSSTITU
                   WHERE HSSREDCO.NNUMETITU = :contrato
                     AND NVL(HSSREDCO.NNUMESETOR,-1) = :locacao 
                     AND HSSREDCO.NNUMETITU = HSSTITU.NNUMETITU "; 
          $sql->addParam(":contrato",$_SESSION['id_contrato']);
          $sql->addParam(":locacao",$_SESSION['id_locacao']);    
          $sql->executeQuery($txt);

          if ($sql->count() > 0 ) {
            $tem_rede = "contrato";           
          }
          else {
            $sql->clear();    
            $txt = "SELECT HSSREDPL.NNUMEPLAN 
                      FROM HSSUSUA,HSSREDPL 
                     WHERE HSSUSUA.NNUMEUSUA = :usuario
                       AND HSSUSUA.NNUMEPLAN = HSSREDPL.NNUMEPLAN"; 
                       
            if (isset($_SESSION['id_titular']))                   
              $sql->addParam(":usuario",$_SESSION['id_titular']);
            else          
              $sql->addParam(":usuario",0);
                       
            $sql->executeQuery($txt);

            if ($sql->count() > 0 ) {
              $tem_rede = "usuario_plano";               
            }        
            else {
              $sql->clear();    
              $txt = "SELECT HSSTITU.NNUMEPLAN FROM HSSTITU,HSSREDPL
                       WHERE NNUMETITU = :contrato
                         AND HSSTITU.NNUMEPLAN = HSSREDPL.NNUMEPLAN"; 
                         
              $sql->addParam(":contrato",$_SESSION['id_contrato']);
              $sql->executeQuery($txt);

              if ($sql->count() > 0 ) {
                $tem_rede = "contrato_plano";           
              }
              else {
                $sql->clear();    
                $txt = "SELECT DISTINCT NNUMEPLAN FROM HSSREDPL,HSSPREDE 
                         WHERE HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA"; 
                $sql->executeQuery($txt);

                if ($sql->count() > 0 )
                  $tem_rede = "plano";             
              }
            }
          }
        }
      }
      else
        $tem_rede = "sem restricao";
      
      $sql->clear(); 
      $sql_tot = new Query();
      
      $f_credenciado = " WHERE FINPRES.CCREDPRES IN ('S','O') "; 

      if ($tipo_prestador <> '') {
        $f_tipo = "   AND FINPRES.CGRUPPRES = :tipo ";
        $sql->addParam(":tipo",$tipo_prestador);
        $sql_tot->addParam(":tipo",$tipo_prestador);
      }
      
      if ($cidade <> '') {
        $f_cidade  = "   AND FINPRES.CCIDAPRES = :cidade ";
        $f_cidade2 = "   AND PRES.CCIDAEPRES = :cidade ";    
        $sql->addParam(":cidade",$cidade);
        $sql_tot->addParam(":cidade",$cidade);
      }  
      
      if ($estado <> '') {
        $f_estado  = "   AND FINPRES.CESTAPRES = :estado ";
        $f_estado2 = "   AND PRES.CESTAEPRES = :estado ";    
        $sql->addParam(":estado",$estado);
        $sql_tot->addParam(":estado",$estado);
      }  
            
      if ($bairro <> '') {
        $f_bairro  = "   AND FINPRES.CBAIRPRES = :bairro ";
        $f_bairro2 = "   AND PRES.CBAIREPRES = :bairro ";    
        $sql->addParam(":bairro",$bairro);
        $sql_tot->addParam(":bairro",$bairro);
      } 
      
      if ($especialidade <> '') {
        $f_especialidade = "   AND HSSESPRE.NNUMEESPEC = :especialidade ";
        $sql->addParam(":especialidade",$especialidade);    
        $sql_tot->addParam(":especialidade",$especialidade);    
      }
      
      $f_publica = "   AND HSSESPRE.CPUBLESPRE = 'S' ";  
        
      if ($area_prestador <> '') {
        $f_area = "   AND HSSAAPRE.NNUMEAATUA = :area ";         
        $sql->addParam(":area",$area_prestador);        
        $sql_tot->addParam(":area",$area_prestador);        
      }    
      
      if ($indicavel <> '') {
        $f_indicavel  = "   AND FINPRES.CINDIPRES = :indicavel ";
        $f_indicavel2 = "   AND HSSEPRES.CINDIEPRES = :indicavel ";    
        $sql->addParam(":indicavel",$indicavel);     
        $sql_tot->addParam(":indicavel",$indicavel);     
      }
      
      if ($nome_prestador <> '') {
        $f_nome  = "   AND (FINPRES.CNOMEPRES LIKE :nome OR FINPRES.CFANTPRES LIKE :nome)";
        $sql->addParam(":nome",$nome_prestador."%");     
        $sql_tot->addParam(":nome",$nome_prestador."%");     
      }
      
      $f_rede = "";
      
      if ($tem_rede == "usuario") {
        $f_rede = "   AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSREDCO,HSSPREDE 
                                                 WHERE NNUMEUSUA = :usuario 
                                                   AND HSSREDCO.NNUMEREDEA = HSSPREDE.NNUMEREDEA) "; 
        $sql->addParam(":usuario",$_SESSION['id_titular']);                                             
        $sql_tot->addParam(":usuario",$_SESSION['id_titular']);                                             
      } 
      else if ($tem_rede == "contrato") {
        $f_rede = " AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSREDCO,HSSPREDE 
                                       WHERE NNUMETITU = :contrato 
                                         AND HSSREDCO.NNUMEREDEA = HSSPREDE.NNUMEREDEA) "; 
        $sql->addParam(":contrato",$_SESSION['id_contrato']);                                                                                
        $sql_tot->addParam(":contrato",$_SESSION['id_contrato']);                                                                                
      }
      elseif ($tem_rede == "usuario_plano") {
        $f_rede = " AND FINPRES.NNUMEPRES IN (SELECT NNUMEPRES FROM HSSUSUA,HSSREDPL,HSSPREDE 
                                               WHERE HSSUSUA.NNUMEUSUA = :usuario 
                                                 AND HSSUSUA.NNUMEPLAN = HSSREDPL.NNUMEPLAN 
                                                 AND HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA) "; 
        $sql->addParam(":usuario",$_SESSION['id_titular']);                                                                                                
        $sql_tot->addParam(":usuario",$_SESSION['id_titular']);                                                                                                
      }
      elseif ($tem_rede == "contrato_plano") {
        $f_rede = " AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSTITU,HSSREDPL,HSSPREDE 
                                       WHERE HSSTITU.NNUMETITU = :contrato 
                                         AND HSSTITU.NNUMEPLAN = HSSREDPL.NNUMEPLAN 
                                         AND HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA) "; 
        $sql->addParam(":contrato",$_SESSION['id_contrato']);                                                                                                                     
        $sql_tot->addParam(":contrato",$_SESSION['id_contrato']);                                                                                                                     
      }      
                 
      $quantidade = 0;    

      $tot = "SELECT MAX(REGISTRO) TOTAL FROM (
              SELECT ROWNUM REGISTRO, A.* FROM ( ";	  
              
      $f_plano =  "AND CSITUPLAN = 'A' ";
	  
      if (($tem_rede == "plano") and ($plano > 0)) {  
        $tot .= "SELECT DISTINCT CDESCPLAN,CNOMEPRES,FINPRES.NNUMEPRES,CPESSPRES, CFANTPRES,CCGC_PRES,CCONSPRES,NCRM_PRES,CUFCRPRES,CGRUPPRES,
                        '' CHOMEPRES,NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) NRGMSPLAN,HSSPLAN.CSITUPLAN
                   FROM FINPRES,HSSPREDE,HSSREDPL,HSSPLAN ".
                $f_credenciado.
                $f_nome.            
                $f_tipo.
                "   AND FINPRES.NNUMEPRES = HSSPREDE.NNUMEPRES
                    AND HSSPREDE.NNUMEREDEA = HSSREDPL.NNUMEREDEA
                    AND HSSREDPL.NNUMEPLAN = HSSPLAN.NNUMEPLAN ". 
                    $f_plano.
                "   AND HSSPLAN.NNUMEPLAN = :id_plano
                    AND FINPRES.NNUMEPRES IN (SELECT PRES.NNUMEPRES FROM FINPRES PRES
                                               WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES ".
                                              $f_cidade.
                                              $f_estado.
                                              $f_bairro.
                                              $f_indicavel.
                "                              UNION ALL
                                              SELECT PRES.NNUMEPRES FROM HSSEPRES PRES
                                               WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES
                                                 AND PRES.CSITUEPRES = 'S'
                                                 AND PRES.CTIPOEPRES = 'A' ".
                                              $f_cidade2.
                                              $f_estado2.
                                              $f_bairro2.
                                              $f_indicavel2.
                "                             )
                    AND FINPRES.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES
                                                FROM HSSESPRE,HSSAAPRE
                                               WHERE HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES ".
                                              $f_especialidade.
                                              $f_publica.
                "                                AND HSSESPRE.NNUMEPRES = HSSAAPRE.NNUMEPRES(+)
                                                 AND HSSESPRE.NNUMEESPEC = HSSAAPRE.NNUMEESPEC(+) ".
                                              $f_area.
                "                             ) ";
                
        $sql_tot->addParam(":id_plano",$plano);                                   
      } else {       
        $tot .= "SELECT '' PLANO,CFANTPRES CNOMEPRES,NNUMEPRES,CPESSPRES,CCGC_PRES,CCONSPRES,NCRM_PRES,CUFCRPRES,CGRUPPRES,";          
        $tot .= "       '' CHOMEPRES,'' NRGMSPLAN,'A' CSITUPLAN
                   FROM FINPRES ".
                $f_credenciado.
                $f_nome.            
                $f_tipo.
                "   AND NNUMEPRES IN (SELECT PRES.NNUMEPRES FROM FINPRES PRES
                                       WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES ".
                                      $f_cidade.
                                      $f_estado.
                                      $f_bairro.
                                      $f_indicavel.
                "                      UNION
                                     SELECT PRES.NNUMEPRES FROM HSSEPRES PRES
                                      WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES
                                        AND PRES.CSITUEPRES = 'S'
                                        AND PRES.CTIPOEPRES = 'A' ".
                                     $f_cidade2.
                                     $f_estado2.
                                     $f_bairro2.
                                     $f_indicavel2.
               "                    )
                   AND NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES
                                       FROM HSSESPRE,HSSAAPRE
                                      WHERE HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES ".
                                     $f_especialidade.
                                     $f_publica.
               "                        AND HSSESPRE.NNUMEPRES = HSSAAPRE.NNUMEPRES(+)
                                        AND HSSESPRE.NNUMEESPEC = HSSAAPRE.NNUMEESPEC(+) ".
                                     $f_area.
               "                    ) ".
                $f_rede;              
      }
      
      $tot .= " ORDER BY 1,2) A )";    
           
      $sql_tot->executeQuery($tot);       
      $total_paginas = $sql_tot->result("TOTAL");
      
      $txt = "SELECT * FROM (
              SELECT ROWNUM REGISTRO, A.* FROM ( ";
              
      if (($tem_rede == "plano") and ($plano > 0)) {   
        $txt .= "SELECT DISTINCT CDESCPLAN,CNOMEPRES,FINPRES.NNUMEPRES,CPESSPRES, CFANTPRES,CCGC_PRES,CCONSPRES,NCRM_PRES,CUFCRPRES,CGRUPPRES,
                        CHOMEPRES,NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) NRGMSPLAN,HSSPLAN.CSITUPLAN
                   FROM FINPRES,HSSPREDE,HSSREDPL,HSSPLAN ".
                $f_credenciado.
                $f_nome.            
                $f_tipo.
                "   AND FINPRES.NNUMEPRES = HSSPREDE.NNUMEPRES
                    AND HSSPREDE.NNUMEREDEA = HSSREDPL.NNUMEREDEA
                    AND HSSREDPL.NNUMEPLAN = HSSPLAN.NNUMEPLAN ". 
                    $f_plano.
                "   AND HSSPLAN.NNUMEPLAN = :id_plano
                    AND FINPRES.NNUMEPRES IN (SELECT PRES.NNUMEPRES FROM FINPRES PRES
                                               WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES ".
                                              $f_cidade.
                                              $f_estado.
                                              $f_bairro.
                                              $f_indicavel.
                "                              UNION ALL
                                              SELECT PRES.NNUMEPRES FROM HSSEPRES PRES
                                               WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES
                                                 AND PRES.CSITUEPRES = 'S'
                                                 AND PRES.CTIPOEPRES = 'A' ".
                                              $f_cidade2.
                                              $f_estado2.
                                              $f_bairro2.
                                              $f_indicavel2.
                "                             )
                    AND FINPRES.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES
                                                FROM HSSESPRE,HSSAAPRE
                                               WHERE HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES ".
                                              $f_especialidade.
                                              $f_publica.
                "                                AND HSSESPRE.NNUMEPRES = HSSAAPRE.NNUMEPRES(+)
                                                 AND HSSESPRE.NNUMEESPEC = HSSAAPRE.NNUMEESPEC(+) ".
                                              $f_area.
                "                             ) ";
                
        $sql->addParam(":id_plano",$plano);                                   
      } else {       
        $txt .= "SELECT '' PLANO,CFANTPRES CNOMEPRES,NNUMEPRES,CPESSPRES,CCGC_PRES,CCONSPRES,NCRM_PRES,CUFCRPRES,CGRUPPRES,";
          
        $txt .= "       CHOMEPRES,'' NRGMSPLAN,'A' CSITUPLAN
                   FROM FINPRES ".
                $f_credenciado.
                $f_nome.            
                $f_tipo.
                "   AND NNUMEPRES IN (SELECT PRES.NNUMEPRES FROM FINPRES PRES
                                       WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES ".
                                      $f_cidade.
                                      $f_estado.
                                      $f_bairro.
                                      $f_indicavel.
                "                      UNION
                                     SELECT PRES.NNUMEPRES FROM HSSEPRES PRES
                                      WHERE PRES.NNUMEPRES = FINPRES.NNUMEPRES
                                        AND PRES.CSITUEPRES = 'S'
                                        AND PRES.CTIPOEPRES = 'A' ".
                                     $f_cidade2.
                                     $f_estado2.
                                     $f_bairro2.
                                     $f_indicavel2.
               "                    )
                   AND NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES
                                       FROM HSSESPRE,HSSAAPRE
                                      WHERE HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES ".
                                     $f_especialidade.
                                     $f_publica.
               "                        AND HSSESPRE.NNUMEPRES = HSSAAPRE.NNUMEPRES(+)
                                        AND HSSESPRE.NNUMEESPEC = HSSAAPRE.NNUMEESPEC(+) ".
                                     $f_area.
               "                    ) ".
                $f_rede;              
      }
      
      $txt .= " ORDER BY 1,2) A )";    
                 
      if (!isset($_POST['imprimir'])) {
        $txt .= "WHERE REGISTRO BETWEEN :inicio + 1 AND :inicio + :qtde";
        $sql->addParam(":inicio",$inicio);                     
        $sql->addParam(":qtde",$registros);         
      }
           
      $sql->executeQuery($txt); 

      if ($sql->count() > 0) {
          
        $paginas = ceil($total_paginas/$registros);
        
        if ($paginas > 1) {
        
          $tpl->PAGINA = 0;
          $tpl->DESC_PAGINA = "&laquo;";

          if ($pg == 0)
            $tpl->PAGINA_CLASSE = "active";
          else
            $tpl->PAGINA_CLASSE = "";
            
          $tpl->block("LINHA_PAGINACAO");
          
          if ($paginas > 2) {
          
            if ($paginas > 10) {
              if ($pg <= 5) {
                $pg3 = 0;
                $pg2 = 10;
              }
              else {
                $pg3 = $pg - 4;
                $pg2 = $pg + 6;
              }
                
            }
            else {
              $pg3 = 0;
              $pg2 = $paginas;
            }
          
            for($i = $pg3;$i < $pg2;$i++) { 

              $tpl->PAGINA = $i;
              $tpl->DESC_PAGINA = $i+1;
              
              if ($pg == $i)
                $tpl->PAGINA_CLASSE = "active";
              else
                $tpl->PAGINA_CLASSE = "";
            
              $tpl->block("LINHA_PAGINACAO");
            }
          }
                   
                   
          $tpl->PAGINA = $paginas-1;
          $tpl->PAGINA_ATUAL = $pg+1;
          $tpl->PAGINA_TOTAL = $paginas;
          $tpl->DESC_PAGINA = "&raquo;";
          
          if ($pg == $paginas -1)
            $tpl->PAGINA_CLASSE = "active";
          else
            $tpl->PAGINA_CLASSE = "";
            
          $tpl->block("LINHA_PAGINACAO");          
        
          $tpl->block("PAGINACAO");
        }      
                            
        $desc_plano = "";
        while (!$sql->eof()) {
          
          if ($desc_plano <> $sql->result("CDESCPLAN")) {
            $tpl->DESCRICAO_PLANO = $sql->result("CDESCPLAN");
            $tpl->REGISTRO_PLANO = $formata->formataRegistroAns($sql->result("NRGMSPLAN"));
          
            if ($sql->result("CSITUPLAN") == "C") {
              $tpl->SITUACAO_PLANO = "Cancelado";
              $tpl->COR_SITUACAO = "red";
            } else if ($sql->result("CSITUPLAN") == "S") {
              $tpl->SITUACAO_PLANO = "Ativo (comercializa��o suspensa)";
              $tpl->COR_SITUACAO = "navy";
            } else {
              $tpl->SITUACAO_PLANO = "Ativo";
              $tpl->COR_SITUACAO = "navy";
            }
             
            $desc_plano = $sql->result("CDESCPLAN");
            
            $tpl->block("PLANOS");
          }        

          $nome_prestador2 = $sql->result("CNOMEPRES");   
		  
		      $sql_docs = new Query($bd);
          $txt_doc = "SELECT CDESCPGRAD FROM HSSPGRAD
                       WHERE NNUMEPRES = :prestador
                       ORDER BY CDESCPGRAD	";          
          $sql_docs->addParam(":prestador",$sql->result("NNUMEPRES"));           
          $sql_docs->executeQuery($txt_doc);
		
		      $tipo_doc = '';
          
          while (!$sql_docs->eof()) {			
            if ($sql_docs->result("CDESCPGRAD") == "AP")
              $tipo_doc = '<img src="../comum/img/documentacao/ap.png" title="Padr�o nacional de qualidade"/>'.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "AD")
              $tipo_doc = '<img src="../comum/img/documentacao/ad.png" title="Padr�o nacional de qualidade"/>'.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "AO")
              $tipo_doc = '<img src="../comum/img/documentacao/ao.png" title="Padr�o nacional de qualidade"/>'.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "AC")
              $tipo_doc = '<img src="../comum/img/documentacao/ac.png" title="Padr�o internacional de qualidade"/>'.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "AI")
              $tipo_doc = '<img src="../comum/img/documentacao/ai.png" title="Padr�o internacional de qualidade"/>'.$tipo_doc;		
            else if ($sql_docs->result("CDESCPGRAD") == "N")
              $tipo_doc = '<img src="../comum/img/documentacao/n.png" title="Comunica��o de eventos adversos"/> '.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "P")
              $tipo_doc = '<img src="../comum/img/documentacao/p.png" title="Profissional com especializa��o"/> '.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "R")
              $tipo_doc = '<img src="../comum/img/documentacao/r.png" title="Profissional com resid�ncia"/> '.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "E")
              $tipo_doc = '<img src="../comum/img/documentacao/e.png" title="T�tulo de especialista"/> '.$tipo_doc;
            else if ($sql_docs->result("CDESCPGRAD") == "Q")
              $tipo_doc = '<img src="../comum/img/documentacao/q.png" title="Qualidade monitorada"/> '.$tipo_doc;
            else 
  		        $tipo_doc = ''; 
          	  
		        $sql_docs->next();			  		 
		      }  
				  
		      $tpl->TIPO_DOCUMENTO = $tipo_doc;
		   
          if ($sql->result('CPESSPRES') == 'J') {
            $tpl->RESULTADO_RAZAO = $sql->result('CNOMEPRES');
            $tpl->RESULTADO_CNPJ = $sql->result('CCGC_PRES');
            $tpl->block("MOSTRA_RAZAO");
            $nome_prestador2 = $sql->result('CFANTPRES');
            
            //if ($sql->result('CCGC_PRES') <> '')
            //  $nome_prestador2 .= ' ( CNPJ: '.$sql->result('CCGC_PRES') . ')';
              
          } else {          
            if (($sql->result('CCONSPRES') <> '') and
                ($sql->result('NCRM_PRES') <> '') and
                ($sql->result('CUFCRPRES') <> ''))
              $nome_prestador2 .= ' ('.$sql->result('CCONSPRES'). '-'. $sql->result('CUFCRPRES') .' '. $sql->result('NCRM_PRES') . ')';
          }
                  
          if (!isset($_POST['imprimir']))               
            $tpl->RESULTADO_NOME_PRESTADOR = $nome_prestador2;
          else {
            $pdf->SetFont('Arial','B',6);        
            $pdf->Cell(100,5,$nome_prestador2,'',1);
            $pdf->SetFont('Arial','',6);         
          }
              
          $tpl->RESULTADO_TIPO_ESTABELECIMENTO = $func->tipoPrestador($sql->result('CGRUPPRES'));
          
          if ($sql->result("CHOMEPRES") <> '') {
            $tpl->RESULTADO_HOMEPAGE = $sql->result("CHOMEPRES"); 
            $tpl->block("HOMEPAGE");
          }        
              
          /* Especialidades */      
          $sql2 = new Query($bd);
          
          $f_publica = "   AND HSSESPRE.CPUBLESPRE = 'S' ";  
            
          if ($especialidade > 0) {
            $f_especialidade = "   AND HSSESPRE.NNUMEESPEC = :especialidade ";
            $sql2->addParam(":especialidade",$especialidade);
          } else {
            $f_especialidade = "";
          }      

          if ($cidade <> '') {
            $f_cidade  = "   AND CCIDAPRES = :cidade ";
            $f_cidade2 = "   AND CCIDAEPRES = :cidade ";    
            $sql2->addParam(":cidade",$cidade);
          } else {
            $f_cidade = "";
            $f_cidade2 = "";        
          }  
              
          if ($estado <> '') {
            $f_estado  = "   AND CESTAPRES = :estado ";
            $f_estado2 = "   AND CESTAEPRES = :estado ";    
            $sql2->addParam(":estado",$estado);
          } else {
            $f_estado = "";
            $f_estado2 = "";        
          } 
              
          if ($bairro <> '') {
            $f_bairro  = "   AND CBAIRPRES = :bairro ";
            $f_bairro2 = "   AND CBAIREPRES = :bairro ";    
            $sql2->addParam(":bairro",$bairro);
          } else {
            $f_bairro = "";
            $f_bairro2 = "";        
          }          
          
          if ($indicavel <> '') {
            $f_indicavel  = "   AND CINDIPRES = :indicavel ";
            $f_indicavel2 = "   AND CINDIEPRES = :indicavel ";    
            $sql2->addParam(":indicavel",$indicavel);     
          } else {
            $f_indicavel = "";
            $f_indicavel2 = "";          
          }      
            
          $txt = "SELECT DISTINCT CNOMEESPEC,HSSESPRE.NNUMEESPEC
                    FROM HSSESPRE,HSSESPEC
                   WHERE HSSESPRE.NNUMEESPEC = HSSESPEC.NNUMEESPEC ".
                  $f_especialidade.
                  $f_publica.              
                 "   AND (HSSESPRE.NNUMEPRES,NVL(HSSESPRE.NNUMEEPRES,0)) IN (SELECT NNUMEPRES,0 FROM FINPRES
                                                                              WHERE NNUMEPRES = :prestador ".
                                                                             $f_cidade.
                                                                             $f_estado.
                                                                             $f_bairro.
                                                                             $f_indicavel.
                 "                                                            UNION 
                                                                             SELECT NNUMEPRES,NNUMEEPRES FROM HSSEPRES
                                                                              WHERE CSITUEPRES = 'S'
                                                                                AND CTIPOEPRES = 'A' ".
                                                                             $f_cidade2.
                                                                             $f_estado2.
                                                                             $f_bairro2.
                                                                             $f_indicavel2.                                                                            
                 "                                                              AND NNUMEPRES = :prestador)
                   ORDER BY 1 ";
            
          $sql2->addParam(":prestador",$sql->result("NNUMEPRES"));
          $sql2->executeQuery($txt);       
          
          $espec = "";
          while (!$sql2->eof()) { 
            
            $sql_areas = new Query($bd);
            $txt_areas = "SELECT CDESCAATUA FROM HSSAAPRE,HSSAATUA
                           WHERE HSSAAPRE.NNUMEPRES = :prestador
                             AND HSSAAPRE.NNUMEESPEC = :especialidade
                             AND HSSAAPRE.NNUMEAATUA = HSSAATUA.NNUMEAATUA
                           ORDER BY 1 ";          
            $sql_areas->addParam(":prestador",$sql->result("NNUMEPRES"));
            $sql_areas->addParam(":especialidade",$sql2->result("NNUMEESPEC"));
            $sql_areas->executeQuery($txt_areas);
            
            $areas = "";
            while (!$sql_areas->eof()) {
              $areas .= strTolower($sql_areas->result("CDESCAATUA")).", ";      
              $sql_areas->next();            
            }
            
            if ($areas <> '') {
              $areas = " - ".substr($areas,0,strlen($areas)-2);
              $espec = substr($espec,0,strlen($espec)-2);
              $espec .= "<br>".$sql2->result("CNOMEESPEC").$areas.", ";
            } else
              $espec .= $sql2->result("CNOMEESPEC").", ";      
              
            $sql2->next();
          }
          
          if (!isset($_POST['imprimir'])) {            
            $tpl->RESULTADO_ESPECIALIDADES = substr($espec,0,strlen($espec)-2); 
            $tpl->block("ESPECIALIDADE");
          } else {
            $pdf->SetFont('Arial','B',6);          
            $pdf->Cell(3,3,'','',0);
            $pdf->Cell(100,3,substr($espec,0,strlen($espec)-2),'',1);          
            $pdf->SetFont('Arial','',6);            
          }
           
          /* Endere�os */
          $sql4 = new Query($bd);
          $sql4->clear();
            
          $sql5 = new Query($bd);
          $sql5->clear();
          
          $txt = "SELECT CFONETLPRE FROM HSSTLPRE WHERE NNUMEPRES = :prestador";
          $sql5->addParam(":prestador",$sql->result("NNUMEPRES"));
          $sql5->executeQuery($txt);
        
          if ($cidade <> '') {
            $f_cidade  = "   AND CCIDAPRES = :cidade ";
            $f_cidade2 = "   AND CCIDAEPRES = :cidade ";    
            $sql4->addParam(":cidade",$cidade);
          } else {
            $f_cidade = "";
            $f_cidade2 = "";        
          }  
            
          if ($estado <> '') {
            $f_estado  = "   AND CESTAPRES = :estado ";
            $f_estado2 = "   AND CESTAEPRES = :estado ";    
            $sql4->addParam(":estado",$estado);
          } else {
            $f_estado = "";
            $f_estado2 = "";        
          } 
            
          if ($bairro <> '') {
            $f_bairro  = "   AND CBAIRPRES = :bairro ";
            $f_bairro2 = "   AND CBAIREPRES = :bairro ";    
            $sql4->addParam(":bairro",$bairro);
          } else {
            $f_bairro = "";
            $f_bairro2 = "";        
          }            
        
          if ($indicavel <> '') {
            $f_indicavel  = "   AND CINDIPRES = :indicavel ";
            $f_indicavel2 = "   AND CINDIEPRES = :indicavel ";    
            $sql4->addParam(":indicavel",$indicavel);     
          } else {
            $f_indicavel = "";
            $f_indicavel2 = "";          
          }

          if ($plano > 0) {
            $f_rede = "   AND HSSEPRES.NNUMEEPRES IN (SELECT HSSREDEN.NNUMEEPRES FROM HSSREDEN,HSSREDPL
                                                       WHERE HSSREDEN.NNUMEREDEA = HSSREDPL.NNUMEREDEA
                                                         AND HSSREDPL.NNUMEPLAN = :plano) ";     
            $sql4->addParam(":plano",$plano);     
          }
          else
            $f_rede = "";
          
          $txt = "SELECT CENDEPRES,CCIDAPRES,CBAIRPRES,CESTAPRES,CFONEPRES,CDESCTLOGR,CNENDPRES,CREFEPRES,CCEP_PRES
                    FROM FINPRES,HSSTLOGR
                   WHERE NNUMEPRES = :prestador ".
                  $f_cidade.
                  $f_estado.
                  $f_bairro.
                  $f_indicavel.               
                 "   AND FINPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)
                   UNION
                  SELECT CENDEEPRES,CCIDAEPRES,CBAIREPRES,CESTAEPRES,CFONEEPRES,CDESCTLOGR,CNENDEPRES,CREFEEPRES,CCEP_EPRES
                    FROM HSSEPRES,HSSTLOGR
                   WHERE HSSEPRES.NNUMEPRES = :prestador
                     AND HSSEPRES.CSITUEPRES = 'S'
                     AND HSSEPRES.CTIPOEPRES = 'A' ".
                  $f_cidade2.
                  $f_estado2.
                  $f_bairro2.
                  $f_indicavel2.
                  $f_rede.
                 "   AND HSSEPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+) ";
          
          $sql4->addParam(":prestador",$sql->result("NNUMEPRES"));
          $sql4->executeQuery($txt);               
          
          if ($sql4->count() > 0) {
            $endereco = '';
            while (!$sql4->eof()) {

              $tel = '';
              while(!$sql5->eof()) {
                $tel .= $formata->formataTelefone($sql5->result('CFONETLPRE')). ' | ';			
                $sql5->next();
              }
              
              $tel = substr($tel,0,strlen($tel)-3);
              
              if (!isset($_POST['imprimir'])) {          
                if ($sql4->result("CDESCTLOGR") <> '')
                  $endereco = $sql4->result("CDESCTLOGR")." ";
                  
                $endereco .= $sql4->result("CENDEPRES");
                
                if($sql4->result("CNENDPRES") <> '')
                  $endereco .= ", " . $sql4->result("CNENDPRES");
                
                if($sql4->result("CREFEPRES") <> '')
                  $endereco .= " - " . $sql4->result("CREFEPRES");
                  
                $tpl->RESULTADO_ENDERECO = $endereco;                            
                $tpl->RESULTADO_BAIRRO = $sql4->result("CBAIRPRES");
                $tpl->RESULTADO_CIDADE = $sql4->result("CCIDAPRES");
                $tpl->RESULTADO_ESTADO = $sql4->result("CESTAPRES");
                $tpl->RESULTADO_CEP    = $sql4->result("CCEP_PRES");
                
                if ($tel <> '') 
                  $tpl->RESULTADO_TELEFONE = $formata->formataTelefone($sql4->result("CFONEPRES")) .' | ' .$tel; 
                else
                  $tpl->RESULTADO_TELEFONE = $formata->formataTelefone($sql4->result("CFONEPRES")); 
                  
                $tpl->block("RESULTADO_LINHA_ENDERECO");
              } else {
                $pdf->Ln(1);                         
                $pdf->Cell(3,3,'','',0);
                if ($sql4->result("CDESCTLOGR") <> '')
                  $endereco = $sql4->result("CDESCTLOGR")." ";
                  
                $endereco .= $sql4->result("CENDEPRES");
                
                if($sql4->result("CNENDPRES") <> '')
                  $endereco .= ", " . $sql4->result("CNENDPRES");
                
                if($sql4->result("CREFEPRES") <> '')
                  $endereco .= " - " . $sql4->result("CREFEPRES");
                 
                $pdf->Cell(100,3,$endereco,'',1);              
                $pdf->Cell(3,3,'','',0);                
                $pdf->Cell(100,3,$sql4->result("CBAIRPRES")." - ".$sql4->result("CCIDAPRES")." - ".$sql4->result("CESTAPRES"),'',1);
                $pdf->Cell(3,3,'','',0);              
                $pdf->Cell(100,3,$sql4->result("CFONEPRES"),'',1);
                $pdf->Ln(1);              
              }
              
              $sql4->next();
            }
          }

          if (!isset($_POST['imprimir'])) {         
            $tpl->block("RESULTADO_LINHA");
          }
            
          $quantidade++;  		
                
          $sql->next();
        }
        
        if (isset($_POST['imprimir'])) {
          $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
          $pdf->Output($file,'F');

          echo "<HTML><SCRIPT>window.open('$file');</SCRIPT></HTML>";        
        }       
      } else {
        $tpl->MSG = "Sem resultado";
        $tpl->block("ERRO");  
      }
        
      ini_set  ("max_execution_time",30);          
    }
  }
  
  $tpl->SEL_NOME_PRESTADOR  = $nome_prestador;
  $tpl->SEL_TIPO_PRESTADOR  = $tipo_prestador; 
  $tpl->SEL_ESTADO          = $estado;  
  $tpl->SEL_CIDADE          = $cidade;
  $tpl->SEL_BAIRRO          = $bairro;  
  $tpl->SEL_ESPECIALIDADE   = $especialidade;
  $tpl->SEL_AREA_PRESTADOR  = $area_prestador;
  $tpl->SEL_INDICAVEL       = $indicavel;
  $tpl->SEL_PLANO_PRESTADOR = $plano;
 
  if ($_SESSION['sistema'] <> 'Rede') {
    $tpl->block("MOSTRA_MENU");
  }
  
  $bd->close();
  $tpl->show();     

?>