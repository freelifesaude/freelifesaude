<?php       
  class MySQL {
    private $connect;  
    
    public function MySQL() {
      try {
        $this->connect = new PDO('mysql:host=172.21.11.201;port=3306;dbname=webger', 'raphael', 'intracorp');
      } catch (PDOException $e) {
        echo "<p>N&atilde;o foi possivel conectar-se ao servidor de Banco de dados.</p>\n".
             "<p><strong>Erro: " . htmlentities($e->getMessage()) . "</strong></p>\n";
        exit();
      }          }   

    public function conn() {
      return $this->connect;
    }        
    
    public function close() {
      $this->connect = null;
    }       
  }
?>