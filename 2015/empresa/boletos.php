<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $bd =  new Oracle();  
    
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/boletos.html");
  
  if ($seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMEBOLETO",true))
    $boleto = 'S';
  else
    $boleto = 'N';  
    
  if ($boleto == "N")
    $_SESSION['titulo'] = "RELA��O DE FATURAS";
  else
    $_SESSION['titulo'] = "RELA��O DE FATURAS E BOLETOS";  
  
  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  $boleto_vencido  = $seg->permissaoOutros($bd,"WEBEMPRESAIMPRIMEBOLETOVENCIDO",false);
  $atualiza_boleto = $seg->permissaoOutros($bd,"WEBEMPRESAATUALIZABOLETO",false);    
  $mostra_boleto   = 'S';
       
  if($_SESSION['apelido_operadora'] == 'lifeSul') {
    $sql->clear();
    $txt = "SELECT COUNT(*) QTD
              FROM HSSPAGA
             WHERE HSSPAGA.NNUMETITU = :contrato
               AND SYSDATE >= HSSPAGA.DVENCPAGA + 60
               AND CPAGOPAGA = 'N'
               AND HSSPAGA.DCANCPAGA IS NULL ";
               
    if ($_SESSION['logando'] == 'PFF') {
      $txt .= " AND HSSPAGA.NNUMEUSUA = :titular ";
      $sql->addParam(":titular",$titular);
    } else
      $txt .= " AND HSSPAGA.NNUMEUSUA IS NULL ";

    $sql->executeQuery($txt);
  
    if ($sql->count() > 0) 
      $mostra_boleto = 'N';
  }
  
  $msg = "";
  if ($boleto_vencido) {
    $msg = "Clicando no bot�o imprimir boleto voc� pode obter a impress�o das segundas vias.
            Est�o dispon�veis para impress�o todos os boletos em aberto.";
  } else if ($boleto == 'S') {
    $msg = "Clicando no bot�o imprimir boleto, voc� pode obter a impress�o das segundas vias.
            Est�o dispon�veis para impress�o os boletos ainda n�o vencidos e em aberto. Em caso de exist�ncia de cobran�a em aberto vencida,
            dirija-se � Operadora para a devida regulariza��o.";
  }
  
  if ($atualiza_boleto)
    $msg .= "<br><br><font color='red'>Caso o boleto esteja vencido h� menos de 60 dias o sistema ir� emitir a segunda via com vencimento e o valor de pagamento para o dia de hoje.
             Qualquer d�vida entre em contato com a Operadora.
             </font>";  

  $msg .= "<br><br>
           H� um espa�o de tempo entre o pagamento efetuado na rede banc�ria e a passagem dessa informa��o � empresa. Pode acontecer de
           mensalidades que j� foram quitadas ainda n�o terem sido baixadas no sistema. Neste caso desconsidere tal informa��o.";
           
  $tpl->MENSAGEM_ORIENTACAO = $msg; 
  
  if ($boleto_vencido) 
    $tpl->BOLETO_VENCIDO  = 'S';
  else
    $tpl->BOLETO_VENCIDO  = 'N';
  
  $tpl->MOSTRA_BOLETO   = $mostra_boleto; 
  
  if ($boleto_vencido)  
    $tpl->ATUALIZA_BOLETO = 'S'; 
  else
    $tpl->ATUALIZA_BOLETO = 'N'; 

  $tpl->BOLETO            = $boleto; 
    
  $tpl->block("MOSTRA_MSG"); 
  
  $sql_ano = new Query($bd);  
  $txt = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'YYYY')) ANO FROM DUAL";
  $sql_ano->executeQuery($txt);

  $ano  = $sql_ano->result("ANO");
  $ano2 = $sql_ano->result("ANO") - 5;
  
  while ($ano >= $ano2) {
    $tpl->ANO = $ano;
    $tpl->block("ANOS");
    $ano = $ano - 1;
  }  
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     

?>