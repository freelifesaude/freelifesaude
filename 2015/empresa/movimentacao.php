<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();  
  
  $_SESSION['titulo'] = "RELA��O DE MOVIMENTA��O";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","movimentacao.htm");
       
  if ($_SESSION['apelido_operadora'] == "sampes")        
    $situacao = '1';
  else
    $situacao = '0';

  $operacao    = '0';
  $locacao     = $_SESSION['id_locacao'];
  $datainicial = '';
  $datafinal   = '';
    
  if (isset($_POST['situacao']))
    $situacao = htmlentities($_POST['situacao']);
    
  if (isset($_POST['operacao']))
    $operacao = htmlentities($_POST['operacao']);
    
  if (isset($_POST['locacao']))
    $locacao = htmlentities($_POST['locacao']);
      
  if (isset($_POST['datainicial']))
    $datainicial = htmlentities($_POST['datainicial']);    

  if (isset($_POST['datafinal']))
    $datafinal = htmlentities($_POST['datafinal']);
  
  if ($_SESSION['apelido_operadora'] == "UnimedLestePaulista") {
    $formato = array('0' => "-- Independente --",
                     '1' => "Inclusao",
                     '2' => "Cancelamento");
	} else {
    $formato = array('0' => "-- Independente --",
                     '1' => "Inclusao",
                     '2' => "Cancelamento",
                     '3' => "Altera��o",
                     '4' => "Inclus�o e cancelamento");
	}	
	
  $i = 0;
  foreach($formato as $valor => $flag){
	  if($_SESSION['apelido_operadora'] == 'clinipam'){
	    if(($valor <> 0) and ($valor <> 4)){
	  
	    $tpl->V_OPERACAO = $valor;
	    $tpl->F_OPERACAO = $flag;
		$tpl->OPERACAO_X = "OPERACAO_" . $i;
		$tpl->block('CB_OPERACAO');
		}
		
	  }
	  else {
		$tpl->V_OPERACAO = $valor;
	    $tpl->F_OPERACAO = $flag;
		$tpl->OPERACAO_X = "OPERACAO_" . $i;   
$tpl->block('CB_OPERACAO');
	  }
	  $i++;
	  
  }
  
 /* 
  <option value='0' {OPERACAO_0}>-- Independente --</option>
                      <option value='1' {OPERACAO_1}>Inclus�o</option>
                      <option value='2' {OPERACAO_2}>Cancelamento</option>
                      <option value='3' {OPERACAO_3}>Altera��o</option>
                      <option value='4' {OPERACAO_4}>Inclus�o e cancelamento</option>
  */
  
  if ($_SESSION['id_locacao'] == -1) {  
    $sql = new Query($bd);
    $txt = "SELECT NNUMESETOR, CNOMESETOR 
              FROM HSSTITU,HSSSETOR
             WHERE HSSTITU.NNUMETITU = :contrato                              
               AND HSSTITU.NNUMEEMPR = HSSSETOR.NNUMEEMPR
               AND NVL(CSITUSETOR,'A') = 'A'";
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
    
    if ($sql->count() > 0) {
      while (!$sql->eof()) {
      
        if ($sql->result("NNUMESETOR") == $locacao)
          $tpl->SELECIONADO = "selected";
        else
          $tpl->SELECIONADO = "";
        
        $tpl->ID_LOCACAO = $sql->result("NNUMESETOR");
        $tpl->LOCACAO = $sql->result("CNOMESETOR");
        $tpl->block("ITEM_LOCACAO");
        $sql->next();
      }
      
      $tpl->block("FILTRO_LOCACAO");
    }
  }

  if($_SESSION['apelido_operadora'] <> 'clinipam'){
    $tpl->{"SITUACAO_".$situacao} = "selected";  
    
    $tpl->OPERACAO_SELECIONADA = $operacao;
  }
  $tpl->DATAINICIAL = $datainicial;
  $tpl->DATAFINAL = $datafinal;
  
  if (isset($_POST['imprimir'])) {
    $parametros = array();
    $parametros[0]['nome'] = 'op';
    $parametros[0]['valor'] = $operacao;
    $parametros[1]['nome'] = 's';
    $parametros[1]['valor'] = $situacao;
    $parametros[2]['nome'] = 'di';
    $parametros[2]['valor'] = $datainicial;
    $parametros[3]['nome'] = 'df';
    $parametros[3]['valor'] = $datafinal;
    $parametros[4]['nome'] = 'lo';
    $parametros[4]['valor'] = $locacao;
    
    if(($_SESSION['apelido_operadora'] == 'clinipam') or ($_SESSION['apelido_operadora'] == 'odontopam'))
      //antes usava p/ movimentacao_imp2.php, mas estava errado do que eles usavam anteriormente. foi recuperado do backup e renomeado para _clinipam, mantendo atual ..._imp2
      $tpl->RESULT = $util->redireciona2('movimentacaoClinipam.php?idSessao='.$_GET['idSessao'],$parametros,'S');
    else if ($_SESSION['apelido_operadora'] == 'vitallis')
      $tpl->RESULT = $util->redireciona2('movimentacaoImp3.php?idSessao='.$_GET['idSessao'],$parametros,'S');
    else if ($_SESSION['apelido_operadora'] == 'promedmg')
      $tpl->RESULT = $util->redireciona2('movimentacaoImp4.php?idSessao='.$_GET['idSessao'],$parametros,'S');
		else if ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista')
      $tpl->RESULT = $util->redireciona2('movimentacaoImpLeste.php?idSessao='.$_GET['idSessao'],$parametros,'S');			
	  else
   	  $tpl->RESULT = $util->redireciona2('movimentacaoImp.php?idSessao='.$_GET['idSessao'],$parametros,'S');
  } 

  if ($_SESSION['apelido_operadora'] == 'unimedLimeira')
    $tpl->MENSAGEM_ALERTA = "Selecione nos campos abaixo o per�odo base para a sua listagem de movimenta��o.<br>
                                    Caso haja alguma discrep�ncia entre o que est� apresentado e o que de fato aconteceu, n�o hesite em entrar em contato conosco.";
  else                                    
    $tpl->MENSAGEM_ALERTA = "Selecione nos campos abaixo o per�odo base para a sua listagem de movimenta��o.<br>
                                    Caso haja alguma discrep�ncia entre o que est� apresentado e o que de fato aconteceu, n�o hesite em entrar em contato conosco.<br>
                                    Solicitamos que a Rela��o de Movimenta��o seja impressa, carimbada com CNPJ, assinada e protocolada na Operadora.";  
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     
  
?>

  