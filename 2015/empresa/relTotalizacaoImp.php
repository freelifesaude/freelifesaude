<?php
  Session_start();

  require_once('../comum/sessao.php'); 
?>

<?php
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  
  $bd      = new Oracle();
  $formata = new Formata();
  $func    = new Funcao();
  $util    = new Util();

  $formato      = $_POST['f'];
  $situacao     = $_POST['s'];
  $status       = $_POST['t'];
  $locacao      = $_POST['l'];
  $periodo      = $_POST['p'];
  $datainicial  = $_POST['di'];
  $datafinal    = $_POST['df'];
  $tipo         = $_POST['tipo'];
  $id_contrato  = $_SESSION['id_contrato'];
  $idadeinicial = $_POST['ii'];
  $idadefinal   = $_POST['if'];  

  if ($status > 0) {
    $filtro_status = "   AND HSSUSUA.NNUMESTAT = :status ";
  }
  else
    $filtro_status = "";

  if ($periodo == 'C') {
    $filtro_data = "   AND HSSUSUA.DSITUUSUA >= :inicio AND HSSUSUA.DSITUUSUA < TO_DATE(:fim) + 1 ";
  } else {
    $filtro_data = "   AND HSSUSUA.DINCLUSUA >= :inicio AND HSSUSUA.DINCLUSUA <= :fim ";
  }
  
  if ( ($idadeinicial <> '') or ($idadefinal <> '')  ) {
    $filtro_idade = "   AND idade_usuario(HSSUSUA.NNUMEUSUA) >= :idadeinicio AND idade_usuario(HSSUSUA.NNUMEUSUA) <= :idadefinal ";
  }

  if ($situacao == 'A') {
    $filtro_situacao = " WHERE HSSUSUA.CSITUUSUA = 'A' ";
  } else if ($situacao == 'C') {
    $filtro_situacao = " WHERE HSSUSUA.CSITUUSUA <> 'A' ";
  } else {
    $filtro_situacao = " WHERE HSSUSUA.NNUMEUSUA > 0";
  }

  if ($locacao > 0) {
    $filtro_locacao = "   AND TITULAR.NNUMESETOR = :locacao ";
  } else {
    $filtro_locacao = "";
  }
  
  // Na gera��o do arquivo eu sempre gero analitico.
  if ($tipo == "A")
    $formato = "2";  
  
  if ($formato == '1') {
    $txt = "SELECT CDESCPLAN, ".
           "       SUM(DECODE(HSSUSUA.CTIPOUSUA,'T',1,'F',1,0)) TITULARES, ".
           "       SUM(DECODE(HSSUSUA.CTIPOUSUA,'T',0,'F',0,'A',0,1)) DEPENDENTES, ".
           "       SUM(DECODE(HSSUSUA.CTIPOUSUA,'A',1,0)) AGREGADOS, ".
           "       COUNT(*) USUARIOS ".
           "  FROM HSSUSUA,HSSTITU,HSSPLAN,HSSUSUA TITULAR ".
           $filtro_situacao.
           $filtro_data.
           $filtro_status.
		   $filtro_idade.
           "   AND HSSUSUA.NNUMETITU = :contrato ".
           $filtro_locacao.  
           "   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ".
           "   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA ".
           "   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ".
           " GROUP BY CDESCPLAN ".
           " ORDER BY 1 ";
  } else {
    $txt = "SELECT NVL(CNOMESETOR,' ') CNOMESETOR,HSSUSUA.CCODIUSUA, HSSUSUA.CNOMEUSUA, HSSPLAN.CDESCPLAN, HSSUSUA.CTIPOUSUA, DECODE(HSSUSUA.NNUMEUSUA,HSSUSUA.NTITUUSUA,0,1) ORDEM, 
                   TITULAR.CNOMEUSUA TITULAR, HSSUSUA.CCHAPUSUA, NVL(HSSUSUA.NNUMEACOM,NVL(HSSTITU.NNUMEACOM,HSSPLAN.NNUMEACOM)) NNUMEACOM, HSSUSUA.C_CPFUSUA C_CPFUSUA,
                   TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,DECODE(HSSUSUA.CSITUUSUA,'A','',TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY')) DSITUUSUA,HSSPROF.CDESCPROF,DECODE(CNPJ_OPERADORA(),'45309606000141',RETORNA_DEPARTAMENTO_USUARIO(HSSUSUA.NNUMEDEPAR),CNOMEFUNSE) CNOMEFUNSE,
                   HSSUSUA.NDCNVUSUA,
                   HSSUSUA.CNMAEUSUA, HSSUSUA.DNASCUSUA,VALOR_USUARIO_VENCIMENTO(HSSUSUA.NNUMEUSUA,SYSDATE) VALOR,
                   DECODE(HSSUSUA.CGRAUUSUA,'F','Filho','E','Esposo','P','Pai','M','Mae','I','Irm�o','A','Avo','T','Tio','R','Primo','C','Cunhado',
                                            'G','Genro/Nora','N','Neto','B','Sobrinho','S','Sogro','O','Outro','D','Agregado','H','Enteado','J','Companheiro',
                                            'X','Pr�prio Titular','Q','Ex-C�njuge','L','Homoafetivo','Z','Bisneto') CGRAUUSUA,
                   DECODE(HSSUSUA.CESTCUSUA,'S','Solteiro','C','Casado','V','VI�VO','D','Divorciado','U','UNI�O EST�VEL','A','Separado','O','Outro')CESTCUSUA,											
									 DECODE(HSSUSUA.CSEXOUSUA,'M','Masculino','F','Feminino')CSEXOUSUA,HSSUSUA.C__RGUSUA,HSSUSUA.CSUS_USUA,HSSUSUA.CORRGUSUA,
									 HSSUSUA.DADMIUSUA, HSSUSUA.CENDEUSUA, HSSUSUA.CNUMEUSUA,HSSUSUA.CBAIRUSUA, HSSUSUA.CCIDAUSUA, HSSUSUA.CESTAUSUA, HSSUSUA.CCEP_USUA
              FROM HSSUSUA,HSSUSUA TITULAR, HSSTITU,HSSPLAN,HSSSETOR,HSSPROF,HSSFUNSE ".
           $filtro_situacao.
           $filtro_data.
           $filtro_status.
		   $filtro_idade.
           "   AND HSSUSUA.NNUMETITU = :contrato ".
           $filtro_locacao.
           "   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA 
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU 
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
               AND TITULAR.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
               AND HSSUSUA.NNUMEFUNSE = HSSFUNSE.NNUMEFUNSE(+)
               AND HSSUSUA.NNUMEPROF = HSSPROF.NNUMEPROF(+)
             ORDER BY 1,7,6,3 ";
  }

  $sql = new Query($bd);
  $sql->addParam(":contrato",$id_contrato);
  
  if ($locacao > 0)
    $sql->addParam(":locacao",$locacao);
  
  $sql->addParam(":inicio",$datainicial);
  $sql->addParam(":fim",$datafinal);
  
  if ( ($idadeinicial <> '') or ($idadefinal <> '') ) {
    $sql->addParam(":idadeinicio",$idadeinicial);
    $sql->addParam(":idadefinal",$idadefinal);  
  }

  if ($status > 0) {
    $sql->addParam(":status",$status);
  }  

  $sql->executeQuery($txt); 
  
  //Gera��o do Arquivo  
  if ($tipo == "A") {
  
    if ($locacao > 0)
      $arquivo = str_replace(' ','_',$formata->somenteCaracteres($func->retornaNomeLocacao($bd,$locacao), 'ABCDEFGHIJKLMNOPQRSTUVXYWZ 1234567890')."_".date('Ymd'));
    else
      $arquivo = str_replace(' ','_',$formata->somenteCaracteres($_SESSION['titular_contrato'], 'ABCDEFGHIJKLMNOPQRSTUVXYWZ 1234567890'))."_".date('Ymd');
      
    $ponteiro = fopen ("../temp/".$arquivo.".csv", "w");
    $delimitador = ';';
                   
    $linha = 'CODIGO'.$delimitador.
             'NOME'.$delimitador.
             'MATRICULA'.$delimitador.    
             'CATEGORIA'.$delimitador.             
             'CPF'.$delimitador;
						 //'NASCIMENTO'.$delimitador;
						 
    if($_SESSION['apelido_operadora'] == 'unimedFranca')
      $linha .='PROFISSAO'.$delimitador.
               'SETOR'.$delimitador;
		else if($_SESSION['apelido_operadora'] == 'consaude')
      $linha .='PARENTESCO'.$delimitador.
               'NOME DA MAE'.$delimitador.
               'NASCIMENTO'.$delimitador;
							 
    else
      $linha .='PLANO'.$delimitador.             
             'ACOMODACAO'.$delimitador;    
            
    $linha .='LOCACAO'.$delimitador.
             'DECLA. NASCIDO VIVO'.$delimitador.
             'INCLUSAO'.$delimitador.
             'CANCELAMENTO'.$delimitador;    
		
		if ($_SESSION['apelido_operadora'] == 'consaude')
			$linha .= 'RG'.$delimitador.
					   'ORG�O EXP.'.$delimitador.
					   'ESTADO CIVIL'.$delimitador.
					   'SEXO'.$delimitador.
					   'DATA ADMISS�O'.$delimitador.
					   'ENDERE�O'.$delimitador.
					   'N� CASA'.$delimitador.
					   'BAIRRO'.$delimitador.
					   'CIDADE'.$delimitador.
					   'ESTADO'.$delimitador.
					   'CEP'.$delimitador.
					   'CART. SUS'.$delimitador.
					   'TELEFONE'.$delimitador;
								
    fwrite($ponteiro,$linha.chr(13).chr(10));
           
    while (!$sql->eof()) {
                       
      $linha = $sql->result("CCODIUSUA").$delimitador.
               $sql->result("CNOMEUSUA").$delimitador.
               $sql->result("CCHAPUSUA").$delimitador.    
               $func->categoriaUsuario($sql->result("CTIPOUSUA")).$delimitador.
               $formata->formataCPF($sql->result("C_CPFUSUA")).$delimitador;
      
      if($_SESSION['apelido_operadora'] == 'unimedFranca')
        $linha .=$sql->result("CDESCPROF").$delimitador.             
                 $sql->result("CNOMEFUNSE").$delimitador;
      else if ($_SESSION['apelido_operadora'] == 'consaude')  
        $linha .= $sql->result("CGRAUUSUA").$delimitador.
                  $sql->result("CNMAEUSUA").$delimitador.
                  $sql->result("DNASCUSUA").$delimitador;									
      else
        $linha .=$sql->result("CDESCPLAN").$delimitador.             
               $func->retornaNomeAcomodacao($bd,$sql->result("NNUMEACOM")).$delimitador;
      
      $linha .=$sql->result("CNOMESETOR").$delimitador.
               $sql->result("NDCNVUSUA").$delimitador.
               $sql->result("DINCLUSUA").$delimitador.
               $sql->result("DSITUUSUA").$delimitador;
      

			if ($_SESSION['apelido_operadora'] == 'consaude')
				$linha .=$sql->result("C__RGUSUA").$delimitador.
                 $sql->result("CORRGUSUA").$delimitador.
								 $sql->result("CESTCUSUA").$delimitador.
								 $sql->result("CSEXOUSUA").$delimitador.
								 $sql->result("DADMIUSUA").$delimitador.
								 $sql->result("CENDEUSUA").$delimitador.
								 $sql->result("CNUMEUSUA").$delimitador.
								 $sql->result("CBAIRUSUA").$delimitador.
								 $sql->result("CCIDAUSUA").$delimitador.
								 $sql->result("CESTAUSUA").$delimitador.
								 $sql->result("CCEP_USUA").$delimitador.
								 $sql->result("CSUS_USUA").$delimitador.
								 $sql->result("CFONETLUSU").$delimitador;
			
      fwrite($ponteiro,$linha.chr(13).chr(10)); 
               
      $sql->next();
    }

    fclose ($ponteiro);
    
    if (extension_loaded('zip')) {  
      $zip = new ZipArchive();
      $zip->open("../temp/".$arquivo.".zip", ZipArchive::CREATE);
      $zip->addfile("../temp/".$arquivo.".csv",$arquivo.".csv" );
      $zip->close();
      
      $util->redireciona('../temp/'.$arquivo.'.zip','N');  
      
    } else {
      echo "Erro ao gerar arquivo. Entre em contato com a Operadora. (Erro: php_zip)";
    }

  // PDF
  } else {  

    class PDF extends PDFSolus {
      
      function Header() {
        //Logo
        $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
        $this->SetFont('Arial','B',9);
        $this->Cell(40,5,'');
        $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
        $this->Cell(40,5,'');
        $this->Cell(150,5,'POPULA��O DE USU�RIOS',0,1);
        $this->SetFont('Arial','B',7);
        $this->Cell(40,3,'');
        $this->Cell(25,3,"Empresa:",0,0,'R');
        $this->Cell(205,3,$_SESSION['titular_contrato']." - ".$_SESSION['cnpj_contrato'],0,1);
        $this->Ln(7);
        
        if ($this->getArray("formato") == '1')      
          $this->Cell(190,1,' ','B',0);
        else
          $this->Cell(270,1,' ','B',0);
        
        $this->Ln(2);

        $this->SetFont('Arial','B',8);
        $this->Ln(1);
        $this->SetFont('Arial','B',8);

        if ($this->getArray("formato") == '1') {
          $this->Cell(90,3,'Plano',0,0);
          $this->Cell(25,3,'Titulares',0,0);
          $this->Cell(25,3,'Dependentes',0,0);
          $this->Cell(25,3,'Agregados',0,0);
          $this->Cell(25,3,'Total',0,1);
        } else {
          $this->Cell(25,3,'C�digo','B',0);
          $this->Cell(57,3,'Nome','B',0);
          $this->Cell(25,3,'Matr�cula','B',0);
          $this->Cell(18,3,'Tipo','B',0);
          $this->Cell(20,3,'CPF','B',0);

          if($_SESSION['apelido_operadora'] == 'unimedFranca'){
            $this->Cell(45,3,'Profissao','B',0);
            $this->Cell(30,3,'Setor','B',0); 
					}else{
            $this->Cell(45,3,'Plano','B',0);
            $this->Cell(30,3,'Acomoda��o','B',0);
          }
            
          $this->Cell(20,3,'D Nasc Vivo','B',0);
          $this->Cell(15,3,'Dt. Incl.','B',0);
          $this->Cell(15,3,'Dt. Canc.','B',1);
                   
        }

        $this->Ln(1);
      }

      function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        //Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
      }
    }

    if ($formato == "1")
      $pdf=new PDF('P','mm','A4');
    else
      $pdf=new PDF('L','mm','A4');
    
    $pdf->AliasNbPages();

    $arr = array("formato" => $formato);
    $pdf->SetArray($arr);
    
    $pdf->Open();
    $pdf->AddPage();
    $pdf->SetFillColor(220,220,200);
    $pdf->SetFont('Arial','',8);

    if ($formato == '1') {
      while (!$sql->eof()) {
        $pdf->Cell(90,3,$sql->result("CDESCPLAN"),0,0);
        $pdf->Cell(25,3,$sql->result("TITULARES"),0,0,'R');
        $pdf->Cell(25,3,$sql->result("DEPENDENTES"),0,0,'R');
        $pdf->Cell(25,3,$sql->result("AGREGADOS"),0,0,'R');
        $pdf->Cell(25,3,$sql->result("USUARIOS"),0,0,'R');

        $sql->next();

        $pdf->Ln(3);
      }
    } else {
    
      $total = 0;
      $total_locacao = 0;
      $locacao = "";
      $pdf->SetFont('Arial','',7);      
      
      while (!$sql->eof()) {
      
        if (($sql->result("CNOMESETOR") <> $locacao) and ($sql->result("CNOMESETOR") <> ' ')) {
          $pdf->Ln(3);      
          $pdf->Cell(100,3,$sql->result("CNOMESETOR"),0,1);
          $pdf->Ln(3);
          $locacao = $sql->result("CNOMESETOR");
          $total_locacao = 0;        
        }

        $pdf->Cell(25,3,$sql->result("CCODIUSUA"),0,0);

        if (($sql->result("CTIPOUSUA") <> 'T') and 
            ($sql->result("CTIPOUSUA") <> 'F')) { 
          $pdf->Cell(5,3,'',0,0);
          $pdf->Cell(52,3,$pdf->Copy($sql->result("CNOMEUSUA"),51),0,0);
        }
        else
          $pdf->Cell(57,3,$pdf->Copy($sql->result("CNOMEUSUA"),56),0,0);

        $pdf->Cell(25,3,$sql->result("CCHAPUSUA"),0,0);
        $pdf->Cell(18,3,$pdf->Copy($func->categoriaUsuario($sql->result("CTIPOUSUA"),'S'),18),0,0);
        $pdf->Cell(20,3,$formata->formataCPF($sql->result("C_CPFUSUA")),0,0);
        
        if($_SESSION['apelido_operadora'] == 'unimedFranca'){
          $pdf->Cell(45,3,$pdf->Copy($sql->result("CDESCPROF"),44),0,0);
          $pdf->Cell(30,3,$pdf->Copy($sql->result("CNOMEFUNSE"),28),0,0);
        }else{
          $pdf->Cell(45,3,$pdf->Copy($sql->result("CDESCPLAN"),44),0,0);
          $pdf->Cell(30,3,$pdf->Copy($func->retornaNomeAcomodacao($bd,$sql->result("NNUMEACOM")),28),0,0);
        }
          
        $pdf->Cell(20,3,$sql->result("NDCNVUSUA"),0,0);
        $pdf->Cell(15,3,$sql->result("DINCLUSUA"),0,0);
        $pdf->Cell(15,3,$sql->result("DSITUUSUA"),0,0);

        $total++;
        $total_locacao++;
        
        $sql->next();
        
        $pdf->Ln(3);
        
        if (($sql->result("CNOMESETOR") <> $locacao) and ($sql->result("CNOMESETOR") <> ' ')) {
          $pdf->Ln(3);   
          $pdf->SetFont('Arial','B',8);
          $pdf->Cell(240,3,"Total de benefici�rios da loca��o:",0,0,'R');      
          $pdf->Cell(30,3,$total_locacao,0,1);     
          $pdf->SetFont('Arial','',8);          
        }      
      }

      $pdf->Ln(3);   
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(240,3,"Total de benefici�rios:",0,0,'R');
      $pdf->Cell(30,3,$total,0,1);
      
    }
    
    $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
    $pdf->Output($file,'F');
    
    echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 
  
  }
  
  $bd->close();  

?>