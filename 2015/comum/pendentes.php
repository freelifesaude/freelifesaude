<?php
  
  if (isset($_POST['operacoes']))
    redireciona("pendentes.php","N");
  else {
   
     
    $i = 1;
    $tipo = '';
  
    while (!$sql->eof()) {
     

      
      $sql2 = new Query($bd);
      $txt2 = "SELECT * FROM HSSATUCA WHERE NNUMEATCAD = :id ";
      $sql2->addParam(":id",$sql->result("NNUMEATCAD"));
      $sql2->executeQuery($txt2);
      
      while (!$sql2->eof()) {
        if ($sql2->result("CCAMPATUCA") == 'NNUMEPLAN') {
          $anterior = retorna_nome_plano($bd,$sql2->result("CANTEATUCA"));
          $novo = retorna_nome_plano($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMEACOM') {
          $anterior = retorna_nome_acomodacao($bd,$sql2->result("CANTEATUCA"));
          $novo = retorna_nome_acomodacao($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMESETOR') {
          $anterior = retorna_nome_locacao($bd,$sql2->result("CANTEATUCA"));
          $novo = retorna_nome_locacao($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NFATUTLOGR') {
          $anterior = retorna_tipo_logradouro($bd,$sql2->result("CANTEATUCA"));
          $novo = retorna_tipo_logradouro($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMEMCANC') {
          $anterior = retorna_motivo_cancelamento($bd,$sql2->result("CANTEATUCA"));
          $novo = retorna_motivo_cancelamento($bd,$sql2->result("CNOVOATUCA"));
        } else {
          $anterior = $sql2->result("CANTEATUCA");
          $novo = $sql2->result("CNOVOATUCA");
        }   
            
        $tpl->CAMPO = $sql2->result("CDESCATUCA");
        $tpl->ANTERIOR = $anterior;
        $tpl->NOVO = $novo;

        $tpl->block("ALTERACAO");            
        $sql2->next();
      }
      
      if ($sql->result("CREJEATCAD") <> '') {
        $tpl->CAMPO = "Motivo da rejei��o:";
        $tpl->ANTERIOR = "";
        $tpl->NOVO = $sql->result("CREJEATCAD");
        $tpl->block("ALTERACAO");           
      }    

      $tpl->OPERADOR = $sql->result("CNOMEUSUA");
          
      $tpl->block("REGISTRO");
      $sql->next();
      
      if ($tipo <> $sql->result("TIPO")) {
        $i = 1;
        
        if ($tipo == '1')
          $tpl->TIPO = "<font color='navy'>Opera��es PENDENTES para aprova��o da operadora</font>";
        else if ($tipo == '2')
          $tpl->TIPO = "<font color='red'>Opera��es REJEITADAS pela operadora</font>";
        else if ($tipo == '3')
          $tpl->TIPO = "<font color='green'>Opera��es CONFIRMADAS pela operadora</font>";
          
        $tpl->block("BLOCO");  
      }    
    } 
  
    if (($mes == '') and ($nome == '')) {
      $sql_visu = new Query($bd);      
      $txt_visu = "UPDATE HSSATCAD SET DVISUATCAD = SYSDATE
                    WHERE NNUMEATCAD IN (SELECT NNUMEATCAD FROM HSSATCAD,HSSUSUA
                                          WHERE HSSATCAD.NNUMETITU = :contrato
                                            AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA";
		
      if ($_SESSION['id_titular'] > 0) {
        $txt_visu .= " AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA  
		               AND HSSUSUA.NTITUUSUA = :titular ";           
        $sql_visu->addParam(":titular",$_SESSION['id_titular']);
      } 		
                                            
                                            
      if (permissao_outros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
        $txt_visu .= "                      AND HSSATCAD.NOPERUSUA = :operador ";
        $sql_visu->addParam(":operador",$_SESSION['id_operador']);
      }                                            
                                            
      $txt_visu .= "                        AND DVISUATCAD IS NULL ) ";
     
      $sql_visu->addParam(":contrato",$_SESSION['id_contrato']);
      $sql_visu->executeSQL($txt_visu);
  
      $sql = new Query($bd);
      $txt = "SELECT DISTINCT TO_CHAR(DDATAATCAD,'MM/YYYY') MES, TO_CHAR(DDATAATCAD,'YYYYMM') MES2
                FROM HSSATCAD
               WHERE NNUMETITU = :contrato ";
               
      if ($_SESSION['id_titular'] > 0) {
        $txt .= "   AND HSSATCAD.NTITUUSUA = :titular ";           
        $sql->addParam(":titular",$_SESSION['id_titular']);
      }  
               
      if ($_SESSION['id_locacao'] > 0) {
        $txt .= "   AND HSSATCAD.NNUMESETOR = :locacao ";           
        $sql->addParam(":locacao",$_SESSION['id_locacao']);
      }            

      if (permissao_outros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
        $txt .= "   AND HSSATCAD.NOPERUSUA = :operador ";
        $sql->addParam(":operador",$_SESSION['id_operador']);
      }                                            
      
      $txt .= "  AND CFLAGATCAD IS NOT NULL
                 AND DDATAATCAD < FIRST_DAY(SYSDATE)
               ORDER BY 2 DESC";       
                         
      $sql->addParam(":contrato",$_SESSION['id_contrato']);
      $sql->executeQuery($txt);
      
      while (!$sql->eof()) {
        $tpl->MES = $sql->result("MES");
        $tpl->block("MESES");  
        $sql->next();
      }
      
      $tpl->block("HISTORICO");
    } else
      $tpl->block("BOTAO_VOLTAR");
  }
  
  $tpl->NOME = $nome;
  
  $bd->close();
  $tpl->show();     

?>