<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd   = new Oracle();   
  $func = new Funcao();
  $seg  = new Seguranca();
  
  
    $carencias = 0;
    $codigo = trim($_POST['codigo']);   
  
    
        /*consultar carência para exibir assim que o código do beneficiário for digitado */
          $sql_carencia = new Query($bd);   
          $txt = "SELECT CGRAUUSUA,CNOMEUSUA,CNOMECARE,DVENCCRUS,CCODIUSUA,HSSCRUS.NNUMECARE,HSSCRUS.NNUMEUSUA
                    FROM HSSUSUA,HSSCRUS,HSSCARE
                   WHERE HSSUSUA.CCODIUSUA = :codigo
                     AND HSSCRUS.DVENCCRUS >= SYSDATE
                     AND HSSUSUA.NNUMEUSUA = HSSCRUS.NNUMEUSUA
                     AND HSSCRUS.NNUMECARE = HSSCARE.NNUMECARE
                   ORDER BY 1 DESC,2,4 DESC";
          $sql_carencia->addParam(":codigo",$codigo);
          $sql_carencia->executeQuery($txt);  
        

        if ($sql_carencia->count() > 0) {
          $carencias = '<table width="80%" align="center" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td align=center><font size=3 color=red>Quadro de carências deste usuário</font></td>
                        </tr>
                        <tr>
                        <td bgcolor="#CCCCCC" class="fonte_11_b">Carência</td>
                          <td bgcolor="#CCCCCC" class="fonte_11_b">Data</td>
                           </tr>';
              while (!$sql_carencia->eof()) {
            $carencias .=  '<tr>
                            <td align="left" class="fonte_11_n">'.$sql_carencia->result("CNOMECARE").'</td>
                            <td align="left" class="fonte_11_n">'.$sql_carencia->result("DVENCCRUS").'</td>
                            </tr>';
              
              $sql_carencia->next();
              }
                                   
           $carencias .=  '</table>';
    }else{
      $carencias = 0;
    }
 

  echo $carencias;      
    
  
  $bd->close();
?>