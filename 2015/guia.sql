-- Status da guia
-- null  = Liberada
-- N     = Negada
-- A     = Sob Auditoria
-- P     = Aguardando autorizacao
-- C     = Aguardando confirma��o
-- B     = Sob auditoria na unimed origem
-- D     = negada na unimed origem
-- X     = cancelada
-- 1     = Pedido de exame (nova vers�o), esse tipo de guia n�o pode ser faturado

-- Status do procedimento
-- 1     = Procedimento do pedido j� utilizado

create or replace package guia as
  procedure calcularGuia(idGuia in number);

  procedure calcularProcedimentoGuia(idGuia             in     number,
                                     procedimento       in     varchar2,
                                     tipoProcedimento   in     varchar2,
                                     quantidade         in     number,
                                     usoDeVideo         in     varchar2,
                                     contraste          in     number,
                                     cobertura          in out number,
                                     cobrancaFranquia   in out varchar2,
                                     valorFranquia         out number,
                                     valorProcedimento     out number,
                                     copartPrestador       out number,
                                     valorFranquiaHon      out number,
                                     valorFranquiaCO       out number,
                                     valorFranquiaFilme    out number,
                                     dataInicioLimite      out date,
                                     situacaoGuia          out varchar2,
                                     motivoCobranca        out varchar2);

  procedure calcularTaxaGuia(idGuia               in     number,
                             idTaxa               in     number,
                             quantidade           in     number,
                             idPrestador          in     number,
                             idContrato           in     number,
                             dataEmissao          in     date,
                             idBeneficiario       in     number,
                             idCongenere          in     number,
                             cobertura            in out number,
                             cobrancaFranquia     in out varchar2,
                             valorTaxa               out number,
                             valorCopart             out number,
                             copartPrestador         out number);

  procedure calcularPacoteGuia(idGuia               in number,
                               idPacote             in number,
                               quantidade           in number,
                               dataEmissao          in date,
                               coberturaConsiderada in out number,
                               cobrancaConsiderada  in out varchar2,
                               valorCopart             out number);                            

  procedure calcularProdutoGuia(idProduto        in     number,
                                tipoProduto      in     varchar2,
                                quantidade       in     number,
                                idPrestador      in     number,
                                dataEmissao      in     date,
                                idCongenere      in     number,
                                idContrato       in     number,
                                idFornecedor     in     number,
                                cobertura        in out number,
                                cobrancaFranquia in out varchar2,
                                valorProduto     in out number,
                                valorFranquia       out number,
                                idTabelaUsada       out number);                               
                                
  procedure insereCritica (idGuia         in number,
                           procedimento   in varchar2,
                           motivo         in number,
                           operador       in number,
                           dente          in number default null,
                           face           in number default null,
                           taxa           in number default null);

  function descricaoCritica (motivo in number) return varchar2;
  
  procedure validar (idGuia       in number,
                     idOperador   in number,
                     localEmissao in varchar2,
                     confirmacao  in varchar2 default 'N',
                     tipoGuia     in varchar2);

end;
/

create or replace package body guia as

acidenteDeTrabalho    varchar2(1);
cnpj                  varchar2(14);
cobrancaCancelados    varchar2(1);
contratoCortesia      number;
dataCancelamento      date;
dataEmissao           date;
executante            number;
executante_temp       number;
formaCalculo          varchar2(1);
hora                  varchar2(5);
idAcomodacao          number;
idadeBeneficiario     number;
idadeRN               number;
idBeneficiario        number;
idCid                 number;
idCongenere           number;
idContrato            number;
idConvenio            number;
idEspecialidade       number;
idGuiaPedido          number;
idNaousuario          number;
idPlano               number;
idStatus              number;
incidenciaCopartLocal varchar2(1);
indiceTabelaPrestador number;
intercambio			      varchar2(1);
localAtendimento      number;
localEmissao          varchar2(1);
modalidade            varchar2(1);
natureza              varchar2(1);
percentualConvenio    number;
sexo                  varchar2(1);
situacaoUsuario       varchar2(1);
solicitante           number;
tabelaPrestador       number;
temp_id               number;
temp_str              varchar2(1);
tipoOperadora         varchar2(1);
urgencia              varchar2(1);
valorNaGuia           varchar2(1);
mensagemRetorno       varchar2(1);
consultaRetorno       varchar2(9);
atendimentoRN         varchar2(1);
tabela_taxa           number;
eh_cooperado          varchar2(1);
nome_cortesia         varchar2(200);
num_auto_origem       varchar2(20);  

  procedure carregaGuia(idGuia in number) as
    cursor cr_guia is
      select hssguia.nsolipres,hssguia.nnumepres,hssguia.nlocapres,hssguia.nnumeespec,hssguia.nnumeusua,hssguia.ctipoguia,
             hssguia.curgeguia,hssguia.demisguia,hssguia.ciaciguia,hssusua.csituusua,hssusua.dsituusua,hssusua.csexousua,
             hsstitu.nnumeconge,hsstitu.nnumetitu,hssusua.nnumeplan,hssguia.nnumeacom,to_char(demisguia,'HH24:mi'),
             hssnusua.nnumenusua,hssguia.ccalcguia,hssguia.nnumetitu,hssguia.nnumecid_,hssguia.clocaguia,catrnguia,idade(dnarnguia,demisguia),
             nvl(hssusua.nnumestat,hsstitu.nnumestat),to_char(to_number(nvl(cmodaplan,'1'))),hssguia.npediguia,DECODE(ContratoIntercambioUnimed(ccodiusua),NULL,'N','S'),ccortguia,cautoguia
        from hssguia,hssusua,hsstitu,hssnusua,hssplan
       where hssguia.nnumeguia = idGuia
         and hssguia.nnumeusua = hssusua.nnumeusua(+)
         and hssusua.nnumetitu = hsstitu.nnumetitu(+)
         and hssguia.nnumenusua = hssnusua.nnumenusua(+)
         and hssusua.nnumeplan  = hssplan.nnumeplan(+);

    cursor cr_tabelaPrestador is
      select hsstabe.nnumetabe,nvl(hsslinea.nnumeinde,hsstabe.nnumeinde)
        from hsslinea,hsstabe
       where hsslinea.nnumepres = executante
         and (hsslinea.nnumeconge = idCongenere or hsslinea.nnumeconge is null)
         and dvigelinea = (select max(linea.dvigelinea) from hsslinea linea
                            where linea.nnumepres = hsslinea.nnumepres
                              and dvigelinea <= dataEmissao
                              and (linea.nnumeconge = idCongenere or linea.nnumeconge is null) 
                              and (linea.ddvallinea >= dataEmissao or linea.ddvallinea is null))
         and hsslinea.nnumetabe = hsstabe.nnumetabe;

    cursor cr_convenio is
      select hssnusua.nnumeconve,npercconve
        from hssnusua,hssconve
       where nnumenusua = idNaoUsuario
         and hssnusua.nnumeconve = hssconve.nnumeconve;

    cursor cr_copartPrestador is
      select ccopapres
        from finpres
       where nnumepres = localAtendimento;
       
    cursor cr_prestador (id_prestador in number) is
      select decode(ccredpres,'O','S','N')
        from finpres
       where nnumepres = id_prestador;
   
    cursor cr_internet is
      select cretointer,ccretpmed 
        from hssinter,hssconf;

  begin
    open cr_guia;
    fetch cr_guia into solicitante,executante,localAtendimento,idEspecialidade,idBeneficiario,natureza,
                       urgencia,dataEmissao,acidenteDeTrabalho,situacaoUsuario,dataCancelamento,sexo,
                       idCongenere,idContrato,idPlano,idAcomodacao,hora,
                       idNaoUsuario,formaCalculo,contratoCortesia,idCid,localEmissao,atendimentoRN,idadeRN,
                       idStatus,modalidade,idGuiaPedido,intercambio,nome_cortesia,num_auto_origem;
    close cr_guia;

    open cr_prestador(executante);
    fetch cr_prestador into eh_cooperado;
    if cr_prestador%notfound then
      eh_cooperado := 'N';
    end if;
    close cr_prestador;
    
    open cr_tabelaPrestador;
    fetch cr_tabelaPrestador into tabelaPrestador,indiceTabelaPrestador;
    close cr_tabelaPrestador;

    open cr_copartPrestador;
    fetch cr_copartPrestador into incidenciaCopartLocal;
    close cr_copartPrestador;

    if idNaousuario > 0 then
      open cr_convenio;
      fetch cr_convenio into idConvenio,percentualConvenio;
      close cr_convenio;
    end if;
    
    open cr_internet;
    fetch cr_internet into mensagemRetorno,consultaRetorno;
    close cr_internet;

    idadeBeneficiario  := beneficiario.idade(idBeneficiario);
    cnpj               := configuracao.cnpjOperadora();
    valorNaGuia        := configuracao.valorNaGuia();
    cobrancaCancelados := configuracao.cobrancaCancelados();
    tipoOperadora      := configuracao.tipoOperadora();

    if configuracao.regraAutorizacao = 'L' then
      executante_temp := localAtendimento;
    else
      executante_temp := executante;
    end if;

  end;

  procedure insereCritica (idGuia         in number,
                           procedimento   in varchar2,
                           motivo         in number,
                           operador       in number,
                           dente          in number default null,
                           face           in number default null,
                           taxa           in number default null) as
  tipoCritica varchar2(1);
  temp        number;
  
  cursor cr_critica is
   select nnumeguia from hsscriti
    where nnumeguia  = idGuia
      and nmoticriti = motivo
      and NVL(ccodipmed,' ')  = NVL(procedimento,' ');
      
  begin
    open cr_critica;
    fetch cr_critica into temp;
    if cr_critica%notfound then
      if taxa is not null then
        tipoCritica := 'T';
      else
        if (intercambio = 'N') then
          tipoCritica := 'P';
        else
          tipoCritica := '';
        end if;        
      end if;

      insert into hsscriti (nnumeguia,ccodipmed,nmoticriti,nnumeusua,nnumedente,nnumeface,nnumetaxa,ctipocriti)
                    values (idGuia,procedimento,motivo,operador,dente,face,taxa,tipoCritica);    
                    
      if (parametro_string('AUTORIZA_WEB_PARCIAL') = 'S') then
        if procedimento is not null and motivo <> 7 and motivo <> 35 then
          update hsspgui set cstatpgui = 'N' where cstatpgui is null and nnumeguia = idGuia and ccodipmed = procedimento;
        end if;
      end if;
    end if;
    close cr_critica;  
  end;

function descricaoCritica (motivo in number) return varchar2 as
  descricao varchar2(500);
  begin
    -- Existem par�metros na HSSPARAM (N_CRIT01_AUDIT at� o N_CRIT.._AUDIT) que vinculam o tipo de critica
    -- ao Processo de auditoria a ser vinculado na guia quando a guia com Critica for enviada para auditoria.
    -- Portanto, ao ser incluido ou alterado as criticas dever� ser criado par�metro para autorizar o processo.
    -- O processo � automatizado a partir da PROCEDURE CRITICA_PROCESSO_AUDITORIA que � chamada pelas TRIGGER's
    -- TRI_HSSGUIA_AUDITORIA_PTU e TRI_HSSCRITI

    case motivo
      when  1 then descricao := 'Idade do benefici�rio incompat�vel com o procedimento';
      when  2 then descricao := 'Regime de atendimento inv�lido';
      when  3 then descricao := 'Prestador n�o autorizado para executar o procedimento';
      when  4 then descricao := 'Procedimento sem cobertura contratual';
      when  5 then descricao := 'Benefici�rio em car�ncia';
      when  6 then descricao := 'Retorno/Reincid�ncia de procedimento';
      when  7 then descricao := 'Necess�rio per�cia pr�via do servi�o de auditoria da Operadora.';
      when  8 then descricao := 'Estouro de limite';
      when  9 then descricao := 'Rede de atendimento inv�lida para o paciente';
      when 10 then descricao := 'N�o foi poss�vel emitir esta autoriza��o. Gentileza entrar em contato com a Operadora ';
      when 11 then descricao := 'Sexo n�o permitido para o procedimento e/ou especialidade.';
      when 12 then descricao := 'Benefici�rio cancelado';
      when 13 then descricao := 'Operador isentou cobran�a de co-participa��o';
      when 14 then descricao := 'Procedimento n�o pertencente ao rol da A.N.S.';
      when 15 then descricao := 'Sem cobertura para a acomoda��o informada';
      when 16 then descricao := 'Estouro de teto financeiro ou de quantidade';
      when 17 then descricao := 'Benefici�rio com pr�-exist�ncia';
      when 18 then descricao := 'Procedimento fere regra de glosa (procedimentos n�o podem ser liberados juntos)';
      when 19 then descricao := 'Estouro de valor m�ximo da guia';
      when 20 then descricao := 'Procedimento autorizado pela auditoria';
      when 21 then descricao := 'Natureza da guia n�o permitida para o procedimento';
      when 22 then descricao := 'Cobran�a diferenciada';
      when 23 then descricao := 'Valor diferenciado autorizado na origem';
      when 24 then descricao := 'Procedimento n�o pode ser liberado pela internet';
      when 25 then descricao := 'Estouro de limita��o t�cnica de procedimento no per�odo';
      when 26 then descricao := 'Validade de carteira vencida ou data de vig�ncia vencida';
      when 27 then descricao := 'Benefici�rio com status que bloqueia atendimento';
      when 28 then descricao := 'Procedimento n�o pode ser liberado pela internet (Cobertura)';
      when 29 then descricao := 'Benefici�rio com coparticipa��o a pagar e programa��o de cancelamento';
      when 30 then descricao := 'Benefici�rio com coparticipa��o a pagar tentando autoriza��o na web';
      when 31 then descricao := 'Operador informado da necessidade de auditoria, optou por liberar o procedimento';
      when 32 then descricao := 'Operador informou que a guia de consulta foi feita fora da operadora';
      when 33 then descricao := 'Benefici�rio com inclus�o futura';
      when 34 then descricao := 'Benefici�rio n�o pertence a Unimed';
      when 35 then descricao := 'Cobertura indica necessidade de pr�-autorizacao da empresa';
      when 36 then descricao := 'Quantidade do procedimento estoura m�ximo de incid�ncia por benefici�rio';
      when 37 then descricao := 'Atendimento fora da �rea de abrang�ncia do plano';
      when 38 then descricao := 'Transa��o de interc�mbio (PTU) duplicada';
      when 39 then descricao := 'Benefici�rio repassado para outra operadora';
      when 40 then descricao := 'Autogera��o de guia de procedimento (operador do sistema com contrato na Operadora)';
      when 41 then descricao := 'Combina��o Dente / Face n�o permitida para o procedimento.';
      when 42 then descricao := 'Beneficiario nao pode ser liberado pela web por causa da cobertura informar que a copart � no caixa.';
      when 43 then descricao := 'Idade do benefici�rio incompat�vel com a especialidade';
      when 44 then descricao := 'J� existe uma guia para o mesmo dia, procedimento e local de atendimento.';
      when 45 then descricao := 'O procedimento solicitado n�o faz parte da tabela cadastrada.'; 
      when 46 then descricao := 'Benefici�rio com menos de 2 anos de contrato e guia com procedimento de alto custo (conforme cobertura)';
      when 47 then descricao :=	'Tentativa de emiss�o de uma guia ambulatorial para um benefici�rio internado';	
      when 48 then descricao :=	'Tentativa de emiss�o de uma solicita��o de interna��o para um benefici�rio j� internado';	
      when 49 then descricao := 'Benefici�rio falecido';
      when 50 then descricao := 'Guia em duplicidade, ja existe uma guia para a mesma especialidade e local de atendimento realizado nos ultimos 5 min.';
      when 51 then descricao := 'Este benefici�rio est� internado, n�o � permitido a gera��o de guias ambulatoriais.';
      when 52 then descricao := 'Guia original n�o est� autorizada';
      when 53 then descricao := 'Emiss�o de autoriza��es recentes';
      when 54 then descricao := 'Estouro de limita��o t�cnica de procedimento no per�odo';   
      when 55 then descricao := 'Guia negada devido ao limite de procedimento autogerado';      
      when 56 then descricao := 'Solicitante n�o autorizado a solicitar procedimento';    
      when 57 then descricao := 'Operador enviou guia pra auditoria';      
      when 58 then descricao := 'Benefici�rio inadimplente';     
      when 59 then descricao := 'Procedimento excedeu quantidade m�xima por guia';
      when 60 then descricao := 'Prestador afastado';
      when 61 then descricao := 'Local de atendimento - prestador afastado';
      when 62 then descricao := 'Cliente fora da �rea de abrang�ncia contratual';
      when 63 then descricao := 'Atendimento fora da RN 338. Gentileza entrar em contato com a Operadora.';
    else
      descricao := '';
    end case;

    return descricao;
  end;

  procedure validar (idGuia       in number,
                     idOperador   in number,
                     localEmissao in varchar2,
                     confirmacao  in varchar2 default 'N',
                     tipoGuia     in varchar2) as

    atitudeEstouroLimite   varchar2(1);
    auditoria              varchar2(1);
    carencia               varchar2(60);
    coberturaWeb           varchar2(1);
    cobrancaFranquia       varchar2(1);
    codigo                 varchar2(9);
    consumo                number;
    contaRetorno           number;
    copartAVista           varchar2(1);
    dataInicio             date;
    dataRetorno            date;
    dias                   number;
    flagPTU                varchar2(1);
    franquiaPercentual     number;
    franquiaValor          number;
    guiaRetorno            number;
    idIndice               number;
    idLimite               number;
    limiteQuantidade       number;
    limiteQuantidadeMensal number;
    limiteValor            number;
    limiteValorMensal      number;
    natureza_limitacao     varchar2(1);
    percAcrescimoLimite    number;
    qtConsumo              number;
    qtde                   number;
    qtde_ano               number;
    qtde_mes               number;
    qtde_usada             number;
    recebidoCaixa          varchar2(1);
    regime_limitacao       varchar2(1);
    retornoEncaminhamento  varchar2(1);
    tipo                   varchar2(1);
    tipoLimite             varchar2(1);
    vencimento             date;
    vlConsumo              number;
    rolAns                 varchar2(1);
    cid                    number;
    aut_puericultura       varchar2(1);
    processo               number;
    pacote                 varchar2(1);
    codigo_unimed          varchar2(4);
    guia_negada            varchar2(1);
    necessita_aut_origem   varchar2(1);  
    proc_regra_glosa       varchar2(9);
    regraglosa             number;
    qtde_auto_gerado       number;
    limite_auto_gerado     number;
    validaProcPacote       varchar2(1);

    cursor cr_puericultura(usuario    in number,
                           guia       in number,
                           quantidade in number) is
      select puericultura(usuario,guia,quantidade) from dual; 
            
    cursor cr_puericultura_emitida(usuario    in number,
                                   guia       in number,
                                   quantidade in number) is
      select puericultura(usuario,guia,quantidade,'N') from dual; 
    
    cursor cr_coberturaweb(cobertura in number) is
      select cweb_cober,ccobccober
        from hsscober
       where nnumecober = cobertura;

    cursor cr_denteFace (procedimento in varchar2,
                         dente        in number,
                         face         in number) is
      select hssdfpro.nnumedente
        from hssdfpro
       where hssdfpro.ccodipmed = procedimento
         and hssdfpro.nnumedente = dente
         and nvl(hssdfpro.nnumeface,0) = decode(face,null,0,face);

    cursor cr_temDenteFace (procedimento in varchar2) is
      select 1
        from hssdfpro
       where hssdfpro.ccodipmed = procedimento;

    cursor cr_caixa is
      select 'S'
        from hssgcaix
       where nnumeguia = idGuia;

    cursor cr_idade_especialidade (especialidade in number,
                                   idade         in number) is
      select 'X' ccodipmed
        from hssespec
       where hssespec.nnumeespec = especialidade
         and nidaiespec is not null
         and nidafespec is not null
         and (idade < nidaiespec or idade > nidafespec)
         --Na condi��o abaixo a consulta em "Pediatria" � em nome da m�e j� que a crian�a n�o dever� estar cadastrada
         --   ai para esta especialidade n�o se deve haver trava por limite de idade.
         and ( (nvl(ctis3espec,'X') <> '225124' or atendimentoRN <> 'S') );

    cursor cr_idade_prestador (prestador     in number,
                               procedimento  in varchar2,
                               idade         in number) is
      select ccodipmed
        from hssautpr
       where hssautpr.nnumepres = prestador
         and (hssautpr.ccodipmed = procedimento or
              substr(hssautpr.ccodipmed,1,7) = grupoprocedimento(procedimento) or
              substr(hssautpr.ccodipmed,1,7) = especialidadeprocedimento(procedimento))
         and nidaiautpr is not null
         and nidafautpr is not null
         and (idade < nidaiautpr or idade > nidafautpr)
         and atendimentoRN <> 'S';

    cursor cr_idade_procedimento (procedimento in varchar2,
                                  idade        in number) is
      select ccodipmed
        from hsspmed
       where hsspmed.ccodipmed = procedimento
         and nidaipmed is not null
         and nidafpmed is not null
         and (idade < nidaipmed or idade > nidafpmed)
         and atendimentoRN <> 'S';

    cursor cr_sexo (procedimento  in varchar2,
                    especialidade in number,
                    sexo          in varchar2) is
      select ccodipmed
        from hsspmed
       where hsspmed.ccodipmed = procedimento
         and ( (csexopmed = 'M' and sexo = 'F') or (csexopmed = 'F' and sexo = 'M') )
      union all
      select csexoespec
        from hssespec
       where hssespec.nnumeespec = especialidade
         and ( (csexoespec = 'M' and sexo = 'F') or (csexoespec = 'F' and sexo = 'M') );

    cursor cr_incidencias_mes (usuario      in number,
                               procedimento in varchar2,
                               nlim         in varchar2,
                               rlim         in varchar2,
                               dataEmissao  in date,
                               guia         in number,
                               rn           in varchar2 default 'N',
                               nomeRN       in varchar2 default null) is
      select sum(nquanpgui)
        from hssguia,hsspgui
       where nnumeusua = usuario
         and demisguia >= trunc(first_day(dataEmissao))
         and demisguia < trunc(last_day(dataEmissao)) + 1
         and cstatguia is null
         and (nlim = 'B' or ctipoguia = nlim)
         and (rlim = 'A' or curgeguia = rlim)
         and hssguia.nnumeguia = hsspgui.nnumeguia
         and cstatpgui is null
         and ccodipmed = procedimento
         and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN))
         and (hssguia.nnumeguia <> nvl(guia,0));

    cursor cr_incidencias_ano (usuario      in number,
                               procedimento in varchar2,
                               nlim         in varchar2,
                               rlim         in varchar2,
                               dataEmissao  in date,
                               guia         in number,
                               rn           in varchar2 default 'N',
                               nomeRN       in varchar2 default null) is
      select sum(nquanpgui)
        from hssguia,hsspgui
       where nnumeusua = usuario
         and demisguia >= to_date('01/01/'||to_char(dataEmissao,'YYYY'),'DD/MM/YYYY')
         and demisguia < to_date('31/12/'||to_char(dataEmissao,'YYYY'),'DD/MM/YYYY') + 1
         and cstatguia is null
         and (nlim = 'B' or ctipoguia = nlim)
         and (rlim = 'A' or curgeguia = rlim)
         and hssguia.nnumeguia = hsspgui.nnumeguia
         and cstatpgui is null
         and ccodipmed = procedimento
         and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN))
         and (hssguia.nnumeguia <> nvl(guia,0));

    cursor cr_incidencias_dia (usuario      in number,
                               procedimento in varchar2,
                               dias         in number,
                               nlim         in varchar2,
                               rlim         in varchar2,
                               dataEmissao  in date,
                               guia         in number,
                               rn           in varchar2 default 'N',
                               nomeRN       in varchar2 default null) is
      select sum(nquanpgui)
        from hssguia,hsspgui
       where nnumeusua = usuario
         and demisguia >= trunc(dataEmissao) - dias
         and demisguia <  trunc(dataEmissao) + 1
         and cstatguia is null
         and (nlim = 'B' or ctipoguia = nlim)
         and (rlim = 'A' or curgeguia = rlim)
         and hssguia.nnumeguia = hsspgui.nnumeguia
         and cstatpgui is null
         and ccodipmed = procedimento
         and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN))
         and (hssguia.nnumeguia <> nvl(guia,0));

    cursor cr_guias_mesmo_dia (usuario          in number,
                               localAtendimento in number,
                               dataEmissao      in date,
                               guia             in number,
                               rn           in varchar2 default 'N',
                               nomeRN       in varchar2 default null) is
      select count(*)
       from hssguia,hsspgui,hsspmed,hssinter
      where demisguia >= dataEmissao
        and demisguia <  dataEmissao + 1
        and nnumeusua = usuario
        and (nlocapres = localAtendimento or localAtendimento = 0)
        and cstatguia is null
        and hssguia.nnumeguia = hsspgui.nnumeguia
        and cstatpgui is null
        and hsspgui.ccodipmed = hsspmed.ccodipmed
        and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN))
        and ctipopmed = 'C'
        and nvl(cblcointer,'N') = 'S'
        and hssguia.nnumeguia <> nvl(guia,0);

    cursor cr_guias_mesmo_dia_proc (usuario          in number,
                                    localAtendimento in number,
                                    dataEmissao      in date,
                                    procedimento     in varchar2,
                                    guia             in number,
                                    rn               in varchar2 default 'N',
                                    nomeRN           in varchar2 default null) is
      select count(*) from hssguia,hsspgui,hsspmed,hssinter
      where demisguia >= dataEmissao
        and demisguia <  dataEmissao + 1
        and nnumeusua = usuario
        and (nlocapres = localAtendimento or localAtendimento = 0)
        and cstatguia is null
        and hssguia.nnumeguia = hsspgui.nnumeguia
        and hssguia.nnumeguia <> nvl(guia,0)
        and cstatpgui is null
        and hsspgui.ccodipmed = procedimento
        and hsspgui.ccodipmed = hsspmed.ccodipmed
        and ctipopmed <> 'C'
        and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN))
        and nvl(cblprinter,'N') = 'S';

    cursor cr_guias_mesmo_dia_odonto (usuario          in number,
                                      localAtendimento in number,
                                      procedimento     in varchar2,
                                      dente            in number,
                                      face             in number,
                                      dataEmissao      in date,
                                      guia             in number,
                                      rn               in varchar2 default 'N',
                                      nomeRN           in varchar2 default null) is
      select count(*) from hssguia,hssdpgui,hsspmed,hssinter
      where demisguia >= dataEmissao
        and demisguia <  dataEmissao + 1
        and nnumeusua = usuario
        and nlocapres = localAtendimento
        and hssguia.nnumeguia = hssdpgui.nnumeguia
        and hssguia.nnumeguia <> nvl(guia,0)
        and hssdpgui.ccodipmed = procedimento
        and hssdpgui.nnumedente = dente
        and hssdpgui.nnumeface = face
        and hssdpgui.ccodipmed = hsspmed.ccodipmed
        and ctipopmed <> 'C'
        and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN))
        and nvl(cblprinter,'N') = 'S';

    cursor cr_guias_mesmo_dia2 (usuario          in number,
                                procedimento     in varchar2,
                                especialidade    in number,
                                localAtendimento in number,
                                dataEmissao      in date,
                                guia             in number,
                                rn               in varchar2,
                                nomeRN           in varchar2) is
      select count(*)
       from hssguia,hsspgui
      where to_char(demisguia,'DD/MM/YYYY HH24:mi') between to_char(dataEmissao-(5/1440),'DD/MM/YYYY HH24:mi') and to_char(dataEmissao,'DD/MM/YYYY HH24:mi')
        and nnumeusua = usuario
        and (nlocapres = localAtendimento or localAtendimento = 0)
        and cstatguia is null
        and hssguia.nnumeguia = hsspgui.nnumeguia
        and hssguia.nnumeguia <> nvl(guia,0)
        and hsspgui.ccodipmed = procedimento
        and hssguia.nnumeespec = especialidade
        and cstatpgui is null
        and ((rn = 'N') or (rn = 'S' and ccortguia = nomeRN)); -- RN de g�meos n�o criticar
        
    cursor cr_config is
     select cnegainter
       from hssinter; 
       
    cursor cr_autorizacao_cobertura (cobertura in number) is    
     select cautocober
       from hsscober
     where nnumecober = cobertura
        and cautocober = 'S';
        
    cursor cr_valida_proc_pacote (idPacote in number) is    
     select cvpropacot
       from hsspacot
     where nnumepacot = idPacote
        and cstatpacot = 'A';    

    cursor cr_pagos_regra_glosa (regra        in number,
                                 procedimento in varchar2) is
     select ccodipmed
       from hssppago
     where hssppago.nnumerglos = regra
        and ccodipmed = procedimento;  

    cursor cr_autogerado  (prestador        in number,
                           procedimento     in varchar2) is
     
     select nvl(sum(hsspgui.nquanpgui),0) qtde, nqtedautgp limite 
        from hssguia, hsspgui, hssautog, hssautgp 
          where demisguia between first_day(sysdate) and last_day(sysdate)  
           and hssguia.nnumepres = executante
           and hssguia.nnumeguia = hsspgui.nnumeguia
           and hssguia.nsolipres = hssguia.nnumepres
           and hssautog.nnumeautog =  hssautgp.nnumeautog
           and hssautog.catlmautog = '1'
           and (hssautog.nlocapres is null or hssautog.nlocapres = localAtendimento)
           and (hssautog.nnumeconge is null or hssautog.nnumeconge = idCongenere)           
           and (hssautog.dfinaautog is null or hssautog.dfinaautog >= sysdate)
           and hssautog.dvigeautog <= sysdate
           and hssautgp.ccodipmed = hsspgui.ccodipmed
           and hsspgui.ccodipmed = procedimento
           and hsspgui.cstatpgui is null
           and hssguia.nnumepres = hssautog.nnumepres
           group by nqtedautgp;     
        
  begin
    carregaGuia(idGuia);
    cid := idCid;
    
    open cr_config;
    fetch cr_config into guia_negada;
    close cr_config;  

    select trim(cunimconf) into codigo_unimed from hssconf;
    
    if (intercambio = 'S') then    
      if (usuarioInadimplente(idBeneficiario,sysdate,'S','WEB',urgencia) <> 'N') then
        guia.insereCritica(idGuia,'',10,idOperador);
      else        
        for proc in (select hsspgui.ccodipmed procedimento,hsspgui.cvidepgui usoDeVideo,hsspgui.nquanpgui quantidade,
                            nvl(hsspgui.ncontpgui,0) contraste,hsspmed.ctipopmed tipoProcedimento,
                            hsspmed.cambupmed natureza,hsspmed.curgepmed urgencia,hssdpgui.nnumedente,hssdpgui.nnumeface,
                            hsspmed.nmlibpmed,hsspmed.npconpmed,hsspmed.nautmpmed,hsspmed.nautapmed,hsspmed.caudipmed,
                            hsspmed.cnlimpmed,hsspmed.crlimpmed,hsspmed.crol_pmed
                       from hsspgui,hsspmed,hssdpgui
                      where hsspgui.nnumeguia = idGuia
                        and hsspgui.ccodipmed = hsspmed.ccodipmed
                        and hsspgui.ccodipmed = hssdpgui.ccodipmed(+)
                        and hsspgui.nnumeguia = hssdpgui.nnumeguia(+)
                        and hsspmed.ccodipmed not in (select distinct celrepmed from hsscoweb where nnumepres = localAtendimento and celrepmed is not null
                                                       union all
                                                      select distinct currepmed from hsscoweb where nnumepres = localAtendimento and currepmed is not null
                                                       union all
                                                      select distinct ccretpmed from hssppamb where ccretpmed is not null
                                                       union all
                                                      select ccretpmed from hssconf where ccretpmed is not null)                               
                      union all
                     select hssppaco.ccodipmed procedimento,
                            '' usoDeVideo,NQUANPCGUI quantidade,
                            0 contraste, '' tipoProcedimento,
                            hsspmed.cambupmed natureza,hsspmed.curgepmed urgencia,0,0,
                            hsspmed.nmlibpmed,hsspmed.npconpmed,hsspmed.nautmpmed,hsspmed.nautapmed,hsspmed.caudipmed,
                            hsspmed.cnlimpmed,hsspmed.crlimpmed,hsspmed.crol_pmed
                       from hsspcgui,hsspmed,hssppaco
                      where hsspcgui.nnumeguia = idGuia
                          --and dvigeppaco = (select max(ppaco2.dvigeppaco) from hssppaco ppaco2
                            --                  where ppaco2.nnumepacot = idPacote
                            --                     and dvigeppaco <= dataEmissao)
                        and ccopappaco = 'S'
                        and hsspcgui.nnumepacot = hssppaco.nnumepacot
                        and hsspmed.ccodipmed = hssppaco.ccodipmed) loop
                -- Verifica retorno
          guiaRetorno := idGuia;
          dataRetorno := dataEmissao;
          temp_id := retorno_procedimento (executante,idBeneficiario,proc.procedimento,dataRetorno,guiaRetorno,contaRetorno,proc.nnumedente,proc.nnumeface,idEspecialidade,1, null, null, urgencia, localAtendimento);
          auditoria := proc.caudipmed;

          if temp_id >= 0 then
            -- Esta configuracao indica que nao ser� dada a mensagem de retorno e o procedimento de "consulta"
            -- ser� trocado pelo procedimento de "consulta retorno"
            if tipoGuia = 'guiaConsulta' and mensagemRetorno = 'N' and localEmissao in ('5','6','7') then
              update hsspgui set ccodipmed = consultaRetorno where nnumeguia = idGuia and ccodipmed = proc.procedimento;
            else
              guia.insereCritica(idGuia,proc.procedimento,6,idOperador);
            end if;
          end if;
          
          -- Verifica se o prestador pode autorizar o procedimento
          if (Parametro_String('CPROAU_WEB') = 'S') then
            if executante > 0 then

              temp_str := substr(procedimento_autorizado(executante,proc.procedimento,natureza,dataEmissao,urgencia,idBeneficiario,idEspecialidade,pacote,'S',idCongenere,localAtendimento),1,1);

              if temp_str = 'N' then
                if (cnpj = '00637500000139') then
                  guia.insereCritica(idGuia,proc.procedimento,45,idOperador);
                else                       
                  guia.insereCritica(idGuia,proc.procedimento,3,idOperador);
                end if;
              end if;
            end if;
          end if;

          Processo := processo_auditoria_guia(idGuia);

          if (Parametro_String('CAUDI_PTU') = 'S') then
            if (Processo > 0) and (tipoGuia <> 'guiaOdonto') then 
              guia.insereCritica(idGuia,proc.procedimento,7,idOperador);
              update hssguia set cstatguia = 'A', nptu_guia = decode(nptu_guia,4,null,nptu_guia) where nnumeguia = idGuia;
            elsif((nvl(auditoria,'N') = 'S') or
                 (nvl(auditoria,'N') = 'A' and natureza = 'A') or
                 (nvl(auditoria,'N') = 'I' and natureza = 'I')) and (tipoGuia <> 'guiaOdonto') and (Parametro_String('CAUDIPROCE_PTU') = 'N') then
              guia.insereCritica(idGuia,proc.procedimento,7,idOperador);
              update hssguia set cstatguia = 'A', nptu_guia = decode(nptu_guia,4,null,nptu_guia) where nnumeguia = idGuia;
            end if;
          end if;
        end loop;               
        end if;    
    end if;    
    
    if (intercambio = 'N') then
    
      if (usuarioInadimplente(idBeneficiario,sysdate,'S','WEB',urgencia) <> 'N') then
        guia.insereCritica(idGuia,'',10,idOperador);
      -- Insere critica de usuario falecido
      elsif (situacaoUsuario = 'F') then
        guia.insereCritica(idGuia,'',49,idOperador);
      elsif (urgencia = 'E') and (codigo_unimed is not null) and (operadora_atend_rep_usuario(idBeneficiario,'R') <> acrescenta_zeros_string(codigo_unimed,4)) then
        guia.insereCritica(idGuia,'',62,idOperador);      
      else
        for proc in (select hsspgui.ccodipmed procedimento,hsspgui.cvidepgui usoDeVideo,hsspgui.nquanpgui quantidade,
                            nvl(hsspgui.ncontpgui,0) contraste,hsspmed.ctipopmed tipoProcedimento,
                            hsspmed.cambupmed natureza,hsspmed.curgepmed urgencia,hssdpgui.nnumedente,hssdpgui.nnumeface,
                            hsspmed.nmlibpmed,hsspmed.npconpmed,hsspmed.nautmpmed,hsspmed.nautapmed,hsspmed.caudipmed,
                            hsspmed.cnlimpmed,hsspmed.crlimpmed,hsspmed.crol_pmed, null idPacote, procedimento_proc_audit(hsspgui.ccodipmed) proc_audit
                       from hsspgui,hsspmed,hssdpgui
                      where hsspgui.nnumeguia = idGuia
                        and hsspgui.ccodipmed = hsspmed.ccodipmed
                        and hsspgui.ccodipmed = hssdpgui.ccodipmed(+)
                        and hsspgui.nnumeguia = hssdpgui.nnumeguia(+)
                        and hsspmed.ccodipmed not in (select distinct celrepmed from hsscoweb where nnumepres = localAtendimento and celrepmed is not null
                                                       union all
                                                      select distinct currepmed from hsscoweb where nnumepres = localAtendimento and currepmed is not null
                                                       union all
                                                      select distinct ccretpmed from hssppamb where ccretpmed is not null
                                                       union all
                                                      select ccretpmed from hssconf where ccretpmed is not null)                               
                      union all
                      select hssppaco.ccodipmed procedimento,
                            '' usoDeVideo,NQUANPCGUI quantidade,
                            0 contraste, '' tipoProcedimento,
                            hsspmed.cambupmed natureza,hsspmed.curgepmed urgencia,0,0,
                            hsspmed.nmlibpmed,hsspmed.npconpmed,hsspmed.nautmpmed,hsspmed.nautapmed,hsspmed.caudipmed,
                            hsspmed.cnlimpmed,hsspmed.crlimpmed,hsspmed.crol_pmed, hsspcgui.nnumepacot idPacote, procedimento_proc_audit(hssppaco.ccodipmed)
                       from hsspcgui,hsspmed,hssppaco
                      where hsspcgui.nnumeguia = idGuia
                        and dvigeppaco = (select max(ppaco2.dvigeppaco) from hssppaco ppaco2
                                           where ppaco2.nnumepacot = hsspcgui.nnumepacot
                                             and dvigeppaco <= dataEmissao)
                         and ccopappaco = 'S'
                         and hsspcgui.nnumepacot = hssppaco.nnumepacot
                         and hsspmed.ccodipmed = hssppaco.ccodipmed) loop

          tipo               := proc.tipoProcedimento;
          qtde               := proc.nmlibpmed;
          dias               := nvl(proc.npconpmed,0);
          qtde_mes           := proc.nautmpmed;
          qtde_ano           := proc.nautapmed;
          auditoria          := proc.caudipmed;
          natureza_limitacao := proc.cnlimpmed;
          regime_limitacao   := proc.crlimpmed;
          rolAns             := proc.crol_pmed;
          
          if proc.idPacote > 0 then
            pacote := 'S';
            open cr_valida_proc_pacote(proc.idPacote);
            fetch cr_valida_proc_pacote into validaProcPacote;
            close cr_valida_proc_pacote; 
          else
            pacote := 'N';
            validaProcPacote := 'S';
          end if;
          
          if (proc.procedimento = '10106146') then
						if (cnpj = '85204279000188') then
							open cr_puericultura_emitida(idBeneficiario,idGuia,proc.quantidade);
							fetch cr_puericultura_emitida into aut_puericultura;
							close cr_puericultura_emitida;
						else
							open cr_puericultura(idBeneficiario,idGuia,proc.quantidade);
							fetch cr_puericultura into aut_puericultura;
							close cr_puericultura; 
            end if;
						
						if aut_puericultura = 'N' then
            
              if idadeBeneficiario > 19 and nvl(atendimentoRN,'N') = 'N' then
                guia.insereCritica(idGuia,proc.procedimento,1,idOperador);
              else
                guia.insereCritica(idGuia,proc.procedimento,63,idOperador);
              end if;
            end if;
					end if;
          
          -- Verifica ROL
          if ((rolAns = 'N') and (validaProcPacote = 'S') and (cnpj_operadora <> '13086566000120')) then
            guia.insereCritica(idGuia,proc.procedimento,14,idOperador);
          end if;    

          -- Verifica natureza do procedimento
          if ((natureza = 'A' and proc.natureza = 'I') or (natureza in ('I','C','O','R','P','S') and proc.natureza = 'A')) then
            guia.insereCritica(idGuia,proc.procedimento,21,idOperador);
          end if;

          -- Verifica urgencia do procedimento
          if ((urgencia = 'E' and proc.urgencia = 'U') or (urgencia = 'U' and proc.urgencia = 'E')) then
            guia.insereCritica(idGuia,proc.procedimento,2,idOperador);
          end if;

          -- Verifica se pode ser liberado pela web
          if localEmissao in ('5','6','7') then
            temp_str := podeSerLiberadoWeb30 (proc.procedimento,executante_temp,idBeneficiario,'S',confirmacao,codigo,
                                            tipo,qtde,dias,qtde_mes,qtde_ano,auditoria,natureza_limitacao,
                                            regime_limitacao);

            if temp_str = 'N' then
              guia.insereCritica(idGuia,proc.procedimento,24,idOperador);
            end if;
          end if;

          -- Verifica combina��o de dente e face
          if proc.tipoProcedimento in ('2','3','4','5','6','7','8','9') then
            open cr_temDenteFace(proc.procedimento);
            fetch cr_temDenteFace into temp_id;
            if cr_temDenteFace%found then
              open cr_denteFace(proc.procedimento,proc.nnumedente,proc.nnumeface);
              fetch cr_denteFace into temp_id;
              if cr_denteFace%notfound then
                guia.insereCritica(idGuia,proc.procedimento,41,idOperador,proc.nnumedente,proc.nnumeface);
              end if;
              close cr_denteFace;
            end if;
            close cr_temDenteFace;
          end if;

          -- Verifica se o prestador pode autorizar o procedimento
          if executante > 0 then
            temp_str := substr(procedimento_autorizado(executante_temp,proc.procedimento,natureza,dataEmissao,urgencia,idBeneficiario,idEspecialidade,pacote,'N',idCongenere,localAtendimento),1,1);

            if temp_str = 'N' then
              if (cnpj = '00637500000139') then
                guia.insereCritica(idGuia,proc.procedimento,45,idOperador);
              else                       
                guia.insereCritica(idGuia,proc.procedimento,3,idOperador);
              end if;
            end if;
          end if;
          
          -- Verifica se o benefici�rio est� em car�ncia para o procedimento          
          temp_id := idBeneficiario;
          
          if executante > 0 then          
            esta_em_carencia (temp_id,proc.procedimento,vencimento,carencia,null,urgencia,natureza,null,null,executante_temp,localAtendimento);
          else          
            esta_em_carencia (temp_id,proc.procedimento,vencimento,carencia,null,urgencia,natureza);
          end if;
         
          if nvl(vencimento,dataEmissao) > dataEmissao then
            guia.insereCritica(idGuia,proc.procedimento,5,idOperador);
          end if;
            
          -- Verifica se o benefici�rio tem pr�-existencia para o procedimento
          temp_id := idBeneficiario;
          tem_pre_existencia(temp_id,proc.procedimento,cid,vencimento,carencia,urgencia);

          if nvl(vencimento,dataEmissao) > dataEmissao then
            guia.insereCritica(idGuia,proc.procedimento,17,idOperador);
          end if;          
          
          -- Verifica se o benefici�rio tem cobertura para o procedimento
          if executante > 0 then
            temp_id := cobertura_usuario (idBeneficiario,proc.procedimento,natureza||urgencia,executante_temp,localAtendimento,
                                          cobrancaFranquia,dataEmissao,idEspecialidade,acidenteDeTrabalho,null,idadeBeneficiario,'0');
          else
            temp_id := cobertura_usuario (idBeneficiario,proc.procedimento,natureza||urgencia,null,null,
                                          cobrancaFranquia,dataEmissao,idEspecialidade,acidenteDeTrabalho,null,idadeBeneficiario,'0');          
          end if;
          
          open cr_autorizacao_cobertura(temp_id);
          fetch cr_autorizacao_cobertura into necessita_aut_origem;
          close cr_autorizacao_cobertura;            
          
          if necessita_aut_origem = 'S' and num_auto_origem is null then
            guia.insereCritica(idGuia,proc.procedimento,35,idOperador);
            update hssguia set cstatguia = 'P' where nnumeguia = idGuia;            
          end if;  

          if temp_id = 0 then
            temp_id := cobertura_aditivo (idBeneficiario,proc.procedimento,cobrancaFranquia,franquiaValor,franquiaPercentual);
          end if;

          if temp_id = 0 then
            guia.insereCritica(idGuia,proc.procedimento,4,idOperador);
          elsif temp_id > 0 then
            recebidoCaixa := 'N';

            if (confirmacao = 'N') and (validaProcPacote = 'S') then
              open cr_coberturaweb(temp_id);
              fetch cr_coberturaweb into coberturaWeb,cobrancaFranquia;
              close cr_coberturaweb;

              cobrancaFranquia := nvl(cobrancaFranquia,'X');

              if cobrancaFranquia = 'C' then
                open cr_caixa;
                fetch cr_caixa into recebidoCaixa;
                if cr_caixa%notfound then
                  recebidoCaixa := 'N';
                end if;
                close cr_caixa;
              end if;
            end if;

            -- Tive que fazer esse remendo para a unimed franca. Nao encontrei forma de parametrizar.
            -- Resumindo: Quando um atendimento de um contrato que teve copart a vista � solicitado na web menos de 24 horas depois de um outro
            --            que j� foi autorizado, o sistema deve autorizar mesmo que seja para o contrato que tem copart a vista porque nesse caso
            --            nao � cobrado copart. O mesmo acontece para casos de encaminhamentos.
            if (cnpj = '45309606000141' and proc.tipoProcedimento = 'C' and urgencia = 'E' and cobrancaFranquia = 'C' and recebidoCaixa = 'N' and confirmacao = 'N') or
               (cnpj = '45309606000141' and proc.procedimento in ('00010014','00010073') and localAtendimento = 1162 and  cobrancaFranquia = 'C' and recebidoCaixa = 'N' and confirmacao = 'N') then
              retornoEncaminhamento := RetornoEncaminhamentoUnimed (idBeneficiario,idGuia,null,dataEmissao,solicitante,localAtendimento,proc.tipoProcedimento,urgencia,proc.procedimento);
            else
              retornoEncaminhamento := 'N';
            end if;

            if cobrancaFranquia = 'C' and recebidoCaixa = 'N' and confirmacao = 'N' and retornoEncaminhamento = 'N' and localEmissao in ('5','6','7') then
              guia.insereCritica(idGuia,proc.procedimento,42,idOperador);
            elsif (nvl(coberturaWeb,'S') = 'N' and localEmissao in ('5','6','7')) and retornoEncaminhamento = 'N' then
              guia.insereCritica(idGuia,proc.procedimento,28,idOperador);
            end if;

            if situacaoUsuario = 'C' and cnpj = '53678264000165' and (dataCancelamento-trunc(dataEmissao)) <= 30 and (cobrancaFranquia is not null and cobrancaFranquia <> 'X') then
              if cobrancaFranquia = 'L' or cobrancaFranquia = 'F' then -- apo�s extrapolar limite
                if executante > 0 then
                  idLimite := retorna_limite (idBeneficiario,proc.procedimento,tipoLimite,limiteQuantidade,
                                              limiteQuantidadeMensal,limiteValor,
                                              limiteValorMensal,idIndice,atitudeEstouroLimite,idEspecialidade,
                                              percAcrescimoLimite,null,natureza,urgencia,localAtendimento);
                else
                  idLimite := retorna_limite (idBeneficiario,proc.procedimento,tipoLimite,limiteQuantidade,
                                              limiteQuantidadeMensal,limiteValor,
                                              limiteValorMensal,idIndice,atitudeEstouroLimite,idEspecialidade,
                                              percAcrescimoLimite,null,natureza,urgencia,null);
                end if;
                
                if nvl(idLimite,0) > 0 then

                  dataInicio := inicio_contagem_limites (idBeneficiario,idLimite,dataEmissao,idEspecialidade);
                  if tipoLimite = 'L' then
                    consumo := nvl(total_consumo (idLimite,idBeneficiario,dataInicio,dataEmissao,idGuia,proc.procedimento,idGuia,idEspecialidade),0);
                  else
                    consumo := nvl(total_consumo_intercambiavel(idLimite,idBeneficiario,dataInicio,dataEmissao,idEspecialidade),0);
                  end if;

                  if nvl(consumo,0) + proc.quantidade > limiteQuantidade then
                    copartAVista := 'S';
                  else
                    copartAVista := 'N';
                  end if;

                else
                  copartAVista := 'N';
                end if;
              else
                copartAVista := 'S';
              end if;		
-- Par�metro CCRITICA_COPART criado para verificar se critica ou n�o a copart de beneficiarios com programa��o de cancelamento.             
  		    elsif situacaoUsuario = 'C' and (parametro_string('CCRITICA_COPART') = 'S') and
			          dataCancelamento >= trunc(dataEmissao) and (cobrancaFranquia is not null and cobrancaFranquia <> 'X') then

              if cobrancaFranquia = 'L' or cobrancaFranquia = 'F' then -- apo�s extrapolar limite
                if executante > 0 then
                  idLimite := retorna_limite (idBeneficiario,proc.procedimento,tipoLimite,limiteQuantidade,
                                              limiteQuantidadeMensal,limiteValor,
                                              limiteValorMensal,idIndice,atitudeEstouroLimite,idEspecialidade,
                                              percAcrescimoLimite,null,natureza,urgencia,localAtendimento);
                else
                  idLimite := retorna_limite (idBeneficiario,proc.procedimento,tipoLimite,limiteQuantidade,
                                              limiteQuantidadeMensal,limiteValor,
                                              limiteValorMensal,idIndice,atitudeEstouroLimite,idEspecialidade,
                                              percAcrescimoLimite,null,natureza,urgencia,null);
                end if;
                
                if nvl(idLimite,0) > 0 then

                  dataInicio := inicio_contagem_limites (idBeneficiario,idLimite,dataEmissao,idEspecialidade);
                  if tipoLimite = 'L' then
                    consumo := nvl(total_consumo (idLimite,idBeneficiario,dataInicio,dataEmissao,idGuia,proc.procedimento,idGuia,idEspecialidade),0);
                  else
                    consumo := nvl(total_consumo_intercambiavel(idLimite,idBeneficiario,dataInicio,dataEmissao,idEspecialidade),0);
                  end if;

                  if nvl(consumo,0) + proc.quantidade > limiteQuantidade then
                    copartAVista := 'S';
                  else
                    copartAVista := 'N';
                  end if;

                else
                  copartAVista := 'N';
                end if;
              else
                copartAVista := 'S';
              end if;
            else
              copartAVista := 'N';
            end if;

            if (copartAVista = 'S') then
              guia.insereCritica(idGuia,proc.procedimento,29,idOperador);
            end if;

          end if;

          if executante > 0 then
            -- Verifica Regra de glosa
            regraglosa := RegrasGlosasGuias(idGuia,proc.procedimento,executante);
            
            if regraglosa > 0 then 
              open cr_pagos_regra_glosa(regraglosa,proc.procedimento);
              fetch cr_pagos_regra_glosa into proc_regra_glosa;
              close cr_pagos_regra_glosa;  
              if proc_regra_glosa <> proc.procedimento then
                guia.insereCritica(idGuia,proc.procedimento,18,idOperador);
              end if;  
            end if;
            
            if solicitante = executante then
              open cr_autogerado(executante,proc.procedimento);
              fetch cr_autogerado into qtde_auto_gerado,limite_auto_gerado;            
              if (qtde_auto_gerado > limite_auto_gerado) and (cr_autogerado%found) then
                guia.insereCritica(idGuia,proc.procedimento,55,idOperador);
              end if;    
              close cr_autogerado;     
            end if;              
            
            -- Verifica retorno
            guiaRetorno := idGuia;
            dataRetorno := dataEmissao;
            temp_id := retorno_procedimento (executante,idBeneficiario,proc.procedimento,dataRetorno,guiaRetorno,contaRetorno,proc.nnumedente,proc.nnumeface,idEspecialidade,1, null, null, urgencia, localAtendimento, null, null);

            if temp_id >= 0 then
              -- Esta configuracao indica que nao ser� dada a mensagem de retorno e o procedimento de "consulta"
              -- ser� trocado pelo procedimento de "consulta retorno"
              if tipoGuia = 'guiaConsulta' and mensagemRetorno = 'N' and localEmissao in ('5','6','7') then
                update hsspgui set ccodipmed = consultaRetorno where nnumeguia = idGuia and ccodipmed = proc.procedimento;
              else
                update hssguia set ctpcoguia = '2' where nnumeguia = idGuia;
                guia.insereCritica(idGuia,proc.procedimento,6,idOperador);
              end if;
            end if;

            -- Verifica rede de atendimento
            temp_id := valida_rede_atendimento30(idBeneficiario,localAtendimento,1,proc.procedimento,idEspecialidade);

            if temp_id <= 0 then
              guia.insereCritica(idGuia,proc.procedimento,9,idOperador);
            end if;

            -- Verifica teto
            temp_id := teto_producao(executante_temp,proc.procedimento,urgencia,dataEmissao,qtConsumo,vlConsumo);

            if temp_id > 0 then
              guia.insereCritica(idGuia,proc.procedimento,16,idOperador);
            end if;
          end if;

          -- Verifica idade do procedimento
          open cr_idade_prestador(executante_temp,proc.procedimento,idadeBeneficiario);
          fetch cr_idade_prestador into codigo;
          if cr_idade_prestador%found then
            guia.insereCritica(idGuia,proc.procedimento,1,idOperador);            
            if (cnpj = '00697509000135') then
              update hsspgui set cstatpgui = 'A' where nnumeguia = idGuia;
              update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
            end if;
          else
            open cr_idade_especialidade(idEspecialidade,idadeBeneficiario);
            fetch cr_idade_especialidade into codigo;
            if cr_idade_especialidade%found then
              guia.insereCritica(idGuia,proc.procedimento,43,idOperador);        
            else
              open cr_idade_procedimento(proc.procedimento,idadeBeneficiario);
              fetch cr_idade_procedimento into codigo;
              if cr_idade_procedimento%found then
                guia.insereCritica(idGuia,proc.procedimento,1,idOperador);
                if (cnpj = '00697509000135') then
                  update hsspgui set cstatpgui = 'A' where nnumeguia = idGuia;
                  update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
                end if;
              end if;
              close cr_idade_procedimento;
            end if;
            close cr_idade_especialidade;
          end if;
          close cr_idade_prestador;

          -- Verifica sexo do procedimento
          open cr_sexo (proc.procedimento,idEspecialidade,sexo);
          fetch cr_sexo into codigo;
          if cr_sexo%found then
            guia.insereCritica(idGuia,proc.procedimento,11,idOperador);
          end if;
          close cr_sexo;

          -- Verifica limita��o t�cnica
          if nvl(qtde_mes,0) > 0 then
            open cr_incidencias_mes (idBeneficiario,proc.procedimento,natureza,urgencia,dataEmissao,idGuia,
                                     atendimentoRN,nome_cortesia);
            fetch cr_incidencias_mes into qtde_usada;
            close cr_incidencias_mes;                                       -- Essa clausula foi colocada para tratar a seguinte situa��o: O pedido de exame foi emitido com critica de estouro de limite,
                                                                            -- e quando v�o confirmar tambem da a critica... isso n�o pode.
            if (nvl(qtde_usada,0) + proc.quantidade > nvl(qtde_mes,0)) and ((nvl(idGuiaPedido,0) = 0) or (nvl(idGuiaPedido,0) > 0 and TemCriticaNaGuia(nvl(idGuiaPedido,0),25) = 'N' ) ) then
              guia.insereCritica(idGuia,proc.procedimento,25,idOperador);
              if (guia_negada <> 'S' and configuracao.apelido_operadora <> 'universal') then
                update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
              end if;  
            end if;
          end if;

          if nvl(qtde_ano,0) > 0 then
            open cr_incidencias_ano (idBeneficiario,proc.procedimento,natureza,urgencia,dataEmissao,idGuia,
                                     atendimentoRN,nome_cortesia);
            fetch cr_incidencias_ano into qtde_usada;
            close cr_incidencias_ano;
            if (nvl(qtde_usada,0) + proc.quantidade > nvl(qtde_ano,0)) and ((nvl(idGuiaPedido,0) = 0) or (nvl(idGuiaPedido,0) > 0 and TemCriticaNaGuia(nvl(idGuiaPedido,0),25) = 'N' ) ) then
              guia.insereCritica(idGuia,proc.procedimento,25,idOperador);
              if (guia_negada <> 'S' and configuracao.apelido_operadora <> 'universal') then
                update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
              end if;  
            end if;
          end if;

          if (nvl(qtde,0) > 0 and nvl(dias,0) > 0) and ((nvl(idGuiaPedido,0) = 0) or (nvl(idGuiaPedido,0) > 0 and TemCriticaNaGuia(nvl(idGuiaPedido,0),25) = 'N' ) ) then
            open cr_incidencias_dia (idBeneficiario,proc.procedimento,dias,natureza,urgencia,dataEmissao,idGuia,
                                     atendimentoRN,nome_cortesia);
            fetch cr_incidencias_dia into qtde_usada;
            close cr_incidencias_dia;
            if nvl(qtde_usada,0) + proc.quantidade > nvl(qtde,0) then
              guia.insereCritica(idGuia,proc.procedimento,25,idOperador);
              if (guia_negada <> 'S' and configuracao.apelido_operadora <> 'universal') then
                update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
              end if; 
            end if;
          end if;

          if (((nvl(auditoria,'N') = 'S') or
               (nvl(auditoria,'N') = 'A' and natureza = 'A') or
               (nvl(auditoria,'N') = 'I' and natureza = 'I')) or (proc.proc_audit = 'S')) and (tipoGuia <> 'guiaOdonto') then
            guia.insereCritica(idGuia,proc.procedimento,7,idOperador);
            update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
          end if;

          if localEmissao in ('5','6','7') then
            if tipo = 'C' then
              open cr_guias_mesmo_dia(idBeneficiario,nvl(localAtendimento,executante),trunc(dataEmissao),idGuia,atendimentoRN,nome_cortesia);
              fetch cr_guias_mesmo_dia into qtde_usada;
              close cr_guias_mesmo_dia;
            else
              if proc.nnumedente > 0 then
                open cr_guias_mesmo_dia_odonto(idBeneficiario,nvl(localAtendimento,executante),proc.procedimento,proc.nnumedente,proc.nnumeface,trunc(dataEmissao),idGuia,atendimentoRN,nome_cortesia);
                fetch cr_guias_mesmo_dia_odonto into qtde_usada;
                close cr_guias_mesmo_dia_odonto;
              else
                open cr_guias_mesmo_dia_proc(idBeneficiario,nvl(localAtendimento,executante),trunc(dataEmissao),proc.procedimento,idGuia,atendimentoRN,nome_cortesia);
                fetch cr_guias_mesmo_dia_proc into qtde_usada;
                close cr_guias_mesmo_dia_proc;
              end if;
            end if;

            if nvl(qtde_usada,0) > 0 then
              guia.insereCritica(idGuia,proc.procedimento,44,idOperador);
            end if;

            -- Verifica se houve emissao de uma mesma guia nos ultimos 5 min
            -- evitando a libera��o de guia em duplicidade
            open cr_guias_mesmo_dia2(idBeneficiario,proc.procedimento,idEspecialidade,nvl(localAtendimento,executante),dataEmissao,idGuia,atendimentoRN,nome_cortesia);
            fetch cr_guias_mesmo_dia2 into qtde_usada;
            close cr_guias_mesmo_dia2;

            if nvl(qtde_usada,0) > 0 then
              guia.insereCritica(idGuia,proc.procedimento,50,idOperador);
            end if;

          end if;

        end loop;
      end if;
    end if;
    
    Processo := PROCESSO_AUDITORIA_GUIA (idGuia);

    if ( intercambio = 'N') then
      -- Insere critica de usuario falecido
      if (situacaoUsuario = 'F') then
        guia.insereCritica(idGuia,'',49,idOperador);
      else        
        for taxa in (select hssdgui.nnumetaxa,hsstaxa.cauditaxa
                       from hssdgui,hsstaxa
                      where hssdgui.nnumeguia = idGuia
                        and hssdgui.nnumetaxa = hsstaxa.nnumetaxa ) loop

          if localAtendimento > 0 then
            -- Verifica se o benefici�rio est� em car�ncia para a taxa
            temp_id := idBeneficiario;
            esta_em_carencia (temp_id,null,vencimento,carencia,null,urgencia,natureza,null,taxa.nnumetaxa,executante_temp,localAtendimento);

            if nvl(vencimento,dataEmissao) > dataEmissao then
              guia.insereCritica(idGuia,null,5,idOperador,null,null,taxa.nnumetaxa);
            end if;
          end if;
          
          auditoria := taxa.cauditaxa;
          
          if ((nvl(auditoria,'N') = 'S') or
             (nvl(auditoria,'N') = 'A' and natureza = 'A') or
             (nvl(auditoria,'N') = 'I' and natureza = 'I')) then
            guia.insereCritica(idGuia,null,7,idOperador,null,null,taxa.nnumetaxa);
            update hssguia set cstatguia = 'A' where nnumeguia = idGuia;
          end if;          

        end loop;
      end if;    
    end if;    
  end;

  procedure calcularGuia(idGuia in number) as

  coberturaConsiderada number;
  cobrancaConsiderada  varchar2(1);

  valorCopart          number;
  valor                number;
  valorCopartPrestador number;
  valorCopartHon       number;
  valorCopartCO        number;
  valorCopartFilme     number;
  cobertura            number;
  cobrancaCopart       varchar2(1);
  dataInicioLimite     date;
  situacaoGuia         varchar2(1);
  motivoCobranca       varchar2(1);
  idTabelaUsada        number;
  tetoAuditoria        number;
  total                number;
  total_procedimentos  number;
  total_baixo          number;
  id_usuario           number;
  data_emissao         date;
  teto                 number;
  teto_atendimento     number;
  tipo_teto            varchar2(1);
  teto_considerado     number;
  temOpmeMatMed        number;

  cursor cr_tem_opme_matmed is
    select sum(total) total from ( 
      select count(*) total
        from hssmmgui
       where nnumeguia= idGuia
       union all
      select count(*) total
        from hssmgui
       where nnumeguia= idGuia);
       
  cursor cr_auditoria is
    select naudiconf from hssconf;
    
  cursor cr_valorGuia is
    select sum(total) total from (
      select sum(nqindpgui) total
        from hsspgui
       where nnumeguia = idGuia
      union all
      select sum(nvalodgui) total 
        from hssdgui 
       where nnumeguia= idGuia
      union all
      select sum(nvprommgui) total 
        from hssmmgui 
       where nnumeguia= idGuia
    );
  
  cursor cr_tem_baixo_risco is
    select sum(total_baixo) total_baixo, sum(total_procedimentos) total_procedimentos from(
      select count(hsspgui.ccodipmed) total_baixo, 0 total_procedimentos
        from hsspgui,hsspmed
       where nnumeguia = idGuia
         and hsspgui.ccodipmed = hsspmed.ccodipmed
         and cpbrupmed = 'S'
       union all
      select 0 total_baixo, count(hsspgui.ccodipmed) total_procedimentos
        from hsspgui,hsspmed
       where nnumeguia = idGuia
         and hsspgui.ccodipmed = hsspmed.ccodipmed
    );
 
  begin
    carregaGuia(idGuia);

    for reg in (select hsspgui.ccodipmed,nquanpgui,ctipopmed,cvidepgui,nvl(ncontpgui,0) ncontpgui,ctipoguia
                  from hssguia,hsspgui,hsspmed
                 where hssguia.nnumeguia = idGuia
                   and hssguia.nnumeguia = hsspgui.nnumeguia
                   and hsspgui.ccodipmed = hsspmed.ccodipmed
                 order by 1) loop      
      guia.calcularProcedimentoGuia(idGuia,reg.ccodipmed,reg.ctipopmed,reg.nquanpgui,reg.cvidepgui,reg.ncontpgui,
                                    cobertura,cobrancaCopart,valorCopart,valor,valorCopartPrestador,
                                    valorCopartHon,valorCopartCO,valorCopartFilme,dataInicioLimite,situacaoGuia,
                                    motivoCobranca);

      if nvl(coberturaConsiderada,0) = 0 then
        coberturaConsiderada := cobertura;
        cobrancaConsiderada  := cobrancaCopart;
      end if;
      
      if reg.ctipoguia = 'D' then
        cobrancaCopart := 'X';
      end if;
        
      update hsspgui
         set nfranpgui  = decode(cstatpgui,'N',0,valorCopart),
             nqindpgui  = valor,
             ncoprpgui  = decode(cstatpgui,'N',0,valorCopartPrestador),
             nhonopgui  = decode(cstatpgui,'N',0,valorCopartHon),
             ncuoppgui  = decode(cstatpgui,'N',0,valorCopartCO),
             nvfilpgui  = decode(cstatpgui,'N',0,valorCopartFilme),
             nnumecober = decode(cobertura,0,null,-1,null,cobertura),
             ccobrpgui  = cobrancaCopart
       where nnumeguia  = idGuia
         and ccodipmed  = reg.ccodipmed;

      update hssguia
         set dlimiguia = decode(dlimiguia,null,dataInicioLimite,dlimiguia),
             cstatguia = decode(cstatguia,null,situacaoGuia,cstatguia),
             cmcobguia = decode(cmcobguia,null,motivoCobranca,cmcobguia)
       where nnumeguia = idGuia;

    end loop;

    for reg in (select nnumedgui,nnumetaxa,nquandgui
                  from hssdgui
                 where nnumeguia = idGuia ) loop

      guia.calcularTaxaGuia(idGuia,reg.nnumetaxa,reg.nquandgui,executante_temp,idContrato,dataEmissao,
                            idBeneficiario,idCongenere,coberturaConsiderada,cobrancaConsiderada,valor,valorCopart,valorCopartPrestador);

      update hssdgui
       set nvalodgui  = valor - decode(cnpj_operadora(),'00299149000113',valorCopartPrestador,0),
           ncopadgui  = decode(cstatdgui,'N',0,valorCopart),
           ccobrdgui  = cobrancaConsiderada,
           ncoprdgui  = decode(cstatdgui,'N',0,valorCopartPrestador),
           nnumecober = decode(coberturaConsiderada,0,null,coberturaConsiderada)
     where nnumeguia = idGuia
       and nnumedgui = reg.nnumedgui;

    end loop;

    for reg in (select nnumeprodu,nnumeforn,nquanmmgui,ctipommgui,cstatmmgui,nnumetabmm
                  from hssmmgui
                 where nnumeguia = idGuia ) loop

      guia.calcularProdutoGuia(reg.nnumeprodu,reg.ctipommgui,reg.nquanmmgui,localAtendimento,dataEmissao,
                               idCongenere,idContrato,reg.nnumeforn,coberturaConsiderada,cobrancaConsiderada,
                               valor,valorCopart,idTabelaUsada);

      if (reg.nnumetabmm > 0) then
        idTabelaUsada := reg.nnumetabmm;
      end if;
      
      update hssmmgui
         set nvprommgui = valor,
             nvpagmmgui = decode(cmanummgui,'A',valor,nvpagmmgui),
             nvsolmmgui = decode(cmanummgui,'A',valor,nvpagmmgui),
             ncopammgui = decode(cstatmmgui,'N',0,valorCopart),
             nnumecober = decode(coberturaConsiderada,0,null,coberturaConsiderada),
             ccobrmmgui = nvl(cobrancaConsiderada,'X'),
             nnumetabmm = idTabelaUsada
       where nnumeguia  = idGuia
         and nnumeprodu = reg.nnumeprodu;
         
      update hssguia
         set cstatguia = decode(cstatguia,null,decode(reg.cstatmmgui,'L',null,reg.cstatmmgui),cstatguia)
       where nnumeguia = idGuia;         

    end loop;

    for reg in (select nnumepacot,nquanpcgui
                  from hsspcgui
                 where nnumeguia = idGuia ) loop

      guia.calcularPacoteGuia(idGuia,reg.nnumepacot,reg.nquanpcgui,dataEmissao,coberturaConsiderada,cobrancaConsiderada,valorCopart);

      update hsspcgui
         set ncopapcgui = decode(cstatpcgui,'N',0,nvl(valorCopart,0)),
             ccobrpcgui = cobrancaConsiderada
       where nnumeguia  = idGuia
         and nnumepacot = reg.nnumepacot;

    end loop;
    
    recalcula_copart_pacote(idGuia);
     
    -- calculo do teto por atendimento
    teto_considerado := 0;
    coberturaConsiderada := 0;
    for reg in (select hssguia.nnumeguia, demisguia, nnumeusua, hsspgui.nnumecober
                  from hssguia, hsspgui
                 where hssguia.nnumeguia = idGuia
                   and hssguia.nnumeguia = hsspgui.nnumeguia
                 union all
                select hssguia.nnumeguia, demisguia, nnumeusua, hssdgui.nnumecober
                  from hssguia, hssdgui
                 where hssguia.nnumeguia = idGuia
                   and hssguia.nnumeguia = hssdgui.nnumeguia
                 union all
                select hssguia.nnumeguia, demisguia, nnumeusua, hssmmgui.nnumecober
                  from hssguia, hssmmgui
                 where hssguia.nnumeguia = idGuia
                   and hssguia.nnumeguia = hssmmgui.nnumeguia) loop
      id_usuario := reg.nnumeusua;
      data_emissao := reg.demisguia;
                  
      teto_cobertura (reg.nnumecober,reg.demisguia,reg.nnumeusua,teto,teto_atendimento,tipo_teto);
      
      if teto_considerado < teto_atendimento then
        teto_considerado := teto_atendimento;
        coberturaConsiderada := reg.nnumecober;
      end if;            
    end loop;    
    if teto_considerado > 0 then
      Teto_Atendimento_Guia(idGuia, data_emissao, coberturaConsiderada, id_usuario);
    end if;

    
    open cr_auditoria;
    fetch cr_auditoria into tetoAuditoria;
    close cr_auditoria;
  
    -- Quando todos os procedimentos da guia sao de Baixo Risco o mesmo n�o pode entrar na rotina de auditoria por teto.
    open cr_tem_baixo_risco;
    fetch cr_tem_baixo_risco into total_baixo, total_procedimentos;
    close cr_tem_baixo_risco;
  
    open cr_valorGuia;
    fetch cr_valorGuia into total;
    close cr_valorGuia;    

    -- Caso exista OPME ou MATMED na guia Unimed Barra quer que caia pra auditoria
    if (cnpj = '37436920000167') then
      open cr_tem_opme_matmed;
        fetch cr_tem_opme_matmed into temOpmeMatMed;
      close cr_tem_opme_matmed;
      
      if (temOpmeMatMed > 0) then
        update hssguia set cstatguia = 'A'
         where nnumeguia = idGuia;
      end if;  
    end if;  
    
    -- Verifica se tem teto por valor para auditoria
    if ( (intercambio = 'N') and 
         (nvl(total,0) > nvl(tetoAuditoria,0)) and 
         (nvl(tetoAuditoria,0) > 0) and 
         ((total_baixo <> total_procedimentos) or (total_procedimentos = 0)) ) then 
      update hssguia set cstatguia = 'A'
       where nnumeguia = idGuia;
    end if;
  end;

  procedure calcularProcedimentoGuia(idGuia             in     number,
                                     procedimento       in     varchar2,
                                     tipoProcedimento   in     varchar2,
                                     quantidade         in     number,
                                     usoDeVideo         in     varchar2,
                                     contraste          in     number,
                                     cobertura          in out number,
                                     cobrancaFranquia   in out varchar2,
                                     valorFranquia         out number,
                                     valorProcedimento     out number,
                                     copartPrestador       out number,
                                     valorFranquiaHon      out number,
                                     valorFranquiaCO       out number,
                                     valorFranquiaFilme    out number,
                                     dataInicioLimite      out date,
                                     situacaoGuia          out varchar2,
                                     motivoCobranca        out varchar2)  as

  anestesista                   number;
  aposExtrapolarLimite          varchar2(1);
  atitudeEstouroLimite          varchar2(1);
  auxiliar1                     number;
  auxiliar2                     number;
  auxiliar3                     number;
  auxiliar4                     number;
  calculo_pospag				        varchar2(1);
  cobrancaCopartStatus          varchar2(1);
  cobranca_PosPag               varchar2(1);
  consumo                       number;
  consumoMes                    number;
  consumoQtde                   number;
  contrato						          number;
  convenioCO                    number;
  convenioFilme                 number;
  convenioHonorario             number;
  copartPrestadorPercentual     number;
  copartPrestadorValor          number;
  custoOperacional              number;
  eh_cooperado					        varchar2(1);
  emissao						            date;
  filme                         number;
  franquiaValor                 number;
  honorario                     number;
  idLimite                      number;
  idLimitePreenchido            number;
  idStatus                      number;
  indiceCopart                  number;
  indiceCoparticipacao          number;
  tabelaCoparticipacao_tmp      number;
  tabelaCoparticipacao2         number;
  tabelaCoparticipacao3         number;
  indiceLimite                  number;
  limiteQuantidade              number;
  limiteQuantidadeMensal        number;
  limiteValor                   number;
  limiteValorMensal             number;
  parteCoberta                  number;
  percAcresLimite               number;
  percentualFranquia            number;
  prestador					    number;
  quantidadePreenchidaAntes     number;
  restante                      number;
  tabelaCoparticipacao          number;
  tabelaPadrao                  number;
  tabela_PosPag                 number;
  tabelaUsada                   number;
  teto                          number;
  tipoLimite                    varchar2(1);
  tipoValorCoparticipacao       varchar2(1);
  valorBaseCopart               number;
  valorBaseCopartCO             number;
  valorBaseCopartFilme          number;
  valorBaseCopartHon            number;
  valorHonorarioPago            number;
  valorIndiceLimite             number;
  valorPreenchido               number;
  valorProcedimentoIndice       number;
  valorTabela                   number;
  valorTabelaCO                 number;
  valorTabelaFilme              number;
  valorTabelaHon                number;
  QtdBaseEstouroLimite          number;
  idOperador                    number;
  ValorTotal                    number;
  naoConfirmado                 number;
  estourouLimite                varchar2(1);
  
  cursor cr_cadastro is
  select demisguia,decode(hssusua.nnumetitu,null,hssnusua.nnumetitu,hssusua.nnumetitu) nnumetitu,hssguia.nnumepres,hssguia.noperusua        
    from hssguia,hssusua,hsspmed,hssnusua,hsstitu
   where hssguia.nnumeguia  = idGuia
     and hssguia.nnumeusua  = hssusua.nnumeusua(+)
     and hsspmed.ccodipmed  = procedimento
     and hssguia.nnumenusua = hssnusua.nnumenusua(+)
     and hssusua.nnumetitu  = hsstitu.nnumetitu(+);
 
  cursor cr_pospag (contrato in number,
                   dataBase in date) is
  select decode(eh_cooperado,'S',nvl(hssposp.ncooptabe,hssposp.nnumetabe),hssposp.nnumetabe),ccposposp,cfcppposp
    from hssposp
   where nnumetitu = contrato
     and dvigeposp = (select max(pospag.dvigeposp)
                        from hssposp pospag
                       where pospag.nnumetitu = hssposp.nnumetitu
                         and pospag.dvigeposp <= dataBase);

  cursor cr_proc_tabela (tabela       in number,
                         procedimento in varchar2) is
    select nnumetabe 
      from hssvpro
     where ccodipmed = procedimento
       and nnumetabe = tabela;
	   
  cursor cr_prestador (id_prestador in number) is
  select decode(ccredpres,'O','S','N')
    from finpres
   where nnumepres = id_prestador;	   
    
  cursor cr_tipoValorCoparticipacao (cobertura in number,
                                     usuario   in number,
                                     emissao   in date) is
    select nvl(cvcopcober,'T'),
           nvl(hssvcopa.nnumetabe,hssconf.nnumetabe),
           nvl(hssvcopa.nnumeinde,hsstabe.nnumeinde),
           hssconf.nnumetabe,
           hssvcopa.nnumetabe,
           hssvcopa.nsegutabe           
      from hssusua,hsstitu,hsscober,hssvcopa,hssconf,hsstabe
     where hssusua.nnumeusua = usuario
       and hssusua.nnumetitu = hsstitu.nnumetitu
       and hsscober.nnumecober = cobertura
       and hsscober.nnumecober = hssvcopa.nnumecober
       and hssusua.nnumetitu = nvl(hssvcopa.nnumetitu,hssusua.nnumetitu)
       and ( (nanoavcopa = to_number(to_char(dconttitu,'YYYY')) and nmesavcopa is null) or
             (nanoavcopa = to_number(to_char(dconttitu,'YYYY')) and nmesavcopa = to_number(to_char(dconttitu,'MM'))) or
             (nanoavcopa is null and nmesavcopa is null) )
       and dvigevcopa = (select max(dvigevcopa) from hssvcopa
                          where nnumecober = cobertura
                            and nvl(hssvcopa.nnumetitu,hssusua.nnumetitu) = hssusua.nnumetitu
                            and dvigevcopa <= emissao)
                            
       and hssconf.nnumetabe = hsstabe.nnumetabe
     order by hssvcopa.nnumetitu,nanoavcopa,nmesavcopa,dvigevcopa desc;

  cursor cr_alteracao(guia in number, procedimento in varchar2) is
    select nnumelimi,nvl(nquanpgui,0),nvl(nqindpgui,0)
      from hsspgui
     where nnumeguia = guia
       and ccodipmed = procedimento;

  cursor cr_status is
    select ccopastat
      from hssstat
     where nnumestat = idStatus;

  cursor cr_teto_franquia is
    -- Esse nvl no nnumeinde nao pode ser feito porque o default nessa tela �
    -- se o indice nao for informado o valor � considerado sempre em reais
    --  select nvl(ntetovcopa,0),nvl(hssvcopa.nnumeinde,hsstabe.nnumeinde)
    select nvl(ntetovcopa,0),hssvcopa.nnumeinde
      from hssusua,hsstitu,hssvcopa,hssconf,hsstabe
     where hssusua.nnumeusua = idBeneficiario
       and hssusua.nnumetitu = hsstitu.nnumetitu
       and hssvcopa.nnumecober = cobertura
       and hssusua.nnumetitu = nvl(hssvcopa.nnumetitu,hssusua.nnumetitu)
       and ( (nanoavcopa = to_number(to_char(dconttitu,'YYYY')) and nmesavcopa is null) or
             (nanoavcopa = to_number(to_char(dconttitu,'YYYY')) and nmesavcopa = to_number(to_char(dconttitu,'MM'))) or
             (nanoavcopa is null and nmesavcopa is null) )
       and dvigevcopa <= dataEmissao
       and hssconf.nnumetabe = hsstabe.nnumetabe
     order by hssvcopa.nnumetitu,nanoavcopa,nmesavcopa,dvigevcopa desc;

  begin
  
    open cr_cadastro;
    fetch cr_cadastro into emissao,contrato,prestador,idOperador;
    close cr_cadastro;

    open cr_prestador(prestador);
    fetch cr_prestador into eh_cooperado;
    if cr_prestador%notfound then
      eh_cooperado := 'N';
    end if;
    close cr_prestador;	
	
    open cr_pospag(contrato,emissao);
    fetch cr_pospag into tabela_pospag,cobranca_pospag,calculo_pospag;
    close cr_pospag;
	
    valorTabelaHon   := honorario_tabela_padrao (procedimento,tabelaPrestador,dataEmissao,valorTabelaCO,valorTabelaFilme,
                                                 anestesista,auxiliar1,auxiliar2,auxiliar3,auxiliar4);
    valorTabelaHon   := nvl(valorTabelaHon,0);
    valorTabelaCO    := nvl(valorTabelaCO,0);
    valorTabelaFilme := nvl(valorTabelaFilme,0);
    valorTabela      := valorTabelaHon + valorTabelaCO + valorTabelaFilme;

    if valorNaGuia = 'T' then
      -- Valor de tabela
      honorario        := valorTabelaHon;
      custoOperacional := valorTabelaCO;
      filme            := valorTabelaFilme;
    elsif valorNaGuia = 'P' then
      -- Negociado com o prestador
      honorarioTabelaProcedimento (idBeneficiario,idPlano,procedimento,executante,natureza,urgencia,idAcomodacao,dataEmissao,
                                   null,localAtendimento,idEspecialidade,honorario,custoOperacional,filme,
                                   null,null,hora,valorHonorarioPago,tabelaUsada);
    elsif valorNaGuia = 'L' then
      -- Negociado com o local de atendimento
      honorarioTabelaProcedimento (idBeneficiario,idPlano,procedimento,localAtendimento,natureza,urgencia,idAcomodacao,dataEmissao,
                                   null,localAtendimento,idEspecialidade,honorario,custoOperacional,filme,
                                   null,null,hora,valorHonorarioPago,tabelaUsada);
    end if;

    honorario               := nvl(honorario,0);
    custoOperacional        := nvl(custoOperacional,0);
    filme                   := nvl(filme,0);
    valorHonorarioPago      := nvl(valorHonorarioPago,0);
    valorProcedimento       := honorario + custoOperacional + filme + valorHonorarioPago;

    if usoDeVideo = 'S' then
      valorProcedimento := round(honorario * 1.5,2) + custoOperacional + filme + valorHonorarioPago;
    end if;

    if idNaousuario > 0 and idConvenio > 0 then
      convenioHonorario := honorarioProcedimentoConvenio (procedimento,idConvenio,dataEmissao,convenioCO,conveniofilme) * quantidade;
      if convenioHonorario is not null then
        valorProcedimento := round(convenioHonorario * (1 + (nvl(percentualConvenio,0)/100)),2);
      end if;
    end if;

    -- ****** inicio calculo co-participacao ******

    -- guias de cortesia
    if (idBeneficiario = 0) and (idNaoUsuario = 0) then
      -- Cortesias com valor zerado
      if formaCalculo = '5' then
        -- D�BITO DE PCMSO e GUIA CORTESIA para CARLOS CHAGAS sempre usava o a forma de c�lculo como 6
        -- por�m o D�BITO DE PCMSO a decisao dever ser igual a 3 e GUIA CORTESIA = 1 por isso inseri a rotina aqui
        if (cnpj = '88578158000194') or (cnpj = '04109859000194') or (cnpj = '07852094000176') then
          valorFranquiaHon   := valor_Cortesia_Contrato(procedimento,contratoCortesia,dataEmissao);
          valorFranquiaCO    := 0;
          valorFranquiaFilme := 0;
          motivoCobranca     := '7';
          cobrancaFranquia   := 'C';

          if nvl(valorFranquiaHon,0) = 0 then
            valorFranquiaHon := (valorProcedimento * quantidade) + contraste;
          end if;
        else
          valorFranquiaHon   := 0;
          valorFranquiaCO    := 0;
          valorFranquiaFilme := 0;
          cobrancaFranquia   := '';
        end if;
      -- Cortesias com valor de tabela
      elsif formaCalculo = '6' then
        valorFranquiaHon   := valor_Cortesia_Contrato(procedimento,contratoCortesia,dataEmissao);
        valorFranquiaCO    := 0;
        valorFranquiaFilme := 0;
        motivoCobranca     := '7';

        if nvl(valorFranquiaHon,0) = 0 then
          valorFranquiaHon := (valorProcedimento * quantidade) + contraste;
        end if;

        if (cnpj = '88578158000194') or (cnpj = '04109859000194') or (cnpj = '07852094000176') then
          cobrancaFranquia   := 'M';
        else
          cobrancaFranquia   := 'C';
        end if;
      else
        valorFranquiaHon   := valorProcedimento * quantidade;
        valorFranquiaCO    := 0;
        valorFranquiaFilme := 0;
      end if;

    -- guias para nao-usuarios (conveniados)
    elsif idNaousuario > 0 then
      valorFranquiaHon   := valorProcedimento * quantidade;
      valorFranquiaCO    := 0;
      valorFranquiaFilme := 0;
      motivoCobranca     := '8';
    elsif (formaCalculo = '2') or   -- guias de urgencia
          (formaCalculo = '4') or   -- guias para usuarios internados
          (formaCalculo = '7') then -- guias com valor zerado e sem baixar limites
      valorFranquiaHon   := 0;
      valorFranquiaCO    := 0;
      valorFranquiaFilme := 0;
    elsif formaCalculo = '3' then -- guias para usuarios a preco de tabela sem baixar limite
      valorFranquiaHon   := (valorTabela * quantidade) + contraste;
      valorFranquiaCO    := 0;
      valorFranquiaFilme := 0;
      motivoCobranca     := '9';
    elsif (idBeneficiario > 0) and (formaCalculo in ('1','8','9','C')) then

      idLimite := retorna_limite (idBeneficiario,procedimento,tipoLimite,limiteQuantidade,
                                  limiteQuantidadeMensal,limiteValor,
                                  limiteValorMensal,indiceLimite,atitudeEstouroLimite,idEspecialidade,
                                  percAcresLimite,idCid,natureza,urgencia,localAtendimento,dataEmissao);

      if idLimite > 0 then
        dataInicioLimite := inicio_contagem_limites (idBeneficiario,idLimite,dataEmissao,idEspecialidade);
      end if;

      -- Atitude
      -- 0 - Bloquear
      -- 3 - Bloquear exceto na web
      -- 1 - Cobrar no caixa
      -- 2 - Cobrar na mensalidade
      -- 5 - Cobrar no caixa ou na mensalidade
      -- 9 - enviar para auditoria

      cobertura := cobertura_usuario(idBeneficiario,procedimento,natureza||urgencia,executante_temp,localAtendimento,
                                     cobrancaFranquia,dataEmissao,idEspecialidade,acidenteDeTrabalho,null,idadeBeneficiario);

      if cobertura = 0 then
        cobertura := cobertura_aditivo (idBeneficiario,procedimento,cobrancaFranquia,franquiaValor,percentualFranquia);
      end if;

      consumoQtde := quantidade;

      if cobertura > 0 then

        if (cobrancaFranquia in ('D','Y','B','G','K','N')) then

          consumoQtde := total_consumo(idLimite,idBeneficiario,dataInicioLimite,dataEmissao,idGuia,procedimento) + quantidade;
          if consumoQtde <= 0 then
            consumoQtde := quantidade;
          end if;
        end if;

        open cr_tipoValorCoparticipacao(cobertura,idBeneficiario,dataEmissao);
        fetch cr_tipoValorCoparticipacao into tipoValorCoparticipacao,tabelaCoparticipacao,indiceCoparticipacao,tabelaPadrao,tabelaCoparticipacao2,tabelaCoparticipacao3;
        close cr_tipoValorCoparticipacao;

        -- Coparticipacao sobre valor faturado
        if tipoValorCoparticipacao = 'F' then
          HonorarioTabelaProcedimento (idBeneficiario,idPlano,procedimento,executante_temp,natureza,urgencia,idAcomodacao,dataEmissao,null,
                                       localAtendimento,idEspecialidade,valorBaseCopartHon,valorBaseCopartCO,valorBaseCopartFilme,
                                       null,null,hora,valorHonorarioPago,tabelaUsada);
        else
        -- Coparticipacao sobre valor de tabela
        
          tabelaCoparticipacao_tmp := null;
          
          if tabelaCoparticipacao3 is null then
            valorTabelaHon   := honorario_tabela_padrao (procedimento,tabelaCoparticipacao,dataEmissao,valorTabelaCO,valorTabelaFilme,
                                                         anestesista,auxiliar1,auxiliar2,auxiliar3,auxiliar4,
                                                         null,null,null,null,null,null,null,IndiceCoparticipacao);
          else 
            open cr_proc_tabela(tabelaCoparticipacao2,procedimento);
            fetch cr_proc_tabela into tabelaCoparticipacao_tmp;
            close cr_proc_tabela; 
            
            if nvl(tabelaCoparticipacao_tmp,0) > 0 then
              valorTabelaHon   := honorario_tabela_padrao (procedimento,tabelaCoparticipacao2,dataEmissao,valorTabelaCO,valorTabelaFilme,
                                                           anestesista,auxiliar1,auxiliar2,auxiliar3,auxiliar4,
                                                           null,null,null,null,null,null,null,IndiceCoparticipacao);           
            else
              open cr_proc_tabela(tabelaCoparticipacao3,procedimento);
              fetch cr_proc_tabela into tabelaCoparticipacao_tmp;
              close cr_proc_tabela;  
              
              if nvl(tabelaCoparticipacao_tmp,0) > 0 then
                valorTabelaHon   := honorario_tabela_padrao (procedimento,tabelaCoparticipacao3,dataEmissao,valorTabelaCO,valorTabelaFilme,
                                                             anestesista,auxiliar1,auxiliar2,auxiliar3,auxiliar4,
                                                             null,null,null,null,null,null,null,IndiceCoparticipacao);           
              else
                valorTabelaHon   := honorario_tabela_padrao (procedimento,TabelaCoparticipacao,dataEmissao,valorTabelaCO,valorTabelaFilme,
                                                             anestesista,auxiliar1,auxiliar2,auxiliar3,auxiliar4,
                                                             null,null,null,null,null,null,null,IndiceCoparticipacao);                        
              end if;
            end if;              
          end if; 
        
          valorTabelaHon   := nvl(valorTabelaHon,0);
          valorTabelaCO    := nvl(valorTabelaCO,0);
          valorTabelaFilme := nvl(valorTabelaFilme,0);
        
          valorBaseCopartHon   := nvl(valorTabelaHon,0);
          valorBaseCopartCO    := nvl(valorTabelaCO,0);
          valorBaseCopartFilme := nvl(valorTabelaFilme,0);
        end if;

        valorBaseCopartHon   := nvl(valorBaseCopartHon,0);
        valorBaseCopartCO    := nvl(valorBaseCopartCO,0);
        valorBaseCopartFilme := nvl(valorBaseCopartFilme,0);
        valorBaseCopart      := valorBaseCopartHon + valorBaseCopartCO + valorBaseCopartFilme;

        valor_copart(cobertura,'2',consumoQtde,valorBaseCopart,dataEmissao,executante_temp,idEspecialidade,franquiaValor,
                     percentualFranquia,parteCoberta,idBeneficiario,solicitante,cnpj,urgencia,tipoProcedimento,
                     procedimento,localAtendimento,null,idGuia,null,tabela_taxa);

        -- Cobranca de coparticipacao diretamente no prestador de servi�os
        if ((cobrancaFranquia = 'H') or (cobrancaFranquia = 'K' and localEmissao not in ('1','2','3','4','6') and (nvl(incidenciaCopartLocal,'1') <> '5') ) ) and
           (nvl(IncidenciaCopartLocal,'1') <> '8') then  -- N�o receber co-participa��o no prestador, por�m cobr�-la na mensalidade     
          copartPrestadorValor      := nvl(franquiaValor,0);
          copartPrestadorPercentual := nvl(percentualFranquia,0);
          percentualFranquia        := 0;
          franquiaValor                 := 0;
        else
          percentualFranquia    := nvl(percentualFranquia,0);
          franquiaValor             := nvl(franquiaValor,0);
          copartPrestadorValor         := 0;
          copartPrestadorPercentual := 0;
        end if;
        
        if (nvl(IncidenciaCopartLocal,'1') = '8') then -- N�o receber co-participa��o no prestador, por�m cobr�-la na mensalidade
          cobrancaFranquia := 'F';
        end if;

        if (cobrancaFranquia = 'X') or (cobrancaFranquia = 'F') or (cobrancaFranquia = 'L') or
           (cobrancaFranquia = 'B') or (cobrancaFranquia = 'Y') then
          aposExtrapolarLimite := 'S';

          if franquiaValor > 0 then
            valorBaseCopart      := franquiaValor;
            valorBaseCopartHon   := franquiaValor;
            valorBaseCopartCO    := 0;
            valorBaseCopartFilme := 0;
          end if;
        else
          aposExtrapolarLimite := 'N';
        end if;
      else
        valorBaseCopartHon   := nvl(valorTabelaHon,0);
        valorBaseCopartCO    := nvl(valorTabelaCO,0);
        valorBaseCopartFilme := nvl(valorTabelaFilme,0);
      end if;

      -- Nao encontrei forma de parametrizar. O caso � o seguinte, Na unimed franca quando a guia for de encaminhamento ou retorno na UE
      -- nao se deve cobrar a copart e tambem nao se deve computar limites
      if cnpj = '45309606000141' and idLimite > 0 and
         RetornoEncaminhamentoUnimed (idBeneficiario,idGuia,null,dataEmissao,solicitante,localAtendimento,
                                      tipoProcedimento,urgencia,procedimento) = 'S' then
        idLimite := 0;
      end if;

      -- Tipo_limite pode ser: L - Limitado por usuario sem cobran�a at� o limite
      --                       F - Limitado por familia sem cobranca at� o limite
      --                       U - Limitado por familia por faixa de usuarios na familia
      --                       C - Limitado para o contrato todo
      if idLimite > 0 then

        if formaCalculo = '1' then
          update hssguia set dlimiguia = dataInicioLimite
           where nnumeguia = idGuia;
          update hsspgui set nnumelimi = decode(idLimite,0,null,idLimite)
           where nnumeguia = idGuia
             and ccodipmed = procedimento;  
        elsif formaCalculo = '8' then
          dataInicioLimite := add_months(dataInicioLimite,12);
          update hssguia set dlimiguia = dataInicioLimite
           where nnumeguia = idGuia;
          update hsspgui set nnumelimi = decode(idLimite,0,null,idLimite)
           where nnumeguia = idGuia
             and ccodipmed = procedimento;           
        elsif formaCalculo = '9' then
          dataInicioLimite := add_months(dataInicioLimite,1);
          update hssguia set dlimiguia = dataInicioLimite
           where nnumeguia = idGuia;
          update hsspgui set nnumelimi = decode(idLimite,0,null,idLimite)
           where nnumeguia = idGuia
             and ccodipmed = procedimento;          
        else
          dataInicioLimite := dataEmissao;
          update hssguia set dlimiguia = dataEmissao
           where nnumeguia = idGuia;
          update hsspgui set nnumelimi = decode(idLimite,0,null,idLimite)
           where nnumeguia = idGuia
             and ccodipmed = procedimento;
        end if;

        if tipoLimite = 'L' then
          consumo := nvl(totalConsumoLimite (idLimite,idBeneficiario,dataInicioLimite,dataEmissao,idGuia,procedimento,idGuia,idEspecialidade,naoConfirmado),0);
        elsif tipoLimite = 'C' then
          consumo := total_consumo_contrato (idLimite,idBeneficiario,dataInicioLimite,dataEmissao,idEspecialidade,idGuia,procedimento);
        else
          consumo := nvl(totalConsumoIntercambiavel(idLimite,idBeneficiario,dataInicioLimite,dataEmissao,idEspecialidade,idGuia,procedimento,idGuia,naoConfirmado),0);
        end if;

        consumo := consumo + nvl(naoConfirmado,0);

        -- Calculo especifico para o Hospitalar
        if (cnpj = '78613841000161') and (idPlano = 182 or idPlano = 246) and
           (substr(procedimento,1,2) = '33' or substr(procedimento,1,2) = '25' or
            substr(procedimento,1,2) = '34' or procedimento = '22010203' or procedimento = '22010190') then
          if consumo < limiteQuantidade then
            percentualFranquia := 0;
          else
            idLimite := retorna_limite (idBeneficiario,'28000000',tipoLimite,limiteQuantidade,
                                        limiteQuantidadeMensal,limiteValor,
                                        limiteValorMensal,indiceLimite,atitudeEstouroLimite,idEspecialidade,
                                        percAcresLimite,idCid,natureza,urgencia,localAtendimento,dataEmissao);

            cobertura := cobertura_usuario(idBeneficiario,'28000000',natureza||urgencia,executante_temp,localAtendimento,
                                           cobrancaFranquia,dataEmissao,idEspecialidade,acidenteDeTrabalho,null,idadeBeneficiario);

            -- Se na configuracao do sistema estiver indicado que � pra se cobrar a vista a copart de beneficiarios cancelados
            -- e for o caso, ou seja, realmente o usuario est� cancelado ou com programacao de cancelamento
            -- Marcamos a cobranca_franquia com A - Caixa ou mensalidade
            if cobrancaCancelados = '2' and situacaoUsuario <> 'A' then
              cobrancaFranquia := 'A';
            end if;

            if cobertura > 0 then
              valor_copart(cobertura,'2',quantidade,valorProcedimento,dataEmissao,executante,idEspecialidade,franquiaValor,percentualFranquia,
                           parteCoberta,idBeneficiario,solicitante,cnpj,urgencia,tipoProcedimento,procedimento,localAtendimento,null,idGuia,null,tabela_taxa);
              percentualFranquia := nvl(percentualFranquia,0);
              franquiaValor      := nvl(franquiaValor,0);
            end if;

            consumo := nvl(totalConsumoLimite (idLimite,idBeneficiario,dataInicioLimite,dataEmissao,idGuia,procedimento,idGuia,idEspecialidade,naoConfirmado),0);
            consumo := nvl(consumo,0) + nvl(naoConfirmado,0);
          end if;
        end if;
        -- Fim do calculo especifico para o Hospitalar

        valorIndiceLimite       := valor_indice(indiceLimite,dataEmissao);
        valorProcedimentoIndice := round(valorProcedimento / valorIndiceLimite,2) * quantidade;
        estourouLimite          := 'N';

        if limiteQuantidade > 0 then -- limite por quantidade

          -- operacao de alteracao de dados da guia
          open cr_alteracao(idGuia,procedimento);
          fetch cr_alteracao into idLimitePreenchido,quantidadePreenchidaAntes,valorPreenchido;

          if (cr_alteracao%found) and nvl(idLimitePreenchido,0) = nvl(idLimite,0) and nvl(idLimite,0) > 0 then
            consumo := consumo - nvl(quantidadePreenchidaAntes,0);
          else
            quantidadePreenchidaAntes := 0;
          end if;
          close cr_alteracao;
          -- fim da operacao de alteracao de dados da guia

          restante := limiteQuantidade - consumo;
          if limiteQuantidadeMensal > 0 then
            consumoMes  := nvl(totalConsumoLimite(idLimite,idBeneficiario,first_day(dataEmissao),dataEmissao,idGuia,procedimento,idGuia,idEspecialidade,naoConfirmado),0) - quantidadePreenchidaAntes + quantidade;
            consumoMes  := consumoMes + nvl(naoConfirmado,0);
          end if;

          if restante > 0 and (nvl(franquiaValor,0) > 0 or nvl(percentualFranquia,0) > 0) then

            if quantidade > restante then

              estourouLimite := 'S';

              -- Ocorre estouro de parte do limite
              motivoCobranca := '1';

              if (atitudeEstouroLimite = 0) then
              -- Bloquear a emissao da guia
                situacaoGuia := 'X';
                guia.insereCritica(idGuia,procedimento,8,idOperador);
              elsif (atitudeEstouroLimite = 3) and (localEmissao not in ('5','6','7')) then
              -- Bloquear a emissao da guia exceto na web
                situacaoGuia := 'X';
              elsif (atitudeEstouroLimite = 1) then
                cobrancaFranquia := 'C';
              elsif (atitudeEstouroLimite = 2) then
                cobrancaFranquia := 'F';
              elsif (atitudeEstouroLimite = 5) then
                cobrancaFranquia := 'B';
              elsif (atitudeEstouroLimite = 9) then
                situacaoGuia := 'A';
              end if;

              if franquiaValor > 0 and aposExtrapolarLimite = 'N' then
                valorFranquiaHon   := round(franquiaValor * quantidade,2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              elsif percentualFranquia > 0 and aposExtrapolarLimite = 'N' then
                valorFranquiaHon   := round((percentualFranquia / 100) * ((valorBaseCopartHon   * quantidade) + contraste),2);
                valorFranquiaCO    := round((percentualFranquia / 100) *  (valorBaseCopartCO    * quantidade),2);
                valorFranquiaFilme := round((percentualFranquia / 100) *  (valorBaseCopartFilme * quantidade),2);
              elsif franquiaValor > 0 and aposExtrapolarLimite = 'S' then
                valorFranquiaHon   := round(franquiaValor * (quantidade - restante),2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              elsif percentualFranquia > 0 and aposExtrapolarLimite = 'S' then
                valorFranquiaHon   := round((percentualFranquia / 100) * ((valorBaseCopartHon   * (quantidade - restante)) + contraste),2);
                valorFranquiaCO    := round((percentualFranquia / 100) *  (valorBaseCopartCO    * (quantidade - restante) ),2);
                valorFranquiaFilme := round((percentualFranquia / 100) *  (valorBaseCopartFilme * (quantidade - restante) ),2);
              end if;
            else
              if nvl(consumoMes,0) > nvl(limiteQuantidadeMensal,0) then
                situacaoGuia := 'X';
                guia.insereCritica(idGuia,procedimento,8,idOperador);
              end if;

              if (franquiaValor > 0) and (aposExtrapolarLimite = 'N') then
                valorFranquiaHon   := round(franquiaValor * quantidade,2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
                motivoCobranca := '6';
              elsif (percentualFranquia > 0) and (aposExtrapolarLimite = 'N') then
                valorFranquiaHon   := round((percentualFranquia / 100) * ((valorBaseCopartHon   * quantidade) + contraste),2);
                valorFranquiaCO    := round((percentualFranquia / 100) *  (valorBaseCopartCO    * quantidade),2);
                valorFranquiaFilme := round((percentualFranquia / 100) *  (valorBaseCopartFilme * quantidade),2);
                motivoCobranca := '6';
              else
                valorFranquiaHon   := 0;
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              end if;
            end if;

          -- Ocorre estouro de limite e nao h� valor de copart
          elsif nvl(franquiaValor,0) = 0 and nvl(percentualFranquia,0) = 0 and nvl(copartPrestadorValor,0) = 0 and (quantidade - restante) > 0 then

            estourouLimite := 'S';
          
            -- Calcula a qntd ap�s estouro de quantidade
            if Quantidade > restante and restante > 0 then
              QtdBaseEstouroLimite := Quantidade - restante;
            else 
              QtdBaseEstouroLimite := Quantidade;
            end if;
          
            -- Ocorre estouro do limite
            motivoCobranca := '1';

            if (atitudeEstouroLimite = 0) then
            -- Bloquear a emissao da guia
              situacaoGuia := 'X';
              guia.insereCritica(idGuia,procedimento,8,idOperador);

            elsif (atitudeEstouroLimite = 3) and (localEmissao not in ('5','6','7')) then
            -- Bloquear a emissao da guia exceto na web
              situacaoGuia := 'X';
            elsif (atitudeEstouroLimite = 1) then
              cobrancaFranquia := 'C';
            elsif (atitudeEstouroLimite = 2) then
              cobrancaFranquia := 'F';
            elsif (atitudeEstouroLimite = 5) then
              cobrancaFranquia := 'B';
            elsif (atitudeEstouroLimite = 9) then
              situacaoGuia := 'A';
            end if;

            -- Cobran�a de co-participacao no caixa ou na mensalidade ap�s extrapolar limites
            valorFranquiaHon   := round((valorBaseCopartHon * QtdBaseEstouroLimite) + contraste,2);
            valorFranquiaHon   := round(valorFranquiaHon * ( (nvl(PercAcresLimite,0) / 100) + 1 ),2);
            valorFranquiaCO    := round((valorBaseCopartCO * QtdBaseEstouroLimite) * ( (nvl(PercAcresLimite,0) / 100) + 1 ),2);
            valorFranquiaFilme := round((valorBaseCopartFilme * QtdBaseEstouroLimite) * ( (nvl(PercAcresLimite,0) / 100) + 1 ),2);
          elsif (quantidade - restante) > 0 then -- Estouro de limite porem com valor de franquia > 0

            estourouLimite := 'S';
          
            -- Ocorre estouro do limite
            motivoCobranca := '1';

            if (atitudeEstouroLimite = 0) then
            -- Bloquear a emissao da guia
              situacaoGuia := 'X';
              guia.insereCritica(idGuia,procedimento,8,idOperador);
            elsif (atitudeEstouroLimite = 3) and (localEmissao not in ('5','6','7')) then
            -- Bloquear a emissao da guia exceto na web
              situacaoGuia := 'X';
            elsif (atitudeEstouroLimite = 1) then
              cobrancaFranquia := 'C';
            elsif (atitudeEstouroLimite = 2) then
              cobrancaFranquia := 'F';
            elsif (atitudeEstouroLimite = 5) then
              cobrancaFranquia := 'B';
            elsif (atitudeEstouroLimite = 9) then
              situacaoGuia := 'A';
            end if;

            if atitudeEstouroLimite in (1,2,5) then
              -- Cobranca na mensalidade, no caixa depois de estourar limite
              if nvl(percentualFranquia,0) > 0 then
                valorFranquiaHon   := round((percentualFranquia / 100) * nvl(valorBaseCopartHon,0),2);
                valorFranquiaCO    := round((percentualFranquia / 100) * nvl(valorBaseCopartCO,0),2);
                valorFranquiaFilme := round((percentualFranquia / 100) * nvl(ValorBaseCopartFilme,0),2);
              else
                valorFranquiaHon   := round(valorBaseCopartHon,2);
                valorFranquiaCO    := round(valorBaseCopartCO,2);
                valorFranquiaFilme := round(ValorBaseCopartFilme,2);
              end if;
            elsif franquiaValor > 0 then
              valorFranquiaHon   := round(franquiaValor * quantidade,2);
              valorFranquiaCO    := 0;
              valorFranquiaFilme := 0;
            else
              valorFranquiaHon   := round((percentualFranquia / 100) * ((valorBaseCopartHon   * quantidade) + contraste),2);
              valorFranquiaCO    := round((percentualFranquia / 100) *  (valorBaseCopartCO    * quantidade),2);
              valorFranquiaFilme := round((percentualFranquia / 100) *  (valorBaseCopartFilme * quantidade),2);
            end if;
          end if;
        end if;

        -- limite por valor
        if limiteValor > 0 and estourouLimite = 'N' then

          consumo := nvl(total_consumoFinanceiro (idLimite,idBeneficiario,dataInicioLimite,dataEmissao,idGuia,procedimento,idGuia,idEspecialidade),0);

          -- operacao de alteracao de dados da guia
          open cr_alteracao(idGuia,procedimento);
          fetch cr_alteracao into idLimitePreenchido,quantidadePreenchidaAntes,valorPreenchido;

          if (cr_alteracao%found) and nvl(idLimitePreenchido,0) = nvl(idLimite,0) and nvl(idLimite,0) > 0 then
            consumo := consumo - Round(valorPreenchido / valorIndiceLimite,2);
          else
            valorPreenchido := 0;
          end if;
          close cr_alteracao;
          -- fim da operacao de alteracao de dados da guia

          restante := limiteValor - consumo;
          if restante > 0 then

            if valorProcedimentoIndice > restante then
              -- Ocorre estouro de parte do limite
              motivoCobranca := '1';
            
              if (atitudeEstouroLimite = 0) then
              -- Bloquear a emissao da guia
                situacaoGuia := 'X';
                guia.insereCritica(idGuia,procedimento,8,idOperador);
              elsif (atitudeEstouroLimite = 3) and (localEmissao not in ('5','6','7')) then
              -- Bloquear a emissao da guia exceto na web
                situacaoGuia := 'X';
              elsif (atitudeEstouroLimite = 1) then
                cobrancaFranquia := 'C';
              elsif (atitudeEstouroLimite = 2) then
                cobrancaFranquia := 'F';
              elsif (atitudeEstouroLimite = 5) then
                cobrancaFranquia := 'B';
              elsif (atitudeEstouroLimite = 9) then
                situacaoGuia := 'A';
              end if;

              if (franquiaValor > 0) and (aposExtrapolarLimite = 'N') then
                valorFranquiaHon   := round(franquiaValor * quantidade,2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              elsif (percentualFranquia > 0) and (aposExtrapolarLimite = 'N') then

                if atitudeEstouroLimite in (1,2,5) then
                 -- Se a decisao for cobrar tabela quando o limite estourar
                  valorFranquiaHon := round((valorProcedimentoIndice - restante) * valorIndiceLimite,2);
                end if;

                valorFranquiaHon   := valorFranquiaHon + round((percentualFranquia / 100) * restante * valorIndiceLimite,2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              else
                valorFranquiaHon   := 0;
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              end if;
            else
              if (franquiaValor > 0) and (aposExtrapolarLimite = 'N') then
                valorFranquiaHon   := round(franquiaValor * quantidade,2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
                motivoCobranca := '6';
              elsif (percentualFranquia > 0) and (aposExtrapolarLimite = 'N') then
                valorFranquiaHon   := round( (percentualFranquia / 100) * ((valorBaseCopartHon   * quantidade) + contraste),2);
                valorFranquiaCO    := round( (percentualFranquia / 100) *  (valorBaseCopartCO    * quantidade),2);
                valorFranquiaFilme := round( (percentualFranquia / 100) *  (valorBaseCopartFilme * quantidade),2);
                motivoCobranca := '6';
              else
                valorFranquiaHon   := 0;
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              end if;
            end if;
          else
            -- Ocorre estouro do limite
            motivoCobranca := '1';

            if (atitudeEstouroLimite = 0) then
            -- Bloquear a emissao da guia
              situacaoGuia := 'X';
              guia.insereCritica(idGuia,procedimento,8,idOperador);
            elsif (atitudeEstouroLimite = 3) and (localEmissao not in ('5','6','7')) then
            -- Bloquear a emissao da guia exceto na web
              situacaoGuia := 'X';
            elsif (atitudeEstouroLimite = 1) then
              cobrancaFranquia := 'C';
            elsif (atitudeEstouroLimite = 2) then
              cobrancaFranquia := 'F';
            elsif (atitudeEstouroLimite = 5) then
              cobrancaFranquia := 'B';
            elsif (atitudeEstouroLimite = 9) then
              situacaoGuia := 'A';
            end if;

            if atitudeEstouroLimite in (1,2,5) then
              -- Cobranca na mensalidade, no caixa depois de estourar limite
              valorFranquiaHon   := round(valorBaseCopartHon,2);
              valorFranquiaCO    := round(valorBaseCopartCO,2);
              valorFranquiaFilme := round(valorBaseCopartFilme,2);
            else
              -- Se a decisao for bloquear ou enviar pra auditoria, o sistema calcula a copart normalmente
              if franquiaValor > 0 then
                valorFranquiaHon   := round(franquiaValor * quantidade,2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              else
                valorFranquiaHon   := round( (percentualFranquia / 100) * ((valorBaseCopart  * quantidade) + contraste),2);
                valorFranquiaCO    := 0;
                valorFranquiaFilme := 0;
              end if;
            end if;
          end if;
        end if;
      else
        -- procedimentos n�o-limitados
        if (percentualFranquia > 0) and (aposExtrapolarLimite = 'N') then
          valorFranquiaHon   := round((percentualFranquia / 100) * ((ValorBaseCopartHon + contraste) * quantidade),2);
          valorFranquiaCO    := round((percentualFranquia / 100) * ValorBaseCopartCO    * quantidade,2);
          valorFranquiaFilme := round((percentualFranquia / 100) * ValorBaseCopartFilme * quantidade,2);
          motivoCobranca     := '6';
        elsif (franquiaValor > 0) and (aposExtrapolarLimite = 'N') then
          valorFranquiaHon   := round(franquiaValor * quantidade,2);
          valorFranquiaCO    := 0;
          valorFranquiaFilme := 0;
          motivoCobranca     := '6';
        else
          valorFranquiaHon   := 0;
          valorFranquiaCO    := 0;
          valorFranquiaFilme := 0;
        end if;
      end if;

    end if;

    parteCoberta := nvl(parteCoberta,0) * quantidade;

    if franquiaValor + percentualFranquia = 0 then
      cobrancaFranquia := '';
    end if;

    valorFranquia := valorFranquiaHon + valorFranquiaCO + valorFranquiaFilme - parteCoberta;

    if valorFranquia < 0 then
      valorFranquia := 0;
    elsif valorFranquia is null then
      valorFranquia := 0;
    end if;

    -- Se na configuracao do sistema estiver indicado que � pra se cobrar a vista a copart de beneficiarios cancelados
    -- e for o caso, ou seja, realmente o usuario est� cancelado ou com programacao de cancelamento
    -- Marcamos a cobranca_franquia com A - Caixa ou mensalidade
    if cobrancaCancelados = '2' and situacaoUsuario <> 'A' then
      cobrancaFranquia := 'A';
    elsif idStatus is not null and cobrancaFranquia <> 'A' then
      open cr_status;
      fetch cr_status into cobrancaCopartStatus;
      close cr_status;

      if cobrancaCopartStatus = '2' then
        cobrancaFranquia := 'C';
      end if;
    end if;

    if ((cobrancaFranquia = 'C' or cobrancaFranquia = 'X' or cobrancaFranquia = 'Y') and valorFranquia > 0) then
      cobrancaFranquia := 'C';
      -- Anteriormente eu preenchia a variavel decisao se o valor_franquia > 0, porem na Cabefi o operador lanca um procedimento chamado
      -- FARMACIA cuja copart � igual a zero, ele deve ser marcado para cobrar apos a guia faturada, porque na hora da guia ser faturada
      -- � que � lancado o valor do mat/med que ele comprou. Por isso nao se pode comparar com valor_franquia > 0

    elsif cobrancaFranquia = 'B' or cobrancaFranquia = 'A' or cobrancaFranquia = 'F' then
      cobrancaFranquia := 'B';
    elsif cobrancaFranquia = 'M' or cobrancaFranquia = 'L' or cobrancaFranquia = 'G' or cobrancaFranquia = 'Y' or
          cobrancaFranquia = 'N' then
      cobrancaFranquia := 'F';
    elsif cobrancaFranquia = 'K' and localEmissao in ('1','2','3','4','5','6','7') then
      cobrancaFranquia := 'F';
    elsif cobrancaFranquia = 'E' or cobrancaFranquia = 'D' then
      cobrancaFranquia := 'M';
    end if;

    if (nvl(tabela_pospag,0) > 0) and ( (((nvl(cobertura,0) = 0) or (nvl(valorFranquiaHon,0) = 0) ) and
       (tipoOperadora = 'C')) or (tipoOperadora <> 'C')  ) then

      -- ***********   Revisar ******* --
      pospag_na_guia (modalidade,tabela_pospag,idContrato,eh_cooperado,procedimento,quantidade,contraste,
                      honorario,custoOperacional,filme,valorFranquiaHon,valorFranquiaCO,valorFranquiaFilme,dataEmissao);

      valorFranquiaHon   := nvl(valorFranquiaHon,0);
      valorFranquiaCO    := nvl(valorFranquiaCO,0);
      valorFranquiaFilme := nvl(valorFranquiaFilme,0);
      valorFranquia      := valorFranquiaHon + valorFranquiaCO + valorFranquiaFilme;

      if cobranca_pospag = 'M' and valorFranquia > 0 then
        cobrancaFranquia := 'F';
      elsif cobranca_pospag = 'E' and valorFranquia > 0 then
        cobrancaFranquia := 'M';
      elsif cobranca_pospag = 'C' and valorFranquia > 0 then
        cobrancaFranquia := 'C';
      end if;
    end if;

    if percentualFranquia > 0 and valorFranquia > 0 and cobertura > 0 then
      open cr_teto_franquia;
      fetch cr_teto_franquia into teto,indiceCopart;
      close cr_teto_franquia;

      if nvl(teto,0) > 0 then
        if (indiceCopart is not null) and nvl(Parametro_String('CCONS_INDTCOPART'),'S') = 'S' then
          teto := teto * valor_indice(indiceCopart,dataEmissao);
        end if;

        if valorFranquia > nvl(teto,0) * quantidade then
          valorFranquia := nvl(teto,0) * quantidade;
        end if;
      end if;
    end if;

    -- affego
    if copartPrestadorValor > 0 then
      copartPrestador := copartPrestadorValor * quantidade;
    elsif copartPrestadorPercentual > 0 then
      copartPrestador := Round((copartPrestadorPercentual * nvl(valorBaseCopart * quantidade,0)) / 100,2); -- "N�o alterar!"
    else
      copartPrestador := 0;
    end if;

    if copartPrestador > 0 then
      valorFranquia := 0;

      open cr_teto_franquia;
      fetch cr_teto_franquia into teto,indiceCopart;
      close cr_teto_franquia;

      if nvl(teto,0) > 0 then
        if (indiceCopart is not null) and nvl(Parametro_String('CCONS_INDTCOPART'),'S') = 'S' then
          teto := teto * valor_indice(indiceCopart,dataEmissao);
        end if;

        if copartPrestador > nvl(teto,0) * quantidade then
          copartPrestador := nvl(teto,0) * quantidade;
        end if;
      end if;
    end if;

    valorProcedimento := (valorProcedimento * quantidade); -- copartPrestador; AFFEGO N�O ESTAVA CALCULANDO O VALOR DE TABELA

    -- ****** fim do c�lculo de co-participacao
  end;

  procedure calcularTaxaGuia(idGuia               in     number,
                             idTaxa               in     number,
                             quantidade           in     number,
                             idPrestador          in     number,
                             idContrato           in     number,
                             dataEmissao          in     date,
                             idBeneficiario       in     number,
                             idCongenere          in     number,
                             cobertura            in out number,
                             cobrancaFranquia     in out varchar2,
                             valorTaxa               out number,
                             valorCopart             out number,
                             copartPrestador         out number) as

  teto                       number;
  percentualfranquia         number;
  franquiaValor              number;
  taxaAdm                    number;
  cobrancaAdm                varchar2(1);
  cobrancaTaxa               varchar2(1);
  idTemp                     number;
  tipoTaxa                   varchar2(1);
  parteCoberta               number;
  copartPrestadorPercentual  number;
  copartPrestadorValor       number;

  cursor cr_cober (id in number) is
    select cthoscober,ccobccober
      from hsscober
     where nnumecober = id;

  cursor cr_taxa_cobertura (id in number) is
    select nnumetaxa
      from hsstcobe
     where nnumetaxa = id;

  cursor cr_tipo_taxa(id in number) is
    select ctipotaxa
      from hsstaxa
     where nnumetaxa = id;

  begin
    valorTaxa := preco_taxa (idTaxa,idPrestador,idContrato,dataEmissao,idBeneficiario,idCongenere) * quantidade;

    if nvl(cobertura,0) = 0 then
      cobertura := cobertura_taxas(idBeneficiario,idTaxa,natureza,executante_temp,localAtendimento,idEspecialidade,
                                   percentualfranquia,franquiaValor,taxaAdm,
                                   cobrancaFranquia,cobrancaAdm,valorTaxa,quantidade,teto,dataEmissao,acidenteDeTrabalho);
    end if;

    open cr_cober(cobertura);
    fetch cr_cober into cobrancaTaxa,cobrancaFranquia;
    close cr_cober;

    if ((cobrancaTaxa = '2') or (cobrancaTaxa = '3')) then

      open cr_taxa_cobertura(idTaxa);
      fetch cr_taxa_cobertura into idTemp;
      close cr_taxa_cobertura;

      if ((idTemp > 0) and (cobrancaTaxa = '2')) then
        cobrancaTaxa := '1';
      elsif ((idTemp is null) and (cobrancaTaxa = '3')) then
        cobrancaTaxa := '1';
      end if;

    end if;

    if ((cobertura > 0) and (cobrancaTaxa = '1')) then
      open cr_tipo_taxa(idTaxa);
      fetch cr_tipo_taxa into tipoTaxa;
      close cr_tipo_taxa;

      if ((tipoTaxa = 'S') or (tipoTaxa = 'T')) then
        tipoTaxa := '5';
      else
        tipoTaxa := tipoTaxa;
      end if;

      valor_copart(cobertura,tipoTaxa,quantidade,valorTaxa,dataEmissao,executante_temp,idEspecialidade,franquiaValor,percentualFranquia,parteCoberta,
                   idBeneficiario,solicitante,cnpj,urgencia,null,null,localAtendimento,0,idGuia,null,tabela_taxa);
                   
      --Affego em visita
      if (franquiaValor = 0) and (percentualFranquia = 0) then
        valor_copart(cobertura,'3',quantidade,valorTaxa,dataEmissao,executante_temp,idEspecialidade,franquiaValor,percentualFranquia,parteCoberta,
                     idBeneficiario,solicitante,cnpj,urgencia,null,null,localAtendimento,0,idGuia,null,tabela_taxa);
      end if;                   

        -- Cobranca de coparticipacao diretamente no prestador de servi�os
        if ((cobrancaFranquia = 'H') or (cobrancaFranquia = 'K' and localEmissao not in ('1','2','3','4','6') and (nvl(incidenciaCopartLocal,'1') <> '5') ) ) and
           (nvl(IncidenciaCopartLocal,'1') <> '8') then  -- N�o receber co-participa��o no prestador, por�m cobr�-la na mensalidade     
          copartPrestadorValor      := nvl(franquiaValor,0);
          copartPrestadorPercentual := nvl(percentualFranquia,0);
          percentualFranquia        := 0;
          franquiaValor                 := 0;
        else
          percentualFranquia    := nvl(percentualFranquia,0);
          franquiaValor             := nvl(franquiaValor,0);
          copartPrestadorValor         := 0;
          copartPrestadorPercentual := 0;
        end if;
        
        if (nvl(IncidenciaCopartLocal,'1') = '8') then -- N�o receber co-participa��o no prestador, por�m cobr�-la na mensalidade
          cobrancaFranquia := 'F';
        end if;       

      -- affego
      if copartPrestadorValor > 0 then
        copartPrestador := copartPrestadorValor * quantidade;
      elsif copartPrestadorPercentual > 0 then
        copartPrestador := Round((copartPrestadorPercentual * valorTaxa) / 100,2);
      else
        copartPrestador := 0;
      end if;  
      
                   
      if franquiaValor > 0 then
        valorCopart := round(franquiaValor,2);
      elsif (percentualFranquia > 0) then
        valorCopart := round(valorTaxa * (percentualFranquia/100),2);
      elsif ((formaCalculo = 'C') or (formaCalculo = '6')) then
        valorCopart := valorTaxa;
      else
        valorCopart := 0;
      end if;
    end if;
    
    if cobrancaFranquia = 'B' or cobrancaFranquia = 'A' or cobrancaFranquia = 'F' then
      cobrancaFranquia := 'B';
    elsif cobrancaFranquia = 'M' or cobrancaFranquia = 'L' or cobrancaFranquia = 'G' or cobrancaFranquia = 'Y' or
          cobrancaFranquia = 'N' then
      cobrancaFranquia := 'F';
    elsif cobrancaFranquia = 'K' and localEmissao in ('1','2','3','4','5','6','7') then
      cobrancaFranquia := 'F';
    elsif cobrancaFranquia = 'E' or cobrancaFranquia = 'D' then
      cobrancaFranquia := 'M';
    end if;    
    
  end;

  procedure calcularPacoteGuia(idGuia               in number,
                               idPacote             in number,
                               quantidade           in number,
                               dataEmissao          in date,
                               coberturaConsiderada in out number,
                               cobrancaConsiderada  in out varchar2,
                               valorCopart             out number)  as
  
  cobertura            number;
  cobrancaFranquia     varchar2(1);
  valor                number;
  valorCopartPrestador number;
  valorCopartHon       number;
  valorCopartCO        number;
  valorCopartFilme     number;
  dataInicioLimite     date;
  situacaoGuia         varchar2(1);
  motivoCobranca       varchar2(1);
  valorCopartTotal     number;
  idTabelaUsada        number;
  
  begin
    valor := vl_pacote(idPacote,dataEmissao) * quantidade;

    valorCopartTotal := 0;

    -- Procedimentos
    for reg in (select hssppaco.*,ctipopmed,
                       nvl(nvaloppaco,0) + nvl(nanesppaco,0) + nvl(nprimppaco,0) +
                       nvl(nseguppaco,0) + nvl(ntercppaco,0) + nvl(nquarppaco,0) +
                       nvl(ncuopppaco,0) + nvl(nfilmppaco,0) + nvl(nptxsppaco,0) valor_total
                  from hssppaco,hsspmed
                 where nnumepacot = idPacote
                   and dvigeppaco = (select max(ppaco2.dvigeppaco) from hssppaco ppaco2
                                      where ppaco2.nnumepacot = idPacote
                                        and dvigeppaco <= dataEmissao)
                   and ccopappaco = 'S'
                   and hssppaco.ccodipmed = hsspmed.ccodipmed) loop

      guia.calcularProcedimentoGuia(idGuia,reg.ccodipmed,reg.ctipopmed,reg.nquanppaco,'N',0,
                                    cobertura,cobrancaFranquia,valorCopart,valor,valorCopartPrestador,
                                    valorCopartHon,valorCopartCO,valorCopartFilme,dataInicioLimite,situacaoGuia,
                                    motivoCobranca);

      if nvl(coberturaConsiderada,0) = 0 then
        coberturaConsiderada := cobertura;
        cobrancaConsiderada  := cobrancaFranquia;
      end if;

      valorCopartTotal := valorCopartTotal + valorCopart;

    end loop;

    -- Taxa
    for reg in (select hsstpaco.*,ctipotaxa
                  from hsstpaco,hsstaxa
                 where nnumepacot = idPacote
                   and dvigetpaco = (select max(ppaco2.dvigetpaco) from hsstpaco ppaco2
                                      where ppaco2.nnumepacot = idPacote
                                        and dvigetpaco <= dataEmissao)
                   and hsstpaco.nnumetaxa = hsstaxa.nnumetaxa) loop
      guia.calcularTaxaGuia(idGuia,reg.nnumetaxa,reg.nquantpaco,localAtendimento,idContrato,dataEmissao,idBeneficiario,
                            idCongenere,coberturaConsiderada,cobrancaConsiderada,valor,valorCopart,valorCopartPrestador);

      valorCopartTotal := valorCopartTotal + valorCopart;

    end loop;

    -- Material e Medicamento detalhado
    for reg in (select hssmpaco.nvalompaco,hssmpaco.nnumeprodu,decode(ctipoprodu,'O','O','E','O','M') tipo,
                       hssmpaco.nquanmpaco
                  from hssmpaco,estprodu
                 where nnumepacot = idPacote
                   and dvigempaco = (select max(mpaco2.dvigempaco) from hssmpaco mpaco2
                                      where mpaco2.nnumepacot = idPacote
                                        and dvigempaco <= dataEmissao)
                   and hssmpaco.nnumeprodu = estprodu.nnumeprodu) loop

      valor := reg.nvalompaco;
      guia.calcularProdutoGuia(reg.nnumeprodu,reg.tipo,reg.nquanmpaco,localAtendimento,dataEmissao,idCongenere,idContrato,0,
                               coberturaConsiderada,cobrancaConsiderada,valor,valorCopart,idTabelaUsada);

      valorCopartTotal := valorCopartTotal + valorCopart;

    end loop;

    -- Material e Medicamento agrupado
    for reg in (select sum(nvl(nmaqummpct,0) + nvl(nmascmmpct,0) + nvl(nmdqummpct,0) + nvl(nmdscmmpct,0)) valorMatMed
                  from hssmmpct
                 where nnumepacot = idPacote
                   and dvigemmpct = (select max(mmpct2.dvigemmpct)
                                       from hssmmpct mmpct2
                                      where mmpct2.nnumepacot = idPacote
                                        and mmpct2.dvigemmpct <= dataEmissao)) loop

      valor := reg.valorMatMed;
      guia.calcularProdutoGuia(0,'M',1,localAtendimento,dataEmissao,idCongenere,idContrato,0,
                               coberturaConsiderada,cobrancaConsiderada,valor,valorCopart,idTabelaUsada);

      valorCopartTotal := valorCopartTotal + valorCopart;
    end loop;
    
    valorCopart := valorCopartTotal * quantidade;
  end;

  procedure calcularProdutoGuia(idProduto        in     number,
                                tipoProduto      in     varchar2,
                                quantidade       in     number,
                                idPrestador      in     number,
                                dataEmissao      in     date,
                                idCongenere      in     number,
                                idContrato       in     number,
                                idFornecedor     in     number,
                                cobertura        in out number,
                                cobrancaFranquia in out varchar2,
                                valorProduto     in out number,
                                valorFranquia       out number,
                                idTabelaUsada       out number) as

  produtoValorizado  number;
  cobrancaMatMed     varchar2(1);
  cobrancaOPME       varchar2(1);
  franquiaValor      number;
  percentualFranquia number;
  parteCoberta       number;
  plano              number;

  cursor cr_cober (id in number) is
    select cmm__cober,copmecober,ccobccober
      from hsscober
     where nnumecober = id;

  cursor cr_plano is
    select nnumeplan 
      from hssusua
     where nnumeusua = idBeneficiario;
  begin
    if idProduto > 0 then
      --pega o plano do usuario
      open  cr_plano;
      fetch cr_plano into plano;
      close cr_plano;      
      valorProduto := preco_produto_plano(idProduto,idPrestador,dataEmissao,idCongenere,idContrato,produtoValorizado,idFornecedor,idTabelaUsada,plano) * quantidade;
    end if;

    if nvl(cobertura,0) = 0 then
      cobertura := cobertura_matmed(idBeneficiario,natureza,urgencia,executante_temp,localAtendimento,idEspecialidade,
                                    cobrancaFranquia,dataEmissao);
    end if;

    open cr_cober(cobertura);
    fetch cr_cober into cobrancaMatMed,cobrancaOPME,cobrancaFranquia;
    close cr_cober;

    if (cobertura > 0) then
      if (cobrancaMatMed = 'S') and (tipoProduto = 'M') then
        valor_copart(cobertura,'4',quantidade,valorProduto,dataEmissao,
                     executante_temp,idEspecialidade,franquiaValor,percentualFranquia,parteCoberta,idBeneficiario,solicitante,
                     null,null,null,null,null,0,0,null,tabela_taxa);
      elsif (cobrancaOPME = 'S') and (tipoProduto = 'O') then
        valor_copart(cobertura,'4',quantidade,valorProduto,dataEmissao,
                     executante_temp,idEspecialidade,franquiaValor,percentualFranquia,parteCoberta,idBeneficiario,solicitante,
                     null,null,null,null,null,0,0,null,tabela_taxa);
      end if;
    end if;

    if franquiaValor > 0 then
      valorFranquia := round(franquiaValor,2);
    elsif (percentualFranquia > 0) then
      valorFranquia := round(valorProduto * (percentualFranquia/100),2);
    elsif ((formaCalculo = 'C') or (formaCalculo = '6')) then
      valorFranquia := valorProduto;
    else
      valorFranquia := 0;
    end if;
    
    if cobrancaFranquia = 'B' or cobrancaFranquia = 'A' or cobrancaFranquia = 'F' then
      cobrancaFranquia := 'B';
    elsif cobrancaFranquia = 'M' or cobrancaFranquia = 'L' or cobrancaFranquia = 'G' or cobrancaFranquia = 'Y' or
          cobrancaFranquia = 'N' then
      cobrancaFranquia := 'F';
    elsif cobrancaFranquia = 'K' and localEmissao in ('1','2','3','4','5','6','7') then
      cobrancaFranquia := 'F';
    elsif cobrancaFranquia = 'E' or cobrancaFranquia = 'D' then
      cobrancaFranquia := 'M';
    end if;      
    
  end;

end guia;
/