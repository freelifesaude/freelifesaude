<?php              
  $sql_fconf = new Query();
  $txt_fconf = "SELECT CMANUCONF,TO_CHAR(SYSDATE,'DD/MM/YYYY') DATA FROM FINCONF";
  $sql_fconf->executeQuery($txt_fconf);
    
  /* Sistema em manutenção */
  if ($sql_fconf->result("CMANUCONF") == 'S') {
    echo '<script type="text/javascript">
            window.location.href="../comum/manutencao.html";
          </script>
          <noscript>
            <meta http-equiv="refresh" content="0;url=../comum/manutencao.html" />
          </noscript> ';    
  }
  else if (date('d/m/Y') <> $sql_fconf->result("DATA"))
    exit('Data do servidor web diferente da data do banco de dados. Entre em contrato com o admistrador do sistema.');
  else {  
    $_SESSION['nome_operador']   = '';   
    $_SESSION['boas_vindas']     = 'S';  
    $_SESSION['troca_de_senha']  = '';
    $_SESSION['senha_usuario']   = '';
    $_SESSION['StatusLiberacao'] = '';
    
    //CONFIGURA O TAMANHO DA LOGO NOS RELATÓRIOS PDF
    $_SESSION['_X'] = 10;  //coordenada X
    $_SESSION['_Y'] = 5;   //coordenada Y
    $_SESSION['_W'] = 40;  //largura
    $_SESSION['_H'] = 25;  //altura
    
    if ($_SESSION['sistema'] == 'Operadora')
      $_SESSION['layout'] = '3.0';
    else {
      /* Layouts: full-vert = Tela cheia - menu vertical
                  1024-hor  = Tela 1024  - menu horizontal */  
      $_SESSION['layout'] = 'full-vert';
      //$_SESSION['layout'] = '1024-hor';    
    }
  
    $sql_empr = new Query();
    $txt_empr = "SELECT CFONEEMPR,CCGC_EMPR,CAPELEMPR,REMOVE_ACENTO(CNOMEEMPR) CNOMEEMPR,CLOGREMPR,CENDEEMPR,CCIDAEMPR,
                        CESTAEMPR,CDESCTLOGR,CBAIREMPR,CHOMEEMPR,CCEP_EMPR                 
                   FROM FINEMPR,HSSTLOGR
                  WHERE FINEMPR.CLOGREMPR = HSSTLOGR.NNUMETLOGR(+) ";
    $sql_empr->executeQuery($txt_empr);   
    
    $_SESSION['fone_operadora']    = $sql_empr->result("CFONEEMPR");
    $_SESSION['cnpj_operadora']    = $sql_empr->result("CCGC_EMPR");  
    $_SESSION['apelido_operadora'] = $sql_empr->result("CAPELEMPR");  
    $_SESSION['nome_operadora']    = $sql_empr->result("CNOMEEMPR");
  
    if ($sql_empr->result("CLOGREMPR") <> '')
      $_SESSION['endereco_operadora'] = $sql_empr->result("CDESCTLOGR").' '.$sql_empr->result("CENDEEMPR");
    else
      $_SESSION['endereco_operadora'] = $sql_empr->result("CENDEEMPR");

    $_SESSION['bairro_operadora']  = $sql_empr->result("CBAIREMPR");    
    $_SESSION['cidade_operadora']  = $sql_empr->result("CCIDAEMPR");
    $_SESSION['uf_operadora']      = $sql_empr->result("CESTAEMPR");
    $_SESSION['cep_operadora']     = $sql_empr->result("CCEP_EMPR");
    $_SESSION['home_page']         = $sql_empr->result("CHOMEEMPR"); 
    
    $sql_conf = new Query();
    $txt_conf = "SELECT REPLACE(CMUSUCONF,'0','9') CMUSUCONF,NVL(CPAUTCONF,'P') CPAUTCONF,NMS__CONF,NVL(CGBIOCONF,'1') CGBIOCONF,CUNIMCONF,
                        CCRETPMED,CARQUCONF,CPCMSCONF,CTOPECONF,COPMECONF,CIMERCONF,CBIOMCONF,CIAUTCONF,NAUDICONF,CMAICCONF,
                        CIMPGCONF
                   FROM HSSCONF  ";
    $sql_conf->executeQuery($txt_conf);    
  
    $_SESSION['mascara']                  = $sql_conf->result("CMUSUCONF");
    $_SESSION['regra_autorizacao']        = $sql_conf->result("CPAUTCONF");
    $_SESSION['registro_ans']             = $sql_conf->result("NMS__CONF");
    $_SESSION['obriga_biometria']         = $sql_conf->result("CGBIOCONF");
    $_SESSION['codigo_unimed']            = $sql_conf->result("CUNIMCONF");
    $_SESSION['intercambio_unimed']       = $sql_conf->result("CIAUTCONF");
    $_SESSION['ConsultaRetorno']          = $sql_conf->result("CCRETPMED");
    $_SESSION['pasta_arquivos']           = $sql_conf->result("CARQUCONF");    
    $_SESSION['utiliza_pcmso']            = $sql_conf->result("CPCMSCONF");
    $_SESSION['valor_auditoria']          = $sql_conf->result("NAUDICONF");
    $_SESSION['tipo_operadora']           = $sql_conf->result("CTOPECONF");
    $_SESSION['regra_opme']               = $sql_conf->result("COPMECONF");
    $_SESSION['procedimentos_realizados'] = $sql_conf->result("CIMERCONF");
    $_SESSION['tipo_sensor_biometria']    = $sql_conf->result("CBIOMCONF");  
    $_SESSION['email_credenciamento']     = $sql_conf->result("CMAICCONF");  
    $_SESSION['impressaoGuiaInternacao']  = $sql_conf->result("CIMPGCONF");  
    
    $sql_inter = new Query();
    $txt_inter = "SELECT NVL(NDEMOINTER,12) NDEMOINTER,NVL(CLUSUINTER,'N') CLUSUINTER, 
                         NVL(CCOPAINTER,'N') CCOPAINTER, NVL(CCARPINTER,'N') CCARPINTER,NVL(CCARTINTER,'N') CCARTINTER,                         
                         CAUDIINTER, NVL(CBLCOINTER,'N') CBLCOINTER,CMPREINTER,CMPPOINTER, 
                         CRETOINTER,CGGPEINTER,CNEGAINTER, 
                         CMEMPINTER,CBOLEINTER,CMEPOINTER,CBOPFINTER,CBOPJINTER,CATPFINTER,CATPJINTER,
                         CCAPFINTER,CCAPJINTER,CRCPFINTER,CRCPJINTER,CIRPFINTER,CMOVIINTER,CALSEINTER,CBOJUINTER,
                         NGLOSINTER,CFMAIINTER,CSMTPINTER,NPORTINTER,CUSMTINTER,CSSMTINTER,CSSL_INTER,NDIABINTER,CSENHINTER,
                         CMUSUINTER,CMPUSINTER,NQTDRINTER,NVL(NDVENINTER,60) NDVENINTER,CPLANINTER,CQUITINTER,
                         TO_CHAR(DINICINTER,'DD/MM/YYYY') DINICINTER,CSENEINTER,CINFUINTER,NVL(CDSENINTER,'S') CDSENINTER, 
                         NVL(NHAGEINTER,30) NHAGEINTER,CPESQINTER,CLIBEINTER,NVL(NNLIBINTER,15) NNLIBINTER,
                         DECODE(CLIBEINTER,'C',TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE),3),'DD/MM/YYYY'),TO_CHAR((TRUNC(SYSDATE)+NVL(NVL(NNLIBINTER,0)-1,15)),'DD/MM/YYYY')) DATA_MAXIMA_AGENDA,
                         TO_CHAR(TRUNC(SYSDATE),'DD/MM/YYYY') DATA_MINIMA_AGENDA,CDIRUINTER,
                         CLINKINTER,CVALOINTER,CMPSOINTER,NVL(NQDACINTER,0) NQDACINTER,CACECAINTER,CBAGEINTER,NHCNAINTER,CPGUIAINTER
                    FROM HSSINTER";
    $sql_inter->executeQuery($txt_inter);

    $_SESSION['numero_demonstrativos']               = $sql_inter->result("NDEMOINTER");
    $_SESSION['localizacao_usuarios']                = $sql_inter->result("CLUSUINTER");
    $_SESSION['Mostra_Copart']                       = $sql_inter->result("CCOPAINTER");
    $_SESSION['segunda_via_carteira_PJ']             = $sql_inter->result("CCARPINTER");
    $_SESSION['segunda_via_carteira_PF']             = $sql_inter->result("CCARTINTER");
    $_SESSION['auditoria_guias']                     = $sql_inter->result("CAUDIINTER");
    $_SESSION['bloqueio_consulta']                   = $sql_inter->result("CBLCOINTER");
    $_SESSION['mensagem_web_prestador']              = $sql_inter->result("CMPREINTER");
    $_SESSION['mensagem_web_prestador_pop']          = $sql_inter->result("CMPPOINTER");
    $_SESSION['msg_prestador_no_saude_ocupacional']  = $sql_inter->result("CMPSOINTER");
    $_SESSION['MensagemRetorno']                     = $sql_inter->result("CRETOINTER");
    $_SESSION['GuiaGeradaPedidoExame']               = $sql_inter->result("CGGPEINTER");
    $_SESSION['gera_negada']                         = $sql_inter->result("CNEGAINTER");    
    $_SESSION['mensagem_web_empresa']                = $sql_inter->result("CMEMPINTER");
    $_SESSION['permite_boleto_vencido']              = $sql_inter->result("CBOLEINTER");
    $_SESSION['mensagem_web_empresa_pop']            = $sql_inter->result("CMEPOINTER");
    $_SESSION['boleto_PF']                           = $sql_inter->result("CBOPFINTER");
    $_SESSION['boleto_PJ']                           = $sql_inter->result("CBOPJINTER");
    $_SESSION['atendimentos_PF']                     = $sql_inter->result("CATPFINTER");
    $_SESSION['atendimentos_PJ']                     = $sql_inter->result("CATPJINTER");
    $_SESSION['carencias_PF']                        = $sql_inter->result("CCAPFINTER");
    $_SESSION['carencias_PJ']                        = $sql_inter->result("CCAPJINTER");
    $_SESSION['rede_PF']                             = $sql_inter->result("CRCPFINTER");
    $_SESSION['rede_PJ']                             = $sql_inter->result("CRCPJINTER");
    $_SESSION['demonstrativo_IRPF']                  = $sql_inter->result("CIRPFINTER");
    $_SESSION['movimentacao_usuario']                = $sql_inter->result("CMOVIINTER");
    $_SESSION['acesso_cancelado']  			             = $sql_inter->result("CACECAINTER");
    $_SESSION['altera_senha']                        = $sql_inter->result("CALSEINTER");
    $_SESSION['atualiza_boleto']                     = $sql_inter->result("CBOJUINTER");     
    $_SESSION['demonstrativos_glosa']                = $sql_inter->result("NGLOSINTER");    
    $_SESSION['email_remetente']                     = $sql_inter->result("CFMAIINTER");
    $_SESSION['email_smtp']                          = $sql_inter->result("CSMTPINTER");
    $_SESSION['email_porta']                         = $sql_inter->result("NPORTINTER");
    $_SESSION['email_usuario']                       = $sql_inter->result("CUSMTINTER");
    $_SESSION['email_senha']                         = $sql_inter->result("CSSMTINTER");
    $_SESSION['email_ssl']                           = $sql_inter->result("CSSL_INTER");  
    $_SESSION['dias_impressao_boleto']               = $sql_inter->result("NDIABINTER");   
    $_SESSION['mensagem_web_usuario']                = $sql_inter->result("CMUSUINTER");
    $_SESSION['mensagem_web_usuario_pop']            = $sql_inter->result("CMPUSINTER"); 
    $_SESSION['quantidade_resultado']                = $sql_inter->result("NQTDRINTER");
    $_SESSION['dias_apos_vencido']                   = $sql_inter->result("NDVENINTER");
    $_SESSION['mostrar_planos']                      = $sql_inter->result("CPLANINTER");       
    $_SESSION['documento_quitacao']                  = $sql_inter->result("CQUITINTER");
    $_SESSION['inicio_busca_demonstrativo']          = $sql_inter->result("DINICINTER");
    $_SESSION['informacao_usuario']                  = $sql_inter->result("CINFUINTER");
    $_SESSION['definicao_senha']                     = $sql_inter->result("CDSENINTER");      
    $_SESSION['historico_agendamento']               = $sql_inter->result("NHAGEINTER");
    $_SESSION['mostra_calendario']                   = $sql_inter->result("CPESQINTER");
    $_SESSION['liberacao_dos_dias']                  = $sql_inter->result("CLIBEINTER");
    $_SESSION['numero_dias_liberados']               = $sql_inter->result("NNLIBINTER");
    $_SESSION['data_maxima_liberada']                = $sql_inter->result("DATA_MAXIMA_AGENDA"); 
    $_SESSION['data_minima_liberada']                = $sql_inter->result("DATA_MINIMA_AGENDA");
    //$_SESSION['diretorio_upload'] = $sql_inter->result("CDIRUINTER");
    $_SESSION['endereco_web']                        = $sql_inter->result("CLINKINTER"); 
    $_SESSION['mostra_valor_procedimentos']          = $sql_inter->result("CVALOINTER");
    $_SESSION['qtde_dias_acesso_cancelado']          = $sql_inter->result("NQDACINTER");
    $_SESSION['agendamento_mesmo_dia']               = $sql_inter->result("CBAGEINTER");    
    $_SESSION['tempo_antecedencia']                  = $sql_inter->result("NHCNAINTER");    
    $_SESSION['permite_guia']                        = $sql_inter->result("CPGUIAINTER");
  } 
?>