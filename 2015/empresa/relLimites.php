<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "UTILIZA��O DE LIMITES";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","relLimites.htm");
  
  $tpl->ID_SESSAO = $_GET['idSessao'];  
  $tpl->MASCARA   = $_SESSION['mascara'];  
  
  if (isset($_POST['codigo']))
    $codigo = htmlentities($_POST['codigo']);  
  else
    $codigo = '';   
        
  if (isset($_POST['imprimir'])) {
  
    if ($codigo <> '') {  
      $parametros = array();
      $parametros[0]['nome']  = 'codigo';
      $parametros[0]['valor'] = $codigo;
      
      $tpl->RESULT = $util->redireciona2('relLimitesImp.php?idSessao='.$_GET['idSessao'],$parametros,'S');
    } else {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG = "Informe o c�digo do benefici�rio.";
      $tpl->block("ERRO");      
    }      
  }
  
  $tpl->CODIGO = $codigo;   
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     

?>