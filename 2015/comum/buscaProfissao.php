<?php
  session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
   
  if(isset($_POST['continua'])){	
    $sql = new Query($bd);  
    $txt = "SELECT CDESCPROF,HSSPROF.NNUMEPROF
              FROM HSSPROF, HSSPROEM, HSSEMPR, HSSTITU
		     WHERE HSSTITU.NNUMETITU = :contrato
		       AND HSSPROF.NNUMEPROF = HSSPROEM.NNUMEPROF
			   AND HSSPROEM.NNUMEEMPR = HSSEMPR.NNUMEEMPR
			   AND HSSEMPR.NNUMEEMPR = HSSTITU.NNUMEEMPR
		     ORDER BY CDESCPROF";
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
	
	
	$opcao = '<select id="nova_prof" name="nova_prof"> <option value="">Selecione a função/profissão</option>';
    while (!$sql->eof()) {
      $opcao .= '<option value="'.$sql->result("NNUMEPROF").'" ';
      $opcao .= '>'.$sql->result("CDESCPROF").'</option>';
      $sql->next();
    }  
	$opcao .= '</select>';
  }
/*   $retorno = array();
   $retorno["id"] = $sql->result("NNUMEUSUA");   
   $retorno["nome"] = $sql->result("CNOMEUSUA");
   $retorno["profissao"] = $sql->result("CDESCPROF");   
   $retorno["id_nova_profissao"] = $sql->result("NNUMEPROF");      
  
  	echo json_encode($retorno); */
	
  echo $opcao;
  $bd->close();


?>