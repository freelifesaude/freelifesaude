<?php     
  define("USER"        ,"solus"); // O nome de usu�rio do banco de dados.
  define("PASSWORD"    ,"s"); // A senha do usu�rio do banco de dados. 
  define("SERVER"      ,"solus"); // O host no qual voc� deseja se conectar.  
  define("SERVER"      ,"(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 172.21.11.4)(PORT = 1521))) (CONNECT_DATA =(SERVICE_NAME = oltp_hmlg) ))"); // O host no qual voc� deseja se conectar.
  define("HOMOLOGACAO" ,"S"); // Se a conex�o � do banco de homologacao ou producao
      
      
  class Oracle {
    private $connect;  
    private $transaction;
    
    public function Oracle($transaction = false,$user='',$pass='',$server='') {
   
      $this->transction = $transaction;

      if ($user <> '') {

        $this->connect = oci_connect($user,$pass,$server,'WE8ISO8859P15');
        }
      else
        $this->connect = oci_connect(USER,PASSWORD,SERVER,'WE8ISO8859P15');

      if(!$this->connect) {
        $last = array();
        $last = error_get_last();
        $erroQuery = "Function: Oracle".chr(13).chr(10).
                     "Erro Oracle: ".chr(13).chr(10).
                     "Mensagem: N�o foi possivel conectar-se ao servidor Oracle.".chr(13).chr(10). 
                      $last['message'].chr(13).chr(10). 
                     "Arquivo: ".$last['file'].chr(13).chr(10). 
                     "Linha: ".$last['line'].chr(13).chr(10).chr(13).chr(10);
        gravaErro("Oracle.class.php",$erroQuery);  
        exit();
      } 
    }   

    public function conn() {
      return $this->connect;
    }    
    
    private function connect($user='',$pass='',$server='') {
      
      if ($user <> '')
        $this->connect = oci_connect($user,$pass,$server,'WE8ISO8859P15');
      else      
        $this->connect = oci_connect(USER,PASSWORD,SERVER);
        
      if(!$this->connect) {
        $last = array();
        $last = error_get_last();
        $erroQuery = "Function: Oracle".chr(13).chr(10).
                     "Erro Oracle: ".chr(13).chr(10).
                     "Mensagem: N�o foi possivel conectar-se ao servidor Oracle.".chr(13).chr(10). 
                      $last['message'].chr(13).chr(10). 
                     "Arquivo: ".$last['file'].chr(13).chr(10). 
                     "Linha: ".$last['line'].chr(13).chr(10).chr(13).chr(10);
        gravaErro("Oracle.class.php",$erroQuery); 
        exit();
      }     
    }
    
    public function close() {
      oci_close($this->connect);
      ini_set  ("display_errors","on");  
    }  
    
    public function startTransaction() {
      $this->transction = true; 
    }    
    
    public function inTransaction() {
      return $this->transction;
    }
    
    public function commit(){
      oci_commit($this->connect);
    }    

    public function rollback(){
      oci_rollback($this->connect);
    }       
  }
?>