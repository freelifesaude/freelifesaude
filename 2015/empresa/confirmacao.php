<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
	
	if($_SESSION['apelido_operadora'] == 'unimedFranca')
    $_SESSION['titulo'] = 'ATEN��O';
  else
    $_SESSION['titulo'] = 'CONFIRMA��O';

  require_once("../comum/autoload.php"); 
  
  $bd = new Oracle();  
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/confirmacao.htm");
  
  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  $filtros = array(); 
  $filtros = $_SESSION['filtrosConfirmacao'];
    
  $operacao  = $filtros['op'];
  $protocolo = $filtros['prot'];
  
  /* Operacao
  
  100 - Inclus�o de benefici�rio
  
  200 - Cancelamento de benefici�rio
  
  300 - Altera��o de benefici�rio
  
  400 - Erro atcad
  401 - Registro excluido
  
  500 - Solicitacao de carteirinha
 
  */

  if ($operacao == '100') {
  
    if ($_SESSION['apelido_operadora'] == 'planmed') {
      $texto = "Registro inclu�do com sucesso! Para imprimir o formul�rio de ades�o clique em imprimir.";
    }
	else if ($_SESSION['apelido_operadora'] == 'clinipam') {
  	  $texto = "Benefici�rio incluido com sucesso!";
	}
    else
      $texto = "Benefici�rio incluido com sucesso! Para imprimir o protocolo " 
                . $protocolo . " de 
                inclus�o, clique em imprimir.";
         
    $tpl->MSG2   = $texto;
    $tpl->CLASSE = "alert-success";
    $tpl->BOTAO1_DESC   = "Imprimir";
    
    if ($_SESSION['apelido_operadora'] == 'planmed')
      $tpl->BOTAO1_ACTION = "abreJanela('formularioPlanmed.php',$protocolo);";
    else
      $tpl->BOTAO1_ACTION = "abreJanela('protocoloMovimentacao.php',$protocolo);";
      
    if ($_SESSION['apelido_operadora'] != 'clinipam'){
	  $tpl->block("BOTAO1");
	}

    $tpl->BOTAO2_DESC   = "Nova inclus�o";
    $tpl->BOTAO2_ACTION = "voltar('inclusao.php','');";
    $tpl->block("BOTAO2");
    
    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
    
  } else if ($operacao == '200') {
  
    if($_SESSION['apelido_operadora'] == 'unimedFranca')
      $texto_padrao_canc = "Cancelamento em andamento! Para imprimir o protocolo n� ".$protocolo.", clique em imprimir.";
		else
			$texto_padrao_canc = "Benefici�rio cancelado com sucesso! Para imprimir o protocolo n� ".$protocolo.", clique em imprimir.";
		
    $texto = $texto_padrao_canc;
    
    if ($seg->permissaoOutros($bd,"WEBEMPRESARELACAODEATENDIMENTO") or
        $seg->permissaoOutros($bd,"WEBEMPRESARELACAODEATENDIMENTOCOMVALOR"))
      $texto .= "<br>Caso desejar visualizar os atendimentos deste benefici�rio, acesse o menu->relat�rios->atendimentos";
             
    $tpl->MSG2   = $texto;
    $tpl->CLASSE = "alert-success";
    $tpl->BOTAO1_DESC   = "Imprimir";
    $tpl->BOTAO1_ACTION = "abreJanela('protocoloMovimentacao.php',$protocolo);";
    $tpl->block("BOTAO1");
    
    $tpl->BOTAO2_DESC   = "Novo cancelamento";
    $tpl->BOTAO2_ACTION = "voltar('cadastro.php','&op=C');";
    $tpl->block("BOTAO2");    

    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
    
  } else if ($operacao == '201') {

    $texto = "<font color='red'>Aten��o!! Este � o �ltimo titular do contrato, por favor envie a carta de cancelamento, caso n�o seja feita nenhuma admiss�o nos pr�ximos 3 meses.</font>".
             "<br>Benefici�rio cancelado com sucesso! Para imprimir o protocolo n� ".$protocolo.", clique em imprimir.";
    
    if ($seg->permissaoOutros($bd,"WEBEMPRESARELACAODEATENDIMENTO") or
        $seg->permissaoOutros($bd,"WEBEMPRESARELACAODEATENDIMENTOCOMVALOR"))
      $texto .= "<br>Caso desejar visualizar os atendimentos deste benefici�rio, acesse o menu->relat�rios->atendimentos";
      
    $tpl->MSG2   = $texto;
    $tpl->CLASSE = "alert-success";
    $tpl->BOTAO1_DESC   = "Imprimir";
    $tpl->BOTAO1_ACTION = "abreJanela('protocoloMovimentacao.php',$protocolo);";
    $tpl->block("BOTAO1");

    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");    
  
  } else if ($operacao == '400') {
  
    $tpl->MSG2   = "Existem dependentes do titular em quest�o com inclus�es pendentes. <br/>
                   Para exclu�-lo, � necess�rio excluir primeiro as altera��es de seus dependentes.";
    $tpl->CLASSE = "";
    $tpl->BOTAO1_DESC   = "OK";
    $tpl->BOTAO1_ACTION = "voltar('pendentes.php','');";
    $tpl->block("BOTAO1");

    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
  
  } else if ($operacao == '401') { 

    $tpl->MSG2   = "Registro excluido com sucesso!";
    $tpl->CLASSE = "alert-success";
    $tpl->BOTAO1_DESC   = "OK";
    $tpl->BOTAO1_ACTION = "voltar('pendentes.php','');";
    $tpl->block("BOTAO1");

    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
    
  } else if ($operacao == '300') {     
    
    $tpl->MSG2   = "Registro alterado com sucesso!";
    $tpl->CLASSE = "alert-success";
    $tpl->BOTAO1_DESC   = "OK";
    $tpl->BOTAO1_ACTION = "voltar('cadastro.php','&op=A');";
    $tpl->block("BOTAO1");

    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
    
  } else if ($operacao == '500') {  
    $tpl->MSG2   = "Carteirinhas solicitadas com sucesso.";
    $tpl->CLASSE = "";
    $tpl->BOTAO1_DESC   = "Imprimir protocolo";
    $tpl->BOTAO1_ACTION = "impCarteira('".$protocolo."','P');";
    $tpl->block("BOTAO1");
 
    $tpl->BOTAO2_DESC   = "Voltar";
    $tpl->BOTAO2_ACTION = "voltar('carteiras.php','')";
    $tpl->block("BOTAO2");
    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
  } else if ($operacao == '600') {  
    $tpl->MSG2   = "Funcion�rio incluso com sucesso. ";
    $tpl->CLASSE = "alert-success";
    $tpl->BOTAO1_DESC   = "Emitir aso";
	$tpl->BOTAO1_ACTION = "voltar('autorizacao.php','');";
      
    $tpl->block("BOTAO1");

/*    $tpl->BOTAO2_DESC   = "Nova inclus�o";
    $tpl->BOTAO2_ACTION = "voltar('inclusao.php','');";
    $tpl->block("BOTAO2");*/
    
    $tpl->block("BOTOES");    
    $tpl->block("MENSAGEM2");
  }
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     

?>