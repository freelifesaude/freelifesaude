<?php     
  header ('Content-type: text/html; charset=ISO-8859-1');  

  $tpl = new Template("../comum/padraoOLD.html");
  
  $tpl->TEMPOSES          = session_cache_expire();
       
  switch ($_SESSION['sistema']) {
    case "Empresa":
      $tpl->IDAREA = "empresa";    
      $tpl->AREA   = "Empresa";
      //$tpl->AREA2  = "Empresa";
      $diretorio   = "../empresa";
      $imagem      = "banner_empresa.png";
    break;
    case "Prestador":
      $tpl->IDAREA = "prestador";    
      $tpl->AREA   = "Prestador";
      $tpl->AREA2  = "Prestador";
      $diretorio   = "../prestador";
      $imagem      = "banner_prestador.png";
    break;
    case "Comercial":
      $tpl->IDAREA = "comercial";    
      $tpl->AREA   = "Comercial";
      $tpl->AREA2  = "Comercial";
      $diretorio   = "../comercial";
      $imagem      = "banner_comercial.png";
    break;
    case "Usuario":
      $tpl->IDAREA = "beneficiario";    
      $tpl->AREA   = "Benefici�rio";
      $tpl->AREA2  = "Benefici�rio";
      $diretorio   = "../usuario";
      $imagem      = "banner_beneficiario.png";
    break;    
    case "Operadora":
      $tpl->IDAREA = "operadora";    
      $tpl->AREA   = "Operadora";
      $tpl->AREA2  = "Operadora";
      $diretorio   = "../operadora";
      $imagem      = "banner_operadora.png";
    break;        
    case "faleconosco":
      $tpl->IDAREA = "faleconosco";    
      $tpl->AREA   = "Fale Conosco";
      $tpl->AREA2  = "";
      $diretorio   = "../rede";
      $imagem      = "banner_rede.png";
    break;
    case "Rede":
      $tpl->IDAREA = "rede";    
      $tpl->AREA   = "Rede";
      $tpl->AREA2  = "Rede";
      $diretorio   = "../rede";
      $imagem      = "banner_rede.png";
    break;  
    case "Auditoria":
      $tpl->IDAREA = "auditoria";    
      $tpl->AREA   = "Auditoria";
      $tpl->AREA2  = "Auditoria";
      $diretorio   = "../auditoria";
      $imagem      = "banner_auditoria.png";
    break; 
    case "Agendamento":
      $tpl->IDAREA = "agendamento";    
      $tpl->AREA   = "Agendamento de consulta";
      $tpl->AREA2  = "Agendamento de consulta";
      $diretorio   = "../agendamento";
      $imagem      = "banner_agendamento.png";
    break; 
    case "Consultorio":
      $tpl->IDAREA = "consultorio";    
      
      if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
        $tpl->AREA  = "u-DOC";
        $tpl->AREA2 = '<img id="banner_direito" src="../comum/img/banner_direito.png" >';
      }
      else {      
        $tpl->AREA   = "Consult�rio WEB";
        $tpl->AREA2  = "Consult�rio WEB";
      }
      
      $diretorio   = "../consultorio";
      $imagem      = "banner_consultorio.png";
    break; 
    case "Atendimento":
      $tpl->IDAREA = "atendimento";    
      $tpl->AREA  = "Atendimento";
      $tpl->AREA2 = "Atendimento";
      $diretorio = "../atendimento";
      $imagem    = "banner_atendimento.png";
    break;  
    case "Saude_Ocupacional":
      $tpl->IDAREA = "saudeocupacional";    
      $tpl->AREA  = "Sa�de Ocupacional";
      $tpl->AREA2 = "Sa�de Ocupacional";
      $diretorio  = "../saudeocupacional";
      $imagem     = "banner_saudeocupacional.png";
      
	    $tpl->addFile("BARRA_EMPRESA","../saudeocupacional/barraEmpresa.htm"); 
      require_once("../saudeocupacional/barraEmpresa.php"); 	      
    break;     
  }
  
  $_SESSION['diretorio'] = $diretorio;
  
  if (file_exists('../comum/img/'.$imagem) and $imagem <> "")
    $tpl->BANNER = $imagem;    
  else
    $tpl->BANNER = "banner.png";    
  
  /* Menu */    
  if ((isset($_GET['idSessao'])) and ($_SESSION['sistema'] <> 'Rede') and ($_SESSION['sistema'] <> 'faleconosco')) {
    require_once($_SESSION['diretorio']."/itensMenu.php");     

    $tpl->addFile("MENU_NAV","../comum/menuNav.html"); 
    require_once("../comum/menuNav.php"); 
    
	$tpl->block("MOSTRA_LOGADO");    
  }

  if (isset($_SESSION['logado']))
    $tpl->LOGADO = $_SESSION['logado'];
      
  $tpl->ANO = date('Y');  
    
  if (isset($_SESSION['titulo']) and ($_SESSION['titulo'] <> ''))
    $tpl->TITULO = "<h5 id='barraTitulo' class='text-center'>".$_SESSION['titulo']."</h5><br>";
  else
    $tpl->TITULO = '';
 /*   
  if (HOMOLOGACAO == 'S')        
    $tpl->HOMOLOGANDO = "<br><h4><font color='red'>HOMOLOGA��O</font></h4>";
  else
    $tpl->HOMOLOGANDO = "";    
 */   
  if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
    $tpl->block("CSS_UNIMED_FRANCA");  

    if ($_SESSION['sistema'] <> 'Consultorio')
      $tpl->block("SUPORTE_UNIMED_FRANCA");  
  }    
?>