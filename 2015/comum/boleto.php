<?php
  session_start();
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/sessao.php");  
  require_once("../comum/autoload.php");
  
  $bd      = new Oracle();
  $formata = new Formata();
  $util    = new Util();
  $func    = new Funcao();  
  
  class PDF extends PDFSolus {     
    public $fill_r;
    public $fill_g;
    public $fill_b;
    public $imagem;
    public $img_largura;
    public $img_altura;  
  
    function Header() {
    }

    function Footer() {
    }
  }  

  if (isset($_SESSION['filtrosBoleto'])) {
    $filtros = array();
    $filtros = $_SESSION['filtrosBoleto'];
    
    $id_pagamento = $filtros['id_pagamento'];
    $id_jpaga     = $filtros['jpaga'];
    $file         = $filtros['arquivo'];
    $para         = $filtros['para'];
    $id_pedido    = $filtros['pedido'];
  }
  
  $boleto = new Boleto($id_pagamento,$id_jpaga);

  if ($boleto->ValidaLayout($boleto->LayoutBoleto)) {  
    
    $pdf=new PDF('P','mm','A4');
    $pdf->AliasNbPages();
    $pdf->Open();
    $pdf->AddPage();
    $pdf->inFooter = false;  
    
    $pdf->fill_r      = $boleto->CorR;
    $pdf->fill_g      = $boleto->CorG;
    $pdf->fill_b      = $boleto->CorB;
    $pdf->img_largura = $boleto->ImagemLargura;
    $pdf->img_altura  = $boleto->ImagemAltura;
    $pdf->imagem      = $boleto->Imagem;    
  
    $pdf->SetFillColor($pdf->fill_r,$pdf->fill_g,$pdf->fill_b);  
    
    $pdf->Ln(6);    
    $pdf->SetFont('Arial','B',10);    
    $pdf->Cell(10,5,'Instru��es',0,1);
    
    $pdf->SetFont('Arial','',10);    
    $pdf->Cell(0,3,'1. Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta N�o use modo econ�mico.',0,1);
    $pdf->Cell(0,3,'2. Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens m�nimas � esquerda e � direita do formul�rio.',0,1);
    $pdf->Ln(12);
 
    /*inclui o valor do INSS e do IR no cabe�alho do boleto de PESSOA JURIDICA*/   
    if($_SESSION["apelido_operadora"] == "unimedJatai"){
      $sql = new Query($bd);	  
      $txt = "SELECT SUM(VALOR) INSS, SUM(VALOR) * (15/100) PERCENTUAL
                FROM ( SELECT ( (nvl(NVLIQUSUPG,0) * (NPACPCOMPE /100)) * (30/100)) valor
                         FROM HSSPAGA, HSSUSUPG, HSSTITU, HSSCNOTA, HSSNOTAF, HSSCOMPE
                        WHERE HSSPAGA.NNUMEPAGA = :Mensalidade
                          AND HSSPAGA.NNUMEPAGA = HSSUSUPG.NNUMEPAGA
                          AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
                          AND HSSTITU.NNUMEEMPR IS NOT NULL
                          AND NVL(CBOLETITU,'L') IN ('L','C')
                          AND HSSPAGA.NNUMEPAGA = HSSCNOTA.NNUMEPAGA
                          AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF
                          AND TO_CHAR(NVL(HSSNOTAF.DEMISNOTAF,DGERAPAGA),'MM/YYYY') = HSSCOMPE.CCOMPCOMPE
                        UNION ALL
                       SELECT ((nvl(NVALOADTPG,0) *(NPACPCOMPE /100)) * (30/100))
                         FROM HSSPAGA, HSSADTPG, HSSTITU, HSSCNOTA, HSSNOTAF, HSSCOMPE
                        WHERE HSSPAGA.NNUMEPAGA = :Mensalidade
                          AND HSSPAGA.NNUMEPAGA = HSSADTPG.NNUMEPAGA
                          AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
                          AND HSSTITU.NNUMEEMPR IS NOT NULL
                          AND NVL(CBOLETITU,'L') IN ('L','C')
                          AND HSSPAGA.NNUMEPAGA = HSSCNOTA.NNUMEPAGA
                          AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF
                          AND TO_CHAR(NVL(HSSNOTAF.DEMISNOTAF,DGERAPAGA),'MM/YYYY') = HSSCOMPE.CCOMPCOMPE
                        UNION ALL
                       SELECT VALOR
                         FROM HSSPAGA, HSSTITU, COMPOSICAO_DESPESA_COOPERATIVA CC, HSSCNOTA, HSSNOTAF, HSSCOMPE
                        WHERE HSSPAGA.NNUMEPAGA = :Mensalidade
                          AND HSSPAGA.NDESPPAGA > 0
                          AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
                          AND HSSTITU.NNUMEEMPR IS NOT NULL
                          AND NVL(CBOLETITU,'L') IN ('L','C')
                          AND HSSPAGA.NNUMEPAGA = CC.NNUMEPAGA
                          AND CC.NNUMEPAGA = HSSCNOTA.NNUMEPAGA
                          AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF
                          AND TO_CHAR(NVL(HSSNOTAF.DEMISNOTAF,DGERAPAGA),'MM/YYYY') = HSSCOMPE.CCOMPCOMPE
                          AND CC.DESCRICAO = 'ATOS COOPERATIVOS PRINCIPAIS')";
     	
      $sql->addParam(":Mensalidade", $id_pagamento);
      $sql->executeQuery($txt);  
     
	
      if ($sql->result("INSS") > 0) {
        $sql2 = new Query($bd);		  
        $txt2 = "SELECT NTRIBNOTAF, NIR__NOTAF
	               FROM HSSNOTAF, HSSCNOTA
		          WHERE HSSCNOTA.NNUMEPAGA = :id_Mensalidade
		            AND HSSNOTAF.NNUMENOTAF = HSSCNOTA.NNUMENOTAF";
        $sql2->addParam(":id_Mensalidade", $id_pagamento);
        $sql2->executeQuery($txt2);
		
      	$pdf->Cell(0,3,' ',0,1);	
	    $pdf->Cell(0,3,'Observa��o: ',0,1);
        $pdf->SetFont('Arial','',9); 
        $pdf->Cell(0,3,'Para efeito do inciso IV do art. 22 Lei 8212/91 IN INSS 03/2005',0,1);
	    $pdf->Cell(0,3,'VALOR SERV. MEDICOS R$ '. $formata->formataNumero($sql->result("INSS")) . ' x 15% = '. str_repeat('.',42) . $formata->formataNumero($sql->result("PERCENTUAL"),2) ,0,1); 
        $pdf->Cell(0,3,' ',0,1);	
        $pdf->Cell(0,3,'(*) IR Lei n� 8541/92 - Artigo 45 Base de C�lculo'. str_repeat('.',42) . $formata->formataNumero($sql2->result("NTRIBNOTAF")) ,0,1);
       
        if ($sql2->result("NIR__NOTAF") > 0) {
          $pdf->Cell(0,3,'(-) Servi�os Pessoais al�quota legal 1.5% Imposto'. str_repeat('.',40) . $formata->formataNumero($sql2->result("NIR__NOTAF")) ,0,1);
        }  
      }		
    } 
	
    // Linha pontilhada
    $pdf->SetFont('Arial','',6);    
    $pdf->Cell(0,1,'Corte na linha pontilhada',0,1);
    $pdf->Cell(0,1,$pdf->Copy(str_repeat('-',300),189),0,1);
    $pdf->Ln(10);

    $pdf->SetFont('Arial','B',12);  
    $pdf->Image($pdf->imagem,10,$pdf->GetY()-3,$pdf->img_largura,$pdf->img_altura);  
    $pdf->Cell(45,6,'',0,0,'L');  
    $pdf->Cell(0.5,6,'',0,0,'L',true);
    $pdf->Cell(20,6,$boleto->Banco,0,0,'L');  
    $pdf->Cell(0.5,6,'',0,0,'L',true);
    $pdf->Cell(124,6,$boleto->LinhaDigitavel,0,1,'L');  
    
    $pdf->Cell(190,1,'',0,1,'L');      
    $pdf->Cell(190,0.5,'',0,1,'C',true);
    $pdf->Cell(190,1,'',0,1,'C');
      
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(90,2,'Benefici�rio',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'Coop. contratante / C�d. Benefici�rio',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {
      $pdf->Cell(10,2,'Moeda',0,0);
    }else{
      $pdf->Cell(10,2,'Esp�cie',0,0);
    }  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(15,2,'Quantidade',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(40,2,'Nosso N�mero',0,1);

    $pdf->SetFont('Arial','B',8);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(90,5,$pdf->Copy($boleto->Cedente,88),'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->CampoAgencia,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(10,5,$boleto->Especie,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);  
    $pdf->Cell(15,5,$boleto->Quantidade,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(40,5,$boleto->NossoNumero,'B',1);    

    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);    
    
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'N�mero do documento',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(20,2,'Contrato',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(40,2,'CPF/CEI/CNPJ',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(40,2,'Vencimento',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'Valor documento',0,1);
  
    $pdf->SetFont('Arial','B',8);   
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->NumeroDocumento,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(20,5,$boleto->Contrato,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(40,5,$boleto->SacadoCpf,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);  
    $pdf->Cell(40,5,$boleto->DataVencimento,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,$formata->formataNumero($boleto->Valor),'B',1);  
  
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);    
    
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {    
      $pdf->Cell(30,2,'(-) Desconto',0,0);
    }else{
      $pdf->Cell(30,2,'(-) Desconto / Abatimento',0,0);
    }  
    $pdf->Cell(1,2,'',0,0,'L',true); 
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {        
      $pdf->Cell(35,2,'(-) Outras dedu��es/Abatimento',0,0);
    }else{
      $pdf->Cell(35,2,'(-) Outras dedu��es',0,0);
    }
    $pdf->Cell(1,2,'',0,0,'L',true);
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {            
      $pdf->Cell(35,2,'(+) Mora / Multa / Juros',0,0);
    }else{
      $pdf->Cell(35,2,'(+) Mora / Multa',0,0);
    }
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'(+) Outros Acr�scimos',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'(=) Valor cobrado',0,1);  
    
    $pdf->SetFont('Arial','B',8);   
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,'','B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(35,5,'','B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(35,5,'','B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);  
    $pdf->Cell(30,5,'','B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,'','B',1); 
    
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);    
    
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(189,2,'Pagador / CPF',0,1);
    
    $pdf->SetFont('Arial','B',8);  
    $pdf->Cell(1,5,'',0,0,'L',true);     
    $pdf->Cell(168,5,$boleto->Sacado." / ".$boleto->SacadoCpf,'B',0);   
    $pdf->logoANS($boleto->RegistroANS,$pdf->GetX(),$pdf->GetY()+1);
    $pdf->Cell(21,5,'','B',1); 
    
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1); 

    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(170,2,'Autentica��o mec�nica',0,1,'R');
     
    $pdf->Ln(12);  
    
    // Linha pontilhada
    $pdf->SetFont('Arial','',6);    
    $pdf->Cell(0,1,'Corte na linha pontilhada',0,1);
    $pdf->Cell(0,1,$pdf->Copy(str_repeat('-',300),189),0,1);
    
    $pdf->Ln(12); 
  
    $pdf->SetFont('Arial','B',12);  
    $pdf->Image($pdf->imagem,10,$pdf->GetY()-3,$pdf->img_largura,$pdf->img_altura);   
    $pdf->Cell(45,6,'',0,0,'L');  
    $pdf->Cell(0.5,6,'',0,0,'L',true);
    $pdf->Cell(20,6,$boleto->Banco,0,0,'L');  
    $pdf->Cell(0.5,6,'',0,0,'L',true);
    $pdf->Cell(124,6,$boleto->LinhaDigitavel,0,1,'L');  
    
    $pdf->Cell(190,1,'',0,1,'L');    
  
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(190,0.5,'',0,1,'C',true);
    $pdf->Cell(190,1,'',0,1,'C');  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(133,2,'Local de pagamento',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'Vencimento',0,1);  

    $pdf->SetFont('Arial','B',8);   
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(133,5,$pdf->Copy($boleto->DescricaoLocalPagamento,132),'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,$boleto->DataVencimento,'B',1);
    
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   
    
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(133,2,'Benefici�rio',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'Coop. contratante / C�d. Benefici�rio',0,1);  
    
    $pdf->SetFont('Arial','B',8);     
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(133,5,$pdf->Copy($boleto->Cedente.' - '.$boleto->CpfCnpjCedente,132),'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,$boleto->CampoAgencia,'B',1); 
  
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);  
    
    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'Data do documento',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(44,2,'N� do documento',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(15,2,'Esp�cie doc.',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(10,2,'Aceite',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'Data process.',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'Nosso n�mero',0,1);  
  
    $pdf->SetFont('Arial','B',8);     
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->DataDocumento,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(44,5,$boleto->NumeroDocumento,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(15,5,$boleto->EspecieDoc,'B',0);    
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(10,5,$boleto->Aceite,'B',0);    
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->DataProcessamento,'B',0);    
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,$boleto->NossoNumero,'B',1); 
  
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   

    $pdf->SetFont('Arial','',6);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'Uso do banco',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(29,2,'Carteira',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {    
      $pdf->Cell(10,2,'Moeda',0,0);
    }else{
      $pdf->Cell(10,2,'Esp�cie',0,0);
    }
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'Quantidade',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(30,2,'x Valor',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'(=) Valor documento',0,1);  
  
    $pdf->SetFont('Arial','B',8);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->UsoBanco,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(29,5,$boleto->Carteira,'B',0);  
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(10,5,$boleto->Especie,'B',0);    
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->Quantidade,'B',0);    
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(30,5,$boleto->ValorUnitario,'B',0);    
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,$formata->formataNumero($boleto->Valor),'B',1);  
    
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   
    
    $pdf->SetFont('Arial','B',6);  
    $pdf->Cell(60,2,'Instru��es (Texto de responsabilidade do cedente)',0,0);
    $pdf->SetFont('Arial','',6);      
    $pdf->Cell(70,2,$pdf->Copy($boleto->PlanoSacado(),69),0,0);    
    $pdf->SetFont('Arial','',6);      
    $pdf->Cell(4,2,'27',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {
      $pdf->Cell(55,2,'(-) Desconto',0,1);
    }else{
      $pdf->Cell(55,2,'(-) Desconto / Abatimento',0,1);      
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();
    
    $pdf->Cell(130,5,'',0,0);
    $pdf->Cell(4,5,'',0,0);
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,'','B',1); 
    
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   
    
    $pdf->SetFont('Arial','',6);   
    $pdf->Cell(130,2,'',0,0);
    $pdf->Cell(4,2,'35',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {
      $pdf->Cell(55,2,'(-) Outras dedu��es/Abatimento',0,1);  
    }else{
      $pdf->Cell(55,2,'(-) Outras dedu��es',0,1);  
    }
    $pdf->SetFont('Arial','B',8);   
    $pdf->Cell(130,5,'',0,0);  
    $pdf->Cell(4,5,'',0,0);
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,'','B',1);   
  
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   

    $pdf->SetFont('Arial','',6);   
    $pdf->Cell(130,2,'',0,0);
    $pdf->Cell(4,2,'19',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);
    if ($_SESSION['apelido_operadora'] == 'unimedMineiros') {    
      $pdf->Cell(55,2,'(+) Mora / Multa / Juros',0,1);  
    }else{
      $pdf->Cell(55,2,'(+) Mora / Multa',0,1);  
    }
    $pdf->SetFont('Arial','B',8);     
    $pdf->Cell(130,5,'',0,0);  
    $pdf->Cell(4,5,'',0,0);
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,'','B',1);    
    
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   
    
    $pdf->SetFont('Arial','',6);   
    $pdf->Cell(130,2,'',0,0);
    $pdf->Cell(4,2,'',0,0);
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'(+) Outros acr�scimos',0,1);  
    
    $pdf->SetFont('Arial','B',8);       
    $pdf->Cell(130,5,'',0,0);  
    $pdf->Cell(4,5,'',0,0);
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,'','B',1);  
  
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1);   
    
    $pdf->Cell(113,2,'',0,0);
    
    $pdf->logoANS($boleto->RegistroANS,$pdf->GetX(),$pdf->GetY()+3);
    
    $pdf->SetFont('Arial','',6);   
    $pdf->Cell(17,2,'',0,0);    
    $pdf->Cell(4,2,'',0,0);  
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(55,2,'(=) Valor cobrado',0,1);  
      
    $pdf->SetFont('Arial','B',8);       
    $pdf->Cell(130,5,$boleto->MensagemJuros(),0,0);  
    $pdf->Cell(4,5,'',0,0);
    $pdf->Cell(1,5,'','B',0,'L',true);    
    $pdf->Cell(55,5,'','B',1);    
    
    $x1 = $pdf->GetX();
    $y1 = $pdf->GetY();
   
    if($_SESSION["apelido_operadora"] == "asfal"){
    $pdf->SetFont('Arial','B',10);   
    $texto = $boleto->InstrucaoBancaria();
    $pdf->SetXY($x,$y);  
    $pdf->MultiCell(130,3,$texto,0,'J',0,9); 
    $pdf->SetXY($x1,$y1); 
    $pdf->SetFont('Arial','',6); 
    }else{
    $pdf->SetFont('Arial','',6);   
    $texto = $boleto->InstrucaoBancaria();
    $pdf->SetXY($x,$y);  
    $pdf->MultiCell(130,3,$texto,0,'J',0,9); 
    $pdf->SetXY($x1,$y1); 
    } 
    
    //Espa�o entre linha
    $pdf->Cell(190,5,'','B',1); 
    
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(189,2,'Pagador',0,1);
    
    $pdf->SetFont('Arial','B',8);     
    $pdf->Cell(1,4,'',0,0,'L',true);    
    $pdf->Cell(189,4,$boleto->Sacado." - ".$boleto->SacadoCpf,0,1); 

    $pdf->Cell(1,4,'',0,0,'L',true);    
    $pdf->Cell(189,4,$boleto->Endereco1,0,1); 

    $pdf->Cell(1,4,'',0,0,'L',true);    
    $pdf->Cell(189,4,$boleto->Endereco2,0,1); 
  
    //Espa�o entre linha
    $pdf->Cell(190,1,'',0,1); 
    $pdf->SetFont('Arial','',6);     
    $pdf->Cell(1,2,'',0,0,'L',true);  
    $pdf->Cell(189,2,'Sacador avalista',0,1);
    
    $pdf->SetFont('Arial','B',8);     
    $pdf->Cell(1,4,'',0,0,'L',true);    
    $pdf->Cell(189,4,$boleto->Avalista,0,1); 

    $pdf->SetFont('Arial','',6);      
    $pdf->Cell(1,4,'','B',0,'L',true);    
    $pdf->Cell(133,4,'','B',0); 
    $pdf->Cell(1,4,'','B',0,'L',true);    
    $pdf->Cell(55,4,'C�d. baixa','B',1); 

    $pdf->Cell(190,4,'Autentica��o mec�nica - Ficha de Compensa��o',0,1,'R'); 
    
    if ($boleto->Modalidade == 'D') {
      $pdf->SetFont('Arial','B',8);     
      $pdf->MultiCell(190,4,$boleto->DebitoConta); 
    }
    else {    
      $barra = $boleto->CodigoBarras;
      $pdf->CodigoBarra($barra);
    }
  
    $pdf->Ln(20);  
    $pdf->SetFont('Arial','',6);    
    $pdf->Cell(0,1,'Corte na linha pontilhada',0,1);
    $pdf->Cell(0,1,str_repeat('-',260),0,1);
  
    //$file=basename(tempnam("../temp/",'pdf'));
    
    if ($file == '') {
      $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
      $pdf->Output($file,'F');          
      echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
    } else {
      $pdf->Output($file,'F');    

      if(file_exists($file)) {            
        $arquivo_anexo[0]['arquivo'] = $file; 
        $arquivo_anexo[0]['name'] = basename('boleto_'.$id_paga.'.pdf');
        $arquivo_anexo[0]['type'] = 'pdf';

        $msg = "<p>Boleto de cobran�a<br/>".
               "N� do pedido: ".$id_pedido."<br /><br />".
               "Cordialmente<br />".
               $formata->initcap($_SESSION['nome_operadora']);
               
        if ($_SESSION['sistema'] == "Empresa")
          $pasta = "empresa";
        else if ($_SESSION['sistema'] == "Usuario")
          $pasta = "usuario";
        else if ($_SESSION['sistema'] == "Comercial")
          $pasta = "comercial";    
               
        $parametros = array();
        
        if ($func->enviaEmail($para,'boleto',$msg,$arquivo_anexo) == '') {        
          $parametros[0]['nome']  = 'op';
          $parametros[0]['valor'] = '600';
          $parametros[1]['nome']  = 'prot';
          $parametros[1]['valor'] = $id_pedido;         
          
          echo $util->redireciona2('../'.$pasta.'/confirmacao.php?idSessao='.$_GET['idSessao'],$parametros,'N');
          
          $protocolo = insere_protocolo($bd,$id_pedido,'I',$para,'E');

        } else {
          $parametros[0]['nome']  = 'op';
          $parametros[0]['valor'] = '601';
          $parametros[1]['nome']  = 'prot';
          $parametros[1]['valor'] = $id_pedido;         
        
          echo $util->redireciona2('../'.$pasta.'/confirmacao.php?idSessao='.$_GET['idSessao'],'N');
        }              
      } 
    }
  } else
    echo "Layout de boleto n�o dispon�vel para a impress�o pela web. (".$boleto->LayoutBoleto.")";
  
  $bd->close();  
?>