<?php
  //require_once("../comum/BD.class.php");
  //$bd = new Oracle();
  //define("FPDF_FONTPATH", "../comum/pdf/font");
  //require_once("../comum/pdf/fpdf.php");

  class PDF extends PDFSolus {
    
    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',9);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(150,5,'RELAT�RIO DE EXAMES VENCIDOS',0,1);
      $this->SetFont('Arial','B',7);
//      $this->Cell(40,3,'');
//      $this->Cell(25,3,"Empresa:",0,0,'R');
//      $this->Cell(205,3,$_SESSION['titular_contrato']." - ".$_SESSION['cnpj_contrato'],0,1);
           
      $this->Ln(9);
      $this->Cell(190,1,' ','B',0);
      $this->Ln(2);


      $this->SetFont('Arial','B',8);
      $this->Cell(80,3,'Funcion�rio',0,0);
      $this->Cell(20,3,'Validade',0,0);      
	  $this->Cell(20,3,'Situa��o',0,0);
	  $this->Cell(45,3,'Procedimento',0,1);
	  $this->Cell(190,1,' ','B',0);
      $this->Ln(3);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('P','mm','A4');
  $pdf->AliasNbPages();
  
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  //require_once("../comum/funcao.php");

  $id_contrato = $_SESSION['id_contrato'];
  $data_fim = $_POST['data'];
  $txtAg = "SELECT DISTINCT HSSEXPAD.CCODIPMED,HSSPMED.CNOMEPMED  
			  FROM HSSAGEFU,HSSAGENT,HSSEMPR,HSSTITU,HSSEXPAD, HSSPMED
			 WHERE HSSAGEFU.NNUMEAGENT = HSSAGENT.NNUMEAGENT
			   AND HSSAGENT.CSITUAGENT = 'A'
			   AND HSSAGEFU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
			   AND HSSEMPR.NNUMEEMPR = HSSTITU.NNUMEEMPR
			   AND HSSAGENT.NNUMEAGENT = HSSEXPAD.NNUMEAGENT
			   AND HSSEXPAD.CCODIPMED = HSSPMED.CCODIPMED
			   AND HSSTITU.NNUMETITU = :contrato
             ORDER BY CCODIPMED ASC";

  $sqlAgentes = new Query($bd);
  $sqlAgentes->addParam(":contrato",$id_contrato);
  //$sqlAgentes->addParam(":fim",$data_fim);  
  $sqlAgentes->executeQuery($txtAg);
 
  while (!$sqlAgentes->eof()) { 
    $txt = "SELECT SUBSTR(CNOMEUSUA,1,60) CNOMEUSUA,ADD_MONTHS(ULTIMA_REALIZACAO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA,'".$sqlAgentes->result("CCODIPMED")."'),12) VALIDADE,
                   (CASE WHEN ADD_MONTHS(ULTIMA_REALIZACAO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA,'".$sqlAgentes->result("CCODIPMED")."'),12) > SYSDATE THEN
                      'A VENCER'
                         WHEN ADD_MONTHS(ULTIMA_REALIZACAO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA,'".$sqlAgentes->result("CCODIPMED")."'),12) <= SYSDATE THEN
                      'VENCIDO'
            END) STATUS
              FROM HSSEMPR,HSSTITU,HSSUSUA
             WHERE HSSEMPR.NNUMEEMPR = HSSTITU.NNUMEEMPR
               AND HSSTITU.NNUMETITU = HSSUSUA.NNUMETITU
               AND HSSTITU.NNUMETITU = :contrato
               AND ADD_MONTHS(ULTIMA_REALIZACAO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA,'".$sqlAgentes->result("CCODIPMED")."'),12) <= :fim
               AND CSITUUSUA = 'A' 
             ORDER BY CNOMEUSUA, ADD_MONTHS(ULTIMA_REALIZACAO_PROCEDIMENTO(HSSUSUA.NNUMEUSUA,'".$sqlAgentes->result("CCODIPMED")."'),12),CNOMEUSUA";

    $sql = new Query($bd);
    $sql->addParam(":contrato",$id_contrato);
    $sql->addParam(":fim",$data_fim);  
    $sql->executeQuery($txt);
    

    while (!$sql->eof()) {
     
      $pdf->Cell(80,5,$sql->result("CNOMEUSUA"),0,0);    
      $pdf->Cell(20,5,$sql->result("VALIDADE"),0,0);     
      $pdf->Cell(20,5,$sql->result("STATUS"),0,0);    
      $pdf->Cell(45,5,substr($sqlAgentes->result("CNOMEPMED"),0,38),0,1);    
      $sql->next();
    }
    $sqlAgentes->next(); 
  }
  $file=basename(tempnam("../temp/",'tmp'));
  $file='../temp/'.$file.'.pdf';
  $pdf->Output($file,'F');
  
  echo "<HTML><SCRIPT>window.open('$file');</SCRIPT></HTML>"; //document.location='principal.php';


?>