<?php

class Boleto {

    private $Layouts_ABN = array('P', '2', 'BB', '+');
    private $Layouts_Banese = array('AS');
    private $Layouts_Banestes = array('E');
    private $Layouts_BB = array('I', ')', '!', 'F', 'k', '.', 'r', '.1', 'O', 'UM', '.d', 'OO', '.b');
    private $Layouts_BlueStar = array('BS');
    private $Layouts_Bradesco = array('H', '18', 'SB', 'BR', 'a');
    private $Layouts_CEF = array('=', '?', '==', '�', '��', 'D', '[]');
    private $Layouts_HSBC = array('5', '3', '.3','.4');
    private $Layouts_Itau = array('C', '14', 'T', 'CB', '*');
    private $Layouts_Mercantil = array('#');
    private $Layouts_Santander = array('SA', 'DD');
    private $Layouts_SICOOB = array('N');
    private $Layouts_Sicredi = array('.K', 'KR');
    private $Layouts_Unibanco = array('9');
    private $Layouts_Basa = array('AB');
    private $Layouts_Padrao = array('15');
    private $Layouts_Sicredi_HGU = array('@@');
    private $IdPagamento;
    private $IdJpaga;
    private $Bd;
  
    public $Sacado;
    public $Endereco1;
    public $Endereco2;
    public $CpfCnpjCedente;
    public $Cedente;
    public $RegistroANS;
    public $DataVencimento;
    public $CpfCnpjEmpresa;
  
    private $DataJuliana;
    private $FatorVencimento;
    public $NossoNumero;
    public $Valor;
    public $Modalidade;
    private $Agencia;
    private $DigitoAgencia;
    private $Conta;
    private $DigitoConta;
    private $Convenio;
    public $Avalista;
    private $Matricula;
    private $Radical;
    public $Carteira;
    public $LayoutBoleto;
    private $CodigoCedente;
    private $TipoCobranca;
    private $CedenteCorrespondente;
    private $NomeCorrespondente;
    private $AgenciaCorrespondente;
    private $BancoCorrespondente;
    private $LocalPagamento;
    private $PlanoSacado;
    private $ValorOriginal;
    private $Competencia;
    public $Plano;
    public $DataDocumento;
    public $NumeroDocumento;
    private $Endereco;
    private $Cidade;
    private $IdContrato;
    private $IdUsuario;
    private $campo_livre;
    public $DataProcessamento;
  
    public $Quantidade;
    public $Aceite;
    public $Especie;
    public $EspecieDoc;
    public $EspecieDocumento;
    public $Contrato;
    public $UsoBanco;
    public $ValorUnitario;
    public $DescricaoLocalPagamento;
    public $Banco;
  
    public $CorR;
    public $CorG;
    public $CorB;
    public $Imagem;
    public $ImagemLargura;
    public $ImagemAltura;

    public function __construct($IdPagamento, $IdJpaga = 0) {
        $this->IdPagamento = $IdPagamento;
        $this->IdJpaga = $IdJpaga;

        $this->ProcessaBoleto();
        $this->DefineBoleto();
    }

    private function ProcessaBoleto() {
        $formata = new Formata();
        $func = new Funcao();
        $bd = new Oracle();

        $txt = "SELECT NOME_TITULAR_BOLETO(HSSPAGA.NNUMEPAGA) TITULAR,TO_CHAR(DVENCPAGA,'DD/MM/YYYY') DVENCPAGA,NDOCUPAGA,
                   NVENCPAGA + NVL(NMULTPAGA,0) + NVL(NJUROPAGA,0) NVENCPAGA,TO_CHAR(TRUNC(SYSDATE),'DD/MM/YYYY') DATA,
                   CAGENLOPG,CDVAGLOPG,CCC__LOPG,CDVCCLOPG,NCONTLOPG,NCARTLOPG,NVL(CCGC_ESTAB,FINEMPR.CCGC_EMPR) CCGC_EMPR,FINEMPR.CNOMEEMPR,FINEMPR.CENDEEMPR,FINEMPR.CCIDAEMPR,
                   DVENCPAGA - TO_DATE('07/10/1997','DD/MM/YYYY') FV,CLREMLOPG,CCEDELOPG,HSSPAGA.NNUMELOPG,CTCOBLOPG,CCCEDLOPG,
                   DECODE(HSSPAGA.NNUMEUSUA,NULL,CENDETITU,DECODE(CCEP_USUA,NULL,CENDETITU,CENDEUSUA)) CENDETITU,
                   DECODE(HSSPAGA.NNUMEUSUA,NULL,CBAIRTITU,DECODE(CCEP_USUA,NULL,CBAIRTITU,CBAIRUSUA)) CBAIRTITU,
                   DECODE(HSSPAGA.NNUMEUSUA,NULL,CCIDATITU,DECODE(CCEP_USUA,NULL,CCIDATITU,CCIDAUSUA)) CCIDATITU,
                   DECODE(HSSPAGA.NNUMEUSUA,NULL,CESTATITU,DECODE(CCEP_USUA,NULL,CESTATITU,CESTAUSUA)) CESTATITU,
                   DECODE(HSSPAGA.NNUMEUSUA,NULL,C_CEPTITU,DECODE(CCEP_USUA,NULL,C_CEPTITU,CCEP_USUA)) C_CEPTITU,
                   DECODE(NNOTANOTAF,NULL,TO_CHAR(NDOCUPAGA),'NF ' || TO_CHAR(NNOTANOTAF)) || ' ' || CSERINOTAF DOCUMENTO,NVL(HSSPAGA.NNUMETITU,0) NNUMETITU,
                   NVENCPAGA VALOR1, HSSEMPR.C_CGCEMPR,CRADILOPG,CMATRLOPG,CCSACLOPG,CMODALOPG,NVL(HSSPAGA.NNUMEUSUA,0) NNUMEUSUA,
                   ACRESCENTA_ZEROS(TRUNC(DVENCPAGA-TO_DATE('01/01/' || TO_CHAR(DVENCPAGA,'YYYY'),'DD/MM/YYYY')) + 1,3) || SUBSTR(TO_CHAR(DVENCPAGA,'YYYY'),4,1) DATA_JULIANA, HSSTITU.NNUMETITU,
                   CPSACLOPG,DECODE(HSSPAGA.NNUMEUSUA,NULL,HSSTITU.NNUMEPLAN,HSSUSUA.NNUMEPLAN) PLANO,NVL(HSSPPROP.NNUMEPROPO,0) NNUMEPROPO,NVL(CCEDEESTAB,NVL(CCEDECONGE,CBOLECONF)) CBOLECONF,
                   CCAGELOPG,CCEDCLOPG,CCBANLOPG,HSSPAGA.CCOMPPAGA,HSSPAGA.NNUMEVEND,CIMPBLOPG,HSSPAGA.CPAGOPAGA,NDVENLOPG,HSSTITU.NNUMESTAT,HSSPAGA.DPARCPAGA,
                   CESPDLOPG,HSSPAGA.NNUMESETOR,CBANCLOPG,CODIGO_TITULAR_BOLETO(HSSPAGA.NNUMEPAGA) CODIGO_TITULAR,CCODITITU
              FROM HSSPAGA,HSSLOPG,FINEMPR,HSSTITU,HSSEMPR,HSSUSUA,HSSCNOTA,HSSNOTAF,HSSPPROP,HSSCONF,SFPESTAB,HSSCONGE
             WHERE HSSPAGA.NNUMEPAGA = :id_pagamento
               AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG(+)
               AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU(+)
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)
               AND HSSPAGA.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
               AND HSSPAGA.NNUMEPAGA = HSSCNOTA.NNUMEPAGA(+)
               AND HSSPAGA.NNUMEPAGA = HSSPPROP.NNUMEPAGA(+)
               AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF(+)
               AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
               AND HSSPAGA.NNUMEESTAB = SFPESTAB.NNUMEESTAB(+) ";

        $sql = new Query();
        $sql->addParam(":id_pagamento", $this->IdPagamento);
        $sql->executeQuery($txt);

        if ($sql->result("NNUMEPROPO") > 0) {
            $sql2 = new Query();
            $txt2 = "SELECT CNOMEPROSP,CENDEPROSP,CNUMEPROSP,CDESCTLOGR,CBAIRPROSP,CCEP_PROSP,CESTAPROSP,CCIDAPROSP,CCOMPPROSP,
                      CPESSPROSP,CCNPJPROSP,CCPF_PROSP,NVL(CNOMEESTAB,CBOLECONF) CNOMEESTAB,NVL(CCGC_ESTAB,CCGC_EMPR) CCGC_ESTAB,
                      NVL(NMS__ESTAB,NMS__CONF) NMS__ESTAB
                 FROM HSSPROPO,HSSPROSP,HSSTLOGR,SFPESTAB,HSSCONF,FINEMPR 
                WHERE HSSPROPO.NNUMEPROPO = :id
                  AND HSSPROPO.NNUMEPROSP = HSSPROSP.NNUMEPROSP
                  AND HSSPROSP.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)
                  AND HSSPROSP.NNUMEESTAB = SFPESTAB.NNUMEESTAB(+) ";
            $sql2->addParam(":id", $sql->result("NNUMEPROPO"));
            $sql2->executeQuery($txt2);

            $this->Sacado = $sql2->result("CNOMEPROSP");
            $this->Endereco1 = $sql2->result("CDESCTLOGR") . " " . $sql2->result("CENDEPROSP") . ", " . $sql2->result("CNUMEPROSP") . " " . $sql2->result("CCOMPPROSP") . " - " . $sql2->result("CBAIRPROSP");
            $this->Endereco2 = $sql2->result("CCIDAPROSP") . " - " . $sql2->result("CESTAPROSP") . " - " . $sql2->result("CCEP_PROSP");

            if ($sql2->result("CPESSPROSP") <> 'F')
                $this->SacadoCpf = $sql2->result("CCNPJPROSP");
            else
                $this->SacadoCpf = $sql2->result("CCPF_PROSP");

            $this->CpfCnpjCedente = $formata->formataCNPJ($sql2->result("CCGC_ESTAB"));
            $this->Cedente = $sql2->result("CNOMEESTAB");
            $this->RegistroANS = $formata->formataRegistroAns($sql2->result("NMS__ESTAB"));

        } else {
            $this->CpfCnpjCedente = $formata->formataCNPJ($sql->result("CCGC_EMPR"));
            $this->CpfCnpjEmpresa = $formata->formataCNPJ($sql->result("C_CGCEMPR"));
            $this->Cedente = $sql->result("CBOLECONF");
            $this->RegistroANS = $formata->formataRegistroAns($_SESSION['registro_ans']);
            $this->Sacado = $sql->result("TITULAR") . " - Contrato: " . $sql->result("CCODITITU");
            $this->SacadoCpf = $func->retornaCnpjCpfContrato($sql->result("NNUMETITU"), $sql->result("NNUMEUSUA"), $sql->result("NNUMESETOR"));

            if ($sql->result("NNUMESETOR") > 0) {
                $this->Endereco1 = $func->retornaEndereco1Locacao($sql->result("NNUMESETOR"));
                $this->Endereco2 = $func->retornaEndereco2Locacao($sql->result("NNUMESETOR"));
            } 
			else {
                $this->Endereco1 = $sql->result("CENDETITU") . " - " . $sql->result("CBAIRTITU");
                $this->Endereco2 = $sql->result("CCIDATITU") . " - " . $sql->result("CESTATITU") . " - " . $sql->result("C_CEPTITU");
            }

        }

        $this->Plano = $sql->result("PLANO");

        if ($this->IdJpaga > 0) {

            $txt_jpaga = "SELECT TO_CHAR(DVENCJPAGA,'DD/MM/YYYY') DVENCJPAGA,HSSLOPG.NNUMELOPG,NVENCJPAGA,NDOCUJPAGA,DVENCJPAGA - TO_DATE('07/10/1997','DD/MM/YYYY') FV,
                           ACRESCENTA_ZEROS(TRUNC(DVENCJPAGA-TO_DATE('01/01/' || TO_CHAR(DVENCJPAGA,'YYYY'),'DD/MM/YYYY')) + 1,3) || SUBSTR(TO_CHAR(DVENCJPAGA,'YYYY'),4,1) DATA_JULIANA,
                           CMODALOPG,CAGENLOPG,CDVAGLOPG,CCC__LOPG,CDVCCLOPG,NCONTLOPG,CCSACLOPG,CMATRLOPG,CRADILOPG,NCARTLOPG,CLREMLOPG,CCEDELOPG,CTCOBLOPG,CCCEDLOPG,
                           CCEDCLOPG,CCAGELOPG,CCBANLOPG,CPSACLOPG,HSSPAGA.NVENCPAGA,HSSPAGA.CCOMPPAGA,CIMPBLOPG,HSSPAGA.CPAGOPAGA,NDVENLOPG,
                           CESPDLOPG,CBANCLOPG
                      FROM HSSJPAGA,HSSLOPG,HSSMJPAG,HSSPAGA
                     WHERE HSSJPAGA.NNUMEJPAGA = :jpaga 
                       AND HSSJPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG 
                       AND HSSJPAGA.NNUMEJPAGA = HSSMJPAG.NNUMEJPAGA
                       AND HSSMJPAG.NNUMEPAGA = HSSPAGA.NNUMEPAGA";
            $sql_jpaga = new Query();
            $sql_jpaga->addParam(":jpaga", $this->IdJpaga);
            $sql_jpaga->executeQuery($txt_jpaga);

            $this->DataVencimento = $sql_jpaga->result("DVENCJPAGA");
            $this->DataJuliana = $sql_jpaga->result("DATA_JULIANA");
            $this->FatorVencimento = $sql_jpaga->result("FV");
            $this->NossoNumero = $sql_jpaga->result("NDOCUJPAGA");
            $this->Valor = $sql_jpaga->result("NVENCJPAGA");

            $this->Modalidade = $sql_jpaga->result("CMODALOPG");
            $this->Agencia = $sql_jpaga->result("CAGENLOPG");
            $this->DigitoAgencia = $sql_jpaga->result("CDVAGLOPG");
            $this->Conta = $sql_jpaga->result("CCC__LOPG");
            $this->DigitoConta = $sql_jpaga->result("CDVCCLOPG");
            $this->Convenio = $sql_jpaga->result("NCONTLOPG");
            $this->Avalista = $sql_jpaga->result("CCSACLOPG");
            $this->Matricula = $sql_jpaga->result("CMATRLOPG");
            $this->Radical = $sql_jpaga->result("CRADILOPG");
            $this->Carteira = $sql_jpaga->result("NCARTLOPG");
            $this->LayoutBoleto = $sql_jpaga->result("CLREMLOPG");
            $this->CodigoCedente = $sql_jpaga->result("CCEDELOPG");
            $this->TipoCobranca = $sql_jpaga->result("CTCOBLOPG");
            $this->CedenteCorrespondente = $sql_jpaga->result("CCEDCLOPG");
            $this->NomeCorrespondente = $sql_jpaga->result("CCCEDLOPG");
            $this->AgenciaCorrespondente = $sql_jpaga->result("CCAGELOPG");
            $this->BancoCorrespondente = $sql_jpaga->result("CCBANLOPG");
            $this->LocalPagamento = $sql_jpaga->result("NNUMELOPG");
            $this->PlanoSacado = $sql_jpaga->result("CPSACLOPG");
            $this->ValorOriginal = $formata->formataNumero($sql_jpaga->result("NVENCPAGA"), 2);
            $this->Competencia = $sql_jpaga->result("CCOMPPAGA");
            $this->EspecieDocumento = $sql_jpaga->result("CESPDLOPG");
            $this->Banco = $sql_jpaga->result("CBANCLOPG");
            $this->NumeroDocumento = $sql_jpaga->result("NDOCUJPAGA");
        } else {
            $this->DataVencimento = $sql->result("DVENCPAGA");
            $this->DataJuliana = $sql->result("DATA_JULIANA");
            $this->FatorVencimento = $sql->result("FV");
            $this->NossoNumero = $sql->result("NDOCUPAGA");
            $this->Valor = $sql->result("NVENCPAGA");

            $this->Modalidade = $sql->result("CMODALOPG");
            $this->Agencia = $sql->result("CAGENLOPG");
            $this->DigitoAgencia = $sql->result("CDVAGLOPG");
            $this->Conta = $sql->result("CCC__LOPG");
            $this->DigitoConta = $sql->result("CDVCCLOPG");
            $this->Convenio = $sql->result("NCONTLOPG");
            $this->Avalista = $sql->result("CCSACLOPG");
            $this->Matricula = $sql->result("CMATRLOPG");
            $this->Radical = $sql->result("CRADILOPG");
            $this->Carteira = $sql->result("NCARTLOPG");
            $this->LayoutBoleto = $sql->result("CLREMLOPG");
            $this->CodigoCedente = $sql->result("CCEDELOPG");
            $this->TipoCobranca = $sql->result("CTCOBLOPG");
            $this->CedenteCorrespondente = $sql->result("CCEDCLOPG");
            $this->NomeCorrespondente = $sql->result("CCCEDLOPG");
            $this->AgenciaCorrespondente = $sql->result("CCAGELOPG");
            $this->BancoCorrespondente = $sql->result("CCBANLOPG");
            $this->LocalPagamento = $sql->result("NNUMELOPG");
            $this->PlanoSacado = $sql->result("CPSACLOPG");
            $this->ValorOriginal = $formata->formataNumero($sql->result("NVENCPAGA"), 2);
            $this->Competencia = $sql->result("CCOMPPAGA");
            $this->EspecieDocumento = $sql->result("CESPDLOPG");
            $this->Banco = $sql->result("CBANCLOPG");
            $this->NumeroDocumento = $sql->result("DOCUMENTO");
        }
            if (!in_array($this->LayoutBoleto, $this->Layouts_Sicredi_HGU))
        	$this->Banco = substr($this->Banco, 1, 3);

        if (($sql->result("NNUMEVEND") > 0) and ( $sql->result("NNUMETITU") == 0) and ( $sql->result("NNUMEPROPO") == 0)) {
            $this->DataVencimento = "";
            $this->Valor = "";
            $this->Sacado = "";
            $this->Endereco1 = "";
            $this->Endereco2 = "";
            $this->FatorVencimento = "0000";
            $this->DataJuliana = "0000";
        }

        $this->DataParcelamento = $sql->result("DPARCPAGA");
        $this->StatusContrato = $sql->result("NNUMESTAT");
        $this->DiasAposVencido = $sql->result("NDVENLOPG");
        $this->Pago = $sql->result("CPAGOPAGA");
        $this->ImprimeBoleto = $sql->result("CIMPBLOPG");
        $this->DataDocumento = $sql->result("DATA");
        //$this->NumeroDocumento       = $sql->result("DOCUMENTO"); 
        $this->CodigoTitular = $sql->result("CODIGO_TITULAR");
        $this->Endereco = $sql->result("CENDEEMPR");
        $this->Cidade = $sql->result("CCIDAEMPR");
        $this->IdContrato = $sql->result("NNUMETITU");
        $this->IdUsuario = $sql->result("NNUMEUSUA");
        $this->DataProcessamento = $sql->result("DATA");

        $this->AgenciaCorrespondente = $this->Formatar($this->AgenciaCorrespondente, 5, "0");
        $bd->close();
    }

    public function PlanoSacado() {
        if (($this->PlanoSacado == 'S') and ( $this->Plano > 0)) {
            $sql = new Query();
            $txt = "SELECT CBOLEPLAN,NRGMSPLAN,CCODIPLAN FROM HSSPLAN WHERE NNUMEPLAN = :plano ";
            $sql->addParam(":plano", $this->Plano);
            $sql->executeQuery($txt);

            return "Plano: " . $sql->result("CBOLEPLAN") . " N�: " . $sql->result("CCODIPLAN") . " ANS: " . $sql->result("NRGMSPLAN");
        } else
            return "";
    }

    public function ValidaLayout($layout) {
        if (in_array($layout, $this->Layouts_BB) or
                in_array($layout, $this->Layouts_ABN) or
                in_array($layout, $this->Layouts_CEF) or
                in_array($layout, $this->Layouts_SICOOB) or
                in_array($layout, $this->Layouts_Unibanco) or
                in_array($layout, $this->Layouts_Bradesco) or
                in_array($layout, $this->Layouts_Itau) or
                in_array($layout, $this->Layouts_Sicredi) or
                in_array($layout, $this->Layouts_Banestes) or
                in_array($layout, $this->Layouts_Mercantil) or
                in_array($layout, $this->Layouts_HSBC) or
                in_array($layout, $this->Layouts_Santander) or
                in_array($layout, $this->Layouts_BlueStar) or
                in_array($layout, $this->Layouts_Banese) or
                in_array($layout, $this->Layouts_Basa) or
                in_array($layout, $this->Layouts_Sicredi_HGU) or
                in_array($layout, $this->Layouts_Padrao))
            return true;
        else
            return false;
    }

    public function ImprimeBoleto($vencido) {
        $data = new Data();
        /*
          - O layout do boleto deve estar preparado para a web
          - O boleto tem que estar em aberto
          - Marcar no local de pagamento que imprime boletos
          - A data de vencimento deve ser maior ou igual a data atual ou
          o operador deve possuir o direito de imprimir boleto vencido e o n�mero de dias vencidos n�o for superior
          ao n�mero de dias configurados
         */
        /*
          sdebug("Pago: ".$this->Pago."\n".
          "Data de parcelamento: ".$this->DataParcelamento."\n".
          "Imprime boleto: ".$this->ImprimeBoleto."\n".
          "Layout Valido: ".$this->ValidaLayout($this->LayoutBoleto)."\n".
          "Vencimento: ".$this->DataVencimento."\n".
          "Config. Dias apos vencido: ".$this->DiasAposVencido." * ".$_SESSION['dias_apos_vencido']."\n".
          "Data atual: ".date('d/m/Y')."\n".
          "Imprime vencido? ".$vencido."\n".
          "Dias vencido: ".$data->numeroDias(date('d/m/Y'),$this->DataVencimento)."\n".
          "Dias impressao: ".$_SESSION['dias_impressao_boleto']."\n".
          $_SESSION['apelido_operadora']
          );
         */
        $resultado = true;

        if ($this->Pago == 'S')
            $resultado = false;
        else if ($this->DataParcelamento <> '')
            $resultado = false;
        else if ($this->ImprimeBoleto == 'N')
            $resultado = false;
        else if ($this->ValidaLayout($this->LayoutBoleto) == false)
            $resultado = false;
        else {
            $datavencimento = explode("/", $this->DataVencimento);
            $datavencimento = $datavencimento[2] . $datavencimento[1] . $datavencimento[0];

            $dataatual = explode("/", $data->dataAtual('DD/MM/YYYY'));
            $dataatual = $dataatual[2] . $dataatual[1] . $dataatual[0];

            if ($this->DiasAposVencido > 0)
                $dias_apos_vencido = $this->DiasAposVencido;
            else
                $dias_apos_vencido = $_SESSION['dias_apos_vencido'];

      if (($datavencimento < $dataatual) and ($vencido == false))
                $resultado = false;
            else if (($vencido == true) and ( $data->numeroDias(date('d/m/Y'), $this->DataVencimento) > $dias_apos_vencido ))
                $resultado = false;
            else if ($data->numeroDias($this->DataVencimento, date('d/m/Y')) > $_SESSION['dias_impressao_boleto'])
                $resultado = false;
            else if (($_SESSION['apelido_operadora'] == 'garantia') and ( $this->StatusContrato <> ''))
                $resultado = false;
            else
                $resultado = true;
        }

        //sdebug($resultado);
        return $resultado;
    }

    private function DefineBoleto() {

        $formata = new Formata();

        $this->Quantidade = "";
        $this->Aceite = "N";
        $this->Especie = "R$";
        $this->EspecieDoc = "DM";
        $this->Contrato = "";
        $this->UsoBanco = "";
        $this->ValorUnitario = "";
        $this->DescricaoLocalPagamento = "QUALQUER BANCO AT� O VENCIMENTO";

        if (in_array($this->LayoutBoleto, $this->Layouts_BB) or
           ( in_array($this->LayoutBoleto, $this->Layouts_BlueStar) and $this->Banco == "001")) {

            if ($this->LayoutBoleto == ".b")
                $this->Banco = "090-6"; // Unicred
            else
                $this->Banco = "001-9"; // Banco do Brasil
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Conta = $this->Formatar($this->Conta, 8, "0");
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");
            $this->ValorUnitario = $this->Valor;

            if ($_SESSION['apelido_operadora'] == "caurj") {
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 17, "0");
                $this->campo_livre = $this->Formatar($this->Convenio, 6, "0") . $this->NossoNumero . "21";
            } 
			else if ((strlen($this->Convenio) == 6) && ($this->Carteira == "18")) {
                if ($_SESSION['apelido_operadora'] == "sinpacel") {
                    $this->NossoNumero = $this->Formatar($this->Convenio . $this->Formatar($this->NossoNumero, 5, "0"), 11, "0");
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . $this->Conta . $this->Carteira;
                } 
				else if ($_SESSION['apelido_operadora'] == "vitallis") {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 17, "0");
                    $this->campo_livre = $this->Convenio . $this->NossoNumero . "21";
                } 
				else {
                    $this->NossoNumero = $this->Formatar($this->Convenio . $this->NossoNumero, 11, "0");
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . $this->Conta . $this->Carteira;
                }

                $this->NossoNumero = ($this->NossoNumero . "-" . $this->modulo11_nn_bb($this->NossoNumero));
            } 
			else if (strlen($this->Convenio) == 6) {
                if ($_SESSION['apelido_operadora'] == 'amazoniaSaude') {
                    $this->campo_livre = $this->Convenio . $this->Formatar($this->NossoNumero, 17, "0") . "21";
                    $this->NossoNumero = $this->Convenio . $this->NossoNumero;
                    $this->NossoNumero = $this->NossoNumero . "-" . $this->modulo11_nn_bb($this->NossoNumero);
                } 
				else if ($this->LayoutBoleto == ".b") {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 10, "0");
                    $this->campo_livre = $this->Agencia . $this->Formatar($this->Conta . $this->DigitoConta, 10, "0") . $this->NossoNumero . $this->modulo11_nn_unicred($this->NossoNumero);
                    $this->NossoNumero = $this->NossoNumero . "-" . $this->modulo11_nn_unicred($this->NossoNumero);
                    $this->EspecieDoc = "DS";
                } 
				else {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 17, "0");
                    $this->campo_livre = $this->Convenio . $this->NossoNumero . "21";
                    $this->NossoNumero = $this->NossoNumero . "-" . $this->modulo11_nn_bb($this->NossoNumero);
                }

            } 
			else if (strlen($this->Convenio) == 7) {
                if ($_SESSION['apelido_operadora'] == "uniaomedica") {
                    $this->NossoNumero = $this->Formatar($this->Convenio, 7, "0") . $this->Formatar($this->NossoNumero, 10, "0");
                    $this->campo_livre = "000000" . $this->NossoNumero . $this->Carteira;
                } else if (($_SESSION['apelido_operadora'] == "unimedJatai") or ( $_SESSION['apelido_operadora'] == "unimedAraguaina")) {
                    $this->NossoNumero = $this->Formatar($this->Convenio, 7, "0") . $this->Formatar($this->NossoNumero, 9, "0");
                    $this->NossoNumero = $this->NossoNumero . $this->calcula_digito_barras($this->NossoNumero);
                    $this->campo_livre = "457415" . $this->NossoNumero . "21";
                } else {
                    $this->NossoNumero = $this->Formatar($this->Convenio, 7, "0") . $this->Formatar($this->NossoNumero, 10, "0");
                    $this->campo_livre = "000000" . $this->NossoNumero . $this->Formatar($this->Carteira, 2, "0");
                }
            } 
			else if (strlen($this->Convenio) == 8) {
                $this->NossoNumero = $this->Convenio . $this->Formatar($this->NossoNumero, 8, "0");
                $this->NossoNumero = $this->NossoNumero . $this->calcula_digito_barras($this->NossoNumero);

                if (($_SESSION['apelido_operadora'] == "unimedJatai") or ( $_SESSION['apelido_operadora'] == "unimedAraguaina"))
                    $this->campo_livre = "457415" . $this->NossoNumero . "21";
                else
                    $this->campo_livre = "000000" . $this->NossoNumero . $this->Carteira;
            }
            else if ($_SESSION['apelido_operadora'] == "planmed") {
                $this->NossoNumero = $this->Formatar($convenio . $this->Formatar($this->NossoNumero, 9, "0"), 17, "0");
                $this->campo_livre = $this->Formatar($this->CodigoCedente, 6, "0") . $this->Convenio . $this->Formatar($this->NossoNumero, 9, "0") . '21';
            } 
			else {
                $this->campo_livre = $this->Formatar($this->NossoNumero, 11, "0") . $this->Agencia . $this->Formatar($this->CodigoCedente, 8, "0") . $this->Carteira;
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 17, "0");
                $this->NossoNumero = $this->NossoNumero . "-" . $this->modulo11_nn_bb($this->NossoNumero);
            }

            $this->CorR = 0;
            $this->CorG = 64;
            $this->CorB = 128;
            if ($this->LayoutBoleto == ".b")
                $this->Imagem = '../comum/img/bancos/unicred.jpg';
            else
                $this->Imagem = '../comum/img/bancos/bb.jpg';
            $this->ImagemLargura = 38;
            $this->ImagemAltura = 8;
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_ABN)) {

            $this->Banco = "356-5"; // Abn
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->Conta = $this->Formatar($this->Conta, 7, "0");
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");

            $this->CodigoCedente = $this->Formatar($this->CodigoCedente, 7, "0");
            $digito = $this->digito_br($this->Formatar($this->NossoNumero, 7, "0") . $this->Agencia . $this->Conta);

            $this->campo_livre = $this->Agencia . $this->Conta . $digito . $this->Formatar($this->NossoNumero, 13, "0");

            if ($_SESSION['apelido_operadora'] == "portoAlegreClinicas" or
                    $_SESSION['apelido_operadora'] == "promedmg" or
                    $_SESSION['apelido_operadora'] == "universal")
                $this->NossoNumero = $this->NossoNumero;
            else
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 13, "0");

            $this->CampoAgencia = $this->Agencia . "/" . $this->CodigoCedente . "/" . $digito;

            $this->CorR = 0;
            $this->CorG = 128;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/abn.jpg';
            $this->ImagemLargura = 38;
            $this->ImagemAltura = 8;
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_CEF)) {

            $this->Banco = "104-0"; // Cef
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->Conta = $this->Formatar($this->Conta, 8, "0");
            $this->Convenio = $this->Formatar($this->Convenio, 8, "0");
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");

            if ($this->LayoutBoleto == "=") {

                if ($_SESSION['apelido_operadora'] == "oralclass")
                    $this->DescricaoLocalPagamento = "Pag�vel em qualquer ag�ncia banc�ria at� o vencimento, ap�s o vencimento exclusivamente na Caixa ou Casas Lot�ricas.";

                if ($_SESSION['apelido_operadora'] == "unimedJiParana")
                    $this->CampoAgencia = $this->Agencia . ".870." . $this->Conta . "-" . $this->DigitoConta;
                else
                    $this->CampoAgencia = $this->Agencia.".".$this->CodigoCedente."-".'3';

                $this->NossoNumero = "82" . $this->Formatar($this->NossoNumero, 8, "0");
                $digito = $this->Modulo11($this->NossoNumero);

                if ($_SESSION['apelido_operadora'] == "unimedJiParana")
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . "870" . $this->Convenio;
                else
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . "003" . $this->Convenio;

                $this->NossoNumero = $this->NossoNumero . "-" . $digito;
        
            }
            else if ($this->LayoutBoleto == "�") {
                $digito = $this->calcula_digito_barras($this->Formatar($this->NossoNumero, 17, "0"));
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 17, "0") . "-" . $digito;

                if (($_SESSION['apelido_operadora'] == "plansul") or
					( $_SESSION['apelido_operadora'] == "unihosp"))

                    $this->CampoAgencia = $this->Agencia . "/" . $this->Formatar($this->CodigoCedente, 8, "0");

                else if ($_SESSION['apelido_operadora'] == "lifeSul") {
                    $this->CampoAgencia = $this->Agencia . "/" . $this->Formatar($this->Conta, 6, "0") . "-" . $this->Formatar($this->DigitoConta, 1, "0");
                    $digito = $this->calcula_digito_barras("9" . $this->Formatar($this->NossoNumero, 17, "0"));
                    $this->NossoNumero = "9" . $this->Formatar($this->NossoNumero, 17, "0") . "-" . $digito;
                } 
				else {
                    $this->CampoAgencia = $this->Agencia . "/" . $this->Conta . "-" . $this->DigitoConta;
                }

                if ($_SESSION['apelido_operadora'] == "amc")
                    $this->NossoNumero = "904027278" + substr($this->NossoNumero, 9, 11);

                $this->campo_livre = "1" . $this->Formatar($this->CodigoCedente, 6, "0") . "9" . $this->Formatar($this->NossoNumero, 17, "0");
            }
            else if ($this->LayoutBoleto == "D") {
                if ($_SESSION['apelido_operadora'] == "global")
                    $this->CampoAgencia = $this->Agencia . ".870.00000083-3";
                else if ($_SESSION['apelido_operadora'] == "saoLucas")
                    $this->CampoAgencia = $this->Agencia . ".870.00000030-7";
                else if ($_SESSION['apelido_operadora'] == "plansul")
                    $this->CampoAgencia = $this->Agencia . ".870.00000011-0";
                else if ($_SESSION['apelido_operadora'] == "santaEdwirgesPlano")
                    $this->CampoAgencia = $this->Agencia . ".870.0183121-1";
                else if ($_SESSION['apelido_operadora'] == "rnMetropolitan")
                    $this->CampoAgencia = $this->Agencia . ".870.0000599-1";
                else if ($_SESSION['apelido_operadora'] == "servmed")
                    $this->CampoAgencia = $this->Agencia . ".870.00000233.6";
                else
                    $this->CampoAgencia = $this->Agencia . ".870." . $this->Conta;

                if ($this->Carteira == 12) {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 10, "0");
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . "870" . $this->Convenio;
                    $digito = $this->Modulo11($this->NossoNumero, 10);
                } 
				else if ($_SESSION['apelido_operadora'] == "servmed") {
                    $this->NumeroDocumento = '* ' . $this->CodigoTitular;
                    $this->campo_livre = "80" . $this->Formatar($this->NossoNumero, 8, "0") . $this->Agencia . "870" . $this->Formatar($this->CodigoCedente, 8, "0");
                    $this->NossoNumero = "8" . $this->Formatar($this->NossoNumero, 9, "0");
                    $digito = $this->Modulo11($this->NossoNumero, 10);
                    $this->EspecieDoc = "SR";
                } 
				else if ($_SESSION['apelido_operadora'] == "rnMetropolitan") {
                    $this->NumeroDocumento = '* ' . $this->CodigoTitular;
                    $this->campo_livre = "80" . $this->Formatar($this->NossoNumero, 8, "0") . $this->Agencia . "870" . $this->Formatar($this->CodigoCedente, 8, "0");
                    $this->NossoNumero = "8" . $this->Formatar($this->NossoNumero, 9, "0");
                    $digito = $this->Modulo11($this->NossoNumero, 10);
                    $this->EspecieDoc = "SR";
                } 
				else {
                    $this->NossoNumero = "8" . $this->Formatar($this->NossoNumero, 14, "0");
                    $this->campo_livre = $this->Formatar($this->CodigoCedente, 9, "0") . "87" . $this->Formatar($this->NossoNumero, 14, "0");
                    $digito = $this->Modulo11($this->NossoNumero, 15);
                }

                $this->NossoNumero = $this->NossoNumero . "-" . $digito;
            } else if ($this->LayoutBoleto == "?") {
                if ($_SESSION['apelido_operadora'] == "servmed")
                    $this->CampoAgencia = $this->Agencia . $this->DigitoAgencia;
                else
                    $this->CampoAgencia = $this->Agencia;

                $this->CampoAgencia = $this->CampoAgencia . "870.000" . $this->CodigoCedente . "-" .
                        $this->calcula_digito_barras($this->Agencia . $this->DigitoAgencia . "870000" . $this->CodigoCedente);

                $this->campo_livre = $this->Formatar($this->Convenio, 4, "0") .
                        $this->Formatar($this->NossoNumero, 6, "0") .
                        $this->Formatar($this->Agencia, 4, "0") . "87" .
                        $this->Formatar($this->CodigoCedente, 9, "0");

                $this->NossoNumero = $this->Convenio . $this->NossoNumero . "-" .
                        $this->calcula_digito_barras($this->Convenio . $this->NossoNumero);
            } else if ($this->LayoutBoleto == "==") {
                $this->CampoAgencia = $this->Agencia . ".870." . $this->Formatar($this->CodigoCedente, 8, "0") . "-" .
                        $this->calcula_digito_barras($this->Agencia . ".870." . $this->CodigoCedente);

                $this->campo_livre = $this->Formatar($this->CodigoCedente, 5, "0") .
                        $this->Agencia . "87" .
                        $this->Formatar($this->NossoNumero, 14, "0");

                $this->NossoNumero = "8" . $this->Formatar($this->NossoNumero, 14, "0") . "-" .
                        $this->calcula_digito_barras("8" . $this->Formatar($this->NossoNumero, 14, "0"));
            } else if ($this->LayoutBoleto == "��") {
                $this->CampoAgencia = $this->Agencia . "/" . $this->Formatar($this->CodigoCedente, 6, "0") . "-" .
                        $this->Modulo11($this->Formatar($this->CodigoCedente, 6, "0"), 6);
                $this->NossoNumero = $this->Carteira . $this->Formatar($this->NossoNumero, 15, "0") . "-" .
                        $this->Modulo11($this->Carteira . $this->Formatar($this->NossoNumero, 15, "0"), 17);
                $this->campo_livre = $this->Formatar($this->CodigoCedente, 6, "0") .
                        $this->Modulo11($this->Formatar($this->CodigoCedente, 6, "0"), 6) .
                        substr($this->NossoNumero, 2, 3) .
                        substr($this->NossoNumero, 0, 1) .
                        substr($this->NossoNumero, 5, 3) .
                        substr($this->NossoNumero, 1, 1) .
                        substr($this->NossoNumero, 8, 9);
                $this->campo_livre = $this->campo_livre . $this->Modulo11($this->campo_livre, 24);
            }

            $this->CorR = 0;
            $this->CorG = 64;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/cef.jpg';
            $this->ImagemLargura = 38;
            $this->ImagemAltura = 10;
        }
		else if (in_array($this->LayoutBoleto, $this->Layouts_Padrao)) {

            if ($this->Banco = '756') {
                $this->UsoBanco = "COBR. DIRETA";
                $this->Quantidade = "1";
                $this->Banco = "756-0"; // Sicoob
                $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
                $this->Carteira = $this->Formatar($this->Carteira, 1, "0");
                $this->Conta = $this->Formatar($this->Conta, 7, "0");
                $this->CodigoCedente = $this->Formatar($formata->somenteNumeros($this->CodigoCedente), 7, "0");

                if ($this->TipoCobranca == '2') {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 7, "0");
                    $digito = $this->DV_NossoNumero_Sicoob($this->NossoNumero, $this->Agencia, $this->Formatar($this->CodigoCedente, 10, "0"));
                    $this->campo_livre = $this->Carteira . $this->Agencia . "02" . $this->CodigoCedente . $this->NossoNumero . $digito . "001";
                    $this->NossoNumero = $this->NossoNumero . '-' . $digito;
                } 
				else {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0");
                    $this->campo_livre = $this->Carteira . $this->Agencia . "01" . $this->CodigoCedente . $this->NossoNumero . "001";
                }

                $this->CampoAgencia = $this->Agencia . "/" . $this->CodigoCedente;
                $this->ValorUnitario = "";

                $this->CorR = 0;
                $this->CorG = 128;
                $this->CorB = 128;
                $this->Imagem = '../comum/img/bancos/sicoob.jpg';
                $this->ImagemLargura = 38;
                $this->ImagemAltura = 8;
            }
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_SICOOB)) {
            $this->Banco = "756-0"; // Sicoob
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->Carteira = $this->Formatar($this->Carteira, 1, "0");
            $this->Conta = $this->Formatar($this->Conta, 7, "0");
            $this->CodigoCedente = $this->Formatar($formata->somenteNumeros($this->CodigoCedente), 7, "0");

            if ($this->TipoCobranca == '2') {
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 7, "0");
                $digito = $this->DV_NossoNumero_Sicoob($this->NossoNumero, $this->Agencia, $this->Formatar($this->CodigoCedente, 10, "0"));
                $this->campo_livre = $this->Carteira . $this->Agencia . "02" . $this->CodigoCedente . $this->NossoNumero . $digito . "001";
                $this->NossoNumero = $this->NossoNumero . '-' . $digito;
            } else {
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0");
                $this->campo_livre = $this->Carteira . $this->Agencia . "01" . $this->CodigoCedente . $this->NossoNumero . "001";
            }

            $this->CampoAgencia = $this->Agencia . "/" . $this->CodigoCedente;
            $this->ValorUnitario = $this->Valor;

            $this->CorR = 0;
            $this->CorG = 128;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/sicoob.jpg';
            $this->ImagemLargura = 38;
            $this->ImagemAltura = 8;
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_Unibanco)) {
            $this->Banco = "409-0"; // Unibanco
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");
            $this->Conta = $this->Formatar($this->Conta, 7, "0");
            $this->CodigoCedente = $this->Formatar($this->CodigoCedente, 7, "0");
            $digito = $this->digito_verificador($this->NossoNumero . $this->Agencia . $this->Conta);

            if (($_SESSION['apelido_operadora'] == "multiSaude") or
				( $_SESSION['apelido_operadora'] == "samedh") or
				( $_SESSION['apelido_operadora'] == "santaRita")) {
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 10, "0") .
                        $this->Modulo11($this->Formatar($this->NossoNumero, 10, "0"), 12);
            } else if ($this->CodigoCedente == "") {
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 10, "0") .
                        $this->Modulo11($this->Formatar($this->NossoNumero, 10, "0"), 12);
            } else {
                $this->NossoNumero = $this->Formatar($this->CodigoCedente, 4, "0") .
                        $this->Formatar($this->NossoNumero, 6, "0") .
                        $this->Modulo11($this->Formatar($this->CodigoCedente, 4, "0") . $this->Formatar($this->NossoNumero, 6, "0"), 12);
            }

            $this->campo_livre = "04" . substr($this->DataVencimento, 8, 2) . substr($this->DataVencimento, 3, 2) . substr($this->DataVencimento, 0, 2) .
                    $this->Agencia . $this->Formatar($this->DigitoAgencia, 1, "0") . $this->NossoNumero . $this->Modulo11("1" . $this->NossoNumero, 12);
            $this->NossoNumero = "1/" . $this->NossoNumero . "/" . $this->Modulo11("1" . $this->NossoNumero, 12);

            $this->CorR = 0;
            $this->CorG = 100;
            $this->CorB = 149;
            $this->Imagem = '../comum/img/bancos/unibanco.jpg';
            $this->ImagemLargura = 39;
            $this->ImagemAltura = 8;
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_Bradesco)) {
            $this->Banco = "237-2"; // Bradesco
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");
            $this->Conta = $this->Formatar($this->Conta, 7, "0");

            if ($this->LayoutBoleto == 'SB') {
                $this->NossoNumero = "09/" . substr($this->DataVencimento, 8, 2) . $this->Formatar($this->NossoNumero, 8, "0") .
                        $this->Modulo11Safra($this->Formatar($this->NossoNumero, 8, "0")) . "-" .
                        $this->dv_nossonumero_bradesco("09" . substr($this->DataVencimento, 8, 2) .
                                $this->Formatar($this->NossoNumero, 8, "0") . $this->Modulo11Safra($this->Formatar($this->NossoNumero, 8, "0")));

                $this->campo_livre = substr($this->AgenciaCorrespondente, 0, 4) . "09" . substr($this->DataVencimento, 8, 2) . $this->Formatar($this->NossoNumero, 8, "0") .
                        $this->Modulo11Safra($this->Formatar($this->NossoNumero, 8, "0")) . $this->Formatar(substr($this->CedenteCorrespondente, 0, 6), 7, "0") . "0";
                $this->CampoAgencia = substr($this->AgenciaCorrespondente, 0, 4) . "-" . substr($this->AgenciaCorrespondente, 4, 1) . "/" . $this->CedenteCorrespondente;

                if (!RetornaIdEmpresa($this->IdContrato) > 0)
                    $this->NumeroDocumento = CodigoUsuarioTitularContrato($this->IdContrato);
                else
                    $this->NumeroDocumento = CodigoTitulo($this->IdContrato);

            } else {
                if ($this->LayoutBoleto == 'BR')
                    $this->NossoNumero = $this->Radical . $this->Matricula . $this->Formatar($this->NossoNumero, 6, "0");
                else
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 11, "0");

                $digito = $this->dv_nossonumero_bradesco($this->Carteira . $this->NossoNumero);
                $this->campo_livre = $this->Agencia . $this->Carteira . $this->NossoNumero . $this->Conta . "0";
                $this->NossoNumero = $this->Carteira . "/" . $this->NossoNumero . "-" . $digito;
            }

            $this->DescricaoLocalPagamento = "Pagavel preferencialmente nas agencias Bradesco";
            $this->EspecieDoc = "DS";

            $this->CorR = 127;
            $this->CorG = 127;
            $this->CorB = 127;
            $this->Imagem = '../comum/img/bancos/bradesco.jpg';
            $this->ImagemLargura = 38;
            $this->ImagemAltura = 8;
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_Itau)) {
            $this->Banco = "341-7"; // Itau
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $carteira = $this->Formatar($this->Carteira, 3, "0");
            $this->Conta = $this->Formatar($this->Conta, 5, "0");
            $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0");
            // No layout itau informa que somente para algumas carteiras que o DAC n�o deve conter agencia e conta, mas simulando no site do itau n�o � o que ocorre
            // com isto alterei so para a AGEMED para que o DAC fique apenas com a carteira e documento
            if ((($_SESSION['apelido_operadora'] == "agemed") or ( $_SESSION['apelido_operadora'] == "saudemed")) and ( $carteira == 112)) {
                $digito1 = $this->digito_verificador($carteira . $this->NossoNumero);
            } 
			else {
                $digito1 = $this->digito_verificador($this->Agencia . $this->Conta . $carteira . $this->NossoNumero);
            }
            $digito2 = $this->digito_verificador($this->Agencia . $this->Conta);
            $this->campo_livre = $carteira . $this->NossoNumero . $digito1 . $this->Agencia . $this->Conta . $digito2 . "000";

            if ($_SESSION['apelido_operadora'] == "scs")
                $this->NossoNumero = $carteira . "/" . $this->NossoNumero;
            else
                $this->NossoNumero = $carteira . "/" . $this->NossoNumero . "-" . $digito1;

            if ($this->LayoutBoleto == "CB")
                $this->Carteira = substr($this->Carteira, 0, 2);

            if (($this->LayoutBoleto == "C") and ( $_SESSION['apelido_operadora'] == "saudemedicol"))
                $this->NumeroDocumento = CodigoUsuarioTitularContrato($this->IdContrato);

            $this->CorR = 0;
            $this->CorG = 64;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/itau.jpg';
            $this->ImagemLargura = 37;
            $this->ImagemAltura = 10;
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_Banestes)) {
            $this->Banco = "021-3"; // Banestes
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");
            $this->Conta = $this->Formatar($this->Conta, 11, "0");
            $this->TipoCobranca = $this->Formatar($this->TipoCobranca, 1, "0");
            $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0");
            $asbace = $this->NossoNumero . $this->Conta . $this->TipoCobranca . "021";
            $digito1 = $this->digito_verificador1($asbace);
            $digito2 = $this->digito_verificador2($asbace, $digito1);
            $this->campo_livre = $asbace . $digito1 . $digito2;
            $this->NossoNumero = $this->NossoNumero . "." . $this->dv_nosso_numero($this->NossoNumero);
            $this->NossoNumero = $this->NossoNumero . $this->dv_nosso_numero($this->NossoNumero);
            $this->CampoAgencia = $this->Agencia . "/" . $this->Conta;

            $this->Aceite = "";
            $this->Especie = "";
            $this->EspecieDoc = "";

            if ($this->NomeCorrespondente <> "") {
                $this->CpfCnpjCedente = $this->CpfCnpjEmpresa;
                $this->Cedente = $this->NomeCorrespondente;
            }

            $this->CorR = 0;
            $this->CorG = 64;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/banestes.jpg';
            $this->ImagemLargura = 39;
            $this->ImagemAltura = 10;
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_Mercantil)) {
            $this->Banco = "389-1"; // Mercantil      
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");
            $this->CampoAgencia = $this->Agencia . "/" . $this->CodigoCedente;

            if ($_SESSION['apelido_operadora'] == "vitallis") {
                $this->NossoNumero = $this->Agencia . $this->Formatar($this->NossoNumero, 10, "0");
            } else {
                $this->NossoNumero = $this->Agencia . "0703" . $this->Formatar($this->NossoNumero, 6, "0");
            }

            $digito1 = $this->modulo11_mercantil($this->NossoNumero);
            $this->campo_livre = $this->Agencia . substr($this->NossoNumero, 4, 10) . $digito1 . $this->Formatar($formata->somenteNumeros($this->CodigoCedente), 9, "0") . "2";
            $this->NossoNumero = substr($this->NossoNumero, 4, 10) . "-" . $this->modulo11_mercantil($this->NossoNumero);

            $this->Aceite = "";
            $this->Especie = "";
            $this->EspecieDoc = "";

            $this->CorR = 0;
            $this->CorG = 64;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/mercantil.jpg';
            $this->ImagemLargura = 26;
            $this->ImagemAltura = 9;
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_HSBC)) {

            $this->Banco = "399-0"; // HSBC
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Carteira = $this->Formatar($this->Carteira, 2, "0");
            $this->Conta = $this->Formatar($this->Conta, 6, "0") . $this->DigitoConta;
            $this->TipoCobranca = $this->Formatar($this->TipoCobranca, 1, "0");

            if (( $this->LayoutBoleto == "3" ) or ( $this->LayoutBoleto == ".3" )) {
                //$this->NossoNumero  = $this->Convenio.$this->Formatar($this->NossoNumero,5,"0");
                $this->campo_livre = $this->Formatar($this->CodigoCedente, 7, "0") . $this->Formatar($this->NossoNumero, 13, "0") . $this->DataJuliana . '2';
                $dv1 = $this->DV1_HSBC($this->NossoNumero);
                $dv2 = $this->DV2_HSBC($this->NossoNumero . $dv1, $this->CodigoCedente, $this->DataVencimento);
                $this->NossoNumero = $this->Formatar($this->NossoNumero . $dv1 . '4' . $dv2, 16, "0");
                $this->CampoAgencia = $this->CodigoCedente;
                $this->EspecieDoc = "";
                $this->Carteira = "CNR";
            } else {

                if ($_SESSION['apelido_operadora'] == 'sampes') {
                    $this->NossoNumero = $this->Convenio . $this->Formatar($this->NossoNumero, 5, "0");
                    $this->NossoNumero = $this->NossoNumero . $this->dv_hsbc_diretiva($this->NossoNumero);
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . $this->Conta . "001";
                } else if ($this->EspecieDocumento == '98') {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 10, "0");
                    $this->NossoNumero = $this->NossoNumero . $this->dv_hsbc_diretiva($this->NossoNumero);
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . $this->Conta . "001";
                } else if ($_SESSION['apelido_operadora'] == 'viva') {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 10, "0");
                    $this->NossoNumero = $this->NossoNumero . $this->dv_hsbc_diretiva($this->NossoNumero);
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . $this->Conta . "001";
                } else {
                    $this->NossoNumero = $this->Formatar($this->NossoNumero, 11, "0");
                    $this->campo_livre = $this->NossoNumero . $this->Agencia . $this->Conta . "001";
                    $this->NossoNumero = substr($this->NossoNumero, 0, 2) . "." .
                            substr($this->NossoNumero, 2, 3) . "." .
                            substr($this->NossoNumero, 5, 3) . "." .
                            substr($this->NossoNumero, 8, 2) . "." .
                            substr($this->NossoNumero, 10, 1);
                }

                $this->CampoAgencia = $this->Agencia . "/" . $this->Conta;
                $this->EspecieDoc = "PD";
                $this->Carteira = "CSB";
            }

            $this->DescricaoLocalPagamento = "PAGAR PREFERENCIALMENTE EM AG�NCIA HSBC";

            $this->CorR = 0;
            $this->CorG = 64;
            $this->CorB = 128;
            $this->Imagem = '../comum/img/bancos/hsbc.jpg';
            $this->ImagemLargura = 32;
            $this->ImagemAltura = 8;
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_Santander)) {
            $this->Banco = "033-7";
            $this->Agencia = $this->Formatar($this->Agencia, 4, "0");
            $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;
            $this->Carteira = $this->Formatar($this->Carteira, 3, "0");

            if ($_SESSION['apelido_operadora'] == 'clinipam')
                $this->DescricaoLocalPagamento = "Pagar em qualquer banco at� o vencimento.";
            else
                $this->DescricaoLocalPagamento = "Pagar em qualquer banco at� o vencimento, ap�s vencto pagar somente no banco Santander.";

            if (($_SESSION['apelido_operadora'] == 'clinipam') or
                ( $_SESSION['apelido_operadora'] == 'vitallis')) {
                $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . substr($this->CodigoCedente, 0, 6) . '-' . substr($this->CodigoCedente, 6, 1);
                $digito1 = $this->Modulo11($this->Formatar($this->NossoNumero, 12, "0"));
                $this->campo_livre = '9' . $this->Formatar($this->CodigoCedente, 7, "0") . $this->Formatar($this->NossoNumero, 12, "0") . $digito1 . '0101';
                $this->NossoNumero = $this->NossoNumero . "-" . $digito1;
            } else if ($_SESSION['apelido_operadora'] == 'promedmg') {
                $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . substr($this->CodigoCedente, 0, 6) . '-' . substr($this->CodigoCedente, 6, 1);
                $digito1 = $this->Modulo11($this->Formatar($this->NossoNumero, 12, "0"));
                $this->campo_livre = '9' . $this->Formatar($this->CodigoCedente, 7, "0") . $this->Formatar($this->NossoNumero, 12, "0") . $digito1 . '0101';
                $this->NossoNumero = $this->NossoNumero . "-" . $digito1;
            } else if (( $this->LayoutBoleto == 'DD' ) and ( $_SESSION['apelido_operadora'] == 'unimedFranca')) {
                $this->DescricaoLocalPagamento = "Pagar preferencialmente no Grupo Santander - GC";
                $this->Banco = "033-9";
                $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta;
                $digito1 = $this->Modulo11($this->Formatar($this->NossoNumero, 12, "0"));
                $this->campo_livre = '9' . $this->Formatar(substr($this->CodigoCedente, 5, 15), 7, "0") . $this->Formatar($this->NossoNumero, 12, "0") . $digito1 . '0101';
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 12, "0") . " " . $digito1;
                $this->NumeroDocumento = '* ' . $this->CodigoTitular;
            } else if ($this->LayoutBoleto == 'DD') {
                $this->DescricaoLocalPagamento = "Pagar preferencialmente no Grupo Santander - GC";
                $this->Banco = "033-9";
                $this->CampoAgencia = $this->Agencia . "-" . $this->DigitoAgencia . "/" . $this->Conta;
                $digito1 = $this->Modulo11($this->Formatar($this->NossoNumero, 12, "0"));
                $this->campo_livre = '9' . $this->Formatar($this->CodigoCedente, 7, "0") . $this->Formatar($this->NossoNumero, 12, "0") . $digito1 . '0102';
                $this->NossoNumero = $this->Formatar($this->NossoNumero, 12, "0") . " " . $digito1;
                $this->NumeroDocumento = '* ' . $this->CodigoTitular;
            } else {
                $this->CampoAgencia = $this->Agencia . "/" . $this->Conta . $this->DigitoConta;
                $digito1 = $this->Modulo11($this->Formatar($this->NossoNumero, 12, "0"));
                $this->campo_livre = '9' . $this->Formatar($this->Conta . $this->DigitoConta, 7, "0") . $this->Formatar($this->NossoNumero, 12, "0") . $digito1 . '0102';
                $this->NossoNumero = $this->NossoNumero . "-" . $digito1;
            }

            $this->CorR = 127;
            $this->CorG = 127;
            $this->CorB = 127;
            $this->Imagem = '../comum/img/bancos/santander.jpg';
            $this->ImagemLargura = 40;
            $this->ImagemAltura = 9;
        } 
		else if (in_array($this->LayoutBoleto, $this->Layouts_Banese)) {
            $this->Banco = "047-7";
            $this->CampoAgencia = $this->Formatar($this->Agencia, 4, "0") . "-" . $this->DigitoAgencia . "/" . $this->Formatar($this->TipoCobranca, 2, "0") . "." . $this->Conta . "-" . $this->DigitoConta;

            $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0") . "-" .
                    $this->Modulo11($this->Formatar($this->Agencia, 3, "0") . $this->Formatar($this->NossoNumero, 8, "0"), 11);

            $asbace = $this->Agencia .
                    $this->Formatar($this->TipoCobranca, 2, "0") .
                    $this->Formatar($this->Conta, 6, "0") .
                    $this->DigitoConta .
                    $formata->somentNumeros($this->NossoNumero) .
                    "047";

            $digito1 = $this->digito_asbace1($asbace);
            $digito2 = $this->digito_asbace2($asbace, $digito1);

            $this->campo_livre = $asbace . $digito1 . $digito2;

            $this->CorR = 127;
            $this->CorG = 127;
            $this->CorB = 127;
            $this->Imagem = '../comum/img/bancos/banese.jpg';
            $this->ImagemLargura = 40;
            $this->ImagemAltura = 9;
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_Sicredi_HGU)){
            $this->Banco = $this->Formatar ($this->Banco, 4, '0');
            $this->CampoAgencia = $this->Formatar($this->Agencia, 4, "0") . "." . $this->Formatar($this->Convenio, 2, "0") . "/" . $this->Formatar($this->CodigoCedente, 5, "0");

            if ($_SESSION['apelido_operadora'] == "UnimedLestePaulista") {
                $this->Carteira = "00";
                $this->EspecieDoc = "DS";
            }

            if ($this->Convenio > 0)
                $agenciaSicredi = $this->Formatar($this->Agencia, 4, "0") . $this->Formatar($this->Convenio, 2, "0");
            else
                $agenciaSicredi = $this->Formatar($this->Agencia, 6, "0");

            $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0");
            $this->NossoNumero = substr($this->NossoNumero, 0, 2) . "/" .
                    substr($this->NossoNumero, 2, 6) . "-" .
                    $this->modulo11SicrediInvertido($agenciaSicredi .
                            $this->Formatar($this->CodigoCedente, 5, "0") .
                            $this->Formatar($this->NossoNumero, 8, "0"));
            
            if ($this->TipoCobranca == null)
                $this->TipoCobranca = '3';

            $this->campo_livre = substr($this->TipoCobranca, 0, 1) .
                    '1' .
                    $this->Formatar($formata->somenteNumeros($this->NossoNumero), 9, "0") .
                    $agenciaSicredi .
                    $this->Formatar($this->CodigoCedente, 5, "0") .
                    '1' .
                    '0';

            $this->campo_livre = $this->campo_livre . $this->modulo11SicrediInvertido($this->campo_livre);

            $this->CorR = 127;
            $this->CorG = 127;
            $this->CorB = 127;
            $this->Imagem = '../comum/img/bancos/sicredi.jpg';
            $this->ImagemLargura = 40;
            $this->ImagemAltura = 9; 
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_Sicredi)) {

            $this->Banco = "748-X";
            $this->CampoAgencia = $this->Formatar($this->Agencia, 4, "0") . "." . $this->Formatar($this->Convenio, 2, "0") . "/" . $this->Formatar($this->CodigoCedente, 5, "0");

            if ($_SESSION['apelido_operadora'] == "UnimedLestePaulista") {
                $this->Carteira = "00";
                $this->EspecieDoc = "DS";
            }

            if ($this->Convenio > 0)
                $agenciaSicredi = $this->Formatar($this->Agencia, 4, "0") . $this->Formatar($this->Convenio, 2, "0");
            else
                $agenciaSicredi = $this->Formatar($this->Agencia, 6, "0");

            $this->NossoNumero = $this->Formatar($this->NossoNumero, 8, "0");
            $this->NossoNumero = substr($this->NossoNumero, 0, 2) . "/" .
                    substr($this->NossoNumero, 2, 6) . "-" .
                    $this->modulo11SicrediInvertido($agenciaSicredi .
                            $this->Formatar($this->CodigoCedente, 5, "0") .
                            $this->Formatar($this->NossoNumero, 8, "0"));
            //$this->campo_livre = '3'.
            //                     '1'.
            //                     $this->Formatar($formata->somenteNumeros($this->NossoNumero),9,"0").
            //                     $agenciaSicredi.
            //                     $this->Formatar($this->CodigoCedente,5,"0").
            //                     $this->Formatar($this->Convenio,2,"0");

            if ($this->TipoCobranca == null)
                $this->TipoCobranca = '3';

            $this->campo_livre = substr($this->TipoCobranca, 0, 1) .
                    '1' .
                    $this->Formatar($formata->somenteNumeros($this->NossoNumero), 9, "0") .
                    $agenciaSicredi .
                    $this->Formatar($this->CodigoCedente, 5, "0") .
                    '1' .
                    '0';

            $this->campo_livre = $this->campo_livre . $this->modulo11SicrediInvertido($this->campo_livre);

            $this->CorR = 127;
            $this->CorG = 127;
            $this->CorB = 127;
            $this->Imagem = '../comum/img/bancos/sicredi.jpg';
            $this->ImagemLargura = 40;
            $this->ImagemAltura = 9;
        }
        else if (in_array($this->LayoutBoleto, $this->Layouts_Basa)) {
            $this->Banco = "003-5";
            $this->Aceite = "N";
            $this->Especie = "R$";
            $this->EspecieDoc = "";
            $this->Carteira = "CNR";
            $this->UsoBanco = "Uso do Banco";
            $this->ValorUnitario = "";

            $this->CampoAgencia = $this->Formatar($this->Agencia, 4, "0") . "-" . $this->DigitoAgencia . "/" . $this->Conta . "-" . $this->DigitoConta;

            $this->NossoNumero = $this->Formatar($this->NossoNumero, 16, "0");

            $this->campo_livre = $this->Formatar($this->Agencia . $this->DigitoAgencia, 4, "0") .
                    $this->Formatar($this->Convenio, 4, "0") .
                    $this->NossoNumero .
                    "8";

            $this->DescricaoLocalPagamento = "PAGUE PREFERENCIALMENTE NO BANCO DA AMAZ�NIA S/A";

            $this->CorR = 127;
            $this->CorG = 127;
            $this->CorB = 127;
            $this->Imagem = '../comum/img/bancos/amazonia.jpg';
            $this->ImagemLargura = 40;
            $this->ImagemAltura = 9;
        }

        $moeda = "9";
        $barras = substr($this->Banco, 0, 3) . $moeda . $this->FatorVencimento . $this->Formatar($this->Valor, 10, "0", "v") . $this->campo_livre;
        $digito_barras = $this->calcula_digito_barras($barras);
        $barras = substr($this->Banco, 0, 3) . $moeda . $digito_barras . $this->FatorVencimento . $this->Formatar($this->Valor, 10, "0", "v") . $this->campo_livre;

        if (($_SESSION['apelido_operadora'] == "asfal") and ( ($this->LayoutBoleto) == "F" ) or ( $this->LayoutBoleto) == "[]") {
            $this->CodigoBarras = "";
            $this->DebitoConta = "ESTA FATURA SERA DEBITADA EM SUA CONTA CORRENTE\nBANC�RIA NA DATA DO VENCIMENTO\nNO VALOR DE R$ " . $this->Valor;
            $this->LinhaDigitavel = "";
        } 
		else {
            $this->CodigoBarras = $barras;
            $this->DebitoConta = "ESTA FATURA SERA DEBITADA EM SUA CONTA CORRENTE\nBANC�RIA NA DATA DO VENCIMENTO\nNO VALOR DE R$ " . $this->Valor;
            $this->LinhaDigitavel = $this->monta_linha_digitavel($barras);
        }

    }

    private function DV1_HSBC($numero) {
        $soma = 0;
        $fator = 9;

        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $soma += ($numero[$i] * $fator);
            $fator--;
            if ($fator == 1)
                $fator = 9;
        }

        $resto = ($soma % 11);
        if ($resto == 10)
            $resto = 0;

        return $resto;
    }

    private function DV2_HSBC($numero, $cedente, $vencimento) {

        $formata = new Formata();

        $numero .= '4';

        if (strlen($cedente) == 7)
            $cedente = $cedente;

        $venc = $formata->somenteNumeros($vencimento);
        $venc = substr($venc, 0, 4) . substr($venc, 6, 2);

        $soma = $numero + $cedente + $venc;
        $soma = sprintf("%01.0f", $soma);
        return $this->DV1_HSBC($soma);
    }

    private function Modulo11($numero, $tamanho = 12) {
        $numero = $this->Formatar($numero, $tamanho, "0");
        $soma = 0;
        $fator = 1;

        for ($i = $tamanho - 1; $i >= 0; $i--) {
            $fator++;
            $soma += ($numero[$i] * $fator);
            if ($fator == 9)
                $fator = 1;
        }

        $soma *= 10;
        $resto = ($soma % 11);
        if (($resto == 0) || ($resto == 10))
            $resto = 0;

        return $resto;
    }

    private function Modulo11Safra($numero, $tamanho = 8) {
        $numero = $this->Formatar($numero, $tamanho, "0");
        $soma = 0;
        $fator = 9;

        for ($i = 0; $i < $tamanho; $i++) {
            $soma += ($numero[$i] * $fator);
            $fator--;
            if ($fator == 0)
                $fator = 9;
        }
        $resto = ($soma % 11);

        if ($resto == 0)
            $resto = 1;
        else if ($resto == 1)
            $resto = 0;
        else {
            $resto = 11 - $resto;

            if ($resto == 10)
                $resto = 0;
            else if ($resto == 11)
                $resto = 1;
        }

        return $resto;
    }

    private function dv_nosso_numero($numero) {
        $formata = new Formata();

        $fator = 1;
        $soma = 0;
        $numero = $formata->somenteNumeros($numero);
        $i = strlen($numero) - 1;

        while ($i >= 0) {
            $fator = $fator + 1;

            $m = $fator * (substr($numero, $i, 1) );

            $soma = $soma + $m;
            $i = $i - 1;
        }

        $resto = ($soma % 11);

        if ($resto == 0 or $resto == 1)
            return 0;
        else
            return 11 - $resto;
    }

    private function dv_hsbc_diretiva($numero) {
        $formata = new Formata();

        $soma = 0;
        $numero = $formata->somenteNumeros($numero);

        $soma = ((substr($numero, 0, 1)) * 5 + (substr($numero, 1, 1)) * 4 +
                (substr($numero, 2, 1)) * 3 + (substr($numero, 3, 1)) * 2 +
                (substr($numero, 4, 1)) * 7 + (substr($numero, 5, 1)) * 6 +
                (substr($numero, 6, 1)) * 5 + (substr($numero, 7, 1)) * 4 +
                (substr($numero, 8, 1)) * 3 + (substr($numero, 9, 1)) * 2);

        $resto = ($soma % 11);

        if ($resto == 0 or $resto == 1)
            return 0;
        else
            return 11 - $resto;
    }

    private function digito_verificador1($numero) {
        $fator = 1;
        $soma = 0;
        $i = strlen($numero) - 1;

        while ($i >= 0) {
            if ($fator == 1)
                $fator = 2;
            else
                $fator = 1;

            $m = $fator * (substr($numero, $i, 1) );

            if ($m > 9)
                $m = $m - 9;

            $soma = $soma + $m;
            $i = $i - 1;
        }

        $resto = ($soma % 10);

        if ($resto == 0)
            return 0;
        else
            return 10 - $resto;
    }

    private function dv_nossonumero_bradesco($numero) {
        $fator = 1;
        $soma = 0;
        $i = 12;

        while ($i >= 0) {
            $fator = $fator + 1;
            if ($fator == 8)
                $fator = 2;
            $m = $fator * (substr($numero, $i, 1) );
            $soma = $soma + $m;
            $i = $i - 1;
        }

        $resto = 11 - ($soma % 11);
        if ($resto == 10)
            return 'P';
        elseif ($resto == 11)
            return '0';
        else
            return $resto;
    }

    private function digito_verificador2($cl, &$digito1) {
        $numero = $cl . $digito1;
        $fator = 1;
        $soma = 0;
        $i = strlen($numero) - 1;

        while ($i >= 0) {
            if ($fator == 7)
                $fator = 2;
            else
                $fator = $fator + 1;

            $m = $fator * (substr($numero, $i, 1) );

            $soma = $soma + $m;
            $i = $i - 1;
        }

        $resto = ($soma % 11);

        if ($resto == 0)
            return 0;
        elseif ($resto == 1) {
            $digito1 = $digito1 + 1;

            if ($digito1 == 10)
                $digito1 = 0;

            $this->digito_verificador2($cl, $digito1);

        } else
            return 11 - $resto;
    }

    private function modulo11_nn_bb($numero) {
        $soma = 0;
        $fator = 10;

        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $fator--;
            if ($fator == 1)
                $fator = 9;
            $soma += $numero[$i] * $fator;
        }

        $resto = $soma % 11;
        if ($resto < 10)
            return $resto;
        else if ($resto == 10)
            return "X";
    }

    private function modulo11_nn_unicred($numero) {
        $soma = 0;
        $fator = 10;

        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $fator--;
            if ($fator == 1)
                $fator = 9;
            $soma += $numero[$i] * $fator;
        }

        $resto = $soma % 11;
        if ($resto < 10)
            return $resto;
        else if ($resto == 10)
            return "0";
    }

    private function modulo11_mercantil($numero) {
        $soma = 0;
        $fator = 7;

        for ($i = 0; $i < strlen($numero); $i++) {

            $soma += $numero[$i] * $fator;

            $fator--;
            if ($fator == 1)
                $fator = 9;
        }

        $resto = $soma % 11;
        $resto = 11 - $resto;
        if (($resto == 10) || ($resto == 11))
            return 0;
        else
            return $resto;
    }

    private function digito_br($numero) {
        $fator = 1;
        $soma = 0;

        for ($i = 17; $i >= 0; $i--) {
            if ($fator == 1)
                $fator = 2;
            else
                $fator = 1;

            $m = $fator * $numero[$i];

            if ($m > 9)
                $m = substr($m, 0, 1) + substr($m, 1, 1);
            $soma = $soma + $m;
        }

        $resto = 10 - ($soma % 10);
        if ($resto == 10)
            return 0;
        else
            return $resto;
    }

    private function calcula_digito_barras($numero) {
        $soma = 0;
        $fator = 1;

        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $fator++;
            if ($fator == 10)
                $fator = 2;
            $soma += $numero[$i] * $fator;
        }

        $resto = $soma % 11;
        $resto = 11 - $resto;
        if (($resto == 10) || ($resto == 11))
            return 1;
        else
            return $resto;
    }

    function modulo11SicrediInvertido($numero, $tipo = 1) {
        $soma = 0;
        $fator = 2;

        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $soma += substr($numero, $i, 1) * $fator;

            $fator++;

            if ($fator == 10)
                $fator = 2;
        }

        $truncado = explode(".", $soma / 11);
        $resto = $truncado[0];
        $resto *= 11;

        $resto = $soma - $resto;

        if (($resto == 0) or ( $resto == 1))
            $dv = 0;
        else
            $dv = 11 - $resto;

        if (($tipo == 2) and ( $dv == 0))
            $dv = 1;

        return $dv;
    }

    private function digito_verificador($numero) {
        $fator = 2;
        $soma = 0;

        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $m = substr($numero, $i, 1) * $fator;

            if ($m > 9)
                $m = (substr($m, 0, 1) + substr($m, 1, 1));

            $soma = $soma + $m;

            if ($fator == 2)
                $fator = 1;
            else
                $fator = 2;
        }

        $soma = 10 - ($soma % 10);

        if ($soma == 10)
            return 0;
        else
            return $soma;
    }

    private function digito_asbace1($numero) {
        $fator = 1;
        $soma = 0;
        $i = strlen($numero) - 1;

        while ($i >= 0) {
            if ($fator == 1)
                $fator = 2;
            else
                $fator = 1;

            $m = $fator * (substr($numero, $i, 1) );

            if ($m > 9)
                $m = $m - 9;

            $soma = $soma + $m;
            $i = $i - 1;
        }

        $resto = ($soma % 10);

        if ($resto == 0)
            return 0;
        else
            return 10 - $resto;
    }

    private function digito_asbace2($cl, &$digito1) {
        $numero = $cl . $digito1;
        $fator = 1;
        $soma = 0;
        $i = strlen($numero) - 1;

        while ($i >= 0) {
            if ($fator == 7)
                $fator = 2;
            else
                $fator = $fator + 1;

            $m = $fator * (substr($numero, $i, 1) );

            $soma = $soma + $m;
            $i = $i - 1;
        }

        $resto = ($soma % 11);

        if ($resto == 0)
            return 0;
        elseif ($resto == 1) {
            $digito1 = $digito1 + 1;

            if ($digito1 == 10)
                $digito1 = 0;

            return $this->digito_asbace2($cl, $digito1);

        } else
            return 11 - $resto;
    }

    private function monta_linha_digitavel($barras) {

        $linha1 = substr($barras, 0, 3) .
                "9" .
                substr($barras, 19, 5) .
                "X" .
                substr($barras, 24, 10) .
                "X" .
                substr($barras, 34, 10) .
                "X" .
                substr($barras, 4, 1) .
                substr($barras, 5, 4) .
                substr($barras, 9, 10);

        $linha2 = substr($linha1, 0, 9) .
                $this->digito_verificador(substr($linha1, 0, 9)) .
                substr($linha1, 10, 10) .
                $this->digito_verificador(substr($linha1, 10, 10)) .
                substr($linha1, 21, 10) .
                $this->digito_verificador(substr($linha1, 21, 10)) .
                substr($linha1, 32, 15);

        return substr($linha2, 0, 5) . "." .
                substr($linha2, 5, 5) . " " .
                substr($linha2, 10, 05) . "." .
                substr($linha2, 15, 06) . " " .
                substr($linha2, 21, 05) . "." .
                substr($linha2, 26, 06) . " " .
                substr($linha2, 32, 01) . " " .
                substr($linha2, 33, 14);
    }

    private function Formatar($variavel, $tamanho, $caracter, $tipo = "e") {
        $formata = new Formata();
        // tipo = v - valores

        if ($tipo == "v") {
            //      $variavel = str_replace(".","",$variavel);
            //      $variavel = str_replace(",",".",$variavel);
            //      $variavel = number_format($variavel,2,"","");
            //      $variavel = str_replace(".","",$variavel);

            $variavel = $formata->somenteNumeros($formata->formataNumero($variavel, 2));

            $tipo = "e";
        }

        while (strlen($variavel) < $tamanho) {
            if ($tipo == "e")
                $variavel = $caracter . $variavel;
            else
                $variavel = $variavel . $caracter;
        }

        if ($tipo == "e")
            $variavel = substr($variavel, 0, $tamanho);
        else
            $variavel = substr($variavel, (strlen($variavel) - $tamanho) - 1, $tamanho);

        return $variavel;
    }

    public function InstrucaoBancaria() {
        $bd = new Oracle();
        $formata = new Formata();

        $resultado = "";
        $contrato = 0;

        $temp = "SELECT CINSTPAGA,CBOLETITU,NNUMEEMPR,HSSPAGA.NNUMETITU,HSSPAGA.NNUMEUSUA,NPARTTITU,CCODIPLAN,NRGMSPLAN, 
                    CINPJCONGE,CINPFCONGE,CIPFFCONGE,CBOLEPLAN,HSSPAGA.CCOMPPAGA,
                    NVENCPAGA VALOR, NJUROPAGA JUROS, NDESCPAGA DESCONTOS
               FROM HSSPAGA,HSSTITU,HSSPLAN,HSSCONGE 
              WHERE NNUMEPAGA = :id_pagamento 
                AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU 
                AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+) ";
        $sql2 = new Query();
        $sql2->addParam(":id_pagamento", $this->IdPagamento);
        $sql2->executeQuery($temp);

        $inst = "SELECT CINSTLOPG,CINPJLOPG,CIPFFLOPG,NNUMELOPG FROM HSSLOPG " .
                " WHERE NNUMELOPG = :local ";
        $sql3 = new Query();
        $sql3->addParam(":local", $this->LocalPagamento);
        $sql3->executeQuery($inst);

        if ($this->IdJpaga > 0) {
            $sql = new Query();
            $msg = "SELECT CINSTJPAGA FROM HSSJPAGA WHERE NNUMEJPAGA = :id";
            $sql->addParam(":id", $this->IdJpaga);
            $sql->executeQuery($msg);

            $resultado = $sql->result("CINSTJPAGA") . "\n";

            if ($_SESSION['apelido_operadora'] == 'sampmg') {
                if ($tipo_contrato == "PF")
                    $resultado .= $sql3->result("CINSTLOPG");
                elseif ($sql2->result("CBOLETITU") == "")
                    $resultado .= $sql3->result("CINPJLOPG");
                else
                    $resultado .= $sql3->result("CIPFFLOPG");
            }
        } else {
            if ($_SESSION['apelido_operadora'] == 'genoveva')
                $resultado .= "Compet�ncia: " . $this->Competencia . "\n";

            if ((str_replace(',', '.', $this->Valor) - str_replace(',', '.', $this->ValorOriginal)) > 0)
                $resultado .= "2 via de boleto pela web acrescido de juros e multa. Valor original da cobran�a: " . $formata->formataNumero($this->ValorOriginal) . "\n";

            if ($this->IdContrato > 0)
                $resultado .= $this->descontoProgramado();

            $sql = new Query();
            $msg = "SELECT CREA1CONF,CREA2CONF,CREA3CONF,CREA4CONF,CREA5CONF,CREA6CONF,CMENSCONF FROM HSSCONF";
            $sql->executeQuery($msg);

            if ($_SESSION['apelido_operadora'] <> 'universal')
            //$resultado .= $sql->result("CMENSCONF"); removido cmensconf, caso seja necess�rio inclua especifico para o cliente

                $resultado .= $sql2->result("CINSTPAGA");

            if ($sql2->result("NNUMEEMPR") == 0)
                $tipo_contrato = "PF";
            else {
                if ($sql2->result("NPARTTITU") == 1)
                    $tipo_contrato = "PJparticipa";
                else
                    $tipo_contrato = "PJnaoparticipa";
            }

            if (trim($resultado) == '') {
                if ($tipo_contrato == "PF")
                    $resultado = $sql2->result("CINPFCONGE");
                elseif ($tipo_contrato == "PJparticipa")
                    $resultado = $sql2->result("CINPFCONGE");
                elseif ($tipo_contrato == "PJnaoparticipa")
                    $resultado = $sql2->result("CIPFFCONGE");
            }

            if (trim($resultado) == '') {
                if ($tipo_contrato == "PF")
                    $resultado .= $sql3->result("CINSTLOPG");
                elseif ($sql2->result("CBOLETITU") == "")
                    $resultado .= $sql3->result("CINPJLOPG");
                else
                    $resultado .= $sql3->result("CIPFFLOPG");
            }

            if ($sql->result("CREA1CONF") <> "" or $sql->result("CREA2CONF") <> "" or
                    $sql->result("CREA3CONF") <> "" or $sql->result("CREA4CONF") <> "" or
                    $sql->result("CREA5CONF") <> "" or $sql->result("CREA6CONF") <> "") {
                // Tem msg especifica de reajuste de mensalidade

                $tabela = "SELECT CLEI_PLAN FROM HSSUSUA,HSSFETA,HSSMANU,HSSPLAN ";

                if ($sql2->result("NNUMEEMPR") > 0 and $sql2->result("CBOLETITU") == 'S') {
                    $Usuario = $sql2->result("NNUMEUSUA");
                    $tabela = $tabela . " WHERE HSSUSUA.NTITUUSUA = :usuario ";
                } 
				else {
                    $contrato = $sql2->result("NNUMETITU");
                    $tabela = $tabela . " WHERE HSSUSUA.NNUMETITU = :contrato ";
                }

                $tabela = $tabela . "   AND CSITUUSUA = 'A' " .
                        "   AND HSSUSUA.NNUMEPLAN = HSSFETA.NNUMEPLAN " .
                        "   AND HSSUSUA.NNUMETPLA = HSSFETA.NNUMETPLA " .
                        "   AND HSSFETA.NNUMEFETA = HSSMANU.NNUMEFETA " .
                        "   AND DVIGEMANU = (SELECT MAX(MANU2.DVIGEMANU) FROM HSSMANU MANU2 " .
                        "                     WHERE MANU2.NNUMEFETA = HSSFETA.NNUMEFETA " .
                        "                       AND TO_DATE(TO_CHAR(MANU2.DVIGEMANU,'DD/MM/YYYY'),'DD/MM/YYYY') <= TO_DATE(:vencimento,'DD/MM/YYYY')) " .
                        "   AND TO_DATE(TO_CHAR(DVIGEMANU,'DD/MM/YYYY'),'DD/MM/YYYY') <= TO_DATE(:vencimento,'DD/MM/YYYY') " .
                        "   AND TO_DATE(TO_CHAR(DVIGEMANU,'DD/MM/YYYY'),'DD/MM/YYYY') >= ADD_MONTHS(TO_DATE(:vencimento,'DD/MM/YYYY'),-1) " .
                        "   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";

                $sql4 = new Query();

                if ($sql2->result("NNUMEEMPR") > 0 and $sql2->result("CBOLETITU") == 'S')
                    $sql4->addParam(":usuario", $usuario);
                else
                    $sql4->addParam(":contrato", $contrato);

                $sql4->addParam(":vencimento", $this->DataVencimento);
                $sql4->executeQuery($tabela);

                if ($sql4->count() > 0) {
                    if ($resultado <> '')
                        $resultado = $resultado . " ";

                    if ($tipo_contrato == "PF" and $sql4->result("CLEI_PLAN") == "1") // PF antes da lei
                        $resultado = $resultado . $sql->result("CREA1CONF");
                    elseif ($tipo_contrato == "PF" and $sql4->result("CLEI_PLAN") == "2") // PF depois da lei
                        $resultado = $resultado . $sql->result("CREA2CONF");
                    elseif ($tipo_contrato == "PJparticipa" and $sql4->result("CLEI_PLAN") == "1") // PJ antes da lei
                        $resultado = $resultado . $sql->result("CREA3CONF");
                    elseif ($tipo_contrato == "PJparticipa" and $sql4->result("CLEI_PLAN") == "2") // PJ depois da lei
                        $resultado = $resultado . $sql->result("CREA4CONF");
                    elseif ($tipo_contrato == "PJnaoparticipa" and $sql4->result("CLEI_PLAN") == "1") // PFF antes da lei
                        $resultado = $resultado . $sql->result("CREA5CONF");
                    elseif ($tipo_contrato == "PJnaoparticipa" and $sql4->result("CLEI_PLAN") == "2") // PFF depois da lei
                        $resultado = $resultado . $sql->result("CREA6CONF");
                }
            }

            if ($_SESSION['apelido_operadora'] == 'clinipam')
                $resultado .= 'RECEBER AT� 60 DIAS AP�S VENCIMENTO';

            if ($_SESSION['apelido_operadora'] == 'garantia') {
                $resultado .= 'PAG. REF. COMP. ';
                $resultado .= $sql2->result("CCOMPPAGA") . ' R$ ' . $formata->formataNumero($sql2->result("VALOR"));

                if ($sql2->result("JUROS") > 0)
                    $resultado .= ' Acrescido juros de R$ ' . $formata->formataNumero($sql2->result("JUROS"));

                if ($sql2->result("DESCONTOS") > 0)
                    $resultado .= ' Concedido descontos de R$ ' . $formata->formataNumero($sql2->result("DESCONTOS"));

                // s� os aditivos acima de 7 s�o considerados      
                $txt_aditivo = "SELECT SUM (NVALOADTPG) TOTAL
                          FROM HSSADTPG
                         WHERE NNUMETXMEN >= 7
                           AND NNUMETXMEN <= 12
                           AND NNUMEPAGA = :id_pagamento ";
                $sql_aditivo = new Query();
                $sql_aditivo->addParam(":id_pagamento", $this->IdPagamento);
                $sql_aditivo->executeQuery($txt_aditivo);

                if ($sql_aditivo->result("TOTAL") > 0)
                    $resultado .= ', INCLUSO NA MENS. REF. ADITAMENTO PARA ATEND. DOM. DE URG�NCIA/EMERG�NCIA, O VALOR DE R$ ' . $formata->formataNumero($sql_aditivo->result("TOTAL"));

                $txt_aberto = "SELECT DISTINCT CCOMPPAGA
                         FROM HSSPAGA
                        WHERE NNUMETITU = :id_contrato ";

                if ($sql2->result("NNUMEUSUA") > 0)
                    $txt_aberto .= "   AND NNUMEUSUA = :id_usuario ";

                $txt_aberto .= "   AND NNUMEPAGA <> :id_pagamento
                             AND CPAGOPAGA = 'N'
                             AND DCANCPAGA IS NULL
                             AND DVENCPAGA < SYSDATE
                           ORDER BY CCOMPPAGA ";

                $sql_aberto = new Query();
                $sql_aberto->addParam(":id_contrato", $sql2->result("NNUMETITU"));

                if ($sql2->result("NNUMEUSUA") > 0)
                    $sql_aberto->addParam(":id_usuario", $sql2->result("NNUMEUSUA"));

                $sql_aberto->addParam(":id_pagamento", $this->IdPagamento);
                $sql_aberto->executeQuery($txt_aberto);

                if ($sql_aberto->count() > 0) {
                    $resultado .= '. Ate ' . $data->dataAtual('DD/MM/YYYY') . ' nao acusamos o recebimento das mens. ref. a(s) comp.(s) ';
                    while (!$sql_aberto->eof()) {
                        $resultado .= $sql_aberto->result("CCOMPPAGA") . ', ';
                        $sql_aberto->next();
                    }
                    $resultado .= 'caso ja esteja paga favor desconsiderar este aviso e nos enviar copia do pagto.';
                }
            }

            $txt = "SELECT CMOTIADPR,NVL(NVALOADPAG,0) NVALOADPAG FROM HSSADPAG,HSSADPR
               WHERE NNUMEPAGA = :id_pagamento
                 AND HSSADPAG.NNUMEADPR = HSSADPR.NNUMEADPR 
                 AND CIMPRADPR = 'S' ";
            $sql_adpr = new Query();
            $sql_adpr->addParam(":id_pagamento", $this->IdPagamento);
            $sql_adpr->executeQuery($txt);

            $valorDesconto = 0;
            while (!$sql_adpr->eof()) {
                $resultado .= $sql_adpr->result("CMOTIADPR") . ' ' . $formata->formataNumero($sql_adpr->result("NVALOADPAG"));
                $valorDesconto += str_replace(',', '.', $sql_adpr->result("NVALOADPAG"));
                $sql_adpr->next();
            }

            if ($_SESSION['apelido_operadora'] == 'saudemed') {

                $txt = "SELECT CCDVENOTAF,SUBSTR(NNOTANOTAF,1,4) ANO, TO_NUMBER(SUBSTR(NNOTANOTAF,5,11)) NOTA
                  FROM HSSCNOTA,HSSNOTAF
                 WHERE NNUMEPAGA = :id_pagamento
                   AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF ";
                $sql = new Query();
                $sql->addParam(":id_pagamento", $this->IdPagamento);
                $sql->executeQuery($txt);

                if ($sql->result("CCDVENOTAF") <> '') {
                    $nota = $sql->result("ANO") . "/" . $sql->result("NOTA");
                    $resultado .= "Site para consulta da NFS-e: <br/>https://bhissdigital.pbh.gov.br/nfse/pages/consultaNFS-e_cidadao.jsf<br/>";
                    $resultado .= "N�mero da nota: " . $nota . "<br/>";
                    $resultado .= "C�digo de verifica��o: " . $sql->result("CCDVENOTAF");
                }
            }
        }

        if ($_SESSION['apelido_operadora'] == 'clinipam')
            $resultado = 'RECEBER AT� O VENCIMENTO.    ' . $resultado;

        if (($_SESSION['apelido_operadora'] == "asfal") and ( ($this->LayoutBoleto) == "F" ) or ( $this->LayoutBoleto) == "[]") {
            $resultado = '

          Aten��o: D�BITO AUTOM�TICO.
      
          BOLETO PARA SIMPLES CONFER�NCIA.   ';
            $this->LinhaDigitavel = "";
        }

        if (($_SESSION['apelido_operadora'] == 'unimedAraguaina') and ( $this->LayoutBoleto == '.b')) {
            $sqltemp = new Query();
            $txttemp = "SELECT NVL(SUM(NVALOADTPG),0) ADITIVO
                      FROM HSSADTPG                      
                     WHERE NNUMEPAGA = :id_pagamento          
                       AND NNUMETXMEN = 21042190 ";
            $sqltemp->addParam(":id_pagamento", $this->IdPagamento);
            $sqltemp->executeQuery($txttemp);

            if ($sqltemp->result("ADITIVO") > 0)
                $resultado = $resultado . "\n" . 'SR. CAIXA, SENDO SOLICITADO, CONCEDER ABATIMENTO DE R$ ' . $formata->formataNumero($sqltemp->result("ADITIVO"), 2) . '  REFERENTE AO ADITIVO UNIMED FONE.';
        }

        if (($_SESSION['apelido_operadora'] == "unimedJiParana") and ( $this->LayoutBoleto == '=')) {
            $sqlreajuste = new Query();
            $txtreajuste = "select retorna_reajuste_retroativo(:titu,:Comp) reajuste from dual";
            $sqlreajuste->addParam(":Titu", $sql2->result("NNUMETITU"));
            $sqlreajuste->addParam(":Comp", $sql2->result("CCOMPPAGA"));
            $sqlreajuste->executeQuery($txtreajuste);

            if ($sqlreajuste->result("REAJUSTE") <> '') {
                $resultado = $resultado . "\n" . $sqlreajuste->result("REAJUSTE");

                $sqltemp = new Query();
                $txttemp = "SELECT DISTINCT CTIPODUSPG
                      FROM HSSDUSPG
                     WHERE NNUMEPAGA = :Id_Paga
                       AND CTIPODUSPG = 'R' ";
                $sqltemp->addParam(":Id_Paga", $this->IdPagamento);
                $sqltemp->executeQuery($txttemp);

                if ($sqltemp->result("CTIPODUSPG") == 'R') {

                    $sqltemp = new Query();
                    $txttemp = "SELECT CNOMEUSUA,DECODE(CTIPODUSPG,'M',' Mensalidade','P','Pr�-rata','A','Ades�o','R','Mensalidade(Reajuste retroativo)') PRODUTO,
                             CTIPODUSPG,CTIPOUSUA, NVL(SUM(NVALODUSPG),0) VALOR
                        FROM HSSUSUPG,HSSUSUA, HSSPLAN,HSSDUSPG
                       WHERE HSSUSUPG.NNUMEPAGA = :Id_Paga
                         AND HSSUSUPG.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                         AND HSSUSUA.CTIPOUSUA <> 'F'
                         AND HSSUSUPG.NNUMEPLAN = HSSPLAN.NNUMEPLAN AND HSSUSUPG.NNUMEUSUA = HSSDUSPG.NNUMEUSUA AND HSSUSUPG.NNUMEPAGA = HSSDUSPG.NNUMEPAGA
                       GROUP BY CNOMEUSUA, CTIPODUSPG,CTIPOUSUA
                       ORDER BY CNOMEUSUA, PRODUTO ";
                    $sqltemp->addParam(":Id_Paga", $this->IdPagamento);
                    $sqltemp->executeQuery($txttemp);

                    while (!$sqltemp->eof()) {
                        $resultado = $resultado . "\n" . ' -- ' . $sqltemp->result(CNOMEUSUA) . '  ' . $sqltemp->result(PRODUTO) . '  valor R$ ' . $sqltemp->result(PRODUTO);
                        $sqltemp->next();
                    }
                }
            }
        }

        $bd->close();
        return $resultado;
    }

    public function MensagemJuros() {
        $formata = new Formata();

        if ($_SESSION['apelido_operadora'] == "santaCasaMontesClaros" or
                $_SESSION['apelido_operadora'] == "vitallis" or
                $_SESSION['apelido_operadora'] == "saudemed" or
                $_SESSION['apelido_operadora'] == "sampmg" or
                $_SESSION['apelido_operadora'] == "caurj" or
                $_SESSION['apelido_operadora'] == "plamheg" or
                $_SESSION['apelido_operadora'] == "asl") {
            return "";
        } 
		else {
            $txt = "SELECT NMULTTITU MULTA, NJUROTITU JURO,0 INICIO,0 ORDEM
                FROM HSSTITU
               WHERE HSSTITU.NNUMETITU = :contrato
                 AND NJUROTITU > 0
               UNION ALL
              SELECT NMULTCONGE MULTA,NJUROCONGE JURO,0 INICIO,1 ORDEM
                FROM HSSTITU,HSSCONGE
               WHERE HSSTITU.NNUMETITU = :contrato
                 AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE
                 AND NJUROCONGE > 0
              UNION ALL
              SELECT NFIXAJURO MULTA,NJUROJURO JURO,NINICJURO INICIO,2 ORDEM
                FROM HSSTITU,HSSJURO
               WHERE HSSTITU.NNUMETITU = :contrato
                 AND HSSTITU.NNUMEPLAN = HSSJURO.NNUMEPLAN
                 AND NJUROJURO > 0
              UNION ALL
              SELECT NMULTCONF MULTA,NJUROCONF JURO,0 INICIO,3 ORDEM FROM HSSCONF
               ORDER BY 4,3 ";
            $sql2 = new Query();
            $sql2->addParam(":contrato", $this->IdContrato);
            $sql2->executeQuery($txt);

            if ($sql2->result("JURO") !== null) {
                $juros = round((str_replace(",", ".", $sql2->result("JURO")) * str_replace(",", ".", $this->Valor)) / 100, 2);
            } else
                $juros = round(str_replace(",", ".", $this->Valor) * ((1 / 30) / 100), 2);

            if ($sql2->result("MULTA") > 0) {
                $multa = round((str_replace(",", ".", $sql2->result("MULTA")) * str_replace(",", ".", $this->Valor)) / 100, 2);
            } else
                $multa = 0;

            if ($_SESSION['apelido_operadora'] == 'cassind') {
                $txt = "SELECT SUM(NVALOVREAJ) TOTAL FROM HSSVREAJ WHERE HSSVREAJ.NNUMEIREAJ = 1
                   AND HSSVREAJ.DVIGEVREAJ = (SELECT MAX(VIGENCIA.DVIGEVREAJ)
                                                FROM HSSVREAJ VIGENCIA
                                               WHERE VIGENCIA.NNUMEIREAJ = 1
                                                 AND VIGENCIA.DVIGEVREAJ <= :inicio)";
                $sql2 = new Query();
                $sql2->addParam(":inicio", $this->DataVencimento);
                $sql2->executeQuery($txt);

                $juros = round(str_replace(",", ".", $this->Valor) * (str_replace(",", ".", $sql2->result("TOTAL")) / 30 / 100), 2);
            }

            if ($_SESSION['apelido_operadora'] == "plansul")
                return "Ap�s 3 dias de vencido: Juros ao dia : R$ " . number_format($juros, 2) . " Multa : R$ " . number_format($multa, 2);
            else
                return "Juros ao dia : R$ " . $formata->formataNumero($juros) . " Multa : R$ " . $formata->formataNumero($multa);

        }
    }

    function descontoProgramado() {
        $formata = new Formata();

        $sql = new Query();
        $txt = "SELECT CFLAGPAGA FROM HSSPAGA WHERE NNUMEPAGA = :id_mensalidade ";
        $sql->addParam(":id_mensalidade", $this->IdPagamento);
        $sql->executeQuery($txt);
        $flag = $sql->result("CFLAGPAGA");
        $texto = "";

        if ($flag <> 'R') {
            $sql = new Query();
            $txt = "SELECT NVALODESCO,NPERCDESCO,DVALIDESCO,NVLIQUSUPG FROM HSSUSUA,HSSDESCO,HSSUSUPG";

            if ($this->IdUsuario == 0) {
                $txt .= " WHERE HSSUSUA.NNUMETITU = :contrato
                    AND HSSUSUA.NNUMEUSUA = HSSDESCO.NNUMEUSUA
                    AND NVL(DVALIDESCO,SYSDATE + 999999) >= :vencimento ";
                $sql->addParam(":contrato", $this->IdContrato);
            } else {
                $txt .= " WHERE NTITUUSUA = :titular
                    AND HSSUSUA.NNUMEUSUA = HSSDESCO.NNUMEUSUA
                    AND NVL(DVALIDESCO,SYSDATE + 999999) >= :vencimento ";
                $sql->addParam(":titular", $this->IdUsuario);
            }

            $txt .= "   AND HSSUSUPG.NNUMEPAGA = :id_mensalidade
                  AND HSSDESCO.NNUMEUSUA = HSSUSUPG.NNUMEUSUA
                  AND NVL(DVALIDESCO,SYSDATE + 999999) = (SELECT MIN(NVL(DESCO2.DVALIDESCO,SYSDATE + 999999)) FROM HSSDESCO DESCO2
                                                           WHERE DESCO2.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                                                             AND NVL(DESCO2.DVALIDESCO,SYSDATE + 999999) >= :vencimento)
                ORDER BY DVALIDESCO ";

            $sql->addParam(":id_mensalidade", $this->IdPagamento);
            $sql->addParam(":vencimento", $this->DataVencimento);
            $sql->executeQuery($txt);

            $valor = 0;

            while (!$sql->eof()) {

                if ($sql->result("NPERCDESCO") > 0)
                    $valor += str_replace(',', '.', ($sql->result("NVLIQUSUPG") * ($sql->result("NPERCDESCO") / 100)));
                else if ($sql->result("NVALODESCO") > 0)
                    $valor += str_replace(',', '.', $sql->result("NVALODESCO"));
                else
                    $valor = 0;

                $sql->next();
            }

            $sql = new Query();
            $txt = "SELECT NVALODESCO,NPERCDESCO,DVALIDESCO,NVLIQUSUPG
                FROM HSSUSUA,HSSDESCO,HSSUSUPG
               WHERE HSSUSUA.NNUMETITU = :contrato
                 AND HSSUSUA.NNUMETITU = HSSDESCO.NNUMETITU
                 AND NVL(DVALIDESCO,SYSDATE + 999999) >= :vencimento
                 AND HSSUSUPG.NNUMEPAGA = :id_mensalidade
                 AND HSSUSUPG.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                 AND NVL(DVALIDESCO,SYSDATE + 999999) = (SELECT MIN(NVL(DESCO2.DVALIDESCO,SYSDATE + 999999)) FROM HSSDESCO DESCO2
                                                          WHERE DESCO2.NNUMETITU = HSSUSUA.NNUMETITU
                                                            AND NVL(DESCO2.DVALIDESCO,SYSDATE + 999999) >= :vencimento)
               ORDER BY DVALIDESCO ";

            $sql->addParam(":contrato", $this->IdContrato);
            $sql->addParam(":id_mensalidade", $this->IdPagamento);
            $sql->addParam(":vencimento", $this->DataVencimento);
            $sql->executeQuery($txt);

            while (!$sql->eof()) {

                if ($sql->result("NPERCDESCO") > 0)
                    $valor += str_replace(',', '.', ($sql->result("NVLIQUSUPG") * ($sql->result("NPERCDESCO") / 100)));
                else if ($sql->result("NVALODESCO") > 0)
                    $valor += str_replace(',', '.', $sql->result("NVALODESCO"));
                else
                    $valor = 0;

                $sql->next();
            }

            if ($valor > 0)
                $texto = "Conceder desconto de R$ " . $formata->formataNumero($valor) . " para pagamento ate " . $vencimento . "." . chr(13) . chr(10);

        }

        return $texto;

    }

    function CalculaJuros($contrato, $data_vencimento, $data_pagamento, $valor) {
        $bd = new Oracle();
        $retorno = array();
        $data = new Data();
        $juros = 0;
        $multa = 0;

        if ($data_pagamento == '')
            $data_pagamento = $data->dataAtual('DD/MM/YYYY');

        $pagamento = explode("/", $data_pagamento);
        $pagamento = mktime(0, 0, 0, $pagamento[1], $pagamento[0], $pagamento[2]);

        $vencimento = explode("/", $data_vencimento);
        $vencimento = mktime(0, 0, 0, $vencimento[1], $vencimento[0], $vencimento[2]);

        $fator = date('N', $vencimento);

        if ($fator == 6)
            $fator = 2;
        else if ($fator == 7)
            $fator = 1;
        else
            $fator = 0;

        $vencimento = $data->incrementaData(date("d/m/Y", $vencimento), $fator, 'D');

        while ($data->ehFeriado($bd, date("d/m/Y", $vencimento)))
            $vencimento = $data->incrementaData(date("d/m/Y", $vencimento), 1);

        if ($pagamento > $vencimento) {
            //Se o boleto esta vencido o calculo de juros deve utilizar de base o vencimento original, mesmo que caia no final de semana
            $vencimento = explode("/", $data_vencimento);
            $vencimento = mktime(0, 0, 0, $vencimento[1], $vencimento[0], $vencimento[2]);

            $txt_juros = "SELECT NMULTCONGE,NJUROCONGE,0 NINICJURO,1 ORDEM " .
                    "  FROM HSSTITU,HSSCONGE " .
                    " WHERE HSSTITU.NNUMETITU = :contrato " .
                    "   AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE " .
                    "   AND NJUROCONGE > 0 " .
                    "UNION ALL " .
                    "SELECT NFIXAJURO,NJUROJURO,NINICJURO,2 " .
                    "  FROM HSSTITU,HSSJURO " .
                    " WHERE HSSTITU.NNUMETITU = :contrato " .
                    "   AND HSSTITU.NNUMEPLAN = HSSJURO.NNUMEPLAN " .
                    "   AND NJUROJURO > 0 " .
                    "UNION ALL " .
                    "SELECT NMULTCONF,NJUROCONF,0,3 FROM HSSCONF " .
                    " ORDER BY 4,3 ";
            $sql_juros = new Query($bd);
            $sql_juros->addParam(":contrato", $contrato);
            $sql_juros->executeQuery($txt_juros);

            $perc_juros = str_replace(',', '.', $sql_juros->result("NJUROCONGE")) / 100;
            $perc_multa = str_replace(',', '.', $sql_juros->result("NMULTCONGE")) / 100;
            $atraso = ceil(($pagamento - $vencimento) / 86400);

            $juros = round(str_replace(',', '.', $valor) * $atraso * ($perc_juros), 2);

            if ($_SESSION['apelido_operadora'] == 'cassind') {
                $txt = "SELECT SUM(NVALOVREAJ) TOTAL FROM HSSVREAJ WHERE HSSVREAJ.NNUMEIREAJ = 1
                   AND HSSVREAJ.DVIGEVREAJ = (SELECT MAX(VIGENCIA.DVIGEVREAJ)
                                                FROM HSSVREAJ VIGENCIA
                                               WHERE VIGENCIA.NNUMEIREAJ = 1
                                                 AND VIGENCIA.DVIGEVREAJ <= :inicio)";
                $sql2 = new Query();
                $sql2->addParam(":inicio", $data_vencimento);
                $sql2->executeQuery($txt);

                $perc_juros = str_replace(',', '.', $sql2->result("TOTAL")) / 30 / 100;
                $juros = round(str_replace(",", ".", $valor) * $atraso * ($perc_juros), 2);
            }

            $multa = round(str_replace(',', '.', $valor) * ($perc_multa), 2);

        } else {
            $juros = 0;
            $multa = 0;
        }

        $retorno['juros'] = $juros;
        $retorno['multa'] = $multa;

        return $retorno;
    }

    function DV_NossoNumero_Sicoob($numero, $agencia, $cedente) {

        $calc = $agencia . $cedente . $numero;
        $fator = '319731973197319731973';
        $soma = 0;
        $x = 0;

        while ($x < 21) {
            $dig = substr($calc, $x, 1);
            $fat = substr($fator, $x, 1);

            $soma += ( $dig * $fat );

            $x += 1;
        }

        $resto = $soma % 11;
        If ($resto < 2)
            $resto = 0;
        else
            $resto = 11 - $resto;

        return $resto;
    }
}


?>