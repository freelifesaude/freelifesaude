<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  ini_set("memory_limit","12M");
  
  if (isset($_POST['arquivo']))
    $arquivo = $_POST['arquivo'];
  else
    $arquivo = '';
    
  if (isset($_GET['mes']))
    $mes = $_GET['mes'];
  else
    $mes = '';
    
  if (isset($_POST['msg']))
    $msg = htmlentities($_POST['msg']);  
  else
    $msg = 0;
      
  if (isset($_POST['atcad']))
    $atcad = htmlentities($_POST['atcad']);
  else
    $atcad = 0;
  
  if (isset($_POST['f_nome']))
    $nome = strtoupper(htmlentities($_POST['f_nome']));
  else
    $nome = '';  
  
  if ($arquivo <> '')
    $_SESSION['titulo'] = "ARQUIVO DE IMPORTA��O - ".$arquivo;    
  else if ($mes == '')
    $_SESSION['titulo'] = "OPERA��ES DE CADASTRO";
  else
    $_SESSION['titulo'] = "HIST�RICO DE OPERA��ES DE CADASTRO - ".$mes;

  require_once("../comum/layout.php");  
  $tpl->addFile("CONTEUDO","../comum/pendentes.htm");
  
  $inclusao     = ($seg->permissaoOutros($bd,"WEBEMPRESAINCLUSODEBENEFICIRIO") or
                   $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO"));
  $alteracao    = ($seg->permissaoOutros($bd,"WEBEMPRESAALTERAODEBENEFICIRIO") or
                   $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO"));
  $cancelamento = ($seg->permissaoOutros($bd,"WEBEMPRESACANCELAMENTODEBENEFICIRIO") or
                   $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO"));  
  
  $tpl->ID_SESSAO = $_GET['idSessao'];
  
  if ($msg == 1) {
    $sql_atcad = new Query($bd);
    $txt_atcad = "SELECT NNUMEATCAD,CNOMEATCAD,
                         DECODE(COPERATCAD,'I','Inclus�o','A','Altera��o','C','Cancelamento') OPERACAO
                   FROM HSSATCAD
                  WHERE NNUMEATCAD = :atcad ";
                    
    $sql_atcad->addParam(":atcad",$atcad);
    $sql_atcad->executeQuery($txt_atcad);
  
    $texto = "<div class='container'>
                <p>
                  S� � permitida uma opera��o de altera��o de cadastro para cada benefici�rio.
                   Este controle funciona desta forma para evitar altera��es divergentes entre si.
                </p>
                <p>
                  <table>
                    <tr>
                      <td align='right' width='25%'>Benefici�rio:</td>
                      <td width='75%'>".$sql_atcad->result("CNOMEATCAD")."</td>                     
                    </tr>
                    <tr>
                      <td align='right' width='25%'>Opera��o:</td>
                      <td width='75%'>".$sql_atcad->result("OPERACAO")."</td>                     
                    </tr>                  
                  </table>
                </p>
              </div>";               

    $tpl->MSG    = $texto;
    $tpl->CLASSE = "";
    $tpl->block("ERRO"); 
    $tpl->PAGINA_VOLTAR = "cadastro.php?op=A&idSessao=".$_GET['idSessao'];
    $tpl->block("BOTAO_VOLTAR");    
  } 
  else if ($msg == 2) {
    $sql_atcad = new Query($bd);
    $txt_atcad = "SELECT NNUMEATCAD,CNOMEATCAD,
                         DECODE(COPERATCAD,'I','Inclus�o','A','Altera��o','C','Cancelamento') OPERACAO
                   FROM HSSATCAD
                  WHERE NNUMEATCAD = :atcad ";
                    
    $sql_atcad->addParam(":atcad",$atcad);
    $sql_atcad->executeQuery($txt_atcad);
                    
    $texto = "<div class='container'>
                <p>
                  Existe uma opera��o de altera��o ou cancelamento para este benefici�rio.<br/>
                  Exclua primeiramente a altera��o para depois efetuar o cancelamento, ou aguarde o aceite da altera��o.
                </p>
                <p>
                  <table>
                    <tr>
                      <td align='right' width='25%'>Benefici�rio:</td>
                      <td width='75%'>".$sql_atcad->result("CNOMEATCAD")."</td>                     
                    </tr>
                    <tr>
                      <td align='right' width='25%'>Opera��o:</td>
                      <td width='75%'>".$sql_atcad->result("OPERACAO")."</td>                     
                    </tr>                  
                  </table>
                </p>
              </div>";   

    $tpl->MSG    = $texto;
    $tpl->CLASSE = "";
    $tpl->block("ERRO"); 
    $tpl->PAGINA_VOLTAR = "cadastro.php?op=C&idSessao=".$_GET['idSessao'];
    $tpl->block("BOTAO_VOLTAR");      
  }
  
  if (isset($_POST['operacoes']))
    if ($arquivo <> '')
      $util->redireciona2("movimentacao_arq.php","N");
    else
      $util->redireciona2("pendentes.php","N");    
      
  else {
    $sql = new Query($bd);
    $txt = "SELECT DECODE(CFLAGATCAD,NULL,'1','R','2','A','3') TIPO,HSSATCAD.NNUMEUSUA,
                   NNUMEATCAD,CNOMEATCAD,TO_CHAR(DDATAATCAD,'DD/MM/YYYY HH24:MI') DDATAATCAD,COPERATCAD,CREJEATCAD,CREAPATCAD,SEGUSUA.CNOMEUSUA,CCODIUSUA
              FROM HSSATCAD,SEGUSUA, HSSUSUA
             WHERE HSSATCAD.NNUMETITU = :contrato 
               AND HSSATCAD.NOPERUSUA = SEGUSUA.NNUMEUSUA(+)
               AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)";
                  
    if (($mes == '') and ($nome == ''))
      $txt .= "   AND (TO_CHAR(DDATAATCAD,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY') OR CFLAGATCAD IS NULL)  ";
    else if ($nome == '') {
      $txt .= "   AND TO_CHAR(DDATAATCAD,'MM/YYYY') = :mes AND CFLAGATCAD IS NOT NULL ";
      $sql->addParam(":mes",$mes);   
    }
    
    if ($nome <> '') {
      $txt .= "   AND CNOMEATCAD LIKE :nome ";
      $sql->addParam(":nome",$nome."%");         
    }
  
    if ($_SESSION['id_locacao'] > 0) {
      $txt .= "   AND HSSATCAD.NNUMESETOR IN (SELECT NNUMESETOR FROM HSSOPTIT
                                               WHERE NNUMETITU = :contrato
                                                 AND NNUMEUSUA = :operador)";           
      $sql->addParam(":operador",$_SESSION['id_operador']);
    }
    
    if ($seg->permissaoOutros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
      $txt .= "   AND HSSATCAD.NOPERUSUA = :operador ";
      $sql->addParam(":operador",$_SESSION['id_operador']);
    }    
      
    $txt .= " ORDER BY 1,COPERATCAD,DDATAATCAD DESC";  
    
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->executeQuery($txt);
       
    $i = 1;
    $tipo = '';
  
    while (!$sql->eof()) {
    
      if ($tipo <> $sql->result("TIPO")) {
        $tipo = $sql->result("TIPO");   

        if ($tipo == '1')
          $tpl->TIPO = "<font color='navy'>Opera��es PENDENTES para aprova��o da operadora</font>";
        else if ($tipo == '2')
          $tpl->TIPO = "<font color='red'>Opera��es REJEITADAS pela operadora</font>";
        else if ($tipo == '3')
          $tpl->TIPO = "<font color='green'>Opera��es CONFIRMADAS pela operadora</font>";
                    
      }
    
      if ($i==1)
        $tpl->COR_LINHA = '';
      else
        $tpl->COR_LINHA = '#EEEEEE';
        
      $i = $i * (-1);
      
      $tpl->PROTOCOLO = $sql->result("NNUMEATCAD");
      $tpl->ATCAD     = $sql->result("NNUMEATCAD");
      $tpl->CODUSUARIO = $sql->result("CCODIUSUA");
     
      if ($sql->result("COPERATCAD") == 'I') {
        $tpl->OPERACAO = '<font color="navy">Inclus�o</font>';    
        $tpl->block("BOTAO_MAIS");
        if ($_SESSION['apelido_operadora'] == 'planmed')
          $tpl->block("BOTAO_FORMULARIO");
          
      }
      else if ($sql->result("COPERATCAD") == 'A') {
        $tpl->block("BOTAO_MAIS");
        $tpl->OPERACAO = '<font color="green">Altera��o</font>';
      } else if ($sql->result("COPERATCAD") == 'C') {
        $tpl->block("BOTAO_MAIS");	  
        $tpl->OPERACAO = '<font color="red">Cancelamento</font>';
        
        if ($tipo == '3') {
          $tpl->block("VERIFICA_ATENDIMENTO");
        }
      }
      
      $tpl->DATA = $sql->result("DDATAATCAD");
      $tpl->PROCESSADO = $sql->result("DCONFATCAD");      
      $tpl->BENEFICIARIO = $sql->result("CNOMEATCAD");
      $tpl->IDUSUARIO = $sql->result("NNUMEUSUA");
      $tpl->IDOPERACAO = $sql->result("COPERATCAD");
      
      if ($inclusao or $cancelamento or $alteracao){
        if ( ($sql->result("TIPO") == '1') and $seg->permissaoOutros($bd,"WEBEMPRESAEXCLUIPROTOCOLO") )
          $tpl->block("BOTAO_EXCLUIR");
        
        if (($sql->result("TIPO") == '2') and ($sql->result("CREAPATCAD") == 'S')) {          
    		 // $tpl->LOCAL = "empresa";
          $tpl->block("BOTAO_REAPRESENTAR");          
        }
      }
      
                  
      $sql2 = new Query($bd);
      $txt2 = "SELECT * FROM HSSATUCA WHERE NNUMEATCAD = :id ";
      $sql2->addParam(":id",$sql->result("NNUMEATCAD"));
      $sql2->executeQuery($txt2);
      
      while (!$sql2->eof()) {
        if ($sql2->result("CCAMPATUCA") == 'NNUMEPLAN') {
          $anterior = $func->retornaNomePlano($bd,$sql2->result("CANTEATUCA"));
          $novo     = $func->retornaNomePlano($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMEACOM') {
          $anterior = $func->retornaNomeAcomodacao($bd,$sql2->result("CANTEATUCA"));
          $novo     = $func->retornaNomeAcomodacao($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMEPROF') {
          $anterior = $func->nomeProfissao($bd,$sql2->result("CANTEATUCA"));
          $novo     = $func->nomeProfissao($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMESETOR') {
          $anterior = $func->retornaNomeLocacao($bd,$sql2->result("CANTEATUCA"));
          $novo     = $func->retornaNomeLocacao($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NFATUTLOGR') {
          $anterior = $func->retornaTipoLogradouro($bd,$sql2->result("CANTEATUCA"));
          $novo     = $func->retornaTipoLogradouro($bd,$sql2->result("CNOVOATUCA"));
        } else if ($sql2->result("CCAMPATUCA") == 'NNUMEMCANC') {
          $anterior = $func->retornaMotivoCancelamento($bd,$sql2->result("CANTEATUCA"));
          $novo     = $func->retornaMotivoCancelamento($bd,$sql2->result("CNOVOATUCA"));
        } else {
          $anterior = $sql2->result("CANTEATUCA");
          $novo     = $sql2->result("CNOVOATUCA");
        }   
            
        $tpl->CAMPO    = $sql2->result("CDESCATUCA");
        $tpl->ANTERIOR = $anterior;
        $tpl->NOVO     = $novo;

        $tpl->block("ALTERACAO");            
        $sql2->next();
      }
    
      if (($sql->result("CREJEATCAD") <> '') and ($sql->result("COPERATCAD") <> 'I')) { 
        $tpl->CAMPO = "Motivo da rejei��o:";
        $tpl->ANTERIOR = "";
        $tpl->NOVO = $sql->result("CREJEATCAD");
        $tpl->block("ALTERACAO");           
      } else if (($sql->result("CREJEATCAD") <> '') and ($sql->result("COPERATCAD") == 'I')) {
        $tpl->CAMPO = "Motivo da rejei��o:";
        $tpl->NOVO = $sql->result("CREJEATCAD");
        $tpl->block("INCLUSAO");        
      }    

      $tpl->OPERADOR = $sql->result("CNOMEUSUA");
      
      $tpl->block("REGISTRO");
      $sql->next();
      
      if ($tipo <> $sql->result("TIPO")) {
        $i = 1;
        

        $tpl->block("BLOCO");  
      }    
    } 
    
    if (($mes == '') and ($nome == '') and ($arquivo == '')) {
   
      $sql_visu = new Query($bd);      
      $txt_visu = "UPDATE HSSATCAD SET DVISUATCAD = SYSDATE
                    WHERE NNUMEATCAD IN (SELECT NNUMEATCAD FROM HSSATCAD,HSSUSUA
                                          WHERE HSSATCAD.NNUMETITU = :contrato
                                            AND HSSATCAD.NNUMEUSUA = HSSUSUA.NNUMEUSUA";
		                                                                         
      if ($seg->permissaoOutros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
        $txt_visu .= "                      AND HSSATCAD.NOPERUSUA = :operador ";
        $sql_visu->addParam(":operador",$_SESSION['id_operador']);
      }                                            
                                            
      $txt_visu .= "                        AND DVISUATCAD IS NULL ) ";
     
      $sql_visu->addParam(":contrato",$_SESSION['id_contrato']);
      $sql_visu->executeSQL($txt_visu);
  
      $sql = new Query($bd);
      $txt = "SELECT DISTINCT TO_CHAR(DDATAATCAD,'MM/YYYY') MES, TO_CHAR(DDATAATCAD,'YYYYMM') MES2
                FROM HSSATCAD
               WHERE NNUMETITU = :contrato ";
               
      if ($_SESSION['id_locacao'] > 0) {
        $txt .= "   AND HSSATCAD.NNUMESETOR = :locacao ";           
        $sql->addParam(":locacao",$_SESSION['id_locacao']);
      }            

      if ($seg->permissaoOutros($bd,"WEBEMPRESAMOSTRASOMENTEMOVIMENTAOFEITAPELOOPERADOR",false)) {
        $txt .= "   AND HSSATCAD.NOPERUSUA = :operador ";
        $sql->addParam(":operador",$_SESSION['id_operador']);
      }                                            
      
      $txt .= "  AND CFLAGATCAD IS NOT NULL
                 AND DDATAATCAD < FIRST_DAY(SYSDATE)
               ORDER BY 2 DESC";       
                         
      $sql->addParam(":contrato",$_SESSION['id_contrato']);
      $sql->executeQuery($txt);
        
      if ($sql->count() > 0) {
        while (!$sql->eof()) {
          $tpl->MES = $sql->result("MES");
          $tpl->block("MESES");  
          $sql->next();
        }
        
        $tpl->block("HISTORICO");
      }
    } else {
    
      if ($arquivo <> '')
        $tpl->PAGINA_VOLTAR = "movimentacaoArq.php?idSessao=".$_GET['idSessao'];
      else    
        $tpl->PAGINA_VOLTAR = "pendentes.php?idSessao=".$_GET['idSessao'];
        
      $tpl->block("BOTAO_VOLTAR");
    }
    
    $tpl->block("REGISTROS");
  }
     
  $tpl->NOME = $nome;     
  $tpl->block("MOSTRA_MENU");     
  $bd->close();
  $tpl->show();     

?>