<?php
  class Imagem
  {
  
    function __call($func,$arq) {
      if(!method_exists(get_class($this),$func)){
        throw new Exception(" O metodo \"$func\" nao existe");
      }
    }

    function insereArquivo ($filename,
                            $descricao,
                            $local,
                            $tabela) {
                            
      $sql_prox = new Query($bd);
      $txt_prox = "SELECT SEQ_ARQUI.NEXTVAL PROXIMO FROM DUAL";
      $sql_prox->executeQuery($txt_prox);
      
      $idArquivo =  $sql_prox->result("PROXIMO");

      if ($idArquivo > 0) {
        $file = array();
        $file = $filename;
      
        $arquivo  = basename($file['name']);
        $extensao = strtoupper(pathinfo($file['name'], PATHINFO_EXTENSION));
        $tamArq   = str_replace('.',',',filesize($file['tmp_name'])/1024);
        
        if ($descricao == '')
          $descricao = substr($arquivo,0,strlen($arquivo)-(strlen($extensao) + 1));
          
        $sql_prop = new Query($bd);
        $txt_prop = "INSERT INTO HSSARQUI (NNUMEARQUI,CNOMEARQUI,CDESCARQUI,NSIZEARQUI,DDATAARQUI,NSUPEARQUI,CTIPOARQUI,CEXT_ARQUI,CLOCAARQUI)
                                    VALUES(:idArquivo,:arquivo,:descricao,TRUNC(:tamanho),sysdate,null,'A',:extensao,:local)";
        $sql_prop->addParam(":idArquivo",$idArquivo);
        $sql_prop->addParam(":arquivo",$arquivo);
        $sql_prop->addParam(":descricao",$descricao);             
        $sql_prop->addParam(":tamanho",$tamArq);
        $sql_prop->addParam(":extensao",$extensao);
        $sql_prop->addParam(":local",strToUpper($local));        
        $sql_prop->executeSQL($txt_prop);      
            
        $imagem = file_get_contents($file['tmp_name']);
        
        $sql2 = new Query($bd);
        $txt2 = "INSERT INTO HSSARQME (NNUMEARQUI,CARQUARQME)
                              VALUES (:idArquivo,EMPTY_BLOB()) RETURNING CARQUARQME INTO :imagem";
        $sql2->addParam(":idArquivo",$idArquivo);
        $sql2->addLob(":imagem",$imagem);
        $sql2->executeSQL($txt2);
      }
      return $idArquivo;  
    }
  }    
?>