<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();
  
  $tpl = new Template("../comum/buscaCepEndereco.html");
  $tpl->FORM_PAI  = $_POST['form_pai'];
  $tpl->CAMPO_CEP = $_POST['campo_cep'];
  
  echo $tpl->parse();
  
  $bd->close();  
?>