<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 

  $bd  = new Oracle();
 
  if ($_SESSION['sistema'] == 'Consultorio') {
    $sql_not = new Query($bd);
    $txt_not = "SELECT COUNT(*) QTDE
                  FROM HSSATPRE,HSSATEND,HSSMENSA
                 WHERE HSSATPRE.NNUMEPRES = :prestador
                   AND HSSATPRE.NNUMEATEND = HSSATEND.NNUMEATEND
                   AND HSSATEND.NNUMEATEND = HSSMENSA.NNUMEATEND
                   AND HSSMENSA.DVISUMENSA IS NULL
                   AND HSSMENSA.CTIPOMENSA IN ('0','3')";
                   
    if ($_SESSION['apelido_operadora'] == 'unimedFranca')
      $txt_not .= "   AND HSSATEND.NNUMECLATE <> 7 ";
      
    $sql_not->addParam(":prestador",$_SESSION['id_contratado']);                   
    $sql_not->executeQuery($txt_not);
    
    $qtde = $sql_not->result("QTDE");
    $html = '';
     
    if ($qtde > 0) {
      if ($qtde == 1)
        $html .= '<a title="Existe 1 contato n�o visualizado." style="padding:8px 0px;" href="faleconosco.php?idSessao='.$_GET['idSessao'].'">
                  <span class="badge badge-warning"><i class="fa fa-user"></i>&nbsp;<font style="font-size:10px">1</font></span>
                  </a>';  
      else
        $html .= '<a title="Existem '.$qtde.' contatos n�o visualizados." style="padding:8px 0px;" href="faleconosco.php?idSessao='.$_GET['idSessao'].'">
                  <span class="badge badge-warning"><i class="fa fa-user"></i>&nbsp;<font style="font-size:10px">'.$qtde.'</font></span>
                  </a>';      
    }
                     
    if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
      $sql_not = new Query($bd);
      $txt_not = "SELECT COUNT(*) QTDE
                    FROM HSSATPRE,HSSATEND,HSSMENSA
                   WHERE HSSATPRE.NNUMEPRES = :prestador
                     AND HSSATPRE.NNUMEATEND = HSSATEND.NNUMEATEND
                     AND HSSATEND.NNUMEATEND = HSSMENSA.NNUMEATEND
                     AND HSSMENSA.DVISUMENSA IS NULL
                     AND HSSMENSA.CTIPOMENSA IN ('0','3')
                     AND HSSATEND.NNUMECLATE = 7 ";
        
      $sql_not->addParam(":prestador",$_SESSION['id_contratado']);                   
      $sql_not->executeQuery($txt_not);
      
      $qtde = $sql_not->result("QTDE");
      
      if ($qtde > 0) {      
        if ($qtde == 1)
          $html .= '<a title="Existe 1 contato de solicita��o de f�rias n�o visualizado." style="padding:8px 0px;" href="solicitacaoFerias.php?idSessao='.$_GET['idSessao'].'">
                    <span class="badge badge-warning"><i class="fa fa-plane"></i>&nbsp;<font style="font-size:10px">1</font></span>
                    </a>';  
        else
          $html .= '<a title="Existem '.$qtde.' contatos de solicita��o de f�rias n�o visualizados." style="padding:8px 0px;" href="solicitacaoFerias.php?idSessao='.$_GET['idSessao'].'">
                    <span class="badge badge-warning"><i class="fa fa-plane"></i>&nbsp;<font style="font-size:10px">'.$qtde.'</font></span>
                    </a>';
      }                  
    }    
  }
  
  echo utf8_encode($html);

  $bd->close();
?>