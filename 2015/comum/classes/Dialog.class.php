<?php      
  class Dialog {  
    public  $text;
    public  $type;
    public  $title;
    
    private $modal;
    private $name;
    private $imagem;
    private $css;
    private $buttons = array();  
    private $params  = array();
    private $textButtons;
    private $widthPopUp;
    private $heightPopUp;  
  
    public function Dialog($txt='',$title="",$type="",$modal=true) {
      $this->widthPopUp  = 800;
      $this->heightPopUp = 600;        
      $this->text        = $txt;
      $this->height      = 0;
      $this->width       = 400;
      $this->setModal($modal);
      $this->type        = $type;
      $this->title       = $title;
    }    
      
    public function setHeight($value) {
      $this->height = $value;     
    }   

    public function setWidth($value) {
      $this->width = $value;     
    }     
      
    public function setModal($modal) {
    
      if ($modal)
        $this->modal = 'true';
      else
        $this->modal = 'false';      
    } 
    
    public function setWidthPopUp($value) {    
      $this->widthPopUp = $value;
    }    
    
    public function setHeightPopUp($value) {    
      $this->heightPopUp = $value;
    }       
    
    public function addButton($name,$type,$url="",$url2="") {
      array_push($this->buttons,array($name,$type,$url,$url2));
    }    
    
    public function setParams($params) {
      $this->params = $params;
    }      
    
    public function parse() {   
      
      if (sizeOf($this->buttons) == 0) {
        $this->addButton("Ok","close");
      }
      
      if ($this->type == "erro") {
        $this->css    = "dialog error";
        $this->imagem = "erro2.png";                     
      } else if ($this->type == "confirmacao") {
        $this->css    = "dialog ok";
        $this->imagem = "confirmacao3.png";        
      } else if ($this->type == "alerta") {
        $this->css    = "dialog alerta";
        $this->imagem = "alerta3.png";        
      } else if ($this->type == "info") {
        $this->css    = "dialog info";
        $this->imagem = "info.png";        
      }     
      
      if (sizeOf($this->buttons) > 0) {
                
        $this->textButtons = ',buttons: {';
      
        for ($b = 0;$b < sizeof($this->buttons);$b++) {
          $button = array();
          $button = $this->buttons[$b];
          
          $this->textButtons .= '"'.$button[0] . '": function () {';
          
          if ($button[1] == "close")
            $this->textButtons .= '$( this ).dialog( "close" );';
          
          if ($button[1] == "redirect") {
            
            if (sizeOf($this->params) > 0) {
              $this->textButtons .= ' $("#formSubmit").attr("target","");
                                      $("#formSubmit").attr("action","'.$button[2].'");
                                      $("#formSubmit").submit(); ';
            }              
            else
              $this->textButtons .= 'window.location = \''.$button[2].'\';';
          }           
            
          if ($button[1] == "popup") {
            $this->textButtons .= 'abrirPopUp(\''.$button[2].'\',\''.$button[0].'\','.$this->heightPopUp.','.$this->widthPopUp.');'.
                                  '$( this ).dialog( "close" );';
          }                   
          
          if ($button[3] <> '')
            $this->textButtons .= 'window.location = \''.$button[3].'\';';;
            
          $this->textButtons .= '},';
          
        }
          
        $this->textButtons = substr($this->textButtons,0,-1).'}';
      }     
    }
    
    public function show() {
      $this->parse();
                                      
    	$html = '<script type="text/javascript">
	               $(function() {
		               // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		               $( "#dialog:ui-dialog" ).dialog( "destroy" );
                   
		               $( "#dialog-modal" ).dialog({
                     closeOnEscape: false,
                     resizable: false,';
                     
      $html .= 'width: '.$this->width.',';
        
      if ($this->height == 0)
        $html .= 'height: "auto",';
      else
        $html .= 'height: '.$this->height.',';
                
      $html .= '    zIndex: 5000,
                     close: function(ev, ui) { $(this).remove(); },
                     open : function(ev, ui) { $(".ui-dialog-titlebar-close",ui.dialog).hide(); },
                     title:  "'.$this->title.'",                     
			               modal:  '.$this->modal.
                     $this->textButtons.'
		               });
                                      
	               });                                  
	            </script>';
              
    
      if (sizeOf($this->params) > 0) {
        $html .= '<form id="formSubmit" name="formSubmit" method="post" action="" target="">';
      
        foreach ($this->params as $p) {
          $p1 = array();
          $p1 = $p;
        
          $html .= '<input type="hidden" id="'.$p1['nome'].'" name="'.$p1['nome'].'" value="'.$p1['valor'].'">';
        }
              
        $html .= '</form>';
      }
      
      $html .= '<div id="dialog-modal" title="Basic modal dialog" class="'.$this->css.'">';
      
      if ($this->imagem <> '')
        $html .= '<div style="width:15%; float: left;" ><img src="../comum/img/'.$this->imagem.'" alt="'.$this->imagem.'" /></div>
                  <div style="width:85%; float: left;" >'.$this->text.'</div>';
      else
        $html .= '<div>'.$this->text.'</div>';
      
      $html .= '</div>';
             
      return $html;  
    } 

    public function showTpl($tpl) {    
      $tpl->TEXTO_MENSAGEM = $this->show();
    }    
     
  }
  
?>