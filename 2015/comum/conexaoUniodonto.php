<?php 
  Session_start();
       
  class PostGre {
    private $connect;  
    private $user = "agemed"; 
    private $pass = "agemed"; 
    private $host =  "sistema.uniodontosc.com.br"; 
    private $port =  "2345";
		private $dbname= "Uniodonto";
      
    public function PostGre() {
        
      $this->connect = pg_connect("host=$this->host port=$this->port dbname=$this->dbname user=$this->user password=$this->pass");

      if(!$this->connect) {
        echo "<p>N&atilde;o foi possivel conectar-se ao servidor PostGre.</p>";
        exit();
      } 
    }   

    public function conn1() {
      return $this->connect; 
    }
    
    public function close1() {
       pg_close($this->connect); 
    }      
  }
  
  class PostGreQuery {
    private $connect;    
    private $binds = array();    
    private $results;
    private $count;
    private $start = 0;
    private $pos = 0;
    private $end = 0;
    private $close;
    private $db;
    
    public function PostGreQuery() {
      $db = new PostGre();
      $this->close = true;        
      $this->db = $db;
      $this->connect = $this->db->conn1();        
    }
        
    public function executeQuery1($sql) {
      try {
        $stmt = pg_query($this->connect,$sql);

        if (!$stmt) {
          $this->count = 0;      
          throw new Exception("Ocorreu um erro ao executar a query!\n");          
        } else {  
          $this->results = pg_fetch_all($stmt);        
          $this->count = pg_num_rows($stmt);
          $this->pos = 0;
          $this->end = $this->count;
        }        
      } catch (Exception $erro) {       
        echo "\n<pre >";
        echo "Function executeQuery\n\n";
        echo "SQL: \"" . htmlentities($sql) . "\"\n"; 
        echo "</pre>";
        exit;
      }

      if ($this->close)
        $this->db->close1();
    }
    
    public function eof1() {
      if ($this->pos == $this->count)
        $value = true;
      else
        $value = false;   
      
      return $value;
    }
    
    public function next1() {
      $this->pos = $this->pos + 1;  
    }
    
    public function result1($column) {
      return $this->results[$this->pos][$column];
    }
    
    public function count1() {
      return $this->count;
    }
    
    public function first1() {
      $this->pos = 0;
    }

    public function end1() {
      $this->pos = $this->count-1;
    }
  }
   
/*
    class Conexao{ 
        var $user = "agemed"; 
        var $pass = "agemed"; 
        var $host =  "sistema.uniodontosc.com.br"; 
//		var $port =  "2346";
        var $port =  "2345";
		var $dbname= "Uniodonto";
        var $link; 
        var $result;

        // Metodo construtor 

        function Conexao($bd){ 

            $this->link = 
			pg_connect("host=$this->host port=$this->port dbname=$this->dbname user=$this->user password=$this->pass") or die ("Configuracao de Banco de Dados Errada!"); 
			
			} 


        // Executa query 

        function Executar($sql){ 


            $this->result = pg_exec($sql) or die ("Erro ao executar query"); 

            return $this->result; 

        } 


        // Salva no array $line resultados retornados 

        function MostrarResultados(){ 


            $line = pg_fetch_array($this->result); 

            return $line; 

        } 


        // Numero de linhas retornada na consulta 

        function ContarLinhas(){ 


            $lines = pg_num_rows($this->result); 

            return $lines; 

        } 


        // Fecha conexao 

        function Fechar(){ 

        pg_close($this->link); 

        } 


     // Libera consulta da memoria 

        function Liberar(){ 
        pg_free_result($this->result); 

       } 

    } 

/*function redirecionar($url, $tempo) 
{ 
    $url = str_replace('&amp;', '&', $url); 
         
    if($tempo > 0) 
    { 
        header("Refresh: $tempo; URL=$url"); 
    } 
    else 
    { 
        @ob_flush();
        @ob_end_clean();
        header("Location: $url"); 
        exit; 
    } 
} */
//$bd="site";

?>