<?php    
  /* Cria classes padr�es */
  $seg  = new Seguranca();  
  $util = new Util();  
  $data = new Data();
  $formata = new Formata();
  $func = new Funcao();
  
  $tpl = new Template("../comum/janelaPadrao.html");
  $banner = '';
  
  if ($_SESSION['sistema'] == 'Empresa') {
    
    if (file_exists('../comum/img/banner_empresa.png'))
      $banner = 'banner_empresa.png';    
  }
  else if ($_SESSION['sistema'] == 'Prestador') {
    
    if (file_exists('../comum/img/banner_prestador.png'))
      $banner = 'banner_prestador.png';        
  } 
  else if ($_SESSION['sistema'] == 'Comercial') {
    
    if (file_exists('../comum/img/banner_comercial.png'))
      $banner = 'banner_comercial.png';            
  }    
  else if ($_SESSION['sistema'] == 'Usuario') {
    
    if (file_exists('../comum/img/banner_beneficiario.png'))
      $banner = 'banner_beneficiario.png';                
  }    
  else if ($_SESSION['sistema'] == 'Operadora') {
    
    if (file_exists('../comum/img/banner_operadora.png'))
      $banner = 'banner_operadora.png';                
  }    
  else if ($_SESSION['sistema'] == 'Rede') {
   
    if (file_exists('../comum/img/banner_rede.png'))
      $banner = 'banner_rede.png';                    
  }    
  else if ($_SESSION['sistema'] == 'Auditoria') {
    $banner = '';
  }
  else if ($_SESSION['sistema'] == 'Agendamento') {
  }    
  else if ($_SESSION['sistema'] == 'Saude_Ocupacional') {
    
    if (file_exists('../comum/img/banner_saudeocupacional.png'))
      $banner = 'banner_saudeocupacional.png';                
  } 
  if ($banner == '')
    $tpl->BANNER = 'banner.png';  
  else
    $tpl->BANNER = $banner;
?>