<?php
  
class GuiaTiss {

  private $formata;
  private $func;
  private $seg;
  private $conta;
  
  public $totalGases;
  public $totalMedicamentos;
  public $totalMateriais;
  public $totalTaxas;
  public $totalDiarias;
  public $totalAlugueis;
  public $totalGeral;
  public $totalOPMEs;
  
  public $procedimentos = array();
  public $despesas      = array();
  public $quimio        = array();  
  public $opmes         = array();
  public $equipe        = array();
  public $profissionalVia = array();
  public $numeroProcedimentos;
  public $numeroDespesa;
  public $numeroOPME;
  public $numeroEquipe;
  
  public $dataEmissao;
  public $dataSolicitacao;
  public $idPrestador;
  public $idLocalAtendimento;
  public $idConta;
  public $idGuia;
  public $numeroGuia;
  public $numeroGuiaPrincipal;
  public $numeroConta;
  public $numeroDaCarteira;
  public $senha;
  public $codigoContrato;
  public $codigoPlano;
  public $descricaoPlano;
      
  public $dataValidadeGuia;
  public $validadeCarteira;
  public $nomeBeneficiario;
  public $cartaoNacionalSaude;
  public $cnpjLocal;
  public $localAtendimento;
  public $cnesLocal;
  public $nomeExecutante;
  public $conselhoExecutante;
  public $cnesExecutante;
  public $crmExecutante;
  public $ufCrmExecutante;
  public $cbos;
  public $indicadorAcidente;
  public $dataAtendimento;
  public $tipoConsulta;
  public $nomeSolicitante;
  public $conselhoSolicitante;
  public $crmSolicitante;
  public $ufCrmSolicitante;
  public $cbosSolicitante;
  public $cnpjExecutante;
  public $cnpjSolicitante;
  public $urgencia;
  public $tipoGuia;
  public $regimeInternacao;
  public $hipoteseDiagnostica;
  public $statusGuia;
  public $tipoAtendimento;
  public $tipoSaida;
  public $tipoFaturamento;
  public $dataInicioFaturamento;
  public $horaInicioFaturamento;
  public $dataFimFaturamento;
  public $horaFimFaturamento;
  public $tipoInternacao;
  public $cidObito;
  public $declaracaoNascidoVivo;
  public $motivoSaidaInternacao;
  public $indicadorDoRN;
  public $atendimentoRN;
  public $cid1;
  public $cid2;
  public $cid3;
  public $cid4;
  public $declaracaoObito;
  public $acomodacaoTiss;
  public $observacao;
  public $diariasSolicitadas;
  public $diariasUtilizadas;
  public $nomeCongenere;
  public $idCongenere;
  public $autenticaBiometria;
  public $valorTotalGuia;
  public $tipoOnco;
  public $estadiamento;
  public $tipoQuimioterapia;
  public $finalidade;
  public $ecog;
  public $planoTerapeutico;
  public $diagnosticoCitoHisto;
  public $informacoesRelevantes;
  public $cirurgia;
  public $dataCirurgia;
  public $areaIrradiada;
  public $dataAplicacao;
  public $numeroCiclos;
  public $cicloAtual;
  public $intervaloCiclos;
  public $diagnosticoImagem;
  public $numeroCampos ;
  public $doseDiaria;
  public $doseTotal;
  public $numeroDias;
  public $dataPrevistaAdm;     
  public $peso;
  public $altura;
  public $superficieCorporal;
  public $idade;
  public $sexo;
  public $carregaFaturada;
  public $previsaoOpme;
  public $previsaoQuimio;
  public $dataDiagnostico;
  public $dataSugeridaInternacao;
      
  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  }  
    
  public function __construct($bd) {    
    $this->bd = $bd;
    $this->formata  = new Formata();
    $this->func     = new Funcao();
    $this->seg      = new Seguranca();
  }
  
  public function carregaDados() {  

    $sql = new Query($this->bd);  
    $txt = "SELECT HSSGUIA.NNUMEGUIA,TO_CHAR(HSSGUIA.DEMISGUIA,'DD/MM/YYYY HH24:Mi') DEMISGUIA,NVL(HSSGUIA.CTPDOGUIA,'|_|') CTPDOGUIA,
                   NVL(HSSGUIA.NTPDOGUIA,'0') NTPDOGUIA,NVL(HSSGUIA.CTTPDGUIA,'|_|') CTTPDGUIA,NVL(HSSGUIA.CIACIGUIA,'|_|') CIACIGUIA,HSSGUIA.COBSEGUIA,
                   NVL(CTPCOGUIA,' ') CTPCOGUIA,RETORNA_NOME_OPERADOR(HSSGUIA.NOPERUSUA) OPERADOR,HSSGUIA.CDINDGUIA,
                   TO_CHAR(HSSGUIA.DEMISGUIA,'DD/MM/YYYY') DATA_GUIA,
                   HSSGUIA.CSENHGUIA,TO_CHAR(HSSGUIA.DVALIGUIA,'DD/MM/YYYY') DVALIGUIA,HSSGUIA.NSOLIPRES,DECODE(HSSGUIA.CURGEGUIA,'E','1','U','2') CURGEGUIA,
                   HSSGUIA.CHIPOGUIA,DECODE(HSSGUIA.CTIPOGUIA,'C','2','O','3','P','4','1') CTIPOGUIA,
                   HSSUSUA.CCODIUSUA,HSSUSUA.CCARTUSUA,TO_CHAR(HSSUSUA.DPVALUSUA,'DD/MM/YYYY') DPVALUSUA,HSSUSUA.CUNIMUSUA,
                   NVL(HSSUSUA.CNOMEUSUA,HSSGUIA.CCORTGUIA) CNOMEUSUA,CSUS_USUA,NOME_TITULAR(HSSUSUA.NNUMETITU) TITULAR,
                   HSSPLAN.CCODIPLAN,HSSPLAN.CDESCPLAN,HSSPLAN.CMODAPLAN MODALIDADE,
                   RETORNA_CHAPA_USUARIO(HSSUSUA.NNUMEUSUA) MATRICULA,HSSUSUA.NNUMEUSUA,HSSTITU.NNUMEEMPR,HSSUSUA.NNUMESETOR,HSSGUIA.NNUMENUSUA,                        
                   DECODE(LOCAL.CPESSPRES,'F',LOCAL.CCPF_PRES,LOCAL.CCGC_PRES) CCGC_LOCAL,LOCAL.CNOMEPRES LOCAL_ATENDIMENTO,LOCAL.CCREDPRES,
                   LOCAL.CCNESPRES CCNESLOCAL,HSSTLOGR.CCODITLOGR,SUBSTR(TRIM(LOCAL.CENDEPRES||' '||LOCAL.CBAIRPRES),1,28) CENDEPRES,
                   LOCAL.CCIDAPRES,LOCAL.CESTAPRES,LOCAL.CCEP_PRES,LOCAL.CFONEPRES,LOCAL.NNUMEPRES,LOCAL.CCON1PRES,
                   PRES.CNOMEPRES,CONSELHOMEDICOTISS_3_0(PRES.CCONSPRES) CONSELHO,PRES.NCRM_PRES NCRM_PRES,RETORNA_TISS_ESTADO(PRES.CUFCRPRES) CUFCRPRES,PRES.NNUMEPRES ID_PRES,
                   DECODE(PRES.CPESSPRES,'F',PRES.CCPF_PRES,PRES.CCGC_PRES) CCGC_PRES,PRES.CCNESPRES,PRES.CFONEPRES FONE_PRES,
                   DECODE(SOLI.CPESSPRES,'F',SOLI.CCPF_PRES,SOLI.CCGC_PRES) CCGC_SOLI,SOLI.CCNESPRES CCNESSOLI,
                   SOLI.CNOMEPRES SOLICITANTE,SOLI.NCRM_PRES NCRM_SOLI, CONSELHOMEDICOTISS_3_0(SOLI.CCONSPRES) CONSELHO_SOLI,RETORNA_TISS_ESTADO(SOLI.CUFCRPRES) CUFCRSOLI,
                   HSSESPEC.CCODIESPEC,HSSGUIA.NCOMPGUIA,HSSGUIA.NNUMEESPEC NNUMEESPEC,
                   HSSCONGE.CRAZACONGE,HSSACOM.CANS_ACOM,HSSACOM.CDESCACOM, CREGIGUIA, NVL(CTATEGUIA,'|_|') CTATEGUIA,
                   NVL(CTSAIGUIA,'|_|') CTSAIGUIA,HSSGUIA.CSTATGUIA, CATRNGUIA, HSSGUIA.CLOCAGUIA, HSSMOTLB.NNUMEMOTLB,HSSCONGE.NNUMECONGE,
                   HSSGUIA.NPESOGUIA,HSSGUIA.NALTUGUIA * 100 NALTUGUIA,HSSGUIA.NSUPCGUIA,IDADE(HSSUSUA.DNASCUSUA,HSSGUIA.DEMISGUIA) IDADE,HSSUSUA.CSEXOUSUA,SOLI.CFONEPRES FONE_SOLI,
                   TO_CHAR(NVL(HSSGUIA.DRECEGUIA,HSSGUIA.DEMISGUIA),'DD/MM/YYYY') DRECEGUIA,HSSGUIA.CJTOPGUIA,HSSTITU.CCODITITU, HSSGUIA.CPUOPGUIA, HSSGUIA.CPUQMGUIA,
                   TO_CHAR(HSSGUIA.DINTEGUIA,'DD/MM/YYYY HH24:Mi') DINTEGUIA
              FROM HSSGUIA,HSSUSUA,HSSPLAN,FINPRES LOCAL,HSSTLOGR,FINPRES PRES,HSSESPEC,HSSTITU,HSSCONGE,FINPRES SOLI,HSSACOM,HSSMOTLB
             WHERE HSSGUIA.NNUMEGUIA = :guia
               AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN(+)
               AND HSSGUIA.NLOCAPRES = LOCAL.NNUMEPRES(+)
               AND LOCAL.NNUMETLOGR  = HSSTLOGR.NNUMETLOGR(+)
               AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES(+)
               AND HSSGUIA.NNUMEESPEC = HSSESPEC.NNUMEESPEC(+)
               AND HSSUSUA.NNUMETITU  = HSSTITU.NNUMETITU(+)
               AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
               AND HSSGUIA.NSOLIPRES  = SOLI.NNUMEPRES(+) 
               AND HSSGUIA.NNUMEACOM  = HSSACOM.NNUMEACOM(+)
               AND HSSGUIA.NNUMEMOTLB = HSSMOTLB.NNUMEMOTLB(+)";
               
    $sql->addParam(":guia",$this->idGuia);    
    $sql->executeQuery($txt);
      
    if ($this->idGuia > 0) {
      $this->numeroGuia          = $sql->result("NNUMEGUIA");
      $this->numeroGuiaPrincipal = $sql->result("NCOMPGUIA");
      $this->numeroConta         = $sql->result("NNUMEGUIA");
      
      if ($_SESSION['apelido_operadora'] == "vitallis" or $_SESSION['apelido_operadora'] == "casf" or $_SESSION['apelido_operadora'] == "fatimaSaude" or $_SESSION['apelido_operadora'] == "cassind")
        $this->numeroDaCarteira = $sql->result("CCARTUSUA");
      else
        $this->numeroDaCarteira = $sql->result("CCODIUSUA");    
        
      if (($_SESSION['apelido_operadora'] == 'idealSaude') or ($_SESSION['apelido_operadora'] == 'casf'))
        $this->senha = $sql->result("CSENHGUIA");
      else
        $this->senha = $sql->result("NNUMEGUIA");    
        
      $this->dataEmissao           = $sql->result("DEMISGUIA");
      $this->dataSolicitacao       = $sql->result("DRECEGUIA");
      $this->dataValidadeGuia      = $sql->result("DVALIGUIA");  
      $this->validadeCarteira      = $sql->result("DPVALUSUA");
      $this->nomeBeneficiario      = $sql->result("CNOMEUSUA");
      $this->cartaoNacionalSaude   = $sql->result("CSUS_USUA");
      $this->cnpjLocal             = $sql->result("CCGC_LOCAL");
      $this->localAtendimento      = $sql->result("LOCAL_ATENDIMENTO");
      $this->cnesLocal             = $sql->result("CCNESLOCAL");
      $this->conselhoExecutante    = $sql->result("CONSELHO");
      
      if ($_SESSION['apelido_operadora'] == 'sindifiscope') {
        $this->cnpjExecutante        = $sql->result("CCGC_LOCAL");
        $this->nomeExecutante        = $sql->result("LOCAL_ATENDIMENTO");      
      }
      else {
        $this->cnpjExecutante        = $sql->result("CCGC_PRES");
        $this->nomeExecutante        = $sql->result("CNOMEPRES");
      }      
      
      $this->cnesExecutante        = $sql->result("CCNESPRES");
      $this->crmExecutante         = $sql->result("NCRM_PRES");
      $this->ufCrmExecutante       = $sql->result("CUFCRPRES");
      $this->cbos                  = $this->func->retornaCodigoEspecialidadeGuia($this->bd,$sql->result("NNUMEGUIA"));
      $this->indicadorAcidente     = $sql->result("CIACIGUIA");
      $this->dataAtendimento       = $sql->result("DATA_GUIA");
      $this->tipoConsulta          = $sql->result("CTPCOGUIA");
      $this->nomeSolicitante       = $sql->result("SOLICITANTE");
      $this->conselhoSolicitante   = $sql->result("CONSELHO_SOLI");
      $this->telefoneSolicitante   = $sql->result("FONE_SOLI");
      $this->emailSolicitante      = $this->func->retornaEmailPrestador($sql->result("NSOLIPRES"),'');
      $this->crmSolicitante        = $sql->result("NCRM_SOLI");
      $this->ufCrmSolicitante      = $sql->result("CUFCRSOLI");
      $this->cbosSolicitante       = $this->func->retornaCodigoEspecialidadePrestador($this->bd,$sql->result("NSOLIPRES"));
      if ($_SESSION['apelido_operadora'] == 'sindifiscope') {
        $this->cnpjExecutante        = $sql->result("CCGC_LOCAL");
        $this->nomeExecutante        = $sql->result("LOCAL_ATENDIMENTO");      
      }
      else {
        $this->cnpjExecutante        = $sql->result("CCGC_PRES");
        $this->nomeExecutante        = $sql->result("CNOMEPRES");
      } 
      $this->cnpjSolicitante       = $sql->result("CCGC_SOLI");
      $this->urgencia              = $sql->result("CURGEGUIA");
      $this->tipoGuia              = $sql->result("CTIPOGUIA");
      $this->regimeInternacao      = $sql->result("CREGIGUIA");
      $this->hipoteseDiagnostica   = $sql->result("CHIPOGUIA");
      $this->statusGuia            = $sql->result("CSTATGUIA");  
      $this->tipoAtendimento       = $sql->result("CTATEGUIA");
      $this->tipoSaida             = $sql->result("CTPSAGUIA");  
      $this->tipoFaturamento       = "";
      $this->dataInicioFaturamento = "|__|__|/|__|__|/|__|__|__|__|";  
      $this->horaInicioFaturamento = "";  
      $this->dataFimFaturamento    = "|__|__|/|__|__|/|__|__|__|__|";  
      $this->horaFimFaturamento    = ""; 
      $this->tipoInternacao        = "";
      $this->cidObito              = "";
      $this->declaracaoNascidoVivo = "";
      $this->motivoSaidaInternacao = "";
      $this->indicadorDoRN         = "|_|";
      $this->atendimentoRN         = $sql->result("CATRNGUIA");
      $this->cid1                  = "";
      $this->cid2                  = "";
      $this->cid3                  = "";
      $this->cid4                  = "";
      $this->declaracaoObito       = "";
      $this->acomodacaoTiss        = $sql->result("CANS_ACOM");  
      $this->idPrestador           = $sql->result("ID_PRES"); 
      $this->idLocalAtendimento    = $sql->result("NNUMEPRES");       
      $this->nomeCongenere         = $sql->result("CRAZACONGE");
      $this->idCongenere           = $sql->result("NNUMECONGE");
      $this->digital               = $sql->result("CDINDGUIA");
      $this->faturada              = "N";
	    $this->justificativa1        = $sql->result("CJTOPGUIA");
	    $this->justificativa2        = $sql->result("CHIPOGUIA");
	    $this->justificativa3        = $sql->result("COBSEGUIA");      
      $this->peso                  = $sql->result("NPESOGUIA");
      $this->altura                = $sql->result("NALTUGUIA");
      $this->superficieCorporal    = $sql->result("NSUPCGUIA");
      $this->idade                 = $sql->result("IDADE");
      $this->sexo                  = $sql->result("CSEXOUSUA");      
      $this->codigoContrato        = $sql->result("CCODITITU");      
      $this->codigoPlano           = $sql->result("CCODIPLAN");      
      $this->descricaoPlano        = $sql->result("CDESCPLAN");     
      $this->previsaoOpme          = $sql->result("CPUOPGUIA");
      $this->previsaoQuimio        = $sql->result("CPUQMGUIA");
      $this->dataSugeridaInternacao= $sql->result("DINTEGUIA");
  
      if($sql->result("CLOCAGUIA") == '7'){
        if ($sql->result("NNUMEMOTLB") <> ''){
          $this->autenticaBiometria = 'Biometria isentada';
        }else{
          $this->autenticaBiometria = 'Biometria autenticada';
        }  
      }else{
         $this->autenticaBiometria = ' ';
      }
            
      $sql_faturada = new Query($this->bd);  
      $txt_faturada = "SELECT HSSGCON.NNUMECONT
                         FROM HSSGCON
                        WHERE HSSGCON.NNUMEGUIA = :guia";                    
      $sql_faturada->addParam(":guia",$this->idGuia);
      $sql_faturada->executeQuery($txt_faturada);
      
      //Alterei para buscar os dados da conta somente para Unimed Barra, caso 
      //alguem reclame, adicione o apelido aqui.
      if (($sql_faturada->count() > 0) and ($this->carregaFaturada <> 'N')) {   
        $this->conta    = new Conta($this->bd);    
        $this->conta->setIdConta($sql_faturada->result("NNUMECONT"));
        $this->conta->carregaDados();
        
        $this->numeroConta           = $this->conta->getIdConta();
        $this->idConta               = $this->conta->getIdConta();
        $this->indicadorAcidente     = $this->conta->indicadorAcidente;
        $this->dataAtendimento       = $this->conta->dataAtendimento;
        $this->tipoConsulta          = $this->conta->tipoConsulta;
        $this->tipoInternacao        = $this->conta->tipoInternacao;
        $this->regimeInternacao      = $this->conta->regimeInternacao;
        $this->cidObito              = $this->conta->codigoCidObito;
        $this->declaracaoNascidoVivo = $this->conta->declaracaoNascido;
        $this->motivoSaidaInternacao = $this->conta->motivoSaidaTiss;  
        $this->declaracaoObito       = $this->conta->declaracaoObito;
        $this->tipoAtendimento       = $this->conta->tipoAtendimento;
        $this->tipoSaida             = $this->conta->tipoSaida;
        $this->idPrestador           = $this->conta->idPrestador;
        $this->faturada              = "S";      
      }
      else
        $this->idConta               = 0;
      
      //Guia Quimioterapia e Radioterapia

      $sql_quimio = new Query($this->bd);
      $txt_quimio = "SELECT HSSONCO.CTIPOONCO,HSSONCO.CESTAONCO,HSSONCO.CTIPQONCO,HSSONCO.CFINAONCO,HSSONCO.CECOGONCO,HSSONCO.CPLATONCO,
                            HSSONCO.CDIAGONCO,HSSONCO.CINFOONCO,HSSONCO.CCIRUONCO,TO_CHAR(HSSONCO.DCIRUONCO,'DD/MM/YYYY') DCIRUONCO,
                            HSSONCO.CTEXTONCO,TO_CHAR(HSSONCO.DAPLIONCO,'DD/MM/YYYY') DAPLIONCO,HSSONCO.NCICPONCO,HSSONCO.NCICAONCO,
                            HSSONCO.NINTCONCO,HSSONCO.CDIAIONCO,TO_CHAR(HSSONCO.DPREVONCO,'DD/MM/YYYY') DPREVONCO,
                            TO_CHAR(HSSONCO.DDIAGONCO,'DD/MM/YYYY') DDIAGONCO,HSSONCO.NCAMPONCO,HSSONCO.NDOSDONCO,HSSONCO.NDIASONCO,HSSONCO.NDOSTONCO,
                            HSSONCO.CNSOLONCO,HSSONCO.CFSOLONCO,HSSONCO.CESOLONCO
                       FROM HSSONCO
                      WHERE HSSONCO.NNUMEGUIA = :guia";
      $sql_quimio->addParam(":guia",$this->idGuia);    
      $sql_quimio->executeQuery($txt_quimio);

      $this->tipoOnco                  = $sql_quimio->result("CTIPOONCO");
      $this->estadiamento              = $sql_quimio->result("CESTAONCO");
      $this->tipoQuimioterapia         = $sql_quimio->result("CTIPQONCO");
      $this->finalidade                = $sql_quimio->result("CFINAONCO");
      $this->ecog                      = $sql_quimio->result("CECOGONCO");
      $this->planoTerapeutico          = $sql_quimio->result("CPLATONCO");
      $this->diagnosticoCitoHisto      = $sql_quimio->result("CDIAGONCO");
      $this->informacoesRelevantes     = $sql_quimio->result("CINFOONCO");
      $this->cirurgia                  = $sql_quimio->result("CCIRUONCO");
      $this->dataCirurgia              = $sql_quimio->result("DCIRUONCO");
      $this->areaIrradiada             = $sql_quimio->result("CTEXTONCO");
      $this->dataAplicacao             = $sql_quimio->result("DAPLIONCO");
      $this->numeroCiclos              = $sql_quimio->result("NCICPONCO");
      $this->cicloAtual                = $sql_quimio->result("NCICAONCO");
      $this->intervaloCiclos           = $sql_quimio->result("NINTCONCO");
      $this->diagnosticoImagem         = $sql_quimio->result("CDIAIONCO");
      $this->numeroCampos              = $sql_quimio->result("NCAMPONCO");
      $this->doseDiaria                = $sql_quimio->result("NDOSDONCO");
      $this->doseTotal                 = $sql_quimio->result("NDOSTONCO");
      $this->numeroDias                = $sql_quimio->result("NDIASONCO");
      $this->dataPrevistaAdm           = $sql_quimio->result("DPREVONCO"); 
      $this->dataDiagnostico           = $sql_quimio->result("DDIAGONCO"); 
      $this->nomeSoliOnco              = $sql_quimio->result("CNSOLONCO"); 
      $this->telefoneSoliOnco          = $sql_quimio->result("CFSOLONCO"); 
      $this->emailSoliOnco             = $sql_quimio->result("CESOLONCO"); 
      
      $this->adicionaProcedimentos();    
      $this->adicionaDespesas();
      $this->adicionaOPMEs();
      $this->contaDiarias();    
      $this->cids();    
      $this->calculaTotais();    
       
      $sql_ende = new Query($this->bd);
      $txt_ende = "SELECT HSSGUIA.NNUMEEPRES, CENDEEPRES, CFONEEPRES, CCIDAEPRES, CESTAEPRES, CCEP_EPRES
                     FROM HSSEPRES,HSSGUIA
                    WHERE HSSEPRES.NNUMEPRES = :local
                      AND HSSGUIA.NNUMEGUIA = :guia
                      AND HSSGUIA.NNUMEEPRES = HSSEPRES.NNUMEEPRES ";
      $sql_ende->addParam(":local",$sql->result("NNUMEPRES"));
      $sql_ende->addParam(":guia",$sql->result("NNUMEGUIA"));
      $sql_ende->executeQuery($txt_ende);
      
      $this->observacao = '';

      if ($this->func->parametroString($this->bd,'CIOAG_GUIA') == 'I') {
        if ($sql->result("COBSEGUIA") <> '')
          $this->observacao .= $sql->result("COBSEGUIA");
      }
	    else if ($_SESSION['apelido_operadora'] == 'nossasaude'){
        $this->observacao .= $sql->result("COBSEGUIA").
                             " Dados Contratado: ".$this->func->enderecoPrestador($this->bd,$this->idLocalAtendimento).
                             " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA").
                             " Empresa / Titular: ".$sql->result("TITULAR");
      }
	    else if (($_SESSION['apelido_operadora'] == 'multiSaude') or
               ($_SESSION['apelido_operadora'] == 'samedh') or
               ($_SESSION['apelido_operadora'] == 'sinamed')) {
                
        if ($sql->result("NNUMEUSUA") <> '')
          $this->observacao .= "Empresa / Titular: ".$this->func->retornaNomeEmpresa($this->bd,$sql->result("NNUMEEMPR"),"S")." / ".
                                                     $this->func->retornaNomeLocacao($this->bd,$sql->result("NNUMESETOR"),"S");
        else if ($sql->result("NNUMENUSUA") <> '')
          $this->observacao .= " Conv�nio: ".$this->func->Retorna_Convenio_NaoUsuario($this->bd,$sql->result("NNUMENUSUA"));
        else
          $this->observacao .= " Conv�nio: GUIA DE CORTESIA";

        if ($sql->result("MODALIDADE") == '2') {
          if ($sql_ende->result("NNUMEEPRES") == '')
            $this->observacao .= " Telefone: ".$sql->result("CFONEPRES")." /".
                                 " Contato: ".$sql->result("CCON1PRES")." /".
                                 " Carteirinha: ".$sql->result("CCARTUSUA")." /".
                                 " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                                 $sql->result("COBSEGUIA");
          else
            $this->observacao .= " Telefone: ".$sql_ende->result("CFONEEPRES")." /".
                                 " Contato: ".$sql->result("CCON1PRES")." /".
                                 " Carteirinha: ".$sql->result("CCARTUSUA")." /".
                                 " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                                 $sql->result("COBSEGUIA");
        }
        else {
          if ($sql_ende->result("NNUMEEPRES") == '')
            $this->observacao .= " Telefone: ".$sql->result("CFONEPRES")." /".
                                 " Contato: ".$sql->result("CCON1PRES")." /".
                                 " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                                 $sql->result("COBSEGUIA");
          else
            $this->observacao .= " Telefone: ".$sql_ende->result("CFONEEPRES")." /".
                                 " Contato: ".$sql->result("CCON1PRES")." /".
                                 " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                                 $sql->result("COBSEGUIA");
        }
      }
      else if ($_SESSION['apelido_operadora'] == 'sampes'){
        $this->observacao .= ' Local de atendimento: '.$sql->result("LOCAL_ATENDIMENTO").
                             ' Telefone: '.$sql->result("CFONEPRES").
                             ' Empresa / Titular: '.$sql->result("TITULAR");
      }
	    else if ($_SESSION['apelido_operadora'] == 'unimedFranca'){
        $this->observacao .= " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                             " Empresa/Titular: ".$sql->result("TITULAR");
	    }      
      else if (($_SESSION['apelido_operadora'] == 'coamo') && ($this->copart > 0)) {
        $this->observacao .= 'Autorizo o d�bito no valor de R$: '.$this->formata->formataNumero($this->copart).' em folha de pagamento, referente a esta guia.';
      
      }
      else if (($_SESSION['apelido_operadora'] == 'apasBotucatu')) {
        if ($sql->result("CCREDPRES") == 'O')
          $this->observacao .= " Caso n�o utilize em 10 dias, favor realizar devolu��o da mesma. /".
                               " Codigo Unimed: ".$sql->result("CUNIMUSUA");
        else
          $this->observacao .= " Caso n�o utilize em 10 dias, favor realizar devolu��o da mesma.";

        $this->observacao .= "Telefone Prestador: ".$sql->result("CFONEPRES")." /".
                             " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                             $sql->result("COBSEGUIA");      
      }
      else if ($_SESSION['apelido_operadora'] == 'apasDracena'){
        $this->observacao .= "Empresa / Titular: ".$sql->result("TITULAR").
                             " Telefone Contratado: ".$sql_ende->result("CFONEEPRES")." /".
                             " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                             " Matricula/RE: ".$sql->result("MATRICULA").
                             $sql->result("COBSEGUIA");      
      }
      else if ($_SESSION['apelido_operadora'] == 'apasJau'){
        $this->observacao .= "Telefone: ".$sql->result("CFONEPRES")." /".
                             " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                             $sql->result("COBSEGUIA").      
                             " Empresa / Titular: ".$sql->result("TITULAR");
      }
      else if ($_SESSION['apelido_operadora'] == 'apasAndradina'){
        $endereco = array();
        $endereco = $this->func->enderecoUsuario($this->bd,$sql->result("NNUMENUSUA"));
        
        $this->observacao .= "Empresa / Titular: ".$sql->result("TITULAR").
                             " Matricula/RE: ".$sql->result("MATRICULA").        
                             " Endere�o do usu�rio: ".$endereco['logradouro']."-".$endereco['bairro'];
                             " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                             $sql->result("COBSEGUIA");
      }
      else if ($_SESSION['apelido_operadora'] == 'cabefi') {
        $this->observacao .= "Telefone Executante: ".$sql->result("FONE_PRES")." /".
                             " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /".
                             $sql->result("COBSEGUIA").      
                             " Empresa / Titular: ".$sql->result("TITULAR");
      
      }
      else {
        if ($this->func->parametroString($this->bd,'CIOAG_GUIA') == 'S') {
          if ($sql_ende->result("NNUMEEPRES") == '')
            $this->observacao .= ' Telefone Contratado: ' .$sql->result("CFONEPRES").  ' /';
          else
            $this->observacao .= ' Telefone Contratado: ' .$sql_ende->result("CFONEEPRES"). ' /';

          $this->observacao .= " Emitido Por: ".$sql->result("OPERADOR"). " em ".$sql->result("DATA_GUIA")." /";
        }
        if ($this->func->parametroString($this->bd,'CMOSTELIDA') == 'S') 
          $this->observacao .= " Benefici�rio: ".$this->func->retornaTelefoneUsuario($this->bd,$sql->result("NNUMEUSUA")).'-'.$sql->result("IDADE").' anos / ';          
          
        if ($sql->result("COBSEGUIA") <> '')
          $this->observacao .= $sql->result("COBSEGUIA").' /';

        if ($_SESSION['apelido_operadora'] == 'apasSorocaba')
          $this->observacao .= " Endere�o Contratado: ".$this->func->enderecoPrestador($this->bd,$this->idLocalAtendimento);          

        if (($_SESSION['apelido_operadora'] == 'apasSorocaba') or ($_SESSION['apelido_operadora'] == 'freelife'))
          $this->observacao .= " Contrato / Plano: ".$this->codigoContrato." - ".$this->codigoPlano." - ".$this->descricaoPlano;          
          
        if (($this->func->tipoGuia($this->bd,$sql->result("NNUMEGUIA")) == 'C') and ($_SESSION['apelido_operadora'] == 'apasSorocaba')) 
          $this->observacao .= " Data de Validade da Senha: " .$sql->result("DVALIGUIA");

        $this->observacao .= " Empresa / Titular: ".$sql->result("TITULAR");
      }

      if ($this->copartPrestador > 0){
        $this->observacao = 'Co-participa��o a ser paga no prestador (R$): '.$this->formata->formataNumero($this->copartPrestador);
      }
      else if (($_SESSION['apelido_operadora'] == 'affego') && ($this->copart > 0)) {
        $this->observacao = 'O VALOR DA CO-PARTICIPA��O SER� DESCONTADO NA MENSALIDADE DO USU�RIO TITULAR';
      }     
    }
    else {
      // Em branco
      $this->numeroProcedimentos = 0;
      $this->numeroEquipe        = 0;
      $this->numeroDespesa       = 0;
      $this->numeroOPME          = 0;
      $this->idConta             = 0;
    }
  }  
  
  private function adicionaProcedimentos() {
    $func = new Funcao();
    $sql_proc = new Query($this->bd);
    
    if ($this->faturada == 'S') {
    
      $txt_proc = "SELECT CODIGOTABELAPRESTADORWEB30(NNUMEPRES,HSSPCON.CCODIPMED) TABELA,CNOMEPMED DESCRICAO,
                          HSSPCON.CCODIPMED CODIGO,CHINIPCON HORA_INI,CHFIMPCON HORA_FIM,
                          TO_CHAR(HSSPCON.DREALPCON,'DD/MM/YYYY') DATA,HSSPCON.NQUANPCON QUANTIDADE,HSSPCON.NVIA_PCON VIA,HSSPCON.CVIDEPCON VIDEO,
                          TOTAL_CONTA_HONORARIOS_PCON(HSSPCON.NNUMEPCON) VALOR_UNITARIO,TOTAL_CONTA_HONORARIOS_PCON(HSSPCON.NNUMEPCON) TOTAL,
                          HSSPCON.NNUMEPRES CIRURGIAO,HSSPCON.NANESPRES ANESTESISTA,HSSPCON.NPAUXPRES PRIMEIRO,
                          HSSPCON.NSAUXPRES SEGUNDO,HSSPCON.NTAUXPRES TERCEIRO,HSSPCON.NQAUXPRES QUARTO,HSSPCON.NINSTPRES INSTRUMENTADOR,HSSPCON.CGRAUPCON GRAU
                     FROM HSSPCON,HSSPMED
                    WHERE HSSPCON.NNUMECONT = :conta
                      AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                    ORDER BY HSSPCON.CCODIPMED, HSSPCON.NNUMEPCON";
      $sql_proc->addParam(":conta",$this->numeroConta);
    }
    else {
      $txt_proc = "SELECT CODIGOTABELAPRESTADORWEB30(:idPrestador,HSSPGUI.CCODIPMED) TABELA,HSSPMED.CNOMEPMED DESCRICAO,
                          HSSPMED.CCODIPMED CODIGO,TO_CHAR(TO_DATE(:emissao,'DD/MM/YYYY HH24:mi'),'HH24:mi') HORA_INI,
                          TO_CHAR(TO_DATE(:emissao,'DD/MM/YYYY HH24:mi'),'HH24:mi') HORA_FIM,
                          TO_CHAR(TO_DATE(:emissao,'DD/MM/YYYY HH24:mi'),'DD/MM/YYYY') DATA,HSSPGUI.NQUANPGUI QUANTIDADE,HSSPGUI.NQSOLPGUI QSOL,'' VIA,HSSPGUI.CVIDEPGUI VIDEO,
                          (NQINDPGUI/DECODE(NQUANPGUI,NULL,1,0,1,NQUANPGUI)) VALOR_UNITARIO,NQINDPGUI TOTAL,
                          0 CIRURGIAO,0 ANESTESISTA,0 PRIMEIRO,
                          0 SEGUNDO,0 TERCEIRO,0 QUARTO,0 INSTRUMENTADOR,DECODE(HSSPMED.CPARTPMED,NULL,'N','S') CPARTPMED
                     FROM HSSPGUI,HSSPMED
                    WHERE HSSPGUI.NNUMEGUIA = :guia
                      AND HSSPGUI.CSTATPGUI IS NULL
                      AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                    UNION ALL
                   SELECT '98' TABELA,HSSPACOT.CDESCPACOT DESCRICAO,
                          HSSPACOT.CCODIPACOT CODIGO,TO_CHAR(TO_DATE(:emissao,'DD/MM/YYYY HH24:mi'),'HH24:mi') HORA_INI,
                          TO_CHAR(TO_DATE(:emissao,'DD/MM/YYYY HH24:mi'),'HH24:mi') HORA_FIM,
                          TO_CHAR(TO_DATE(:emissao,'DD/MM/YYYY HH24:mi'),'DD/MM/YYYY') DATA,HSSPCGUI.NQUANPCGUI QUANTIDADE,0 QSOL,'' VIA,'' VIDEO,
                          (NVALOPCGUI/DECODE(NQUANPCGUI,NULL,1,0,1,NQUANPCGUI)) VALOR_UNITARIO,NVALOPCGUI TOTAL,
                          0 CIRURGIAO,0 ANESTESISTA,0 PRIMEIRO,
                          0 SEGUNDO,0 TERCEIRO,0 QUARTO,0 INSTRUMENTADOR,'N' CPARTPMED          
                     FROM HSSPCGUI,HSSPACOT
                    WHERE HSSPCGUI.NNUMEGUIA = :guia
                      AND HSSPCGUI.CSTATPCGUI IS NULL
                      AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT";
      $sql_proc->addParam(":guia",$this->idGuia);
      $sql_proc->addParam(":emissao",$this->dataEmissao);
      $sql_proc->addParam(":idPrestador",$this->idPrestador);                      
    }
    
    $sql_proc->executeQuery($txt_proc);
       
    $i            = 0;
    $this->numeroEquipe = 0;
    $this->numeroProcedimentos = 0;    
    
    while (!$sql_proc->eof()) {
      $this->procedimentos[$i]['tabela']      = $sql_proc->result("TABELA");
      $this->procedimentos[$i]['codigo']      = $sql_proc->result("CODIGO");
      $this->procedimentos[$i]['data']        = $sql_proc->result("DATA");
      $this->procedimentos[$i]['horaInicial'] = $sql_proc->result("HORA_INI");
      $this->procedimentos[$i]['horaFinal']   = $sql_proc->result("HORA_FIM");
      $this->procedimentos[$i]['via']         = $sql_proc->result("VIA");
      $this->procedimentos[$i]['video']       = $sql_proc->result("VIDEO");
      $this->procedimentos[$i]['qtSo']        = $sql_proc->result("QSOL");
      $this->procedimentos[$i]['qtde']        = $sql_proc->result("QUANTIDADE"); 
      $this->procedimentos[$i]['descricao']   = $sql_proc->result("DESCRICAO");
      $this->procedimentos[$i]['acres']       = "|__|__|__|,|__|__|";
      
      if ($_SESSION['mostra_valor_procedimentos'] == "S") {    
        $this->procedimentos[$i]['unitario']    = $this->formata->formataNumero($sql_proc->result("VALOR_UNITARIO")); 
        $this->procedimentos[$i]['total']       = $this->formata->formataNumero($sql_proc->result("TOTAL")); 
      }
      else {
        $this->procedimentos[$i]['unitario']    = "|__|__|__|__|,|__|__|";
        $this->procedimentos[$i]['total']       = "|__|__|__|__|,|__|__|";    
      }
      
      $this->procedimentos[$i]['parto']         = $sql_proc->result("CPARTPMED");            
      
      $porte = array();
      $porte = $func->retornaPorteAnestesicoTabela ($this->bd,
                                                    $this->idPrestador,
                                                    $this->idCongenere,
                                                    $sql_proc->result("CODIGO"),
                                                    $this->dataEmissao);
                                                    
      $this->procedimentos[$i]['porte']         = $porte['porte'];
      $this->procedimentos[$i]['auxiliares']    = $porte['auxiliares'];
                                                    
                                                         
      if ($_SESSION['impressaoGuiaInternacao'] == '1') {
        if (!in_array('(Via HOSPITAL)',$this->profissionalVia))
          $this->profissionalVia[] = '(Via HOSPITAL)';
      }
      else {
        if (!in_array('(Via HOSPITAL)',$this->profissionalVia))
          $this->profissionalVia[] = '(Via HOSPITAL)';

        if (!in_array('(Via M�DICO)',$this->profissionalVia))
          $this->profissionalVia[] = '(Via M�DICO)';

        if ($porte['porte'] > 0)
          if (!in_array('(Via ANESTESISTA)',$this->profissionalVia))
            $this->profissionalVia[] = '(Via ANESTESISTA)';
          
        for ($a = 1;$a <= $porte['auxiliares'];$a++) {
        
          if (($a == 1) and (!in_array('(Via 1� AUXILIAR)',$this->profissionalVia)))
            $this->profissionalVia[] = '(Via 1� AUXILIAR)';
        
          if (($a == 2) and (!in_array('(Via 2� AUXILIAR)',$this->profissionalVia)))
            $this->profissionalVia[] = '(Via 2� AUXILIAR)';

          if (($a == 3) and (!in_array('(Via 3� AUXILIAR)',$this->profissionalVia)))
            $this->profissionalVia[] = '(Via 3� AUXILIAR)';

          if (($a == 4) and (!in_array('(Via 4� AUXILIAR)',$this->profissionalVia)))
            $this->profissionalVia[] = '(Via 4� AUXILIAR)';
        }

        if ($sql_proc->result("CPARTPMED") == 'S')
          if (!in_array('(Via PEDIATRA ASSIT)',$this->profissionalVia))
            $this->profissionalVia[] = '(Via PEDIATRA ASSIT)';
      } 
      
      /** Cirurgiao **/
      if($this->faturada == 'S')
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("CIRURGIAO"),$i,$sql_proc->result("GRAU"));
      else
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("CIRURGIAO"),$i,"00");
      $this->numeroEquipe++;
      
      /** Anestesista **/
      if ($sql_proc->result("ANESTESISTA") > 0) {
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("ANESTESISTA"),$i,"06");
        $this->numeroEquipe++;
      }
      
      /** Primeiro Auxilio **/
      if ($sql_proc->result("PRIMEIRO") > 0) {
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("PRIMEIRO"),$i,"01");
        $this->numeroEquipe++;   
      }  

      /** Segundo Auxilio **/
      if ($sql_proc->result("SEGUNDO") > 0) {
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("SEGUNDO"),$i,"02");
        $this->numeroEquipe++;   
      } 

      /** Terceiro Auxilio **/
      if ($sql_proc->result("TERCEIRO") > 0) {
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("TERCEIRO"),$i,"03");
        $this->numeroEquipe++;   
      } 

      /** Quarto Auxilio **/
      if ($sql_proc->result("QUARTO") > 0) {
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("QUARTO"),$i,"04");
        $this->numeroEquipe++;   
      }       
      
      /** Instrumentador **/
      if ($sql_proc->result("INSTRUMENTADOR") > 0) {
        $this->equipe[$this->numeroEquipe] = $this->adicionaEquipe($sql_proc->result("INSTRUMENTADOR"),$i,"05");
        $this->numeroEquipe++;   
      }         
        
      //$this->totalGeral += str_replace(',','.',$sql_proc->result("TOTAL"));        
      $this->numeroProcedimentos++;
      $sql_proc->next();
      $i++;
    }         
  }
  
  private function adicionaDespesas() {
  
    /** Taxas, Materiais e Medicamentos **/
    $sql_desp = new Query($this->bd);
    
    if ($this->faturada == 'S') {
      $txt_desp = "SELECT DECODE(HSSTAXA.CTIPOTAXA,'D','5','C','5','G','1','7') CODIGO_DESPESA,
                          '18' TABELA,NVL(HSSTAXA.CTUSSTAXA,HSSTAXA.CCODITAXA) CODIGO_TISS, NFRANTCON,0 NCOPRPGUI,HSSTAXA.CDESCTAXA DESCRICAO,NQUANTCON QTDE,NVALOTCON VALOR_UNITARIO,
                          (NVALOTCON * NQUANTCON) VALOR_TOTAL,'UN' UNIDADE,' ' ANVISA,DATENCONT
                     FROM HSSTCON,HSSTAXA,HSSCONT
                    WHERE HSSTCON.NNUMECONT = :conta
                      AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                      AND HSSTCON.NNUMECONT = HSSCONT.NNUMECONT
                    UNION ALL
                   SELECT DECODE(CTIPOPRODU,'M','2','3') CODIGO_DESPESA,
                          ESTTABMM.CCODITABMM TABELA,RETORNA_CODIGO_MATERIAL(HSSMCON.NNUMEPRODU,:vigencia,CVERTCONF) CODIGO_TISS,NFRANMCON,0,RETORNA_NOME_MATERIAL(HSSMCON.NNUMEPRODU,SYSDATE,CVERTCONF) DESCRICAO,NPQUAMCON QTDE,TRUNC((NAVALMCON),2) VALOR_UNITARIO,
                          TRUNC(NAVALMCON*NPQUAMCON,2) VALOR_TOTAL,ESTUNIDA.CANS_UNIDA UNIDADE,CRANVPRODU ANVISA,DDATAMCON
                     FROM HSSMCON,ESTPRODU,ESTTABMM,ESTUNIDA,HSSCONF
                    WHERE HSSMCON.NNUMECONT = :conta
                      AND HSSMCON.NNUMEPRODU = ESTPRODU.NNUMEPRODU
                      AND HSSMCON.NNUMETABMM = ESTTABMM.NNUMETABMM(+)
                      AND ESTPRODU.NNUMEUNIDA = ESTUNIDA.NNUMEUNIDA(+)
                    ORDER BY 1"; 
      $sql_desp->addParam(":conta",$this->numeroConta);    
      $sql_desp->addParam(":vigencia",substr($this->dataEmissao,0,10));
    }
    else {    
      $txt_desp = "SELECT DECODE(HSSTAXA.CTIPOTAXA,'D','5','C','5','G','1','7') CODIGO_DESPESA,
                          '18' TABELA,NVL(HSSTAXA.CTUSSTAXA,HSSTAXA.CCODITAXA) CODIGO_TISS, NCOPADGUI,0 NCOPRPGUI,HSSTAXA.CDESCTAXA DESCRICAO,NQUANDGUI QTDE,CASE WHEN NQUANDGUI = 0 THEN 0 ELSE (NVALODGUI/NQUANDGUI) END VALOR_UNITARIO,
                          NVALODGUI VALOR_TOTAL,'UN' UNIDADE,' ' ANVISA,NQSOLDGUI QTDSO,
                          ' ' CANS_VIAME,NULL DATAADM,NULL DOSE, NULL FREQUENCIA, 0 ORDEM,'T' TIPO
                     FROM HSSDGUI,HSSTAXA
                    WHERE HSSDGUI.NNUMEGUIA = :guia
                      AND HSSDGUI.CSTATDGUI IS NULL
                      AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                    UNION ALL
                   SELECT DECODE(CTIPOPRODU,'M','2','3') CODIGO_DESPESA,
                          ESTTABMM.CCODITABMM TABELA,RETORNA_CODIGO_MATERIAL(HSSMMGUI.NNUMEPRODU,:vigencia,CVERTCONF) CODIGO_TISS,NCOPAMMGUI,0,RETORNA_NOME_MATERIAL(HSSMMGUI.NNUMEPRODU,SYSDATE,CVERTCONF) DESCRICAO,NQUANMMGUI QTDE,TRUNC((NVPAGMMGUI/NQUANMMGUI),2) VALOR_UNITARIO,
                          TRUNC(NVPAGMMGUI,2) VALOR_TOTAL,ESTUNIDA.CANS_UNIDA UNIDADE,CRANVPRODU ANVISA,NQSOLMMGUI,
                          ESTVIAME.CANS_VIAME, DPREVMMGUI, NDOSEMMGUI, NFREQMMGUI, NVL(NORDEMMGUI,0) ORDEM,'M' TIPO
                     FROM HSSMMGUI,ESTPRODU,ESTTABMM,ESTUNIDA,ESTVIAME,HSSCONF
                    WHERE HSSMMGUI.NNUMEGUIA = :guia
                      AND CTIPOMMGUI = 'M'
                      AND NVL(CSTATMMGUI,'L') = 'L'
                      AND HSSMMGUI.NNUMEPRODU = ESTPRODU.NNUMEPRODU
                      AND HSSMMGUI.NNUMETABMM = ESTTABMM.NNUMETABMM(+)
                      AND ESTPRODU.NNUMEUNIDA = ESTUNIDA.NNUMEUNIDA(+)
                      AND HSSMMGUI.NNUMEVIAME = ESTVIAME.NNUMEVIAME(+)
                    ORDER BY ORDEM, DATAADM, CODIGO_TISS"; 
      $sql_desp->addParam(":guia",$this->idGuia);
      $sql_desp->addParam(":vigencia",substr($this->dataEmissao,0,10));
    }
    $sql_desp->executeQuery($txt_desp); 

    $i                   = 0;
    $this->numeroDespesa = 0;
    
    while (!$sql_desp->eof()) {

      if ($_SESSION['impressaoGuiaInternacao'] == '1') {
        if (!in_array('(Via HOSPITAL)',$this->profissionalVia))
          $this->profissionalVia[] = '(Via HOSPITAL)';
      }
      else {
        if (!in_array('(Via HOSPITAL)',$this->profissionalVia))
          $this->profissionalVia[] = '(Via HOSPITAL)';
      }
      
      if (($this->tipoOnco == 'Q') and ($sql_desp->result("TIPO") <> 'T')) { // Guia de Quimio n�o pode imprimir taxas
        $this->quimio[$i]['dataAdm']                    = substr($sql_desp->result("DATAADM"),0,10);
        $this->quimio[$i]['tabela']                     = $sql_desp->result("TABELA"); 
        $this->quimio[$i]['codigo']                     = $sql_desp->result("CODIGO_TISS");
        $this->quimio[$i]['descricao']                  = $sql_desp->result("DESCRICAO"); 
        $this->quimio[$i]['dose']                       = $sql_desp->result("DOSE"); 
        $this->quimio[$i]['via']                        = $sql_desp->result("CANS_VIAME"); 
        $this->quimio[$i]['frequencia']                 = $sql_desp->result("FREQUENCIA");     
      }
      else {    
        $this->despesas[$i]['dataAdm']                  = substr($sql_desp->result("DATAADM"),0,10);
        $this->despesas[$i]['tipoDespesa']              = $this->formata->acrescentaZeros($sql_desp->result("CODIGO_DESPESA"),2);
        $this->despesas[$i]['data']                     = substr($this->dataEmissao,0,10);
        $this->despesas[$i]['horaInicial']              = "____:____";
        $this->despesas[$i]['horaFinal']                = "____:____";
        $this->despesas[$i]['tabela']                   = $sql_desp->result("TABELA"); 
        $this->despesas[$i]['codigo']                   = $sql_desp->result("CODIGO_TISS"); 
        $this->despesas[$i]['qtde']                     = $sql_desp->result("QTDE");
        $this->despesas[$i]['qtSo']                     = $sql_desp->result("QTDSO");
        $this->despesas[$i]['unidade']                  = $sql_desp->result("UNIDADE"); 
        $this->despesas[$i]['acres']                    = "|__|,|__|__|";
        $this->despesas[$i]['unitario']                 = $this->formata->formataNumero($sql_desp->result("VALOR_UNITARIO")); 
        $this->despesas[$i]['total']                    = $this->formata->formataNumero($sql_desp->result("VALOR_TOTAL"));
        $this->despesas[$i]['anvisa']                   = $sql_desp->result("ANVISA");
        $this->despesas[$i]['fabricante']               = "|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|";
        $this->despesas[$i]['autorizacaoFuncionamento'] = "|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|";
        $this->despesas[$i]['descricao']                = $sql_desp->result("DESCRICAO"); 
        
        if ($sql_desp->result("CODIGO_DESPESA") == '5')
          $this->totalDiarias      += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));
        else if ($sql_desp->result("CODIGO_DESPESA") == '2')
          $this->totalMedicamentos += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));
        else if ($sql_desp->result("CODIGO_DESPESA") == '3')
          $this->totalMateriais    += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));
        else if ($sql_desp->result("CODIGO_DESPESA") == '1')
          $this->totalGases        += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));
        else            
          $this->totalTaxas        += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));
          
        $this->totalAlugueis     += 0;//str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));      
        $this->totalGeral        += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));
      }
            
      $sql_desp->next();
      $this->numeroDespesa++;      
      $i++;
    }     
  
  }
  
  private function adicionaOPMEs() {
  
    /** OPME **/
    $sql_desp = new Query($this->bd);
    
    if ($this->faturada == 'S') {
      $txt_desp = "SELECT ESTTABMM.CCODITABMM TABELA,RETORNA_CODIGO_MATERIAL(HSSMMCON.NNUMEPRODU,:vigencia,CVERTCONF) CODIGO_TISS,NFRANMMCON,0,CNOMEPRODU DESCRICAO,NPQUAMMCON QTDE,TRUNC((NPVALMMCON/NPQUAMMCON),2) VALOR_UNITARIO,
                          TRUNC(NPVALMMCON,2) VALOR_TOTAL,ESTUNIDA.CANS_UNIDA UNIDADE,CRANVPRODU ANVISA,DDATAMMCON
                     FROM HSSMMCON,ESTPRODU,ESTTABMM,ESTUNIDA,HSSCONF
                    WHERE HSSMMCON.NNUMECONT = :conta
                      AND HSSMMCON.NNUMEPRODU = ESTPRODU.NNUMEPRODU
                      AND HSSMMCON.NNUMETABMM = ESTTABMM.NNUMETABMM(+)
                      AND ESTPRODU.NNUMEUNIDA = ESTUNIDA.NNUMEUNIDA(+)
                    ORDER BY 1"; 
      $sql_desp->addParam(":conta",$this->numeroConta);    
      $sql_desp->addParam(":vigencia",substr($this->dataAtendimento,0,10));      
    }
    else {    
      $txt_desp = "SELECT ESTTABMM.CCODITABMM TABELA,RETORNA_CODIGO_MATERIAL(HSSMMGUI.NNUMEPRODU,:vigencia,CVERTCONF) CODIGO_TISS,NCOPAMMGUI,0,CNOMEPRODU DESCRICAO,NQUANMMGUI QTDE,TRUNC((NVPAGMMGUI/NQUANMMGUI),2) VALOR_UNITARIO,
                          TRUNC(NVPAGMMGUI,2) VALOR_TOTAL,ESTUNIDA.CANS_UNIDA UNIDADE,CRANVPRODU ANVISA
                     FROM HSSMMGUI,ESTPRODU,ESTTABMM,ESTUNIDA,HSSCONF
                    WHERE HSSMMGUI.NNUMEGUIA = :guia
                      AND CTIPOMMGUI = 'O'
                      AND NVL(CSTATMMGUI,'L') = 'L'
                      AND HSSMMGUI.NNUMEPRODU = ESTPRODU.NNUMEPRODU
                      AND HSSMMGUI.NNUMETABMM = ESTTABMM.NNUMETABMM(+)
                      AND ESTPRODU.NNUMEUNIDA = ESTUNIDA.NNUMEUNIDA(+)"; 
      $sql_desp->addParam(":guia",$this->idGuia);
      $sql_desp->addParam(":vigencia",substr($this->dataEmissao,0,10));      
    }
    $sql_desp->executeQuery($txt_desp); 

    $i                   = 0;
    $this->numeroOPME = 0;
    
    while (!$sql_desp->eof()) {    
      $this->opmes[$i]['tabela']                   = $sql_desp->result("TABELA"); 
      $this->opmes[$i]['codigo']                   = $sql_desp->result("CODIGO_TISS"); 
      $this->opmes[$i]['descricao']                = $sql_desp->result("DESCRICAO"); 
      $this->opmes[$i]['opcao']                    = "|__|"; 
                      
      if ($this->seg->permissaoOutros($this->bd,'WEBPRESTADORMOSTRARVALORESDEOPMENAGUIA',false)) {
      
        if ($_SESSION['apelido_operadora'] == 'sindifiscope') {
          $this->opmes[$i]['qtdeSol']                  = "|__|__|__|"; 
          $this->opmes[$i]['valorSol']                 = "|__|__|__|__|,|__|__|";		
        }
        else {
          $this->opmes[$i]['qtdeSol']                  = $sql_desp->result("QTDE"); 
          $this->opmes[$i]['valorSol']                 = $this->formata->formataNumero($sql_desp->result("VALOR_UNITARIO")); 
        }
    
        $this->opmes[$i]['qtdeSol']                  = $sql_desp->result("QTDE"); 
        $this->opmes[$i]['valorSol']                 = $this->formata->formataNumero($sql_desp->result("VALOR_UNITARIO")); 
        $this->opmes[$i]['qtdeAut']                  = $sql_desp->result("QTDE"); 
        $this->opmes[$i]['valorAut']                 = $this->formata->formataNumero($sql_desp->result("VALOR_UNITARIO")); 
      } 
      else {
        $this->opmes[$i]['qtdeSol']                  = "|__|__|__|"; 
        $this->opmes[$i]['valorSol']                 = "|__|__|__|__|,|__|__|";
        $this->opmes[$i]['qtdeAut']                  = "|__|__|__|";
        $this->opmes[$i]['valorAut']                 = "|__|__|__|__|,|__|__|";      
      }
      
      $this->opmes[$i]['anvisa']                   = $sql_desp->result("ANVISA");
      $this->opmes[$i]['fabricante']               = "|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|";
      $this->opmes[$i]['autorizacaoFuncionamento'] = "|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|";
        
      $this->totalOPMEs += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));        
      $this->totalGeral += str_replace(',','.',$sql_desp->result("VALOR_TOTAL"));        
        
      $sql_desp->next();
      $this->numeroOPME++;      
      $i++;
    }     
  
  }  
  
  function contaDiarias() {
    $sql_diarias = new Query($this->bd);
    
    $txt_diarias = "SELECT NNUMEGUIA,SUM(DECODE(NVL(CSTATDGUI,'L'),'L',NQUANDGUI,0)) NQUANDGUI,SUM(NVL(NQSOLDGUI,NQUANDGUI)) NQSOLDGUI 
                      FROM (SELECT NNUMEGUIA,HSSDGUI.NNUMETAXA,CTIPOTAXA,NQUANDGUI,NQSOLDGUI,CSTATDGUI
                              FROM HSSDGUI, HSSTAXA
                             WHERE HSSDGUI.NNUMEGUIA = :guia
                               AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                               AND HSSTAXA.CTIPOTAXA IN ('D','C')
                             UNION ALL
                            SELECT NNUMEGUIA,HSSTPACO.NNUMETAXA,CTIPOTAXA,NQUANTPACO,NQUANTPACO,CSTATPCGUI
                              FROM HSSPCGUI,HSSTPACO,HSSTAXA
                             WHERE NNUMEGUIA = :guia
                               AND HSSPCGUI.NNUMEPACOT = HSSTPACO.NNUMEPACOT
                               AND HSSTPACO.NNUMETAXA = HSSTAXA.NNUMETAXA
                               AND HSSTAXA.CTIPOTAXA IN ('D','C'))
                     GROUP BY NNUMEGUIA ";    
    $sql_diarias->addParam(":guia",$this->idGuia); 
    $sql_diarias->executeQuery($txt_diarias);
    
    if ($sql_diarias->result("NNUMEGUIA") > 0) {
      $this->diariasSolicitadas = $sql_diarias->result("NQSOLDGUI");
      $this->diariasUtilizadas  = $sql_diarias->result("NQUANDGUI");
    }
    else {
      $this->diariasSolicitadas = 0;
      $this->diariasUtilizadas  = 0;
    }   
  }
  
  private function adicionaEquipe($id,$sequencia,$funcao) {     
    $sql_prestadores = new Query($this->bd);
    $txt_prestadores ="SELECT CCODIPRES, CNOMEPRES, NCRM_PRES,DECODE(CPESSPRES,'F',CCPF_PRES,CCGC_PRES) CCGC_PRES,
                              CCONSPRES CONSELHO, RETORNA_TISS_ESTADO(CUFCRPRES) CUFCRPRES,NNUMEPRES
                         FROM FINPRES
                        WHERE NNUMEPRES = :prestador ";
    $sql_prestadores->addParam(":prestador",$id);
    $sql_prestadores->executeQuery($txt_prestadores);

    if ($sql_prestadores->result("CONSELHO") == 'CRAS')
      $conselho = '01';
    else if ($sql_prestadores->result("CONSELHO") == 'COREN')
      $conselho = '02';
    else if ($sql_prestadores->result("CONSELHO") == 'CRF')
      $conselho = '03';
    else if ($sql_prestadores->result("CONSELHO") == 'CRFA')
      $conselho = '04';
    else if ($sql_prestadores->result("CONSELHO") == 'CREFITO')
      $conselho = '05';
    else if ($sql_prestadores->result("CONSELHO") == 'CRM')
      $conselho = '06';
    else if ($sql_prestadores->result("CONSELHO") == 'CRN')
      $conselho = '07';
    else if ($sql_prestadores->result("CONSELHO") == 'CRO')
      $conselho = '08';
    else if ($sql_prestadores->result("CONSELHO") == 'CRP')
      $conselho = '09';
    else
      $conselho = '10';  
    
    $prestador = array();
    $prestador['seq']            = $this->formata->acrescentaZeros($sequencia + 1,2);
    $prestador['grau']           = $funcao;
    $prestador['identificacao']  = $sql_prestadores->result("CCGC_PRES");
    $prestador['nome']           = $sql_prestadores->result("CNOMEPRES");
    $prestador['conselho']       = $conselho;
    $prestador['numeroConselho'] = $sql_prestadores->result("NCRM_PRES");
    $prestador['ufConselho']     = $sql_prestadores->result("CUFCRPRES");
    $prestador['cbos']           = $this->func->retornaCodigoEspecialidadePrestador($this->bd,$sql_prestadores->result("NNUMEPRES"));   
    
    return $prestador;    
  }  
  
  private function calculaTotais() {

    $sql_copart = new Query($this->bd);
  
    if ($this->faturada == 'S') {
      $txt_copart = "SELECT TOTAL_COPART_CONTA(NNUMECONT) COPART,
                            TOTAL_CONTA_PLANO(NNUMECONT) VALOR_TOTAL,
                            TOTAL_COPART_PRESTADOR_CONTA(NNUMECONT) COPART_PRES
                       FROM HSSCONT 
                      WHERE NNUMECONT = :conta ";
      $sql_copart->addParam(":conta",$this->numeroConta);  
    }
    else {
      $txt_copart = "SELECT TOTAL_COPART_GUIA(NNUMEGUIA) COPART,
                            TOTAL_GUIA(NNUMEGUIA) VALOR_TOTAL, 
                            TOTAL_COPART_GUIA(NNUMEGUIA,'1') COPART_PRES
                       FROM HSSGUIA 
                      WHERE NNUMEGUIA = :guia ";           
      $sql_copart->addParam(":guia",$this->idGuia);    
    }
    
    $sql_copart->executeQuery($txt_copart);    
    $this->copart          = $this->formata->formataNumero($sql_copart->result("COPART"));
    if ($_SESSION['mostra_valor_procedimentos'] == "S")  
      $this->valorTotalGuia  = $this->formata->formataNumero($sql_copart->result("VALOR_TOTAL"));   
    else
      $this->valorTotalGuia  = '';
    $this->copartPrestador = $this->formata->formataNumero($sql_copart->result("COPART_PRES"));              
  }
  
  private function cids() {
       
    if ($this->faturada == 'S') {
    
      /** CIDs **/    
      
      $arr = $this->conta->getCIDs();      
      for ($c = 0; $c < sizeOf($arr);$c++) {
        $item = array();      
        $item = $arr[$c];
        
        if ($c == 0)
          $this->cid1 = $item[0];
          
        if ($c == 1)
          $this->cid2 = $item[0];

        if ($c == 2)
          $this->cid3 = $item[0];

        if ($c == 3)
          $this->cid4 = $item[0];
        
      }  
    
    }
    else {
    
      $sql_cid = new Query($this->bd);
      $txt_cid = "SELECT CCODICID_, 2 ORDEM
                    FROM HSSCIDG,HSSCID_
                   WHERE HSSCIDG.NNUMEGUIA = :guia
                     AND HSSCIDG.NNUMECID_ = HSSCID_.NNUMECID_
                   UNION ALL
                  SELECT CCODICID_, 1 ORDEM
                    FROM HSSGUIA,HSSCID_
                   WHERE HSSGUIA.NNUMEGUIA = :guia
                     AND HSSGUIA.NNUMECID_ = HSSCID_.NNUMECID_
                   ORDER BY 2,1";
      $sql_cid->addParam(":guia",$this->idGuia);
      $sql_cid->executeQuery($txt_cid);
                
      $c = 0;             
      while ((!$sql_cid->eof()) and ($c < 4)) {
        if ($c == 0)
          $this->cid1 = $sql_cid->result("CCODICID_");
          
        if ($c == 1)
          $this->cid2 = $sql_cid->result("CCODICID_");

        if ($c == 2)
          $this->cid3 = $sql_cid->result("CCODICID_");

        if ($c == 3)
          $this->cid4 = $sql_cid->result("CCODICID_");
          
        $sql_cid->next();
        $c++;
      }  
    }
    
  }
}


?>