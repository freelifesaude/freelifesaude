<?php
   
  function __autoload($classe) {
  
    if ($classe <> 'FPDF')
      include_once "classes/{$classe}.class.php";
      
  }  

  /* Cria classes padr�es */   
  $seg     = new Seguranca();  
  $util    = new Util();  
  $data    = new Data();
  $formata = new Formata();
  $func    = new Funcao();  
  
  function sdebug($var, $exit = false) { 
    
    echo "\n<pre>"; 
    
    if (is_array($var) || is_object($var)) { 
      echo htmlentities(print_r($var,true),ENT_NOQUOTES,'ISO-8859-1',true); 
    } 
    elseif (is_string($var)) { 
      echo "string(" . strlen($var) . ") \"" . htmlentities($var,ENT_NOQUOTES,'ISO-8859-1',true) . "\"\n"; 
    } 
    else { 
      var_dump($var); 
    } 
    
    echo "</pre>"; 
    
    if ($exit)
      exit; 
  } 

  function debugToFile($var, $exit = false,$nome = '') { 
    
    $arquivo  = $nome.date('YmdHis');
    $ponteiro = fopen ("../erros/debug_".$arquivo.".txt", "w");    

    $erro = '';

    if (is_array($var)) {    
      foreach ($_POST as $i => $value) {
        if (is_array($value)) {
          $value2 = "array('".implode("','",$value)."')";
        }
        else
          $value2 = "'".$value."'";
          
        $erro .= "\$_POST['".$i."'] = ".$value2.";".chr(13).chr(10);
      }
    }
    else
      $erro = $var;
    /*
    if (is_array($var) || is_object($var)) { 
      $erro .= print_r($var,true); 
    } 
    elseif (is_string($var)) { 
      $erro .= $var; 
    } 
    */
    fwrite($ponteiro,$erro); 
    fclose ($ponteiro); 
    
    if ($exit)
      exit; 
  }  
  
  function gravaErro($pagina,$erro) {
    $arquivo  = date('YmdHis');
    $ponteiro = fopen ("../erros/".$arquivo.".txt", "w");
    fwrite($ponteiro,$erro); 
    fclose ($ponteiro); 
    echo "<script>window.location.href='../comum/erro.php?codigo=".$arquivo."&pagina=".$pagina."';</script>";
  }
?>