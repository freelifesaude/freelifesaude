<?php 
  require_once("../comum/autoload.php");
    
  $seg->secureSessionStart();
  
  $bd = new Oracle();
        
  if (isset($_POST['plano'])){
  
    $plano = $seg->antiInjection($_POST['plano']);
    $local = $seg->antiInjection($_POST['local']);
    $selecionado = $seg->antiInjection($_POST['rede']);
  
    if ($local == 'rede')
      $padrao = "Todas";
    else
      $padrao = "Selecione a rede";
        
    $opcao = '<option value="">'.$padrao.'</option>';  
        
    $sql = new Query($bd);
    
    if ($local == 'rede') {	  
      $txt = "SELECT NNUMEREDEA,CNOMEREDEA 
                FROM HSSREDEA 
               WHERE CSITUREDEA <> 'C' 
                 AND NNUMEREDEA IN(SELECT NNUMEREDEA FROM HSSREDPL WHERE NNUMEPLAN = :plano)		   
               ORDER BY 1";
    } 
    
    $sql->addParam(":plano",$plano);        
    $sql->executeQuery($txt);  

    if ($sql->count() > 0) {		                    
      while (!$sql->eof()) {
      
        if ($sql->count() == 1)          
          $opcao .= '<option selected value="'.$sql->result("NNUMEREDEA").'">'.$sql->result("CNOMEREDEA").'</option>'; 
        else if ($selecionado == $sql->result("NNUMEREDEA"))          
          $opcao .= '<option selected value="'.$sql->result("NNUMEREDEA").'">'.$sql->result("CNOMEREDEA").'</option>'; 
        else
          $opcao .= '<option value="'.$sql->result("NNUMEREDEA").'">'.$sql->result("CNOMEREDEA").'</option>'; 
              
        $sql->next();
      }
    }

    echo $opcao;       
  }
  else
    echo '<option value="">Todas</option>';

  $bd->close();  
?>