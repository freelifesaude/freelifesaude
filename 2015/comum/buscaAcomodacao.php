<?php
  session_start(); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  if (isset($_POST['plano'])){
  
    if ($_POST['plano'] > 0) {
      $sql = new Query($bd);
      $txt = "SELECT CCODIACOM,INITCAP(CDESCACOM) CDESCACOM,HSSPLTIT.NNUMEACOM
                FROM HSSPLTIT,HSSACOM
               WHERE HSSPLTIT.NNUMETITU = :contrato
                 AND HSSPLTIT.NNUMEPLAN = :plano
                 AND HSSPLTIT.NNUMEACOM = HSSACOM.NNUMEACOM
               UNION
              SELECT CCODIACOM,INITCAP(CDESCACOM) CDESCACOM,HSSPLAN.NNUMEACOM
                FROM HSSPLAN,HSSACOM
               WHERE HSSPLAN.NNUMEPLAN = :plano
                 AND HSSPLAN.NNUMEACOM = HSSACOM.NNUMEACOM
               UNION
              SELECT CCODIACOM,INITCAP(CDESCACOM) CDESCACOM,HSSACPLA.NNUMEACOM
                FROM HSSACPLA,HSSACOM
               WHERE HSSACPLA.NNUMEPLAN = :plano
                 AND HSSACPLA.NNUMEACOM = HSSACOM.NNUMEACOM ";             
                 
      $sql->clear();
      $sql->addParam(":contrato",$_POST['contrato']);
      $sql->addParam(":plano",$_POST['plano']);    
      $sql->executeQuery($txt);

      echo '<option value="">Selecione a acomoda&ccedil;&atilde;o</option>';     
      while (!$sql->eof()) {
        $opcao = '<option value="'.$sql->result("NNUMEACOM").'" ';
        
        if ($_POST['acomodacao'] == $sql->result("NNUMEACOM"))
          $opcao .= ' selected ';
          
        $opcao .= '>'.$sql->result("CDESCACOM").'</option>';
          
        echo $opcao;
        
        $sql->next();
      }  
    } else {
      echo '<option value="">Selecione a acomoda&ccedil;&atilde;o</option>'; 
    }
  }

  $bd->close();  
?>