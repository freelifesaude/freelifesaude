<?php   
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(230,5,$_SESSION['nome_operadora']." - ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(230,5,'EXTRATO DE ATENDIMENTOS N�O COBRADOS - ODONTOLOGIA',0,1);
      $this->SetFont('Arial','B',7);           
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(205,3,$_SESSION['logado'],0,1);      
      $this->SetFont('Arial','B',10);
      $this->Ln(3);
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    function cabecalho($cobranca) {
      $this->SetFont('Arial','B',8);
      $this->Ln(1);
      $this->SetFont('Arial','B',8);
      $this->Cell(5,3,'',0,0);   

      if (($cobranca == '2'))
        $this->Cell(18,3,'Guia',0,0);
      else
        $this->Cell(18,3,'Conta',0,0);
      
      $this->Cell(18,3,'Atendimento',0,0);
      $this->Cell(50,3,'Prestador',0,0);
      $this->Cell(10,3,'Parc.',0,0);
      $this->Cell(18,3,'C�digo',0,0);
      $this->Cell(85,3,'Item',0,0);
      $this->Cell(15,3,'Dente',0,0);
      $this->Cell(15,3,'Face',0,0);
      $this->Cell(15,3,'Co-part',0,1);
      $this->Cell(18,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(60,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(90,3,'',0,0);
      $this->Cell(10,3,'',0,0);
      $this->Cell(15,3,'',0,0);
      $this->Cell(12,3,'',0,0);
      $this->Ln(1);  
      $this->SetFont('Arial','',8);      
    }    
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  $usuario = "";
  $titular = "";
  $tipo = "";
  $conta = 0;
  $total_geral = 0;
  $desp_usuario = 0;
  $total_usuario = 0;
  $total_copart_titular = 0;
  $total_copart_titular_prestador = 0;
  $total_titular_reciprocidade = 0;
  $total_copart_usuario = 0;
  $total_copart_prestador = 0;
  $total_reciprocidade = 0;

  require_once("../comum/conexaoUniodonto.php");  
  
  $pdf->SetFont('Arial','B',8);  
  $pdf->Cell(15,3,'UTILIZA��O ODONTOLOGIA',0,0);  
  $pdf->Ln(6);  
  
  $sql = new Query();
  $txt = "SELECT OUTROS.CCODIUSUA,OUTROS.CNOMEUSUA,LAST_DAY(SYSDATE) DATA_BASE
            FROM HSSUSUA,HSSUSUA OUTROS
           WHERE (HSSUSUA.CCODIUSUA = :codigo OR HSSUSUA.CCARTUSUA = :codigo)
             AND HSSUSUA.NNUMETITU = :id_contrato
             AND HSSUSUA.NTITUUSUA = OUTROS.NTITUUSUA ";

  $codigo2 = $codigo;
  $sql->addParam(":codigo",$codigo2);
  $sql->addParam(":id_contrato",$_SESSION['id_contrato']); 

  $sql->executeQuery($txt);
  
  $qtde = 0;
  while (!$sql->eof()) {
  
  //98001718930000100342
  
    $sql_odonto = new PostGreQuery();
    $txt_odonto = "SELECT CONTROLE,
                          COUNT(*) PARCELAS,
                          SUM(VALOR_USUARIO_REAIS) VALOR_USUARIO,
                          SUM(VALOR_EMPRESA_REAIS) VALOR_EMPRESA,
                          VALOR_PARCELA_REAIS VALOR_PARCELA,
                          SUM(VALOR_PARCELA_REAIS)  VALOR_TOTAL_RESTANTE,
                          CARTAO_AGEMED
                     FROM V_AGEMED_FICHA_PARCELAS
                    WHERE CARTAO_AGEMED LIKE ''%\".$sql->result(\"CCODIUSUA\").\"%''
                      AND MES_PARCELA >= ''\".$sql->result(\"DATA_BASE\").\"''
                    GROUP BY CONTROLE,VALOR_PARCELA_REAIS,CARTAO_AGEMED
                    UNION ALL
                   SELECT P.CONTROLE,
                          A.TOTAL_PARCELAS           PARCELAS,
                          SUM(P.VALOR_USUARIO_REAIS) VALOR_USUARIO,
                          SUM(P.VALOR_EMPRESA_REAIS) VALOR_EMPRESA,
                          SUM(P.VALOR_REAIS)              VALOR_PARCELA,
                          SUM(P.VALOR_REAIS)         VALOR_TOTAL_RESTANTE,
                          P.CARTAO_AGEMED
                     FROM V_AGEMED_PROCEDIMENTOS P,V_AGEMED_FICHAS_E_AUTORIZACOES A
                    WHERE A.CONTROLE = P.CONTROLE
                      AND NVL(A.MESREF, ''1910-01-01'') = NVL(P.MESREF, ''1910-01-01'')
                      AND P.CARTAO_AGEMED LIKE ''%\".$sql->result(\"CCODIUSUA\").\"%''
                      AND P.MESREF IS NULL
                    GROUP BY P.CONTROLE,A.TOTAL_PARCELAS,P.CARTAO_AGEMED";
                    
    $sql_odonto->executeQuery1($txt_odonto); 

    if ($sql_odonto->count1() > 0) { 
      $qtde++;
      $controle = 0;
      $cartao = '';
      $total_usuario = 0;       
      $total_copart_usuario = 0;
      
      $pdf->SetTextColor(0,64,128);
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(20,3,'Benefici�rio: ',0,0,'R');
      $pdf->Cell(100,3,$sql_odonto->result1('cartao_agemed').' - '.utf8_decode($sql->result('CNOMEUSUA')),0,0);    
      $pdf->SetTextColor(0,0,0);
      $pdf->Ln(6);    
      $pdf->SetFont('Arial','',8);
      
      while(!$sql_odonto->eof1()) {  

        //if ($controle <> $sql_odonto->result1('CONTROLE') ) {
        
          $sql_odonto_det = new PostGreQuery();
          $txt_odonto_det = "SELECT *
                               FROM V_AGEMED_FICHAS_E_AUTORIZACOES
                              WHERE CARTAO_AGEMED = '".$sql_odonto->result1("cartao_agemed")."' AND CONTROLE = ".$sql_odonto->result1("controle");
          $sql_odonto_det->executeQuery1($txt_odonto_det);
          
          $controle = $sql_odonto->result1('controle');
          $qtde_proc = 0;
          
          $pdf->Cell(50,3,'C�digo libera��o: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto_det->result1('codigo_liberacao'),0,1);
          $pdf->Cell(50,3,'Data libera��o: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto_det->result1('data_tratamento'),0,1);
          $pdf->Cell(50,3,'Data baixa: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto_det->result1('data_digitacao_ficha'),0,1);
          $pdf->Cell(50,3,'N� parcelas que falta cobrar: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto->result1('parcelas'),0,1);
          $pdf->Cell(50,3,'Valor da parcela: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto->result1('valor_parcela'),0,1);
          $pdf->Cell(50,3,'Total empresa: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto->result1('valor_empresa'),0,1);
          $pdf->Cell(50,3,'Total usu�rio: ',0,0,'R');
          $pdf->Cell(50,3,$sql_odonto->result1('valor_usuario'),0,1);
          $pdf->Ln(3);
          $pdf->Cell(20,3,'Cooperado: ',0,0,'R');
          $pdf->Cell(170,3,$sql_odonto_det->result1('codcoop'). " - ". $sql_odonto_det->result1('cooperado'),0,1);
          $pdf->Ln(3);
          $pdf->Cell(90,3,'--------------------------------------------------------------------------------',0,0,'L');          
          $pdf->Cell(10,3,'PROCEDIMENTOS',0,0,'C');          
          $pdf->Cell(90,3,'--------------------------------------------------------------------------------',0,1,'R');          
          $pdf->Ln(3);
          $pdf->Cell(15,3,'Dt. Exame',0,0,'L');          
          $pdf->Cell(10,3,'Dente',0,0,'L');          
          $pdf->Cell(10,3,'Face',0,0,'L');          
          $pdf->Cell(10,3,'USO',0,0,'L');          
          $pdf->Cell(125,3,'Servi�o',0,0,'L');          
          $pdf->Cell(20,3,'Copart',0,1,'L');          
        //}        

        $sql_item_odonto = new PostGreQuery();       
        $itens_odonto = "SELECT * FROM V_AGEMED_PROCEDIMENTOS WHERE CONTROLE = ".$sql_odonto->result1('controle'). " AND CARTAO_AGEMED = '".$sql_odonto->result1('cartao_agemed')."'";          
        $sql_item_odonto->executeQuery1($itens_odonto); 

        while(!$sql_item_odonto->eof1()) { 
           
          $pdf->Cell(15,3,$sql_item_odonto->result1('data_exame'),0,0);
          $pdf->Cell(10,3,$sql_item_odonto->result1('dente'),0,0);
          $pdf->Cell(10,3,$sql_item_odonto->result1('faces'),0,0);
          $pdf->Cell(10,3,$sql_item_odonto->result1('quantidade_uso'),0,0);
          $pdf->Cell(125,3,$pdf->Copy(utf8_decode($sql_item_odonto->result1('descricao_servico')),124),0,0);
          
          $valor = str_replace(',','.',$sql_item_odonto->result1('valor_usuario_reais'));
          $pdf->Cell(20,3,$formata->formataNumero($valor),0,1,'R');

          $qtde_proc++;
          $total_copart_usuario = $total_copart_usuario + $valor;

          $sql_item_odonto->next1();
        }
            
        $pdf->Cell(190,3,'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,0,'L');          
        
        $pdf->Ln(6);      
        
        $sql_odonto->next1();

      }
      
      $pdf->SetFont('Arial','B',8);      
      $pdf->Ln(3);      
      $pdf->Cell(234,3,'Total do utiliza��o: ',0,0,'R');
      $pdf->Cell(15,3,$formata->formataNumero($total_copart_usuario),0,0,'R');
      //$pdf->Cell(15,3,formata_numero($total_usuario),0,0,'R');
      $pdf->Ln(6); 
    }
 
    $sql->next();
  }
  
  if ($qtde == 0) {
    $tpl->CLASSE = "";
    $tpl->MSG = "N�o existe nenhum atendimento de dental sem cobran�a.";
    $tpl->block("ERRO");  
  } else {
    $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
    $pdf->Output($file,'F');
    $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";
  }

?>
