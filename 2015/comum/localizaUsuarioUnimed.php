<?php
  Session_start();
    
  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");

  $bd      = new Oracle();
  $seg     = new Seguranca();
  $util    = new Util();
  $formata = new Formata();
   
  $tpl = new Template("../comum/localizaUsuarioUnimed.html");
  $tpl->LEITORA = $_SESSION['tem_leitora'];
    
  $cadastro    = false;
  $alterar     = false;  
  $modo_verde  = "N";
  $id_contrato = 0;
  $id_locacao  = 0;
  $sexo        = '';
  $cpf         = '';    
  $nascimento  = '';
  $codigo2     = '';
  $codigo3     = '';
  $codigo4     = '';
  $via         = '';
  $nome        = '';
  $validadenormal = '';
  $codigo         = '';
  $plano_tarja    = ''; 
  $email          = '';
  $telefone       = '';  
  $vencimento     = '';
  
  if ((isset($_POST['localiza'])) or (isset($_POST['localiza2'])) ) {           
    $cadastro   = false;
    $alterar    = false;    
    $codigo1    = "0";    
    $codigo2    = $seg->antiInjection($_POST['codigo2']);
    $codigo3    = $seg->antiInjection($_POST['codigo3']);
    $codigo4    = $seg->antiInjection($_POST['codigo4']);
    $via        = $seg->antiInjection($_POST['via']);	
    $modo_verde = $seg->antiInjection($_POST['modo_verde1']); 	
    $codigo_validade = $codigo2.$codigo3.$codigo4;
	  $via_carteira = $via;	
    if (($_SESSION['tem_leitora'] == "S") and ($modo_verde == "N"))  {

      $variavel = $_POST['codigo6'];
 	   	   
      if (strlen($variavel) == 39) {
        $variavel = str_replace("�0","�0",$variavel);
        $variavel = str_replace(";0","�0",$variavel);
        
        $posicao        = strpos($variavel,"�0");	   	
        $codigo         = substr($variavel,$posicao+1,18);	  
      }else if (strlen($variavel) == 25){
	      $posicao        = strpos($variavel,"0");	   	
        $codigo         = substr($variavel,$posicao,17);	
	    }
      else {
        $variavel = str_replace(":�0",":�0",$variavel);
        $variavel = str_replace("?;0",":�0",$variavel);
        $variavel = str_replace("/0",":�0",$variavel);
        $variavel = str_replace("(","%",$variavel);
        $variavel = str_replace(")",":",$variavel);
        $variavel = str_replace("[0",":�0",$variavel);
        $variavel = str_replace("]",":",$variavel);
        $posicao        = strpos($variavel,":�0");	   	
        $codigo         = substr($variavel,$posicao+2,17);	        
      }
      
      $via            = substr($variavel,$posicao+20,2);
      $vencimento     = substr($variavel,$posicao+22,4);
      $nascimento     = substr($variavel,$posicao+59,8);
	    $plano_tarja    = substr($variavel,$posicao+31,3); 	  
      $vencimento     = '01/'.substr($vencimento,2,2).'/20'.substr($vencimento,0,2);
      $nome           = strtoupper(trim(substr($variavel,1,$posicao-1)));            
      $vencimento1    = substr($vencimento,6,4).'/'.substr($vencimento,3,2).'/'.substr($vencimento,0,2);
      $validadenormal = date("t/m/Y", strtotime(date($vencimento1.'H:i:s')));
      
      $codigo1 = substr($codigo,0,1);
      $codigo2 = substr($codigo,1,3);
      $codigo3 = substr($codigo,4,12);
      $codigo4 = substr($codigo,16,1); 
      $codigo_validade = substr($codigo,1,17); 
      if ($_SESSION['apelido_operadora'] == 'unimedSalto')
        $via_carteira    =  substr($variavel,$posicao+20,2);
      else  
        $via_carteira    = substr($codigo,16,2);	
     
    }
    $id_usuario1 = 0;
    $via_cart    = 0;
    if ( ($_SESSION['apelido_operadora'] == 'unimedLestePaulista') or ($_SESSION['apelido_operadora'] == 'unimedSalto') ){
      $sql_validade = new Query();
      $txt_validade = " SELECT NNUMEUSUA,ultima_via_carteira(NNUMEUSUA) VIA,DPVALUSUA
                  FROM HSSUSUA
               WHERE CCODIUSUA = :codigo ";	
      $sql_validade->addParam(":codigo",$codigo_validade);  
      $sql_validade->executeQuery($txt_validade);	
      
      $via_usuario  = $sql_validade->result("VIA");		
      if ($via_carteira == $via_usuario){ 
        $via_cart = $via_usuario;
      }   
    }

    if (strlen($codigo1) <> 1 || strlen($codigo2) <> 3 || strlen($codigo3) <> 12 || strlen($codigo4) <> 1 ) {
      $tpl->MSG = "N�mero de carteirinha inv�lido!";
      $tpl->block("MENSAGEM");      
    } 
    else if ($via == '') {
      $tpl->MSG = "Informe a via do cart�o.";
      $tpl->block("MENSAGEM");    
    } 
	  else if (($via_cart == 0) and ($_SESSION['apelido_operadora'] == 'unimedLestePaulista' or ($_SESSION['apelido_operadora'] == 'unimedSalto') )) {
      $tpl->MSG = "Via da carteirinha Incorreta. Entre em contato com a operadora.";
      $tpl->block("MENSAGEM");    
    }	
    else {
      $codigo = $codigo1.$codigo2.$codigo3;      
      $digito = $util->modulo11($codigo);
      $codigo = substr($codigo,1,strlen($codigo)).$digito;
      
      
      
      if ($digito == $codigo4) {	 
              
        $sql = new Query();
        $txt = "SELECT CNOMEUSUA,CDINDUSUA 
                  FROM HSSUSUA 
                 WHERE CCODIUSUA = :codigo ";
        $sql->addParam(":codigo",$codigo);
        $sql->executeQuery($txt);
        
        if ($sql->result("CNOMEUSUA") <> '') { 
          $sql2 = new Query();
          $txt2 = "SELECT HSSCEVEN.NNUMETITU NNUMETITU,NNUMESETOR FROM HSSCEVEN,HSSTITU 
                    WHERE CCODICEVEN = ContratoIntercambioUnimed(:codigo) 
                      AND HSSCEVEN.NNUMETITU = HSSTITU.NNUMETITU ";

          $sql2->addParam(":codigo",$codigo);
          $sql2->executeQuery($txt2);
		
          if ($sql2->result("NNUMETITU") > 0) {
            $txt3 = "UPDATE HSSUSUA SET DPVALUSUA = LAST_DAY(:validade)
                      WHERE CCODIUSUA = :codigo 
					    AND DPVALUSUA < LAST_DAY(:validade) ";					  
		
            $sql3 = new Query($bd);
            $sql3->addParam(":validade",$vencimento);                  
            $sql3->addParam(":codigo",$codigo);                  
            $sql3->executeSQL($txt3);
            $alterar = true;  
			
          }
       
          $nome = $sql->result("CNOMEUSUA");
          $digital = $sql->result("CDINDUSUA");
          
    		  if ($alterar == false) {
          
            if ($_GET['local'] == '1') {
              echo "<script language='JavaScript'> 
                      window.opener.jQuery('#codigo1').val('".$codigo."');    
                      window.opener.jQuery('#codigo1').blur();
                      window.close(); 
                    </script>";
            }
            else {
              echo "<script language='JavaScript'> 
                      window.opener.jQuery('#codigo').val('".$codigo."');    
                      window.opener.jQuery('#codigo').blur();
                      window.close(); 
                    </script>";            
            }
          }
        }
        else {		
        
		  $sql = new Query($bd);
			  
          if (substr($codigo,0,3) == '994')  {	
					 
            $codigo2 = '0994'.'0000'.substr($codigo,7,2);  			  
                  
            $txt = "SELECT HSSCEVEN.NNUMETITU NNUMETITU,NNUMESETOR FROM HSSCEVEN,HSSTITU ".
			             " WHERE CCODICEVEN = :codigo".
				           "   AND HSSCEVEN.NNUMETITU = HSSTITU.NNUMETITU ";					   
            				   
            $sql->addParam(":codigo",$codigo2);		

			
			$sql->executeQuery($txt);
			
			if ($sql->result("NNUMETITU") == '') {
			  $sql->clear();
			
			  $txt = "SELECT HSSCEVEN.NNUMETITU NNUMETITU,NNUMESETOR FROM HSSCEVEN,HSSTITU ".
			             " WHERE CCODICEVEN = ContratoIntercambioUnimed(:codigo) ".
				           "   AND HSSCEVEN.NNUMETITU = HSSTITU.NNUMETITU ";
				   
			  $sql->addParam(":codigo",$codigo);	
              $sql->executeQuery($txt);			  
			}
          }          
		  else { 
		    $txt = "SELECT HSSCEVEN.NNUMETITU NNUMETITU,NNUMESETOR FROM HSSCEVEN,HSSTITU ".
			       " WHERE CCODICEVEN = ContratoIntercambioUnimed(:codigo) ".
			        "   AND HSSCEVEN.NNUMETITU = HSSTITU.NNUMETITU ";
				 
		    $sql->addParam(":codigo",$codigo);
            $sql->addParam(":codigo",$codigo);	
            $sql->executeQuery($txt);						  
		  }       

          if  ($sql->result("NNUMETITU") > 0)  {
            $erro = "";
            $cadastro = true;
            $id_contrato = $sql->result("NNUMETITU");
            $id_locacao = $sql->result("NNUMESETOR");
          } else {
            $nome = '';
            $digital = '';
            $tpl->MSG = "Benefici�rio n�o localizado! Entre em contato com a central de atendimento.";
            $tpl->block("MENSAGEM");
            
            
          }
        }
      } 
      else {
        $tpl->MSG = "N�mero de carteirinha inv�lido!";
        $tpl->block("MENSAGEM");
      }      
    }    
  }
		
  else if (isset($_POST['fechar'])) {
    echo "<script language='JavaScript'> window.close(); </script>";    
  } 
  else if (isset($_POST['cadastrar'])) {
  
    $cadastro             = true;
    $retorno              = 0;
    $cpf                  = $seg->antiInjection($_POST['cpf']);
    $telefone             = $seg->antiInjection($_POST['i_telefone']);
    $email                = $seg->antiInjection($_POST['email']);	    
    $nome                 = $formata->removeCaracteresInvalidos(strtoUpper($_POST['i_nome']));
    $nascimento           = $seg->antiInjection($_POST['i_nascimento']);
    $nascimento_explodido = explode("/",$nascimento);      
    $validade             = $seg->antiInjection($_POST['i_validade']);
    $validadenormal       = $seg->antiInjection($_POST['i_validade']); // Esta variavel contem a data de validade no formato correto
    $validade_explodido   = explode("/",$validade);
    $validade             = explode("/",$validade);
    $validade             = $validade[2].$validade[1].$validade[0];
    $codigo               = $seg->antiInjection($_POST['codigo']);
    $id_contrato          = $seg->antiInjection($_POST['id_contrato']);	
	  $plano_tarja          = $seg->antiInjection($_POST['plano_tarja']);	
    $sexo                 = $seg->antiInjection($_POST['i_sexo']);
    $id_locacao           = $seg->antiInjection($_POST['id_locacao']);
    
    //sdebug($validade . ' ' . date('Ymd'),true);
    if ($nome == '') 
      $erro = "Informe o nome do benefici�rio.";
    else if ($util->nomeValido($nome) == false)    
      $erro = "Nome inv�lido.";
    else if (strlen($util->caracteresInvalidos($nome)) > 0)    
      $erro = "O Campo 'Nome' cont�m caracteres inv�lidos. ".$util->caracteresInvalidos($nome);
    else if ($nascimento == '') 
      $erro = "Informe a data de nascimento do benefici�rio.";
    else if (strlen($nascimento) <> 10)
      $erro = "A data de nascimento obrigatoriamente deve ser digitada no formato DD/MM/YYYY.";
    else if ($nascimento_explodido[0] <= '00' or $nascimento_explodido[0] > '31')
      $erro = "Dia de nascimento incorreto.";
    else if ($nascimento_explodido[1] <= '00' or $nascimento_explodido[1] > '12')
      $erro = "M�s de nascimento incorreto.";
    else if ($nascimento_explodido[2].$nascimento_explodido[1].$nascimento_explodido[0] > date('Ymd') )
      $erro = "Data de nascimento incorreta.";
	  else if ($sexo == '')
      $erro = "Informe o sexo do benefici�rio.";  
	  else if ($telefone == '')
      $erro = "Informe o telefone do benefici�rio." . $telefone ; 	  
    else if ($validade == '') 
      $erro = "Informe a data de validade do cart�o.";
    else if (strlen($validadenormal) <> 10)
      $erro = "A data de validade obrigatoriamente deve ser digitada no formato DD/MM/YYYY.";
    else if ($validade_explodido[0] <= '00' or $validade_explodido[0] > '31')
      $erro = "Dia de validade incorreto.";
    else if ($validade_explodido[1] <= '00' or $validade_explodido[1] > '12')
      $erro = "M�s de validade incorreto.";
    else if ($validade < date('Ymd')) 
      $erro = "Carteira com validade vencida.";
    else {
      $erro = "";
      $cpf_semmascara = $formata->somenteNumeros($cpf);

      $sql4 = new Query($bd);
      $txt_tarja = "SELECT RETORNAIDACOMINTERCAMBIO30(:codigo_tarja,:contrato) ID FROM DUAL ";			   	 
      $sql4->addParam(":codigo_tarja",$plano_tarja);
      $sql4->addParam(":contrato",$id_contrato);     
      $sql4->executeQuery($txt_tarja);    
      $id_acomodacao = $sql4->result("ID"); 
      
      $stored = "BEGIN INSERE_USUARIO_POSPAG30(:contrato,:nome,:codigo,'T',to_date(:nascimento,'DD/MM/YYYY'),:sexo,:cpf,:telefone,to_date(:validade,'DD/MM/YYYY'),:id_locacao,:via,:retorno,:acomodacao,:email); END;";

      $sql2 = new Query($bd);
      $sql2->addParam(":contrato",$id_contrato);
      $sql2->addParam(":nome",$nome);
      $sql2->addParam(":codigo",$codigo);
      $sql2->addParam(":nascimento",$nascimento);
      $sql2->addParam(":sexo",$sexo);
      $sql2->addParam(":cpf",$cpf_semmascara);
      $sql2->addParam(":telefone",$telefone);
      $sql2->addParam(":validade",$validadenormal);
      $sql2->addParam(":id_locacao",$id_locacao);
      $sql2->addParam(":via",$via);
      $sql2->addParam(":retorno",'',10);
      $sql2->addParam(":email",$email);	        
	    $sql2->addParam(":acomodacao",$id_acomodacao);	  
	  	 
      $e = $sql2->executeSQL($stored);	  
          
      if ($sql2->getReturn(":retorno") > 0) {
        $txt = "SELECT CNOMEUSUA,CCODIUSUA,CDINDUSUA ".
               "  FROM HSSUSUA ".
               " WHERE NNUMEUSUA = :id ";
        
        $sql3 = new Query($bd);
        $sql3->addParam(":id",$sql2->getReturn(":retorno"));
        $sql3->executeQuery($txt);
        
        if ($_GET['local'] == '1') {
          echo "<script language='JavaScript'> 
                  window.opener.jQuery('#codigo1').val('".$sql3->result("CCODIUSUA")."');    
                  window.opener.jQuery('#codigo1').blur();
                  window.close(); 
                </script>";
        }
        else {
          echo "<script language='JavaScript'> 
                  window.opener.jQuery('#codigo').val('".$sql3->result("CCODIUSUA")."');    
                  window.opener.jQuery('#codigo').blur();
                  window.close(); 
                </script>";            
        }
      } 
      else {
        $tpl->MSG = "Erro ao incluir benefici�rio. ".$e;
        $tpl->block("MENSAGEM");
      }
    }
    
    if ($erro <> '') {
      $tpl->MSG = $erro;
      $tpl->block("MENSAGEM");
    }
  }
  else if (isset($_POST['alterar'])) {
    $codigo               = $seg->antiInjection($_POST['codigo']);
	  $email                = $seg->antiInjection($_POST['a_email']);	    
    $telefone             = $seg->antiInjection($_POST['a_telefone']);
    $nome                 = strtoUpper($seg->antiInjection($_POST['a_nome'])); 
    
    if ($telefone == '') {
      $erro = "Informe o telefone do benefici�rio." . $telefone ; 	  
    }
    else{
      $sql5 = new Query($bd);
      $txt5 = "UPDATE HSSUSUA
                  SET CMAILUSUA = :email
                WHERE CCODIUSUA = :codigo
                  AND ((CMAILUSUA <> :email) OR CMAILUSUA IS NULL)";					  
		
      $sql5->addParam(":email",$email);                  
      $sql5->addParam(":codigo",$codigo);                  
      $sql5->executeSQL($txt5);	
      
      $sql4 = new Query();
      $txt4 = "SELECT HSSUSUA.NNUMEUSUA NNUMEUSUA
                FROM HSSUSUA,HSSTLUSU 
                WHERE CCODIUSUA = :codigo 
                  AND HSSUSUA.NNUMEUSUA = HSSTLUSU.NNUMEUSUA
                  AND COBSETLUSU = 'contato'";
      $sql4->addParam(":codigo",$codigo);
      $sql4->executeQuery($txt4);

      $sql6 = new Query($bd);    
      if ($sql4->count() > 0) {    
        $txt6 = "UPDATE HSSTLUSU
                    SET CFONETLUSU = :telefone
                  WHERE NNUMEUSUA = (SELECT NNUMEUSUA FROM HSSUSUA WHERE CCODIUSUA = :codigo)
                    AND ( ( CFONETLUSU <> :TELEFONE ) OR (CFONETLUSU IS NULL))
                    AND COBSETLUSU = 'contato'";					  
      }
      else {
        $txt6 = "INSERT INTO HSSTLUSU (NNUMEUSUA,CFONETLUSU,COBSETLUSU)
                    SELECT NNUMEUSUA,:telefone,'contato' FROM HSSUSUA 
                    WHERE CCODIUSUA = :codigo";
      }
		
      $sql6->addParam(":codigo",$codigo);                  
      $sql6->addParam(":telefone",$telefone);                  
      $sql6->executeSQL($txt6);	

      if ($_GET['local'] == '1') {
        echo "<script language='JavaScript'> 
                window.opener.jQuery('#codigo1').val('".$codigo."');    
                window.opener.jQuery('#codigo1').blur();
                window.close(); 
              </script>";
      }
      else {
        echo "<script language='JavaScript'> 
                window.opener.jQuery('#codigo').val('".$codigo."');    
                window.opener.jQuery('#codigo').blur();
                window.close(); 
              </script>";            
      }
    }
    if ($erro <> '') {
      $tpl->MSG = $erro;
      $tpl->block("MENSAGEM");
    }
  }
  
  if ($cadastro)
    $tpl->block("INCLUSAO");  
  else if ($alterar){
    
    $sql4 = new Query();
    $txt4 = "SELECT CNOMEUSUA, CMAILUSUA, CFONETLUSU 
               FROM HSSUSUA, HSSTLUSU 
              WHERE CCODIUSUA = :codigo 
			          AND HSSUSUA.NNUMEUSUA = HSSTLUSU.NNUMEUSUA(+)
			          AND COBSETLUSU = 'contato'";
    $sql4->addParam(":codigo",$codigo);
    $sql4->executeQuery($txt4);

	  $email    = $sql4->result("CMAILUSUA");
	  $telefone = $sql4->result("CFONETLUSU");	
	
    $tpl->block("ALTERACAO"); 
  }      
  else  
    $tpl->block("CARTEIRINHA");
    
  if (($_SESSION['tem_leitora'] == "S") and ($modo_verde == "N"))  {
    $tpl->NOME_READONLY = "readonly";
    $tpl->VALIDADE_READONLY = "readonly";		
    
  } else if ($tpl->MANUAL = "S"){
    $tpl->NOME_READONLY = "";
    $tpl->VALIDADE_READONLY = "";  
    
    
  }

  $tpl->EMAIL       = $email;
	$tpl->TELEFONE    = $telefone;
  $tpl->ID_CONTRATO = $id_contrato;
  $tpl->ID_LOCACAO  = $id_locacao;   
  $tpl->CODIGO2     = $codigo2;
  $tpl->CODIGO3     = $codigo3;
  $tpl->CODIGO4     = $codigo4;
  $tpl->VIA         = $via;    
  $tpl->MODO_VERDE  = $modo_verde;		
  $tpl->NOME        = $nome;
  $tpl->NASCIMENTO  = $nascimento;
  $tpl->VALIDADE    = $validadenormal;
  $tpl->SEXO        = $sexo;
  $tpl->CPF         = $cpf;    
  $tpl->CODIGO      = $codigo;	
  $tpl->PLANO_TARJA = $plano_tarja;        
  
  if ($seg->permissaoOutros($bd,'WEBPRESTADORINFORMACODIGOMANUALUNIMED',false)) {
    $tpl->MANUAL = "S";	
  }	else {
    $tpl->MANUAL = "N";	
  }	
  
  $bd->close();
  $tpl->show();  
  
?>