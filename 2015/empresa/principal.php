<?php 
  Session_start(); 
  
  require_once("../comum/autoload.php");
  require_once("../comum/sessao.php");
  
  $bd   = new Oracle();  
  
  $_SESSION['titulo'] = "P�GINA INICIAL";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/principal.html");

  $tpl->ID_SESSAO      = $_GET['idSessao'];
  
  $idContrato = 0;
  $idLocacao  = 0;
  
  if (isset($_SESSION['id_contrato'])) {
    $tpl->ID_SELECIONADO = $_SESSION['id_contrato'];
    $idContrato = $_SESSION['id_contrato'];
  }

  if (isset($_SESSION['id_locacao'])) {
    $tpl->LOCACAO_SELECIONADO = $_SESSION['id_locacao'];
    $idLocacao = $_SESSION['id_locacao'];
  }
    
  $sql = new Query();  
  $txt = "SELECT HSSOPTIT.NNUMETITU,HSSTITU.CCODITITU,CRAZAEMPR,CCODIPLAN, NVL(CNPCAPLAN, CDESCPLAN) PLANO,
                 NVL(HSSOPTIT.NNUMESETOR,-1) NNUMESETOR,CNOMESETOR,CCUOPCONGE,NVL(CASO_TITU,'N') CASO_TITU
            FROM HSSOPTIT,HSSTITU,HSSEMPR,HSSSETOR ,HSSPLAN,HSSCONGE
           WHERE HSSOPTIT.NNUMEUSUA = :id_operador
             AND HSSOPTIT.NNUMETITU = HSSTITU.NNUMETITU 
             AND (HSSTITU.CSITUTITU = 'A' OR (CSITUTITU <> 'A' AND DSITUTITU + :dias > SYSDATE))
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR                 
             AND HSSTITU.NNUMEPLAN = HSSPLAN.NNUMEPLAN
             AND HSSOPTIT.NNUMESETOR = HSSSETOR.NNUMESETOR(+)
             AND NVL(CSITUSETOR,'A') = 'A'
             AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
           ORDER BY 3,4 ASC";
           
  $sql->addParam(":id_operador",$_SESSION['id_operador']);   
  $sql->addParam(":dias",$_SESSION['qtde_dias_acesso_cancelado']);     
  $sql->executeQuery($txt);
  
  $tpl->DESCRICAO_RELACAO = "Rela��o de contratos";
  
  while (!$sql->eof()) {
	$_SESSION['razao_social_empresa'] = $sql->result("CRAZAEMPR");
    if ($sql->result("NNUMESETOR") > 0)
      $nome_locacao = " / Loca��o: ".$sql->result("CNOMESETOR");
    else
      $nome_locacao = ''; 

    if ($sql->result("CCUOPCONGE") == '2')
      $nome_plano = "";
    else
      $nome_plano = " (".$sql->result("CCODIPLAN")." - ".$sql->result("PLANO").") ";
    
	$_SESSION['contrato_pcmso'] = $sql->result("CASO_TITU");  
    $tpl->CONTRATO_ID     = $sql->result("NNUMETITU");
    $tpl->CONTRATO_LOC    = $sql->result("NNUMESETOR");	
    
    $tpl->CONTRATO_CODIGO = $sql->result("CCODITITU");
    $tpl->CONTRATO_NOME   = $formata->initCap($sql->result("CRAZAEMPR").$nome_locacao.$nome_plano." - ".$sql->result("SITUACAO"));
        
    if (($idContrato == $sql->result("NNUMETITU")) and
        ($idLocacao == $sql->result("NNUMESETOR")))
      $tpl->CONTRATO_LOGADO = "<i class='icon-hand-right'></i>";
    else
      $tpl->CONTRATO_LOGADO = "";
      
    $tpl->block("CONTRATO");            
    $sql->next();
  }
  
  if ($_SESSION['numero_contratos'] > 1)
    $tpl->block("CONTRATOS");   
/*
  if ($_SESSION['mensagem_web_empresa_pop'] <> '') {
    $tpl->block("POPUP");  
  } 
  */
  
  $aviso = '';
  
  if ($_SESSION['apelido_operadora'] == 'saudemed') {
    $txt = "SELECT COUNT(*) QTDE FROM HSSATCAD
             WHERE NNUMETITU = :contrato
               AND NVL(NNUMESETOR,-1) = :locacao
               AND DVISUATCAD IS NULL
               AND CFLAGATCAD = 'R' ";

    $sql = new Query();             
    $sql->addParam(":contrato",$idContrato);   
    $sql->addParam(":locacao",$idLocacao);   
    $sql->executeQuery($txt);  

    $qtde = $sql->result("QTDE");

    if ($qtde > 0) {
      if ($qtde == 1)
        $aviso = "<div style='margin-left:250px; text-align:center; padding:15px; width:290px; color:#FFFFFF; font-size: 15px; text-decoration: blink; background:#009999;'>Existe 1 opera��o pendente.";      
      else
        $aviso = "<p style='font-size:14'>Existem ".$qtde." opera��es pendentes.";
        
      $aviso .= " <br>Clique <a href='pendentes.php?idSessao=".$_GET['idSessao']."' class='link'>aqui</a> para verificar.</div><br><br><br>";
    }
  }
  
  if ($_SESSION['apelido_operadora'] == 'unimedFranca') {
    $txt = "SELECT CNOMEATCAD,TO_CHAR(DDATAATCAD,'DD/MM/YYYY HH24:MI') DDATAATCAD,DECODE(COPERATCAD,'A','Altera��o','I','Inclus�o','C','Cancelamento') COPERATCAD
              FROM HSSATCAD,SEGUSUA
             WHERE NNUMETITU = :contrato 
               AND HSSATCAD.NOPERUSUA = SEGUSUA.NNUMEUSUA(+)    
               AND TO_CHAR(DDATAATCAD,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY')
               AND CFLAGATCAD = 'R'
               AND CREAPATCAD <> 'N'
             ORDER BY 1,COPERATCAD,DDATAATCAD DESC";

    $sql = new Query();    
    $sql->addParam(":contrato",$idContrato);         
    $sql->executeQuery($txt);  

    if ($sql->count() > 0) {
      while (!$sql->eof()) {
        $tpl->REJE_NOME = $sql->result("CNOMEATCAD");
        $tpl->REJE_DATA = $sql->result("DDATAATCAD");
        $tpl->REJE_OPERACAO = $sql->result("COPERATCAD");   

        $tpl->block("REGISTRO");            
        $sql->next();
      }
        
      $tpl->block("BLOCO");
    }
  }
  
  if (($seg->permissaoOutros($bd,"WEBEMPRESAALTERAODEBENEFICIRIO",false) or 
       $seg->permissaoOutros($bd,"WEBEMPRESAMOVIMENTACAODEUSUARIO",false)))  {
  
    $sql_cpf = new Query();
    
    if ($idLocacao > 0) {
      $f_locacao = "   AND HSSUSUA.NNUMESETOR = :locacao";
      $sql_cpf->addParam(":locacao",$idLocacao);      
    }
    else
      $f_locacao = "";
      
    $txt_cpf = "SELECT * FROM (
                SELECT TITULAR.CNOMEUSUA TITULAR,HSSUSUA.CCODIUSUA, HSSUSUA.CNOMEUSUA,HSSUSUA.CTIPOUSUA,HSSUSUA.NNUMEUSUA
                  FROM HSSUSUA,HSSUSUA TITULAR
                 WHERE HSSUSUA.NNUMETITU = :contrato
                   AND HSSUSUA.CSITUUSUA = 'A'
                   AND HSSUSUA.C_CPFUSUA IS NULL ".
                   $f_locacao."
                   AND IDADE(HSSUSUA.DNASCUSUA,SYSDATE) > 17
                   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
                   AND HSSUSUA.NNUMEUSUA NOT IN (SELECT NNUMEUSUA FROM HSSATCAD WHERE CFLAGATCAD IS NULL AND NNUMEUSUA = HSSUSUA.NNUMEUSUA)
                 ORDER BY 1,3)
                 WHERE ROWNUM <= 5";      
   
    $sql_cpf->addParam(":contrato",$idContrato);   
    $sql_cpf->executeQuery($txt_cpf);  

    if ($sql_cpf->count() > 0) {
      while (!$sql_cpf->eof()) {
        $tpl->SEMCPF_ID        = $sql_cpf->result("NNUMEUSUA");
        $tpl->SEMCPF_TITULAR   = $sql_cpf->result("TITULAR");
        $tpl->SEMCPF_CODIGO    = $sql_cpf->result("CCODIUSUA");
        $tpl->SEMCPF_USUARIO   = $sql_cpf->result("CNOMEUSUA");
        $tpl->SEMCPF_CATEGORIA = $func->categoriaUsuario($sql_cpf->result("CTIPOUSUA"));

        $tpl->block("REGISTRO_SEM_CPF");            
        $sql_cpf->next();
      }
      
      $tpl->block("SEM_CPF");
    }
  }   
  
  if (($_SESSION['mensagem_web_empresa'] <> '') or ($aviso <> '')) {
    $tpl->AVISOS = $aviso.$_SESSION['mensagem_web_empresa'];
    $tpl->block("MOSTRA_AVISOS");    
  }
    
  $tpl->block("MOSTRA_MENU");   
  
  $bd->close();
  $tpl->show();     
?>