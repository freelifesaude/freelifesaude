<?php

class Atendimento {

  private $bd;
  private $idAtendimento;
  private $erros         = array();
  private $mensagens     = array();
  
  public $texto;
  public $idBeneficiario;
  public $idContrato;
  public $idPrestador;
  public $origem;
  public $idSetor;
  public $idClassificacao;
  public $idSubClassificacao;
  public $status;
  public $classificacao;
  public $subClassificacao;
  public $solicitante;
  public $dataAtendimento;
  public $titulo;
  public $situacao;
  public $situacaoDesc;
  
  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  }  
    
  public function __construct($bd) {
    $this->bd = $bd;
  }
  
  public function getIdAtendimento() {
    return $this->idAtendimento;
  }  
  
  public function setIdAtendimento($id) {
    $this->idAtendimento = $id;
  }    
  
  public function getMensagens() {
    return $this->mensagens;
  }

  public function insereMensagem($texto,$tipo,$anexos) {
    $func = new Funcao();  
                       
    /*
      tipo
        0 = Operadora/Central
        P = Prestador
    */
    
    $tamanhoArquivo = 0;
    $erro = 0;
    $mgs  = '';
    
    if ($texto == '') {
      $erro++;
      $this->adicionaErro('A descri��o do atendimento � de preenchimento obrigat�rio.');    
    }
    
    for ($i = 0; $i < sizeOf($anexos);$i++) {
      $anexo = array();
      $anexo = $anexos[$i];
      
      $tamanhoArquivo = filesize($anexo['tmp_name'])/1024;
        
      if ($tamanhoArquivo > $func->convertBytes(ini_get("upload_max_filesize"))) {
        $erro++;
        $this->adicionaErro($anexo['name'].' - O tamanho do arquivo n�o pode ser superior a '.ini_get("upload_max_filesize"));    
      }
    }    
     
    if ($erro == 0) {
    
      $sql_prox = new Query($this->bd);
      $txt_prox = "SELECT SEQ_MENSA.NEXTVAL PROXIMO FROM DUAL";
      $sql_prox->executeQuery($txt_prox);
      
      $idMensagem =  $sql_prox->result("PROXIMO");
    
      $sql = new Query($this->bd);    
      $txt = "INSERT INTO HSSMENSA (NNUMEMENSA,NNUMEATEND,DDATAMENSA,CTEXTMENSA,NOPERUSUA,CTIPOMENSA)
                            VALUES (:idMensagem,:idAtendimento,SYSDATE,:texto,:idOperador,:tipo)";
                         
      $sql->addParam(":idMensagem"         ,$idMensagem);
      $sql->addParam(":idAtendimento"      ,$this->idAtendimento);
      $sql->addParam(":texto"              ,$texto);
      $sql->addParam(":idOperador"         ,$_SESSION['id_operador']);
      $sql->addParam(":tipo"               ,$tipo);

      $msg = $sql->executeSQL($txt);          
      
      for ($i = 0; $i < sizeOf($anexos);$i++) {
        $anexo = array();
        $anexo = $anexos[$i];
   
        if ($anexo['tmp_name'] <> '') {
          $img = new Imagem();        
          $idArquivo = $img->insereArquivo ($anexo,'','HSSMENSA','HSSMENSA');

          if ($idArquivo > 0) {
            $sql_anex = new Query($bd);
            $txt_anex = "INSERT INTO HSSARMEN (NNUMEARQUI,NNUMEMENSA)
                                       VALUES (:idArquivo,:idMensagem)";
            $sql_anex->addParam(":idArquivo",$idArquivo);
            $sql_anex->addParam(":idMensagem",$idMensagem);
            $sql_anex->executeSQL($txt_anex);          
          }       
        }
      }  

      if ($msg <> '') {
        $erro++;
        $this->adicionaErro($msg);      
      }
    }
  }
  
  public function insereGuia($guia) {
    $sql = new Query($this->bd);
                       
    $txt = "INSERT INTO HSSATGUI (NNUMEATEND,NNUMEGUIA)
                          VALUES (:idAtendimento,:guia)";
                       
    $sql->addParam(":idAtendimento"      ,$this->idAtendimento);
    $sql->addParam(":guia"               ,$guia);

    $erro = $sql->executeSQL($txt);    
  }  
  
  public function insert() {
    $func = new Funcao();
    
    $sql_atend = new Query($this->bd);  
    $txt_atend = "SELECT SEQATEND.NEXTVAL PROXIMO FROM DUAL";
    $erro = $sql_atend->executeQuery($txt_atend); 
  
    $this->idAtendimento = $sql_atend->result("PROXIMO");
    
    if ($this->idAtendimento > 0) {
            
      $sql = new Query($this->bd);
                         
      $txt = "INSERT INTO HSSATEND (NNUMEATEND,DDATAATEND,CASSUATEND,NNUMEUSUA,NNUMETITU,NOPERUSUA,CSTATATEND,CTITUATEND)
                            VALUES (:idAtendimento,SYSDATE,nvl(:texto,' '),:idBeneficiario,:idContrato,:idOperador,:status,:titulo)";
                         
      $sql->addParam(":idAtendimento"      ,$this->idAtendimento);
      $sql->addParam(":texto"              ,$this->texto);
      $sql->addParam(":idBeneficiario"     ,$this->idBeneficiario);
      $sql->addParam(":idContrato"         ,$this->idContrato);
      $sql->addParam(":idOperador"         ,$_SESSION['id_operador']);
      $sql->addParam(":status"             ,$this->status);
      $sql->addParam(":titulo"             ,strToUpper($this->titulo));
        
      $erro = $sql->executeSQL($txt);   
      
      if ($this->idBeneficiario > 0) {
        $sql_rel = new Query($this->bd);
        $txt_rel = "INSERT INTO HSSATUSU (NNUMEATEND,NNUMEUSUA)
                                  VALUES (:idAtendimento,:idBeneficiario)";
                           
        $sql_rel->addParam(":idAtendimento"      ,$this->idAtendimento);
        $sql_rel->addParam(":idBeneficiario"     ,$this->idBeneficiario);
    
        $erro = $sql_rel->executeSQL($txt_rel);   
      }
      else if ($this->idContrato > 0) {
        $sql_rel = new Query($this->bd);
        $txt_rel = "INSERT INTO HSSATTIT (NNUMEATEND,NNUMETITU)
                                  VALUES (:idAtendimento,:idContrato)";
                           
        $sql_rel->addParam(":idAtendimento"      ,$this->idAtendimento);
        $sql_rel->addParam(":idContrato"         ,$this->idContrato);
    
        $erro = $sql_rel->executeSQL($txt_rel);   
      }
      else if ($this->idPrestador > 0) {
        $sql_rel = new Query($this->bd);
        $txt_rel = "INSERT INTO HSSATPRE (NNUMEATEND,NNUMEPRES)
                                  VALUES (:idAtendimento,:idPrestador)";
                           
        $sql_rel->addParam(":idAtendimento"      ,$this->idAtendimento);
        $sql_rel->addParam(":idPrestador"        ,$this->idPrestador);    
        $erro = $sql_rel->executeSQL($txt_rel);   
      }        
    } 
  } 
  
  public function temSubClassificacao($id) {
    $sql = new Query($this->bd);
    $txt = "SELECT 1 QTDE FROM HSSCLATE WHERE NSUPECLATE = :id";
    $sql->addParam(":id",$id);
    $sql->executeQuery($txt);
    
    if ($sql->count() > 0)
      return true;
    else
      return false;    
  }

  public function update() {
    $func = new Funcao();

    if ($this->idAtendimento > 0) {
      
      //if ($this->texto == '')
      //  $this->adicionaErro("A descri��o do atendimento � de preenchimento obrigat�rio.");        

      if ($this->idClassificacao == '')
        $this->adicionaErro("A classifica��o � de preenchimento obrigat�rio.");        

      if (($this->idSubClassificacao == '') and ($this->temSubClassificacao($this->idClassificacao)))
        $this->adicionaErro("A sub-classifica��o � de preenchimento obrigat�rio.");        
        
      if ($this->qtdeErros() <= 0) {        
        $sql = new Query($this->bd);
                           
        $txt = "UPDATE HSSATEND 
                   SET CASSUATEND = :texto,
                       CORIGATEND = :origem,
                       NNUMESETOR = :setor,
                       NNUMECLATE = :classificacao,
                       NCOBRCLATE = :subClassificacao,
                       NALTEUSUA  = :idOperador,
                       DALTEATEND = SYSDATE,
                       CSTATATEND = :status,
                       CTITUATEND = :titulo
                 WHERE NNUMEATEND = :idAtendimento ";
                           
        $sql->addParam(":idAtendimento"      ,$this->idAtendimento);
        $sql->addParam(":texto"              ,$this->texto);
        $sql->addParam(":origem"             ,$this->origem);
        $sql->addParam(":setor"              ,$this->idSetor);
        $sql->addParam(":classificacao"      ,$this->idClassificacao);
        $sql->addParam(":subClassificacao"   ,$this->idSubClassificacao);
        $sql->addParam(":status"             ,$this->status);
        $sql->addParam(":idOperador"         ,$_SESSION['id_operador']);
        $sql->addParam(":titulo"             ,strToUpper($this->titulo));
    
        $erro = $sql->executeSQL($txt);       
        
        if ($erro <> '')
          $this->adicionaErro($erro);       
      }
    }
    else
      $this->adicionaErro("Atendimento n�o informado");     
  }  

  public function carregaDados() {  
    $sql_atend = new Query($this->bd);
    $txt_atend = "SELECT HSSATEND.CASSUATEND,HSSATEND.NNUMEUSUA,HSSATEND.NNUMETITU,HSSATEND.CORIGATEND,HSSATEND.CURGEATEND,HSSATEND.NNUMECLATE,HSSATEND.NCOBRCLATE,
                         HSSATEND.CSTATATEND,HSSATEND.NNUMESETOR,CLASS.CDESCCLATE CLASSIFICACAO,SUB.CDESCCLATE SUBCLASSIFICACAO,INITCAP(SEGUSUA.CNCOMUSUA) CNCOMUSUA,
                         TO_CHAR(HSSATEND.DDATAATEND,'DD/MM/YYYY HH24:mi') DATAATENDIMENTO,TO_CHAR(HSSATEND.DDESLATEND,'DD/MM/YYYY HH24:MI') DATAFECHAMENTO,
                         INITCAP(FINALIZADO.CNCOMUSUA) FINALIZADO_POR,HSSATEND.CTITUATEND
                    FROM HSSATEND,HSSCLATE CLASS,HSSCLATE SUB,SEGUSUA,SEGUSUA FINALIZADO
                   WHERE HSSATEND.NNUMEATEND = :idAtendimento
                     AND HSSATEND.NNUMECLATE = CLASS.NNUMECLATE(+)
                     AND HSSATEND.NCOBRCLATE = SUB.NNUMECLATE(+)
                     AND HSSATEND.NOPERUSUA = SEGUSUA.NNUMEUSUA(+) 
                     AND HSSATEND.NFINAUSUA = FINALIZADO.NNUMEUSUA(+)";
    $sql_atend->addParam(":idAtendimento",$this->idAtendimento);
    $sql_atend->executeQuery($txt_atend);      
    
    $this->texto              = $sql_atend->result("CASSUATEND");
    $this->idContrato         = $sql_atend->result("NNUMETITU");
    
    if ($sql_atend->result("CORIGATEND") == '')
      $this->origem             = "T";
    else
      $this->origem             = $sql_atend->result("CORIGATEND");
     
    $this->idSetor            = $sql_atend->result("NNUMESETOR");
    $this->dataAtendimento    = $sql_atend->result("DATAATENDIMENTO");
    $this->idClassificacao    = $sql_atend->result("NNUMECLATE");
    $this->idSubClassificacao = $sql_atend->result("NCOBRCLATE");
    $this->subClassificacao   = $sql_atend->result("SUBCLASSIFICACAO");
    $this->classificacao      = $sql_atend->result("CLASSIFICACAO");
    $this->solicitante        = $sql_atend->result("CNCOMUSUA");
    $this->status             = $sql_atend->result("CSTATATEND");
    $this->titulo             = $sql_atend->result("CTITUATEND");
    
    if ($sql_atend->result("CSTATATEND") == "P")
      $this->situacao = "A";
    else if ($sql_atend->result("DATAFECHAMENTO") <> '')
      $this->situacao = "F";
    else
      $this->situacao = "A";
      
    if ($sql_atend->result("CSTATATEND") == "P")
      $this->situacaoDesc = "<font color='red'>Aberto</font>";
    else if ($sql_atend->result("DATAFECHAMENTO") <> '')
      $this->situacaoDesc = "<font color='green'>Fechado em ".$sql_atend->result("DATAFECHAMENTO")." por ".$sql_atend->result("FINALIZADO_POR")."</font>";
    else
      $this->situacaoDesc = "";
      
    $sql_vinculo = new Query($this->bd);
    $txt_vinculo = "SELECT 'B' TIPO, NNUMEUSUA ID
                      FROM HSSATUSU
                     WHERE NNUMEATEND = :idAtendimento 
                     UNION ALL
                    SELECT 'P' TIPO, NNUMEPRES ID
                      FROM HSSATPRE
                     WHERE NNUMEATEND = :idAtendimento 
                     UNION ALL
                    SELECT 'E' TIPO, NNUMETITU ID
                      FROM HSSATTIT
                     WHERE NNUMEATEND = :idAtendimento ";
    $sql_vinculo->addParam(":idAtendimento",$this->idAtendimento);
    $sql_vinculo->executeQuery($txt_vinculo);       
    
    if ($sql_vinculo->result("TIPO") == 'B')
      $this->idBeneficiario     = $sql_vinculo->result("ID");
    else if ($sql_vinculo->result("TIPO") == 'P')
      $this->idPrestador        = $sql_vinculo->result("ID");
    else if ($sql_vinculo->result("TIPO") == 'E')
      $this->idContrato         = $sql_vinculo->result("ID");
    else
      $this->idBeneficiario     = $sql_atend->result("NNUMEUSUA");
      
    $sql_msg = new Query($this->bd);
    $txt_msg = "SELECT HSSMENSA.DDATAMENSA,TO_CHAR(HSSMENSA.DDATAMENSA,'DD/MM/YYYY HH24:MI') DATA,INITCAP(SEGUSUA.CNCOMUSUA) CNCOMUSUA,HSSMENSA.CTEXTMENSA,
                       HSSMENSA.CTIPOMENSA,DECODE(HSSMENSA.DVISUMENSA,NULL,DECODE(HSSMENSA.CTIPOMENSA,'0','N','3','N','S'),'S') LIDO,HSSMENSA.NNUMEMENSA
                  FROM HSSMENSA,SEGUSUA
                 WHERE HSSMENSA.NNUMEATEND = :idAtendimento
                   AND HSSMENSA.NOPERUSUA = SEGUSUA.NNUMEUSUA
                 ORDER BY 1";
    $sql_msg->addParam(":idAtendimento",$this->idAtendimento);
    $sql_msg->executeQuery($txt_msg);  

    while (!$sql_msg->eof()) {
      $this->adicionaMensagem($sql_msg->result("CTEXTMENSA"),$sql_msg->result("DATA"),$sql_msg->result("CNCOMUSUA"),
                              $sql_msg->result("CTIPOMENSA"),$sql_msg->result("LIDO"),$sql_msg->result("NNUMEMENSA"));
      
      $sql_msg->next();
    }     
  }
  
  public function adicionaErro($descricao) {
    array_push($this->erros,array("* ".$descricao));
  }  
  
  public function adicionaMensagem($descricao,$data,$operador,$tipo,$lido,$id) {
    $array = array();
    $array['descricao'] = $descricao;
    $array['data']      = $data;
    $array['operador']  = $operador;
    $array['tipo']      = $tipo;
    $array['lido']      = $lido;
    $array['id']        = $id;
 
    array_push($this->mensagens,$array);
  }    
  
  public function qtdeErros() {
    return sizeOf($this->erros);
  }  
  
  public function getErros() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->erros);$b++) {      
      $erro = array();
      $erro = $this->erros[$b];
      
      $desc_erros .= $erro[0]."<br>";      
    } 
    
    return $desc_erros;
  } 

  public function configMensagem($tipo) {

    $config = array();
    
    if ($tipo == 'P') { // Prestador
      $config['titulo'] = "#F0F0F0";
      $config['corpo']  = "#F9F9F9";
      $config['tipoCliente']  = "Prestador";
    } 
    else if ($tipo == '0') { // Operadora
      $config['titulo'] = "#C4DBFF";
      $config['corpo']  = "#EAF2FF";
      $config['tipoCliente']  = "Operadora";
    } 
    else if ($tipo == '3') { // Conclus�o
      $config['titulo'] = "#669999";
      $config['corpo']  = "#B3CCCC";
      $config['tipoCliente']  = "Conclus�o";
    }
    
    return $config;
  }

  public function marcarLido($local) {
    $sql = new Query($this->bd);
    $txt = "UPDATE HSSMENSA SET DVISUMENSA = SYSDATE, NVISUUSUA = :operador WHERE NNUMEATEND = :idAtendimento AND CTIPOMENSA IN ('0','3')";
    
    if ($local == 'SF')
      $txt .= " AND NNUMEATEND IN (SELECT NNUMEATEND FROM HSSATEND WHERE NNUMEATEND = :idAtendimento AND NNUMECLATE = 7)";
    else if ($_SESSION['apelido_operadora'] == 'unimedFranca')
      $txt .= " AND NNUMEATEND IN (SELECT NNUMEATEND FROM HSSATEND WHERE NNUMEATEND = :idAtendimento AND NNUMECLATE <> 7)";
    
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":idAtendimento",$this->idAtendimento);
    $sql->executeSQL($txt);
  }  
}

?>