<?php

class Conta {
  private $bd;
  private $idConta;  
  private $idLote;
  private $procedimentos     = array();
  private $pacotes           = array();
  private $taxas             = array();
  private $matmeds           = array();
  private $OPME              = array();
  private $erros             = array();
  private $anexos            = array();
  private $complementares    = array();
  private $taxasExcluidas    = array();
  private $matmedsExcluidos  = array();
  private $opmesExcluidos    = array();
  private $cids              = array();
  private $cidsExcluidos     = array();
  
  public $idLocalAtendimento; 
  public $idOperador;
  public $tipoAtendimento;
  public $tipoSaida;  
  public $nascidoTermo;  
  public $nascidoMorto;  
  public $nascidoPrematauro;  
  
  
  
  /*
  private $modalidade;
  private $apelidoOperadora;  
  private $idAcomodacao;  
  private $diaVencimento;    

  
  public $idCid;
  public $digital;
  public $motivoDigital;
  public $idMotivoDigital;
  public $idBeneficiario;
  public $idSolicitante;
  public $idExecutante;

  public $regime;
  public $natureza;

  public $idEspecialidade;
  public $hipoteseDiagnostica;
  public $indicadorDeAcidente;
  public $tipoDeDoenca;
  public $tempoDaDoenca;
  public $unidadeDeTempoDaDoenca;
  public $status;
  public $localEmissao;
  public $tipoConsulta;
  public $regimeInternacao;
  public $idGuiaPrincipal;  
  public $idGuiaPedido;
  public $tipoGuia;
  public $ptuGuia;
  public $autorizacaoOrigem;
  public $indicadorAcidente;
  public $tipoDoenca;
  public $tempoDoenca;
  public $unidadeTempoDoenca;
  public $observacoes;
  */
  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  }  
    
  public function __construct($bd) {
    $this->bd = $bd;
  }
  
  public function getIdConta() {
    return $this->idConta;
  }
  
  public function setIdConta($id) {
    $this->idConta = $id;
  }  
   
  public function adicionaProcedimento($codigo,$descricao,$quantidade,$dente,$face,$idPres=0,$idAnes=0,$idPaux=0,$idSaux=0,$idTaux=0,$idQaux=0,$idInst=0,$grau='',$via=1,$id='') {
    array_push($this->procedimentos,array($codigo,$descricao,$quantidade,$dente,$face,$idPres,$idAnes,$idPaux,$idSaux,$idTaux,$idQaux,$idInst,$grau,$via,$id));
  }    
    
  public function adicionaTaxa($codigo,$quantidade,$idTaxa,$id,$descTaxa) {
    array_push($this->taxas,array($codigo,$quantidade,$idTaxa,$id,$descTaxa));
  }  
  
  public function adicionaPacote($codigo,$quantidade,$id) {
    array_push($this->pacotes,array($codigo,$quantidade,$id));
  }  

  public function adicionaMatMed($codigo,$quantidade,$tabela,$preco,$descmatmed) {
    array_push($this->matmeds,array($codigo,$quantidade,$tabela,$preco,$descmatmed));
  }  

  public function adicionaOPME($codigo,$quantidade,$tabela,$preco,$descOPME) {
    array_push($this->OPME,array($codigo,$quantidade,$tabela,$preco,$descOPME));
  } 

  public function adicionaComplementar($conta,$atendimento) {
    array_push($this->complementares,array($conta,$atendimento));
  }
  
  public function adicionaCID($codigo) {
    array_push($this->cids,array($codigo));
  } 
  
  public function adicionaAnexo($name,$temp,$size) {
    array_push($this->anexos,array($name,$temp,$size));
  }    
    
  public function adicionaErro($descricao) {
    array_push($this->erros,array("* ".$descricao));
  }   
  
  public function qtdeProcedimentos() {
    return sizeOf($this->procedimentos);
  }
  
  public function getProcedimentos() {
    return $this->procedimentos;
  }  
  
  public function getTaxas() {
    return $this->taxas;
  }   
  
  public function getMatMeds() {
    return $this->matmeds;
  }   
  
  public function getOPMEs() {
    return $this->OPME;
  }
  
  public function getComplementares() {
    return $this->complementares;
  } 
  
  public function getCIDs() {
    return $this->cids;
  }

  public function removeProcedimentos() {
    $this->procedimentos = array();
  }   
  
  public function removeTaxas() {
    $this->taxas = array();
  }   
  
  public function removeMatMeds() {
    $this->matmeds = array();
  }   
  
  public function removeOPMEs() {
    $this->OPME = array();
  }     
  
  public function removeCIDs() {
    $this->cids = array();
  }    

  public function removeTaxa($codigo) {
    array_push($this->taxasExcluidas,array($codigo));
  } 
  
  public function removeMatMed($codigo,$tabela) {
    array_push($this->matmedsExcluidos,array($codigo,$tabela));
  }  
  
  public function removeOPME($codigo,$tabela) {
    array_push($this->opmesExcluidos,array($codigo,$tabela));
  }  
  
  public function removeCID($codigo) {
    array_push($this->cidsExcluidos,array($codigo));
  }  
  
  public function qtdeErros() {
    return sizeOf($this->erros);
  }  
  
  public function getErros() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->erros);$b++) {      
      $erro = array();
      $erro = $this->erros[$b];
      
      $desc_erros .= $erro[0]."<br>";      
    } 
    
    return $desc_erros;
  }  
   
  public function teste() {
    echo "Procedimentos: <br>";  
    for ($b = 0;$b < sizeof($this->procedimentos);$b++) {      
      $procedimento = array();
      $procedimento = $this->procedimentos[$b];      
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]." = ".$procedimento[3]."<br/>";
    }    
    
    echo "<br>Taxas: <br>";
    for ($b = 0;$b < sizeof($this->taxas);$b++) {      
      $procedimento = array();
      $procedimento = $this->taxas[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }  
    
    echo "<br>Taxas Excluidas: <br>";
    for ($b = 0;$b < sizeof($this->taxasExcluidas);$b++) {              
      $procedimento = array();
      $procedimento = $this->taxasExcluidas[$b];
        
      echo $procedimento[0]."<br/>";      
    }     

    for ($b = 0;$b < sizeof($this->pacotes);$b++) {      
      $procedimento = array();
      $procedimento = $this->pacotes[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }     
    
    for ($b = 0;$b < sizeof($this->matmeds);$b++) {      
      $procedimento = array();
      $procedimento = $this->matmeds[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }  

    for ($b = 0;$b < sizeof($this->OPME);$b++) {      
      $procedimento = array();
      $procedimento = $this->OPME[$b];
        
      echo $procedimento[0]." = ".$procedimento[1]." = ".$procedimento[2]."<br/>";
    }      
  }

  /***
    Esta fun��o carrega os dados da conta
  **/
  public function carregaDados() {
            
    $seg     = new Seguranca();
    $formata = new Formata();
    $util    = new Util();
    
    $sql = new Query($this->bd);    
    $txt = "SELECT HSSCONT.NNUMECONT,CCORTCONT,TO_CHAR(DATENCONT,'DD/MM/YYYY') ATENDIMENTO,HSSCONT.NNUMEUSUA,
                   HSSCONT.CHINTCONT,HSSCONT.CHALTCONT,HSSCONT.CAMBUCONT,TO_CHAR(DALTACONT,'DD/MM/YYYY') ALTA,CTPATCONT,CTPCOCONT,
                   HSSCONT.NNUMEPRES,HSSCONT.NDNVICONT,HSSCONT.CREGICONT,HSSCONT.CCLINCONT,HSSCONT.NLOCAPRES,
                   HSSCONT.CURGECONT,HSSCONT.NDOBICONT,HSSCONT.CIACICONT,HSSCONT.NNUMEMOTAL,HSSCONT.NOBITCID_,FINPRES.CNOMEPRES,
                   HSSCONT.NNUMELOTE,HSSCONT.CTPATCONT,HSSCONT.CTPSACONT,HSSCONT.CTPDOCONT,HSSCONT.NTPDOCONT,HSSCONT.CTTPDCONT,
                   HSSCONT.NNVITCONT,HSSCONT.NNMORCONT,HSSCONT.NNVIPCONT
              FROM HSSCONT,FINPRES
             WHERE HSSCONT.NNUMECONT = :conta
               AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES(+)";
             
    $sql->addParam(":conta",$this->idConta);
    $sql->executeQuery($txt);
    
    $this->idLote              = $sql->result("NNUMELOTE");
    $this->idBeneficiario      = $sql->result("NNUMEUSUA");       
    $this->cortesia            = $sql->result("CCORTCONT");       
    $this->dataAtendimento     = $sql->result("ATENDIMENTO");
    $this->horaInicial         = $sql->result("CHINTCONT");
    $this->horaFinal           = $sql->result("CHALTCONT");
    $this->dataAlta            = $sql->result("ALTA");
    $this->caraterAtendimento  = $sql->result("CURGECONT");
    $this->naturezaAtendimento = $sql->result("CAMBUCONT");
    $this->tipoInternacao      = $sql->result("CCLINCONT");
    $this->regimeInternacao    = $sql->result("CREGICONT");
    $this->declaracaoNascido   = $sql->result("NDNVICONT");
    $this->idLocalAtendimento  = $sql->result("NLOCAPRES");
    $this->declaracaoObito     = $sql->result("NDOBICONT");
    $this->indicadorAcidente   = $sql->result("CIACICONT");
    $this->idMotivo            = $sql->result("NNUMEMOTAL");
    $this->idCidObito          = $sql->result("NOBITCID_");
    $this->idPrestador         = $sql->result("NNUMEPRES");
    $this->nomePrestador       = $sql->result("CNOMEPRES");
    $this->tipoAtendimento     = $sql->result("CTPATCONT");
    $this->tipoConsulta        = $sql->result("CTPCOCONT");
    $this->tipoSaida           = $sql->result("CTPSACONT");
    $this->tipoDoenca          = $sql->result("CTPDOCONT");
    $this->tempoDoenca         = $sql->result("NTPDOCONT");
    $this->unidadeDoenca       = $sql->result("CTTPDCONT");
    $this->nascidoTermo        = $sql->result("NNVITCONT");
    $this->nascidoMorto        = $sql->result("NNMORCONT");
    $this->nascidoPrematuro    = $sql->result("NNVIPCONT");
    
    $sql_mot = new Query($this->bd);    
    $txt_mot = "SELECT CCOD2MOTAL
                  FROM HSSMOTAL
                 WHERE NNUMEMOTAL = :idMotivo";             
    $sql_mot->addParam(":idMotivo",$this->idMotivo);
    $sql_mot->executeQuery($txt_mot);
    
    $this->motivoSaidaTiss     = $sql_mot->result("CCOD2MOTAL");
    
    $sql_cid = new Query($this->bd);    
    $txt_cid = "SELECT CCODICID_
                  FROM HSSCID_
                 WHERE NNUMECID_ = :idCid";
    $sql_cid->addParam(":idCid",$this->idCidObito);
    $sql_cid->executeQuery($txt_cid);
    
    $this->codigoCidObito      = $sql_cid->result("CCODICID_"); 

    $sql_proc = new Query($this->bd);        
    $txt_proc = "SELECT DECODE(:natureza,'I','I',DECODE(CTIPOPMED,'C','C','E')) TIPO
                   FROM HSSPCON,HSSPMED
                  WHERE HSSPCON.NNUMECONT = :conta
                    AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                  ORDER BY 1";   
    $sql_proc->addParam(":conta",$this->idConta);
    $sql_proc->addParam(":natureza",$this->naturezaAtendimento);
    $sql_proc->executeQuery($txt_proc);
    
    $sql_gui = new Query($this->bd);    
    $txt_gui = "SELECT HSSGCON.NNUMEGUIA
                  FROM HSSGCON,HSSGUIA
                 WHERE HSSGCON.NNUMECONT = :conta 
                   AND HSSGCON.NNUMEGUIA = HSSGUIA.NNUMEGUIA
                   AND HSSGUIA.NCOMPGUIA IS NULL ";                   
    $sql_gui->addParam(":conta",$this->idConta);
    $sql_gui->executeQuery($txt_gui);
    
    $this->guiaPrincipal     = $sql_gui->result("NNUMEGUIA");    
    
    $this->tipoConta = $sql_proc->result("TIPO");
    
    /** procedimentos **/
    $sql_proc = new Query($this->bd);        
    $txt_proc = "SELECT HSSPCON.CCODIPMED,CNOMEPMED,NQUANPCON,CTIPOPMED,DECODE(CTIPOPMED,'C',0,1) ORDEM,CCODIDENTE,CCODIFACE,
                        HSSPCON.NNUMEPRES,HSSPCON.NANESPRES,HSSPCON.NPAUXPRES,HSSPCON.NSAUXPRES,HSSPCON.NTAUXPRES,
                        HSSPCON.NQAUXPRES,HSSPCON.NINSTPRES,HSSPCON.CGRAUPCON,HSSPCON.NVIA_PCON,HSSPCON.NNUMEPCON
                   FROM HSSPCON,HSSPMED,HSSDENTE,HSSFACE
                  WHERE HSSPCON.NNUMECONT = :conta
                    AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                    AND HSSPCON.NNUMEDENTE = HSSDENTE.NNUMEDENTE(+)
                    AND HSSPCON.NNUMEFACE  = HSSFACE.NNUMEFACE(+)                      
                  ORDER BY 5,1";        
    $sql_proc->addParam(":conta",$this->idConta);
    $sql_proc->executeQuery($txt_proc);
    
    while (!$sql_proc->eof()) {
      $this->adicionaProcedimento($sql_proc->result("CCODIPMED"),$sql_proc->result("CNOMEPMED"),$sql_proc->result("NQUANPCON"),
                                  $sql_proc->result("CCODIDENTE"),$sql_proc->result("CCODIFACE"),$sql_proc->result("NNUMEPRES"),
                                  $sql_proc->result("NANESPRES"),$sql_proc->result("NPAUXPRES"),$sql_proc->result("NSAUXPRES"),
                                  $sql_proc->result("NTAUXPRES"),$sql_proc->result("NQAUXPRES"),$sql_proc->result("NINSTPRES"),
                                  $sql_proc->result("CGRAUPCON"),$sql_proc->result("NVIA_PCON"),$sql_proc->result("NNUMEPCON"));
      $sql_proc->next();
    }
    
    /** taxas **/
    $sql_taxa = new Query($this->bd);        
    $txt_taxa = "SELECT NVL(HSSTAXA.CTUSSTAXA,CCODITAXA) CCODITAXA,NQUANTCON,HSSTCON.NNUMETAXA,HSSTCON.NNUMETCON,HSSTAXA.CDESCTAXA
                   FROM HSSTCON,HSSTAXA
                  WHERE HSSTCON.NNUMECONT = :conta
                    AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                  ORDER BY 1";   
    $sql_taxa->addParam(":conta",$this->idConta);                      
    $sql_taxa->executeQuery($txt_taxa); 

    while (!$sql_taxa->eof()) {
      $this->adicionaTaxa($sql_taxa->result("CCODITAXA"),$sql_taxa->result("NQUANTCON"),$sql_taxa->result("NNUMETAXA"),$sql_taxa->result("NNUMETCON"),$sql_taxa->result("CDESCTAXA"));
      $sql_taxa->next();
    }    
    
    /** Materiais e Medicamentos **/
    $sql_item = new Query($this->bd);        
    $txt_item = "SELECT REMOVE_ACENTO(NVL(RETORNA_NOME_MATERIAL(ESTPRODU.NNUMEPRODU,DDATAMCON,:versaoTiss),CNOMEPRODU)) CNOMEPRODU,NVL(RETORNA_CODIGO_MATERIAL(ESTPRODU.NNUMEPRODU,DDATAMCON,:versaoTiss),CCODIPRODU) CCODIPRODU,ESTTABMM.CCODITABMM,NPQUAMCON,CAST(NPVALMCON AS NUMBER) NPVALMCON
                   FROM HSSMCON,ESTPRODU,ESTTABMM
                  WHERE HSSMCON.NNUMECONT = :conta
                    AND HSSMCON.NNUMEPRODU = ESTPRODU.NNUMEPRODU
                    AND HSSMCON.NNUMETABMM = ESTTABMM.NNUMETABMM(+)
                  ORDER BY 1";        
    $sql_item->addParam(":conta",$this->idConta);                                              
	$sql_item->addParam(":versaoTiss",$_SESSION['versaoTiss']);
    $sql_item->executeQuery($txt_item);
      
    while (!$sql_item->eof()) {
      $this->adicionaMatMed($sql_item->result("CCODIPRODU"),$sql_item->result("NPQUAMCON"),$sql_item->result("CCODITABMM"),$sql_item->result("NPVALMCON"),$sql_item->result("CNOMEPRODU"));
      $sql_item->next();
    }  
    
    /** OPME **/
    $sql_item = new Query($this->bd);        
    $txt_item = "SELECT  NVL(RETORNA_NOME_MATERIAL(ESTPRODU.NNUMEPRODU,DDATAMMCON,:versaoTiss),CNOMEPRODU) CNOMEPRODU, NVL(RETORNA_CODIGO_MATERIAL(ESTPRODU.NNUMEPRODU,DDATAMMCON,:versaoTiss),CCODIPRODU) CCODIPRODU,ESTTABMM.CCODITABMM,NPQUAMMCON,NPVALMMCON
                   FROM HSSMMCON,ESTPRODU,ESTTABMM
                  WHERE HSSMMCON.NNUMECONT = :conta
                    AND HSSMMCON.NNUMEPRODU = ESTPRODU.NNUMEPRODU
                    AND HSSMMCON.NNUMETABMM = ESTTABMM.NNUMETABMM(+)
                  ORDER BY 1";        
    $sql_item->addParam(":conta",$this->idConta);                                              
	$sql_item->addParam(":versaoTiss",$_SESSION['versaoTiss']);
    $sql_item->executeQuery($txt_item);
      
    while (!$sql_item->eof()) {
      $this->adicionaOPME($sql_item->result("CCODIPRODU"),$sql_item->result("NPQUAMMCON"),$sql_item->result("CCODITABMM"),$sql_item->result("NPVALMMCON"),$sql_item->result("CNOMEPRODU"));
      $sql_item->next();
    }      
    
    /** Contas complementares **/
    $sql_compl = new Query($this->bd);        
    $txt_compl = "SELECT HSSCONT.NNUMECONT,TO_CHAR(DATENCONT,'DD/MM/YYYY') EMISSAO,DATENCONT 
                    FROM HSSCONT 
                   WHERE NCOMPCONT = :conta
                     AND NLOCAPRES = :contratado
                   ORDER BY 3";
    $sql_compl->addParam(":conta",$this->idConta);
    $sql_compl->addParam(":contratado",$this->idLocalAtendimento);
    $sql_compl->executeQuery($txt_compl);
    
    while (!$sql_compl->eof()) {
      $this->adicionaComplementar($sql_compl->result("NNUMECONT"),$sql_compl->result("EMISSAO"));
      $sql_compl->next();
    }
    
    /** CIDs **/
    $sql_compl = new Query($this->bd);        
    $txt_compl = "SELECT CCODICID_
                    FROM HSSCIDC,HSSCID_
                   WHERE HSSCIDC.NNUMECONT = :conta
                     AND HSSCIDC.NNUMECID_ = HSSCID_.NNUMECID_
                   ORDER BY 1";
    $sql_compl->addParam(":conta",$this->idConta);
    $sql_compl->executeQuery($txt_compl);
    
    while (!$sql_compl->eof()) {
      $this->adicionaCID($sql_compl->result("CCODICID_"));
      $sql_compl->next();
    }     
    
  }
  
  /***
    Esta fun��o salva o registro da conta
  **/
  public function save() {
            
    $seg     = new Seguranca();
    $formata = new Formata();
    $util    = new Util();
    
    $this->preValidar();
    
    if ($this->qtdeErros() == 0) {
      $seg->alteraOperador($this->bd,$this->idOperador);
      
      $sql = new Query($this->bd);
                         
      $txt = "UPDATE HSSCONT
                 SET DATENCONT  = TO_DATE(:dataAtendimento,'DD/MM/YYYY'),
                     DALTACONT  = TO_DATE(:dataAlta,'DD/MM/YYYY'),
                     CHINTCONT  = :horaInicial,
                     CHALTCONT  = :horaFinal,
                     CURGECONT  = :caraterUrgencia,
                     CCLINCONT  = :tipoInternacao,
                     CREGICONT  = :regimeInternacao,
                     NDNVICONT  = :declaracaoVivo,
                     NOBITCID_  = :cidObito,
                     NDOBICONT  = :declaracaoObito,
                     CIACICONT  = :indicadorAcidente,
                     NNUMEMOTAL = :idMotivoSaida,
                     CTPATCONT  = :tipoAtendimento,
                     CTPSACONT  = :tipoSaida,
                     CTPDOCONT  = :tipoDoenca,
                     NTPDOCONT  = :tempoDoenca,
                     CTTPDCONT  = :unidadeDoenca,
                     CTPCOCONT  = :tipoConsulta,
                     NNVITCONT  = :nascidoTermo,
                     NNMORCONT  = :nascidoMorto,
                     NNVIPCONT  = :nascidoPrematuro
               WHERE NNUMECONT  = :conta";
                                         
      $sql->addParam(":conta"             ,$this->idConta);
      $sql->addParam(":dataAtendimento"   ,$this->dataAtendimento);
      $sql->addParam(":dataAlta"          ,$this->dataAlta);
      $sql->addParam(":horaInicial"       ,$this->horaInicial);
      $sql->addParam(":horaFinal"         ,$this->horaFinal);
      $sql->addParam(":caraterUrgencia"   ,$this->caraterAtendimento);
      $sql->addParam(":tipoInternacao"    ,$this->tipoInternacao);
      $sql->addParam(":regimeInternacao"  ,$this->regimeInternacao);
      $sql->addParam(":declaracaoVivo"    ,$formata->somenteNumeros($this->declaracaoNascido));      
      $sql->addParam(":cidObito"          ,$this->idCidObito);        
      $sql->addParam(":declaracaoObito"   ,$formata->somenteNumeros($this->declaracaoObito));
      $sql->addParam(":indicadorAcidente" ,$this->indicadorAcidente);
      $sql->addParam(":idMotivoSaida"     ,$this->idMotivoSaida);
      $sql->addParam(":tipoSaida"         ,$this->tipoSaida);
      $sql->addParam(":tipoAtendimento"   ,$this->tipoAtendimento);
      $sql->addParam(":tipoDoenca"        ,$this->tipoDoenca);
      $sql->addParam(":tempoDoenca"       ,$this->tempoDoenca);
      $sql->addParam(":unidadeDoenca"     ,$this->unidadeDoenca);            
      $sql->addParam(":tipoConsulta"      ,$this->tipoConsulta);            
      $sql->addParam(":nascidoTermo"      ,$this->nascidoTermo);            
      $sql->addParam(":nascidoMorto"      ,$this->nascidoMorto);            
      $sql->addParam(":nascidoPrematuro"  ,$this->nascidoPrematuro);            
    
      $erro = $sql->executeSQL($txt);

      if ($erro == '') {
        /** Procedimentos **/
        
        for ($i = 0;$i < sizeOf($this->procedimentos);$i++) {      
          $procedimento = array();
          $procedimento = $this->procedimentos[$i];
          
          $sql_existe = new Query($this->bd);
          $txt_existe = "SELECT NNUMEPRES,NANESPRES,NPAUXPRES,NSAUXPRES,NTAUXPRES,NQAUXPRES,NINSTPRES
                           FROM HSSPCON 
                          WHERE NNUMECONT = :conta 
                            AND CCODIPMED = :procedimento";
          $sql_existe->addParam(":conta",$this->idConta);
          $sql_existe->addParam(":procedimento",$procedimento[0]);
          $erro .=$sql_existe->executeQuery($txt_existe); 
                  
          if ($sql_existe->count() > 0) {
                    
            $sql_proc = new Query($this->bd);
            $txt_proc = "UPDATE HSSPCON 
                            SET NNUMEPRES = :prestador,
                                NANESPRES = :anes,
                                NPAUXPRES = :prim,
                                NSAUXPRES = :segu,
                                NTAUXPRES = :terc,
                                NQAUXPRES = :quar,
                                NINSTPRES = :inst,
                                CGRAUPCON = :grau,
                                NVIA_PCON = :via
                          WHERE NNUMECONT = :conta
                            AND CCODIPMED = :procedimento";
                            
            if ($procedimento[14] > 0) {
              $txt_proc .= "   AND NNUMEPCON = :pcon ";
              $sql_proc->addParam(":pcon" ,$procedimento[14]);
            }            
            
            $sql_proc->addParam(":conta",$this->idConta);
            $sql_proc->addParam(":procedimento",$procedimento[0]);
            $sql_proc->addParam(":prestador",$procedimento[5]);
            $sql_proc->addParam(":anes",$procedimento[6]);
            $sql_proc->addParam(":prim",$procedimento[7]);
            $sql_proc->addParam(":segu",$procedimento[8]);
            $sql_proc->addParam(":terc",$procedimento[9]);
            $sql_proc->addParam(":quar",$procedimento[10]);
            $sql_proc->addParam(":inst",$procedimento[11]);
            $sql_proc->addParam(":grau",$procedimento[12]);
            $sql_proc->addParam(":via" ,$procedimento[13]);
            $erro .=$sql_proc->executeSQL($txt_proc);           
          }
        }  
        
        /** Taxas **/
        for ($i = 0;$i < sizeOf($this->taxas);$i++) {      
          $taxa = array();
          $taxa = $this->taxas[$i];
          
          if ($taxa[3] > 0) {
            $sql_existe = new Query($this->bd);
            $txt_existe = "SELECT * FROM HSSTCON WHERE NNUMECONT = :conta AND NNUMETCON = :id";
            $sql_existe->addParam(":conta",$this->idConta);
            $sql_existe->addParam(":id",$taxa[3]);
            $erro .=$sql_existe->executeQuery($txt_existe); 
      
            if ($sql_existe->count() > 0) {
              $sql_taxa = new Query($this->bd);
              $txt_taxa = "UPDATE HSSTCON SET NQUANTCON = :qtde WHERE NNUMECONT = :conta AND NNUMETCON = :id";
              $sql_taxa->addParam(":conta",$this->idConta);
              $sql_taxa->addParam(":id",$taxa[3]);
              $sql_taxa->addParam(":qtde",$taxa[1]);
              $erro .=$sql_taxa->executeSQL($txt_taxa);           
            }
            else {
              $sql_taxa = new Query($this->bd);
              $txt_taxa = "BEGIN FATURA_TAXA_WEB(:conta,:taxa,:qtde); END;";
              $sql_taxa->addParam(":conta",$this->idConta);
              $sql_taxa->addParam(":taxa",$taxa[2]);
              $sql_taxa->addParam(":qtde",$taxa[1]);
              $erro .=$sql_taxa->executeSQL($txt_taxa);               
            }			
          }
          else {
            $sql_existe = new Query($this->bd);
            $txt_existe = "SELECT * FROM HSSTCON WHERE NNUMECONT = :conta AND NNUMETAXA = :taxa";
            $sql_existe->addParam(":conta",$this->idConta);
            $sql_existe->addParam(":taxa",$taxa[2]);
            $erro .=$sql_existe->executeQuery($txt_existe); 		  
                
            if ($sql_existe->count() > 0) {
              $sql_taxa = new Query($this->bd);
              $txt_taxa = "UPDATE HSSTCON SET NQUANTCON = :qtde WHERE NNUMECONT = :conta AND NNUMETAXA = :taxa";
              $sql_taxa->addParam(":conta",$this->idConta);
              $sql_taxa->addParam(":taxa",$taxa[2]);
              $sql_taxa->addParam(":qtde",$taxa[1]);
              $erro .=$sql_taxa->executeSQL($txt_taxa);           
            }
            else {
              $sql_taxa = new Query($this->bd);
              $txt_taxa = "BEGIN FATURA_TAXA_WEB(:conta,:taxa,:qtde); END;";
              $sql_taxa->addParam(":conta",$this->idConta);
              $sql_taxa->addParam(":taxa",$taxa[2]);
              $sql_taxa->addParam(":qtde",$taxa[1]);
              $erro .=$sql_taxa->executeSQL($txt_taxa);               
            }	            
		      }
        }    

        for ($i = 0;$i < sizeOf($this->taxasExcluidas);$i++) {      
          $taxa = array();
          $taxa = $this->taxasExcluidas[$i];
          
          $sql_taxa = new Query($this->bd);
          $txt_taxa = "DELETE FROM HSSTCON WHERE NNUMECONT = :conta AND NNUMETAXA = :taxa";
          $sql_taxa->addParam(":conta",$this->idConta);
          $sql_taxa->addParam(":taxa",$taxa[1]);
          $erro .=$sql_taxa->executeSQL($txt_taxa);         
        }       

        /** Materiais e Medicamentos **/
        for ($i = 0;$i < sizeOf($this->matmeds);$i++) {      
          $matmed = array();
          $matmed = $this->matmeds[$i];
          
          $sql_existe = new Query($this->bd);
          $txt_existe = "SELECT * FROM HSSMCON WHERE NNUMECONT = :conta AND NNUMEPRODU = :produto";
          $sql_existe->addParam(":conta",$this->idConta);
          $sql_existe->addParam(":produto",$matmed[4]);
          $erro .=$sql_existe->executeQuery($txt_existe);
          //sdebug($sql_existe->count(),true); 					

          $sql_tabelas = new Query($this->bd);
          $txt_tabelas = "SELECT DISTINCT TABELA.NNUMETABMM,ESTTABMM.CCODITABMM
                            FROM HSSLINEA,(SELECT NNUMETABMM,NNUMEPRES,DVIGELINEA, 1 ORDEM FROM HSSLINEA WHERE NNUMETABMM > 0
                                            UNION ALL
                                           SELECT NSEGUTABMM,NNUMEPRES,DVIGELINEA, 2 ORDEM FROM HSSLINEA WHERE NSEGUTABMM > 0
                                            UNION ALL
                                           SELECT NTERCTABMM,NNUMEPRES,DVIGELINEA, 3 ORDEM FROM HSSLINEA WHERE NTERCTABMM > 0
                                            UNION ALL
                                           SELECT NQUARTABMM,NNUMEPRES,DVIGELINEA, 4 ORDEM FROM HSSLINEA WHERE NQUARTABMM > 0
                                            UNION ALL
                                           SELECT NQUINTABMM,NNUMEPRES,DVIGELINEA, 5 ORDEM FROM HSSLINEA WHERE NQUARTABMM > 0
                                            UNION ALL
                                           SELECT NSEXTTABMM,NNUMEPRES,DVIGELINEA, 6 ORDEM FROM HSSLINEA WHERE NQUARTABMM > 0
                                             UNION ALL
                                           SELECT NSETITABMM,NNUMEPRES,DVIGELINEA, 7 ORDEM FROM HSSLINEA WHERE NQUARTABMM > 0
                                             UNION ALL
                                           SELECT NOITATABMM,NNUMEPRES,DVIGELINEA, 8 ORDEM FROM HSSLINEA WHERE NQUARTABMM > 0) TABELA,ESTTABMM
                           WHERE HSSLINEA.NNUMEPRES = :prestador                                   
                             AND HSSLINEA.NNUMEPRES = TABELA.NNUMEPRES
                             AND HSSLINEA.DVIGELINEA = TABELA.DVIGELINEA
                             AND HSSLINEA.DVIGELINEA = (SELECT MAX(LINEA.DVIGELINEA) FROM HSSLINEA LINEA
                                                         WHERE LINEA.NNUMEPRES = :prestador
                                                           AND LINEA.DVIGELINEA <= SYSDATE)
                             AND ESTTABMM.CCODITABMM = :codigoTabela
                             AND TABELA.NNUMETABMM = ESTTABMM.NNUMETABMM
                           ORDER BY ESTTABMM.CCODITABMM";  
                               
          $sql_tabelas->addParam(":prestador",$_SESSION['id_contratado']);
          $sql_tabelas->addParam(":codigoTabela",$matmed[2]);
          $erro .= $sql_tabelas->executeQuery($txt_tabelas);
          
          if ($sql_existe->count() > 0) {
            $sql_matmed = new Query($this->bd);
            /*$txt_matmed = "UPDATE HSSMCON 
                              SET NPQUAMCON = :qtde,
                                  NAQUAMCON = :qtde, 
                                  NAVALMCON = DECODE(:valor,NULL,NAVALMCON,:valor),
                                  NPVALMCON = DECODE(:valor,NULL,NPVALMCON,:valor)
                            WHERE NNUMECONT = :conta AND NNUMEPRODU = :produto";*/
            $txt_matmed = "BEGIN FATURA_MATMED_WEB30(:conta,:produto,:qtde,'M',:valor,:tabela,'R'); END;";                
            $sql_matmed->addParam(":conta",$this->idConta);
            $sql_matmed->addParam(":produto",$matmed[4]);
            $sql_matmed->addParam(":qtde",$matmed[1]);
            $sql_matmed->addParam(":tabela",$sql_tabelas->result("NNUMETABMM"));
            $sql_matmed->addParam(":valor",$matmed[3]);
						//sdebug($matmed['valor'],false);
						//sdebug($matmed[3],true);
            $erro .=$sql_matmed->executeSQL($txt_matmed);           
          }
          else {
            $sql_matmed = new Query($this->bd);
            $txt_matmed = "BEGIN FATURA_MATMED_WEB30(:conta,:produto,:qtde,'M',:valor,:tabela); END;";
            $sql_matmed->addParam(":conta",$this->idConta);
            $sql_matmed->addParam(":produto",$matmed[4]);
            $sql_matmed->addParam(":qtde",$matmed[1]);
            $sql_matmed->addParam(":tabela",$sql_tabelas->result("NNUMETABMM"));
            $sql_matmed->addParam(":valor",$matmed[3]);
            $erro .=$sql_matmed->executeSQL($txt_matmed);               
          }
        }       

        for ($i = 0;$i < sizeOf($this->matmedsExcluidos);$i++) {      
          $matmed = array();
          $matmed = $this->matmedsExcluidos[$i];
          
          $sql_matmed = new Query($this->bd);
          $txt_matmed = "DELETE FROM HSSMCON WHERE NNUMECONT = :conta AND NNUMEPRODU = :produto";
          $sql_matmed->addParam(":conta",$this->idConta);
          $sql_matmed->addParam(":produto",$matmed[2]);
          $erro .= $sql_matmed->executeSQL($txt_matmed);         
        }  

        /** OPME **/
        for ($i = 0;$i < sizeOf($this->OPME);$i++) {      
          $matmed = array();
          $matmed = $this->OPME[$i];
          
          $sql_existe = new Query($this->bd);
          $txt_existe = "SELECT * FROM HSSMMCON WHERE NNUMECONT = :conta AND NNUMEPRODU = :produto";
          $sql_existe->addParam(":conta",$this->idConta);
          $sql_existe->addParam(":produto",$matmed[4]);
          $erro .=$sql_existe->executeQuery($txt_existe); 
                  
          if ($sql_existe->count() > 0) {
            $sql_matmed = new Query($this->bd);
            $txt_matmed = "UPDATE HSSMMCON 
                              SET NPQUAMMCON = :qtde,
                                  NAQUAMMCON = :qtde,
                                  NAVALMMCON = DECODE(:valor,NULL,NAVALMMCON,:valor),
                                  NPVALMMCON = DECODE(:valor,NULL,NPVALMMCON,:valor)
                            WHERE NNUMECONT = :conta AND NNUMEPRODU = :produto";
            $sql_matmed->addParam(":conta",$this->idConta);
            $sql_matmed->addParam(":produto",$matmed[4]);
            $sql_matmed->addParam(":qtde",$matmed[1]);
            $sql_matmed->addParam(":valor",$matmed[3]);
           
            $erro .=$sql_matmed->executeSQL($txt_matmed);           
          }
          else {
            $sql_matmed = new Query($this->bd);
            $txt_matmed = "BEGIN FATURA_MATMED_WEB30(:conta,:produto,:qtde,'O',:valor); END;";
            $sql_matmed->addParam(":conta",$this->idConta);
            $sql_matmed->addParam(":produto",$matmed[4]);
            $sql_matmed->addParam(":qtde",$matmed[1]);
            $sql_matmed->addParam(":valor",$matmed[3]);
            $erro .=$sql_matmed->executeSQL($txt_matmed);               
          }
        }       

        for ($i = 0;$i < sizeOf($this->opmesExcluidos);$i++) {      
          $matmed = array();
          $matmed = $this->opmesExcluidos[$i];
          
          $sql_matmed = new Query($this->bd);
          $txt_matmed = "DELETE FROM HSSMMCON WHERE NNUMECONT = :conta AND NNUMEPRODU = :produto";
          $sql_matmed->addParam(":conta",$this->idConta);
          $sql_matmed->addParam(":produto",$matmed[2]);
          $erro .= $sql_matmed->executeSQL($txt_matmed);         
        }    

        /** CID **/
        for ($i = 0;$i < sizeOf($this->cids);$i++) {      
          $cid = array();
          $cid = $this->cids[$i];
          
          $sql_existe = new Query($this->bd);
          $txt_existe = "SELECT * FROM HSSCIDC WHERE NNUMECONT = :conta AND NNUMECID_ = :cid";
          $sql_existe->addParam(":conta",$this->idConta);
          $sql_existe->addParam(":cid",$cid[1]);
          $erro .=$sql_existe->executeQuery($txt_existe); 
                  
          if ($sql_existe->count() == 0) {
            $sql_cid = new Query($this->bd);
            $txt_cid = "INSERT INTO HSSCIDC (NNUMECONT,NNUMECID_) VALUES (:conta,:cid)";
            $sql_cid->addParam(":conta",$this->idConta);
            $sql_cid->addParam(":cid",$cid[1]);           
            $erro .= $sql_cid->executeSQL($txt_cid);           
          }
        }       

        for ($i = 0;$i < sizeOf($this->cidsExcluidos);$i++) {      
          $cid = array();
          $cid = $this->cidsExcluidos[$i];
          
          $sql_cid = new Query($this->bd);
          $txt_cid = "DELETE FROM HSSCIDC WHERE NNUMECONT = :conta AND NNUMECID_ = :cid";
          $sql_cid->addParam(":conta",$this->idConta);
          $sql_cid->addParam(":cid",$cid[1]);        
          $erro .= $sql_cid->executeSQL($txt_cid);   
        }          
      }

      if ($erro == '') { 
     
        $sql->clear();
        $txt = "BEGIN ".
               "  ATUALIZA_HONORARIOS_MEDICOS('S',:conta,0); ".
               "  ATUALIZAMATMEDOPMECONTA(:conta); ".               
               "  FATURAMENTO_CONTA(:conta); ".
               "  ATUALIZA_VALOR_LOTE30(:lote); ".
               "END; ";
        $sql->addParam(":conta",$this->idConta);
        $sql->addParam(":lote",$this->idLote);
        $erro = $sql->executeSQL($txt);      
      }
                  
      return $erro;
    }
  }
  
  /***
    Esta fun��o avalia os dados da guia, cobertura, carencia....
  **/  
  function validarGuia() {
    $func = new Funcao();
    $sql  = new Query($this->bd);
    $txt = "BEGIN ".
           "  GUIA.VALIDAR(:guia,:operador,:localEmissao,'N',:tipoGuia);".
           "END;";          
    $sql->addParam(":guia",$this->idGuia);
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":localEmissao",$this->localEmissao);
    $sql->addParam(":tipoGuia",$this->tipoGuia);
    $sql->executeSQL($txt);  
    
    $sql = new Query($this->bd);
    $txt = "SELECT CCODIPMED || ' - ' || GUIA.descricaoCritica(NMOTICRITI) MOTIVO ,NMOTICRITI,CCODIPMED
              FROM HSSCRITI
             WHERE NNUMEGUIA = :guia
               AND NNUMETAXA  IS NULL
               AND NNUMEDENTE IS NULL
               AND NNUMEFACE  IS NULL
             UNION ALL
            SELECT NVL(HSSTAXA.CTUSSTAXA,CCODITAXA) CCODITAXA || ' - ' || GUIA.descricaoCritica(NMOTICRITI) MOTIVO,NMOTICRITI,CCODIPMED 
              FROM HSSCRITI,HSSTAXA
             WHERE NNUMEGUIA = :guia
               AND HSSCRITI.NNUMETAXA = HSSTAXA.NNUMETAXA
             UNION ALL
            SELECT CCODIPMED || ' - (Dente: ' || CCODIDENTE || '/Face: ' || CCODIFACE || ')' || GUIA.descricaoCritica(NMOTICRITI) MOTIVO ,NMOTICRITI,CCODIPMED
              FROM HSSCRITI,HSSDENTE,HSSFACE
             WHERE NNUMEGUIA = :guia
               AND HSSCRITI.NNUMEDENTE = HSSDENTE.NNUMEDENTE
               AND HSSCRITI.NNUMEFACE  = HSSFACE.NNUMEFACE(+) ";
    $sql->addParam(":guia",$this->idGuia);
    $sql->executeQuery($txt);  
    
    while (!$sql->eof()) {

      // Usuario inadimplente
      if ($sql->result("NMOTICRITI") == 10)        
        $this->adicionaErro('N�o foi poss�vel emitir esta autoriza��o. Gentileza entrar em contato com a Operadora.');
      else {
              
        if ($sql->result("NMOTICRITI") <> 25) {
        
          if ($_SESSION['auditoria_guias'] == '3') {
            if ($sql->result("NMOTICRITI") <> 7)
              $this->adicionaErro($sql->result("MOTIVO"));
          } else 
            $this->adicionaErro($sql->result("MOTIVO"));
        }
      }
        
      if ($sql->result("NMOTICRITI") == 6)        
        $this->adicionaErro($func->listaRetorno($this->bd,$this->idBeneficiario,$this->idExecutante,$sql->result("CCODIPMED")));
        
      $sql->next();
    }

  }
    
  /***
    Esta fun��o faz avalia��o de integridade dos itens
  **/   
  function preValidar() {
    $func = new Funcao();
    
    if ($this->motivoSaidaTiss <> '') {
      $sql_mot = new Query($this->bd);    
      $txt_mot = "SELECT NNUMEMOTAL
                    FROM HSSMOTAL
                   WHERE CCOD2MOTAL = :codigo";             
      $sql_mot->addParam(":codigo",$this->motivoSaidaTiss);
      $sql_mot->executeQuery($txt_mot);
      
      if ($sql_mot->count() > 0)
        $this->idMotivoSaida = $sql_mot->result("NNUMEMOTAL");
      else
        $this->idMotivoSaida = '';
    }
    else
      $this->idMotivoSaida = '';
    
    if ($this->codigoCidObito <> '') {
      $this->idCidObito = $func->validaCid($this->bd,$this->codigoCidObito);
    
      if ($this->idCidObito == 0) {
        $this->idCidObito = '';
        $this->adicionaErro($this->codigoCidObito.'- C.I.D. inv�lido!');
      }  
    }
    else
      $this->idCidObito = "";
      
    /** cids **/
    for ($p = 0;$p < sizeof($this->cids);$p++) {        
      $item = array();
      $item = $this->cids[$p];
      
      $idCid = $func->validaCid($this->bd,$item[0]);
      
      if ($idCid == 0) {
        $this->adicionaErro($item[0].'- C.I.D. inv�lido!');
      }        
      else {
        $this->cids[$p] = array($item[0],$idCid);
      }
    } 
    
    /** CIDs excluidos **/
    for ($p = 0;$p < sizeof($this->cidsExcluidos);$p++) {        
      $item = array();
      $item = $this->cidsExcluidos[$p];
      
      $idCid = $func->validaCid($this->bd,$item[0]);
      
      if ($idCid == 0) {
        $this->adicionaErro($item[0].'- C.I.D. inv�lido!');
      }        
      else {
        $this->cidsExcluidos[$p] = array($item[0],$idCid);
      }            
    }         
            
    /** Procedimentos **/
    for ($p = 0;$p < sizeof($this->procedimentos);$p++) {        
      $item = array();
      $item = $this->procedimentos[$p];
      
      $sql_proc = new Query($this->bd);
      $txt_proc = "SELECT CCODIPMED,CSITUPMED FROM HSSPMED WHERE CCODIPMED = :procedimento";                                   
      $sql_proc->addParam(":procedimento",$item[0]);
      $erro = $sql_proc->executeQuery($txt_proc);
      
      if ($sql_proc->result("CCODIPMED") == '')
        $this->adicionaErro($item[0]." - C�digo do procedimento n�o existe.");
      else if ($sql_proc->result("CSITUPMED") <> 'A')
        $this->adicionaErro($item[0]." - C�digo do procedimento inativo.");
    } 
    
    /** Taxas **/
    for ($p = 0;$p < sizeof($this->taxas);$p++) {        
      $item    = array();
      $item    = $this->taxas[$p];
      $id_taxa = $func->validaTaxa($this->bd,$item[0],$_SESSION['id_contratado']);
      
      if ($id_taxa > 0)          
        $this->taxas[$p] = array($item[0],$item[1],$id_taxa,$item[3]);
      else {
        $this->adicionaErro($item[0]." - C�digo de taxa n�o existe ou n�o autorizado.");
      }                     
    }     
    
    /** Taxas excluidas **/    
    for ($p = 0;$p < sizeof($this->taxasExcluidas);$p++) {        
      $item    = array();
      $item    = $this->taxasExcluidas[$p];
      $id_taxa = $func->validaTaxa($this->bd,$item[0],$_SESSION['id_contratado']);
      
      if ($id_taxa > 0)          
        $this->taxasExcluidas[$p] = array($item[0],$id_taxa);
      else {
        $this->adicionaErro($item[0]." - C�digo de taxa n�o existe ou n�o autorizado.");
      }                     
    }     
          
    /** Pacotes **/
    for ($p = 0;$p < sizeof($this->pacotes);$p++) {        
      $item = array();
      $item = $this->pacotes[$p];

      if ($item[2] > 0) 
        $id_pacote = $item[2];
      else {
        $sql_id = new Query($this->bd);
        $txt_id = "SELECT RETORNAIDPACOTE30(:pacote,:executante,:locaAtendimento) ID_PACOTE FROM DUAL";
        $sql_id->addParam(":pacote",$item[0]);
        $sql_id->addParam(":executante",$this->idExecutante);  
        $sql_id->addParam(":locaAtendimento",$this->idLocalAtendimento);  
        $erro = $sql_id->executeQuery($txt_id); 
        $id_pacote = $sql_id->result("ID_PACOTE");
      }                      
      
      if ($id_pacote > 0)
        $this->pacotes[$p] = array($item[0],$item[1],$id_pacote);        
      else
        $this->adicionaErro($item[0]." - C�digo do pacote n�o existe.");
    }  
        
        
    /** Materiais e Medicamentos **/
    for ($p = 0;$p < sizeof($this->matmeds);$p++) {        
      $item      = array();
      $item      = $this->matmeds[$p];         

      if ($func->parametroString($bd,"CLOCA_OPPRE") == "1")
        $id_matmed = $func->validaMatmed($this->bd,$item[0],$this->idPrestador,'',$item[2],'1',$this->idBeneficiario);
      else  
        $id_matmed = $func->validaMatmed($this->bd,$item[0],$this->idLocalAtendimento,'',$item[2],'1',$this->idBeneficiario);       
     
      if ($id_matmed > 0)
        $this->matmeds[$p] = array($item[0],$item[1],$item[2],$item[3],$id_matmed);
      else
        $this->adicionaErro($item[0]." - C�digo de material/medicamento n�o existe ou n�o autorizado.");            
    }    
    
    /** Materiais e Medicamentos excluidos **/
    for ($p = 0;$p < sizeof($this->matmedsExcluidos);$p++) {        
      $item      = array();
      $item      = $this->matmedsExcluidos[$p];   
      
      if ($func->parametroString($bd,"CLOCA_OPPRE") == "1")      
        $id_matmed = $func->validaMatmed($this->bd,$item[0],$this->idPrestador,'',$item[1],'1',$this->idBeneficiario);
      else
        $id_matmed = $func->validaMatmed($this->bd,$item[0],$this->idLocalAtendimento,'',$item[1],'1',$this->idBeneficiario); 
      
      if ($id_matmed > 0)
        $this->matmedsExcluidos[$p] = array($item[0],$item[1],$id_matmed);
      else
        $this->adicionaErro($item[0]." - C�digo de material/medicamento n�o existe ou n�o autorizado.");            
    }    
    
    
    /** OPMEs **/
    for ($p = 0;$p < sizeof($this->OPME);$p++) {        
      $item      = array();
      $item      = $this->OPME[$p];          
      $id_opme = $func->validaMatmed($this->bd,$item[0],$this->idLocalAtendimento,'',$item[2],'O',$this->idBeneficiario);
          
      if ($id_opme > 0)
        $this->OPME[$p] = array($item[0],$item[1],$item[2],$item[3],$id_opme);
      else 
        $this->adicionaErro($item[0]." - C�digo do OPME n�o existe ou n�o autorizado.");              
    } 

    /** OPMEs excluidos **/
    for ($p = 0;$p < sizeof($this->opmesExcluidos);$p++) {        
      $item      = array();
      $item      = $this->opmesExcluidos[$p];          
      $id_opme   = $func->validaMatmed($this->bd,$item[0],$this->idLocalAtendimento,'',$item[1],'O');
          
      if ($id_opme > 0)
        $this->opmesExcluidos[$p] = array($item[0],$item[1],$id_opme);
      else 
        $this->adicionaErro($item[0]." - C�digo do OPME n�o existe ou n�o autorizado.");              
    }       
    
  }
  
  /***
    Esta fun��o exclui a conta
  **/   
  function excluir() {  
    $delete = new Query();
    $txt_delete = "DELETE FROM HSSCONT WHERE NNUMECONT = :conta";
    $delete->addParam(":conta",$this->idConta);
    $delete->executeSQL($txt_delete);  
  }
}


?>