<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();
  require_once('../comum/sessao.php'); 
  
  define("FPDF_FONTPATH", "../comum/pdf/font");  
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  
  $bd      = new Oracle();
  $formata = new Formata();
  $seg     = new Seguranca();
  
  ini_set("max_execution_time", 90);
        
  class PDF extends PDFSolus {
    
    function Header() {
      $formata = new Formata();
      
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      
      if (($this->getArray("Layout") == ",") or
          ($this->getArray("Layout") == "]") or
          ($this->getArray("Layout") == ";") or      
          ($this->getArray("Layout") == "`") or
          ($this->getArray("Layout") == "<")) {       
          
        $this->Cell(40,5,'');      
        $this->Cell(130,5,$_SESSION['nome_operadora'],0,0);
        
        $x = $this->GetX();
        $y = $this->GetY();
        
        $this->Cell(20,5,'',0,1);      
        
        $this->logoANS($formata->formataRegistroAns($_SESSION['registro_ans']),$x,$y+3);
      
        $this->SetFont('Arial','B',10);      
        $this->Cell(40,5,'');
        $this->Cell(150,5,"CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
        
        $this->Cell(40,5,'');
        $this->Cell(150,5,$_SESSION['endereco_operadora'],0,1);

        $this->Cell(40,5,'');
        $this->Cell(150,5,$_SESSION['bairro_operadora']." - ".$_SESSION['cidade_operadora']."-".$_SESSION['uf_operadora'],0,1);      
      } else {       
        $this->Cell(40,5,'');      
        $this->Cell(130,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,0);
        
        $x = $this->GetX();
        $y = $this->GetY();
        
        $this->Cell(20,5,'',0,1);      
        
        $this->logoANS($formata->formataRegistroAns($_SESSION['registro_ans']),$x,$y+3);      
      
        $this->SetFont('Arial','B',8);      
        $this->Cell(40,4,'');
        $this->Cell(40,4,"Empresa:",0,0,'R');
        $this->Cell(110,4,$this->getArray("Empresa"),0,1);
        $this->Cell(40,4,'');
        $this->Cell(40,4,"Vencimento:",0,0,'R');
        $this->Cell(110,4,$this->getArray("Vencimento"),0,1);
        $this->Cell(40,4,'');
        $this->Cell(40,4,"Per�odo de cobertura:",0,0,'R');
        $this->Cell(110,4,$this->getArray("Periodo"),0,1);
      
      }
      
      $this->Ln(2);
      $this->Cell(190,1,' ','B',0);
      $this->Ln(2);
    }

    function Totalizacao_plano($bd,$id_pagamento,$id_setor=0) {
      $formata = new Formata();
  
      $txt_plano = "SELECT CCODIPLAN,CDESCPLAN,
                           SUM(DECODE(FE.CTIPOUSUPG,'T',1,'F',1,0)) QTDE_TITULARES,
                           SUM(DECODE(FE.CTIPOUSUPG,'T',0,'F',0,'A',0,1)) QTDE_DEPENDENTES,
                           SUM(DECODE(FE.CTIPOUSUPG,'A',1,0)) QTDE_AGREGADOS,
                           SUM(DECODE(FE.CTIPOUSUPG,'T',VALOR,'F',VALOR,0)) VALOR_TITULARES,
                           SUM(DECODE(FE.CTIPOUSUPG,'T',0,'F',0,'A',0,VALOR)) VALOR_DEPENDENTES,
                           SUM(DECODE(FE.CTIPOUSUPG,'A',VALOR,0)) VALOR_AGREGADOS 
                      FROM FATURA_COMPOSICAO FE
                     WHERE FE.NNUMEPAGA = :id_pagamento 
                       AND FE.TIPO = 'MENSALIDADE' ";
                       
      if ($id_setor > 0)
        $txt_plano .= "   AND FE.NNUMESETOR = :id_setor";
      else if ($id_setor == 0) 
        $txt_plano .= "   AND FE.NNUMESETOR IS NULL";
                
      $txt_plano .= " GROUP BY CCODIPLAN,CDESCPLAN
                      ORDER BY 2";
    
      $sql_plano = new Query($bd);
      $sql_plano->clear();
      $sql_plano->addParam(":id_pagamento",$id_pagamento);

      if ($id_setor > 0)
        $sql_plano->addParam(":id_setor",$id_setor);

      $sql_plano->executeQuery($txt_plano);

      $this->Ln();
      $this->Ln();
      $this->Cell(190,3,'TOTALIZA��O POR PLANO',0,1);

      $this->SetFont('Arial','B',8);
      $this->Cell(70,5,'Plano','',0);
      $this->Cell(30,5,'Titulares','',0);
      $this->Cell(30,5,'Dependentes','',0);
      $this->Cell(30,5,'Agregados','',0);
      $this->Cell(30,5,'Total','',1);

      $this->Cell(70,5,'','B');
      $this->Cell(10,5,'Qtde','B',0);
      $this->Cell(20,5,'Valor','B',0);
      $this->Cell(10,5,'Qtde','B',0);
      $this->Cell(20,5,'Valor','B',0);
      $this->Cell(10,5,'Qtde','B',0);
      $this->Cell(20,5,'Valor','B',0);
      $this->Cell(10,5,'Qtde','B',0);
      $this->Cell(20,5,'Valor','B',1);

      $continua = true;
      $total_titulares = 0;
      $total_dependentes = 0;
      $total_agregados = 0;               
      $qtde_titulares = 0;
      $qtde_dependentes = 0;
      $qtde_agregados = 0;                 
      $valor_total_geral = 0;
      $qtde_total_geral = 0;

      $this->SetFont('Arial','',8);

      while (!$sql_plano->eof()) {
        $this->Cell(70,3,$this->Copy($sql_plano->result("CCODIPLAN")." - ".$sql_plano->result("CDESCPLAN"),69),0,0);
        $this->Cell(10,3,$sql_plano->result("QTDE_TITULARES"),0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($sql_plano->result("VALOR_TITULARES")),0,0,'R');
        $this->Cell(10,3,$sql_plano->result("QTDE_DEPENDENTES"),0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($sql_plano->result("VALOR_DEPENDENTES")),0,0,'R');
        $this->Cell(10,3,$sql_plano->result("QTDE_AGREGADOS"),0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($sql_plano->result("VALOR_AGREGADOS")),0,0,'R');

        $valor_total = str_replace(',','.',$sql_plano->result("VALOR_TITULARES")) +
                       str_replace(',','.',$sql_plano->result("VALOR_DEPENDENTES")) +
                       str_replace(',','.',$sql_plano->result("VALOR_AGREGADOS"));
        
        $qtde_total = $sql_plano->result("QTDE_TITULARES") +
                      $sql_plano->result("QTDE_DEPENDENTES") +
                      $sql_plano->result("QTDE_AGREGADOS");

        $total_titulares = $total_titulares + str_replace(',','.',$sql_plano->result("VALOR_TITULARES"));
        $total_dependentes = $total_dependentes + str_replace(',','.',$sql_plano->result("VALOR_DEPENDENTES"));
        $total_agregados = $total_agregados + str_replace(',','.',$sql_plano->result("VALOR_AGREGADOS"));

        $qtde_titulares = $qtde_titulares + $sql_plano->result("QTDE_TITULARES");
        $qtde_dependentes = $qtde_dependentes + $sql_plano->result("QTDE_DEPENDENTES");
        $qtde_agregados = $qtde_agregados + $sql_plano->result("QTDE_AGREGADOS");

        $valor_total_geral = $valor_total_geral + $valor_total;
        $qtde_total_geral = $qtde_total_geral + $qtde_total;

        $this->Cell(10,3,$qtde_total,0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($valor_total),0,1,'R');
        
        $sql_plano->next();
      }

      if ($valor_total_geral > 0) {
        $this->Cell(190,3,'','B',1);
        $this->Ln(2);
        $this->Cell(70,3,'','',0);
        $this->Cell(10,3,$qtde_titulares,0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($total_titulares),0,0,'R');
        $this->Cell(10,3,$qtde_dependentes,0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($total_dependentes),0,0,'R');
        $this->Cell(10,3,$qtde_agregados,0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($total_agregados),0,0,'R');
        $this->Cell(10,3,$qtde_total_geral,0,0,'R');
        $this->Cell(20,3,$formata->formataNumero($valor_total_geral),0,1,'R');
      }
    }

    function Footer()
    {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('P','mm','A4');
  $pdf->AliasNbPages();
  
  $contrato     = $_SESSION['id_contrato'];
  $id_pagamento = $_POST['id_pagamento'];
  
  if (($_SESSION['apelido_operadora'] == 'sinamed') or ($_SESSION['apelido_operadora'] == 'UnimedLestePaulista'))
    $detalhado = true;      
  else
    $detalhado = false;    
  
  $txt = "SELECT NOME_TITULAR_BOLETO(NNUMEPAGA) TITULAR, TO_CHAR(DVENCPAGA,'DD/MM/YYYY') DVENCPAGA, TO_CHAR(DGERAPAGA,'DD/MM/YYYY') DGERAPAGA, 
                 DINICPAGA,DFINAPAGA, NVENCPAGA, CLFATTITU, NDOCUPAGA, C_CGCEMPR, CCODITITU,CCIDATITU,CESTATITU,CENDETITU,CBAIRTITU,CNUMETITU,
                 CDESCTLOGR,HSSTITU.NNUMETITU, HSSTITU.NNUMEPLAN
            FROM HSSPAGA, HSSTITU, HSSEMPR, HSSTLOGR
           WHERE NNUMEPAGA = :id_pagamento
             AND HSSPAGA.NNUMETITU = HSSTITU.NNUMETITU
             AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+) 
             AND HSSTITU.NENDETLOGR = HSSTLOGR.NNUMETLOGR(+) ";
  $sql4 = new Query($bd);
  $sql4->clear();
  $sql4->addParam(":id_pagamento",$id_pagamento);
  $sql4->executeQuery($txt);

  $arr = array("Empresa" => $sql4->result("TITULAR"),
               "CNPJ" => $formata->formataCNPJ($sql4->result("CCGC_EMPR")),
               "Contrato" => $sql4->result("CCODITITU"),
               "Vencimento" => $sql4->result("DVENCPAGA"),
               "Documento" => $sql4->result("NDOCUPAGA"),
               "Periodo" => $sql4->result("DINICPAGA").' a '.$sql4->result("DFINAPAGA"),
               "Layout"=> $sql4->result("CLFATTITU"));
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);
  

  // Fatura Agemed 
  if (($sql4->result("CLFATTITU") == ",") or
      ($sql4->result("CLFATTITU") == "]") or
      ($sql4->result("CLFATTITU") == "�") or
      ($sql4->result("CLFATTITU") == ";") or      
      ($sql4->result("CLFATTITU") == "`") or
      ($sql4->result("CLFATTITU") == "<"))
      
    require_once('../empresa/faturaAgemed.php');
  
  // Fatura Servmed   
  else if ($_SESSION['apelido_operadora'] == 'servmed')

    require_once('../empresa/faturaServmed.php');
  
  // Fatura Unimed  
  else if (($_SESSION['tipo_operadora'] == "C") or
           ($sql4->result("CLFATTITU") == "�")  or 
           ($sql4->result("CLFATTITU") == "�") ) 
                     
    require_once('../empresa/faturaUnimed.php');
  
  // Fatura padr�o

  else {  
    $total_mensalidade = 0;
    $total_despesa = 0;
    $total_fatura = 0;

    $txt = "SELECT NVL(FE.CNOMESETOR,' ') LOCACAO,DECODE(FE.NTITUUSUA,FE.NNUMEUSUA,0,1) ORDEM,FE.CNOMEUSUA,FE.TITULAR,FE.CGRAUUSUA,FE.CTIPOUSUPG, 
                   SUM(DECODE(TIPO,'MENSALIDADE',FE.VALOR,0)) + SUM(DECODE(FE.TIPO,'PRO-RATA',FE.VALOR,0)) VALOR, SUM(DECODE(FE.TIPO,'ADITIVO',FE.VALOR,0)) VALOR_ADT,
                   TO_CHAR(FE.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,FE.CSITUUSUA,TO_CHAR(FE.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,FE.NNUMEUSUA,FE.CCODIPLAN,IDADE(FE.DNASCUSUA,:vencimento) IDADE, FE.CCODIUSUA, ";

    if ($_SESSION['apelido_operadora'] == 'sampes')
      $txt .=  "  NVL(FE.NNUMESETOR,0) NNUMESETOR, FE.CCODIUSUA CCHAPUSUA,FE.NTITUUSUA";
    else
      $txt .=  "  NVL(FE.NNUMESETOR,0) NNUMESETOR,DECODE(FE.CCHAPUSUA,NULL,FE.CCODIUSUA,FE.CCHAPUSUA) CCHAPUSUA,FE.NTITUUSUA";
    
      $txt .= "  FROM FATURA_COMPOSICAO FE 
           WHERE FE.NNUMEPAGA = :id_pagamento 
             AND FE.TIPO IN ('MENSALIDADE','ADITIVO','PRO-RATA') ";
                
    $txt .= "GROUP BY NVL(FE.CNOMESETOR,' '),DECODE(FE.NTITUUSUA,FE.NNUMEUSUA,0,1),FE.CNOMEUSUA,FE.TITULAR,FE.CGRAUUSUA,FE.CTIPOUSUPG, 
                      TO_CHAR(FE.DINCLUSUA,'DD/MM/YYYY'),FE.CSITUUSUA,TO_CHAR(FE.DNASCUSUA,'DD/MM/YYYY'),FE.NNUMEUSUA,FE.CCODIPLAN,IDADE(FE.DNASCUSUA,:vencimento), FE.CCODIUSUA, ";

    if ($_SESSION['apelido_operadora'] == 'sampes')
      $txt .=  "  NVL(FE.NNUMESETOR,0), FE.CCODIUSUA,FE.NTITUUSUA";
    else
      $txt .=  "  NVL(FE.NNUMESETOR,0),DECODE(FE.CCHAPUSUA,NULL,FE.CCODIUSUA,FE.CCHAPUSUA),FE.NTITUUSUA";
                                    
    $txt .=  " ORDER BY 1,4,2,5 DESC";
            
    $sql = new Query($bd);
    $sql->clear();
    $sql->addParam(":id_pagamento",$id_pagamento);
    $sql->addParam(":vencimento",$sql4->result("DVENCPAGA"));
    $sql->executeQuery($txt);

    $total_familia = 0;
    $total_locacao = 0;
    $titular = 0;
    $locacao = '';
    $id_setor = 0;

    $pdf->SetFont('Arial','B',8);
   
    If ($_SESSION['apelido_operadora'] == "vitallis") {
      $pdf->Cell(15,5,'Matr�cula','B',0);
      $pdf->Cell(15,5,'C�digo','B',0);
    } else if($_SESSION['apelido_operadora'] == "universal") //323238
      $pdf->Cell(30,5,'C�digo','B',0);
    else
      $pdf->Cell(30,5,'Matr�cula','B',0);
     
     $pdf->Cell(49,5,'Usu�rio','B',0);
     $pdf->Cell(10,5,'Plano','B',0,'R');
     $pdf->Cell(25,5,'Tipo do usu�rio','B',0);
     $pdf->Cell(18,5,'Nascimento','B',0,'C');
     $pdf->Cell(10,5,'Idade','B',0,'R');
     $pdf->Cell(18,5,'Inclus�o','B',0,'C');      
    
    if ($seg->permissaoOutros($bd,'WEBEMPRESAMOSTRARAGRUPADONAFATURAOVALORDOADITIVOMENSALIDADE',false)) {
      $pdf->Cell(15,5,'','B',0,'R');
      $pdf->Cell(15,5,'Valor','B',1,'R');
    } else {
      $pdf->Cell(15,5,'Aditivo','B',0,'R');
      $pdf->Cell(15,5,'Valor','B',1,'R');
    }
        
    $pdf->Ln(3);

    //$locacao = $sql->result("LOCACAO");
    $titular = $sql->result("NTITUUSUA");
    
    $total_aditivo = 0;
    $total_conta   = 0;
    $carteira      = 0;

    while (!$sql->eof()) {
      if ($locacao != $sql->result("LOCACAO"))  {
        if ($sql->result("NNUMESETOR") > 0) {
          $pdf->SetFont('Arial','B',8);
          $pdf->Cell(190,3,"Loca��o: ".$sql->result("LOCACAO"),0,1);
          $pdf->Ln(6);
        }
        $locacao = $sql->result("LOCACAO");
        $id_setor = $sql->result("NNUMESETOR");
      }

      if ($sql->result("NTITUUSUA") != 0) {
        $pdf->SetFont('Arial','',8);
        if (($sql->result("CTIPOUSUPG") == 'T') or
            ($sql->result("CTIPOUSUPG") == 'F')) {
          if ($_SESSION['apelido_operadora'] == "vitallis") {
            $pdf->Cell(15,3,$sql->result("CCHAPUSUA"),0,0);
            $pdf->Cell(15,3,$pdf->Copy($sql->result("CCODIUSUA"),48),0,0);
          } else if ($_SESSION['apelido_operadora'] == "universal") //323238
            $pdf->Cell(30,3,$sql->result("CCODIUSUA"),0,0);
          else
            $pdf->Cell(30,3,$sql->result("CCHAPUSUA"),0,0);
          
          $pdf->Cell(49,3,$pdf->Copy($sql->result("CNOMEUSUA"),48),0,0);
        } else { 
          if ($_SESSION['apelido_operadora'] == "vitallis") {
            $pdf->Cell(15,3,$sql->result("CCHAPUSUA"),0,0);
            $pdf->Cell(15,3,$sql->result("CCODIUSUA"),0,0);
          } else if ($_SESSION['apelido_operadora'] == "universal") //323238
            $pdf->Cell(30,3,$sql->result("CCODIUSUA"),0,0); 
          else
            $pdf->Cell(30,3,$sql->result("CCHAPUSUA"),0,0);
          
          $pdf->Cell(49,3,$pdf->Copy($sql->result("CNOMEUSUA"),48),0,0);
        }

        $pdf->Cell(10,3,$pdf->Copy($sql->result("CCODIPLAN"),9),0,0,'R');

        Switch ($sql->result("CTIPOUSUPG")) {
          case 'T' : $pdf->Cell(25,3,'Titular',0,0); 	break;
          case 'F' : $pdf->Cell(25,3,'Titular',0,0); 	break;
          case 'A' : $pdf->Cell(25,3,'Agregado',0,0);   break;
          default  : $pdf->Cell(25,3,'Dependente',0,0); break;
        }
      
        $pdf->Cell(18,3,$sql->result("DNASCUSUA"),0,0,'C');
        $pdf->Cell(10,3,$sql->result("IDADE"),0,0,'R');
        $pdf->Cell(18,3,$sql->result("DINCLUSUA"),0,0,'C');
        
        if ($seg->permissaoOutros($bd,'WEBEMPRESAMOSTRARAGRUPADONAFATURAOVALORDOADITIVOMENSALIDADE',false)) {
          $valor = str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
          $pdf->Cell(15,3,'',0,0,'R');
          $pdf->Cell(15,3,$formata->formataNumero($valor),0,1,'R');
        } else {
          $pdf->Cell(15,3,$formata->formataNumero($sql->result("VALOR_ADT")),0,0,'R');
          $pdf->Cell(15,3,$formata->formataNumero($sql->result("VALOR")),0,1,'R');      
        }
      }
      
      $total_familia = $total_familia + str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
      $total_locacao = $total_locacao + str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
      
      if ($seg->permissaoOutros($bd,'WEBEMPRESAMOSTRARAGRUPADONAFATURAOVALORDOADITIVOMENSALIDADE',false)){ 
        $total_mensalidade = $total_mensalidade + str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
        $total_aditivo = 0; //Devido o aditivo estar contido no total de mensalidade       
      } else {  
        $total_aditivo = $total_aditivo + str_replace(',','.',$sql->result("VALOR_ADT"));
        $total_mensalidade = $total_mensalidade + str_replace(',','.',$sql->result("VALOR"));
      } 
      
      $titular = $sql->result("NTITUUSUA");
      
      $sql->next();

      if (($titular != $sql->result("NTITUUSUA")) or ($sql->eof())) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(175,3,'Total da fam�lia: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_familia),0,1,'R');
        $pdf->Ln();
        $total_familia = 0;
      }

      if (($locacao != $sql->result("LOCACAO")) or ($sql->eof())) {
        $pdf->SetFont('Arial','B',8);

        if (trim($locacao) == '') 
          $pdf->Cell(175,3,'Total: ',0,0,'R');
        else
          $pdf->Cell(175,3,'Total da loca��o: ',0,0,'R');

        $pdf->Cell(15,3,$formata->formataNumero($total_locacao),0,1,'R');
        $pdf->Ln();
        $total_locacao = 0;
        
        if (trim($locacao) == '')         
          $pdf->Totalizacao_plano($bd,$id_pagamento,-1);
        else
          $pdf->Totalizacao_plano($bd,$id_pagamento,$id_setor);
        
        $pdf->Ln();
      }
    }    
    
    $pdf->Ln();
    
    $txt = " SELECT CCODIUSUA,CNOMEUSUA,VALOR                   
               FROM FATURA_COMPOSICAO FE
              WHERE FE.TIPO = 'ADESAO'
                AND FE.NNUMEPAGA = :id_Pagamento
              ORDER BY CCODIUSUA ";
    
    $sql_adesao = new Query($bd);
    $sql_adesao->clear();
    $sql_adesao->addParam(":id_pagamento",$id_pagamento);
    $sql_adesao->executeQuery($txt);          
    
    $adesao = 0;
    
    if ($sql_adesao->count() > 0) {
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(190,3,'ADES�ES COBRADAS',0,1);
      
      $pdf->Cell(30,5,'Codigo','B',0);
      $pdf->Cell(80,5,'Nome','B',0);
      $pdf->Cell(15,5,'Ades�o R$','B',1);
      $pdf->Ln(3);
      $pdf->SetFont('Arial','',8);
      
      while(!$sql_adesao->eof()) {
        $pdf->Cell(30,5,$sql_adesao->result("CCODIUSUA"),'',0);
        $pdf->Cell(80,5,$sql_adesao->result("CNOMEUSUA"),'',0);
        $pdf->Cell(15,5,$formata->formataNumero($sql_adesao->result("VALOR")),'',1,'R');
        $adesao = $adesao + str_replace(',','.',$sql_adesao->result("VALOR"));
        $sql_adesao->next();
      }
      
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(110,5,'Total: ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($adesao),0,1,'R');
      $pdf->Ln();
    }
    
    $pdf->Ln();
    
    $txt = "SELECT NVL(FE.CNOMESETOR,' ') LOCACAO,FE.CNOMEUSUA,FE.TITULAR,
                   TO_CHAR(FE.EMISSAO,'DD/MM/YYYY') DATENCONT,FE.NTITUUSUA,
                   NVL(FE.CCHAPUSUA,FE.CCODIUSUA) CCHAPUSUA,FE.CCODIUSUA,FE.ID NNUMECONT,
                   FE.PRESTADOR CNOMEPRES,FE.VALOR NVALOPACON,
                   FE.NNUMESETOR,FE.CODIGO_ITEM,FE.DESCRICAO_ITEM,FE.QTDE_ITEM                   
              FROM FATURA_COMPOSICAO FE
             WHERE FE.NNUMEPAGA = :id_pagamento
               AND FE.TIPO = 'COPARTICIPACAO_SINTETICA'
             ORDER BY 1,3,2,8 ";
             
    $sql1 = new Query($bd);
    $sql1->clear();
    $sql1->addParam(":id_pagamento",$id_pagamento);
    $sql1->executeQuery($txt);

    $total_familia = 0;
    $titular = 0;

    if ($sql1->count() > 0) {
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(190,3,'DESPESAS COBRADAS',0,1);

      $pdf->Cell(30,5,'Matr�cula','B',0);
      $pdf->Cell(30,5,'Titular','B',0);
      $pdf->Cell(39,5,'Usu�rio','B',0);
      $pdf->Cell(15,5,'Conta','B',0);
      $pdf->Cell(19,5,'Atendimento','B',0,'C');
      $pdf->Cell(12,5,'Regime','B',0);      
      $pdf->Cell(30,5,'Prestador','B',0);
      $pdf->Cell(15,5,'Valor','B',1,'R');
      $pdf->Ln(3);
      
      $locacao = '';
      $conta = 0;        

      while (!$sql1->eof()) {
      
        if ($locacao != $sql1->result("LOCACAO"))  {
          if ($sql1->result("NNUMESETOR") > 0) {
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(190,3,"Loca��o: ".$sql1->result("LOCACAO"),0,1);
            $pdf->Ln(6);
          }
          $locacao = $sql1->result("LOCACAO");
        }
      
        if ($conta <> $sql1->result("NNUMECONT")) {
          $conta = $sql1->result("NNUMECONT");
          
          $txt_regime = "SELECT DECODE(CURGECONT,'E','Eletivo','Urgente') REGIME
                           FROM HSSCONT
                          WHERE NNUMECONT = :conta ";
          $sql_regime = new Query();
          $sql_regime->addParam(":conta",$conta);
          $sql_regime->executeQuery($txt_regime);
                          
          $pdf->SetFont('Arial','',8);
          $pdf->Cell(30,3,$sql1->result("CCHAPUSUA"),'',0);
          $pdf->Cell(30,3,$pdf->Copy($sql1->result("TITULAR"),29),'',0);
          $pdf->Cell(39,3,$pdf->Copy($sql1->result("CNOMEUSUA"),38),'',0);
          $pdf->Cell(15,3,$sql1->result("NNUMECONT"),'',0);
          $pdf->Cell(19,3,$sql1->result("DATENCONT"),'',0,'C');
          $pdf->Cell(12,3,$sql_regime->result("REGIME"),'',0,'C');

          if ($detalhado)
            $pdf->Cell(30,3,$pdf->Copy($sql1->result("CNOMEPRES"),29),'',1);
          else
            $pdf->Cell(30,3,$pdf->Copy($sql1->result("CNOMEPRES"),29),'',0);
        }
        
        $total_conta = $total_conta + str_replace(',','.',$sql1->result("NVALOPACON"));
        $total_familia = $total_familia + str_replace(',','.',$sql1->result("NVALOPACON"));
        $total_despesa = $total_despesa + str_replace(',','.',$sql1->result("NVALOPACON"));
        $titular = $sql1->result("NTITUUSUA");
        $total_locacao = $total_locacao + str_replace(',','.',$sql1->result("NVALOPACON"));
      
        if ($detalhado) {
          $pdf->SetFont('Arial','',6);
          $pdf->Cell(10,3,'','',0);            
          $pdf->Cell(16,3,$sql1->result("CODIGO_ITEM"),'',0);          
          $pdf->Cell(60,3,$pdf->Copy($sql1->result("DESCRICAO_ITEM"),59),'',0);          
          $pdf->Cell(10,3,$formata->formataNumero($sql1->result("QTDE_ITEM")),'',0);                    
          $pdf->Cell(20,3,$formata->formataNumero($sql1->result("NVALOPACON")),'',1);                              
          $pdf->SetFont('Arial','',8);          
        }

        $sql1->next();
        
        if ($conta <> $sql1->result("NNUMECONT")) {
          if ($detalhado) {
            $pdf->Cell(175,5,'Total da conta:','',0,'R');                    
            $pdf->Cell(15,5,$formata->formataNumero($total_conta),'',1,'R');          
          } else
            $pdf->Cell(15,3,$formata->formataNumero($total_conta),'',1,'R');          
          
          $total_conta = 0;
        }
        
        if (($titular != $sql1->result("NTITUUSUA")) or ($sql1->eof())) {
          $pdf->SetFont('Arial','B',8);
          $pdf->Cell(175,3,'Total da fam�lia: ',0,0,'R');
          $pdf->Cell(15,3,$formata->formataNumero($total_familia),0,1,'R');
          $pdf->Ln();
          $total_familia = 0;
        }
        
        if (($locacao != $sql1->result("LOCACAO")) or ($sql1->eof())) {
          $pdf->SetFont('Arial','B',8);

          if (trim($locacao) == '') 
            $pdf->Cell(175,3,'Total: ',0,0,'R');
          else
            $pdf->Cell(175,3,'Total da loca��o: ',0,0,'R');

          $pdf->Cell(15,3,$formata->formataNumero($total_locacao),0,1,'R');
          $pdf->Ln();
          $total_locacao = 0;
          $pdf->Ln();
        }      
      }
    }
                             
    $txt_cart =  "SELECT FE.NTITUUSUA,FE.CCODIUSUA,FE.CNOMEUSUA,FE.TITULAR,FE.VALOR 
                    FROM FATURA_COMPOSICAO FE 
                   WHERE FE.TIPO = 'CARTEIRINHA'
                     AND FE.NNUMEPAGA = :id_pagamento
                   ORDER BY 4,3";

    $sql_cart = new Query($bd);
    $sql_cart->clear();
    $sql_cart->addParam(":id_pagamento",$id_pagamento);
    $sql_cart->executeQuery($txt_cart);

    $carteiras = 0;

    if ($sql_cart->count() > 0) {
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(190,3,'SEGUNDA VIA DE CARTEIRAS',0,1);

      $pdf->Cell(15,5,'C�digo','B',0);
      $pdf->Cell(80,5,'Titular','B',0);
      $pdf->Cell(80,5,'Usu�rio','B',0);
      $pdf->Cell(15,5,'Valor','B',1);
      $pdf->Ln(3);
      $pdf->SetFont('Arial','',8);

      while(!$sql_cart->eof()) {
        $pdf->Cell(15,5,$sql_cart->result("CCODIUSUA"),'',0);
        $pdf->Cell(80,5,$sql_cart->result("CNOMEUSUA"),'',0);
        $pdf->Cell(80,5,$sql_cart->result("TITULAR"),'',0);
        $pdf->Cell(15,5,$formata->formataNumero($sql_cart->result("VALOR")),'',1,'R');
        $carteira = $carteira + str_replace(',','.',$sql_cart->result("VALOR"));
        $sql_cart->next();
      }
    }
                   
    $txt_falta = "SELECT FE.VALOR,FE.CCODIUSUA,FE.EMISSAO DDATAAGEND,FE.PRESTADOR CNOMEPRES,FE.CNOMEUSUA,FE.TITULAR,
                         FE.ID NNUMEAGEND,NVL(FE.CCHAPUSUA,FE.CCODIUSUA) CCHAPUSUA,FE.NNUMEUSUA,FE.CODIGO_ITEM CHORAAGEND
                    FROM FATURA_COMPOSICAO FE
                   WHERE FE.TIPO = 'FALTA'
                     AND FE.NNUMEPAGA = :id_pagamento
                   ORDER BY 6,8 ";

    $sql_falta = new Query($bd);
    $sql_falta->clear();
    $sql_falta->addParam(":id_pagamento",$id_pagamento);
    $sql_falta->executeQuery($txt_falta);
    
    $faltas = 0;

    if ($sql_falta->count() > 0) {
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(190,3,'COBRAN�A DE FALTAS',0,1);

      $pdf->Cell(15,5,'Data','B',0);
      $pdf->Cell(10,5,'Hor�rio','B',0);
      $pdf->Cell(70,5,'Prestador','B',0);
      $pdf->Cell(80,5,'Usu�rio','B',0);
      $pdf->Cell(15,5,'Valor','B',1);
      $pdf->Ln(3);
      $pdf->SetFont('Arial','',8);

      while(!$sql_falta->eof()) {
        $pdf->Cell(15,5,$sql_falta->result("DDATAAGEND"),'',0);
        $pdf->Cell(10,5,$sql_falta->result("CHORAAGEND"),'',0);
        $pdf->Cell(70,5,$sql_falta->result("CNOMEPRES"),'',0);
        $pdf->Cell(80,5,$sql_falta->result("TITULAR"),'',0);
        $pdf->Cell(15,5,$formata->formataNumero($sql_falta->result("VALOR")),'',1,'R');
        $faltas = $faltas + str_replace(',','.',$sql_falta->result("VALOR"));
        $sql_falta->next();
      }
    }

    $txt_acres = "SELECT FE.DESCRICAO_ITEM CMOTIADPAG,FE.VALOR VALOR
                    FROM FATURA_COMPOSICAO FE
                   WHERE FE.TIPO = 'ACRESCIMO/DESCONTO'
                     AND FE.NNUMEPAGA = :id_pagamento";
    
    $sql_acres = new Query($bd);
    $sql_acres->clear();
    $sql_acres->addParam(":id_pagamento",$id_pagamento);
    $sql_acres->executeQuery($txt_acres);

    $acrescimo = 0;
    $desconto = 0;

    if ($sql_acres->count() > 0) {
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(190,3,'ACR�SCIMOS E DESCONTO',0,1);

      $pdf->Cell(100,5,'Motivo','B',0);
      $pdf->Cell(15,5,'Valor','B',1);
      $pdf->Ln(3);

      $pdf->SetFont('Arial','',8);
      while (!$sql_acres->eof()) {   
        $pdf->Cell(100,3,$sql_acres->result("CMOTIADPAG"),'',0);
        $pdf->Cell(15,3,$formata->formataNumero($sql_acres->result("VALOR")),'',1,'R');

        if ($sql_acres->result("VALOR") > 0)
          $acrescimo = $acrescimo + str_replace(',','.',$sql_acres->result("VALOR"));
        else  
          $desconto = $desconto + (str_replace(',','.',$sql_acres->result("VALOR")) * -1);

        $sql_acres->next();
      }
    }
    
    $txt_acres = "SELECT FE.VALOR
                    FROM FATURA_COMPOSICAO FE
                   WHERE FE.NNUMEPAGA = :id_pagamento
                     AND FE.TIPO = 'JUROS/MULTA' ";
    
    $sql_acres = new Query($bd);
    $sql_acres->clear();
    $sql_acres->addParam(":id_pagamento",$id_pagamento);
    $sql_acres->executeQuery($txt_acres);    
      
    $acrescimo += str_replace(',','.',$sql_acres->result("VALOR"));
    
    $txt_faturamento_m = "SELECT SUM(FE.VALOR)
                            FROM FATURA_COMPOSICAO FE
                           WHERE FE.NNUMEPAGA = :id_pagamento
                             AND FE.TIPO = 'FATURAMENTO MINIMO'";

    $sql_faturamento_m = new Query($bd);
    $sql_faturamento_m->clear();
    $sql_faturamento_m->addParam(":id_pagamento",$id_pagamento);
    $sql_faturamento_m->executeQuery($txt_faturamento_m);

    $faturamento_minimo = str_replace(',','.',$sql_faturamento_m->result("VALOR"));
    
    $txt = "SELECT NIR__NOTAF, NISS_NOTAF, NPIS_NOTAF, NCOFINOTAF, NCSLLNOTAF ".
           "  FROM HSSCNOTA,HSSNOTAF ".
           " WHERE NNUMEPAGA IN (SELECT NNUMEPAGA FROM HSSPAGA ".
           "                      WHERE NNUMEPAGA = :id_pagamento ".
           "                      UNION ".
           "                     SELECT HSSPARCE.NDESTPAGA FROM HSSPARCE ".
           "                      WHERE HSSPARCE.NNUMEPAGA = :id_pagamento ".
           "                      UNION ".
           "                     SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2 ". 
           "                      WHERE PARCE1.NDESTPAGA = :id_pagamento ".
           "                        AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ".
           "   AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF ".
           "   AND CSITUNOTAF = 'A' ";

    $sql2 = new Query($bd);
    $sql2->clear();
    $sql2->addParam(":id_pagamento",$id_pagamento);
    $sql2->executeQuery($txt);
    
    $txt_d_ban = "SELECT SUM(NVL(NDBANPAGA,0)) VALOR FROM HSSPAGA ".
                 " WHERE NNUMEPAGA IN (SELECT NNUMEPAGA FROM HSSPAGA ".
                 "                      WHERE NNUMEPAGA = :id_pagamento ".
                 "                      UNION ".
                 "                     SELECT HSSPARCE.NDESTPAGA FROM HSSPARCE ".
                 "                      WHERE HSSPARCE.NNUMEPAGA = :id_pagamento ".
                 "                      UNION ".
                 "                     SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2 ". 
                 "                      WHERE PARCE1.NDESTPAGA = :id_pagamento ".
                 "                        AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ";
                 
    $sql_d_ban = new Query($bd);
    $sql_d_ban->clear();
    $sql_d_ban->addParam(":id_pagamento",$id_pagamento);
    $sql_d_ban->executeQuery($txt_d_ban);

    $pdf->Ln();
    $pdf->Ln();

    $total_fatura = $total_mensalidade + $total_aditivo + $adesao + $acrescimo + $faturamento_minimo + $total_despesa - 
                    $desconto + $carteira + $faltas -
                    str_replace(',','.',$sql2->result("NIR__NOTAF")) -
                    str_replace(',','.',$sql2->result("NPIS_NOTAF")) -
                    str_replace(',','.',$sql2->result("NCOFINOTAF")) -
                    str_replace(',','.',$sql2->result("NCSLLNOTAF")) -
                    str_replace(',','.',$sql2->result("NISS_NOTAF")) +
                    str_replace(',','.',$sql_d_ban->result("VALOR"));

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(175,5,'Mensalidade (+): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($total_mensalidade),0,1,'R');
    
    if ($total_aditivo > 0){
      $pdf->Cell(175,5,'Aditivo (+): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($total_aditivo),0,1,'R'); 
    }
    
    if ($adesao > 0) {
      $pdf->Cell(175,5,'Ades�o (+): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($adesao),0,1,'R'); 
    }
    
    if ($acrescimo > 0) {
      $pdf->Cell(175,5,'Acr�scimos (+): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($acrescimo),0,1,'R'); 
    }
    
    if ($faturamento_minimo > 0) {
      $pdf->Cell(175,5,'Faturamento m�nimo por plano (+): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($faturamento_minimo),0,1,'R'); 
    }
    
    if ($desconto > 0) {
      $pdf->Cell(175,5,'Descontos (-): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($desconto),0,1,'R'); 
    }

    if ($total_despesa > 0) {
      $pdf->Cell(175,5,'Despesas (+): ',0,0,'R');  
      $pdf->Cell(15,5,$formata->formataNumero($total_despesa),0,1,'R');
    }

    if ($carteira > 0) {
      $pdf->Cell(175,5,'Carteirinhas (+): ',0,0,'R');  
      $pdf->Cell(15,5,$formata->formataNumero($carteira),0,1,'R');
    }

    if ($faltas > 0) {
      $pdf->Cell(175,5,'Faltas (+): ',0,0,'R');  
      $pdf->Cell(15,5,$formata->formataNumero($faltas),0,1,'R');
    }

    if ($sql_d_ban->result("VALOR") > 0) {
      $pdf->Cell(175,5,'Despesa banc�ria (+): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($sql_d_ban->result("VALOR")),0,1,'R');
    }

    if ($sql2->result("NISS_NOTAF") > 0 ) {
      $pdf->Cell(175,5,'ISS (-): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NISS_NOTAF")),0,1,'R');
    }

    if ($sql2->result("NIR__NOTAF") > 0 ) {
      $pdf->Cell(175,5,'Imposto de Renda (-): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NIR__NOTAF")),0,1,'R');
    }

    if ($sql2->result("NPIS_NOTAF") > 0 ) {
      $pdf->Cell(175,5,'PIS (-): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NPIS_NOTAF")),0,1,'R');
    }

    if ($sql2->result("NCOFINOTAF") > 0 ) {
      $pdf->Cell(175,5,'COFINS (-): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NCOFINOTAF")),0,1,'R');
    }

    if ($sql2->result("NCSLLNOTAF") > 0 ) {
      $pdf->Cell(175,5,'CSLL (-): ',0,0,'R');
      $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NCSLLNOTAF")),0,1,'R');
    }

    $pdf->Cell(175,5,'Total (=): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($total_fatura),0,1,'R');

    $txt = "SELECT TO_CHAR(DVENCPAGA,'DD/MM/YYYY') DVENCPAGA,NDOCUPAGA,NVENCPAGA ".
           "  FROM HSSPAGA ".
           " WHERE NNUMEPAGA IN (SELECT HSSPARCE.NDESTPAGA FROM HSSPARCE ".
           "                      WHERE HSSPARCE.NNUMEPAGA = :id_pagamento ".
           "                      UNION ".
           "                     SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2 ". 
           "                      WHERE PARCE1.NDESTPAGA = :id_pagamento ".
           "                        AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ".
           " ORDER BY DVENCPAGA ";
           
    $sql3 = new Query($bd);
    $sql3->clear();
    $sql3->addParam(":id_pagamento",$id_pagamento);
    $sql3->executeQuery($txt);
    
    if ($sql3->count() > 0) {
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial','B',8);
      $pdf->Cell(190,3,'PARCELAMENTOS',0,1);

      $pdf->Cell(20,3,'Vencimento','B',0);
      $pdf->Cell(20,3,'Documento','B',0);
      $pdf->Cell(20,3,'Valor','B',0);
      $pdf->Cell(130,3,'','B',1);
      $pdf->Ln(3);
      $pdf->SetFont('Arial','',8);

      while (!$sql3->eof()) {
        $pdf->Cell(20,3,$sql3->result("DVENCPAGA"),0,0);
        $pdf->Cell(20,3,$sql3->result("NDOCUPAGA"),0,0);
        $pdf->Cell(20,3,$formata->formataNumero($sql3->result("NVENCPAGA")),0,0);
        $pdf->Cell(130,3,'',0,1);
        $sql3->next();
      }

      $pdf->Ln();
      $pdf->Cell(190,3,'','B',1);
    }

    if (($contrato == 2016844712) and ($id_pagamento == 378226) and ($_SESSION['apelido_operadora'] == "vitallis") ) {
      $pdf->Ln(10);
      $pdf->Cell(30,3,'',0,0);
      $pdf->Cell(160,3,'Plano coletivo com patrocinador, contrato n.� 9940',0,1);
      $pdf->Ln(6);
      $pdf->Cell(30,3,'',0,0);
      $pdf->Cell(160,3,'Global, registro ANS sob o nome V7000-AE-RC801-CE, n.� 436.830/01-4.',0,1);
      $pdf->Ln(6);
      $pdf->Cell(30,3,'',0,0);
      $pdf->Cell(160,3,'Platina, registro ANS sob o nome V7000-AE-RC1001-CE, n.� 436.831/01-2.',0,1);
      $pdf->Ln(6);
      $pdf->Cell(30,3,'',0,0);
      $pdf->Cell(160,3,'A comunica��o do reajuste ser� protocolada na ANS em at� trinta dias ap�s',0,1);
      $pdf->Cell(30,3,'',0,0);
      $pdf->Cell(160,3,'sua aplica��o, por for�a do disposto na RN n�128/06.',0,1);
    }
  }

  $bd->close();
  
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';  
  $pdf->Output($file,'F');
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>"; 

?>
