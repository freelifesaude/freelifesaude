<?php
  require_once('../comum/autoload.php');
  $seg->secureSessionStart();
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();

  $_SESSION['titulo'] = "ATUALIZAÇÃO CADASTRAL";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","atualizacaoResponsavel.html");
    
  $codigo            	 = $_SESSION['id_contrato'];
  $responsavel           = '';
  $cargo                 = '';
  $cpf                   = '';
  $rg                    = '';
  $nascimento            = '';
  
  $sql_empresa = new Query($bd);    
  $txt_empresa = "SELECT HSSEMPR.NNUMEEMPR, CRESEEMPR, CCAREEMPR, CCPFREMPR, CRGR_EMPR, to_char(DNASREMPR, 'DD/MM/YYYY') DNASREMPR
				  FROM HSSTITU, HSSEMPR
				  WHERE NNUMETITU = :codigo
				  AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR";
  $sql_empresa->addParam(":codigo",$codigo); 
  $sql_empresa->executeQuery($txt_empresa);  
  
  if (isset($_POST['enviar'])) {      
    $responsavel    = $seg->antiInjection($_POST['responsavel']);
    $cargo          = $seg->antiInjection($_POST['cargo']);
    $cpf            = $func->somenteNumeros($seg->antiInjection($_POST['cpf']));
    $rg             = $seg->antiInjection($_POST['rg']);
    $nascimento     = $seg->antiInjection($_POST['nascimento']);
   
		$filtros = array();
		$filtros['op']   = 900;
		$filtros['prot'] = '';          
	    $_SESSION['filtrosConfirmacao'] = $filtros;				
		 
		 $sql = new Query($bd);    
         $txt = "UPDATE HSSEMPR
                     SET CRESEEMPR = :responsavel,
                         CCAREEMPR = :cargo,
						 CCPFREMPR = :cpf,
						 CRGR_EMPR = :rg,
						 DNASREMPR = :nascimento
                   WHERE NNUMEEMPR = :empresa";  
				   
         $sql->addParam(":empresa",$sql_empresa->result("NNUMEEMPR")); 
		 $sql->addParam(":responsavel",$responsavel); 
		 $sql->addParam(":cargo",$cargo); 
		 $sql->addParam(":cpf",$cpf); 
		 $sql->addParam(":rg",$rg); 
		 $sql->addParam(":nascimento",$nascimento);	
		 
         $erro = $sql->executeSQL($txt);     
		 
		    if($erro == '')
				$util->redireciona('confirmacao.php?idSessao='.$_GET['idSessao']);
			else{
				  $tpl->CLASSE = "alert-error";    
				  $tpl->MENSAGEM = "<span style='font-weight: bold; font-size: 15px; color: #FF0000;'>".$resultado."</span>";		  
				  $tpl->block("MSG");
			}    
  } 
           
  /* Montagem da Tela */
   
  
  $tpl->ID_SESSAO                  = $_GET['idSessao'];    
  //$tpl->ID                         = $sql_empresa->result("NNUMEEMPR");    
  $tpl->NOME                       = $sql_empresa->result("CRESEEMPR");    
  $tpl->CARGO                      = $sql_empresa->result("CCAREEMPR");    
  $tpl->CPF                        = $sql_empresa->result("CCPFREMPR");    
  $tpl->RG                         = $sql_empresa->result("CRGR_EMPR");    
  $tpl->NASCIMENTO                 = $sql_empresa->result("DNASREMPR");    

  $tpl->BOTAO_ID                   = "enviar";      
  $tpl->CADASTRO_BOTAO             = "Atualizar";    
  
  $tpl->block("MOSTRA_MENU");  
  $tpl->show();     
  $bd->close();
?>