<?php
  $total_mensalidade = 0;
  $total_despesa = 0;
  $total_fatura = 0;

  $txt = "SELECT NVL(FE.CNOMESETOR,' ') LOCACAO,DECODE(FE.NTITUUSUA,FE.NNUMEUSUA,0,1) ORDEM,FE.CNOMEUSUA,FE.TITULAR,FE.CGRAUUSUA,FE.CTIPOUSUPG, 
                  SUM(DECODE(TIPO,'MENSALIDADE',FE.VALOR,0)) + SUM(DECODE(FE.TIPO,'PRO-RATA',FE.VALOR,0)) VALOR, SUM(DECODE(FE.TIPO,'ADITIVO',FE.VALOR,0)) VALOR_ADT,
                 TO_CHAR(FE.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,FE.CSITUUSUA,TO_CHAR(FE.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,FE.NNUMEUSUA,FE.CCODIPLAN,IDADE(FE.DNASCUSUA,:vencimento) IDADE,
                 NVL(FE.NNUMESETOR,0) NNUMESETOR,DECODE(FE.CCHAPUSUA,NULL,FE.CCODIUSUA,FE.CCHAPUSUA) CCHAPUSUA,FE.NTITUUSUA  
            FROM FATURA_COMPOSICAO FE 
           WHERE FE.NNUMEPAGA = :id_pagamento 
             AND FE.TIPO IN ('MENSALIDADE','ADITIVO','PRO-RATA') 
           GROUP BY NVL(FE.CNOMESETOR,' '),DECODE(FE.NTITUUSUA,FE.NNUMEUSUA,0,1),FE.CNOMEUSUA,FE.TITULAR,FE.CGRAUUSUA,FE.CTIPOUSUPG, 
                    TO_CHAR(FE.DINCLUSUA,'DD/MM/YYYY'),FE.CSITUUSUA,TO_CHAR(FE.DNASCUSUA,'DD/MM/YYYY'),FE.NNUMEUSUA,FE.CCODIPLAN,IDADE(FE.DNASCUSUA,:vencimento),
                    NVL(FE.NNUMESETOR,0),DECODE(FE.CCHAPUSUA,NULL,FE.CCODIUSUA,FE.CCHAPUSUA),FE.NTITUUSUA                                  
           ORDER BY 1,4,2,5 DESC";
          
  $sql = new Query();
  $sql->clear();
  $sql->addParam(":id_pagamento",$id_pagamento);
  $sql->addParam(":vencimento",$sql4->result("DVENCPAGA"));
  $sql->executeQuery($txt);

  $total_familia = 0;
  $total_locacao = 0;
  $titular = 0;
  $locacao = '';
  $id_setor = 0;

  if ($sql->count() > 0) {
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(30,5,'Matr�cula','B',0);
    $pdf->Cell(49,5,'Usu�rio','B',0);
    $pdf->Cell(10,5,'Plano','B',0,'R');
    $pdf->Cell(25,5,'Tipo do usu�rio','B',0);
    $pdf->Cell(18,5,'Nascimento','B',0,'C');
    $pdf->Cell(10,5,'Idade','B',0,'R');
    $pdf->Cell(18,5,'Inclus�o','B',0,'C');
    
    if ($seg->permissaoOutros($bd,'WEBEMPRESAMOSTRARAGRUPADONAFATURAOVALORDOADITIVOMENSALIDADE',false)) {
      $pdf->Cell(15,5,'','B',0,'R');
      $pdf->Cell(15,5,'Valor','B',1,'R');
    } else {
      $pdf->Cell(15,5,'Aditivo','B',0,'R');
      $pdf->Cell(15,5,'Valor','B',1,'R');
    }
        
    $pdf->Ln(3);

    //$locacao = $sql->result("LOCACAO");
    $titular = $sql->result("NTITUUSUA");

    while (!$sql->eof()) {
      if ($locacao != $sql->result("LOCACAO"))  {
        if ($sql->result("NNUMESETOR") > 0) {
          $pdf->SetFont('Arial','B',8);
          $pdf->Cell(190,3,"Loca��o: ".$sql->result("LOCACAO"),0,1);
          $pdf->Ln(6);
        }
        $locacao = $sql->result("LOCACAO");
        $id_setor = $sql->result("NNUMESETOR");
      }

      if ($sql->result("NTITUUSUA") != 0) {
        $pdf->SetFont('Arial','',8);
        if (($sql->result("CTIPOUSUPG") == 'T') or
            ($sql->result("CTIPOUSUPG") == 'F')) {
          $pdf->Cell(30,3,$sql->result("CCHAPUSUA"),0,0);
          $pdf->Cell(49,3,$pdf->Copy($sql->result("CNOMEUSUA"),48),0,0);
        } else {    	
          $pdf->Cell(30,3,$sql->result("CCHAPUSUA"),0,0);
          $pdf->Cell(49,3,$pdf->Copy($sql->result("CNOMEUSUA"),48),0,0);
        }

        $pdf->Cell(10,3,$pdf->Copy($sql->result("CCODIPLAN"),9),0,0,'R');

        Switch ($sql->result("CTIPOUSUPG")) {
          case 'T' : $pdf->Cell(25,3,'Titular',0,0); 	break;
          case 'F' : $pdf->Cell(25,3,'Titular',0,0); 	break;
          case 'A' : $pdf->Cell(25,3,'Agregado',0,0);   break;
          default  : $pdf->Cell(25,3,'Dependente',0,0); break;
        }
      
        $pdf->Cell(18,3,$sql->result("DNASCUSUA"),0,0,'C');
        $pdf->Cell(10,3,$sql->result("IDADE"),0,0,'R');
        $pdf->Cell(18,3,$sql->result("DINCLUSUA"),0,0,'C');
        
        if ($seg->permissaoOutros($bd,'WEBEMPRESAMOSTRARAGRUPADONAFATURAOVALORDOADITIVOMENSALIDADE',false)) {
          $valor = str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
          $pdf->Cell(15,3,'',0,0,'R');
          $pdf->Cell(15,3,$formata->formataNumero($valor),0,1,'R');
        } else {
          $pdf->Cell(15,3,$formata->formataNumero($sql->result("VALOR_ADT")),0,0,'R');
          $pdf->Cell(15,3,$formata->formataNumero($sql->result("VALOR")),0,1,'R');      
        }
      }

      $total_familia = $total_familia + str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
      $total_locacao = $total_locacao + str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
      $total_mensalidade = $total_mensalidade + str_replace(',','.',$sql->result("VALOR_ADT")) + str_replace(',','.',$sql->result("VALOR"));
      $titular = $sql->result("NTITUUSUA");
      
      $sql->next();

      if (($titular != $sql->result("NTITUUSUA")) or ($sql->eof())) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(175,3,'Total da fam�lia: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_familia),0,1,'R');
        $pdf->Ln();
        $total_familia = 0;
      }

      if (($locacao != $sql->result("LOCACAO")) or ($sql->eof())) {
        $pdf->SetFont('Arial','B',8);

        if (trim($locacao) == '') 
          $pdf->Cell(175,3,'Total: ',0,0,'R');
        else
          $pdf->Cell(175,3,'Total da loca��o: ',0,0,'R');

        $pdf->Cell(15,3,$formata->formataNumero($total_locacao),0,1,'R');
        $pdf->Ln();
        $total_locacao = 0;
        
        if (trim($locacao) == '')         
          $pdf->Totalizacao_plano($bd,$id_pagamento,-1);
        else
          $pdf->Totalizacao_plano($bd,$id_pagamento,$id_setor);
        
        $pdf->Ln();
      }
    }    
  
    $pdf->Ln();
  }
    
  $txt = " SELECT CCODIUSUA,CNOMEUSUA,VALOR                   
             FROM FATURA_COMPOSICAO FE
            WHERE FE.TIPO = 'ADESAO'
              AND FE.NNUMEPAGA = :id_Pagamento
            ORDER BY CCODIUSUA ";
  
  $sql_adesao = new Query($bd);
  $sql_adesao->clear();
  $sql_adesao->addParam(":id_pagamento",$id_pagamento);
  $sql_adesao->executeQuery($txt);          
  
  $adesao = 0;
  
  if ($sql_adesao->count() > 0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'ADES�ES COBRADAS',0,1);
    
    $pdf->Cell(30,5,'Codigo','B',0);
    $pdf->Cell(80,5,'Nome','B',0);
    $pdf->Cell(15,5,'Ades�o R$','B',1);
    $pdf->Ln(3);
    $pdf->SetFont('Arial','',8);
    
    while(!$sql_adesao->eof()) {
      $pdf->Cell(30,5,$sql_adesao->result("CCODIUSUA"),'',0);
      $pdf->Cell(80,5,$sql_adesao->result("CNOMEUSUA"),'',0);
      $pdf->Cell(15,5,$formata->formataNumero($sql_adesao->result("VALOR")),'',1,'R');
      $adesao = $adesao + str_replace(',','.',$sql_adesao->result("VALOR"));
      $sql_adesao->next();
    }
    
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(110,5,'Total: ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($adesao),0,1,'R');
    $pdf->Ln();
  }
  
  $pdf->Ln();
  
  $txt = "SELECT NVL(FE.CNOMESETOR,' ') LOCACAO,FE.CNOMEUSUA,FE.TITULAR,
                 TO_CHAR(FE.EMISSAO,'DD/MM/YYYY') DATENCONT,FE.NTITUUSUA,
                 NVL(FE.CCHAPUSUA,FE.CCODIUSUA) CCHAPUSUA,FE.CCODIUSUA,FE.ID NNUMECONT,
                 FE.PRESTADOR CNOMEPRES,FE.VALOR NVALOPACON,
                 FE.NNUMESETOR,FE.CODIGO_ITEM,FE.DESCRICAO_ITEM,FE.QTDE_ITEM                   
            FROM FATURA_COMPOSICAO FE
           WHERE FE.NNUMEPAGA = :id_pagamento ";
		   
  if ($detalhado)
    $txt .= " AND FE.TIPO = 'COPARTICIPACAO' ";  
  else 
    $txt .= " AND FE.TIPO = 'COPARTICIPACAO_SINTETICA' "; 
           
  $txt .= " ORDER BY 1,3,2,8 ";
           
  $sql1 = new Query($bd);
  $sql1->clear();
  $sql1->addParam(":id_pagamento",$id_pagamento);
  $sql1->executeQuery($txt);

  $total_familia = 0;
  $titular = 0;

  if ($sql1->count() > 0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'DESPESAS COBRADAS',0,1);

    $pdf->Cell(30,5,'Matr�cula','B',0);
    $pdf->Cell(30,5,'Titular','B',0);
    $pdf->Cell(39,5,'Usu�rio','B',0);
    $pdf->Cell(15,5,'Conta','B',0);
    $pdf->Cell(19,5,'Atendimento','B',0,'C');
    $pdf->Cell(12,5,'Regime','B',0);      
    $pdf->Cell(30,5,'Prestador','B',0);
    $pdf->Cell(15,5,'Valor','B',1,'R');
    $pdf->Ln(3);
    
    $locacao       = '';
    $conta         = 0;  
    $total_conta   = 0;
    $total_familia = 0; 
    $total_despesa = 0; 
    $titular       = 0; 
    $total_locacao = 0; 

    while (!$sql1->eof()) {
    
      if ($locacao != $sql1->result("LOCACAO"))  {
        if ($sql1->result("NNUMESETOR") > 0) {
          $pdf->SetFont('Arial','B',8);
          $pdf->Cell(190,3,"Loca��o: ".$sql1->result("LOCACAO"),0,1);
          $pdf->Ln(6);
        }
        $locacao = $sql1->result("LOCACAO");
      }
    
      if ($conta <> $sql1->result("NNUMECONT")) {
        $conta = $sql1->result("NNUMECONT");
        
        $txt_regime = "SELECT DECODE(CURGECONT,'E','Eletivo','Urgente') REGIME
                         FROM HSSCONT
                        WHERE NNUMECONT = :conta ";
        $sql_regime = new Query();
        $sql_regime->addParam(":conta",$conta);
        $sql_regime->executeQuery($txt_regime);
                        
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,3,$sql1->result("CCHAPUSUA"),'',0);
        $pdf->Cell(30,3,$pdf->Copy($sql1->result("TITULAR"),29),'',0);
        $pdf->Cell(39,3,$pdf->Copy($sql1->result("CNOMEUSUA"),38),'',0);
        $pdf->Cell(15,3,$sql1->result("NNUMECONT"),'',0);
        $pdf->Cell(19,3,$sql1->result("DATENCONT"),'',0,'C');
        $pdf->Cell(12,3,$sql_regime->result("REGIME"),'',0,'C');
        $pdf->Cell(30,3,$pdf->Copy($sql1->result("CNOMEPRES"),29),'',0);
      }
      
      $total_conta = $total_conta + str_replace(',','.',$sql1->result("NVALOPACON"));
      $total_familia = $total_familia + str_replace(',','.',$sql1->result("NVALOPACON"));
      $total_despesa = $total_despesa + str_replace(',','.',$sql1->result("NVALOPACON"));
      $titular = $sql1->result("NTITUUSUA");
      $total_locacao = $total_locacao + str_replace(',','.',$sql1->result("NVALOPACON"));
    
      if ($detalhado) {
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,3,'','',0);            
        $pdf->Cell(16,3,$sql1->result("CODIGO_ITEM"),'',0);          
        $pdf->Cell(60,3,$pdf->Copy($sql1->result("DESCRICAO_ITEM"),59),'',0);          
        $pdf->Cell(10,3,$formata->formataNumero($sql1->result("QTDE_ITEM")),'',0);                    
        $pdf->Cell(20,3,$formata->formataNumero($sql1->result("NVALOPACON")),'',1);                              
        $pdf->SetFont('Arial','',8);          
      }

      $sql1->next();
      
      if ($conta <> $sql1->result("NNUMECONT")) {
        if ($detalhado) {
          $pdf->Cell(175,5,'Total da conta:','',0,'R');                    
          $pdf->Cell(15,5,$formata->formataNumero($total_conta),'',1,'R');          
        } else
          $pdf->Cell(15,3,$formata->formataNumero($total_conta),'',1,'R');          
        
        $total_conta = 0;
      }
      
      if (($titular != $sql1->result("NTITUUSUA")) or ($sql1->eof())) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(175,3,'Total da fam�lia: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_familia),0,1,'R');
        $pdf->Ln();
        $total_familia = 0;
      }
      
      if (($locacao != $sql1->result("LOCACAO")) or ($sql1->eof())) {
        $pdf->SetFont('Arial','B',8);

        if (trim($locacao) == '') 
          $pdf->Cell(175,3,'Total: ',0,0,'R');
        else
          $pdf->Cell(175,3,'Total da loca��o: ',0,0,'R');

        $pdf->Cell(15,3,$formata->formataNumero($total_locacao),0,1,'R');
        $pdf->Ln();
        $total_locacao = 0;
        $pdf->Ln();
      }      
    }
  }
   
  $txt = "SELECT HSSUSUA.CNOMEUSUA,HSSUSUA.NNUMEUSUA,TITU.CNOMEUSUA TITULAR,HSSUSUA.NTITUUSUA,NNUMELIMI,CFORMLIMI,CTIPOLIMI,NQUANLIMI,
                 MIN(DEMISGUIA) DATA_MINIMA,MAX(DEMISGUIA) DATA_MAXIMA
            FROM HSSUSUA,
                 (
                   SELECT UL.NNUMEUSUA,UL.NNUMELIMI,HSSLIMI.CFORMLIMI,HSSLIMI.CTIPOLIMI,HSSLIMI.NQUANLIMI,UL.DEMISGUIA
                     FROM HSSGPAGA,UTILIZACAO_LIMITES UL,HSSLIMI
                    WHERE HSSGPAGA.NNUMEPAGA = :id_pagamento
                      AND HSSGPAGA.NNUMEGUIA = UL.ID_GUIA
                      AND UL.TIPO = 'G'
                      AND UL.NNUMELIMI = HSSLIMI.NNUMELIMI
                    UNION ALL
                   SELECT UL.NNUMEUSUA,UL.NNUMELIMI,HSSLIMI.CFORMLIMI,HSSLIMI.CTIPOLIMI,HSSLIMI.NQUANLIMI,UL.DEMISGUIA
                     FROM HSSPACON,UTILIZACAO_LIMITES UL,HSSLIMI
                    WHERE HSSPACON.NNUMEPAGA = :id_pagamento
                      AND HSSPACON.NNUMECONT = UL.ID_GUIA
                      AND UL.TIPO = 'C'                      
                      AND UL.NNUMELIMI = HSSLIMI.NNUMELIMI
                    UNION ALL
                   SELECT UL.NNUMEUSUA,UL.NNUMELIMI,HSSLIMI.CFORMLIMI,HSSLIMI.CTIPOLIMI,HSSLIMI.NQUANLIMI,UL.DEMISGUIA
                     FROM HSSPACON,HSSGCON,UTILIZACAO_LIMITES UL,HSSLIMI
                    WHERE HSSPACON.NNUMEPAGA = :id_pagamento
                      AND HSSPACON.NNUMECONT = HSSGCON.NNUMECONT
                      AND HSSGCON.NNUMEGUIA = UL.ID_GUIA
                      AND UL.TIPO = 'G'                      
                      AND UL.NNUMELIMI = HSSLIMI.NNUMELIMI
                    ) GUIAS,HSSUSUA TITU
              WHERE HSSUSUA.NNUMEUSUA = GUIAS.NNUMEUSUA
                AND HSSUSUA.NTITUUSUA = TITU.NNUMEUSUA
              GROUP BY HSSUSUA.CNOMEUSUA,HSSUSUA.NNUMEUSUA,TITU.CNOMEUSUA,HSSUSUA.NTITUUSUA,NNUMELIMI,CFORMLIMI,CTIPOLIMI,NQUANLIMI
          ORDER BY 3,1 ";
               
  $sql_desp_ant = new Query($bd);
  $sql_desp_ant->clear();
  $sql_desp_ant->addParam(":id_pagamento",$id_pagamento);
  $sql_desp_ant->executeQuery($txt);

  $total_familia = 0;
  $titular = 0;

  if ($sql_desp_ant->count() > 0) {
  
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'---------- RELACAO DE UTILIZACAO ----------',0,1);
    $pdf->Ln(3);
    $pdf->Cell(5,5,'','',0);
    $pdf->Cell(20,5,'Guia / Conta','B',0);
    $pdf->Cell(55,5,'Usu�rio','B',0);
    $pdf->Cell(20,5,'Atendimento','B',0,'C');
    $pdf->Cell(50,5,'Prestador','B',0);
    $pdf->Cell(20,5,'C�digo','B',0);
    $pdf->Cell(20,5,'Vencimento','B',1);
    
    $nome_titular = '';
    $nome_usuario = '';
    
    while (!$sql_desp_ant->eof()) {
    
      if ( ( ($nome_titular <> $sql_desp_ant->result('TITULAR')) and ($sql_desp_ant->result('CTIPOLIMI') == 'F') ) or
           ( ($nome_usuario <> $sql_desp_ant->result('CNOMEUSUA')) and ($sql_desp_ant->result('CTIPOLIMI') == 'L') ) ) {

        $quantidade = 0;
        
        if ($nome_titular <> $sql_desp_ant->result('TITULAR')) {
          $pdf->Ln(5);
          $pdf->SetFont('Arial','B',8);          
          $pdf->Cell(190,5,'Titular: '.$sql_desp_ant->result('TITULAR'));
          $pdf->Ln(5);
          $pdf->SetFont('Arial','',8);
           
        }

        $nome_titular = $sql_desp_ant->result('TITULAR');
        $nome_usuario = $sql_desp_ant->result('CNOMEUSUA');

        $sql_limi = new Query();        
        $txt_limi = "SELECT USUA.CNOMEUSUA,UL.DEMISGUIA,UL.ID,NVL(UL.NNUMEGUIA,UL.NNUMECONT) NNUMEGUIA2,FINPRES.CNOMEPRES,UL.CCODIPMED,CNOMEESPEC,UL.NNUMECONT,UL.NNUMEGUIA,UL.CCOBRPGUI
                       FROM UTILIZACAO_LIMITES UL,HSSUSUA USUA,FINPRES
                      WHERE UL.NNUMELIMI   = :limite ";
                      
        if ($sql_desp_ant->result('CTIPOLIMI') == 'F') {
          $txt_limi .= "   AND UL.NTITUUSUA   = :titular";
          $sql_limi->addParam(":titular",$sql_desp_ant->result('NTITUUSUA'));
        }
        else if ($sql_desp_ant->result('CTIPOLIMI') == 'L') {
          $txt_limi .= "   AND UL.NNUMEUSUA   = :usuario";
          $sql_limi->addParam(":usuario",$sql_desp_ant->result('NNUMEUSUA'));
        }
        
        $txt_limi .= "   AND UL.DEMISGUIA  >= TRUNC(INICIO_CONTAGEM_LIMITES(UL.NNUMEUSUA,UL.NNUMELIMI,:datamaxima,UL.NNUMEESPEC)) 
                         AND UL.DEMISGUIA  < LAST_DAY(:datamaxima) + 1
                         AND UL.NNUMEUSUA   = USUA.NNUMEUSUA
                         AND UL.NNUMEPRES   = FINPRES.NNUMEPRES(+) ";
                    
        if ($sql_desp_ant->result('CTIPOLIMI') == 'F')
          $txt_limi .= " ORDER BY 2,3";
        else if ($sql_desp_ant->result('CTIPOLIMI') == 'L')
          $txt_limi .= " ORDER BY 1,2,3";
        
        $sql_limi->addParam(":limite",$sql_desp_ant->result('NNUMELIMI'));
        $sql_limi->addParam(":datamaxima",$sql_desp_ant->result('DATA_MAXIMA'));
        
        $sql_limi->executeQuery($txt_limi);
        
        
      }

      while (!$sql_limi->eof()) {

        $quantidade++;

        if (!( ($quantidade > $sql_desp_ant->result('NQUANLIMI')) and ($sql_limi->result('CCOBRPGUI') == 'X') )) {
          
          $sql_cobr = new Query();
          $txt_cobr = "SELECT DVENCPAGA FROM HSSGPAGA,HSSPAGA
                        WHERE HSSGPAGA.NNUMEGUIA = :guia AND NVALOGPAGA > 0
                          AND HSSGPAGA.NNUMEPAGA = HSSPAGA.NNUMEPAGA
                          AND DCANCPAGA IS NULL
                       UNION ALL
                       SELECT DVENCPAGA FROM HSSGCON,HSSPACON,HSSPAGA
                        WHERE HSSGCON.NNUMEGUIA = :guia
                          AND HSSGCON.NNUMECONT = HSSPACON.NNUMECONT AND NVALOPACON > 0
                          AND HSSPACON.NNUMEPAGA = HSSPAGA.NNUMEPAGA
                          AND DCANCPAGA IS NULL
                       UNION ALL
                       SELECT DVENCPAGA FROM HSSPACON,HSSPAGA
                        WHERE HSSPACON.NNUMECONT = :conta AND NVALOPACON > 0
                          AND HSSPACON.NNUMEPAGA = HSSPAGA.NNUMEPAGA
                          AND DCANCPAGA IS NULL
                       UNION ALL
                       SELECT TRUNC(DLANCBANC) FROM HSSGCAIX,FINBANC
                        WHERE HSSGCAIX.NNUMEGUIA = :guia
                          AND HSSGCAIX.NMOVIBANC = FINBANC.NMOVIBANC";
          $sql_cobr->addParam(":guia",$sql_limi->result('NNUMEGUIA'));
          $sql_cobr->addParam(":conta",$sql_limi->result('NNUMECONT'));
          $sql_cobr->executeQuery($txt_cobr);
          
          $pdf->Cell(5,3,'','',0);
          $pdf->Cell(20,3,$sql_limi->result('NNUMEGUIA2'),'',0);
          $pdf->Cell(55,3,$pdf->Copy($sql_limi->result('CNOMEUSUA'),38),'',0);
          $pdf->Cell(20,3,$sql_limi->result('DEMISGUIA'),'',0);
          $pdf->Cell(50,3,$pdf->Copy($sql_limi->result('CNOMEPRES'),29),'',0);
          $pdf->Cell(20,3,$sql_limi->result('CCODIPMED'),'',0);

          if ($sql_cobr->result('DVENCPAGA') <> '') 
            $pdf->Cell(20,3,$sql_cobr->result('DVENCPAGA'),'',0);
            
          $pdf->Ln(3);            
        }
        else
          $quantidade--;
          
        $sql_limi->next();
      }   
           
      $sql_desp_ant->next();
    }
    
  }
  
  $txt_cart =  "SELECT FE.NTITUUSUA,FE.CCODIUSUA,FE.CNOMEUSUA,FE.TITULAR,FE.VALOR 
                  FROM FATURA_COMPOSICAO FE 
                 WHERE FE.TIPO = 'CARTEIRINHA'
                   AND FE.NNUMEPAGA = :id_pagamento
                 ORDER BY 4,3";

  $sql_cart = new Query($bd);
  $sql_cart->clear();
  $sql_cart->addParam(":id_pagamento",$id_pagamento);
  $sql_cart->executeQuery($txt_cart);

  $carteira = 0;

  if ($sql_cart->count() > 0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'SEGUNDA VIA DE CARTEIRAS',0,1);

    $pdf->Cell(30,5,'C�digo','B',0);
    $pdf->Cell(75,5,'Titular','B',0);
    $pdf->Cell(70,5,'Usu�rio','B',0);
    $pdf->Cell(15,5,'Valor','B',1);
    $pdf->Ln(3);
    $pdf->SetFont('Arial','',8);

    while(!$sql_cart->eof()) {
      $pdf->Cell(30,5,$sql_cart->result("CCODIUSUA"),'',0);
      $pdf->Cell(75,5,$pdf->Copy($sql_cart->result("CNOMEUSUA"),74),'',0);
      $pdf->Cell(70,5,$pdf->Copy($sql_cart->result("TITULAR"),69),'',0);
      $pdf->Cell(15,5,$formata->formataNumero($sql_cart->result("VALOR")),'',1,'R');
      $carteira = $carteira + str_replace(',','.',$sql_cart->result("VALOR"));
      $sql_cart->next();
    }
  }
                 
  $txt_falta = "SELECT FE.VALOR,FE.CCODIUSUA,FE.EMISSAO DDATAAGEND,FE.PRESTADOR CNOMEPRES,FE.CNOMEUSUA,FE.TITULAR,
                       FE.ID NNUMEAGEND,NVL(FE.CCHAPUSUA,FE.CCODIUSUA) CCHAPUSUA,FE.NNUMEUSUA,FE.CODIGO_ITEM CHORAAGEND
                  FROM FATURA_COMPOSICAO FE
                 WHERE FE.TIPO = 'FALTA'
                   AND FE.NNUMEPAGA = :id_pagamento
                 ORDER BY 6,8 ";

  $sql_falta = new Query($bd);
  $sql_falta->clear();
  $sql_falta->addParam(":id_pagamento",$id_pagamento);
  $sql_falta->executeQuery($txt_falta);
  
  $faltas = 0;

  if ($sql_falta->count() > 0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'COBRAN�A DE FALTAS',0,1);

    $pdf->Cell(15,5,'Data','B',0);
    $pdf->Cell(10,5,'Hor�rio','B',0);
    $pdf->Cell(70,5,'Prestador','B',0);
    $pdf->Cell(80,5,'Usu�rio','B',0);
    $pdf->Cell(15,5,'Valor','B',1);
    $pdf->Ln(3);
    $pdf->SetFont('Arial','',8);

    while(!$sql_falta->eof()) {
      $pdf->Cell(15,5,$sql_falta->result("DDATAAGEND"),'',0);
      $pdf->Cell(10,5,$sql_falta->result("CHORAAGEND"),'',0);
      $pdf->Cell(70,5,$sql_falta->result("CNOMEPRES"),'',0);
      $pdf->Cell(80,5,$sql_falta->result("TITULAR"),'',0);
      $pdf->Cell(15,5,$formata->formataNumero($sql_falta->result("VALOR")),'',1,'R');
      $faltas = $faltas + str_replace(',','.',$sql_falta->result("VALOR"));
      $sql_falta->next();
    }
  }

  $txt_acres = "SELECT FE.DESCRICAO_ITEM CMOTIADPAG,FE.VALOR VALOR
                  FROM FATURA_COMPOSICAO FE
                 WHERE FE.TIPO = 'ACRESCIMO/DESCONTO'
                   AND FE.NNUMEPAGA = :id_pagamento";
  
  $sql_acres = new Query($bd);
  $sql_acres->clear();
  $sql_acres->addParam(":id_pagamento",$id_pagamento);
  $sql_acres->executeQuery($txt_acres);

  $acrescimo = 0;
  $desconto = 0;

  if ($sql_acres->count() > 0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'ACR�SCIMOS E DESCONTO',0,1);

    $pdf->Cell(100,5,'Motivo','B',0);
    $pdf->Cell(15,5,'Valor','B',1);
    $pdf->Ln(3);

    $pdf->SetFont('Arial','',8);
    while (!$sql_acres->eof()) {   
      $pdf->Cell(100,3,$sql_acres->result("CMOTIADPAG"),'',0);
      $pdf->Cell(15,3,$formata->formataNumero($sql_acres->result("VALOR")),'',1,'R');

      if ($sql_acres->result("VALOR") > 0)
        $acrescimo = $acrescimo + str_replace(',','.',$sql_acres->result("VALOR"));
      else  
        $desconto = $desconto + (str_replace(',','.',$sql_acres->result("VALOR")) * -1);

      $sql_acres->next();
    }
  }
  
  $txt_acres = "SELECT FE.VALOR
                  FROM FATURA_COMPOSICAO FE
                 WHERE FE.NNUMEPAGA = :id_pagamento
                   AND FE.TIPO = 'JUROS/MULTA' ";
  
  $sql_acres = new Query($bd);
  $sql_acres->clear();
  $sql_acres->addParam(":id_pagamento",$id_pagamento);
  $sql_acres->executeQuery($txt_acres);    
    
  $acrescimo += str_replace(',','.',$sql_acres->result("VALOR"));
  
  $txt_faturamento_m = "SELECT SUM(FE.VALOR)
                          FROM FATURA_COMPOSICAO FE
                         WHERE FE.NNUMEPAGA = :id_pagamento
                           AND FE.TIPO = 'FATURAMENTO MINIMO'";

  $sql_faturamento_m = new Query($bd);
  $sql_faturamento_m->clear();
  $sql_faturamento_m->addParam(":id_pagamento",$id_pagamento);
  $sql_faturamento_m->executeQuery($txt_faturamento_m);

  $faturamento_minimo = str_replace(',','.',$sql_faturamento_m->result("VALOR"));
  
  $txt = "SELECT NIR__NOTAF, NISS_NOTAF, NPIS_NOTAF, NCOFINOTAF, NCSLLNOTAF ".
         "  FROM HSSCNOTA,HSSNOTAF ".
         " WHERE NNUMEPAGA IN (SELECT NNUMEPAGA FROM HSSPAGA ".
         "                      WHERE NNUMEPAGA = :id_pagamento ".
         "                      UNION ".
         "                     SELECT HSSPARCE.NDESTPAGA FROM HSSPARCE ".
         "                      WHERE HSSPARCE.NNUMEPAGA = :id_pagamento ".
         "                      UNION ".
         "                     SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2 ". 
         "                      WHERE PARCE1.NDESTPAGA = :id_pagamento ".
         "                        AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ".
         "   AND HSSCNOTA.NNUMENOTAF = HSSNOTAF.NNUMENOTAF ".
         "   AND CSITUNOTAF = 'A' ";

  $sql2 = new Query($bd);
  $sql2->clear();
  $sql2->addParam(":id_pagamento",$id_pagamento);
  $sql2->executeQuery($txt);
  
  $txt_d_ban = "SELECT SUM(NVL(NDBANPAGA,0)) VALOR FROM HSSPAGA ".
               " WHERE NNUMEPAGA IN (SELECT NNUMEPAGA FROM HSSPAGA ".
               "                      WHERE NNUMEPAGA = :id_pagamento ".
               "                      UNION ".
               "                     SELECT HSSPARCE.NDESTPAGA FROM HSSPARCE ".
               "                      WHERE HSSPARCE.NNUMEPAGA = :id_pagamento ".
               "                      UNION ".
               "                     SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2 ". 
               "                      WHERE PARCE1.NDESTPAGA = :id_pagamento ".
               "                        AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ";
               
  $sql_d_ban = new Query($bd);
  $sql_d_ban->clear();
  $sql_d_ban->addParam(":id_pagamento",$id_pagamento);
  $sql_d_ban->executeQuery($txt_d_ban);

  $pdf->Ln();
  $pdf->Ln();

  $total_fatura = $total_mensalidade + $adesao + $acrescimo + $faturamento_minimo + $total_despesa - 
                  $desconto + $carteira + $faltas -
                  str_replace(',','.',$sql2->result("NIR__NOTAF")) -
                  str_replace(',','.',$sql2->result("NPIS_NOTAF")) -
                  str_replace(',','.',$sql2->result("NCOFINOTAF")) -
                  str_replace(',','.',$sql2->result("NCSLLNOTAF")) -
                  str_replace(',','.',$sql2->result("NISS_NOTAF")) +
                  str_replace(',','.',$sql_d_ban->result("VALOR"));

  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(175,5,'Mensalidade (+): ',0,0,'R');
  $pdf->Cell(15,5,$formata->formataNumero($total_mensalidade),0,1,'R');
      
  if ($adesao > 0) {
    $pdf->Cell(175,5,'Ades�o (+): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($adesao),0,1,'R'); 
  }
  
  if ($acrescimo > 0) {
    $pdf->Cell(175,5,'Acr�scimos (+): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($acrescimo),0,1,'R'); 
  }
  
  if ($faturamento_minimo > 0) {
    $pdf->Cell(175,5,'Faturamento m�nimo por plano (+): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($faturamento_minimo),0,1,'R'); 
  }
  
  if ($desconto > 0) {
    $pdf->Cell(175,5,'Descontos (-): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($desconto),0,1,'R'); 
  }

  if ($total_despesa > 0) {
    $pdf->Cell(175,5,'Despesas (+): ',0,0,'R');  
    $pdf->Cell(15,5,$formata->formataNumero($total_despesa),0,1,'R');
  }

  if ($carteira > 0) {
    $pdf->Cell(175,5,'Carteirinhas (+): ',0,0,'R');  
    $pdf->Cell(15,5,$formata->formataNumero($carteira),0,1,'R');
  }

  if ($faltas > 0) {
    $pdf->Cell(175,5,'Faltas (+): ',0,0,'R');  
    $pdf->Cell(15,5,$formata->formataNumero($faltas),0,1,'R');
  }

  if ($sql_d_ban->result("VALOR") > 0) {
    $pdf->Cell(175,5,'Despesa banc�ria (+): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($sql_d_ban->result("VALOR")),0,1,'R');
  }

  if ($sql2->result("NISS_NOTAF") > 0 ) {
    $pdf->Cell(175,5,'ISS (-): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NISS_NOTAF")),0,1,'R');
  }

  if ($sql2->result("NIR__NOTAF") > 0 ) {
    $pdf->Cell(175,5,'Imposto de Renda (-): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NIR__NOTAF")),0,1,'R');
  }

  if ($sql2->result("NPIS_NOTAF") > 0 ) {
    $pdf->Cell(175,5,'PIS (-): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NPIS_NOTAF")),0,1,'R'); 
  }

  if ($sql2->result("NCOFINOTAF") > 0 ) {
    $pdf->Cell(175,5,'COFINS (-): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NCOFINOTAF")),0,1,'R');
  }

  if ($sql2->result("NCSLLNOTAF") > 0 ) {
    $pdf->Cell(175,5,'CSLL (-): ',0,0,'R');
    $pdf->Cell(15,5,$formata->formataNumero($sql2->result("NCSLLNOTAF")),0,1,'R');
  }

  $pdf->Cell(175,5,'Total (=): ',0,0,'R');
  $pdf->Cell(15,5,$formata->formataNumero($total_fatura),0,1,'R');

  $txt = "SELECT TO_CHAR(DVENCPAGA,'DD/MM/YYYY') DVENCPAGA,NDOCUPAGA,NVENCPAGA ".
         "  FROM HSSPAGA ".
         " WHERE NNUMEPAGA IN (SELECT HSSPARCE.NDESTPAGA FROM HSSPARCE ".
         "                      WHERE HSSPARCE.NNUMEPAGA = :id_pagamento ".
         "                      UNION ".
         "                     SELECT PARCE2.NDESTPAGA FROM HSSPARCE PARCE1,HSSPARCE PARCE2 ". 
         "                      WHERE PARCE1.NDESTPAGA = :id_pagamento ".
         "                        AND PARCE1.NNUMEPAGA = PARCE2.NNUMEPAGA) ".
         " ORDER BY DVENCPAGA ";
         
  $sql3 = new Query($bd);
  $sql3->clear();
  $sql3->addParam(":id_pagamento",$id_pagamento);
  $sql3->executeQuery($txt);
  
  if ($sql3->count() > 0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(190,3,'PARCELAMENTOS',0,1);

    $pdf->Cell(20,3,'Vencimento','B',0);
    $pdf->Cell(20,3,'Documento','B',0);
    $pdf->Cell(20,3,'Valor','B',0);
    $pdf->Cell(130,3,'','B',1);
    $pdf->Ln(3);
    $pdf->SetFont('Arial','',8);

    while (!$sql3->eof()) {
      $pdf->Cell(20,3,$sql3->result("DVENCPAGA"),0,0);
      $pdf->Cell(20,3,$sql3->result("NDOCUPAGA"),0,0);
      $pdf->Cell(20,3,$formata->formataNumero($sql3->result("NVENCPAGA")),0,0);
      $pdf->Cell(130,3,'',0,1);
      $sql3->next();
    }

    $pdf->Ln();
    $pdf->Cell(190,3,'','B',1);
  }

?>    