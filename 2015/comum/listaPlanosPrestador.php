<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   

  $bd = new Oracle();
	$func = new Funcao();

  $tpl = new Template("listaPlanosPrestador.html");
    
  $sql = new Query($bd);
  
  $idPrestador = $seg->antiInjection($_POST['idPrestador']);
  
  if($_SESSION['apelido_operadora'] == 'UnimedLestePaulista') {
		$txt = "SELECT DISTINCT HSSPLAN.CCODIPLAN,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) CDESCPLAN,
                   NVL(HSSTITU.CSCPATITU,NVL(TO_CHAR(HSSPLAN.NRGMSPLAN),HSSPLAN.CANTIPLAN)) REGISTRO,HSSPLAN.CNPCAPLAN,
						       DECODE(HSSPLAN.CSITUPLAN, 'A', 'Ativo','C','Cancelado','S','Ativo (comercialização suspensa)') SITUACAO,
						       DECODE(HSSPLAN.CNATUPLAN,'1','Individual / Familiar','3','Coletivo empresarial','4','Coletivo por adesão','SEM PARAMETRO') TIPO_CONTRA
							FROM HSSPLAN,HSSTITU
						 WHERE HSSPLAN.CWEB_PLAN = 'S'
               AND HSSPLAN.NNUMEPLAN = HSSTITU.NNUMEPLAN 
             ORDER BY 1,2";  
  }
  else if($func->temRedeAtendimento() == 1){
		$txt = "SELECT CCODIPLAN,NVL(CNPCAPLAN,CDESCPLAN) CDESCPLAN,CNOMEREDEA,NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) REGISTRO,CNPCAPLAN,
						       DECODE(CSITUPLAN, 'A', 'Ativo','C','Cancelado','S','Ativo (comercialização suspensa)') SITUACAO,
						       DECODE(CNATUPLAN,'1','Individual / Familiar','3','Coletivo empresarial','4','Coletivo por adesão','SEM PARAMETRO') TIPO_CONTRA
							FROM HSSPREDE,HSSREDPL,HSSPLAN,HSSREDEA
						 WHERE HSSPREDE.NNUMEPRES = :prestador          
							 AND HSSPREDE.NNUMEREDEA = HSSREDPL.NNUMEREDEA
							 AND HSSREDPL.NNUMEPLAN = HSSPLAN.NNUMEPLAN
							 AND HSSPREDE.NNUMEREDEA = HSSREDEA.NNUMEREDEA
               AND CWEB_PLAN = 'S'
						 ORDER BY 2";
												
		$sql->addParam(":prestador",$idPrestador);
  }
  else{
		$txt = "SELECT CCODIPLAN,NVL(CNPCAPLAN,CDESCPLAN) CDESCPLAN,NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) REGISTRO,CNPCAPLAN,
						       DECODE(CSITUPLAN, 'A', 'Ativo','C','Cancelado','S','Ativo (comercialização suspensa)') SITUACAO,
						       DECODE(CNATUPLAN,'1','Individual / Familiar','3','Coletivo empresarial','4','Coletivo por adesão','SEM PARAMETRO') TIPO_CONTRA
							FROM HSSPLAN
						 WHERE CWEB_PLAN = 'S' ";
		
	}
		
  $sql->executeQuery($txt);
          
  while (!$sql->eof()) {                                           
    $tpl->PLANO_CODIGO   = $sql->result('CCODIPLAN');
		if($_SESSION['apelido_operadora'] == 'camboriuSaude')
      $tpl->PLANO_NOME     = $sql->result('CNPCAPLAN');
	  else
	    $tpl->PLANO_NOME        = $sql->result('CDESCPLAN');
    
    $tpl->PLANO_REGISTRO      = $sql->result('REGISTRO');
    $tpl->SITUACAO            = $sql->result('SITUACAO');
    $tpl->CLASSIFICACAO_PLANO = $sql->result('TIPO_CONTRA');
    $tpl->block("PLANOS");
    $sql->next();
  }
   
  $tpl->block("RESULTADO");    
   

  $bd->close();
  echo utf8_encode($tpl->parse()); 
?>