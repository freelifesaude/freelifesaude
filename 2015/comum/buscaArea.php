<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  if (isset($_POST['especialidade'])){
        
    if ($_POST['especialidade'] <> "") {
      $sql = new Query($bd);
      $txt = "SELECT NNUMEAATUA, CDESCAATUA FROM HSSAATUA WHERE NNUMEESPEC = :especialidade ORDER BY CDESCAATUA";
      $sql->clear();
      $sql->addParam(":especialidade",$_POST['especialidade']);
      $sql->executeQuery($txt);

      echo '<option value="">Todas</option>';       
        
      while (!$sql->eof()) {
        $opcao = '<option value="'.$sql->result("NNUMEAATUA").'" ';
        
        if ($_POST['area'] == $sql->result("NNUMEAATUA"))
          $opcao .= ' selected ';
          
        $opcao .= '>'.$sql->result("CDESCAATUA").'</option>';
          
        echo $opcao;
        
        $sql->next();
      }  
    } else {
      echo '<option value="">Todas</option>';       
    }
  }
  
  $bd->close();   
?>