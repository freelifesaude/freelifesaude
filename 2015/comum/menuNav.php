<?php
  foreach($menu as $m) {
    if (is_array($m['valor'])) {
      $tpl->LINK_MENU_NAV = "#";
      $tpl->DESC_LINK_MENU_NAV = $m['nome'];

      foreach ($m['valor'] as $s1) {

        if (is_array($s1['valor'])) {
          $tpl->LINK_MENU1_NAV      = "#";
          $tpl->DESC_LINK_MENU1_NAV = $s1['nome'];

          foreach ($s1['valor'] as $s2) {

            if (strpos($s2['valor'],".php?") > 0)
              $tpl->LINK_MENU2_NAV      = $s2['valor']."&idSessao=".$_GET['idSessao'];
            else
              $tpl->LINK_MENU2_NAV      = $s2['valor']."?idSessao=".$_GET['idSessao'];

            $tpl->DESC_LINK_MENU2_NAV = $s2['nome'];

            if (isset($s2['novo']) and ($s2['novo'] == 'S'))
              $tpl->NOVO_MENU2_NAV = '_blank';
            else
              $tpl->NOVO_MENU2_NAV = '_self';

            $tpl->block("ITEM_SUBMENU2_NAV");
          }

          $tpl->block("ITEM_SUBMENU1_NAV_B");
        }
        else {
          if (strpos($s1['valor'],".php?") > 0)
            $tpl->LINK_MENU1_NAV      = $s1['valor']."&idSessao=".$_GET['idSessao'];
          else
            $tpl->LINK_MENU1_NAV      = $s1['valor']."?idSessao=".$_GET['idSessao'];

          $tpl->DESC_LINK_MENU1_NAV = $s1['nome'];

          if (isset($s1['novo']) and ($s1['novo'] == 'S'))
            $tpl->NOVO_MENU1_NAV = '_blank';
          else
            $tpl->NOVO_MENU1_NAV = '_self';

          $tpl->block("ITEM_SUBMENU1_NAV_A");
        }

        $tpl->block("ITEM_SUBMENU1_NAV");
      }

      $tpl->block("ITEM_MENU_NAV_B");
    }
    else {
      if (strpos($m['valor'],".php?") > 0)
        $tpl->LINK_MENU_NAV      = $m['valor']."&idSessao=".$_GET['idSessao'];
      else
        $tpl->LINK_MENU_NAV      = $m['valor']."?idSessao=".$_GET['idSessao'];

      $tpl->DESC_LINK_MENU_NAV = $m['nome'];

      if (isset($m['novo']) and ($m['novo'] == 'S'))
        $tpl->NOVO_MENU_NAV = '_blank';
      else
        $tpl->NOVO_MENU_NAV = '_self';

      $tpl->block("ITEM_MENU_NAV_A");
    }

    $tpl->block("ITEM_MENU_NAV");
  }

  $tpl->VERSAO_MOBILE = "hidden-phone";
  $tpl->ID_SESSAO2     = $_GET['idSessao'];

  if ((($_SESSION['sistema'] == 'Consultorio') or
       ($_SESSION['sistema'] == 'Prestador')) and
      ((!$seg->permissaoOutros($bd,"WEBPRESTADOREFETUAAGENDAMENTOONLINE",false)) and
       ($seg->permissaoOutros($bd,"WEBPRESTADORPLANEJAAGENDAMENTOONLINE",true))))
    $tpl->block("AVISO_CONSULTA");
?>