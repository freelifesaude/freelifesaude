<?php

  require_once("../comum/autoload.php");
  $seg->secureSessionStart();

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");

  if (isset($_POST['plano']))
    $plano        = $seg->antiInjection($_POST['plano']);
  else
    $plano       = '';

  $nome_prestador = $seg->antiInjection($_POST['nome_prestador']);
  $tipo_prestador = $seg->antiInjection($_POST['tipo_prestador']);
  $tipoRede       = $seg->antiInjection($_POST['tipoRede']);
  $estado         = $seg->antiInjection($_POST['estado']);
  $cidade         = $seg->antiInjection($_POST['cidade']);
  $bairro         = $seg->antiInjection($_POST['bairro']);
  $graduacao      = $seg->antiInjection($_POST['graduacao']);
  $especialidade  = $seg->antiInjection($_POST['especialidade']);
  $area_prestador = $seg->antiInjection($_POST['area']);

  if (isset($_POST['indicavel']))
    $indicavel    = $seg->antiInjection($_POST['indicavel']);
  else
    $indicavel    = '';
 
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg', 10, 5, 40, 25);
      $this->SetFont('Arial', 'B', 9); // Diminui pois existem nomes de Unimed que ficam grande demais e pode ficar cortado na p�gina.
      $this->Cell(40, 5, '');
      $this->Cell(150, 5, $_SESSION['nome_operadora'] . " - CNPJ: " . $_SESSION['cnpj_operadora'], 0, 1);
      $this->Cell(40, 5, '');
      $this->Cell(150, 5, 'Rede Credenciada', 0, 1);
      $this->Ln(10);
      $this->Cell(190, 1, ' ', 'B', 0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial', '', 8);
      //Page number
      $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

  }

  /* Cabecalho PDF */
  $pdf = new PDF('P', 'mm', 'A4');
  $pdf->AliasNbPages();
  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFont('Arial', '', 8);

  $pdf->Cell(130, 3, 'Filtros:', 0, 1);
      
  if (($_SESSION['apelido_operadora'] <> 'saudemed') and ( $_SESSION['apelido_operadora'] <> 'affego')) {

    $pdf->Cell(25, 3, "Tipo:", 0, 0, 'R');

    if ($tipo_prestador == '')
      $pdf->Cell(130, 3, 'Todos', 0, 1);
    else
      $pdf->Cell(130, 3, $func->tipoPrestador($tipo_prestador), 0, 1);
  }

  $pdf->Cell(25, 3, "Estado:", 0, 0, 'R');

  if ($estado == '')
    $pdf->Cell(130, 3, 'Todos', 0, 1);
  else
    $pdf->Cell(130, 3, $estado, 0, 1);

  $pdf->Cell(25, 3, "Cidade:", 0, 0, 'R');

  if ($cidade == '')
    $pdf->Cell(130, 3, 'Todas', 0, 1);
  else
    $pdf->Cell(130, 3, $cidade, 0, 1);

  $pdf->Cell(25, 3, "Bairro:", 0, 0, 'R');

  if ($bairro == '')
    $pdf->Cell(130, 3, 'Todos', 0, 1);
  else
    $pdf->Cell(130, 3, $bairro, 0, 1);

  $pdf->Cell(25, 3, "Gradua��o:", 0, 0, 'R');

  if ($graduacao == '')
    $pdf->Cell(130, 3, 'Independente', 0, 1);
  else
    $pdf->Cell(130, 3, $func->retornaNomeGraduacao($graduacao), 0, 1);

  $pdf->Cell(25, 3, "Especialidade:", 0, 0, 'R');

  if ($especialidade == '')
    $pdf->Cell(130, 3, 'Todas', 0, 1);
  else
    $pdf->Cell(130, 3, $func->retornaNomeEspecialidade($bd, $especialidade), 0, 1);

  if ($_SESSION['apelido_operadora'] <> 'affego') {
    $pdf->Cell(25, 3, "�rea de atua��o:", 0, 0, 'R');

    if ($area_prestador == '')
      $pdf->Cell(130, 3, 'Todas', 0, 1);
    else
      $pdf->Cell(130, 3, $func->retornaNomeArea($bd, $area_prestador), 0, 1);
  }

  if ($_SESSION['apelido_operadora'] == 'sampes') {
    $pdf->Cell(25, 3, "Indic�vel:", 0, 0, 'R');
    $pdf->Cell(130, 3, $func->simNao($indicavel, "Todos"), 0, 1);
  }

  $pdf->Cell(190, 3, ' ', 'B', 1);

  $func = new Funcao();
  
  $rede = new RedeCredenciada();
  $rede->plano                = $plano;
  $rede->nome_prestador       = $nome_prestador;
  $rede->tipo_prestador       = $tipo_prestador;
  $rede->tipoRede             = $tipoRede;
  $rede->estado               = $estado;
  $rede->cidade               = $cidade;
  $rede->bairro               = $bairro;
  $rede->graduacao            = $graduacao;
  $rede->especialidade        = $especialidade;
  $rede->area_prestador       = $area_prestador;
  $rede->indicavel            = $indicavel;
  
  $rede->processaRedeCredenciada(false);

  if ($rede->total_registros > 0) {
    if ($plano > 0) {

      $sql_plano = new Query();
      $txt_plano = "SELECT HSSPLAN.CDESCPLAN,NVL(TO_CHAR(HSSPLAN.NRGMSPLAN),HSSPLAN.CANTIPLAN) NRGMSPLAN,
                           HSSPLAN.CSITUPLAN
                      FROM HSSPLAN
                     WHERE NNUMEPLAN = :idPlano";
      $sql_plano->addParam(":idPlano",$plano);
      $sql_plano->executeQuery($txt_plano);

      if ($sql_plano->result("CSITUPLAN") == "C")
        $situ_plano = 'Cancelado';
      else if ($sql_plano->result("CSITUPLAN") == "S")
        $situ_plano = 'Ativo (comercializa��o suspensa)';
      else
        $situ_plano = 'Ativo';

      $desc_grupo = 'Plano: ' . $sql_plano->result("CDESCPLAN").
                    ' - Reg.: ' . $formata->formataRegistroAns($sql_plano->result("NRGMSPLAN")) . ' - ' .
                    $situ_plano;

      $pdf->SetTextColor(0,0,128);
      $pdf->SetFont('Arial', 'B', 6);
      $pdf->Cell(100, 5, $desc_grupo, '', 1);
      $pdf->SetFont('Arial', '', 6);
      $pdf->SetTextColor(0,0,0);
    }

    foreach ($rede->grupo02 as $grupo02) {

      if ($grupo02->nome <> '') {
        // Inicio do calculo de tamanho do registro
        $tamanhoRegistro = 8;
        
        if ($grupo02->registros[0]->fantasia <> '')
          $tamanhoRegistro += 3;
        
        if (sizeOf($grupo02->registros[0]->lista_graduacoes) > 0)
          $tamanhoRegistro += 6;

        foreach ($grupo02->registros[0]->lista_especialidades as $especialidade) {
          $tamanhoRegistro += 3;
        
          if ($grupo02->registros[0]->tipo_pessoa == 'J') {
            if ($_SESSION['apelido_operadora'] <> 'UnimedLestePaulista')
              foreach ($especialidade->lista_corpo_clinico as $corpo)
                $tamanhoRegistro += 3;
          }
          else {
            foreach ($especialidade->lista_areas as $area)
              $tamanhoRegistro += 3;
          }
        }
        
        if ($grupo02->registros[0]->data_cancelamento <> '') {
          $tamanhoRegistro += 3;
          
          $i = 0;
          foreach ($grupo02->registros[0]->lista_substitutos as $subs) {
            $i++;           
            if ($i > 1)
              $tamanhoRegistro += 3;
          }
        }         
        
        foreach ($grupo02->registros[0]->lista_enderecos as $endereco) {
          $tamanhoRegistro += 4;

          if ($grupo02->registros[0]->homepage <> '')
            $tamanhoRegistro += 3;
        }         
        // Fim do calculo de tamanho do registro        
          
        if ($pdf->getY() + $tamanhoRegistro > 265)
          $pdf->AddPage();
      
        $pdf->SetTextColor(0,0,128);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(100, 5, $grupo02->nome, '', 1);
        $pdf->SetFont('Arial', '', 6);
        $pdf->SetTextColor(0,0,0);
      }
      
      foreach ($grupo02->registros as $reg) {
      
        // Inicio do calculo de tamanho do registro
        $tamanhoRegistro = 3;
        
        if ($reg->fantasia <> '')
          $tamanhoRegistro += 3;
        
        if (sizeOf($reg->lista_graduacoes) > 0)
          $tamanhoRegistro += 6;

        foreach ($reg->lista_especialidades as $especialidade) {
          $tamanhoRegistro += 3;
        
          if ($reg->tipo_pessoa == 'J') {
            if ($_SESSION['apelido_operadora'] <> 'UnimedLestePaulista')
              foreach ($especialidade->lista_corpo_clinico as $corpo)
                $tamanhoRegistro += 3;
          }
          else {
            foreach ($especialidade->lista_areas as $area)
              $tamanhoRegistro += 3;
          }
        }
        
        if ($reg->data_cancelamento <> '') {
          $tamanhoRegistro += 3;
          
          $i = 0;
          foreach ($reg->lista_substitutos as $subs) {
            $i++;           
            if ($i > 1)
              $tamanhoRegistro += 3;
          }
        }          
        
        foreach ($reg->lista_enderecos as $endereco) {
          $tamanhoRegistro += 4;

          if ($reg->homepage <> '')
            $tamanhoRegistro += 3;
        }         
        // Fim do calculo de tamanho do registro        
          
        if ($pdf->getY() + $tamanhoRegistro > 265)
          $pdf->AddPage();
          
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(100, 3, $reg->nome." ".$reg->documento." - ".$func->tipoPrestador($reg->tipo_prestador), '', 1);
        $pdf->SetFont('Arial', '', 6);

        if ($reg->data_cancelamento <> '') {
          
          $pdf->SetTextColor(255,0,0);
          $txtCancelamento = "Cancelado em: ". $reg->data_cancelamento;
          $pdf->Cell(30, 3, $txtCancelamento, '',0);
          
          $i = 0;
          $pdf->SetTextColor(0,0,128);
          foreach ($reg->lista_substitutos as $subs) {
            $i++;
            $prestador_substituto = new Prestador($subs->idPrestador);
            $txtCancelamento = 'Substituido por: '.$prestador_substituto->nome." em ".$subs->inicio;
            
            if ($i > 1)
              $pdf->Cell(30, 3,'', '',0);
            
            $pdf->Cell(70, 3, $txtCancelamento, '',1);
          }

          if ($i == 0)
            $pdf->Ln(3);
            
          $pdf->SetTextColor(0,0,0);          
        }
          
        if ($reg->fantasia <> '')
          $pdf->Cell(100, 3, $reg->fantasia, '', 1);
               
        /* Inicio impress�o da Gradua��o */
        
        /* Seta valores horizontais e verticais das imagens */        
        $x = $pdf->GetX() + 4;
        $y = $pdf->GetY();
        $temGraduacao = false;

        foreach ($reg->lista_graduacoes as $graduacao) {
          $temGraduacao = true;
          
          $pdf->Image($graduacao->imagem, $x, $y);
          $x = $x + 5;
        }

        if ($temGraduacao == true)
          $pdf->Ln(6);
        /* Fim impress�o da gradua��o. */        
        
        //Especialidades
        foreach ($reg->lista_especialidades as $especialidade) {
          $nome_especialidade = $especialidade->nome;

          if ($especialidade->num_certificacao <> '')
            $nome_especialidade .= " - RQE n� ".$especialidade->num_certificacao;

          $pdf->SetFont('Arial', 'B', 6);              
          $pdf->Cell(3, 3, '', '', 0);
          $pdf->Cell(100, 3, $nome_especialidade, '', 1);
          $pdf->SetFont('Arial', '', 6);              
        
          if ($reg->tipo_pessoa == 'J') {
          
            if ($_SESSION['apelido_operadora'] <> 'UnimedLestePaulista') {
              $lista_corpo_clinico = $especialidade->lista_corpo_clinico;

              foreach ($lista_corpo_clinico as $corpo) {

                $prestador_corpo = new Prestador($corpo);
                
                $pdf->SetFont('Arial', 'I', 5);              
                $pdf->Cell(6, 3, '', '', 0);
                $pdf->Cell(100, 3, '- '.$formata->initCap($prestador_corpo->nome) . " (" . $prestador_corpo->conselho . '-' . $prestador_corpo->uf_conselho . ' ' . $prestador_corpo->doc_conselho . ")", '', 1);
                $pdf->SetFont('Arial', '', 6);              
              }
            }
          }
          else {
            $lista_areas = $especialidade->lista_areas;
            foreach ($lista_areas as $area) {
              $nome_area = $area->nome;

              if ($area->num_certificacao <> '')
                $nome_area .= " - RQE n� ".$area->num_certificacao;

              $pdf->SetFont('Arial', 'I', 5);              
              $pdf->Cell(6, 3, '', '', 0);
              $pdf->Cell(100, 3, '- '.$nome_area, '', 1);
              $pdf->SetFont('Arial', '', 6);              
            }
          }
        }
        
        //Enderecos
        foreach ($reg->lista_enderecos as $endereco) {

          $pdf->Ln(1);
          $pdf->Cell(3, 3, '', '', 0);

          $pdf->Cell(100, 3,'Tel: '.$endereco->telefone, '', 1);
          $pdf->Cell(3, 3, '', '', 0);
          $pdf->Cell(100, 3, $endereco->endereco, '', 1);
          $pdf->Cell(3, 3, '', '', 0);
          $pdf->Cell(100, 3, $endereco->bairro . " - " . $endereco->cidade . " - " . $endereco->estado." - CEP: ".$endereco->cep, '', 1);
                   
          if ($reg->homepage <> '') {
            $pdf->Cell(3, 3, '', '', 0);
            $pdf->Cell(100, 3, $reg->homepage, '', 1);
            $pdf->Ln(3);
          }          

          $pdf->Ln(3);
        }        
      }
    }

    $file = '../temp/' . md5(uniqid(rand(), true)) . '.pdf';
    $pdf->Output($file, 'F');
    echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
  } 
  else
    echo "<HTML><SCRIPT>close();</SCRIPT></HTML>";
?>