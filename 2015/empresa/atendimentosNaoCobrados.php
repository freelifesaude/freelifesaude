<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  
  $_SESSION['titulo'] = "EXTRATO DE ATENDIMENTOS N�O COBRADOS";
  $IP = getenv("REMOTE_ADDR");
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","atendimentosNaoCobrados.htm");

  $func->insereLogOperacao($_SESSION['id_contrato'],'Acesso a tela de atendimentos n�o cobrados','EMPRESA');

  $tpl->ID_SESSAO = $_GET['idSessao'];  
  $tpl->MASCARA   = $_SESSION['mascara'];  
  
  if (isset($_POST['codigo']))
    $codigo = htmlentities($_POST['codigo']);  
  else
    $codigo = ''; 

  if (isset($_POST['nome']))
    $nome = htmlentities($_POST['nome']);  
  else
    $nome = '';	
  
  if (isset($_POST['listar_todos']))
    $todos_usuarios = htmlentities($_POST['listar_todos']);
  else
    $todos_usuarios = '';  
  
  if (isset($_POST['quebrar_pagina']))
    $quebrar = htmlentities($_POST['quebrar_pagina']);
  else
    $quebrar = '';  
  
  if(isset($_POST['cod'])) {
    $tpl->CODIGO = $_POST['cod'];
  }
  
  if (isset($_POST['evento'])) {      
    $sql = new Query($bd);
    $txt = "BEGIN ".
           "  LOG_WEB(:contrato,:operador, :modulo, :ip_computador, :usuario, :listar_todos); ".
           "END;";
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->addParam(":modulo",'Acesso aos atendimentos nao cobrados.');
    $sql->addParam(":ip_computador",$IP);
    $sql->addParam(":usuario",$codigo);	
    $sql->addParam(":listar_todos",$todos_usuarios);
    $erro = $sql->executeSQL($txt); 
  
    if (($codigo == '') and ($todos_usuarios == 'S') and ($_POST['evento'] <> 'saude')) {
      $tpl->CLASSE = "";
      $tpl->MSG    = "Para o extrato de odontologia, �tica e farm�cia. N�o � possivel utilizar a op��o de listar todos os benefici�rios.";
      $tpl->block("ERRO");    
    } else if ( (($codigo <> '') or ($todos_usuarios == 'S')) ) {
      $id_locacao  = $_SESSION['id_locacao'];
      $id_contrato = $_SESSION['id_contrato'];

      if ($_POST['evento'] == 'saude') {
        require_once("atendimentosNaoCobradosImp.php");
      } else if ($_POST['evento'] == 'farmacia') {
      
        if (strlen($codigo) == 20)
          $codigo2 = substr($codigo,5,15);
        else
          $codigo2 = $codigo;
          
        $util->redireciona('sysprodata.php?u='.base64_encode($codigo2),'S');      
      } else if ($_POST['evento'] == 'dental') {
        require_once("atendimentosUniodontoNaoCobradosImp.php");      
      }
    } else {
      $tpl->CLASSE = "";
      $tpl->MSG = "Informe o c�digo do benefici�rio.";
      $tpl->block("ERRO");    
    }
    
    $tpl->CODIGO = $codigo; 

	$tpl->NOME = $nome;	
    
    if ($todos_usuarios == 'S')
      $tpl->TODOS = 'checked';
    else
      $tpl->TODOS = '';
      
    if ($quebrar == 'S')
      $tpl->QUEBRAR = 'checked';
    else
      $tpl->QUEBRAR = '';    
  }
  
  if ($_SESSION['apelido_operadora'] == 'agemed') {
  
    $sql_odonto = new Query();
    $txt_odonto = "SELECT COUNT(*) QTDE FROM HSSUSUA,HSSTXUSU,HSSTXMEN
                    WHERE HSSUSUA.NNUMETITU = :contrato
                      AND HSSUSUA.NNUMEUSUA = HSSTXUSU.NNUMEUSUA
                      AND HSSTXUSU.NNUMETXMEN = HSSTXMEN.NNUMETXMEN   
                      AND HSSTXMEN.CTRECTXMEN = 'O' ";
    $sql_odonto->addParam(":contrato",$_SESSION['id_contrato']);
    $sql_odonto->executeQuery($txt_odonto);
    
    if ($sql_odonto->result("QTDE") > 0)
      $tpl->block("DENTAL");
      
    $sql_farmacia = new Query();
    $txt_farmacia = "SELECT COUNT(*) QTDE FROM HSSUSUA,HSSTXUSU,HSSTXMEN
                      WHERE HSSUSUA.NNUMETITU = :contrato
                        AND HSSUSUA.NNUMEUSUA = HSSTXUSU.NNUMEUSUA
                        AND HSSTXUSU.NNUMETXMEN = HSSTXMEN.NNUMETXMEN   
                        AND HSSTXMEN.CTRECTXMEN IN ('F','T') ";
    $sql_farmacia->addParam(":contrato",$_SESSION['id_contrato']);
    $sql_farmacia->executeQuery($txt_farmacia);      
      
    if ($sql_farmacia->result("QTDE") > 0)      
      $tpl->block("FARMACIA");
                    
    $tpl->block("AGEMED"); 


  } else
    $tpl->block("PADRAO");
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     

?>