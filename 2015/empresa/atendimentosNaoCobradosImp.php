<?php   
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(230,5,$_SESSION['nome_operadora']." - ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(230,5,'EXTRATO DE ATENDIMENTOS N�O COBRADOS - SA�DE',0,1);
      $this->SetFont('Arial','B',7);           
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Empresa:",0,0,'R');
      $this->Cell(205,3,$_SESSION['logado'],0,1); 
      $data = date("d/m/Y H:m:s");
      $this->Cell(77,3,'Emissao: '.$data,0,1,'R'); 			
      $this->SetFont('Arial','B',10);
      $this->Ln(3);
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      if ($_SESSION['apelido_operadora'] == 'unimedSalto') {
        $this->SetTextColor(0,0,0);
        $this->SetFont('Arial','B',8);
        $this->Cell(0,5,'OBS: Lembrando que pode haver consultas que n�o estejam em nosso sistema e que ser�o cobradas nas pr�ximas faturas.');
        $this->SetTextColor(0,64,128);
      }
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    function cabecalho($cobranca) {
      $this->SetFont('Arial','B',8);
      $this->Ln(1);
      $this->SetFont('Arial','B',8);
      $this->Cell(5,3,'',0,0);   

      if (($cobranca == '2'))
        $this->Cell(18,3,'Guia',0,0);
      else
        $this->Cell(18,3,'Conta',0,0);
      
      $this->Cell(18,3,'Atendimento',0,0);
      $this->Cell(60,3,'Prestador',0,0);
      $this->Cell(18,3,'C�digo',0,0);
      $this->Cell(90,3,'Item',0,0);
      $this->Cell(10,3,'Qtde',0,0);
      $this->Cell(15,3,'Co-part',0,0);
      $this->Cell(15,3,'',0,1);
      $this->Cell(18,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(60,3,'',0,0);
      $this->Cell(18,3,'',0,0);
      $this->Cell(90,3,'',0,0);
      $this->Cell(10,3,'',0,0);
      $this->Cell(15,3,'',0,0);
      $this->Cell(12,3,'',0,0);
      $this->Ln(1);  
      $this->SetFont('Arial','',8);      
    }    
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();
  $arr = "";
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  $usuario = "";
  $titular = "";
  $tipo = "";
  $conta = 0;
  $total_geral = 0;
  $desp_usuario = 0;
  $total_copart_titular = 0;
  $total_copart_titular_prestador = 0;
  $total_titular_reciprocidade = 0;
  $total_copart_usuario = 0;
  $total_copart_prestador = 0;
  $total_reciprocidade = 0;
  
  if ($_SESSION['apelido_operadora'] == 'vitallis') {
    $txt_congenere = "SELECT NNUMECONGE FROM HSSTITU
                       WHERE NNUMETITU = :contrato
                         AND HSSTITU.NNUMECONGE = 2040382163 ";
    $sql_conge = new Query($bd);                         
    $sql_conge->addParam(":contrato",$id_contrato);
    $sql_conge->executeQuery($txt_congenere);

    if ($sql_conge->count() > 0) {
      $f_at1 = "   AND HSSCONT.CAMBUCONT <> '6' ";
      $f_at2 = "   AND HSSGUIA.CTIPOGUIA <> '6' ";
    }
  } else {
    $f_at1 = "";
    $f_at2 = "";
  }
  
  if (($codigo <> "") and ($todos_usuarios <> 'S'))
    $f_codigo = "   AND (TITU.CCODIUSUA LIKE :codigo OR TITU.CCARTUSUA LIKE :codigo)";
  else
    $f_codigo = "";
    
  if ($id_locacao > 0) 
    $f_locacao = "   AND TITU.NNUMESETOR = :id_locacao "; 
  else      
    $f_locacao = "";
                   
  if ($_SESSION['apelido_operadora'] != 'unimedSalto') {
  
    $txt = "SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSCONT.NNUMECONT,DATENCONT,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                   USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'C' TIPO,NFRANCONT, 
                   TO_CHAR(DATENCONT,'DD/MM/YY') DATA, TOTAL_CONTA_PLANO(HSSCONT.NNUMECONT) CONTA,HSSCONT.NNUMECONT,USUA.NNUMEPLAN, 
                   IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                   '1' COBRANCA                     
              FROM HSSUSUA TITU,HSSUSUA USUA,HSSCONT,FINPRES,FINPRES SOLI,HSSPLAN 
             WHERE TITU.NNUMETITU = :id_contrato ".
           $f_codigo.
           $f_locacao.
           "   AND HSSCONT.CCOBRCONT <> 'X'
               AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
               AND USUA.CTIPOUSUA <> 'F' 
               AND USUA.NNUMEUSUA = HSSCONT.NNUMEUSUA
               AND USUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ".
           $f_at1.
           "   AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
               AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES
               AND HSSCONT.NNUMECONT NOT IN (SELECT NNUMECONT FROM HSSPACON WHERE NNUMECONT = HSSCONT.NNUMECONT
                                              UNION ALL
                                             SELECT NNUMECONT FROM HSSGCON,HSSGPAGA WHERE NNUMECONT = HSSCONT.NNUMECONT
                                                AND HSSGCON.NNUMEGUIA = HSSGPAGA.NNUMEGUIA)                             
             UNION ALL
            SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,DEMISGUIA,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                   USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'G',0,
                   TO_CHAR(DEMISGUIA,'DD/MM/YY') DATA, TOTAL_GUIA(HSSGUIA.NNUMEGUIA) CONTA,HSSGUIA.NNUMEGUIA,USUA.NNUMEPLAN,
                   IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                   '2' COBRANCA
              FROM HSSUSUA TITU,HSSUSUA USUA,HSSGUIA,FINPRES,FINPRES SOLI,HSSPLAN
             WHERE TITU.NNUMETITU = :id_contrato ".
           $f_codigo.
           $f_locacao.
           "   AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
               AND USUA.NNUMEUSUA = HSSGUIA.NUTILUSUA
               AND USUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ".
           $f_at2.
           "   AND CSTATGUIA IS NULL
           AND CLOCAGUIA <> 'I'
               AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES
               AND HSSGUIA.NSOLIPRES = SOLI.NNUMEPRES
               AND HSSGUIA.NNUMEGUIA NOT IN (SELECT NNUMEGUIA FROM HSSGCON WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA
                                              UNION ALL
                                             SELECT NNUMEGUIA FROM HSSGPAGA WHERE NNUMEGUIA = HSSGUIA.NNUMEGUIA)                              
             ORDER BY 8,2,19,4, 3";
  } else {
  
    $txt = "SELECT USUA.CGRAUUSUA,USUA.CNOMEUSUA,HSSCONT.NNUMECONT,DATENCONT,FINPRES.CNOMEPRES,SOLI.CNOMEPRES SOLICITANTE,
                   USUA.CCODIUSUA,TITU.CNOMEUSUA TITULAR,TITU.CCODIUSUA CODIGO_TITULAR,'C' TIPO,NFRANCONT, 
                   TO_CHAR(DATENCONT,'DD/MM/YY') DATA, TOTAL_CONTA_PLANO(HSSCONT.NNUMECONT) CONTA,HSSCONT.NNUMECONT,USUA.NNUMEPLAN, 
                   IDADE(USUA.DNASCUSUA,SYSDATE) IDADE, TO_CHAR(USUA.DINCLUSUA,'DD/MM/YY') DINCLUSUA,NVL(HSSPLAN.CNPCAPLAN,HSSPLAN.CDESCPLAN) PLANO,
                   '1' COBRANCA                     
              FROM HSSUSUA TITU,HSSUSUA USUA,HSSCONT,FINPRES,FINPRES SOLI,HSSPLAN 
             WHERE TITU.NNUMETITU = :id_contrato ".
           $f_codigo.
           $f_locacao.
           "   AND HSSCONT.CCOBRCONT <> 'X'
               AND TITU.NNUMEUSUA  = USUA.NTITUUSUA
               AND USUA.CTIPOUSUA <> 'F' 
               AND USUA.NNUMEUSUA = HSSCONT.NNUMEUSUA
               AND USUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN ".
           $f_at1.
           "   AND HSSCONT.NNUMEPRES = FINPRES.NNUMEPRES
               AND HSSCONT.NSOLIPRES = SOLI.NNUMEPRES
               AND HSSCONT.NFRANCONT > 0
               AND HSSCONT.NNUMECONT NOT IN (SELECT NNUMECONT FROM HSSPACON WHERE NNUMECONT = HSSCONT.NNUMECONT
                                              UNION ALL
                                             SELECT NNUMECONT FROM HSSGCON,HSSGPAGA WHERE NNUMECONT = HSSCONT.NNUMECONT
                                                AND HSSGCON.NNUMEGUIA = HSSGPAGA.NNUMEGUIA)                                                           
             ORDER BY 8,2,19,4, 3";

  }                 
         
  $sql = new Query($bd);                         
  $sql->addParam(":id_contrato",$_SESSION['id_contrato']); 
  
  $codigo2 = $codigo."%";
  $sql->addParam(":codigo",$codigo2);
   
  if ($id_locacao > 0)   
    $sql->addParam(":id_locacao",$id_locacao);  
       
  $sql->executeQuery($txt);    

  if ($sql->count() > 0) {
  
    while (!$sql->eof()) {
   
      if ($titular <> $sql->result("TITULAR")) {   

        if (($quebrar == 'S') and ($titular <> ""))
          $pdf->AddPage();    
          
        $pdf->Cell(270,3,'Titular: '.$sql->result("TITULAR"),0,1);
        $pdf->Ln(3);        
        $titular                        = $sql->result("TITULAR");
        $total_copart_titular           = 0;
        $total_copart_titular_prestador = 0;
        $total_titular_reciprocidade    = 0;
        $tipo                           = "";
      }

      if ($usuario <> $sql->result("CNOMEUSUA")) {
        $pdf->SetTextColor(0,64,128);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(20,3,'Benefici�rio: ',0,0,'R');
        $pdf->Cell(100,3,$sql->result("CCODIUSUA").' - '.$sql->result("CNOMEUSUA"),0,0);      
        $pdf->Cell(20,3,'Idade: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("IDADE").' ANOS',0,0);
        $pdf->Cell(20,3,'Inclus�o: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("DINCLUSUA"),0,1);
        $pdf->Cell(20,3,'Plano: ',0,0,'R');        
        $pdf->Cell(20,3,$sql->result("PLANO"),0,1);
        $pdf->Ln(3);        
        $usuario                = $sql->result("CNOMEUSUA");
        $codigo_usuario         = $sql->result("CCODIUSUA");
        $total_copart_usuario   = 0;
        $total_copart_prestador = 0;
        $total_reciprocidade    = 0;
        $tipo                   = "";
        $pdf->SetTextColor(0,0,0);
      }
      
      if ($tipo <> $sql->result("COBRANCA")) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Ln(3);
        
        if ($sql->result("COBRANCA") == '1') 
          $pdf->Cell(50,3,'CONTAS FATURADAS',0,0);
        elseif ($sql->result("COBRANCA") == '2') 
          $pdf->Cell(50,3,'GUIAS AUTORIZADAS',0,0);
          
        $pdf->Ln(3);
        $pdf->cabecalho($sql->result("COBRANCA"));
        
        $tipo = $sql->result("COBRANCA");
      }      

      $pdf->Cell(5,3,'',0,0);      
      $pdf->Cell(18,3,$sql->result("NNUMECONT"),0,0);
      $pdf->Cell(18,3,$sql->result("DATA"),0,0);
      $pdf->Cell(60,3,$pdf->Copy($sql->result("CNOMEPRES"),59),0,0);

      if ($sql->result("TIPO") == "G") {
        $txt2 = "SELECT HSSPGUI.CCODIPMED,CNOMEPMED,NFRANPGUI COPART,NQINDPGUI VALOR,NQUANPGUI QTDE,
                        NCOPRPGUI COPART_PRES,0 RECIPROCIDADE
                   FROM HSSPGUI,HSSPMED
                  WHERE NNUMEGUIA = :numero
                    AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED";
      } else {
        $txt2 = "SELECT HSSPCON.CCODIPMED,CNOMEPMED,NFRANPCON COPART,TOTAL_CONTA_HONORARIOS_PCON(HSSPCON.NNUMEPCON) VALOR,NQUANPCON QTDE,
                        NCOPRPCON COPART_PRES,NVL(NRECIPCON,0)+NVL(NREC1PCON,0)+NVL(NREC2PCON,0)+NVL(NREC3PCON,0)+NVL(NREC4PCON,0) RECIPROCIDADE
                   FROM HSSPCON,HSSPMED
                  WHERE NNUMECONT = :numero
                    AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                    AND HSSPCON.NFRANPCON > 0
                  UNION
                 SELECT 'TAXAS' CCODIPMED,CDESCTAXA CNOMEPMED,SUM(NFRANTCON) COPART,SUM(NVALOTCON) VALOR,SUM(NQUANTCON) QTDE,
                        0 COPART_PRES,SUM(NVL(NRECITCON,0)) RECIPROCIDADE
                   FROM HSSTCON,HSSTAXA
                  WHERE NNUMECONT = :numero
                    AND NVALOTCON > 0
                    AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                    AND HSSTCON.NFRANTCON > 0
                  GROUP BY CDESCTAXA
                  UNION
                 SELECT 'MAT/MED' CCODIPMED,'MATERIAIS E MEDICAMENTOS' CNOMEPMED,VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT) COPART,TOTAL_FARMACIA(HSSCONT.NNUMECONT) VALOR, 1 QTDE,
                        0 COPART_PRES,0 RECIPROCIDADE
                   FROM HSSCONT
                  WHERE NNUMECONT = :numero
                    AND TOTAL_FARMACIA(HSSCONT.NNUMECONT) > 0 
                    AND VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT) > 0 ";
      }

      $sql3 = new Query($bd);
      $sql3->addParam(":numero",$sql->result("NNUMECONT"));
      $sql3->executeQuery($txt2);
      
      $qtde_proc = 0;

      while (!$sql3->eof()) {
        if ($qtde_proc > 0)
          $pdf->Cell(101,3,'',0,0);

        $pdf->Cell(18,3,$sql3->result("CCODIPMED"),0,0);
        $pdf->Cell(90,3,$pdf->Copy($sql3->result("CNOMEPMED"),89),0,0);
        $pdf->Cell(10,3,$sql3->result("QTDE"),0,0,'C');
        $pdf->Cell(15,3,$formata->formataNumero($sql3->result("COPART")),0,0,'R');	  
        $pdf->Cell(15,3,'',0,1,'R');

        $qtde_proc++;
        $total_copart_usuario   += str_replace(',','.',$sql3->result("COPART"));
        $total_copart_prestador += str_replace(',','.',$sql3->result("COPART_PRES"));
        $total_reciprocidade    += str_replace(',','.',$sql3->result("RECIPROCIDADE"));

        $total_copart_titular           += str_replace(',','.',$sql3->result("COPART"));
        $total_copart_titular_prestador += str_replace(',','.',$sql3->result("COPART_PRES"));
        $total_titular_reciprocidade    += str_replace(',','.',$sql3->result("RECIPROCIDADE"));
        $sql3->next();
      }

      $sql->next();
      
      if (($usuario <> $sql->result("CNOMEUSUA")) and ($_SESSION['apelido_operadora'] <> 'unimedBarra')) {
        $pdf->Ln(3);      
        $pdf->Cell(219,3,'Total do usu�rio: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_copart_usuario),0,0,'R');
        $pdf->Ln(6);  
      }

      if ($titular <> $sql->result("TITULAR")) {
        $pdf->Cell(219,3,'Total do titular: ',0,0,'R');
        $pdf->Cell(15,3,$formata->formataNumero($total_copart_titular),0,0,'R');
        $pdf->Ln(3);
      }

    }    
    
    $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
    $pdf->Output($file,'F');
    $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";
    
  } else {
    $tpl->CLASSE = "";
    $tpl->MSG = "N�o existe nenhum atendimento de sa�de sem cobran�a.";
    $tpl->block("ERRO");
  }  
?>
