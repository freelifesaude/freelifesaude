<?php
  require_once("../comum/autoload.php");
  $seg->secureSessionStart();   
  require_once('../comum/sessao.php'); 
       
  $bd = new Oracle();  
  
  $idArquivo = $seg->antiInjection($_POST['pIdArquivo']);
    
  $sql = new Query($bd);
  $txt = "SELECT COUNT(*) ARQUIVO FROM HSSARQUI 
           WHERE NSUPEARQUI = :idArquivo";
  $sql->addParam(":idArquivo",$idArquivo);
  $sql->executeQuery($txt);  
  if($sql->result("ARQUIVO") <= 0){
    $sql->clear();
    $txt = "DELETE FROM HSSARQUI 
             WHERE NNUMEARQUI = :idArquivo";
    $sql->addParam(":idArquivo",$idArquivo);
    $sql->executeSQL($txt); 
    echo "1";    
  }else
    echo "2"; 

  $bd->close();
?>