<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
  
  if (isset($_POST['prestador'])){
  
    if ($_POST['prestador'] <> "") {
	  $sql = new Query($bd);
	  $i = 0;
	  //busca a especialidade do corpo clínico, isso foi feito para facilitar os casos de saúde ocupacional
	  //onde o prestador está com a especialidade MEDICINA OCUPACIONAL diretamente no corpo clínico	
	  if($_POST['espCorpoClinico'] == 'S'){	          
        $txt = "SELECT DISTINCT HSSESPEC.NNUMEESPEC,CNOMEESPEC
  	              FROM HSSCCLIN,FINPRES,HSSESPEC 
                 WHERE HSSCCLIN.NNUMEPRES = :prestador 
			       AND HSSCCLIN.NHOSPPRES = :ambulatorio
				   AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSCCLIN.NNUMEESPEC = HSSESPEC.NNUMEESPEC 
                 ORDER BY CNOMEESPEC ";
        $sql->addParam(":prestador",$_POST['prestador']);
		$sql->addParam(":ambulatorio",$_POST['ambulatorio']);
        $sql->executeQuery($txt);
		$i = $sql->count();
	  }
	  
      if(($_POST['espCorpoClinico'] != 'S') or ($i == 0)){
        $sql = new Query($bd);      
        $txt = "SELECT DISTINCT HSSESPEC.NNUMEESPEC,CNOMEESPEC,CPADRESPRE FROM HSSESPRE,HSSESPEC 
                 WHERE NNUMEPRES = :prestador 
                   AND HSSESPRE.NNUMEESPEC = HSSESPEC.NNUMEESPEC 
                   AND NNUMEEPRES IS NULL
                   AND HSSESPRE.CLWEBESPRE = 'S'
                 ORDER BY CPADRESPRE DESC,CNOMEESPEC ";
        $sql->addParam(":prestador",$_POST['prestador']);
        $sql->executeQuery($txt);
	  }
      while (!$sql->eof()) {
        $opcao = '<option value="'.$sql->result("NNUMEESPEC").'" ';
        
        if ($_POST['especialidade'] == $sql->result("NNUMEESPEC"))
          $opcao .= ' selected ';
          
        $opcao .= '>'.$sql->result("CNOMEESPEC").'</option>';
          
        echo $opcao;
        
        $sql->next();
      }  
    }
  } 
 
  $bd->close(); 
?>