<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd   = new Oracle();   
  $func = new Funcao();
  $seg  = new Seguranca();
  
  if (isset($_SESSION['id_estabelecimento']))
    $idEstabelecimento = $_SESSION['id_estabelecimento'];
  else
    $idEstabelecimento = 0;
    
  if (isset($_POST['codigo']) and $_POST['codigo'] <> "") {
    $codigo = trim($_POST['codigo']);
    
    $sql = new Query($bd);
    
  if ($_SESSION['apelido_operadora'] == 'casf') {
   $txt = "SELECT CNOMEUSUA,CDINDUSUA,HSSUSUA.NNUMEUSUA NNUMEUSUA,
                     SUBSTR(NVL(HSSCLACL.CCOR_CLACL,CL.CCOR_CLACL),1,3)||','||SUBSTR(NVL(HSSCLACL.CCOR_CLACL,CL.CCOR_CLACL),4,3)||','||SUBSTR(NVL(HSSCLACL.CCOR_CLACL,CL.CCOR_CLACL),7,3) COR,
                     NVL(HSSCLACL.CRECOCLACL,CL.CRECOCLACL) CRECOCLACL, DNASCUSUA, CL.CWEB_CLACL,HSSUSUA.CCARTUSUA,
                     DECODE(CTIPOUSUA,'T','TITULAR','F','TITULAR FINANCEIRO','A','AGREGADO','DEPENDENTE') CATEGORIA,
                     TO_CHAR(DNASCUSUA,'DD/MM/YYYY') NASCIMENTO, CCODIUSUA
                FROM HSSUSUA,HSSCLACL,HSSTITU, HSSCLACL CL 
               WHERE CCARTUSUA = :codigo 
                 AND HSSUSUA.NNUMECLACL = HSSCLACL.NNUMECLACL(+)
                 AND HSSTITU.NNUMECLACL = CL.NNUMECLACL(+)
                 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";    
  }
  else {  
    $txt = "SELECT CNOMEUSUA,CDINDUSUA,HSSUSUA.NNUMEUSUA NNUMEUSUA,
                   SUBSTR(NVL(HSSCLACL.CCOR_CLACL,CL.CCOR_CLACL),1,3)||','||SUBSTR(NVL(HSSCLACL.CCOR_CLACL,CL.CCOR_CLACL),4,3)||','||SUBSTR(NVL(HSSCLACL.CCOR_CLACL,CL.CCOR_CLACL),7,3) COR,
                   NVL(HSSCLACL.CRECOCLACL,CL.CRECOCLACL) CRECOCLACL, DNASCUSUA, CL.CWEB_CLACL,HSSUSUA.CCARTUSUA,
                   DECODE(CTIPOUSUA,'T','TITULAR','F','TITULAR FINANCEIRO','A','AGREGADO','DEPENDENTE') CATEGORIA,
                   TO_CHAR(DNASCUSUA,'DD/MM/YYYY') NASCIMENTO, CCODIUSUA
              FROM HSSUSUA,HSSCLACL,HSSTITU, HSSCLACL CL 
             WHERE CCODIUSUA = :codigo";
      
    if ($seg->permissaoOutros($bd,'WEBPRESTADORNOMOSTRAUSUARIOPCMSO',false) )      
      $txt .=" AND NVL(CASO_TITU,'N') = 'N'";
          
    $txt .="   AND HSSUSUA.NNUMECLACL = HSSCLACL.NNUMECLACL(+)
               AND HSSTITU.NNUMECLACL = CL.NNUMECLACL(+)
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";
  }       
               
    if ($idEstabelecimento > 0 ) {
      $txt .= "AND HSSTITU.NNUMEESTAB = :estabelecimento ";
      $sql->addParam(":estabelecimento",$idEstabelecimento);                                  
    }
    
    // Para casse lista somente beneficiarios da rede de atendimento do prestador logado
    if (($_SESSION['sistema'] == 'Prestador') and ($_SESSION['apelido_operadora'] == 'casse')) {
      $txt .= "   AND HSSUSUA.NNUMEPLAN IN (SELECT NNUMEPLAN FROM HSSREDPL,HSSPREDE
                                             WHERE HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA
                                               AND HSSPREDE.NNUMEPRES = :prestador
                                             UNION ALL
                                            SELECT NNUMEPLAN FROM HSSPLAN
                                             WHERE 0 = (SELECT COUNT(*) FROM HSSPREDE WHERE NNUMEPRES = :prestador))";
      $sql->addParam(":prestador",$_SESSION['id_contratado']);                                            
    }   
		
    
    $txt .= "   AND (CSITUUSUA = 'A' OR (CSITUUSUA <> 'A' AND DALIBUSUA >= TRUNC(SYSDATE)) OR (CSITUUSUA = 'M' AND DALIBUSUA >= TRUNC(SYSDATE)))";		
    
    $txt .= "ORDER BY CSITUUSUA";      
    $sql->addParam(":codigo",$codigo);
    $sql->executeQuery($txt);
      
    $json['nome']           = utf8_encode($sql->result("CNOMEUSUA"));
    $json['digital']        = utf8_encode($sql->result("CDINDUSUA"));
    $json['recomen']        = str_replace('?o','ao',utf8_encode($sql->result("CRECOCLACL")));
    $json['idade']          = $func->aniversario($sql->result("NNUMEUSUA"));
    $json['numero_usuario'] = utf8_encode(md5($sql->result("NNUMEUSUA")));
    $json['categoria']      = utf8_encode($sql->result("CATEGORIA"));
    $json['nascimento']     = utf8_encode($sql->result("NASCIMENTO"));
    $json['carteira']       = utf8_encode($sql->result("CCARTUSUA"));
    $json['codigo']         = utf8_encode($sql->result("CCODIUSUA"));
    
	  if ($sql->result("CWEB_CLACL")== "S")    
      $json['cor']            = $sql->result("COR");
    else
      $json['cor']            = '';
                  
    if ($func->verificaBiometria ($bd,$codigo,'N','N') == false)
      $json['biometria'] = 'S';
    else
      $json['biometria'] = 'N';
  }  
  else {
    $json['nome'] = '';
  }

  echo json_encode($json);       
    
  
  $bd->close();
  
?>