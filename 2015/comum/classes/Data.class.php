<?php
 date_default_timezone_set('America/Sao_Paulo');
 
  class Data {
  
  	function __call($func,$arq) {
		  if(!method_exists(get_class($this),$func)){
			  throw new Exception("Classe: Data - O metodo \"$func\" nao existe");
		  }
	  }
    
    function dataAtual($mascara = 'DD/MM/YYYY') {
      //Conversao da mascara do php para a mascara do oracle;    
      if ($mascara == 'd/m/Y')
        $mascara = 'DD/MM/YYYY';
      else if ($mascara == 'd/m/Y H:i:s')
        $mascara = 'DD/MM/YYYY HH24:MI:SS';
      else if ($mascara == 'Ymd')
        $mascara = 'YYYYMMDD';
      else if ($mascara == 'm/Y')
        $mascara = 'MMYYYY';        
      else if ($mascara == 'Y')
        $mascara = 'YYYY';   
      else if ($mascara == 'm')
        $mascara = 'MM';          
	  else if ($mascara == 'd/m/Y HH24:mi')
        $mascara = 'DD/MM/YYYY HH24:MI';
        
      $sql = new Query();
      $sql->clear();
      $txt = "SELECT TO_CHAR(SYSDATE,:mascara) DATA FROM DUAL ";
      $sql->addParam(":mascara",$mascara);  
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");      
    }
    
    function boasVindas() {
      $hora = date('G');
        
      if (($hora >= 0) and ($hora < 6)) {
        $mensagem = "Boa madrugada";
      } else if (($hora >= 6) and ($hora < 12)) {
        $mensagem = "Bom dia";
      } else if (($hora >= 12) and ($hora < 18)) {
        $mensagem = "Boa tarde";
      } else {
        $mensagem = "Boa noite".$hora;
      }
       
      return $mensagem;
    }
    
    function diaSemanaOracle($dia) {

      if($dia==1) { $semana =  "Domingo"; }
      elseif($dia==2) { $semana = "Segunda-feira"; }
      elseif($dia==3) { $semana = "Ter�a-feira"; }
      elseif($dia==4) { $semana = "Quarta-feira"; }
      elseif($dia==5) { $semana = "Quinta-feira"; }
      elseif($dia==6) { $semana = "Sexta-feira"; }
      elseif($dia==7) { $semana = "S�bado"; }
      elseif($dia==8) { $semana = "Feriado"; }  
      
      return $semana;
    }    
  
    function dataExtenso($data1,$completa) {
      $data = getDate(strtotime(substr($data1,6,4)."-".substr($data1,3,2)."-".substr($data1,0,2)));

      if($data["wday"]==0) { $semana =  "Domingo, "; }
      elseif($data["wday"]==1) { $semana = "Segunda-feira, "; }
      elseif($data["wday"]==2) { $semana = "Ter�a-feira, "; }
      elseif($data["wday"]==3) { $semana = "Quarta-feira, "; }
      elseif($data["wday"]==4) { $semana = "Quinta-feira, "; }
      elseif($data["wday"]==5) { $semana = "Sexta-feira, "; }
      elseif($data["wday"]==6) { $semana = "S�bado, "; }

      if($data["mon"]==1) { $mes="Janeiro"; }
      elseif($data["mon"]==2) { $mes="Fevereiro"; }
      elseif($data["mon"]==3) { $mes="Mar�o"; }
      elseif($data["mon"]==4) { $mes="Abril"; }
      elseif($data["mon"]==5) { $mes="Maio"; }
      elseif($data["mon"]==6) { $mes="Junho"; }
      elseif($data["mon"]==7) { $mes="Julho"; }
      elseif($data["mon"]==8) { $mes="Agosto"; }
      elseif($data["mon"]==9) { $mes="Setembro"; }
      elseif($data["mon"]==10) { $mes="Outubro"; }
      elseif($data["mon"]==11) { $mes="Novembro"; }
      elseif($data["mon"]==12) { $mes="Dezembro"; }

      if ($completa==1)
        return $semana.$data["mday"]." de ".$mes." de ".$data["year"].".";
      else
        return $data["mday"]." de ".$mes." de ".$data["year"].".";
    }
    
    function mesExtenso($mes) {
      switch ($mes) {
        case '01' : $extenso = 'Janeiro';   break;
        case '02' : $extenso = 'Fevereiro'; break;
        case '03' : $extenso = 'Mar�o';     break;
        case '04' : $extenso = 'Abril';     break;
        case '05' : $extenso = 'Maio';      break;
        case '06' : $extenso = 'Junho';     break;
        case '07' : $extenso = 'Julho';     break;
        case '08' : $extenso = 'Agosto';    break;
        case '09' : $extenso = 'Setembro';  break;
        case '10' : $extenso = 'Outubro';   break;
        case '11' : $extenso = 'Novembro';  break;
        case '12' : $extenso = 'Dezembro';  break;
      }
      
      return $extenso;
    }    
    
    function formataData ($bd,$data,$mascara,$mascara2 = 'DD/MM/YYYY') {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT TO_CHAR(TO_DATE(:data,:mascara2),:mascara) DATA FROM DUAL ";
      $sql->addParam(":data",$data);
      $sql->addParam(":mascara2",$mascara2);
      $sql->addParam(":mascara",$mascara);  
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");
    }    
    
    function dataInvertida($data) {
      return substr($data,6,4)."-".substr($data,3,2)."-".substr($data,0,2);
    } 

    function anoBissexto($ano) {
      if($ano % 4 == 0)
        return true;
      else
        return false;
    } 

    function comparaData($bd,$data1,$data2,$sinal) {

      if ($data2 == '')
        $data2 = $this->dataAtual();
        
      $sql = new Query($bd);
      $txt = "SELECT 1 RETORNO FROM DUAL 
               WHERE TO_DATE(:data1,'DD/MM/YYYY') ".$sinal." TO_DATE(:data2,'DD/MM/YYYY') ";
      $sql->addParam(":data1",$data1);
      $sql->addParam(":data2",$data2);
      $sql->executeQuery($txt);
      
      if ($sql->result("RETORNO") == 1 )
        return 1;
      else 
        return 0;
    } 
    
    function comparaDataHora($bd,$data1,$data2,$sinal) {

      if ($data2 == '')
        $data2 = $this->dataAtual('DD/MM/YYYY HH24:mi');
        
      $sql = new Query($bd);
      $txt = "SELECT 1 RETORNO FROM DUAL 
               WHERE TO_DATE(:data1,'DD/MM/YYYY HH24:mi') ".$sinal." TO_DATE(:data2,'DD/MM/YYYY HH24:mi') ";
      $sql->addParam(":data1",$data1);
      $sql->addParam(":data2",$data2);
      $sql->executeQuery($txt);
      
      if ($sql->result("RETORNO") == 1 )
        return 1;
      else 
        return 0;
    }     
    
    function diferenca ($data1,$data2="",$tipo=""){
      
      if ($data2 == "")
        $data2 = date("d/m/Y H:i");

      if ($tipo == "")
        $tipo = "h";

      $dia1      = substr($data1,0,2);
      $mes1      = substr($data1,3,2);
      $ano1      = substr($data1,6,4);
      $horas1    = substr($data1,11,2);
      $minutos1  = substr($data1,14,2);
      
      $dia2      = substr($data2,0,2);
      $mes2      = substr($data2,3,2);
      $ano2      = substr($data2,6,4);
      $horas2    = substr($data2,11,2);
      $minutos2  = substr($data2,14,2);
           
      $segundos = mktime($horas2,$minutos2,0,$mes2,$dia2,$ano2) - mktime($horas1,$minutos1,0,$mes1,$dia1,$ano1);

      switch($tipo){
        case "m": $difere = $segundos/60;
        break;
        case "H": $difere = $segundos/3600;
        break;
        case "h": $difere = round($segundos/3600);
        break;
        case "D": $difere = $segundos/86400;
        break;
        case "d": $difere = round($segundos/86400);
        break;
      }

      return $difere;
    }    
        
    function validaData($bd,$data) {
      
      $sql = new Query($bd);
      $txt = "SELECT VALIDA_DATA(:data) VALID0 FROM DUAL";
      $sql->addParam(":data",$data);
      $sql->executeQuery($txt);  
     
      if ($sql->result("VALID0") == 0)
        return false;
      else
        return true; 
    } 

    function idade($bd,$data) {
      
      $sql = new Query($bd);
      $txt = "SELECT IDADE(:data) IDADE FROM DUAL";
      $sql->addParam(":data",$data);
      $sql->executeQuery($txt);  
     
      return $sql->result("IDADE");
    } 
	
    function idadeDias($bd,$data) {
      
      $sql = new Query($bd);
      $txt = "SELECT TRUNC(SYSDATE - TO_DATE(:data,'DD/MM/YYYY'),0) IDADE FROM DUAL";
      $sql->addParam(":data",$data);
      $sql->executeQuery($txt);  
     
      return $sql->result("IDADE");
    } 	
    
    function validaHora($horario) {
      $formata = new Formata();
      
      if ($formata->somenteNumeros($horario) == '')
        return false;
      else {
        if (strLen($horario) == 5) {
          $hora      = substr($horario,0,2);
          $separador = substr($horario,2,1);
          $minuto    = substr($horario,3,2); 

          //echo $hora." = ".$separador." = ".$minuto;
          if (($separador <> ':') or
              (intval($hora) > 23) or 
              (intval($minuto) > 60))
            return false;
          else
            return true;
        }
        else
          return false;
      }
    }  
    
    function numeroDias($data1,$data2) {
      $sql = new Query();
      $txt = "SELECT TO_DATE(:data1,'DD/MM/YYYY') - TO_DATE(:data2,'DD/MM/YYYY') DATA FROM DUAL ";
      $sql->addParam(":data1",$data1);
      $sql->addParam(":data2",$data2);
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");
    }    

    function incrementaData($data,$numero,$tipo='D') {
      $sql = new Query();
      
      if ($tipo == 'D')
        $txt = "SELECT TO_CHAR(TO_DATE(:data1,'DD/MM/YYYY') + (:numero),'DD/MM/YYYY') DATA FROM DUAL ";
      else if ($tipo == 'M')
        $txt = "SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:data1,'DD/MM/YYYY'),:numero),'DD/MM/YYYY') DATA FROM DUAL ";
      else if ($tipo == 'A')
        $txt = "SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:data1,'DD/MM/YYYY'),:numero * 12),'DD/MM/YYYY') DATA FROM DUAL ";
      
      $sql->addParam(":data1",$data);
      $sql->addParam(":numero",$numero);
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");
    }    
   
    function incrementaMeses($bd,$data,$numero) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT TO_CHAR(ADD_MONTHS(nvl(TO_DATE(:data,'DD/MM/YYYY'),trunc(sysdate)),nvl(:numero,0)),'DD/MM/YYYY') DATA FROM DUAL ";
      $sql->addParam(":data",$data);
      $sql->addParam(":numero",$numero);
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");
    }    
    
    function proximaDataValida ($bd,$data_str,$incr_decr = 'D') {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT PROXIMA_DATA_VALIDA(:data_str,:incr_decr) DATA FROM DUAL ";
      $sql->addParam(":data_str",$data_str);
      $sql->addParam(":incr_decr",$incr_decr);
      $sql->executeQuery($txt);
      
      return $sql->result("DATA");
    }

    function ehFeriado($bd,$data) { 
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT DDATAFERIA DATA FROM HSSFERIA WHERE DDATAFERIA = :data";
      $sql->addParam(":data",$data);
      $sql->executeQuery($txt);  
      return ($sql->result("DATA") <> '');  
    }    
  }
?>