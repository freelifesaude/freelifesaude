create or replace procedure insere_prospect (id_vendedor       in     number,
                                             estabelecimento   in     number,
                                             tipo_cliente      in     varchar2,
                                             cpf               in     varchar2,
                                             cnpj              in     varchar2,
											                       cei               in     varchar2,
                                             nome              in     varchar2,
                                             fantasia          in     varchar2,
                                             responsavel       in     varchar2,
                                             qtde_usu          in     number,
                                             cep               in     varchar2,
                                             tipo_logradouro   in     number,
                                             logradouro        in     varchar2,
                                             numero            in     varchar2,
                                             complemento       in     varchar2,
                                             bairro            in     varchar2,
                                             estado            in     varchar2,
                                             cidade            in     varchar2,
                                             risco             in     varchar2,
                                             observacao        in     varchar2,
                                             situacao          in     number,
                                             operador          in     number,
                                             atividade         in     number,
                                             empresa           in     number,
                                             unidade_comercial in     number,
                                             ie                in     varchar2,
                                             im                in     varchar2,
                                             id_prospect          out number,
                                             principal         in     number,
                                             agrupador         in     varchar2) as
begin
  select seqgeral.nextval into id_prospect from dual;
  
  insert into hssprosp (nnumeprosp,nnumevend,cnomeprosp,cfantprosp,cpessprosp,ccpf_prosp,ccnpjprosp,ccei_prosp,crespprosp,
                        nqusuprosp,noperusua,cobseprosp,ccep_prosp,cendeprosp,cnumeprosp,
                        nnumetlogr,ccompprosp,cbairprosp,cestaprosp,ccidaprosp,criscprosp,nnumestatc,nnumeestab,nnumeativi,
                        nnumetitu,nnumeuncom,cie__prosp, cim__prosp, nfatuprosp,cagruprosp)
                values (id_prospect,id_vendedor,upper(nome),upper(fantasia),tipo_cliente,cpf,cnpj,cei,upper(responsavel),
                        qtde_usu,operador,observacao,cep,upper(logradouro),numero,
                        tipo_logradouro,upper(complemento),upper(bairro),estado,cidade,risco,situacao,estabelecimento,atividade,
                        empresa,unidade_comercial,ie, im,principal,agrupador);
end;
/

create or replace procedure insere_contatos_prospect (id_prospect       in  number,
                                                      telefone          in  varchar2,
                                                      fax               in  varchar2,
                                                      celular           in  varchar2,
                                                      email             in  varchar2,
                                                      contato           in  varchar2) as

begin                                                      

  insert into hssconpe (nnumeprosp,cteleconpe,cfax_conpe,cceluconpe,cmailconpe,cfuncconpe)
                values (id_prospect,telefone,fax,celular,email,contato);
       
end;
/       
  

create or replace procedure altera_prospect (id_prospect       in     number,
                                             situacao          in     number,
                                             estabelecimento   in     number,
                                             tipo_cliente      in     varchar2,
                                             cpf               in     varchar2,
                                             cnpj              in     varchar2,
											                       cei               in     varchar2,
                                             nome              in     varchar2,
                                             fantasia          in     varchar2,
                                             responsavel       in     varchar2,
                                             qtde_usu          in     number,
                                             cep               in     varchar2,
                                             tipo_logradouro   in     number,
                                             logradouro        in     varchar2,
                                             numero            in     varchar2,
                                             complemento       in     varchar2,
                                             bairro            in     varchar2,
                                             estado            in     varchar2,
                                             cidade            in     varchar2,
                                             risco             in     varchar2,
                                             observacao        in     varchar2,
                                             atividade         in     number,
                                             empresa           in     number,
                                             principal         in     number,
                                             unidade_comercial in     number,
                                             ie                in     varchar2,
                                             agrupador         in     varchar2) as
begin 
  update hssprosp 
     set cnomeprosp = nome,
         cfantprosp = fantasia,
         cpessprosp = tipo_cliente,
         ccpf_prosp = cpf,
         ccnpjprosp = cnpj,
		     ccei_prosp = cei,
         crespprosp = responsavel,
         nqusuprosp = qtde_usu,
         cobseprosp = observacao,
         ccep_prosp = cep,
         cendeprosp = logradouro,
         cnumeprosp = numero,
         nnumetlogr = tipo_logradouro,
         ccompprosp = complemento,
         cbairprosp = bairro,
         cestaprosp = estado,
         ccidaprosp = cidade,
         criscprosp = risco,
         nnumestatc = situacao,
         nnumeestab = estabelecimento,
         nnumeativi = atividade,
         nnumetitu  = empresa,
         nfatuprosp = principal,
         nnumeuncom = unidade_comercial,
         cie__prosp = ie,
         cagruprosp = agrupador
   where nnumeprosp = id_prospect;

end;
/

create or replace procedure insere_planos_proposta(proposta          in number,
                                                   plano             in number,
                                                   inicio            in number,
                                                   fim               in number,
                                                   titulares         in number,
                                                   agregados         in number,
                                                   total             in number,
                                                   classificacao     in number,
                                                   regra_desconto    in number,
                                                   tabela            in number default 0) as     
  
percentual        number;
valor_titular     number;
valor_agregado    number;
unidade_comercial number;
estabelecimento   number;
id_regra_desconto number;
valor_proposta_plano number;
faturamento_minimo   number;

cursor cr_regra_desconto(regra           in number,
                         numero_usuarios in number) is 
  select nvl(npercurdes,0) 
    from hssrdesc,hssurdes
   where hssrdesc.nnumerdesc = regra
     and hssrdesc.caplirdesc = '4'
     and hssrdesc.nnumerdesc = hssurdes.nnumerdesc
     and numero_usuarios >= ninicurdes
     and numero_usuarios <= nfim_urdes
   order by ninicurdes desc;      

cursor cr_prospect is
  select hssprosp.nnumeestab,hssprosp.nnumeuncom
    from hsspropo,hssprosp
   where hsspropo.nnumepropo = proposta
     and hsspropo.nnumeprosp = hssprosp.nnumeprosp;
                                                   
cursor cr_fat_minimo is
  select nvl(nvalofatmi,0) from hssfatmi
   where nnumeplan = plano
     and nnumetitu is null
     and dvigefatmi = (select max(dvigefatmi) from hssfatmi
                        where nnumeplan = plano
                          and nnumetitu is null
                          and dvigefatmi <= sysdate);
                                                   
begin 
-- desenvolvido somente para tabelas baseada em faixa et�ria e quantidade de benefici�rios por contrato

  if (tabela > 0) then
    if (cnpj_operadora() = '53678264000165') then -- Unimed Leste Paulista come�a a venda antes do come�o da vig�ncia dos valores de tabela.
      for reg in (select hsstpla.nnumetpla,hssfeta.nnumefeta,
                         nvl(ndepemanu,0) ndepemanu,nvl(nagremanu,0) nagremanu,nvl(nvalomanu,0) nvalomanu,
                         nvl(nadedmanu,0) nadedmanu,nvl(nadeamanu,0) nadeamanu,nvl(nadetmanu,0) nadetmanu
                    from hsstpla,hssfeta,hssmanu
                   where hsstpla.nnumeplan = plano
                     and hsstpla.nnumetpla = tabela
                     and hsstpla.cweb_tpla = 'S'
                     and hsstpla.nnumetpla = hssfeta.nnumetpla
                     and (hsstpla.dvigetpla is null or hsstpla.dvigetpla >= sysdate)
                     and hssfeta.nminifeta = inicio
                     and hssfeta.nmaxifeta = fim
                     and hssfeta.nnumefeta = hssmanu.nnumefeta
                     and total between hssmanu.nqtdimanu and hssmanu.nqtdfmanu
                     and hssmanu.dvigemanu = (select max(manu.dvigemanu) from hssmanu manu
                                               where manu.dvigemanu <= (sysdate + 30)
                                                 and manu.nnumefeta = hssmanu.nnumefeta)) loop

          valor_titular  := reg.nvalomanu;
          valor_agregado := reg.nagremanu;
        
        insert into hssiprop (nnumepropo,nnumeplan,nnumetpla,nnumefeta,nquaniprop,nvaloiprop,nadesiprop,nagreiprop,nqagriprop,nadeaiprop)
                      values (proposta,plano,reg.nnumetpla,reg.nnumefeta,titulares,valor_titular,reg.nadetmanu,valor_agregado,agregados,reg.nadeamanu);
    
      end loop;
    else 
      for reg in (select hsstpla.nnumetpla,hssfeta.nnumefeta,
                         nvl(ndepemanu,0) ndepemanu,nvl(nagremanu,0) nagremanu,nvl(nvalomanu,0) nvalomanu,
                         nvl(nadedmanu,0) nadedmanu,nvl(nadeamanu,0) nadeamanu,nvl(nadetmanu,0) nadetmanu
                    from hsstpla,hssfeta,hssmanu
                   where hsstpla.nnumeplan = plano
                     and hsstpla.nnumetpla = tabela
                     and hsstpla.cweb_tpla = 'S'
                     and hsstpla.nnumetpla = hssfeta.nnumetpla
                     and (hsstpla.dvigetpla is null or hsstpla.dvigetpla >= sysdate)
                     and hssfeta.nminifeta = inicio
                     and hssfeta.nmaxifeta = fim
                     and hssfeta.nnumefeta = hssmanu.nnumefeta
                     and total between hssmanu.nqtdimanu and hssmanu.nqtdfmanu
                     and hssmanu.dvigemanu = (select max(manu.dvigemanu) from hssmanu manu
                                               where manu.dvigemanu <= sysdate
                                                 and manu.nnumefeta = hssmanu.nnumefeta)) loop

          valor_titular  := reg.nvalomanu;
          valor_agregado := reg.nagremanu;
        
        insert into hssiprop (nnumepropo,nnumeplan,nnumetpla,nnumefeta,nquaniprop,nvaloiprop,nadesiprop,nagreiprop,nqagriprop,nadeaiprop)
                      values (proposta,plano,reg.nnumetpla,reg.nnumefeta,titulares,valor_titular,reg.nadetmanu,valor_agregado,agregados,reg.nadeamanu);
    
      end loop;    
    end if;        
  else
    open cr_prospect;
    fetch cr_prospect into estabelecimento,unidade_comercial;
    close cr_prospect;
    
    if (cnpj_operadora() = '53678264000165') then -- Unimed Leste Paulista come�a a venda antes do come�o da vig�ncia dos valores de tabela.
      for reg in (select hsstpla.nnumetpla,hssfeta.nnumefeta,
                         nvl(ndepemanu,0) ndepemanu,nvl(nagremanu,0) nagremanu,nvl(nvalomanu,0) nvalomanu,
                         nvl(nadedmanu,0) nadedmanu,nvl(nadeamanu,0) nadeamanu,nvl(nadetmanu,0) nadetmanu
                    from hsstpla,hssfeta,hssmanu
                   where hsstpla.nnumeplan = plano
                     and hsstpla.ctipotpla = '1'
                     and hsstpla.cweb_tpla = 'S'
                     and hsstpla.nnumetpla = nvl(tabela,hsstpla.nnumetpla)
                     and ((classificacao = 0) or (hsstpla.nnumectpla = classificacao))                   
                     and hsstpla.nnumetpla = hssfeta.nnumetpla
		  		           and hsstpla.nnumeplan = hssfeta.nnumeplan
                     and (hsstpla.dvigetpla is null or hsstpla.dvigetpla >= sysdate)
                     and hssfeta.nminifeta = inicio
                     and hssfeta.nmaxifeta = fim
                     and hsstpla.nnumetitu is null
                     and (hsstpla.nnumeestab = estabelecimento or hsstpla.nnumeestab is null)
                     and (hsstpla.nnumeuncom = unidade_comercial or hsstpla.nnumeuncom is null)                 
                     and hssfeta.nnumefeta = hssmanu.nnumefeta
                     and total between hssmanu.nqtdimanu and hssmanu.nqtdfmanu
                     and hssmanu.dvigemanu = (select max(manu.dvigemanu) from hssmanu manu
                                               where manu.dvigemanu <= (sysdate + 30)
                                                 and manu.nnumefeta = hssmanu.nnumefeta)) loop
        
        open cr_regra_desconto(regra_desconto,total);
        fetch cr_regra_desconto into percentual;
        if cr_regra_desconto%notfound then
          percentual := 0;
          id_regra_desconto := null;
        else
          id_regra_desconto := regra_desconto;
        end if;
        close cr_regra_desconto;
        
        if (percentual > 0) then
          valor_titular  := round(reg.nvalomanu - (reg.nvalomanu * (percentual / 100)),2);
          valor_agregado := round(reg.nagremanu - (reg.nagremanu * (percentual / 100)),2);      
        else
          valor_titular  := reg.nvalomanu;
          valor_agregado := reg.nagremanu;
        end if;
          
        insert into hssiprop (nnumepropo,nnumeplan,nnumetpla,nnumefeta,nquaniprop,nvaloiprop,nadesiprop,nagreiprop,nqagriprop,nadeaiprop,nnumerdesc)
                      values (proposta,plano,reg.nnumetpla,reg.nnumefeta,titulares,valor_titular,reg.nadetmanu,valor_agregado,agregados,reg.nadeamanu,id_regra_desconto);
    
      end loop;                               
    else
      for reg in (select hsstpla.nnumetpla,hssfeta.nnumefeta,
                         nvl(ndepemanu,0) ndepemanu,nvl(nagremanu,0) nagremanu,nvl(nvalomanu,0) nvalomanu,
                         nvl(nadedmanu,0) nadedmanu,nvl(nadeamanu,0) nadeamanu,nvl(nadetmanu,0) nadetmanu
                    from hsstpla,hssfeta,hssmanu
                   where hsstpla.nnumeplan = plano
                     and hsstpla.ctipotpla = '1'
                     and hsstpla.cweb_tpla = 'S'
                     and hsstpla.nnumetpla = nvl(tabela,hsstpla.nnumetpla)
                     and ((classificacao = 0) or (hsstpla.nnumectpla = classificacao))                   
                     and hsstpla.nnumetpla = hssfeta.nnumetpla
		  		           and hsstpla.nnumeplan = hssfeta.nnumeplan
                     and (hsstpla.dvigetpla is null or hsstpla.dvigetpla >= sysdate)
                     and hssfeta.nminifeta = inicio
                     and hssfeta.nmaxifeta = fim
                     and hsstpla.nnumetitu is null
                     and (hsstpla.nnumeestab = estabelecimento or hsstpla.nnumeestab is null)
                     and (hsstpla.nnumeuncom = unidade_comercial or hsstpla.nnumeuncom is null)                 
                     and hssfeta.nnumefeta = hssmanu.nnumefeta
                     and total between hssmanu.nqtdimanu and hssmanu.nqtdfmanu
                     and hssmanu.dvigemanu = (select max(manu.dvigemanu) from hssmanu manu
                                               where manu.dvigemanu <= sysdate
                                                 and manu.nnumefeta = hssmanu.nnumefeta)) loop
        
        open cr_regra_desconto(regra_desconto,total);
        fetch cr_regra_desconto into percentual;
        if cr_regra_desconto%notfound then
          percentual := 0;
          id_regra_desconto := null;
        else
          id_regra_desconto := regra_desconto;
        end if;
        close cr_regra_desconto;
        
        if (percentual > 0) then
          valor_titular  := round(reg.nvalomanu - (reg.nvalomanu * (percentual / 100)),2);
          valor_agregado := round(reg.nagremanu - (reg.nagremanu * (percentual / 100)),2);      
        else
          valor_titular  := reg.nvalomanu;
          valor_agregado := reg.nagremanu;
        end if;
          
        insert into hssiprop (nnumepropo,nnumeplan,nnumetpla,nnumefeta,nquaniprop,nvaloiprop,nadesiprop,nagreiprop,nqagriprop,nadeaiprop,nnumerdesc)
                      values (proposta,plano,reg.nnumetpla,reg.nnumefeta,titulares,valor_titular,reg.nadetmanu,valor_agregado,agregados,reg.nadeamanu,id_regra_desconto);
    
      end loop;                               
    end if;   
  end if;
  
  -- faturamento minimo
  delete hssprmin
   where nnumepropo = proposta
     and nnumeplan = plano;
     
  open cr_fat_minimo;
  fetch cr_fat_minimo into faturamento_minimo;
  close  cr_fat_minimo;
  
  if faturamento_minimo > 0 then
    select sum(nvl(nquaniprop,0) * nvl(nvaloiprop,0) +
               nvl(nqagriprop,0) * nvl(nagreiprop,0)) into valor_proposta_plano from hssiprop
     where nnumepropo = proposta
       and nnumeplan = plano;
       
    if faturamento_minimo >= valor_proposta_plano then
      insert into HSSPRMIN
        (nnumepropo, nnumeplan, nvaloprmin)
        values
        (proposta, plano, faturamento_minimo - valor_proposta_plano); 
    end if;
  end if;
end;
/

create or replace procedure atualiza_planos_proposta(proposta          in number,
                                                     total             in number,
                                                     classificacao     in number,
                                                     principal         in number default null) as     
  
percentual        number;
valor_titular     number;
valor_agregado    number;
adesao_titular    number;
adesao_agregado   number;

cursor cr_regra_desconto(regra           in number,
                         numero_usuarios in number) is 
  select nvl(npercurdes,0) 
    from hssrdesc,hssurdes
   where hssrdesc.nnumerdesc = regra
     and hssrdesc.caplirdesc = '4'
     and hssrdesc.nnumerdesc = hssurdes.nnumerdesc
     and numero_usuarios >= ninicurdes
     and numero_usuarios <= nfim_urdes
   order by ninicurdes desc;      
    
cursor cr_valor (feta in number,total in number) is
  select nvl(nvalomanu,0),nvl(nagremanu,0),nvl(nadetmanu,0),nvl(nadeamanu,0)
    from hssmanu
   where nnumefeta = feta
     and total between nqtdimanu and nqtdfmanu
     and dvigemanu = (select max(manu.dvigemanu) from hssmanu manu
                       where manu.nnumefeta = hssmanu.nnumefeta)
     and dvigemanu <= sysdate;
                                                   
begin 
-- desenvolvido somente para tabelas baseada em faixa et�ria e quantidade de benefici�rios por contrato
  
  for reg in (select hssiprop.nnumeiprop,hssiprop.nnumefeta,hssiprop.nnumeplan,hssiprop.nnumerdesc
                from hssiprop,hssplan
               where hssiprop.nnumepropo in (proposta, principal)
                 and hssiprop.nnumeplan = hssplan.nnumeplan
                 and (hssplan.nnumeclpla = classificacao or classificacao is null)) loop
               
    open cr_valor(reg.nnumefeta,total);
    fetch cr_valor into valor_titular,valor_agregado,adesao_titular,adesao_agregado;
    close cr_valor;
    
    open cr_regra_desconto(reg.nnumerdesc,total);
    fetch cr_regra_desconto into percentual;
    if cr_regra_desconto%notfound then
      percentual := 0;
    end if;
    close cr_regra_desconto;
    
    if (percentual > 0) then
      valor_titular  := round(valor_titular - (valor_titular * (percentual / 100)),2);
      valor_agregado := round(valor_agregado - (valor_agregado * (percentual / 100)),2);      
    end if;

    update hssiprop
       set nvaloiprop = valor_titular,
           nagreiprop = valor_agregado,
           nadesiprop = adesao_titular,
           nadeaiprop = adesao_agregado
     where nnumeiprop = reg.nnumeiprop;
    
  end loop;    

  if (principal > 0) then
    update hsspropo set nunifpropo = principal
     where nnumepropo = proposta;
  end if;  
end;
/

create or replace procedure insere_aditivos_proposta(proposta  in number,
                                                     aditivo   in number,
                                                     inicio    in number,
                                                     fim       in number,
                                                     titulares in number,
                                                     agregados in number) as

unidade_comercial number;
estabelecimento   number;                                                     
 

cursor cr_prospect is
  select hssprosp.nnumeestab,hssprosp.nnumeuncom
    from hsspropo,hssprosp
   where hsspropo.nnumepropo = proposta
     and hsspropo.nnumeprosp = hssprosp.nnumeprosp;

 
begin

   open cr_prospect;
   fetch cr_prospect into estabelecimento,unidade_comercial;
   close cr_prospect;
  
  
  for reg in (select hsstbadt.nnumetbadt,hssfeadt.nnumefeadt,
                     nvl(ndepevfadt,0) ndepevfadt,nvl(nagrevfadt,0) nagrevfadt,nvl(ntituvfadt,0) ntituvfadt,
                     nvl(nadedvfadt,0) nadedvfadt,nvl(nadeavfadt,0) nadeavfadt,nvl(nadesvfadt,0) nadesvfadt
                from hsstbadt,hssfeadt,hssvfadt
               where hsstbadt.nnumetxmen = aditivo
                 and (hsstbadt.dvigetbadt is null or hsstbadt.dvigetbadt <= sysdate)                
                 and hsstbadt.nnumetbadt = hssfeadt.nnumetbadt
                 and hssfeadt.nminifeadt = inicio
                 and hssfeadt.nmaxifeadt = fim
                 and nvl(hsstbadt.cweb_tbadt,'S') = 'S'
                 and (hsstbadt.nnumeuncom = unidade_comercial or hsstbadt.nnumeuncom is null)
                 and (hsstbadt.nnumeestab = estabelecimento or hsstbadt.nnumeestab is null)
                 and hssfeadt.nnumefeadt = hssvfadt.nnumefeadt
                 and dvigevfadt = (select max(dvigevfadt) from hssvfadt
                                    where dvigevfadt <= sysdate
                                      and hssvfadt.nnumefeadt = hssfeadt.nnumefeadt)) loop
                                             
    insert into hssaprop (nnumepropo,nnumetxmen,nnumetbadt,nnumefeadt,nquanaprop,nvaloaprop,nadesaprop,nagreaprop,nqagraprop,nadeaaprop)
                  values (proposta,aditivo,reg.nnumetbadt,reg.nnumefeadt,titulares,reg.ntituvfadt,reg.nadesvfadt,reg.nagrevfadt,agregados,reg.nadeavfadt);
  
  end loop;                                  
end;
/

create or replace function valida_pedido_comercial (id_pedido in number) return varchar as

qtde           number;
qtde_info      number;
valido         boolean;
erro           varchar2(2000);
conta_corrente varchar2(20);
agencia        varchar2(20);
correntista    varchar2(60);
nome_titular   varchar2(60);
banco          number;
cidade         varchar2(60);
estado         varchar2(60);

cursor cr_vidas (id_pedido in number,
                 inicio    in number,
                 fim       in number) is
  select count(*) 
    from hsspprop,hssatcad
   where hsspprop.nnumepprop = id_pedido
     and hsspprop.nnumepprop = hssatcad.nnumepprop
     and trunc(months_between(nvl(dvigepprop,ddatapprop),dnascatcad)/12) between inicio and fim 
     and ctipoatcad <> 'F';
           
cursor cr_vidas_informadas (id_pedido in number) is
  select sum(nquaniprop)
    from hssiprop
   where nnumepprop = id_pedido
     and hssiprop.nquaniprop > 0;

cursor cr_vidas_inclusas (id_pedido in number) is
  select count(*) 
    from hssatcad
   where nnumepprop = id_pedido;
     
cursor cr_titular_fin_sem_depe (id_pedido in number) is
  select cnomeatcad 
    from hssatcad titular
   where nnumepprop = id_pedido
     and ctipoatcad = 'F'
     and not exists (select ntituusua from hssatcad dependente
                      where dependente.nnumepprop = id_pedido
                        and dependente.ntituusua = titular.nnumeusua
                        and dependente.nnumeusua <> titular.nnumeusua);
   
cursor cr_dados_pedido (id_pedido in number) is
  select CCC__PPROP, CAGENPPROP, CCORRPPROP, NNUMECDBA, CCIDAPPROP, CESTAPPROP from hsspprop
   where hsspprop.nnumepprop = id_pedido;  
begin
  conta_corrente := null;
  agencia        := null;
  correntista    := null;
  banco          := null;

  open cr_dados_pedido(id_pedido);
  fetch cr_dados_pedido into conta_corrente, agencia, correntista, banco, cidade, estado;
  close cr_dados_pedido; 
  
  erro   := '';
  
  for reg in (select nquaniprop, nminifeta,nmaxifeta
                from hssiprop,hssfeta
               where hssiprop.nnumepprop = id_pedido
                 and hssiprop.nnumefeta = hssfeta.nnumefeta
                 and hssiprop.nquaniprop > 0 
               order by nminifeta) loop
               
    open cr_vidas (id_pedido,reg.nminifeta,reg.nmaxifeta);               
    fetch cr_vidas into qtde;
    close cr_vidas;
    
    if qtde <> reg.nquaniprop then
      erro := erro || chr(13) ||
              'Faixa et�ria: ' || to_char(reg.nminifeta) || ' a ' || to_char(reg.nmaxifeta) || 
              ' , cadastrado: ' || to_char(qtde) || ', necess�rio: ' || to_char(reg.nquaniprop);
    end if;
    
  end loop;

  if (erro = '') then
    open cr_vidas_informadas (id_pedido);
    fetch cr_vidas_informadas into qtde_info;
    close cr_vidas_informadas;

    open cr_vidas_inclusas (id_pedido);
    fetch cr_vidas_inclusas into qtde;
    close cr_vidas_inclusas;  

    if (qtde <> qtde_info) then
      erro := 'A quantidade de benefici�rios inclusos n�o corresponde com o n�mero de vidas da proposta,' ||
              ' cadastrado: ' || to_char(qtde) || ', proposta: ' || to_char(qtde_info);      
    end if;      

  end if;
  
  open cr_titular_fin_sem_depe(id_pedido);
  fetch cr_titular_fin_sem_depe into nome_titular;
  if cr_titular_fin_sem_depe%found then
    erro := 'Titular financeiro '||nome_titular||' sem dependente cadastrado.';
  end if;
  close cr_titular_fin_sem_depe;
  
  if (erro <> '') then
    erro := 'O n�mero de benefici�rios inclusos n�o corresponde com o n�mero de benefici�rios do pedido.'||erro;
  else
    if (conta_corrente = '') or (conta_corrente is null) then
      erro := 'Conta corrente no pedido � de preenchimento obrigat�rio.';
    elsif (agencia = '') or (agencia is null) then
      erro := 'Agencia no pedido � de preenchimento obrigat�rio.';
    elsif (correntista = '') or (correntista is null) then
      erro := 'Correntista no pedido � de preenchimento obrigat�rio.';
    elsif (banco = '') or (banco is null) then
      erro := 'Banco no pedido � de preenchimento obrigat�rio.';
    elsif (cidade = '') or (cidade is null) then
      erro := 'Cidade no pedido � de preenchimento obrigat�rio.';
    elsif (estado = '') or (estado is null) then
      erro := 'Estado no pedido � de preenchimento obrigat�rio.';
    end if;
  end if;
  
  return erro;
   
end;
/

create or replace function retorna_menor_valor_proposta (id_prospect in  number) return number as
menor_valor number;

cursor cr_valores is
  select round(sum(nvl(nquaniprop,0) * nvl(nvaloiprop,0)),2) valor from hsspropo, hssiprop
   where nnumeprosp = id_prospect
     and hsspropo.nnumepropo = hssiprop.nnumepropo
   group by hsspropo.nnumepropo
   order by valor;
   
begin
  open cr_valores;
  fetch cr_valores into menor_valor;
  close cr_valores;
  
  return menor_valor;
end;
/

/* Retorna a proposta Ano+Proposta*/
create or replace function proxima_proposta_franca(data in date) return number as
  proximo number;
cursor cr_proximo is
  select to_number(nvl(max(substr(nnumepropo,5,5)),0))
    from hsspropo
   where substr(nnumepropo,1,4)=to_char(data,'YYYY');
begin
  open cr_proximo;
  fetch cr_proximo into proximo;
  close cr_proximo;
  return to_number(to_char(data,'YYYY'))||acrescenta_zeros(proximo+1,5);
end;
/