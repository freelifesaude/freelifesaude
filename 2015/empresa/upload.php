<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");

  $bd = new Oracle(true);  
  
  $_SESSION['titulo'] = "ENVIO E RECEBIMENTO DE ARQUIVOS";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","upload.htm"); 

  $filename = '';
  
  function listaDiretorio($tpl,$dir,$recursivo) {
    
    $i = 0;
    
    foreach (glob($dir."/*",GLOB_NOCHECK ) as $filename) { 
                   
      if (is_dir($filename)) {
        if ($recursivo == true)
          lista_diretorio($tpl,$filename,$recursivo);          
      } 
      else {       
        if (file_exists($filename)) { 
          if ($i==1)
            $tpl->COR_LINHA = '';
          else
            $tpl->COR_LINHA = '#EEEEEE';
              
          $i = $i * (-1);	  
        
          $data_arquivo = mktime (date('H',filemtime($filename)), date('i',filemtime($filename)), 0, date('m',filemtime($filename)),date('d',filemtime($filename)),date('Y',filemtime($filename)));
          
          $tpl->PROTOCOLO = 0;
          $tpl->DATA_ENVIO = date('d/m/Y H:i',$data_arquivo);
          $tpl->ARQUIVO = "<a href='".$filename."'>".basename($filename)."</a>";
          
          $tamanho = filesize($filename);
          $tamanho = ceil($tamanho / 1024);
          $tpl->TAMANHO = $tamanho." KB";
          $tpl->IMAGEM_BTN = 'bot_desmarcar.gif';            
          $tpl->block("LINHA"); 
        }
      }
    }
  }  
  $sql_setor = new Query($bd);
  $txt = "SELECT CNOMESETOR FROM HSSSETOR
            WHERE NNUMESETOR = :Setor
            GROUP BY CNOMESETOR";
                    
  $sql_setor->addParam(":Setor",$_SESSION['id_locacao']); 
  $sql_setor->executeQuery($txt);
  
  if (isset($_POST['enviar'])) {

    $arquivo      = $_FILES['arquivo'];
    $nome_arquivo = $arquivo['name'];
  	$assunto      = $_POST['assunto'];
    $erro         = '';
    
    if ($arquivo['name'] <> '')
      $erro .= $util->validaExtensaoArquivo($arquivo['name']);
   
    if ($erro == '') {   
      $path = getcwd();             

      $dir = "anexos";
	
      $dir = $util->criaDiretorio($dir);
      $dir = $util->criaDiretorio($data->dataAtual('YYYY'));
      $dir = $util->criaDiretorio($_SESSION['codigo_contrato']);
      
      if ($_SESSION['id_locacao'] > 0)
        $dir = $util->criaDiretorio($sql_setor->result("CNOMESETOR"));
      
      if (isset($arquivo)) {      
        $protocolo = $func->insereProtocolo($bd,$_SESSION['id_contrato'],'C',$nome_arquivo,'U',$assunto);
          
        if ($protocolo['erro'] <> '') {
          $bd->rollback();  
          $tpl->CLASSE = "alert-error";
          $tpl->MSG = $protocolo['erro'];               
          $tpl->block("ERRO");           
        } else {       
          move_uploaded_file($arquivo['tmp_name'],$dir."/".$protocolo['id']."_".$nome_arquivo);

          $msg = "  Protocolo N.o: " . $protocolo['id'] .
                 "        Empresa: " . $_SESSION['codigo_contrato'] . ' - ' . $_SESSION['titular_contrato'] . "<br><br>".		
                 "        Assunto: " . $_POST['assunto'] . "<br><br>".
                 "Nome do arquivo: " . $nome_arquivo . "<br><br>".
                 "           Data: " . date('d/m/Y H:i') . "<br><br>";

          if ((file_exists($dir."/".$protocolo['id']."_".$nome_arquivo))) { 
        if ($_SESSION['apelido_operadora'] == 'clinipam')
            $func->enviaEmail('protocolo@clinipam.com.br',"Envia de arquivo",$msg);         
            $bd->commit();  
            $tpl->CLASSE = "alert-success";
            $tpl->MSG = "Arquivo enviado com sucesso. Protocolo n� ".$protocolo['id'];
            $tpl->block("ERRO");                   
          } else {
            $bd->rollback();  
            $tpl->CLASSE = "alert-error";
            $tpl->MSG = "Erro na transmiss�o. O tamanho do arquivo deve possuir no m�ximo ".ini_get("upload_max_filesize").".";
            $tpl->block("ERRO"); 
          }
        }        
      }

      chdir($path);   
    }
    else {
      $tpl->CLASSE = "alert-error";
      $tpl->MSG    = $erro;
      $tpl->block("ERRO");     
    }
  }  

  $sql = new Query($bd);
  $txt = "SELECT NNUMEPROTW,TO_CHAR(DDATAPROTW,'DD/MM/YYYY HH24:mi') DDATAPROTW,CDESCPROTW,COPERPROTW,CASSUPROTW
            FROM HSSPROTW
           WHERE HSSPROTW.NID__PROTW = :contrato
             AND HSSPROTW.CTIPOPROTW = 'U'
           ORDER BY NNUMEPROTW DESC";   
                 
  $sql->addParam(":contrato",$_SESSION['id_contrato']);            
  $sql->executeQuery($txt);
   
  $i = 1;
  while (!$sql->eof()) {
    if ($i==1)
      $tpl->COR_LINHA = '';
    else
      $tpl->COR_LINHA = '#EEEEEE';
        
    $i = $i * (-1);
      
    if ($_SESSION['id_locacao'] > 0) {      
      $filename = "anexos/".$data->dataAtual('YYYY')."/".$_SESSION['codigo_contrato']."/".$sql_setor->result("CNOMESETOR")."/".$sql->result("NNUMEPROTW")."_".$sql->result("CDESCPROTW");
    
      if (file_exists($filename)) {
        $tpl->PROTOCOLO = $sql->result("NNUMEPROTW");
        $tpl->DATA_ENVIO = $sql->result("DDATAPROTW");
		    $tpl->ASSUNTO = $sql->result("CASSUPROTW");
        $tpl->ARQUIVO = "<a href='".$filename."'>".$sql->result("CDESCPROTW")."</a>";
        
        $tamanho = filesize($filename);
        $tamanho = ceil($tamanho / 1024);
        $tpl->TAMANHO = $tamanho." KB";

        if ($sql->result("COPERPROTW") == 'C')
          $tpl->IMAGEM_BTN = 'bot_marcar.gif';
        else
          $tpl->IMAGEM_BTN = 'bot_desmarcar.gif';    
          
        $tpl->block("LINHA"); 
      }  
    } else {
      	       
      if (is_dir($filename)) {    
        $filename = $filename."/".$sql->result("NNUMEPROTW")."_".$sql->result("CDESCPROTW");      
      } else {
        $filename = "anexos/".$data->dataAtual('YYYY')."/".$_SESSION['codigo_contrato']."/".$sql->result("NNUMEPROTW")."_".$sql->result("CDESCPROTW");
      }       
       
      if (file_exists($filename)) {    
        $tpl->PROTOCOLO = $sql->result("NNUMEPROTW");
        $tpl->DATA_ENVIO = $sql->result("DDATAPROTW");
		    $tpl->ASSUNTO = $sql->result("CASSUPROTW");
        $tpl->ARQUIVO = "<a href='".$filename."'>".$sql->result("CDESCPROTW")."</a>";
          
        $tamanho = filesize($filename);
        $tamanho = ceil($tamanho / 1024);
        $tpl->TAMANHO = $tamanho." KB";

        if ($sql->result("COPERPROTW") == 'C')
          $tpl->IMAGEM_BTN = 'bot_marcar.gif';
        else
          $tpl->IMAGEM_BTN = 'bot_desmarcar.gif';    
          
        $tpl->block("LINHA");        
      }
    }    
    $sql->next();
  }

  /* Alterei para padronizar o diretorios ficar todos na pasta anexos.
  if ($_SESSION['id_locacao'] > 0)
    lista_diretorio($tpl,"download/".$_SESSION['codigo_contrato']."/".$sql_setor->result("CNOMESETOR"),true);      
  else {
    // Caso o acesso seja pela empresa ir� listar os subdiret�rios criados na pasta e que n�o possue protocolo
    foreach (glob("upload/".$_SESSION['codigo_contrato']."/*",GLOB_NOCHECK ) as $filename) { 
      if (is_dir($filename)) {    
        lista_diretorio($tpl,$filename,false);          
      }
    } 

    lista_diretorio($tpl,"download/".$_SESSION['codigo_contrato'],true); 
  }

    //Diret�rio raiz, para todos os contratos
  lista_diretorio($tpl,"download",false);  
  */

  if ($_SESSION['id_locacao'] > 0)
    listaDiretorio($tpl,"anexos/".$data->dataAtual('YYYY')."/".$_SESSION['codigo_contrato']."/".$sql_setor->result("CNOMESETOR"),true);      
  else {
    // Caso o acesso seja pela empresa ir� listar os subdiret�rios criados na pasta e que n�o possue protocolo
    foreach (glob("anexos/".$data->dataAtual('YYYY')."/".$_SESSION['codigo_contrato']."/*",GLOB_NOCHECK ) as $filename) { 
      if (is_dir($filename)) {    
        listaDiretorio($tpl,$filename,false);          
      }
    } 

    listaDiretorio($tpl,"anexos/".$data->dataAtual('YYYY')."/".$_SESSION['codigo_contrato'],true); 
  }

    //Diret�rio raiz, para todos os contratos
  listaDiretorio($tpl,"anexos",false);  
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     
  
?>