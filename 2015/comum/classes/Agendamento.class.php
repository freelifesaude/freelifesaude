<?php

class Agendamento {

  private $bd;
  private $idAgendamento;
  private $procedimentos    = array();  
  private $erros            = array();
    
  public $idGuia;
  public $dataAgenda;
  public $horaAgenda;
  public $idPrestador;
  public $idBeneficiario;
  public $codigoPaciente;
  public $tipoPaciente;
  public $telefone1;
  public $telefone2;
  public $email;
  public $idAmbulatorio;
  public $idNaoBeneficiario;
  public $idEspecialidade; 
  public $natureza; 
  public $urgencia; 
  public $localAgenda;
  public $nomePaciente;
  public $dataMarcacao;
  public $operadorMarcacao;
  public $idPaciente;
  public $idConvenio;
  public $tipoAtendimento;
      
  function __call($func,$arq) {
    if(!method_exists(get_class($this),$func)){
      throw new Exception(" O metodo \"$func\" nao existe");
    }
  }  
    
  public function __construct($bd) {
    $this->bd = $bd;
  }
  
  public function getIdAgendamento() {
    return $this->idAgendamento;
  }  
  
  public function setIdAgendamento($id) {
    $this->idAgendamento = $id;
  }    
    
  public function getIdUsuarioPaciente($idPaciente,$ehPaciente,$idConvenio,$ehRN='N') {
  
    $idUsuario = 0;
    
    $sql_conve = new Query($this->bd);
    $txt_conve = "SELECT NNUMECONVE
                    FROM HSSCONVE
                   WHERE CTIPOCONVE = 'O'";  
    $sql_conve->executeQuery($txt_conve); 
      
    if (($idConvenio == $sql_conve->result("NNUMECONVE")) and ($ehPaciente == 'N') and ($ehRN == 'N')) {
      $idUsuario = $idPaciente;
    }
    else {  
      $sql_paci = new Query($this->bd);
      $txt_paci = "SELECT HSSUSUA.NNUMEUSUA
                     FROM HSSCONPA,HSSUSUA
                    WHERE HSSCONPA.NNUMECONVE = :idConvenio
                      AND HSSCONPA.NNUMEPACIE = :idPaciente
                      AND HSSCONPA.CCARTCONPA = HSSUSUA.CCODIUSUA";                                     
      $sql_paci->addParam(":idConvenio"    ,$idConvenio);
      $sql_paci->addParam(":idPaciente"    ,$idPaciente);
      $erro = $sql_paci->executeQuery($txt_paci);
      
      if ($sql_paci->result("NNUMEUSUA") > 0)
        $idUsuario = $sql_paci->result("NNUMEUSUA");  
    }
          
    return $idUsuario;
  }
  
  public function getIdPaciente($idPaciente,$ehPaciente) {
  
    $idUsuario = 0;
    
    if ($ehPaciente == 'S')
      $idUsuario = $idPaciente;
      
    return $idUsuario;
  }  
  
  public function insert() {
                             
    $sql = new Query($this->bd);
    $txt = "SELECT SEQGERAL.NEXTVAL PROXIMO FROM DUAL";
    $erro = $sql->executeQuery($txt);
    
    $this->idAgendamento = $sql->result("PROXIMO");
    
    if ($this->idAgendamento > 0) {      
      $sql = new Query($this->bd);
      $txt = "INSERT INTO HSSAGEND (NNUMEAGEND,NNUMEPRES,NAMBUPRES, DDATAAGEND,CHORAAGEND, CSITUAGEND,CAGEWAGEND, 
                                    DCRIAAGEND,NGERAUSUA,NNUMEESPEC,CTIPOAGEND,NNUMEUSUA,
                                    CFONEAGEND,CFON1AGEND,CMAILAGEND,CNAGEAGEND,CURGEAGEND,CENCAAGEND,CLOCAAGEND,
                                    CCORTAGEND,NNUMECONVE,CTATEAGEND)
                            VALUES (:idAgendamento,:idPrestador,:idAmbulatorio,TO_DATE(:dataAgenda,'DD/MM/YYYY'),:horaAgenda,'A','S',
                                    SYSDATE,:operador,:idEspecialidade,'N',:idUsuario,
                                    :telefone1,:telefone2,:email,:natureza,:urgencia,:encaixe,:localAgenda,
                                    :nome,:convenio,:tipoAtendimento)";
                                    
      $sql->addParam(":idAgendamento"      ,$this->idAgendamento);
      $sql->addParam(":idAmbulatorio"      ,$this->idAmbulatorio);
      $sql->addParam(":idPrestador"        ,$this->idPrestador);
      $sql->addParam(":dataAgenda"         ,$this->dataAgenda);
      $sql->addParam(":horaAgenda"         ,$this->horaAgenda);
      $sql->addParam(":idEspecialidade"    ,$this->idEspecialidade);
      $sql->addParam(":operador"           ,$_SESSION['id_operador']);
      
      if ($this->idBeneficiario > 0)
        $sql->addParam(":idUsuario"          ,$this->idBeneficiario);
      else
        $sql->addParam(":idUsuario"          ,'');
        
      if ($this->idPaciente > 0) {
        $sql_paci = new Query($this->bd);
        $txt_paci = "SELECT HSSUSUA.NNUMEUSUA
                       FROM HSSCONPA,HSSUSUA
                      WHERE HSSCONPA.NNUMECONVE = :idConvenio
                        AND HSSCONPA.NNUMEPACIE = :idPaciente
                        AND HSSCONPA.CCARTCONPA = HSSUSUA.CCODIUSUA";                                     
        $sql_paci->addParam(":idConvenio"    ,$this->idConvenio);
        $sql_paci->addParam(":idPaciente"    ,$this->idPaciente);
        $erro = $sql_paci->executeQuery($txt_paci);
        
        if ($sql_paci->result("NNUMEUSUA") > 0)
          $sql->addParam(":idUsuario"          ,$sql_paci->result("NNUMEUSUA"));          
      }        
      
      $sql->addParam(":telefone1"          ,$this->telefone1);
      $sql->addParam(":telefone2"          ,$this->telefone2);
      $sql->addParam(":email"              ,$this->email);
      $sql->addParam(":natureza"           ,$this->natureza);
      $sql->addParam(":urgencia"           ,$this->urgencia);
      $sql->addParam(":encaixe"            ,'S');
      $sql->addParam(":localAgenda"        ,$this->localAgenda);
      $sql->addParam(":nome"               ,strToUpper($this->nomePaciente));
      $sql->addParam(":convenio"           ,$this->idConvenio);
      $sql->addParam(":tipoAtendimento"    ,$this->tipoAtendimento);
                         
      $erro = $sql->executeSQL($txt);       
      
      if ($erro <> '')
        $this->adicionaErro($erro);  
      else {
      
        if ($this->idPaciente > 0) {
          $sql_paci = new Query($this->bd);
          $txt_paci = "INSERT INTO HSSAGPAC (NNUMEAGEND,NNUMEPACIE)
                                     VALUES (:idAgendamento,:idPaciente)";
                                       
          $sql_paci->addParam(":idAgendamento" ,$this->idAgendamento);
          $sql_paci->addParam(":idPaciente"    ,$this->idPaciente);
          $erro = $sql_paci->executeSQL($txt_paci);
        }
        
        /** Procedimentos **/
        for ($p = 0;$p < sizeof($this->procedimentos);$p++) {        
          $item = array();
          $item = $this->procedimentos[$p];
          
          $sql_proc = new Query($this->bd);
          $txt_proc = "INSERT INTO HSSPRAGE (NNUMEAGEND,CCODIPMED,NQUANPRAGE)
                                    VALUES (:idAgendamento,:procedimento,:qtde)";
                                       
          $sql_proc->addParam(":idAgendamento" ,$this->idAgendamento);
          $sql_proc->addParam(":procedimento"  ,$item[0]);
          $sql_proc->addParam(":qtde"          ,$item[1]);  
          $erro = $sql_proc->executeSQL($txt_proc);  
        } 
    
      }
    }
  } 

  public function update() {

    if ($this->idAgendamento > 0) {
      
      $sql = new Query($this->bd);
                         
      $txt = "UPDATE HSSAGEND 
                 SET NNUMEUSUA  = :idUsuario,
                     CFONEAGEND = :telefone1,
                     NOPERUSUA  = :operadorMarcacao,
                     NALTEUSUA  = :operador,
                     DMARCAGEND = TO_DATE(:marcacao,'DD/MM/YYYY HH24:MI:SS'),
                     CNAGEAGEND = :natureza,
                     CURGEAGEND = :urgencia,
                     NNUMEESPEC = :idEspecialidade,
                     CMAILAGEND = :email,
                     CFON1AGEND = :telefone2,
                     NAMBUPRES  = :idAmbulatorio,
                     NNUMEPRES  = :idPrestador,
                     CLOCAAGEND = :localAgenda,
                     CCORTAGEND = :nome,
                     NNUMECONVE = :convenio,
                     CTATEAGEND = :tipoAtendimento
               WHERE NNUMEAGEND = :idAgendamento ";
                         
      $sql->addParam(":idAgendamento"      ,$this->idAgendamento);
      
      if ($this->idBeneficiario > 0)
        $sql->addParam(":idUsuario"          ,$this->idBeneficiario);
      else
        $sql->addParam(":idUsuario"          ,'');
      
      $sql->addParam(":telefone1"          ,$this->telefone1);
      $sql->addParam(":telefone2"          ,$this->telefone2);
      $sql->addParam(":email"              ,$this->email);
      $sql->addParam(":marcacao"           ,$this->dataMarcacao);
      $sql->addParam(":natureza"           ,$this->natureza);
      $sql->addParam(":urgencia"           ,$this->urgencia);
      $sql->addParam(":idEspecialidade"    ,$this->idEspecialidade);
      $sql->addParam(":idAmbulatorio"      ,$this->idAmbulatorio);
      $sql->addParam(":idPrestador"        ,$this->idPrestador);      
      $sql->addParam(":operadorMarcacao"   ,$this->operadorMarcacao);
      $sql->addParam(":operador"           ,$_SESSION['id_operador']);
      $sql->addParam(":localAgenda"        ,$this->localAgenda);
      $sql->addParam(":nome"               ,strToUpper($this->nomePaciente));
      $sql->addParam(":convenio"           ,$this->idConvenio);
      $sql->addParam(":tipoAtendimento"    ,$this->tipoAtendimento);

      $erro = $sql->executeSQL($txt);       
      
      if ($erro <> '')
        $this->adicionaErro($erro);  
      else {
      
        if ($this->idPaciente > 0) {
          $sql_paci = new Query($this->bd);
          $txt_paci = "INSERT INTO HSSAGPAC (NNUMEAGEND,NNUMEPACIE)
                                     VALUES (:idAgendamento,:idPaciente)";
                                       
          $sql_paci->addParam(":idAgendamento" ,$this->idAgendamento);
          $sql_paci->addParam(":idPaciente"    ,$this->idPaciente);
          $erro = $sql_paci->executeSQL($txt_paci);
        }
        
        /** Procedimentos **/
        for ($p = 0;$p < sizeof($this->procedimentos);$p++) {        
          $item = array();
          $item = $this->procedimentos[$p];
          
          $sql_proc = new Query($this->bd);
          $txt_proc = "INSERT INTO HSSPRAGE (NNUMEAGEND,CCODIPMED,NQUANPRAGE)
                                    VALUES (:idAgendamento,:procedimento,:qtde)";
                                       
          $sql_proc->addParam(":idAgendamento" ,$this->idAgendamento);
          $sql_proc->addParam(":procedimento"  ,$item[0]);
          $sql_proc->addParam(":qtde"          ,$item[1]);  
          $erro = $sql_proc->executeSQL($txt_proc);  
        } 
    
      }      
    }
    else
      $this->adicionaErro("Agendamento n�o informado");     
  }  

  public function carregaDados() {  
    $sql_agend = new Query($this->bd);
    $txt_agend = "SELECT HSSAGEND.NNUMEGUIA,TO_CHAR(HSSAGEND.DDATAAGEND,'DD/MM/YYYY') DDATAAGEND,HSSAGEND.CHORAAGEND,
                         HSSAGEND.NNUMEPRES,HSSAGEND.NNUMEUSUA,HSSAGEND.CFONEAGEND,HSSAGEND.NAMBUPRES,
                         HSSAGEND.NNUMENUSUA,HSSAGEND.NNUMEESPEC,HSSAGEND.CURGEAGEND,HSSAGEND.CNAGEAGEND,
                         HSSAGPAC.NNUMEPACIE,HSSAGEND.NNUMECONVE,HSSCONVE.CTIPOCONVE,HSSAGEND.CFONEAGEND,
                         HSSAGEND.CFON1AGEND,HSSAGEND.NOPERUSUA,HSSAGEND.DMARCAGEND,HSSAGEND.CMAILAGEND,
                         HSSAGEND.CLOCAAGEND,HSSAGEND.CCORTAGEND,HSSAGEND.CTATEAGEND
                    FROM HSSAGEND,HSSAGPAC,HSSCONVE
                   WHERE HSSAGEND.NNUMEAGEND = :idAgendamento 
                     AND HSSAGEND.NNUMEAGEND = HSSAGPAC.NNUMEAGEND(+)
                     AND HSSAGEND.NNUMECONVE = HSSCONVE.NNUMECONVE(+)";
    $sql_agend->addParam(":idAgendamento",$this->idAgendamento);
    $sql_agend->executeQuery($txt_agend);      
    
    $this->idGuia            = $sql_agend->result("NNUMEGUIA");
    $this->dataAgenda        = $sql_agend->result("DDATAAGEND");
    $this->horaAgenda        = $sql_agend->result("CHORAAGEND");
    $this->idPrestador       = $sql_agend->result("NNUMEPRES");
    $this->idBeneficiario    = $sql_agend->result("NNUMEUSUA");
    $this->telefone          = $sql_agend->result("CFONEAGEND");
    $this->idAmbulatorio     = $sql_agend->result("NAMBUPRES");
    $this->idNaoBeneficiario = $sql_agend->result("NNUMENUSUA");
    $this->idEspecialidade   = $sql_agend->result("NNUMEESPEC");  
    $this->natureza          = $sql_agend->result("CNAGEAGEND");  
    $this->urgencia          = $sql_agend->result("CURGEAGEND");  
    $this->idPaciente        = $sql_agend->result("NNUMEPACIE");  
    $this->idConvenio        = $sql_agend->result("NNUMECONVE");      
    $this->tipoConvenio      = $sql_agend->result("CTIPOCONVE");   
    $this->telefone1         = $sql_agend->result("CFONEAGEND");   
    $this->telefone2         = $sql_agend->result("CFON1AGEND");   
    $this->operadorMarcacao  = $sql_agend->result("NOPERUSUA");   
    $this->dataMarcacao      = $sql_agend->result("DMARCAGEND");   
    $this->email             = $sql_agend->result("CMAILAGEND");   
    $this->localAgenda       = $sql_agend->result("CLOCAAGEND");   
    $this->nomePaciente      = $sql_agend->result("CCORTAGEND");   
    $this->tipoAtendimento   = $sql_agend->result("CTATEAGEND");   
      
    $sql_proc = new Query($this->bd);    
    $txt_proc = "SELECT * FROM HSSPRAGE WHERE NNUMEAGEND = :idAgendamento";
    $sql_proc->addParam(":idAgendamento",$this->idAgendamento);
    $sql_proc->executeQuery($txt_proc);
    
    while (!$sql_proc->eof()) {
      $this->adicionaProcedimento($sql_proc->result("CCODIPMED"),$sql_proc->result("NQUANPRAGE"),'','');
      $sql_proc->next();
    }
  }
  
  public function validarAgendamento() {
    
    $beneficiario = new Beneficiario($this->bd,$this->codigoPaciente);
    
    //$beneficiario->verificarInadimplencia();
    
    if ($beneficiario->getQtdeErrosFatal() > 0) { 
    
      $erros = array();
      $erros = $beneficiario->getArrayFatalErros();       
      for ($b = 0;$b < sizeof($erros);$b++) {      
        $erro = array();
        $erro = $erros[$b];                    
        $this->adicionaErro($erro[1]);
      }       
    }
    else if ($beneficiario->getQtdeErros() > 0) {
    
      $erros = array();
      $erros = $beneficiario->getArrayErros();       
      
      for ($b = 0;$b < sizeof($erros);$b++) {      
        $erro = array();
        $erro = $erros[$b];                    
        $this->adicionaErro($erro[1]);
      } 
    } 
    
    $this->idBeneficiario = $beneficiario->idBeneficiario;

    /** Procedimentos **
    for ($p = 0;$p < sizeof($this->procedimentos);$p++) {        
      $item = array();
      $item = $this->procedimentos[$p];
      
      
      $sql_carencia = new Query($this->bd);
      $txt_carencia = "SELECT ESTAEMCARENCIA(:usuario,:procedimento,TO_DATE(:data,'DD/MM/YYYY')) CARENCIA FROM DUAL";                                   
      $sql_carencia->addParam(":usuario"       ,$this->idBeneficiario);
      $sql_carencia->addParam(":procedimento"  ,$item[0]);
      $sql_carencia->addParam(":data"          ,$this->dataAgenda);  
      $sql_carencia->executeQuery($txt_carencia);  
      
      if ($sql_carencia->result("CARENCIA") == "S")
        $this->adicionaErro($item[0] . " - Procedimento em car�ncia para a data solicitada." );
        
    } 
    
    */
    
  }
  
  public function agendar() {
    //$this->validarAgendamento();
    
    if ($this->qtdeErros() > 0) {
      return false;
    }
    else {
      if ($this->idAgendamento == 0) {
        $sql_agend = new Query($this->bd);
        $txt_agend = "SELECT NNUMEAGEND
                        FROM HSSAGEND
                       WHERE DDATAAGEND = :dataAgenda
                         AND CHORAAGEND = :horaAgenda
                         AND NNUMEPRES  = :idPrestador
                         AND NAMBUPRES  = :idAmbulatorio";
        $sql_agend->addParam(":dataAgenda"         ,$this->dataAgenda);
        $sql_agend->addParam(":horaAgenda"         ,$this->horaAgenda);                          
        $sql_agend->addParam(":idAmbulatorio"      ,$this->idAmbulatorio);
        $sql_agend->addParam(":idPrestador"        ,$this->idPrestador);                          
        $sql_agend->executeQuery($txt_agend);  
        
        $this->idAgendamento = $sql_agend->result("NNUMEAGEND");
      }
       
      if ($this->idAgendamento > 0)
        $this->update();
      else
        $this->insert();
      
      return true;
    }
  }
  
  public function adicionaProcedimento($codigo,$quantidade,$dente,$face) {
    array_push($this->procedimentos,array($codigo,$quantidade,$dente,$face));
  }  
  
  public function adicionaErro($descricao) {
    array_push($this->erros,array("* ".$descricao));
  }  
  
  public function qtdeErros() {
    return sizeOf($this->erros);
  }  
  
  public function getErros() {
  
    $desc_erros = '';
    
    for ($b = 0;$b < sizeof($this->erros);$b++) {      
      $erro = array();
      $erro = $this->erros[$b];
      
      $desc_erros .= $erro[0]."<br>";      
    } 
    
    return $desc_erros;
  }  
  
  public function getProcedimentos() {
    return $this->procedimentos;
  }   
  
  public function avisoAgendamento($tipo) {
  
    //Tipo = A = No momento do agendamento
    //       L = Lembrete 24 horas antes
    //       F = Falta
    $formata = new Formata();
    $func    = new Funcao();
    $data    = new Data();
    
    $sql = new Query($this->bd);
    $txt = "SELECT INITCAP(TRIM(CDESCTLOGR || ' ' || CENDEPRES || ', ' || CNENDPRES)) ENDERECO, 
                   INITCAP(ABREVIAR_NOME(CNOMEPRES,26)) NOME_ABREVIADO, CNOMEPRES, CFONEPRES,
                   INITCAP(CFANTEMPR) FANTASIA,CPESSPRES,CSEXOPRES,CGRUPPRES
              FROM FINPRES,HSSTLOGR,FINEMPR
             WHERE NNUMEPRES = :nume_pres
               AND FINPRES.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)";
    $sql->clear();
    $sql->addParam(":nume_pres",$this->idPrestador);  
    $sql->executeQuery($txt);    

    $retorno = '';
    
    if (($formata->somenteNumeros($this->telefone1) == '16992575646') or
        ($formata->somenteNumeros($this->telefone1) == '4391616033')) {
    
      //*** Enviar somente para o convenio unimed franca e intercambio
    
      //Verifica se o telefone � celular
      if ($func->validaTelefone($this->telefone1,'C') == "S") 
        $telefone = $this->telefone1;
      else if ($func->validaTelefone($this->telefone2,'C') == "S") 
        $telefone = $this->telefone2;
      else
        $telefone = '';
      
      if ($telefone <> '') {      
      
        if (($sql->result("CPESSPRES") == 'F') and 
            (!in_array($sql->result("CGRUPPRES"),array('T','Q','U')))) { 
            
          if ($sql->result("CSEXOPRES") == 'F')
            $tratamento = 'Dra ';
          else
            $tratamento = 'Dra ';
        }
        else
          $tratamento = '';

        //SMS de confirma��o do agendamento
        if ($tipo == "A") {      
          $enviarEm = $this->dataAgenda." ".$this->horaAgenda;
          $textoSMS = "A ".$sql->result("FANTASIA").", confirma seu agendamento para o dia ".
                      $this->dataAgenda." as ".$this->horaAgenda.", ".
                      $tratamento.$sql->result("NOME_ABREVIADO").", Tel:".$formata->formataTelefone($sql->result("CFONEPRES")). " Esperamos voce!";
        }
        //SMS agendado para enviar com 24 horas de antecedencia           
        else if ($tipo == 'L') {        
          $enviarEm = $data->incrementaData($this->dataAgenda,-1,'D')." ".$this->horaAgenda;

          $textoSMS = "Lembramos seu agendamento no dia ".
                      $this->dataAgenda." as ".$this->horaAgenda.", ".
                      $tratamento.$sql->result("NOME_ABREVIADO").", Tel:".$formata->formataTelefone($sql->result("CFONEPRES")).". Se nao puder, ligue e desmarque!";
        }
        //SMS de falta          
        else if ($tipo == 'F') {
          $enviarEm = $data->dataAtual('DD/MM/YYYY HH24:mi:ss');            
          $textoSMS = "Registramos sua falta no agendamento do dia ".$this->dataAgenda." as ".$this->horaAgenda.", ".
                      $tratamento.$sql->result("NOME_ABREVIADO").". Seu atendimento nao sera mais prioritario!";
        }
        
        $textoSMS = substr($textoSMS,0,140);
        
        $idSaida = $func->enviaSMS($telefone,$textoSMS,$enviarEm);
        
        if ($idSaida > 0) { 

          $sql = new Query($this->bd);
          $txt = "INSERT INTO HSSAGSAI(NNUMEAGEND,NNUMESAIDA) VALUES (:idAgendamento,:idSaida)";
          $sql->addParam(":idAgendamento",$this->getIdAgendamento());  
          $sql->addParam(":idSaida",$idSaida);  
          $sql->executeSQL($txt);   
          
          $retorno = '';
        }
        else
          $retorno = $idSaida;
      }
      
      if (($func->validaEmail($this->email) == "S") and ($retorno == '')) {

        //Email de confirma��o do agendamento
        if ($tipo == "A") {    
          $assunto  = "Confirma��o de agendamento";
          $enviarEm = $this->dataAgenda." ".$this->horaAgenda;
          $texto    = "A ".$sql->result("FANTASIA").", confirma seu agendamento para o dia ".
                      $this->dataAgenda." as ".$this->horaAgenda.", ".
                      $tratamento.$sql->result("CNOMEPRES").", Tel:".$formata->formataTelefone($sql->result("CFONEPRES")). " Esperamos voce!";
        }
        //Email agendado para enviar com 24 horas de antecedencia           
        else if ($tipo == 'L') {        
          $assunto  = "Aviso de agendamento";
          $enviarEm = $data->incrementaData($this->dataAgenda,-1,'D')." ".$this->horaAgenda;          
          $texto    = "Lembramos seu agendamento no dia ".
                      $this->dataAgenda." as ".$this->horaAgenda.", ".
                      $tratamento.$sql->result("CNOMEPRES").", Tel:".$formata->formataTelefone($sql->result("CFONEPRES")).". Se n�o puder, ligue e desmarque!";
        }
        //Email de falta     
        else if ($tipo == 'F') {
          $assunto  = "Aviso de falta";
          $enviarEm = $data->dataAtual('DD/MM/YYYY HH24:mi:ss');            
          $texto    = "Registramos sua falta no agendamento do dia ".$this->dataAgenda." as ".$this->horaAgenda.", ".
                      $tratamento.$sql->result("CNOMEPRES").". Seu atendimento n�o ser� mais priorit�rio para esta agenda!";
        }
        
        $texto      = substr($texto,0,140);
        
        $idSaida = $func->enviaMail($this->email,$assunto,$texto,$enviarEm); 
        
        if ($idSaida > 0) { 

          $sql = new Query($this->bd);
          $txt = "INSERT INTO HSSAGSAI(NNUMEAGEND,NNUMESAIDA) VALUES (:idAgendamento,:idSaida)";
          $sql->addParam(":idAgendamento",$this->getIdAgendamento());  
          $sql->addParam(":idSaida",$idSaida);  
          $sql->executeSQL($txt);   
          
          $retorno = '';
        }
        else
          $retorno = $idSaida;
      }     
    }
    
    return $retorno;
  }  
  
}

?>