<?php
  require_once("../comum/autoload.php"); 
  $seg->secureSessionStart(); 
  require_once('../comum/sessao.php'); 
  
  $bd = new Oracle();  
  
  $_SESSION['titulo'] = "ATUALIZAÇÃO CADASTRAL DO OPERADOR";
    
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","alterarOperador.html");
  
  $tpl->VOLTAR = $_SESSION['diretorio'].'/principal.php?idSessao='.$_GET['idSessao'];   

  if (isset($_POST['alterar'])) {
    $login      = $seg->antiInjection(strtoupper($_POST['login']));
    $nome       = $seg->antiInjection(strtoupper($_POST['nome']));
    $cpf        = $seg->antiInjection($_POST['cpf']);
    $nascimento = $seg->antiInjection($_POST['nascimento']);
    $tel1       = $seg->antiInjection($_POST['telefone1']);  
    $tel2       = $seg->antiInjection($_POST['telefone2']);  
 
    if ($data->validaData($bd,$nascimento)) {
      $sql = new Query();

      $txt = "UPDATE SEGUSUA
                 SET CNCOMUSUA = :nome,
                     DNASCUSUA = :nascimento,
                     C_CPFUSUA = :cpf,
                     CFRESUSUA = :telefone1,
                     CFCELUSUA = :telefone2
               WHERE NNUMEUSUA = :id_operador
                 AND CNOMEUSUA = :login ";

      $sql->addParam(":id_operador",$_SESSION['id_operador']);
      $sql->addParam(":nome",$nome);           
      $sql->addParam(":cpf",$formata->formataCPF($cpf));                   
      $sql->addParam(":nascimento",$nascimento);
      $sql->addParam(":telefone1",$tel1);    
      $sql->addParam(":telefone2",$tel2);    
      $sql->addParam(":login",$login);
      $erro = $sql->executeSQL($txt);   
    }
    else
      $erro = "Data de nascimento inválida.";
       
    if ($erro <> '') {
      $tpl->CLASSE = 'alert-error';
      $tpl->MENSAGEM = $erro;
      $tpl->block("MSG");       
    } else {
      $protocolo = $util->insereLogOperacao($_SESSION['id_contratado'],"Atualização do cadastro do operador via web.");    

      $tpl->CLASSE = 'alert-success';
      $tpl->MENSAGEM = "Alteração efetuada com sucesso! Protocolo: ".$protocolo['id'];
      $tpl->block("MSG");         
    }
    
  } 
  else {
    $txt = "SELECT CNCOMUSUA,TO_CHAR(DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,C_CPFUSUA,CFRESUSUA,CFCELUSUA,CNOMEUSUA
              FROM SEGUSUA
             WHERE NNUMEUSUA = :operador ";
    $sql = new Query();
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->executeQuery($txt);
  
    $nome       = $sql->result("CNCOMUSUA");
    $login      = $sql->result("CNOMEUSUA");
    $cpf        = $sql->result("C_CPFUSUA");
    $nascimento = $sql->result("DNASCUSUA");
    
    if ($sql->result("CFRESUSUA") <> '')
      $tel1 = $formata->formataTelefone($sql->result("CFRESUSUA"));    
      
    if ($sql->result("CFCELUSUA") <> '')
      $tel2 = $formata->formataTelefone($sql->result("CFCELUSUA"));    
  }
  
  $txt = "SELECT CNOMEUSUA
            FROM SEGUSUA
           WHERE NNUMEUSUA = :operador ";
  $sql = new Query();
  $sql->addParam(":operador",$_SESSION['id_operador']);
  $sql->executeQuery($txt);  
    
  $tpl->CADASTRO_LOGIN      = $sql->result("CNOMEUSUA");  
  $tpl->CADASTRO_NOME       = $nome;  
  $tpl->CADASTRO_CPF        = $formata->formataCPF($cpf);
  $tpl->CADASTRO_NASCIMENTO = $nascimento;
  $tpl->CADASTRO_TELEFONE   = $tel1;
  $tpl->CADASTRO_CELULAR    = $tel2;
    
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     
  
?>