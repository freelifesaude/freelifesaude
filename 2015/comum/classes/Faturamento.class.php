<?php
  class Faturamento
  {
  
  	function __call($func,$arq) {
		  if(!method_exists(get_class($this),$func)){
			  throw new Exception(" O metodo \"$func\" nao existe");
		  }
	  }

    function faturamentoGuiaSimplificado ($bd,$guia,$ncid1,$ncid2,$ncid3,$ncid4,$tipo_saida,$tipo_atend,$tipo_consulta,                  
                                          $protocolo) {
      $func = new Funcao();
      $sql = new Query($bd);
      $txt = "BEGIN ".
             "  FATURAMENTO_WEB30(:guia,:prestador,:data,:retorno,:lote,:conta,:material,:medicamento,".
             "                  :operador,:inicio,:ncid1,:ncid2,:ncid3,:ncid4,:tipo_saida,:tipo_atend,:tipo_consulta,:tipo_fatu,:dateSaida,:horaFim,'',:localFatu); ".
             "END;";
             
      $sql->addParam(":guia",$guia);
      $sql->addParam(":prestador",$_SESSION['id_contratado']);
      $sql->addParam(":data",null);
      $sql->addParam(":retorno",0,2);
      $sql->addParam(":material",'');
      $sql->addParam(":medicamento",'');
      $sql->addParam(":operador",$_SESSION['id_operador']);
      $sql->addParam(":inicio",'');
      $sql->addParam(":ncid1",$ncid1);
      $sql->addParam(":ncid2",$ncid2);
      $sql->addParam(":ncid3",$ncid3);
      $sql->addParam(":ncid4",$ncid4);
      $sql->addParam(":tipo_saida",$tipo_saida);
      $sql->addParam(":tipo_atend",$tipo_atend);
      $sql->addParam(":tipo_consulta",$tipo_consulta);
      $sql->addParam(":tipo_fatu",'1');  
      $sql->addParam(":dateSaida",'');  
      $sql->addParam(":horaFim",''); 
      $sql->addParam(":localFatu",'7');      
      $sql->addParam(":lote",0,12);
      $sql->addParam(":conta",0,12);
      $erro = $sql->executeSQL($txt);
      
      $retorno['erro']    = '';
      $retorno['retorno'] = $sql->getReturn(":retorno");
      $retorno['lote']    = $sql->getReturn(":lote");
      $retorno['conta']   = $sql->getReturn(":conta");
        
      if ($retorno['conta'] > 0) {   
        $sql->clear();
        $txt = "BEGIN ".
               "  FATURA_SIMPLIFICADO_WEB30(:conta,:guia); ".
               "END;";
        $sql->addParam(":conta",$retorno['conta']);
        $sql->addParam(":guia",$guia);
        $erro = $sql->executeSQL($txt); 

        if ($erro == '') {    
          $sql->clear();
          $txt = "BEGIN ".
                 "  ATUALIZA_HONORARIOS_MEDICOS('S',:conta,0); ".
                 "  FATURAMENTO_CONTA(:conta); ".
                 "  ATUALIZA_VALOR_LOTE30(:lote); ".
                 "END; ";
          $sql->addParam(":conta",$retorno['conta']);
          $sql->addParam(":lote",$retorno['lote']);
          $erro = $sql->executeSQL($txt);
        }
           
        if ($erro == '')       
          $erro .= $func->insereItemProtocolo($bd,$guia,$protocolo,'I'); 

        if ($erro <> '') {
          $retorno['retorno'] = -1;
          $retorno['erro']    = $erro;
        }                
      } else {
        //$retorno['retorno'] = -1;
        $retorno['erro']    = $erro." *";       
      }
      
      return $retorno;      
    }
                              
    function faturamentoGuia ($bd,$guia,$data,$material,$medicamento,$inicio,$fim,
                              $ncid1,$ncid2,$ncid3,$ncid4,$tipo_saida,$tipo_atend,$tipo_consulta,$tipo_fatu,
                              $procedimentos,$taxas,$matmeds,$opmes,$protocolo,$dateSaida = '',$complementares,$localFatu = '') {

      $func = new Funcao();
      $sql = new Query($bd);
      $txt = "BEGIN ".
             "  FATURAMENTO_WEB30(:guia,:prestador,to_date(:data,'DD/MM/YYYY'),:retorno,:lote,:conta,:material,:medicamento,".
             "                  :operador,:inicio,:ncid1,:ncid2,:ncid3,:ncid4,:tipo_saida,:tipo_atend,:tipo_consulta,:tipo_fatu,:dateSaida,:horaFim,'',:localFatu); ".
             "END;";
      $sql->addParam(":guia"         ,$guia);
      $sql->addParam(":prestador"    ,$_SESSION['id_contratado']);
      $sql->addParam(":data"         ,$data);
      $sql->addParam(":retorno"      ,0,2);
      $sql->addParam(":material"     ,$material);
      $sql->addParam(":medicamento"  ,$medicamento);
      $sql->addParam(":operador"     ,$_SESSION['id_operador']);
      $sql->addParam(":inicio"       ,$inicio);
      $sql->addParam(":ncid1"        ,$ncid1);
      $sql->addParam(":ncid2"        ,$ncid2);
      $sql->addParam(":ncid3"        ,$ncid3);
      $sql->addParam(":ncid4"        ,$ncid4);
      $sql->addParam(":tipo_saida"   ,$tipo_saida);
      $sql->addParam(":tipo_atend"   ,$tipo_atend);
      $sql->addParam(":tipo_consulta",$tipo_consulta);
      $sql->addParam(":tipo_fatu"    ,$tipo_fatu);  
      $sql->addParam(":dateSaida"    ,$dateSaida);  
      $sql->addParam(":horaFim"      ,$fim);
      $sql->addParam(":localFatu"    ,$localFatu);      
      $sql->addParam(":lote"         ,0,12);
      $sql->addParam(":conta"        ,0,12);
      $erro = $sql->executeSQL($txt);
      
      $retorno['erro']    = '';
      $retorno['retorno'] = $sql->getReturn(":retorno");
      $retorno['lote']    = $sql->getReturn(":lote");
      $retorno['conta']   = $sql->getReturn(":conta");
        
      if ($retorno['conta'] > 0) {
        $procedimento = '';
        $valor        = 0;
        $erro         = '';
        
        for ($i=0;$i<sizeOf($procedimentos);$i++) {
          $proc = array();
          $proc = $procedimentos[$i];
          
          $sql->clear();
          $txt = "BEGIN ".
                 "  FATURA_PROCEDIMENTO_WEB30(:conta,:guia,:prestador,:procedimento,:qtde,:funcao,:valor,to_date(:data,'DD/MM/YYYY'),:hora_ini,:hora_fim, ".
                 "                          :via,:equipe,:anestesista,:paux,:saux,:taux,:qaux,:inst,:grau); ".
                 "END;";
          $sql->addParam(":conta",$retorno['conta']);
          $sql->addParam(":guia",$guia);
            
          if ($proc['idPrestador'] > 0)
            $id_prestador = $proc['idPrestador'];
          else
            $id_prestador = $_SESSION['id_contratado'];
              
          $sql->addParam(":prestador"   ,$id_prestador);
          $sql->addParam(":procedimento",$proc['codigo']);
          $sql->addParam(":qtde"        ,$proc['qtde']);
          
          if ($tipo_fatu == '2')
            $sql->addParam(":funcao"      ,$proc['grau']);
          else
            $sql->addParam(":funcao"      ,'');
          
          $sql->addParam(":valor"       ,$valor);
          $sql->addParam(":data"        ,$data);
          $sql->addParam(":hora_ini"    ,$inicio);
          $sql->addParam(":hora_fim"    ,$fim);  
          $sql->addParam(":via"         ,$proc['via']);  
          $sql->addParam(":equipe"      ,1);    
          $sql->addParam(":grau"        ,$proc['grau']);

          $idAnestesista    = '';
          $idPrimeiroAux    = '';
          $idSegundoAux     = '';
          $idTerceiroAux    = '';
          $idQuartoAux      = '';
          $idInstrumentador = '';
          
          if (isset($proc['idAnestesista']))
            $idAnestesista = $proc['idAnestesista']; 

          if (isset($proc['idPrimeiroAux']))
            $idPrimeiroAux = $proc['idPrimeiroAux']; 

          if (isset($proc['idSegundoAux']))
            $idSegundoAux = $proc['idSegundoAux']; 

          if (isset($proc['idTerceiroAux']))
            $idTerceiroAux = $proc['idTerceiroAux']; 

          if (isset($proc['idQuartoAux']))
            $idQuartoAux = $proc['idQuartoAux']; 

          if (isset($proc['idInstrumentador']))
            $idInstrumentador = $proc['idInstrumentador']; 
            
          if ($idAnestesista > 0)
            $sql->addParam(":anestesista",$idAnestesista);
          else
            $sql->addParam(":anestesista","");
          
          if ($idPrimeiroAux > 0)
            $sql->addParam(":paux",$idPrimeiroAux);  
          else
            $sql->addParam(":paux","");  

          if ($idSegundoAux > 0)
            $sql->addParam(":saux",$idSegundoAux);  
          else
            $sql->addParam(":saux","");  
            
          if ($idTerceiroAux > 0)
            $sql->addParam(":taux",$idTerceiroAux);  
          else
            $sql->addParam(":taux","");  

          if ($idQuartoAux > 0)
            $sql->addParam(":qaux",$idQuartoAux);  
          else
            $sql->addParam(":qaux","");  
            
          if ($idInstrumentador > 0)
            $sql->addParam(":inst",$idInstrumentador);  
          else
            $sql->addParam(":inst","");  

          $erro = $sql->executeSQL($txt);
        }
                           
        if (sizeOf($procedimentos) == 0) {
          $sql->clear();
          $txt = "BEGIN ".
                 "  FATURA_PROCEDIMENTO_WEB30(:conta,:guia,:prestador,:procedimento,:qtde,:funcao,:valor,to_date(:data,'DD/MM/YYYY'),:hora_ini,:hora_fim); ".
                 "END;";
          $sql->addParam(":conta"       ,$retorno['conta']);
          $sql->addParam(":guia"        ,$guia);
          $sql->addParam(":prestador"   ,$_SESSION['id_contratado']);
          $sql->addParam(":procedimento",'');
          $sql->addParam(":qtde"        ,0);      
          $sql->addParam(":funcao"      ,'');
          $sql->addParam(":valor"       ,$valor);
          $sql->addParam(":data"        ,$data);
          $sql->addParam(":hora_ini"    ,$inicio);
          $sql->addParam(":hora_fim"    ,$fim);           
          $erro = $sql->executeSQL($txt);
        }
        
        if ($erro == '') {    
          for ($i=0;$i<sizeOf($taxas);$i++) {
            $taxa = array();
            $taxa = $taxas[$i];
            
            $sql->clear();
            $txt = "BEGIN FATURA_TAXA_WEB30(:conta,:taxa,:quantidade); END;";
            $sql->addParam(":conta"     ,$retorno['conta']);
            $sql->addParam(":taxa"      ,$taxa['id']);
            $sql->addParam(":quantidade",$taxa['qtde']);
            $erro .= $sql->executeSQL($txt);        
          }            
        }
        
        if ($erro == '') {    
          for ($i=0;$i<sizeOf($matmeds);$i++) {
            $matmed = array();
            $matmed = $matmeds[$i];
            
            $sql->clear();
            $txt = "BEGIN FATURA_MATMED_WEB30(:conta,:produto,:quantidade,'M',:valor,:tabela); END;";
            $sql->addParam(":conta"     ,$retorno['conta']);
            $sql->addParam(":produto"   ,$matmed['id']);
            $sql->addParam(":quantidade",$matmed['qtde']); 

            $valor = str_replace('.',',',$matmed['valor']);

            if ($valor <> '')
              $sql->addParam(":valor"     ,$valor);
            else
              $sql->addParam(":valor"     ,'');
            $sql->addParam(":tabela",$matmed['tabela']); 
              
            $erro .= $sql->executeSQL($txt); 
          }   
        }  

        if ($erro == '') {    
          for ($i=0;$i<sizeOf($opmes);$i++) {
            $opme = array();
            $opme = $opmes[$i];
            
            $sql->clear();
            $txt = "BEGIN FATURA_MATMED_WEB30(:conta,:produto,:quantidade,'O',:valor,:tabela); END;";
            $sql->addParam(":conta"     ,$retorno['conta']);
            $sql->addParam(":produto"   ,$opme['id']);           
            $sql->addParam(":quantidade",$opme['qtde']);             
            
            $valor = str_replace('.',',',$opme['valor']);

            if ($valor <> '')
              $sql->addParam(":valor"     ,$opme['valor']);
            else
              $sql->addParam(":valor"     ,'');
            $sql->addParam(":tabela",$opme['tabela']); 

            $erro .= $sql->executeSQL($txt);        
          }   
        }   

        if ($erro == '') {    
          for ($i=0;$i<sizeOf($complementares);$i++) {
            $complementar = array();
            $complementar = $complementares[$i];
            
            $sql->clear();
            $txt = "INSERT INTO HSSGCON (NNUMECONT,NNUMEGUIA,DFATUGCON) VALUES (:conta,:guia,sysdate)";
            $sql->addParam(":conta"     ,$retorno['conta']);
            $sql->addParam(":guia"      ,$complementar['guia']);
            $erro .= $sql->executeSQL($txt);        
          }            
        }        

        if ($erro == '') {    
          $sql->clear();
          $txt = "BEGIN ".
                 "  ATUALIZA_HONORARIOS_MEDICOS('S',:conta,0); ".
                 "  FATURAMENTO_CONTA(:conta); ".
                 "  ATUALIZA_VALOR_LOTE(:lote); ".
                 "END; ";
          $sql->addParam(":conta",$retorno['conta']);
          $sql->addParam(":lote" ,$retorno['lote']);
          $erro = $sql->executeSQL($txt);
        }
           
        if ($erro == '')       
          $erro .= $func->insereItemProtocolo($bd,$guia,$protocolo,'I'); 

        if ($erro <> '') {
          $retorno['retorno'] = -1;
          $retorno['erro'] = $erro;    
        }
      } else {
       // $retorno['retorno'] = -1;
        $retorno['erro'] = $erro." *";       
      }
      
      return $retorno;
    }
    
    
    
  }    
  
?>