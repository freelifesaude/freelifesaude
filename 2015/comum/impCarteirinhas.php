<?php
  Session_start();

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");
  require_once("../comum/autoload.php");
  $bd = new Oracle();  
  
  $contrato  = $_SESSION['id_contrato'];
  
  if (isset($_POST['op'])) {
    $operacao  = $_POST['op'];
    $protocolo = $_POST['p'];  
  }
  else if (isset($_SESSION['filtrosCarteira'])) {
    $filtros   = array();
    $filtros   = $_SESSION['filtrosCarteira'];
    $operacao  = $filtros['op'];
    $protocolo = $filtros['p'];
  }
  
  class PDF extends FPDF {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(150,5,$_SESSION['nome_operadora']." - CNPJ: ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(150,5,'Protocolo de solicitação de carteirinha',0,1);
      $this->SetFont('Arial','B',9);

      $this->Cell(40,5,'');
      $this->Cell(25,5,"Empresa:",0,0,'R');
      $this->Cell(130,5,$_SESSION['titular_contrato'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(25,5,"Protocolo:",0,0,'R');
      $this->Cell(130,5,$this->getArray("protocolo"),0,1);
      $this->Cell(40,5,'');
      $this->Cell(25,5,"Data:",0,0,'R');
      $this->Cell(130,5,$this->getArray("data"),0,1);
      $this->Ln(6);
      $this->Cell(190,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf = new PDF('P','mm','A4');
  $pdf->AliasNbPages();

  $data = new Data();
  $data = $data->dataAtual();
  $arr = array("protocolo" => $protocolo,
               "data" => $data);
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFont('Arial','',8);

  $sql =  new Query($bd);
  $sql->clear();
  $txt = "SELECT HSSUSUA.NNUMEUSUA, HSSUSUA.CNOMEUSUA, HSSUSUA.CCODIUSUA,TITULAR.CNOMEUSUA NOME_TITULAR,
                 DECODE(HSSUSUA.CTIPOUSUA,'T','Titular','F','Tit Financ','A','Agregado','Dependente') CTIPOUSUA, 
                 CMOTICAWEB,CDESCMTCAR,CBO__CAWEB,CMREJCAWEB,CSITUCAWEB
            FROM HSSCAWEB,HSSUSUA,HSSMTCAR,HSSUSUA TITULAR 
           WHERE HSSCAWEB.CSITUCAWEB = :operacao 
             AND HSSCAWEB.CPROTCAWEB = :protocolo 
             AND HSSCAWEB.NNUMEUSUA = HSSUSUA.NNUMEUSUA 
             AND HSSUSUA.NNUMETITU = :contrato 
             AND HSSCAWEB.NNUMEMTCAR = HSSMTCAR.NNUMEMTCAR(+)
             AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
           ORDER BY 2";
  $sql->addParam(":contrato",$contrato);
  $sql->addParam(":operacao",$operacao);
  $sql->addParam(":protocolo",$protocolo);
  $sql->executeQuery($txt);  

  $total = 0;
  while(!$sql->eof()) {
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'Código:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("CCODIUSUA"),'',1);

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'Beneficiário:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("CNOMEUSUA"),'',1);

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'Categoria:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("CTIPOUSUA"),'',1);

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'Titular:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("NOME_TITULAR"),'',1);

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'Motivo:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("CDESCMTCAR"),'',1);

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'Observação:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("CMOTICAWEB"),'',1);

    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(20,5,'B.O.:','',0,'R');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(60,5,$sql->result("CBO__CAWEB"),'',1);

    $pdf->Cell(190,1,' ','B',1);
    $total += 1;
    $sql->next();
  }

  $pdf->Cell(190,5,'Quantidade de carteirinhas solicitadas: '.$total,'',1);

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  
  $bd->close();
  
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
?>