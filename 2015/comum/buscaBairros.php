<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
      
  if (isset($_POST['cidade'])){
    if ($_POST['local'] == 'rede')
      $padrao = "Todos";
    else
      $padrao = "Selecione o bairro";
        
    if ($_POST['cidade'] <> "") {
      $sql = new Query($bd);
      
      if ($_POST['local'] == 'rede') {
        $txt = "SELECT DISTINCT CBAIRPRES CNOMEBAIRR
                  FROM FINPRES
		             WHERE CCREDPRES IN ('S','O')
                   AND CESTAPRES = :estado
      		         AND CCIDAPRES = :cidade  
                   AND CBAIRPRES IS NOT NULL                   
                 UNION
                SELECT DISTINCT CBAIREPRES CNOMECIDAD 
                  FROM HSSEPRES,FINPRES
                 WHERE HSSEPRES.CSITUEPRES = 'S'
                   AND HSSEPRES.CTIPOEPRES = 'A'
                   AND HSSEPRES.CESTAEPRES = :estado
                   AND HSSEPRES.CCIDAEPRES = :cidade
                   AND HSSEPRES.CBAIREPRES IS NOT NULL                   
                   AND HSSEPRES.NNUMEPRES = FINPRES.NNUMEPRES
                   AND CCREDPRES IN ('S','O')
      		       ORDER BY 1 ";
      } else {     
        $txt = "SELECT NNUMEBAIRR, CNOMEBAIRR FROM HSSCIDAD,HSSBAIRR
                 WHERE HSSCIDAD.CNOMECIDAD = :cidade
                   AND HSSCIDAD.CESTACIDAD = :estado                 
                   AND HSSCIDAD.NNUMECIDAD = HSSBAIRR.NNUMECIDAD
                 ORDER BY CNOMEBAIRR ";
      }
      
      $sql->clear();
      $sql->addParam(":cidade",$_POST['cidade']);
      $sql->addParam(":estado",$_POST['estado']);      
      $sql->executeQuery($txt);

      echo '<option value="">'.$padrao.'</option>';       
        
      while (!$sql->eof()) {
        $opcao = '<option value="'.$sql->result("CNOMEBAIRR").'" ';
        
        if ($_POST['bairro'] == $sql->result("CNOMEBAIRR"))
          $opcao .= ' selected ';
          
        $opcao .= '>'.utf8_encode($sql->result("CNOMEBAIRR")).'</option>';
          
        echo $opcao;
        
        $sql->next();
      }  
    } else {
      echo '<option value="">'.$padrao.'</option>';       
    }
  }

  $bd->close();  
?>