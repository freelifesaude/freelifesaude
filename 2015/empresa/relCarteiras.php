<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  $_SESSION['titulo'] = "PROTOCOLOS DE CARTEIRINHAS";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/relCarteiras.htm");
  
  $tpl->ID_SESSAO = $_GET['idSessao'];
                  
  $sql = new Query($bd);
  
  $txt = "SELECT DISTINCT CPROTCAWEB FROM HSSCAWEB
           WHERE NNUMETITU = :contrato
             AND NVL(NNUMESETOR,-1) = :locacao
             AND CSITUCAWEB = :operacao
           ORDER BY CPROTCAWEB DESC";
           
  /* Pendentes */  
  $sql->clear();       
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->addParam(":locacao",$_SESSION['id_locacao']);  
  $sql->addParam(":operacao",'P');
  $sql->executeQuery($txt);
  
  $tpl->CARTEIRNHAS_TIPO = "Carteirinhas pendentes";
  
  while (!$sql->eof()) {
    $tpl->ID = $sql->result("CPROTCAWEB");
    $tpl->OPERACAO = 'P';
    $tpl->block("LINHA");
    $sql->next();    
  }

  $tpl->block("PROTOCOLOS");
  
  /* Rejeitadas */ 
  $sql->clear();  
      
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->addParam(":locacao",$_SESSION['id_locacao']);  
  $sql->addParam(":operacao",'R');
  $sql->executeQuery($txt);
  
  $tpl->CARTEIRNHAS_TIPO = "Carteirinhas rejeitadas";
  
  while (!$sql->eof()) {
    $tpl->ID = $sql->result("CPROTCAWEB");
    $tpl->OPERACAO = 'R';
    $tpl->block("LINHA");
    $sql->next();    
  }
  
  $tpl->block("PROTOCOLOS");
  
  /* Confirmadas */
  $sql->clear();    
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->addParam(":locacao",$_SESSION['id_locacao']);  
  $sql->addParam(":operacao",'C');
  $sql->executeQuery($txt);
  
  $tpl->CARTEIRNHAS_TIPO = "Carteirinhas confirmadas";
  
  while (!$sql->eof()) {
    $tpl->ID = $sql->result("CPROTCAWEB");
    $tpl->OPERACAO = 'C';
    $tpl->block("LINHA");
    $sql->next();    
  }
  
  $tpl->block("PROTOCOLOS");  

  $tpl->block("MOSTRA_MENU");    
  $bd->close();
  $tpl->show();     

?>