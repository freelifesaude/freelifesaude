<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  $_SESSION['titulo'] = "PROCEDIMENTOS POR CAR�NCIA";
  require_once("../comum/layoutJanela.php");
  $tpl->addFile("JANELA_CONTEUDO","../comum/procCarencias.html");
  $tpl->block("MOSTRA_LOGO");
  
  if (isset($_POST['idc']))
    $id_carencia = $_POST['idc'];
    
  if (isset($_POST['n']))
    $id_usuario = $_POST['n'];
        
  $sql = new Query($bd);
  $sql->clear();    
  $txt = "SELECT CNOMEUSUA,DVENCCRUS 
            FROM HSSUSUA,HSSCRUS
           WHERE HSSUSUA.NNUMEUSUA = :id_usuario
             AND HSSUSUA.NNUMEUSUA = HSSCRUS.NNUMEUSUA
             AND NNUMECARE = :id_carencia "; 
  $sql->addParam(":id_carencia",$id_carencia);
  $sql->addParam(":id_usuario",$id_usuario);  
  $sql->executeQuery($txt);

  $tpl->NOME = $sql->result("CNOMEUSUA");
  $tpl->VENCIMENTO = $sql->result("DVENCCRUS");

  $sql->clear();  
	if ($_SESSION['apelido_operadora'] == 'quallity'){  
		$txt = "SELECT CNOMEPMED,HSSPMED.CCODIPMED 
            FROM HSSPCAR,HSSPMED 
           WHERE HSSPCAR.NNUMECARE = :id_carencia
		     AND HSSPCAR.CCODIPMED = HSSPMED.CCODIPMED
             ORDER BY 2,1"; 
	} 
	else {
    $txt = "SELECT CNOMEPMED,HSSPMED.CCODIPMED 
              FROM HSSPCAR,HSSPMED 
             WHERE HSSPCAR.NNUMECARE = :id_carencia
               AND ((SUBSTR(HSSPCAR.CCODIPMED,5,3) <> '000' AND SUBSTR(HSSPCAR.CCODIPMED,3,2) <> '00' AND HSSPCAR.CCODIPMED = HSSPMED.CCODIPMED) OR
                    (SUBSTR(HSSPCAR.CCODIPMED,3,5) = '00000' AND SUBSTR(GRUPOPROCEDIMENTO(HSSPCAR.CCODIPMED),1,2) = SUBSTR(HSSPMED.CCODIPMED,1,2)) OR
                    (SUBSTR(HSSPCAR.CCODIPMED,5,3) = '000' AND SUBSTR(HSSPCAR.CCODIPMED,3,2) <> '00' AND SUBSTR(GRUPOPROCEDIMENTO(HSSPCAR.CCODIPMED),1,4) = SUBSTR(HSSPMED.CCODIPMED,1,4)))
             ORDER BY 2,1"; 
  }
  $sql->addParam(":id_carencia",$id_carencia);
  $sql->executeQuery($txt);
       
  if ($sql->count() > 0) { 
    while (!$sql->eof()) {
      if (substr($sql->result("CNOMEPMED"),4,3) <> '000') {    
        $tpl->PROCEDIMENTO_NOME = $sql->result("CNOMEPMED");
        $tpl->PROCEDIMENTO_CODIGO = $sql->result("CCODIPMED");
        $tpl->block("LINHA");        
      }
      $sql->next();     
    }
    $tpl->block("PROCEDIMENTOS");              
    $tpl->block("RESULTADO");   
    
  } else {
    $tpl->MSG = "** N�o existe procedimento para essa car�ncia **";
    $tpl->block("ERRO");
  }      
 
  $bd->close();
  $tpl->show();     

?>