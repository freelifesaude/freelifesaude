<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  $bd = new Oracle();
  
  if (isset($_POST['estado'])){  
    if ((isset($_POST['padrao'])) and ($_POST['padrao'] <> ''))
      $padrao = $_POST['padrao'];
    else if ($_POST['local'] == 'rede')
      $padrao = "Todas";
    else
      $padrao = "Selecione a cidade";
        
    if ($_POST['estado'] <> "") {    
      $sql = new Query($bd);
      
      if ($_POST['local'] == 'rede') {
        $txt = "SELECT DISTINCT CCIDAPRES CNOMECIDAD
                  FROM FINPRES
		             WHERE CCREDPRES IN ('S','O')
                       AND NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES
                                           FROM HSSESPRE,HSSAAPRE
                                          WHERE HSSESPRE.NNUMEPRES = FINPRES.NNUMEPRES    
                                            AND HSSESPRE.CPUBLESPRE = 'S'                         
                                            AND HSSESPRE.NNUMEPRES = HSSAAPRE.NNUMEPRES(+)
                                            AND HSSESPRE.NNUMEESPEC = HSSAAPRE.NNUMEESPEC(+)) 
      		         AND CESTAPRES = :estado
                   AND CCIDAPRES IS NOT NULL
                 UNION
                SELECT DISTINCT CCIDAEPRES CNOMECIDAD 
                  FROM HSSEPRES,FINPRES
                 WHERE HSSEPRES.CSITUEPRES = 'S'
                   AND HSSEPRES.CTIPOEPRES = 'A'
                   AND HSSEPRES.CESTAEPRES = :estado
                   AND HSSEPRES.CESTAEPRES IS NOT NULL
                   AND HSSEPRES.NNUMEPRES = FINPRES.NNUMEPRES
                   AND CCREDPRES IN ('S','O')
      		       ORDER BY 1 ";
      } else if ($_POST['local'] == 'comercial') {
        $txt = "SELECT NNUMECIDAD, CNOMECIDAD 
                  FROM HSSCIDAD 
                 WHERE CESTACIDAD = :estado
                  AND 0 = (SELECT COUNT(*) FROM HSSCICOM WHERE NNUMEUNCOM = :unidade)
                UNION 
                SELECT HSSCIDAD.NNUMECIDAD, CNOMECIDAD 
                  FROM HSSCIDAD, HSSCICOM 
                 WHERE CESTACIDAD = :estado
                  AND HSSCICOM.NNUMECIDAD = HSSCIDAD.NNUMECIDAD
                  AND NNUMEUNCOM = :unidade
                 ORDER BY 2";
        $sql->addParam(":unidade",$_POST['unidade']);
      } else {     
        $txt = "SELECT NNUMECIDAD, CNOMECIDAD FROM HSSCIDAD WHERE CESTACIDAD = :estado ORDER BY 2";        
      }
      
      $sql->addParam(":estado",$_POST['estado']);
      $sql->executeQuery($txt);
      
      echo '<option value="">'.$padrao.'</option>';       
        
      while (!$sql->eof()) {
        $opcao = '<option value="'.$sql->result("CNOMECIDAD").'" ';
        
        if ($_POST['cidade'] == $sql->result("CNOMECIDAD"))
          $opcao .= ' selected ';
          
        $opcao .= '>'.$sql->result("CNOMECIDAD").'</option>';
          
        echo utf8_encode($opcao);
        
        $sql->next();
      }        
    } else {
      echo '<option value="">'.$padrao.'</option>';       
    }
  }
  
  $bd->close();  
  
?>