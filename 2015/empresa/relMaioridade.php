<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();  
  
  $_SESSION['titulo'] = "RELATÓRIO DE MAIORIDADE";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","relMaioridade.htm");
    
  $txt = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'MM')) MES FROM DUAL";
  $sql = new Query($bd);
  $sql->executeQuery($txt);
  
  if (isset($_POST['listar']))
    $mes = $_POST['mes'];
  else
    $mes = $sql->result("MES");

  $tpl->MES = 'Todos';
  $tpl->block("ITEM_MES");

  for ($m=0;$m<12;$m++) {
    $tpl->MES = $formata->acrescentaZeros($m+1,2);
    
    if ($mes == $m+1)
      $tpl->SELECIONADO = "selected";
    else
      $tpl->SELECIONADO = '';
      
    $tpl->block("ITEM_MES"); 
  }

  if (isset($_POST['listar'])) {

    $locacao = $_SESSION['id_locacao'];

    if ($locacao > 0)
      $filtro_locacao = "   AND TITULAR.NNUMESETOR = :locacao";
    else
      $filtro_locacao = "";
	
	if($_POST['mes'] == 'Todos')
	  $filtro_mes = "";
	else
	  $filtro_mes = "   AND ((TO_CHAR(HSSUSUA.DNASCUSUA,'MM') = :mes AND :mes <> '00') OR (:mes = '00'))";

    $txt  = "SELECT HSSUSUA.CCODIUSUA, HSSUSUA.CNOMEUSUA, TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA, IDADE(HSSUSUA.DNASCUSUA,SYSDATE) IDADE,".
            "       HSSUSUA.CTIPOUSUA, CDESCPLAN, TITULAR.CNOMEUSUA TITULAR".
            "  FROM HSSUSUA,HSSUSUA TITULAR,HSSTITU,HSSPLAN".
            " WHERE HSSUSUA.NNUMETITU = :contrato".
            $filtro_locacao.
            "   AND HSSUSUA.CSITUUSUA = 'A'".
            $filtro_mes.
            "   AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA".
            "   AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU".
            "   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN".
            "   AND NVL(DLIMIPLAN,HSSUSUA.DINCLUSUA) <= HSSUSUA.DINCLUSUA".
            "   AND (".
            "        (HSSUSUA.CTIPOUSUA = 'D' AND HSSUSUA.CSEXOUSUA = 'M' AND HSSUSUA.CGRAUUSUA <> 'E' AND HSSUSUA.CGRAUUSUA <> 'J' AND IDADE(HSSUSUA.DNASCUSUA,LAST_DAY(SYSDATE)) >= NVL(NLIMMTITU,NVL(NLDEMPLAN,999))) OR".
            "        (HSSUSUA.CTIPOUSUA = 'D' AND HSSUSUA.CSEXOUSUA = 'F' AND HSSUSUA.CGRAUUSUA <> 'E' AND HSSUSUA.CGRAUUSUA <> 'J' AND IDADE(HSSUSUA.DNASCUSUA,LAST_DAY(SYSDATE)) >= NVL(NLIMFTITU,NVL(NLDEFPLAN,999))) OR".
            "        (HSSUSUA.CTIPOUSUA = 'U' AND HSSUSUA.CSEXOUSUA = 'M' AND IDADE(HSSUSUA.DNASCUSUA,LAST_DAY(SYSDATE)) >= NVL(NLIMUTITU,NVL(NLDEUPLAN,999))) OR".
            "        (HSSUSUA.CTIPOUSUA = 'U' AND HSSUSUA.CSEXOUSUA = 'F' AND IDADE(HSSUSUA.DNASCUSUA,LAST_DAY(SYSDATE)) >= NVL(NLIUFTITU,NVL(NLDUFPLAN,999))) OR".
            "        (HSSUSUA.CTIPOUSUA = 'A' AND IDADE(HSSUSUA.DNASCUSUA,LAST_DAY(SYSDATE)) >= NVL(NLIMATITU,NVL(NLAGRPLAN,999)))".
            "        )".
            " ORDER BY 2";           
            
    $sql = new Query($bd);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    
    if ($locacao > 0)
      $sql->addParam(":locacao",$locacao);
    
	if($_POST['mes'] <> 'Todos')
      $sql->addParam(":mes",$_POST['mes']);
    
    $sql->executeQuery($txt);
       
  
    while (!$sql->eof()) {    
      $tpl->CODIGO = $sql->result("CCODIUSUA");
      $tpl->NOME = $sql->result("CNOMEUSUA");
      $tpl->NASCIMENTO = $sql->result("DNASCUSUA");
      $tpl->IDADE = $sql->result("IDADE");
      $tpl->CATEGORIA = $func->categoriaUsuario($sql->result("CTIPOUSUA"));
      $tpl->PLANO = $sql->result("CDESCPLAN");
      $tpl->TITULAR = $sql->result("TITULAR");
      $tpl->block("LINHA");
      $sql->next();
    }    
  }
  
  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     
  
?>

  