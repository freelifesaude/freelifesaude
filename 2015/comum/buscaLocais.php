<?php 
  session_start();
  
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();
  
  if (isset($_POST['executante'])) {
  
    if ($_POST['executante'] <> "") {
      $sql = new Query($bd);      
      $txt = "SELECT NNUMEPRES,DECODE(CPESSPRES,'F','Consultório próprio',CNOMEPRES) CNOMEPRES
                FROM FINPRES
               WHERE NNUMEPRES = :prestador 
                 AND (NVL(CCREDPRES,'S') IN ('S','O','V') OR (CCREDPRES IN ('N','M') AND DCANCPRES >= TRUNC(SYSDATE)))
               UNION ALL
              SELECT FINPRES.NNUMEPRES,CNOMEPRES
                FROM HSSCCLIN,FINPRES
               WHERE HSSCCLIN.NNUMEPRES = :prestador
                 AND HSSCCLIN.NHOSPPRES = FINPRES.NNUMEPRES
                 AND (NVL(FINPRES.CCREDPRES,'S') IN ('S','O','V') OR (FINPRES.CCREDPRES IN ('N','M') AND FINPRES.DCANCPRES >= TRUNC(SYSDATE)))";
      $sql->addParam(":prestador",$_POST['executante']);
      $sql->executeQuery($txt);
        
      while (!$sql->eof()) {
        $opcao = '<option value="'.$sql->result("NNUMEPRES").'" ';
                  
        $opcao .= '>'.$sql->result("CNOMEPRES").'</option>';
          
        echo $opcao;
        
        $sql->next();
      }  
    }
  } 
 
  $bd->close(); 
?>