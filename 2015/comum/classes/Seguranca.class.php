<?php
  class Seguranca {
            
    public function geraID($idOperador) { 
      $sql = new Query();
      $txt = "BEGIN ".
             "  ALTERA_APPLICATION_INFO(:operador); ".
             "END; ";
      $sql->addParam(":operador",$idOperador);
      $sql->executeSQL($txt); 
      
      return md5(date('dmYHis').$idOperador.$_SESSION['sistema']);
    }   
    
    function registraLogin ($operador) {
      $sql = new Query();
      $txt = "INSERT INTO SEGTENT (DDATATENT,CIP__TENT,CLOGITENT) VALUES (SYSDATE,:ip,:login)";
      $sql->addParam(":ip",getenv("REMOTE_ADDR"));
      $sql->addParam(":login",$operador);
      $sql->executeSQL($txt);
    }

    function bloqueiaLogin($operador) {

      //Apaga todos os registros com mais de 1 hora
      $sql = new Query();
      $txt = "DELETE FROM SEGTENT WHERE DDATATENT < (SYSDATE - (60/1440)) ";
      $sql->executeSQL($txt);

      //Verifica se houve mais de 10 tentativas nos ultimos 30 segundos
      $sql = new Query();
      $txt = "SELECT COUNT(*) TENTATIVAS FROM SEGTENT
               WHERE CIP__TENT = :ip
                 AND CLOGITENT = :operador
                 AND DDATATENT > SYSDATE -(1/1440) ";
      $sql->addParam(":operador",$operador);
      $sql->addParam(":ip",getenv("REMOTE_ADDR"));
      $sql->executeQuery($txt);

      return $sql->result("TENTATIVAS") + 1;
    }
    
    function permissaoOutros($bd,$texto,$administrador_pode_passar=true) {
     
      $txt = "SELECT 1 A FROM DUAL WHERE 0 = (SELECT COUNT(*) FROM SEGUSUA)";
     
      if ($administrador_pode_passar) {
        $txt .= " UNION ALL 
                 SELECT 1 A FROM SEGUSUA
                  WHERE NNUMEUSUA = :Nume_Usua
                    AND CADMIUSUA = 'S'
                    AND CSTATUSUA = 'A'";
      }
     
      $txt .= " UNION ALL 
               SELECT 1 A FROM SEGDIRE, SEGFORM 
                WHERE SEGDIRE.NNUMEFORM = SEGFORM.NNUMEFORM
                  AND NNUMEUSUA = :Nume_Usua
                  AND CNFSAFORM = :Nome_Form
                  AND CTIPOFORM = 'O'
                UNION ALL 
               SELECT 1 A FROM SEGUSUA, SEGDIRE, SEGFORM
                WHERE SEGUSUA.NNUMEUSUA = :Nume_Usua
                  AND CSTATUSUA = 'A'
                  AND SEGUSUA.NNUMEPERF = SEGDIRE.NNUMEPERF
                  AND SEGDIRE.NNUMEFORM = SEGFORM.NNUMEFORM
                  AND CNFSAFORM = :Nome_Form
                  AND CTIPOFORM = 'O'";

      $sql = new Query($bd);
      $sql->addParam(":Nume_Usua",$_SESSION['id_operador']);
      $sql->addParam(":Nome_Form",$texto);
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0)
        return true;
      else
        return false;
    }    
    
    function alteraOperador ($bd,$operador) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "BEGIN ".
             "  ALTERA_APPLICATION_INFO(:operador); ".
             "END; ";
      $sql->addParam(":operador",$operador);
      $sql->executeSQL($txt);  
    }
    
    function retornaNomeOperador($bd,$id) {
      $sql = new Query($bd);
      $txt = "SELECT CNOMEUSUA FROM SEGUSUA WHERE NNUMEUSUA = :id ";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CNOMEUSUA");
    }    
    
    function idPerfilOperador($bd) {
      $sql = new Query($bd);
      $txt = "SELECT NNUMEPERF FROM SEGUSUA WHERE NNUMEUSUA = :id ";
      $sql->addParam(":id",$_SESSION['id_operador']);
      $sql->executeQuery($txt);
      return $sql->result("NNUMEPERF");
    }        
    
    function registraLog($modulo) {
      $sql = new Query();
      $txt = "BEGIN ".
             "  LOG_WEB(:contrato, :operador, :modulo, :ip_computador); ".
             "END;";
      $sql->addParam(":contrato",$_SESSION['id_contrato']);		 
      $sql->addParam(":operador",$_SESSION['id_operador']);
      $sql->addParam(":modulo",$modulo);
      $sql->addParam(":ip_computador",getenv("REMOTE_ADDR")) ;
      $erro = $sql->executeSQL($txt);     
    }
    
		public function antiInjection($sql,$html = true){    
      // remove palavras que contenham sintaxe sql
      $sql = preg_replace(strtolower("/(from|select|insert|delete|where|drop table|show tables|#|\|--|\\\\)/"),"",$sql);
      
      if (!is_array($sql)) {
        $sql = trim($sql);//limpa espa�os vazio
        $sql = strip_tags($sql);//tira tags html e php
        $sql = addslashes($sql);//Adiciona barras invertidas a uma string
              
	     // if($html == true)
         // $sql = htmlentities($sql,ENT_NOQUOTES,'ISO-8859-1',false);
      }
        
      return $sql;
	  }
    
    function secureSessionStart() {
      ini_set('display_errors','off'); 
      ini_set("error_reporting","E_ALL & ~E_NOTICE");  
    /*
      if (substr(phpversion(),0,3) == '5.4') {
        if(session_status() != 2)
          session_start();
      }
      else
        session_start();
      *//*
      $session_name = 'solus_session_id'; // Define um nome padr�o de sess�o
      $secure       = false; // Defina como true (verdadeiro) caso esteja utilizando https.
      $httponly     = true;  // Isto impede que o javascript seja capaz de acessar a id de sess�o. 

      ini_set('session.use_only_cookies', 1); // For�a as sess�es a apenas utilizarem cookies. 
      $cookieParams = session_get_cookie_params(); // Recebe os par�metros atuais dos cookies.
      session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
      session_name($session_name); // Define o nome da sess�o como sendo o acima definido.
      */
      if (substr(phpversion(),0,3) == '5.4') {
        if(session_status() != 2)
          session_start();
      }
      else
        session_start();
        
     // session_regenerate_id(true); // regenerada a sess�o, deleta a outra.
      /**/
    } 

    function geraSenha() {
      $retorno    = '';
      $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%*-';

      $len = strlen($caracteres);
  
      for ($n = 1; $n <= 6; $n++) {
    
         // Criamos um n�mero aleat�rio de 1 at� $len para pegar um dos caracteres
        $rand = mt_rand(1, $len);
        // Concatenamos um dos caracteres na vari�vel $retorno
        $retorno .= $caracteres[$rand-1];
      }
      
      return $retorno;
    }
    
    function valida_ldap($server, $operador, $senha) {
      
      if (!($connect = @ldap_connect('ldap://'.$server))) 
        return False;
      
      if (!($bind = @ldap_bind($connect, $operador, $senha)))
        return False;
      else
        return True;      
    
    }
    
  }
?>