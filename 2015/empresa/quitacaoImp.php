<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");

  $formata = new Formata();
  $data    = new Data();  
  $bd      = new Oracle(); 
  
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,8,30,22); 

      $this->logoANS($_SESSION['registro_ans'],170,20);
      //$this->Image('../comum/img/ans.png',170,20,22,6); 
      
      $this->Ln(25);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-20);
      $this->SetFont('Arial','',10);
      
      $this->Cell(190,4,$_SESSION['nome_operadora'],0,1,'R');
      $this->Cell(190,4,$_SESSION['endereco_operadora']." ".$_SESSION['cidade_operadora']." - ".$_SESSION['uf_operadora'],0,1,'R');
      $this->Cell(190,4,"CNPJ: ".$_SESSION['cnpj_operadora'],0,1,'R');      	      
    }
  }

  $filtros = array();
  $filtros = $_SESSION['filtrosQuitacao'];
  
  $ano      = $filtros['ano'];
  $contrato = $filtros['contrato'];
  $usuario  = $filtros['usuario'];
 
  $pdf=new PDF();
  $pdf->AliasNbPages();
  //ImagemANS($_SESSION['registro_ans'],'../comum/img/ans.png');  
            
  $sql_titu = new Query($bd);  
  $txt_titu = "SELECT CRAZAEMPR TITULAR, 
                      C_CGCEMPR CNPJ,CENDETITU,
                      CBAIRTITU, 
                      CCIDATITU, 
                      CESTATITU,CCODITITU                      
                 FROM HSSTITU,HSSEMPR
                WHERE HSSTITU.NNUMETITU = :contrato 
                  AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+) ";

  $sql_titu->addParam(":contrato",$contrato);
  $erro = $sql_titu->executeQuery($txt_titu);
  
  $titular         = $sql_titu->result("TITULAR");
  $codigo_contrato = $sql_titu->result("CCODITITU");
  
  if ($erro <> '') {
    echo $erro;
    break;
  }

  $pdf->Open();
  $pdf->AddPage();
  
  $pdf->SetFont('Arial','B',10);
  $pdf->Cell(0,4,$_SESSION['cidade_operadora'].", ".$data->dataExtenso($data->dataAtual(),0),0,1);    
  $pdf->Ln(10);

  $pdf->Cell(5,4,'',0,0);  
  
  $pdf->Cell(185,4,'A/C',0,1); 
  $pdf->Cell(5,4,'',0,0);
  $pdf->Cell(185,4,$sql_titu->result("TITULAR"),0,1);

  $pdf->Cell(5,4,'',0,0);
  
  $pdf->Cell(185,4,'CNPJ: '.$formata->formataCNPJ($sql_titu->result("CNPJ")),0,1);
  
  $pdf->Ln(20);  
  $pdf->Cell(0,4,'DECLARA��O ANUAL DE QUITA��O DE D�BITOS',0,1,'C');
  $pdf->Ln(20);  

  $texto = "    Em atendimento � Lei n�. 12.007/2009, declaramos que as faturas relativas ".
           "ao plano assistencial � sa�de do contrato ".$codigo_contrato." emitidas em nome de ".$titular.
           ", referentes ao exerc�cio de ".$ano.", est�o devidamente quitadas.";

  $pdf->MultiCell(0,4,$texto,0,'J');  
  $pdf->Ln(8);  

  $texto = "    A referida declara��o n�o engloba outros exerc�cios ou outros contratos que porventura existam e substitui, ".
           "para a comprova��o do cumprimento das obriga��es do associado, ".
           "os documentos de quita��es das faturas vencidas em ".$ano.".";

  $pdf->MultiCell(0,4,$texto,0,'J');  
  $pdf->Ln(20);  
  
  $pdf->Cell(45,4,'',0,0);  
  $pdf->Cell(25,4,'Vencimento','TB',0,'C');
  $pdf->Cell(25,4,'Documento','TB',0,'R');
  $pdf->Cell(25,4,'Pagamento','TB',0,'C');
  $pdf->Cell(25,4,'Valor pago (R$)','TB',1,'R');
  $pdf->Cell(45,4,'',0,0);  
  $pdf->Cell(100,1,'',0,1); 
  
  $sql = new Query($bd);  
  $txt = "SELECT DVENCPAGA,DPAGAPAGA,NVENCPAGA,NDOCUPAGA,NPAGOPAGA,NNUMEPAGA 
            FROM HSSPAGA,HSSLOPG
           WHERE NNUMETITU = :Contrato 
             AND NNUMEUSUA IS NULL
             AND TO_CHAR(DVENCPAGA,'YYYY') = :ano
             AND CPAGOPAGA = 'S'
             AND DCANCPAGA IS NULL
             AND DPARCPAGA IS NULL
             AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG
             AND HSSLOPG.CQUITLOPG = 'S'
           ORDER BY 1";
             
  $sql->addParam(":contrato",$contrato);
  $sql->addParam(":ano",$ano);             
  $sql->executeQuery($txt);
  
  $pdf->SetFont('Arial','B',8);  
  while (!$sql->eof()) {  
    $pdf->Cell(45,4,'',0,0);  
    $pdf->Cell(25,4,$sql->result("DVENCPAGA"),0,0,'C');
    $pdf->Cell(25,4,$sql->result("NDOCUPAGA"),0,0,'R');
    $pdf->Cell(25,4,$sql->result("DPAGAPAGA"),0,0,'C');
    $pdf->Cell(25,4,$formata->formataNumero($sql->result("NPAGOPAGA")),0,1,'R');  
    $sql->next();
  }
  
  $pdf->Cell(45,4,'',0,0);  
  $pdf->Cell(100,1,'',0,1);   
  $pdf->Cell(45,4,'',0,0);  
  $pdf->Cell(100,4,'','T',0);
   
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');

  $bd->close();
  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
?>
