<?php
  class Funcao
  {
  
    function __call($func,$arq) {
      if(!method_exists(get_class($this),$func)){
        throw new Exception(" O metodo \"$func\" nao existe");
      }
    }
  
    function validaPacote($bd,$pacote,$prestador,$local,$critica_procedimentos,$usuario=0,$especialidade=0,$urgencia='',$natureza='',
                           $pagina=0,$guia=0,$confirmacao='N',$solicitante=0,$id_pacote=0) {

    /* A variavel critica_procedimentos existe porque a funcao VALIDA_PACOTE30 pode ser chamada somente para verificar se o pacote existe, 
       ai ela contem N e pode tambem ser chamada para validar o conteudo do pacote, a� a critica_procedimentos deve conter S */
      
      $sql = new Query($bd);
      $txt = "SELECT VALIDA_PACOTE30(:pacote,:prestador,:local,:critica_procedimentos,:usuario,:especialidade,:urgencia,:natureza,:pagina,:guia,:confirmacao,:solicitante,:id_pacote) VALIDACAO FROM DUAL";
      $sql->clear();
      $sql->addParam(":pacote",$pacote);
      $sql->addParam(":prestador",$prestador);
      $sql->addParam(":local",$local);
      $sql->addParam(":critica_procedimentos",$critica_procedimentos);
      $sql->addParam(":usuario",$usuario);
      $sql->addParam(":especialidade",$especialidade);
      $sql->addParam(":urgencia",$urgencia);
      $sql->addParam(":natureza",$natureza);
      $sql->addParam(":pagina",$pagina);
      $sql->addParam(":guia",$guia);
      $sql->addParam(":confirmacao",$confirmacao);
      $sql->addParam(":solicitante",$solicitante);
      $sql->addParam(":id_pacote",$id_pacote); 
      $sql->executeQuery($txt);
          
      return $sql->result("VALIDACAO");
    }  

    function validaTaxa($bd,$codigo,$prestador = '',$data = '',$usuario = '') {

      if ($prestador == '')
        $prestador = $_SESSION['id_contratado'];
        
      if ($data == '')
        $data = date('d/m/Y');
          
      $sql = new Query($bd);                     
      $txt = "SELECT VALIDA_TAXA(:codigo,:prestador,:data,:usuario) RESULTADO FROM DUAL";
      $sql->clear();
      $sql->addParam(":codigo",$codigo);
      $sql->addParam(":prestador",$prestador);
      $sql->addParam(":data",$data);
      $sql->addParam(":usuario",$usuario);  
      $sql->executeQuery($txt);
      
      if ($sql->result("RESULTADO") > 0) 
        return $sql->result("RESULTADO");
      else
        return 0;
    } 
    
    function validaMatmed($bd,$codigo_produto,$prestador,$data,$tabela,$tipo,$id_beneficiario) {
      $sql = new Query($bd);
      
      $txt = "SELECT VALIDA_MATMED(:codigo,:prestador,SYSDATE,:tabela,:tipo,:beneficiario,:versaoTiss) RESULTADO FROM DUAL";
      $sql->clear();
      $sql->addParam(":codigo",$codigo_produto);
      $sql->addParam(":tabela",$tabela);
      $sql->addParam(":tipo",$tipo);
      $sql->addParam(":prestador",$prestador);
      $sql->addParam(":beneficiario",$id_beneficiario);
      $sql->addParam(":versaoTiss",$_SESSION['versaoTiss']);
      $sql->executeQuery($txt);
      
      $id_produto = $sql->result("RESULTADO");
      if (($id_produto == 0) and ($_SESSION['apelido_operadora'] == 'unimedBarra')){
        $sql->addParam(":versaoTiss",2);
        $sql->executeQuery($txt);
        $id_produto = $sql->result("RESULTADO");  
      }  
      return $id_produto;
            
    }    
    
    function validaMatmed2($bd,$codigo_produto,$prestador,$data,$tabela,$tipo,$id_beneficiario) {
      $sql = new Query($bd);
      
      $txt = "BEGIN VALIDA_MATMED2(:codigo,:prestador,SYSDATE,:tabela,:tipo,:beneficiario,:idProduto,:idTabela,:versaoTiss); END;";
      $sql->clear();
      $sql->addParam(":codigo",$codigo_produto);
      $sql->addParam(":tabela",$tabela);
      $sql->addParam(":tipo",$tipo);
      $sql->addParam(":prestador",$prestador);
      $sql->addParam(":beneficiario",$id_beneficiario);
      $sql->addParam(":idTabela",0,15);
      $sql->addParam(":idProduto",0,15);
      $sql->addParam(":versaoTiss",$_SESSION['versaoTiss']);
      $sql->executeSQL($txt);
                  
      $retorno['produto'] = $sql->getReturn(":idProduto");
      $retorno['tabela']  = $sql->getReturn(":idTabela");
      
      return $retorno;
            
    }        

    function mostraCalendario() {
      $sql = new Query();
      $txt = "SELECT COUNT(*) QTDE, NDINICLPAG,NDFIMCLPAG,CLOCACLPAG
                FROM HSSCLPAG
               WHERE NNUMEPRES = :prestador
                 AND CLOCACLPAG IN ('W','A')
                 AND DVIGECLPAG = (SELECT MAX(DVIGECLPAG) FROM HSSCLPAG
                                    WHERE NNUMEPRES = :prestador
                                      AND DVIGECLPAG <= SYSDATE) 
               GROUP BY NDINICLPAG,NDFIMCLPAG,CLOCACLPAG
              HAVING COUNT(*) >= 1";
              
      $sql->addParam(":prestador",$_SESSION['id_contratado']);
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0) 
        return 1;
      else
        return 0;  
    }
    
    function retornaDataPagamentoLote ($bd,$local) {	   
      if ($this->mostraCalendario() == 1) {
        $sql = new Query($bd); 
        $txt = "BEGIN ".
               "  CALCULA_DATA_PREVISAO(:prestador,:data_previsao,:tipo,SYSDATE,NULL,:local); ".
               "END; ";         
                 
        $sql->addParam(":prestador",$_SESSION['id_contratado']);
        $sql->addParam(":data_previsao",0,10);
        $sql->addParam(":tipo",0,1);  
        $sql->addParam(":local",$local);
        $sql->executeSQL($txt);
        
        if ($sql->getReturn(":data_previsao") > 0) {       
          $data = new Data();
          return $sql->getReturn(":tipo").$data->formataData($bd,$sql->getReturn(":data_previsao"),'DD/MM/YYYY','DD/MM/YY');
        } else
          return '';            
      }
      else
        return 'C';     
    }

    function retornaPeriodoPagamentoLote ($bd) {
      if ($this->mostraCalendario() == 1) {
        $sql = new Query($bd);
        $sql->clear();
        
        $txt = "SELECT RETORNA_PERIODO_CALENDARIO(:prestador,'W') PERIODO FROM DUAL";  
        $sql->addParam(":prestador",$_SESSION['id_contratado']);
        $sql->executeQuery($txt);
        
        return $sql->result("PERIODO");
      }
      else
        return '';
    } 

    function validaCid($bd,$cid) {
      $formata = new Formata();
      
      $sql = new Query($bd);
      $txt = "SELECT NNUMECID_ CID, 0 
                FROM HSSCID_ 
               WHERE CCODICID_ = :cid 
                 AND (NVL(CSTATCID_,'A') = 'A'  OR (NVL(CSTATCID_,'A') <> 'A' AND DCANCCID_ > SYSDATE))
               UNION ALL
              SELECT NNUMECID_ CID, 1 
                FROM HSSCID_ 
               WHERE CCODICID_ = :cid || '0'
                 AND (NVL(CSTATCID_,'A') = 'A'  OR (NVL(CSTATCID_,'A') <> 'A' AND DCANCCID_ > SYSDATE))
               ORDER BY 2";
             
      $cid = strtoupper($cid);         
      $cid = $formata->somenteCaracteres($cid,'1234567890ABDCEFGHIJKLMNOPQRSTUVWXYZ');          
      $sql->addParam(":cid",$cid);
      $sql->executeQuery($txt);  
                        
      if ($sql->count() > 0)
        return $sql->result("CID");
      else
        return 0;
    }     

    function retornaNomePrestador($bd,$id) {
      $sql = new Query($bd);
      $txt = "SELECT CNOMEPRES FROM FINPRES
               WHERE NNUMEPRES = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CNOMEPRES");
    }
    
    function valorFaturadoLote($bd,$lote) {
      $formata = new Formata();
      
      $sql     = new Query($bd);      
      $txt = "SELECT TOTAL_CONTAS_LOTE(:Lote) FATURADO FROM DUAL";
      $sql->addParam(":lote",$lote);
      $sql->executeQuery($txt);  
      return $formata->formataNumero($sql->result("FATURADO"));
    }   

    function retornaNomeStatus($bd,$id) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CDESCSTAT DESCRICAO FROM HSSSTAT WHERE NNUMESTAT = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("DESCRICAO");
    }    
    
    function trocaDePlanos ($bd,$id){
      $sql = new Query($bd);
      $txt = "SELECT CCODIPLAN,DDATAHPLAN FROM HSSUSUA,HSSHPLAN,HSSPLAN
               WHERE HSSUSUA.NNUMEUSUA = :Usuario
                 AND HSSUSUA.NNUMEUSUA = HSSHPLAN.NNUMEUSUA
                 AND DDATAHPLAN <= TRUNC(SYSDATE)
                 AND DDATAHPLAN > DINCLUSUA
                 AND HSSHPLAN.NNUMEPLAN = HSSPLAN.NNUMEPLAN
               ORDER BY DDATAHPLAN DESC";  
      $sql->addParam(":Usuario",$id);
      $sql->executeQuery($txt);
      
      if ($sql->result("CCODIPLAN") <> null) 
        return 'Trocou de plano em ' + $sql->result("DDATAHPLAN");
      else
        return '';
    }    

    function codigoTitulo($id) {
      $bd = new Query();
      $txt = "SELECT CCODITITU FROM HSSTITU WHERE NNUMETITU = :id";  
      $bd->addParam(":id",$id);
      $bd->executeQuery($txt);
      
      return $bd->result("CCODITITU");
    }   
    
    function diaVencimentoContrato($id) {
      $sql = new Query();
      $txt = "SELECT NDIA_VENCI FROM HSSTITU WHERE NNUMETITU = :id";  
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      
      return $sql->result("NDIA_VENCI");
    }    

    function estadoCivil ($estado) {
      if ($estado == 'S')
        return ('Solteiro (a)');
      elseif ($estado == 'C') 
        return ('Casado (a)');
      elseif ($estado == 'V') 
        return ('Vi�vo (a)');
      elseif ($estado == 'D') 
        return ('Divorciado (a)');
      elseif ($estado == 'U') 
        return ('Uni�o est�vel');
      elseif ($estado == 'A') 
        return ('Separado (a)');
      elseif ($estado == 'O') 
        return ('Outro');
    }    
    
    function categoriaUsuario ($categoria,$abreviado="N") {
      if ($abreviado == "N") {
        if ($categoria == 'T')
          return ('Titular');
        elseif ($categoria == 'F')
          return ('Titular Financeiro');
        elseif ($categoria == 'D')
          return ('Dependente');
        elseif ($categoria == 'A')
          return ('Agregado');
        elseif ($categoria == 'U')
          return ('Dependente Universit�rio');
        elseif ($categoria == 'E')
          return ('Dependente especial');
        elseif ($categoria == 'L')
          return ('Dependente legal');  
      } else {
        if ($categoria == 'T')
          return ('Titular        ');
        elseif ($categoria == 'F')
          return ('Tit. Financeiro');
        elseif ($categoria == 'D')
          return ('Dependente     ');
        elseif ($categoria == 'A')
          return ('Agregado       ');
        elseif ($categoria == 'U')
          return ('Dep. Universit.');
        elseif ($categoria == 'E')
          return ('Dep. deficiente');
        elseif ($categoria == 'L')
          return ('Dep. legal     ');
      }
    }

    function grauDeParentesco ($grau) {
      if ($grau == 'F')
        return ('Filho');
      elseif ($grau == 'E') 
        return ('Esposo (a)');
      elseif ($grau == 'P') 
        return ('Pai');
      elseif ($grau == 'M') 
        return ('M�e');
      elseif ($grau == 'I') 
        return ('Irm�o (�)');
      elseif ($grau == 'A') 
        return ('Avo (�)');
      elseif ($grau == 'T') 
        return ('Tio (a)');
      elseif ($grau == 'R') 
        return ('Primo (a)');
      elseif ($grau == 'C') 
        return ('Cunhado (a)');
      elseif ($grau == 'G') 
        return ('Genro/Nora');
      elseif ($grau == 'N') 
        return ('Neto(a)');
      elseif ($grau == 'B') 
        return ('Sobrinho(a)');
      elseif ($grau == 'S') 
        return ('Sogro(a)');
      elseif ($grau == 'O') 
        return ('Outro');
      elseif ($grau == 'D') 
        return ('Agregado(a)');
      elseif ($grau == 'H') 
        return ('Enteado(a)');
      elseif ($grau == 'J') 
        return ('Companheiro(a)');
      elseif ($grau == 'X') 
        return ('Pr�prio Titular');
      elseif ($grau == 'Q') 
        return ('Ex-C�njuge');
      elseif ($grau == 'L') 
        return ('Homoafetivo(a)');
    }    
    
    function coberturaPlano ($codigo) {
      switch ($codigo) {
        case '01' : $cobertura = 'Ambulatorial'; break;
        case '02' : $cobertura = 'Hospitalar COM obstetr�cia'; break;
        case '03' : $cobertura = 'Hospitalar SEM obstetr�cia'; break;
        case '04' : $cobertura = 'Odontol�gico'; break;
        case '05' : $cobertura = 'Refer�ncia'; break;
        case '06' : $cobertura = 'Ambulatorial + Hospitalar COM Obstetr�cia'; break;
        case '07' : $cobertura = 'Ambulatorial + Hospitalar SEM Obstetr�cia'; break;
        case '08' : $cobertura = 'Ambulatorial + Odontol�gico'; break;
        case '09' : $cobertura = 'Hospitalar COM Obstetr�cia + Odontol�gico'; break;
        case '10' : $cobertura = 'Hospitalar SEM Obstetr�cia + Odontol�gico'; break;
        case '11' : $cobertura = 'Ambulatorial + Hospitalar COM Obstetr�cia + Odontol�gico'; break;
        case '12' : $cobertura = 'Ambulatorial + Hospitalar SEM Obstetr�cia + Odontol�gico'; break;
        case '90' : $cobertura = 'Outros'; break;
      }
      
      return $cobertura;
    }    
    
    function abrangenciaPlano ($codigo) {
      switch ($codigo) {
        case '1' : $abrangencia = 'Nacional';            break;
        case '2' : $abrangencia = 'Grupo de estados';    break;
        case '3' : $abrangencia = 'Estadual';            break;
        case '4' : $abrangencia = 'Grupo de munic�pios'; break;
        case '5' : $abrangencia = 'Municipal';           break;
      }
      
      return $abrangencia;
    }
    
    function retornaCodigoUsuario ($bd,$carteira) {
      $sql = new Query($bd);
      
      if ($_SESSION['apelido_operadora'] == "smile") {
        $txt = "SELECT CCODIUSUA FROM HSSUSUA 
                 WHERE LPAD(CCODIUSUA,16,'0') = :carteira ";
      } else {
        $txt = "SELECT CCODIUSUA FROM HSSUSUA 
                 WHERE CCARTUSUA = :carteira 
                   AND (CSITUUSUA = 'A' OR (CSITUUSUA <> 'A' AND DALIBUSUA > SYSDATE) OR (CSITUUSUA = 'M' AND DALIBUSUA > SYSDATE))"; 
      }  

      $sql->clear();
      $sql->addParam(":carteira",$carteira);
      $sql->executeQuery($txt);  
      
      return $sql->result("CCODIUSUA"); 
    }    
    
    function retornaDataFechamento ($bd) {
      $sql     = new Query($bd);
      $formata = new Formata();
      $data1    = new Data();
      
      $txt = "SELECT DECODE(CLOCACLPAG,'W',0,1) ORDEM, NDFIMCLPAG DIA,TO_CHAR(SYSDATE,'MM') MES,NMESPCLPAG M,TO_CHAR(SYSDATE,'YYYY') ANO                                 
                FROM HSSCLPAG                                                                
               WHERE NNUMEPRES = :prestador                                                  
                 AND TO_NUMBER(TO_CHAR(SYSDATE,'DD')) >= NDINICLPAG                                                       
                 AND TO_NUMBER(TO_CHAR(SYSDATE,'DD')) <= NDFIMCLPAG 
                 AND CLOCACLPAG IN ('W','A')     
                 AND DVIGECLPAG = (SELECT MAX(DVIGECLPAG) FROM HSSCLPAG
                                    WHERE NNUMEPRES = :prestador
                                      AND DVIGECLPAG <= SYSDATE)
               UNION                                                                        
              SELECT 2 ORDEM, 31 DIA,TO_CHAR(SYSDATE,'MM') MES,1 M,TO_CHAR(SYSDATE,'YYYY') ANO                             
                FROM DUAL                                                               
               ORDER BY 1";   
       
      $sql->addParam(":prestador",$_SESSION['id_contratado']);
      $sql->executeQuery($txt);

      $dia = $formata->acrescentaZeros($sql->result("DIA"),2);  
      $mes = $sql->result("MES");  
      $ano = $sql->result("ANO"); 

      if (($mes == 2) and ($dia > 28))
      
        if ($data1->anoBissexto($ano))
          $dia = 29;
        else
          $dia = 28;
          
      else if (($dia == 31) and (($mes == 4) or ($mes == 6) or ($mes == 9) or ($mes == 11)))
        $dia = 30;
           
      $data = $dia."/".$mes."/".$ano;
      
      if ($data1->comparaData($bd,$data,'',"<"))
        return $data1->incrementaData($data,$sql->result("M"),'M');
      else
        return $data;
    }
    function carregaExecutantes2($bd,$tpl,$selecionado,$bloco,$flag, $valor,$selecionado,$tipo="normal",$opcao_inicial = "Selecione o prestador executante",$restringe="N") {  
      $sql = new Query($bd);
      $sql->clear();
        
      if(($restringe == "S") and ($_SESSION['apelido_operadora'] == 'coamo'))
          $temp = "    AND NVL(CSOLIPRES,'S') = 'S' ";
      else    
          $temp = "";
          
      if (isset($_SESSION['executantes']))
        $executantes = $_SESSION['executantes'];
      else
        $executantes = '';

      if ($tipo == "auditoria") {
        $txt = "SELECT DISTINCT NNUMEPRES || '*'  || NNUMEPRES NNUMEPRES,CNOMEPRES,NVL(NCRM_PRES,'00000') NCRM_PRES
                  FROM FINPRES
                 WHERE CCREDPRES IN ('S','O')
                   AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = FINPRES.NNUMEPRES
                                           AND CLWEBESPRE = 'S')
                   AND NNUMEPRES <> :contratado                                           
                 ORDER BY 2 ";
      }        
      else if ($tipo == "pedido") {
        $txt = "SELECT DISTINCT NNUMEPRES || '*'  || NNUMEPRES NNUMEPRES,CNOMEPRES,NVL(NCRM_PRES,'00000') NCRM_PRES
                  FROM FINPRES
                 WHERE CCREDPRES IN ('S','O')
                   AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = :contratado
                                           AND CLWEBESPRE = 'S')
                   AND 0 = (SELECT COUNT(*) FROM HSSPEXEC WHERE HSSPEXEC.NNUMEPRES = :contratado) " . $temp .               
               "  UNION ALL
                SELECT NEXECPRES || '*'  || NLOCAPRES NNUMEPRES,CNOMEPRES,NVL(NCRM_PRES,'00000') NCRM_PRES
                  FROM HSSPEXEC,FINPRES
                 WHERE HSSPEXEC.NNUMEPRES = :contratado
                   AND HSSPEXEC.NEXECPRES = FINPRES.NNUMEPRES " . $temp .
               "   AND HSSPEXEC.NNUMEPRES IN (SELECT HSSPEXEC.NNUMEPRES FROM HSSPEXEC,HSSESPRE
                                                WHERE HSSPEXEC.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                   AND CLWEBESPRE = 'S'
                                                   AND HSSPEXEC.NNUMEPRES = :contratado )
                 ORDER BY 2 ";
      } else if (( ($executantes == '2') and 
                  (($tipo == "consulta") or ($tipo == "normal") or ($tipo == "procedimento") or ($tipo == "faturamento")) ) or
                 ( ($executantes == '5') and ($tipo == "consulta"))) {           // Corpo Clinico
        $txt = "SELECT DISTINCT HSSCCLIN.NNUMEPRES NNUMEPRES,CNOMEPRES FROM HSSCCLIN,FINPRES
                 WHERE NHOSPPRES = :contratado
                   AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSCCLIN.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE,HSSCCLIN
                                                 WHERE HSSCCLIN.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                   AND CLWEBESPRE = 'S'
                                                   AND NHOSPPRES = :contratado )
                   AND NVL(CSTATCCLIN,'A') = 'A' " . $temp .
               " ORDER BY 2";
      } else if ( ($executantes == '3') and 
                  (($tipo == "consulta") or ($tipo == "normal") or ($tipo == "procedimento") or ($tipo == "faturamento")) ) {    // Contratado
        $txt = "SELECT NNUMEPRES NNUMEPRES,CNOMEPRES FROM FINPRES
                 WHERE NNUMEPRES = :contratado " . $temp .
                 "  AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = :contratado
                                           AND CLWEBESPRE = 'S')
                ORDER BY 2";
      } else if ( ($executantes == '4') and (($tipo == "procedimento") or ($tipo == "faturamento")) ) {    // Contratado
        $txt = "SELECT NNUMEPRES,CNOMEPRES FROM FINPRES
                 WHERE NNUMEPRES = :contratado " . $temp .
                 "  AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = :contratado
                                           AND CLWEBESPRE = 'S')
                ORDER BY 2";           
      } else {                                         // Contratado + Corpo Clinico
        $txt = "SELECT DISTINCT HSSCCLIN.NNUMEPRES NNUMEPRES,CNOMEPRES FROM HSSCCLIN,FINPRES
                 WHERE NHOSPPRES = :contratado
                   AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   AND HSSCCLIN.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE,HSSCCLIN
                                                 WHERE HSSCCLIN.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                     AND CLWEBESPRE = 'S'
                                                     AND NHOSPPRES = :contratado )
                   AND NVL(CSTATCCLIN,'A') = 'A' " . $temp .
               " UNION ALL
                SELECT NNUMEPRES,CNOMEPRES FROM FINPRES
                 WHERE NNUMEPRES = :contratado " . $temp .
                 "  AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = :contratado
                                           AND CLWEBESPRE = 'S')
                ORDER BY 2";
      }
          
      
      if (isset($_SESSION['id_contratado']))
        $sql->addParam(":contratado",$_SESSION['id_contratado']);
      else
        $sql->addParam(":contratado",0);   
        
      $sql->executeQuery($txt);  

      $tpl->$flag = "";
      
      if (isset($_SESSION['pedido_exames']) and ($_SESSION['pedido_exames'] == '2') and ($tipo == "pedido")) {
        $tpl->$valor = "Livre escolha";  
        $tpl->$flag = "0*0";
      } else if (($tipo == "glosa") or ($tipo == "faturamento") or ($tipo == "auditoria")){
      $tpl->$flag = "";
        $tpl->$valor = "Todos os prestadores";
    }else {
      $tpl->$flag = "";
        $tpl->$valor = $opcao_inicial; 
    }
      $tpl->block($bloco);      
      
      while (!$sql->eof()) {
        $tpl->$flag = $sql->result("NNUMEPRES");
        $tpl->$valor = $sql->result("CNOMEPRES");
        
        if ($selecionado == $sql->result("NNUMEPRES"))
          $tpl->$selecionado = "selected";
        else
          $tpl->$selecionado = "";
          
        $tpl->block($bloco);
        $sql->next();
      }    
    }
  
  
  
  
  
  
      
    function carregaExecutantes($bd,$tpl,$selecionado,$tipo="normal",$restringe="N") {  
      $sql = new Query($bd);
      $sql->clear();
        
      if(($restringe == "S") and ($_SESSION['apelido_operadora'] == 'coamo'))
          $temp = "    AND NVL(CSOLIPRES,'S') = 'S' ";
      else    
          $temp = "";

      if ($tipo == "agenda")
        $temp_cclin = "   AND NVL(CAGEWCCLIN,'S') = 'S'";
      else
        $temp_cclin = "";
          
      if (isset($_SESSION['executantes']))
        $executantes = $_SESSION['executantes'];
      else
        $executantes = '';

      if ($tipo == "auditoria") {
        $txt = "SELECT DISTINCT NNUMEPRES || '*'  || NNUMEPRES NNUMEPRES,CNOMEPRES,NVL(NCRM_PRES,'00000') NCRM_PRES, FINPRES.NNUMEPRES ID
                  FROM FINPRES
                 WHERE CCREDPRES IN ('S','O')
                   AND AFASTAMENTOPRESTADOR (NNUMEPRES,SYSDATE) = 'N'
                   AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = FINPRES.NNUMEPRES
                                           AND CLWEBESPRE = 'S')
                   AND NNUMEPRES <> :contratado                                           
                 ORDER BY 2 ";
      }        
      else if ($tipo == "pedido") {
        $txt = "SELECT DISTINCT NNUMEPRES || '*'  || NNUMEPRES NNUMEPRES,CNOMEPRES,NVL(NCRM_PRES,'00000') NCRM_PRES, FINPRES.NNUMEPRES ID
                  FROM FINPRES
                 WHERE CCREDPRES IN ('S','O')
                   AND AFASTAMENTOPRESTADOR (NNUMEPRES,SYSDATE) = 'N'
                   AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = :contratado
                                           AND CLWEBESPRE = 'S')
                   AND 0 = (SELECT COUNT(*) FROM HSSPEXEC WHERE HSSPEXEC.NNUMEPRES = :contratado) " . $temp .               
               "  UNION ALL
                SELECT NEXECPRES || '*'  || NLOCAPRES NNUMEPRES,CNOMEPRES,NVL(NCRM_PRES,'00000') NCRM_PRES, FINPRES.NNUMEPRES ID
                  FROM HSSPEXEC,FINPRES
                 WHERE HSSPEXEC.NNUMEPRES = :contratado
                   AND HSSPEXEC.NEXECPRES = FINPRES.NNUMEPRES " . $temp .
               "   AND AFASTAMENTOPRESTADOR (FINPRES.NNUMEPRES,SYSDATE) = 'N' ".
               "   AND HSSPEXEC.NNUMEPRES IN (SELECT HSSPEXEC.NNUMEPRES FROM HSSPEXEC,HSSESPRE
                                                WHERE HSSPEXEC.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                   AND CLWEBESPRE = 'S'
                                                   AND HSSPEXEC.NNUMEPRES = :contratado )
                 ORDER BY 2 ";
      } else if (( ($executantes == '2') and 
                   ( ($tipo == "agenda") or ($tipo == "consulta") or ($tipo == "normal") or ($tipo == "procedimento") or ($tipo == "faturamento")) ) or
                 ( ($executantes == '5') and ($tipo == "consulta"))) {           // Corpo Clinico
        $txt = "SELECT DISTINCT HSSCCLIN.NNUMEPRES NNUMEPRES,CNOMEPRES, FINPRES.NNUMEPRES ID FROM HSSCCLIN,FINPRES
                 WHERE NHOSPPRES = :contratado
                   AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   AND AFASTAMENTOPRESTADOR (FINPRES.NNUMEPRES,SYSDATE) = 'N'
                   AND HSSCCLIN.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE,HSSCCLIN
                                                 WHERE HSSCCLIN.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                   AND CLWEBESPRE = 'S'
                                                   AND NHOSPPRES = :contratado )
                   AND NVL(CSTATCCLIN,'A') = 'A' " . $temp.$temp_cclin;
                             
        $txt .= " ORDER BY 2";
        
      } else if ( ($executantes == '3') and 
                  (($tipo == "agenda") or ($tipo == "consulta") or ($tipo == "normal") or ($tipo == "procedimento") or ($tipo == "faturamento")) ) {    // Contratado
        $txt = "SELECT NNUMEPRES NNUMEPRES,CNOMEPRES, FINPRES.NNUMEPRES ID FROM FINPRES
                 WHERE NNUMEPRES = :contratado " . $temp .
                 " AND AFASTAMENTOPRESTADOR (NNUMEPRES,SYSDATE) = 'N' ".
                  "AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                      WHERE NNUMEPRES = :contratado
                                        AND CLWEBESPRE = 'S') ";
                                                     
        $txt .= " ORDER BY 2";                                           

      } else if ( ($executantes == '4') and (($tipo == "agenda") or ($tipo == "procedimento") or ($tipo == "faturamento")) ) {    // Contratado
        $txt = "SELECT NNUMEPRES,CNOMEPRES, FINPRES.NNUMEPRES ID FROM FINPRES
                 WHERE NNUMEPRES = :contratado " . $temp .                 
                 " AND AFASTAMENTOPRESTADOR (NNUMEPRES,SYSDATE) = 'N' ".
                  "AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                      WHERE NNUMEPRES = :contratado
                                        AND CLWEBESPRE = 'S') ";
                                                     
        $txt .= " ORDER BY 2";         
      } else if (($executantes == '6') and 
                  (($tipo == "procedimento") or ($tipo == "faturamento")) ) {  // Contratado
        $txt = "SELECT NNUMEPRES,CNOMEPRES FROM FINPRES
                 WHERE NNUMEPRES = :contratado " . $temp .
                 " AND AFASTAMENTOPRESTADOR (NNUMEPRES,SYSDATE) = 'N' ".
                  "AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                      WHERE NNUMEPRES = :contratado
                                        AND CLWEBESPRE = 'S')
                 ORDER BY 2";      
      }else if (($executantes == '6') and 
                  (($tipo == "consulta") or ($tipo == "faturamento")) ) {  // Corpo Clinico
        $txt = "SELECT DISTINCT HSSCCLIN.NNUMEPRES NNUMEPRES,CNOMEPRES FROM HSSCCLIN,FINPRES
                 WHERE NHOSPPRES = :contratado
                   AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   AND AFASTAMENTOPRESTADOR (FINPRES.NNUMEPRES,SYSDATE) = 'N'
                   AND HSSCCLIN.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE,HSSCCLIN
                                                 WHERE HSSCCLIN.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                   AND CLWEBESPRE = 'S'
                                                   AND NHOSPPRES = :contratado )
                   AND NVL(CSTATCCLIN,'A') = 'A' " . $temp .
               " ORDER BY 2";     
      }else {                                         // Contratado + Corpo Clinico
        $txt = "SELECT DISTINCT HSSCCLIN.NNUMEPRES NNUMEPRES,CNOMEPRES, FINPRES.NNUMEPRES ID FROM HSSCCLIN,FINPRES
                 WHERE NHOSPPRES = :contratado
                   AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                   AND AFASTAMENTOPRESTADOR (FINPRES.NNUMEPRES,SYSDATE) = 'N'
                   AND HSSCCLIN.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE,HSSCCLIN
                                                 WHERE HSSCCLIN.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                     AND CLWEBESPRE = 'S'
                                                     AND NHOSPPRES = :contratado )
                   AND NVL(CSTATCCLIN,'A') = 'A' " . $temp.$temp_cclin;
                             
        $txt .= " UNION ALL
                 SELECT NNUMEPRES,CNOMEPRES, FINPRES.NNUMEPRES ID FROM FINPRES
                  WHERE NNUMEPRES = :contratado " . $temp .
                  " AND AFASTAMENTOPRESTADOR (NNUMEPRES,SYSDATE) = 'N' ".
                   "AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                       WHERE NNUMEPRES = :contratado
                                         AND CLWEBESPRE = 'S') ";
                                                                                      
        $txt .= " ORDER BY 2";
      }
          
      
      if (isset($_SESSION['id_contratado']))
        $sql->addParam(":contratado",$_SESSION['id_contratado']);
      else
        $sql->addParam(":contratado",0);   
        
      $sql->executeQuery($txt);  

      $tpl->EXECUTANTE_ID = "";
      
      if (isset($_SESSION['pedido_exames']) and ($_SESSION['pedido_exames'] == '2') and ($tipo == "pedido")) {
        $tpl->EXECUTANTE_NOME = "Livre escolha";  
        $tpl->EXECUTANTE_ID = "0*0";
      } else if (($tipo == "glosa") or ($tipo == "faturamento") or ($tipo == "auditoria"))
        $tpl->EXECUTANTE_NOME = "Todos os prestadores";
      else    
        $tpl->EXECUTANTE_NOME = "Selecione o prestador executante"; 
        
      $tpl->block("EXECUTANTE");      
      
      while (!$sql->eof()) {
        $tpl->EXECUTANTE_ID = $sql->result("NNUMEPRES");
        $tpl->EXECUTANTE_NOME = $sql->result("CNOMEPRES");
        
        if ($selecionado == $sql->result("ID"))
          $tpl->EXECUTANTE_SELECIONADO = "selected";
        else
          $tpl->EXECUTANTE_SELECIONADO = "";
          
        $tpl->block("EXECUTANTE");
        $sql->next();
      }    
    }  
    
    function carregaSolicitantes($bd,$tpl,$selecionado,$tipo="normal") {  
      $sql = new Query($bd);
      $sql->clear();
      
      $natureza = "SELECT CPESSPRES FROM FINPRES
                   WHERE NNUMEPRES = :contratado";

      if (isset($_SESSION['id_contratado']))
        $sql->addParam(":contratado",$_SESSION['id_contratado']);
      else
        $sql->addParam(":contratado",0);            
        
      $sql->executeQuery($natureza);      

      $natureza = $sql->result("CPESSPRES");  
      
      $sql->clear();

      if (($_SESSION['solicitantes'] == '1')) { // Todos configurados no sistema
        $tpl->block("SOLICITANTE_INPUT");   
      } else {
        if ((($_SESSION['solicitantes'] == '2') and ($tipo == "normal" or $tipo == "consulta")) or 
            (($_SESSION['solicitantes'] == '5') and ($tipo == "consulta")) or 
            (($_SESSION['executantes'] == '2') and ($tipo == "pedido"))) { // Corpo Clinico
          $txt = "SELECT DISTINCT DECODE(:natureza,'J',HSSCCLIN.NNUMEPRES,HSSCCLIN.NHOSPPRES) NNUMEPRES,CNOMEPRES FROM HSSCCLIN,FINPRES
                   WHERE DECODE(:natureza,'J',NHOSPPRES,HSSCCLIN.NNUMEPRES) = :contratado
                     AND DECODE(:natureza,'J',HSSCCLIN.NNUMEPRES,HSSCCLIN.NHOSPPRES) = FINPRES.NNUMEPRES
                     AND NVL(CSTATCCLIN,'A') = 'A'
                   ORDER BY 2 ";
            $sql->addParam(":contratado",$_SESSION['id_contratado']);
            $sql->addParam(":natureza",$natureza);
        } else if ((($_SESSION['solicitantes'] == '3') and ($tipo == "normal" or $tipo == "consulta")) or
                   (($_SESSION['executantes'] == '3') and ($tipo == "pedido"))) {    // Contratado
          $txt = "SELECT NNUMEPRES,CNOMEPRES FROM FINPRES
                   WHERE NNUMEPRES = :contratado
                   ORDER BY 2";
          $sql->addParam(":contratado",$_SESSION['id_contratado']);           
        } else if ($tipo == "pedido_empresa") {
          $txt = "SELECT NNUMEPRES, CNOMEPRES FROM HSSTITU, FINPRES
                   WHERE HSSTITU.NAUDIUSUA = FINPRES.NNUMEUSUA
                     AND CAUDIPRES = 'S'
                     AND HSSTITU.NNUMETITU = :contrato
                   ORDER BY 2";
          $sql->addParam(":contrato",$_SESSION['id_contrato']);     
        } else {                                         // Contratado + Corpo Clinico                   
          $txt = "SELECT DISTINCT HSSCCLIN.NNUMEPRES NNUMEPRES,CNOMEPRES 
                    FROM HSSCCLIN,FINPRES
                   WHERE NHOSPPRES = :contratado
                     AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                     AND HSSCCLIN.NNUMEPRES IN (SELECT HSSESPRE.NNUMEPRES FROM HSSESPRE,HSSCCLIN
                                                 WHERE HSSCCLIN.NNUMEPRES = HSSESPRE.NNUMEPRES
                                                   AND CLWEBESPRE = 'S'
                                                   AND NHOSPPRES = :contratado )
                     AND NVL(CSTATCCLIN,'A') = 'A' " . 
               " UNION ALL
                SELECT NNUMEPRES,CNOMEPRES FROM FINPRES
                 WHERE NNUMEPRES = :contratado " .
                 "  AND NNUMEPRES IN (SELECT NNUMEPRES FROM HSSESPRE
                                        WHERE NNUMEPRES = :contratado
                                           AND CLWEBESPRE = 'S')
                ORDER BY 2";                   
            $sql->addParam(":contratado",$_SESSION['id_contratado']);
        }

        $sql->executeQuery($txt);  
        
        if (($tipo == "pedido_empresa") and ($sql->count() == 0)) {
          $sql->clear();
          $txt = "SELECT NNUMEPRES, CNOMEPRES FROM FINPRES
                   WHERE CSOLIPRES = 'S'
                   ORDER BY 2";
          $sql->executeQuery($txt);             
        }

      
        if ($sql->count() <> 1) {
          $tpl->SOLICITANTE_ID = "";
          $tpl->SOLICITANTE_NOME = "Selecione o prestador solicitante";
          $tpl->block("SOLICITANTE");
        }
      
        while (!$sql->eof()) {
          $tpl->SOLICITANTE_ID = $sql->result("NNUMEPRES");
          $tpl->SOLICITANTE_NOME = $sql->result("CNOMEPRES");
          
          if ($selecionado == $sql->result("NNUMEPRES"))
            $tpl->SOLICITANTE_SELECIONADO = "selected";
          else
            $tpl->SOLICITANTE_SELECIONADO = "";
            
          $tpl->block("SOLICITANTE");
          $sql->next();
        }    
        
        $tpl->block("SOLICITANTE_SELECT");         
      }
    }
   function carregaSolicitantesClinipam($bd,$tpl,$selecionado,$tipo="normal") {  
      $sql = new Query($bd);
      $sql->clear();
      
      $natureza = "SELECT CPESSPRES FROM FINPRES
                   WHERE NNUMEPRES = :contratado";

      if (isset($_SESSION['id_contratado']))
        $sql->addParam(":contratado",$_SESSION['id_contratado']);
      else
        $sql->addParam(":contratado",0);            
        
      $sql->executeQuery($natureza);      

      $natureza = $sql->result("CPESSPRES");  
      
      $sql->clear();

      if (($_SESSION['solicitantes'] > '4')) { // Todos configurados no sistema
        $tpl->block("SOLICITANTE_INPUT");   
      } else {
        if ((($_SESSION['solicitantes'] == '2') and ($tipo == "normal" or $tipo == "consulta")) or 
            (($_SESSION['solicitantes'] == '5') and ($tipo == "consulta")) or 
            (($_SESSION['executantes'] == '2') and ($tipo == "pedido"))) { // Corpo Clinico
          $txt = "SELECT DISTINCT DECODE(:natureza,'J',HSSCCLIN.NNUMEPRES,HSSCCLIN.NHOSPPRES) NNUMEPRES,CNOMEPRES FROM HSSCCLIN,FINPRES
                   WHERE DECODE(:natureza,'J',NHOSPPRES,HSSCCLIN.NNUMEPRES) = :contratado
                     AND DECODE(:natureza,'J',HSSCCLIN.NNUMEPRES,HSSCCLIN.NHOSPPRES) = FINPRES.NNUMEPRES
                     AND NVL(CSTATCCLIN,'A') = 'A'
                   ORDER BY 2 ";
            $sql->addParam(":contratado",$_SESSION['id_contratado']);
            $sql->addParam(":natureza",$natureza);
        } else if ((($_SESSION['solicitantes'] == '3') and ($tipo == "normal" or $tipo == "consulta")) or
                   (($_SESSION['executantes'] == '3') and ($tipo == "pedido"))) {    // Contratado
          $txt = "SELECT NNUMEPRES,CNOMEPRES FROM FINPRES
                   WHERE NNUMEPRES = :contratado
                   ORDER BY 2";
          $sql->addParam(":contratado",$_SESSION['id_contratado']);           
        } else if ($tipo == "pedido_empresa") {
          $txt = "SELECT NNUMEPRES, CNOMEPRES FROM HSSTITU, FINPRES
                   WHERE HSSTITU.NAUDIUSUA = FINPRES.NNUMEUSUA
                     AND CAUDIPRES = 'S'
                     AND HSSTITU.NNUMETITU = :contrato
                   ORDER BY 2";
          $sql->addParam(":contrato",$_SESSION['id_contrato']);     
        } else {    
            if($_SESSION['solicitantes'] == '2'){                                     // Contratado + Corpo Clinico                   
               $txt = "SELECT DISTINCT DECODE(:natureza,'J',HSSCCLIN.NNUMEPRES,HSSCCLIN.NHOSPPRES) NNUMEPRES,CNOMEPRES FROM HSSCCLIN,FINPRES
                       WHERE DECODE(:natureza,'J',NHOSPPRES,HSSCCLIN.NNUMEPRES) = :contratado
                         AND DECODE(:natureza,'J',HSSCCLIN.NNUMEPRES,HSSCCLIN.NHOSPPRES) = FINPRES.NNUMEPRES
                         AND NVL(CSTATCCLIN,'A') = 'A'
                       ORDER BY 2 ";
                $sql->addParam(":contratado",$_SESSION['id_contratado']);
                $sql->addParam(":natureza",$natureza);
            }elseif(($_SESSION['solicitantes'] == '1') or ($_SESSION['solicitantes'] == '3')){       
                $txt = "SELECT NNUMEPRES, CNOMEPRES, NCRM_PRES, CCONSPRES, CUFCRPRES
                        FROM FINPRES, HSSCONF
                        WHERE NNUMEPRES > 0
                        AND CSOLIPRES = 'S'
                       AND (NVL(CPJSOCONF, 'S') = 'S' OR
                      (NVL(CPJSOCONF, 'S') = 'N' AND CPESSPRES = 'F'))
                      ORDER BY CNOMEPRES";
            }if($_SESSION['solicitantes'] == '4'){
                   $txt = " SELECT 1 ORDEM, CNOMEPRES,NNUMEPRES
                              FROM FINPRES
                             WHERE NNUMEPRES = :contratado                            
                             UNION
                            SELECT 2 ORDEM, CNOMEPRES,FINPRES.NNUMEPRES
                              FROM HSSCCLIN, FINPRES
                             WHERE HSSCCLIN.NHOSPPRES = :contratado
                               AND HSSCCLIN.NNUMEPRES = FINPRES.NNUMEPRES
                               AND HSSCCLIN.CSTATCCLIN = 'A' 
                             ORDER BY 1, CNOMEPRES";
                    $sql->addParam(":contratado",$_SESSION['id_contratado']);
                  
            }        
        }

        $sql->executeQuery($txt);  
        
        if (($tipo == "pedido_empresa") and ($sql->count() == 0)) {
          $sql->clear();
          $txt = "SELECT NNUMEPRES, CNOMEPRES FROM FINPRES
                   WHERE CSOLIPRES = 'S'
                   ORDER BY 2";
          $sql->executeQuery($txt);             
        }
      
        if ($sql->count() <> 1) {
          $tpl->SOLICITANTE_ID = "";
          $tpl->SOLICITANTE_NOME = "Selecione o prestador solicitante";
          $tpl->block("SOLICITANTE");
        }
      
        while (!$sql->eof()) {
          $tpl->SOLICITANTE_ID = $sql->result("NNUMEPRES");
          $tpl->SOLICITANTE_NOME = $sql->result("CNOMEPRES");
          
          if ($selecionado == $sql->result("NNUMEPRES"))
            $tpl->SOLICITANTE_SELECIONADO = "selected";
          else
            $tpl->SOLICITANTE_SELECIONADO = "";
            
          $tpl->block("SOLICITANTE");
          $sql->next();
        }    
        
        $tpl->block("SOLICITANTE_SELECT");         
      }
    }        
    function insereProtocolo($bd,$id,$operacao,$descricao,$tipo,$assunto = '') {
      $sql = new Query();
      $txt = "BEGIN ".
             "  INSERE_PROTOCOLO_WEB30 (:tipo,:descricao,:operador,:id,:operacao,:protocolo,:assunto); ".
             "END; ";
      $sql->addParam(":tipo",$tipo); 
      $sql->addParam(":descricao",$descricao); 
      $sql->addParam(":operador",$_SESSION['id_operador']);                                                 
      $sql->addParam(":id",$id);  
      $sql->addParam(":operacao",$operacao);
      $sql->addParam(":assunto",$assunto); 
      $sql->addParam(":protocolo",0,12);   
      $erro = $sql->executeSQL($txt);
      $retorno['erro'] = $erro;
      $retorno['id'] = $sql->getReturn(":protocolo");

      return $retorno;
    }  

    function insereItemProtocolo($bd,$id,$protocolo,$operacao) {
      $sql = new Query($bd);
      $txt = "BEGIN ".
             "  INSERE_ITEM_PROTOCOLO_WEB30 (:protocolo,:id,:operacao); ".
             "END; ";
      $sql->addParam(":protocolo",$protocolo);
      $sql->addParam(":id",$id);  
      $sql->addParam(":operacao",$operacao);
      $erro = $sql->executeSQL($txt); 

      return $erro;  
    }    
    
    function tipoGuia($bd,$guia) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT SUBSTR(TIPOPROCEDIMENTOGUIA30(:guia),1,1) TIPO FROM DUAL";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);
      
      return $sql->result("TIPO");
    }
    
    function verificaBiometria ($bd,$carteira,$confere,$cadastra="S") {
      /*
      $_SESSION['obriga_biometria']
      1 = Para todo atendimento no sistema de guias deve-se pedir a digital do paciente
      2 = O pedido de digital � opcional � atendente
      3 = O pedido de digital � opcional para pacientes com idade X e Y
      4 = O pedido de digital � opcional na operadora e obrigat�rio na web
      5 = O pedido de digital � obrigat�rio na web para pacientes com idade X e Y
      */

      $sql = new Query($bd);
      $txt = "SELECT IDADE(DNASCUSUA,SYSDATE) IDADE,NIDAICONF,NIDAFCONF,CFOIDCONF,NVL(CBIOMUSUA,'S') OBRIGA
                FROM HSSUSUA,HSSCONF 
               WHERE CCODIUSUA = :codigo ";
      $sql->addParam(":codigo",$carteira);
      $sql->executeQuery($txt);
        
      if ($sql->result("OBRIGA") == 'N')
        $continua = true;
      else {          
        if (($_SESSION['obriga_biometria'] == '3') or ($_SESSION['obriga_biometria'] == '5')) {

          $continua = true;

          if (($sql->result("CFOIDCONF") == '><') and 
              (($sql->result("IDADE") > $sql->result("NIDAICONF")) and 
               ($sql->result("IDADE") < $sql->result("NIDAFCONF"))))
            $continua = false;
          else if (($sql->result("CFOIDCONF") == '<') and
                     ($sql->result("IDADE") < $sql->result("NIDAICONF")))
            $continua = false;
          else if (($sql->result("CFOIDCONF") == '>') and
                   ($sql->result("IDADE") > $sql->result("NIDAICONF")))
            $continua = false;
        } 
        else if (($_SESSION['obriga_biometria'] == '1') or ($_SESSION['obriga_biometria'] == '4'))
          $continua = false;
        else
          $continua = true; 
      }          
  
      if (isset($_SESSION['tem_leitor_biometrico']))
        $temLeitor = $_SESSION['tem_leitor_biometrico'];
      else
        $temLeitor = "N";
        
      if (($continua == false) and ($temLeitor == "S" and $confere <> "S" and ($confere <> "C" and $cadastra = "S"))) 
        return false;
      else
        return true;    
    }    
    
    function enderecoUsuario ($bd,$usuario) {
      $sql = new Query($bd);
      $txt = "SELECT NVL(USUA.CENDCUSUA,NVL(USUA.CENDEUSUA,NVL(TITU.CENDCUSUA,TITU.CENDEUSUA))) ENDERECO, 
                     NVL(USUA.CBAICUSUA,NVL(USUA.CBAIRUSUA,NVL(TITU.CBAICUSUA,TITU.CBAIRUSUA))) BAIRRO,
                     NVL(USUA.CCIDCUSUA,NVL(USUA.CCIDAUSUA,NVL(TITU.CCIDCUSUA,TITU.CCIDAUSUA))) CIDADE,
                     NVL(USUA.CUFC_USUA,NVL(USUA.CESTAUSUA,NVL(TITU.CUFC_USUA,TITU.CESTAUSUA))) ESTADO,
                     NVL(USUA.CCEPCUSUA,NVL(USUA.CCEP_USUA,NVL(TITU.CCEPCUSUA,TITU.CCEP_USUA))) CEP
                FROM HSSUSUA USUA,HSSUSUA TITU
               WHERE USUA.NNUMEUSUA = :usuario
                 AND USUA.NTITUUSUA = TITU.NNUMEUSUA ";
      $sql->clear();
      $sql->addParam(":usuario",$usuario);  
      $sql->executeQuery($txt);

      $endereco = array();
      $endereco['bairro'] = $sql->result("BAIRRO");
      $endereco['cidade'] = $sql->result("CIDADE");
      $endereco['estado'] = $sql->result("ESTADO");
      $endereco['cep'] = $sql->result("CEP");
      $endereco['logradouro'] = $sql->result("ENDERECO");
      
      return $endereco;
    } 

    function enderecoPrestador($bd,$prestador) {
      $sql = new Query($bd);
      $txt = "SELECT CENDEPRES || ', ' || CNENDPRES || ' - ' || CBAIRPRES || ' - ' || CCIDAPRES || ' - ' || CFONEPRES ENDERECO 
                FROM FINPRES
               WHERE NNUMEPRES = :nume_pres ";
      $sql->clear();
      $sql->addParam(":nume_pres",$prestador);  
      $sql->executeQuery($txt);    
      
      return $sql->result("ENDERECO");      
    }    

    function retornaCodigoTabelaPrestador ($bd,$prestador,$data,$tipo = 'P',$sequencia = 1,$id_item = 0) {
      $sql = new Query($bd);
      $sql->clear();
      
      if ($tipo == 'P') {
        $txt = "SELECT SUBSTR(CCODITABE,1,2) CCODITABE,0 ORDEM 
                  FROM HSSLINEA,HSSTABE
                 WHERE NNUMEPRES = :prestador
                   AND HSSLINEA.NNUMETABE = HSSTABE.NNUMETABE
                   AND DVIGELINEA = (SELECT MAX(LINEA.DVIGELINEA) FROM HSSLINEA LINEA
                                      WHERE LINEA.NNUMEPRES = HSSLINEA.NNUMEPRES
                                        AND LINEA.DVIGELINEA <= TO_DATE(:data,'DD/MM/YY HH24:mi'))
                UNION ALL
                SELECT SUBSTR(CCODITABE,1,2) CCODITABE,1 ORDEM
                  FROM HSSCONF,HSSTABE
                 WHERE HSSCONF.NNUMETABE = HSSTABE.NNUMETABE
                 ORDER BY ORDEM";
        $sql->addParam(":prestador",$prestador);
        $sql->addParam(":data",$data);   
      } 
      else if  ($tipo == 'T') {
        $txt = "SELECT RETORNA_CODIGO_TABELA_ID_TCON(NNUMETCON) CCODITABE
                  FROM HSSTCON
                 WHERE NNUMETCON = :id_tcon";  
        $sql->addParam(":id_tcon",$id_item);    
      }
       else if  (($tipo == 'M') and ($sequencia == 11)) { //Mat/Med detalhado
        $txt = "SELECT RETORNA_CODIGO_TABELA_ID_MCON(NNUMEMCON) CCODITABE
                  FROM HSSMCON
                 WHERE NNUMEMCON = :id_mcon";  
        $sql->addParam(":id_mcon",$id_item);    
      }
      else if  (($tipo == 'M') and ($sequencia == 10)) { //Mat/Med totalizado
        $txt = "SELECT '00' CCODITABE FROM DUAL ";         
      }
      
      $sql->executeQuery($txt);  
      return $sql->result("CCODITABE");
    }    
    
    function codigoIbge($bd,$estado,$cidade) {
      $sql = new Query($bd);
      $txt = "SELECT CIBGECIDAD FROM HSSCIDAD WHERE CNOMECIDAD = :cidade AND CESTACIDAD = :estado";
      $sql->addParam(":estado",$estado);
      $sql->addParam(":cidade",$cidade);
      $sql->executeQuery($txt);
      return $sql->result("CIBGECIDAD");  
    }      
    
    function retornaCodigoEspecialidadePrestador($bd,$id) {
      $sql = new Query($bd);
      $txt = "SELECT CTIS3ESPEC FROM HSSESPEC,HSSESPRE
               WHERE HSSESPRE.NNUMEPRES = :id
                 AND HSSESPRE.NNUMEESPEC = HSSESPEC.NNUMEESPEC
                 AND CPADRESPRE = 'S' ";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CTIS3ESPEC");
    }
  

  
    function retornaCodigoEspecialidadeGuia($bd,$guia) {
      $sql = new Query($bd);
      $txt = "SELECT CTIS3ESPEC FROM HSSESPEC,HSSGUIA
               WHERE HSSGUIA.NNUMEGUIA = :guia
                 AND HSSGUIA.NNUMEESPEC = HSSESPEC.NNUMEESPEC";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);
      return $sql->result("CTIS3ESPEC");    
      
    }   
      
    
    function reciboAffego ($bd,$pdf,$guia) {  
      $pdf->AddPage('P');
      $formata = new Formata();
       
      $sql = new Query($bd);
      $txt = "SELECT HSSGUIA.NNUMEGUIA,USUA.CCODIUSUA||' - '|| USUA.CNOMEUSUA CNOMEUSUA, TITU.CNOMEUSUA TITULAR, CNOMEPRES,
                     TO_CHAR(DEMISGUIA,'DD/MM/YYYY') DEMISGUIA
                FROM HSSGUIA, HSSUSUA USUA, HSSUSUA TITU, FINPRES
               WHERE HSSGUIA.NNUMEGUIA = :guia
                 AND HSSGUIA.NUTILUSUA = USUA.NNUMEUSUA
                 AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                 AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES ";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);
      
      $qtde = 0;
      while ($qtde < 2) {  
      
        if ($qtde == 0) {
          $pdf->Ln(20);    
          $pdf->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
        } else {
          $pdf->Ln(10);    
          $pdf->Cell(190,1,str_repeat('-',270),0,1);    
          $pdf->Ln(30);
          $pdf->Image('../comum/img/logo_relatorio.jpg',10,155,40,25);
        }
        
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,3,'Usu�rio/Paciente:','',0,'L');
        $pdf->SetFont('Arial','B',8);   
        $pdf->Cell(160,5,$sql->result("CNOMEUSUA"),'',1,'L');
        
        $pdf->SetFont('Arial','',8);   
        $pdf->Cell(30,3,'Associado titular:','',0,'L');
        $pdf->SetFont('Arial','B',8);   
        $pdf->Cell(130,5,$sql->result("TITULAR"),'',0,'L');
        $pdf->SetFont('Arial','B',12);      
        $pdf->Cell(30,5,'Guia N� '.$formata->acrescentaZeros($sql->result("NNUMEGUIA"),7),'',1,'R');  
        
        $pdf->SetFont('Arial','',8);   
        $pdf->Cell(30,3,'M�dico/Prestador:','',0,'L');
        $pdf->SetFont('Arial','B',8);   
        $pdf->Cell(130,5,$sql->result("CNOMEPRES"),'',0,'L');
        $pdf->Cell(30,5,'Data: '.$sql->result("DEMISGUIA"),'',1,'R');  

        $pdf->SetFont('Arial','B',5);  
        
        if ($qtde == 0)
          $pdf->Cell(190,3,'Via prestador','',1,'R');
        else
          $pdf->Cell(190,3,'Via benefici�rio','',1,'R');
        
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'Qtde',1,0,'L');
        $pdf->Cell(30,5,'C�digo',1,0,'L');
        $pdf->Cell(90,5,'Procedimento / Descri��o do servi�o',1,0,'L');
        $pdf->Cell(20,5,'Servi�o (R$)',1,0,'L');
        $pdf->Cell(20,5,'Prestador (R$)',1,0,'L');
        $pdf->Cell(20,5,'Co-particip. (R$)',1,1,'L');
      
        $txt_proc = "SELECT HSSPGUI.CCODIPMED,NQUANPGUI,NQINDPGUI,NCOPRPGUI,CNOMEPMED
                       FROM HSSPGUI,HSSPMED
                      WHERE HSSPGUI.NNUMEGUIA = :guia
                        AND CSTATPGUI IS NULL
                        AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                     UNION ALL
                     SELECT HSSTAXA.CCODITAXA,NQUANDGUI,NVALODGUI,NCOPRDGUI,CDESCTAXA
                       FROM HSSDGUI, HSSTAXA
                      WHERE HSSDGUI.NNUMEGUIA = :guia
                        AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                     UNION ALL
                     SELECT HSSPACOT.CCODIPACOT,NQUANPCGUI,NVALOPCGUI,NCOPAPCGUI,CDESCPACOT
                       FROM HSSPCGUI, HSSPACOT
                      WHERE HSSPCGUI.NNUMEGUIA = :guia
                        AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT
                     ORDER BY 1 ";
        $sql2 = new Query($bd);
        $sql2->addParam(":guia",$guia);
        $sql2->executeQuery($txt_proc);
        
        $proc = 0;
        $total_servicos = 0;
        $total_copart = 0;
        $total_prestador = 0;
          while (!$sql2->eof()) {    
            $pdf->Cell(10,5,$formata->acrescentaZeros($sql2->result("NQUANPGUI"),3),'LR',0,'L');
            $pdf->Cell(30,5,$sql2->result("CCODIPMED"),'LR',0,'L');
            $pdf->Cell(90,5,$pdf->Copy($sql2->result("CNOMEPMED"),89),'LR',0,'L');                                      
            
            $servico = $formata->formataNumero(str_replace(',','.',$sql2->result("NQINDPGUI")) + str_replace(',','.',$sql2->result("NCOPRPGUI")));	  
            $pdf->Cell(20,5,$servico,'LR',0,'R');
   
            $prestador = $formata->formataNumero(str_replace(',','.',$sql2->result("NQINDPGUI")));
            $pdf->Cell(20,5,str_replace(',','.',$prestador),'LR',0,'R');
           
            //$copart = 
            $pdf->Cell(20,5,$formata->formataNumero(str_replace(',','.',$sql2->result("NCOPRPGUI"))),'LR',1,'R');
                
       
            //$pdf->Cell(20,5,$formata->formataNumero($sql2->result("NQINDPGUI")),'LR',0,'R');
            $total_servicos += (str_replace(',','.',$sql2->result("NQINDPGUI")) + str_replace(',','.',$sql2->result("NCOPRPGUI")));
            $total_prestador += (str_replace(',','.',$sql2->result("NQINDPGUI")));
	          $total_copart += str_replace(',','.',$sql2->result("NCOPRPGUI"));

            $proc++;
            $sql2->next();

          }
        
          for ($i=0;$i<14-$proc;$i++) {
            $pdf->Cell(10,5,'','LR',0,'L');
            $pdf->Cell(30,5,'','LR',0,'L');
            $pdf->Cell(90,5,'','LR',0,'L');
            $pdf->Cell(20,5,'','LR',0,'L');
            $pdf->Cell(20,5,'','LR',0,'L');
            $pdf->Cell(20,5,'','LR',1,'L');      
          }
        
          $pdf->Cell(130,5,'Total (R$) ','T',0,'R');
          $pdf->Cell(20,5,$formata->formataNumero($total_servicos),'LBTR',0,'R');
          $pdf->Cell(20,5,$formata->formataNumero($total_prestador),'LBTR',0,'R');
          $pdf->Cell(20,5,$formata->formataNumero($total_copart),'LBTR',1,'R');    
        

        if ($qtde == 0) {
          $pdf->Ln(1);
          $pdf->Cell(40,5,' ',0,0,'R');
          $pdf->Cell(50,5,' ','B',1,'R');
      
          $pdf->Cell(40,3,' ',0,0,'R');
          $pdf->Cell(50,3,'Assinatura do paciente/Respons�vel',0,1,'C');

          $pdf->Cell(40,3,' ',0,0,'R');
          $pdf->Cell(50,3,'RG: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _',0,1,'C');
        }

        $qtde++;
      }  
    }
        /* Recibo da Multi Saude */
    function reciboweb($bd,$pdf,$guia) {  
      $pdf->AddPage('P');
      $formata = new Formata();
       
      $sql = new Query($bd);
      $txt = "SELECT HSSGUIA.NNUMEGUIA,USUA.CCODIUSUA||' - '|| USUA.CNOMEUSUA CNOMEUSUA, TITU.CNOMEUSUA TITULAR, CNOMEPRES,
                     TO_CHAR(DEMISGUIA,'DD/MM/YYYY') DEMISGUIA
                FROM HSSGUIA, HSSUSUA USUA, HSSUSUA TITU, FINPRES
               WHERE HSSGUIA.NNUMEGUIA = :guia
                 AND HSSGUIA.NUTILUSUA = USUA.NNUMEUSUA
                 AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                 AND HSSGUIA.NNUMEPRES = FINPRES.NNUMEPRES ";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);
      
      $qtde = 0;
      while ($qtde < 2) {  
      
        if ($qtde == 0) {
          $pdf->Ln(20);    
          $pdf->Image('../comum/img/logo_relatorio.jpg',10,5,40,25);
        } else {
          $pdf->Ln(10);    
          $pdf->Cell(190,1,str_repeat('-',270),0,1);    
          $pdf->Ln(30);
          $pdf->Image('../comum/img/logo_relatorio.jpg',10,155,40,25);
        }
        
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,3,'Usu�rio/Paciente:','',0,'L');
        $pdf->SetFont('Arial','B',8);   
        $pdf->Cell(160,5,$sql->result("CNOMEUSUA"),'',1,'L');
        
        $pdf->SetFont('Arial','',8);   
        $pdf->Cell(30,3,'Associado titular:','',0,'L');
        $pdf->SetFont('Arial','B',8);   
        $pdf->Cell(130,5,$sql->result("TITULAR"),'',0,'L');
        $pdf->SetFont('Arial','B',12);      
        $pdf->Cell(30,5,'Guia N� '.$formata->acrescentaZeros($sql->result("NNUMEGUIA"),6),'',1,'R');  
        
        $pdf->SetFont('Arial','',8);   
        $pdf->Cell(30,3,'M�dico/Prestador:','',0,'L');
        $pdf->SetFont('Arial','B',8);   
        $pdf->Cell(130,5,$sql->result("CNOMEPRES"),'',0,'L');
        $pdf->Cell(30,5,'Data: '.$sql->result("DEMISGUIA"),'',1,'R');  

        $pdf->SetFont('Arial','B',5);  
        
        if ($qtde == 0)
          $pdf->Cell(190,3,'Via prestador','',1,'R');
        else
          $pdf->Cell(190,3,'Via benefici�rio','',1,'R');
        
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(10,5,'Qtde',1,0,'L');
        $pdf->Cell(30,5,'C�digo',1,0,'L');
        $pdf->Cell(90,5,'Procedimento / Descri��o do servi�o',1,0,'L');
        $pdf->Cell(20,5,'Servi�o (R$)',1,0,'L');
        $pdf->Cell(20,5,'Prestador (R$)',1,0,'L');
        $pdf->Cell(20,5,'Co-particip. (R$)',1,1,'L');
      
        $txt_proc = "SELECT HSSPGUI.CCODIPMED,NQUANPGUI,NQINDPGUI,NCOPRPGUI,CNOMEPMED
                       FROM HSSPGUI,HSSPMED
                      WHERE HSSPGUI.NNUMEGUIA = :guia
                        AND CSTATPGUI IS NULL
                        AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                     UNION ALL
                     SELECT HSSTAXA.CCODITAXA,NQUANDGUI,NVALODGUI,NCOPRDGUI,CDESCTAXA
                       FROM HSSDGUI, HSSTAXA
                      WHERE HSSDGUI.NNUMEGUIA = :guia
                        AND HSSDGUI.NNUMETAXA = HSSTAXA.NNUMETAXA
                     UNION ALL
                     SELECT HSSPACOT.CCODIPACOT,NQUANPCGUI,NVALOPCGUI,NCOPAPCGUI,CDESCPACOT
                       FROM HSSPCGUI, HSSPACOT
                      WHERE HSSPCGUI.NNUMEGUIA = :guia
                        AND HSSPCGUI.NNUMEPACOT = HSSPACOT.NNUMEPACOT
                     ORDER BY 1 ";
        $sql2 = new Query($bd);
        $sql2->addParam(":guia",$guia);
        $sql2->executeQuery($txt_proc);
        
        $proc = 0;
        $total_servicos = 0;
        $total_copart = 0;
        $total_prestador = 0;
        while (!$sql2->eof()) {
          $pdf->Cell(10,5,$formata->acrescentaZeros($sql2->result("NQUANPGUI"),3),'LR',0,'L');
          $pdf->Cell(30,5,$sql2->result("CCODIPMED"),'LR',0,'L');
          $pdf->Cell(90,5,$pdf->Copy($sql2->result("CNOMEPMED"),89),'LR',0,'L');
          $pdf->Cell(20,5,$formata->formataNumero(str_replace(',','.',$sql2->result("NQINDPGUI")) + str_replace(',','.',$sql2->result("NCOPRPGUI"))),'LR',0,'R');
          $pdf->Cell(20,5,$formata->formataNumero($sql2->result("NQINDPGUI")),'LR',0,'R');
          $pdf->Cell(20,5,$formata->formataNumero($sql2->result("NCOPRPGUI")),'LR',1,'R');    
          
          $total_servicos += (str_replace(',','.',$sql2->result("NQINDPGUI")) + str_replace(',','.',$sql2->result("NCOPRPGUI")));
          $total_prestador += str_replace(',','.',$sql2->result("NQINDPGUI"));
          $total_copart += str_replace(',','.',$sql2->result("NCOPRPGUI"));
          
          $proc++;
          $sql2->next();
        }
        
        for ($i=0;$i<14-$proc;$i++) {
          $pdf->Cell(10,5,'','LR',0,'L');
          $pdf->Cell(30,5,'','LR',0,'L');
          $pdf->Cell(90,5,'','LR',0,'L');
          $pdf->Cell(20,5,'','LR',0,'L');
          $pdf->Cell(20,5,'','LR',0,'L');
          $pdf->Cell(20,5,'','LR',1,'L');      
        }
        
        $pdf->Cell(130,5,'Total (R$) ','T',0,'R');      
        $pdf->Cell(20,5,$formata->formataNumero($total_servicos),'LBTR',0,'R');
        $pdf->Cell(20,5,$formata->formataNumero($total_prestador),'LBTR',0,'R');
        $pdf->Cell(20,5,$formata->formataNumero($total_copart),'LBTR',1,'R');    
        

        if ($qtde == 0) {
          $pdf->Ln(1);
          $pdf->Cell(40,5,' ',0,0,'R');
          $pdf->Cell(50,5,' ','B',1,'R');
      
          $pdf->Cell(40,3,' ',0,0,'R');
          $pdf->Cell(50,3,'Assinatura do paciente/Respons�vel',0,1,'C');

          $pdf->Cell(40,3,' ',0,0,'R');
          $pdf->Cell(50,3,'RG: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _',0,1,'C');
        }

        $qtde++;
      }  
    }

    function retornaTelefoneUsuario ($bd,$usuario) {
      $formata = new Formata();
      $sql     = new Query($bd);
      
      $txt = "SELECT CBOLEUSUA,1 ORDEM,CFONETLUSU,NNUMETLUSU
                FROM HSSUSUA,HSSTLUSU
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NNUMEUSUA = HSSTLUSU.NNUMEUSUA
               UNION ALL
              SELECT HSSUSUA.CBOLEUSUA,2 ORDEM,CFONETLUSU,NNUMETLUSU
                FROM HSSUSUA,HSSUSUA TITULAR,HSSTLUSU
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
                 AND TITULAR.NNUMEUSUA = HSSTLUSU.NNUMEUSUA
               UNION ALL
              SELECT CBOLEUSUA,3 ORDEM,CFONETLCON,NNUMETLCON
                FROM HSSUSUA,HSSTLCON
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NNUMETITU = HSSTLCON.NNUMETITU
              ORDER BY 2,4 ";
      $sql->clear();
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);
      
      $telefone = '';
      while (!$sql->eof()) {
        $telefone .= $telefone.$formata->acrescentaBrancos($sql->result("CFONETLUSU"),15,'D',' ');
        $sql->next();
      }

      return $telefone;
    }

    function retornaTitularUsuario ($bd,$usuario) {
      $sql = new Query($bd);
      
      $txt = "SELECT USUA2.CNOMEUSUA 
                FROM HSSUSUA USUA1, HSSUSUA USUA2
               WHERE USUA1.NNUMEUSUA = :usuario
                 AND USUA1.NTITUUSUA = USUA2.NNUMEUSUA "; 

      $sql->clear();
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);  
      
      return $sql->result("CNOMEUSUA");
    }    
    
    function retornaCodigoEspecialidade($bd,$id) {
      $sql = new Query($bd);
      $txt = "SELECT CCODIESPEC FROM HSSESPEC WHERE NNUMEESPEC = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CCODIESPEC");
    }    
    
    function aniversario($usuario) {
      $idade = 0;
      $sql = new Query();
      $txt = "SELECT TO_CHAR(DNASCUSUA,'DDMM') MES,CNOMEUSUA,IDADE(DNASCUSUA,SYSDATE) IDADE
                FROM HSSUSUA
               WHERE NNUMEUSUA = :usuario";
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);
       
      if ($sql->result("MES") == '0811') {       
        $aniv = new Query(); 
        $txt = "SELECT COUNT(*) QTD
                   FROM HSSGUIA
                  WHERE (CLOCAGUIA = '5' OR CLOCAGUIA = '6') 
                    AND DEMISGUIA >= TRUNC(SYSDATE)
                    AND DEMISGUIA <  TRUNC(SYSDATE) + 1
                    AND NNUMEUSUA = :usuario";
        $aniv->addParam(":usuario",$usuario);
        $aniv->executeQuery($txt);        
        
        if ($aniv->result("QTD") == 0) {
          $idade = $sql->result("IDADE");
        }
      }      
      
      return $idade;
    }   

    function statusGuia ($status) {
      if ($status == 'A')
        return ('Sob auditoria');
      elseif ($status == 'C') 
        return ('Aguard. confirma��o');
      elseif ($status == 'P') 
        return ('Aguard. autoriza��o');
      elseif ($status == 'N') 
        return ('Negada');
      elseif ($status == 'X') 
        return ('Cancelada');
      elseif ($status == 'E') 
        return ('Cancelada na unimed origem');
      elseif ($status == 'B') 
        return ('Sob auditoria na unimed origem');
      elseif ($status == 'D') 
        return ('Negada na unimed origem');
      elseif ($status == '4') 
        return ('Aguard. resp. da unimed origem');
      else
        return ('Liberada');     
    }    
    
    function retornaCnpjCpfContrato($contrato,$usuario = 0,$locacao = 0) {
      $formata = new Formata();
      $sql = new Query();
      $txt = "SELECT HSSTITU.NNUMEEMPR,C_CGCEMPR,CBOLETITU
                FROM HSSTITU,HSSEMPR 
               WHERE HSSTITU.NNUMETITU = :id_contrato
                 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+) ";
      $sql->addParam(":id_contrato",$contrato);
      $sql->executeQuery($txt);  

      if ($locacao > 0) {
      //SQL especifico para loca��o  
        $sql1 = new Query();
        $txt1 = "SELECT NNUMESETOR, CCGC_SETOR
                   FROM HSSSETOR
                  WHERE NNUMESETOR = :id_locacao";
        $sql1->addParam(":id_locacao",$locacao);
        $sql1->executeQuery($txt1);

        return $formata->formataCNPJ($sql1->result("CCGC_SETOR"));    
      }
      else {  
        if(($usuario == 0) && ($sql->result("NNUMEEMPR") == NULL)) {
          $sql = new Query();  
          $txt = "SELECT TITU.C_CPFUSUA 
                    FROM HSSUSUA USUA,HSSUSUA TITU
                   WHERE USUA.NNUMETITU = :id_contrato
                     AND USUA.NTITUUSUA = TITU.NNUMEUSUA";
          $sql->addParam(":id_contrato",$contrato);
          $sql->executeQuery($txt); 
      
          return $formata->formataCPF($sql->result("C_CPFUSUA"));    
        }
        else if((( $sql->result("NNUMEEMPR") == NULL)) || (($sql->result("CBOLETITU") == "S") && ($usuario > 0)) ){ 
          $sql = new Query();  
          $txt = "SELECT TITU.C_CPFUSUA 
                    FROM HSSUSUA USUA,HSSUSUA TITU
                   WHERE USUA.NNUMEUSUA = :usuario
                     AND USUA.NTITUUSUA = TITU.NNUMEUSUA ";
          $sql->addParam(":usuario",$usuario);
          $sql->executeQuery($txt); 
          
          return  $formata->formataCPF($sql->result("C_CPFUSUA"));
        } 
        else
          return  $formata->formataCNPJ($sql->result("C_CGCEMPR"));
      }    
    }

    function retornaEndereco1Locacao($locacao = 0) {

        $sql = new Query();
        $txt = "SELECT CENDESETOR,CBAIRSETOR
                  FROM HSSSETOR 
                 WHERE HSSSETOR.NNUMESETOR = :id_locacao";
        $sql->addParam(":id_locacao",$locacao);
        $sql->executeQuery($txt); 
        
        return     $sql->result("CENDESETOR")." - ".$sql->result("CBAIRSETOR");
    }

    function retornaEndereco2Locacao($locacao = 0) {

        $sql = new Query();
        $txt = "SELECT CCIDASETOR,CESTASETOR,CCEP_SETOR
                  FROM HSSSETOR 
                 WHERE HSSSETOR.NNUMESETOR = :id_locacao";
        $sql->addParam(":id_locacao",$locacao);
        $sql->executeQuery($txt); 
        
        return     $sql->result("CCIDASETOR")." - ".$sql->result("CESTASETOR")." - ".$sql->result("CCEP_SETOR");
    }        
    
    function statusCarteirinha($bd,$usuario) {
      $sql = new Query($bd);
      $sql->clear();   
      $txt = "SELECT NNUMECAWEB,CSITUCAWEB,DDIGICAWEB
                FROM HSSCAWEB
               WHERE NNUMEUSUA = :usuario 
                 AND CSITUCAWEB IN ('P','I') ";
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0) {
        if ($sql->result("CSITUCAWEB") == 'I')   
          return "Selecionado";
        else
          return "Aguardando confirma&ccedil;&atilde;o.";    
      } else {
        $sql->clear();   
        $txt = "SELECT HSSCARTE.NNUMEUSUA 
                  FROM HSSCARTE,HSSRCART 
                 WHERE HSSCARTE.NNUMEUSUA = :usuario
                   AND HSSCARTE.NNUMERCART = HSSRCART.NNUMERCART 
                   AND DGERARCART IS NULL";
        $sql->addParam(":usuario",$usuario);
        $sql->executeQuery($txt); 
      
        if ($sql->count() > 0) {
          return "Aguardando Impress&atilde;o";
        } else {
          $txt = "SELECT CSITUCAWEB
                    FROM HSSCAWEB
                   WHERE NNUMEUSUA = :usuario 
                     AND CSITUCAWEB = 'C'
                     AND DDIGICAWEB > (SYSDATE - 30) ";
          $sql->addParam(":usuario",$usuario);
          $sql->executeQuery($txt); 

          if ($sql->count() > 0) {
            return "Solicitado a menos de 30 dias.";
          } else {
            return "";
          }
        }    
      }
    }

    function tipoPrestador ($tipo) {
      $sql = new Query($bd);  
      $txt = "SELECT RETORNA_NOME_GRUPO_PRESTADOR(:tipo) GRUPO FROM DUAL";
      $sql->addParam(":tipo",$tipo);
      $sql->executeQuery($txt);

      return $sql->result("GRUPO");   
    }   

    function nomeEstado($estado) {
      if ($estado == "AC") return "Acre";
      else if ($estado == "AM") return "Amazonas";
      else if ($estado == "AL") return "Alagoas";
      else if ($estado == "AP") return "Amap&aacute;";
      else if ($estado == "BA") return "Bahia";
      else if ($estado == "CE") return "Cear&aacute;";
      else if ($estado == "DF") return "Distrito Federal";
      else if ($estado == "ES") return "Esp&iacute;rito Santo";
      else if ($estado == "GO") return "Goi&aacute;s";
      else if ($estado == "MA") return "Maranh&atilde;o";
      else if ($estado == "MT") return "Mato Grosso";
      else if ($estado == "MS") return "Mato Grosso do sul";
      else if ($estado == "MG") return "Minas Gerais";
      else if ($estado == "PA") return "Par&aacute;";
      else if ($estado == "PR") return "Paran&aacute;";
      else if ($estado == "PB") return "Paraiba";
      else if ($estado == "PE") return "Pernambuco";
      else if ($estado == "PI") return "Piau&iacute;";
      else if ($estado == "RJ") return "Rio de Janeiro";
      else if ($estado == "RN") return "Rio Grande do Norte";
      else if ($estado == "RS") return "Rio Grande do Sul";
      else if ($estado == "RO") return "Rond&ocirc;nia";
      else if ($estado == "RR") return "Roraima";
      else if ($estado == "SC") return "Santa Catarina";
      else if ($estado == "SP") return "S&atilde;o Paulo";
      else if ($estado == "SE") return "Sergipe";  
      else if ($estado == "TO") return "Tocantins";
      else return "";
    }  
      
    function retornaNomeGraduacao($graduacao){
      if ($graduacao == "AP")      return "APALC - Padr�o nacional de qualidade";
      else if ($graduacao == "AD") return "ADICQ - Padr�o nacional de qualidade";
      else if ($graduacao == "AO") return "AONA - Padr�o nacional de qualidade";
      else if ($graduacao == "AC") return "ACBA - Padr�o internacional de qualidade";
      else if ($graduacao == "AI") return "AIQG - Padr�o internacional de qualidade";
      else if ($graduacao == "N" ) return "N - Comunica��o de eventos adversos";
      else if ($graduacao == "P" ) return "P - Profissional com especializa��o";
      else if ($graduacao == "R" ) return "R - Profissional com resid�ncia";
      else if ($graduacao == "E" ) return "E - T�tulo de especialista";
      else if ($graduacao == "Q" ) return "Q - Qualidade monitorada";
      else return "";
    }
    
    function retornaNomeEspecialidade($bd,$id) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CNOMEESPEC FROM HSSESPEC WHERE NNUMEESPEC = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CNOMEESPEC");
    }    
    
    function retornaNomeArea($bd,$id) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CDESCAATUA FROM HSSAATUA WHERE NNUMEAATUA = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CDESCAATUA");
    } 

    function simNao($variavel,$padrao) {
      if ($variavel == "N")
        return "N�o";
      else if ($variavel == "S")
        return "Sim";
      else
        return $padrao;  
    }    
    
    function retornaCpfTitular ($bd,$usuario) {
      $sql = new Query($bd);
      
      $txt = "SELECT TITU.C_CPFUSUA C_CPFUSUA
                FROM HSSUSUA USUA, HSSUSUA TITU
               WHERE USUA.NNUMEUSUA = :usuario
                 AND USUA.NTITUUSUA = TITU.NNUMEUSUA"; 

      $sql->clear();
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);  
      
      return $sql->result("C_CPFUSUA"); 
    }    
    
    function retornaCpfUsuario ($bd,$usuario) {
      $sql = new Query($bd);
      
      $txt = "SELECT C_CPFUSUA
                FROM HSSUSUA
               WHERE NNUMEUSUA = :usuario"; 

      $sql->clear();
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);  
      
      return $sql->result("C_CPFUSUA"); 
    }       
    
    function insereUsuario ($bd,$id_contrato,$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,$titular,
                             $matricula,$salario,$admissao,$locacao,$plano,$acomodacao,
                             $telefone1,$telefone2,$celular,$email,
                             $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,$inclusao,
                             $estado_civil,$profissao,$setor,$local,$vendedor,$id_proposta,$valor,$estabelecimento,$anexo,$csus,$ndnv,$observacao,$uniao,
                             $aditivos,$centro_custo,$atcadpai,$peso,$altura,$id_operadora_repasse,$cidade_atendimento) {
                                        
      // Local E = Empresa C = Comercial  
      
      if (isset($anexo['tmp_name']))
        $tamanhoArquivo = filesize($anexo['tmp_name'])/1024;
      else
        $tamanhoArquivo = 0;

      if ($tamanhoArquivo <= $this->convertBytes(ini_get("upload_max_filesize"))) {
        
        $formata = new Formata();
        $seg     = new Seguranca();
        $util    = new Util();
        $data    = new Data();
        
        $cpf  = $formata->somenteNumeros($cpf);
        $pis  = $formata->somenteNumeros($pis);
        $cep  = $formata->somenteNumeros($cep);
        $csus = $formata->somenteNumeros($csus);
        $ndnv = $formata->somenteNumeros($ndnv);
        
        if ($categoria == 'T' or $categoria == 'F')
          $grau = 'X';
          
        $sqlcomp = new Query($bd);
        
        $retorno = array();
        $retorno['erro'] = '';
 
        $anoinclusao = substr($inclusao,7,4);
        $mesinclusao = substr($inclusao,4,2);
        
        $txt = "SELECT DCOBRCOMPE FROM HSSCOMPE
                 WHERE CCOMPCOMPE = :competencia
                   AND DCOBRCOMPE IS NOT NULL";
               
        $sqlcomp->clear();
        $sqlcomp->addParam(":competencia",substr($inclusao,3,2).'/'.substr($inclusao,6,4));
        $sqlcomp->executeQuery($txt);
        
        $sql = new Query($bd);
        
        $txt = "SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
                 WHERE C_CPFUSUA = :cpf
                   AND CTIPOUSUA = :categoria
                   AND CSITUUSUA = 'A'
                   AND NNUMETITU = :contrato
                 UNION ALL
                SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
                 WHERE C_CPFUSUA = :cpf
                   AND NTITUUSUA = :titular
                   AND CSITUUSUA = 'A'
                   AND NNUMETITU = :contrato           
                 UNION ALL
                SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                 WHERE C_CPFATCAD = :cpf
                   AND CTIPOATCAD = :categoria
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I'
                   AND NNUMETITU = :contrato
                 UNION ALL
                SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                 WHERE C_CPFATCAD = :cpf
                   AND NTITUUSUA = :titular
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I'
                   AND NNUMETITU = :contrato             
                 UNION ALL
                SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                 WHERE C_CPFATCAD = :cpf
                   AND CTIPOATCAD = :categoria
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I'
                   AND NNUMEPPROP = :proposta
                 UNION ALL
                SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                 WHERE C_CPFATCAD = :cpf
                   AND NTITUUSUA = :titular
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I'
                   AND NNUMEPPROP = :proposta ";
                  
        $sql->clear();
        $sql->addParam(":cpf",$cpf);
        $sql->addParam(":categoria",$categoria);
        $sql->addParam(":titular",$titular);
        $sql->addParam(":contrato",$id_contrato);
        $sql->addParam(":proposta",$id_proposta);
                
        $sql->executeQuery($txt);              
        
        $duplicidade_cpf = $seg->permissaoOutros($bd,"WEBEMPRESACADASTRARCPFDUPLICADOPCMSO",false);
                      
        $sqlstat = new Query($bd);        
        $txt = "SELECT HSSSTAT.NNUMESTAT,CDESCSTAT FROM HSSSTAT,HSSTITU
                 WHERE CIDIGSTAT = 'S'
                   AND NNUMETITU = :contrato
                   AND HSSTITU.NNUMESTAT = HSSSTAT.NNUMESTAT";
        $sqlstat->clear();
        $sqlstat->addParam(":contrato",$id_contrato);
        $sqlstat->executeQuery($txt);
        
        if ($sqlstat->count() > 0)
          $retorno['erro'] = "N�o � poss�vel realizar a inclus�o de benefici�rios. Status: (".$sqlstat->result("CDESCSTAT").")"; 
        else if ( ($sql->count() > 0) && ( $duplicidade_cpf == false ) )
          $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio. CPF em duplicidade! (".$sql->result("CNOMEUSUA").").";
        else if ($sqlcomp->count() > 0)
          $retorno['erro'] = "A compet�ncia para esta data de inclus�o est� fechada! (".$inclusao.").";
        else if (!$util->validaCNS($csus))
          $retorno['erro'] = 'N�mero do Cart�o do SUS inv�lido.';                
        else if ($estado_civil == 'C' and $grau == 'F')
          $retorno['erro'] = 'N�o � permitido a inclus�o de filho casado como dependente.'; 
        else {
          $txt = "SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
                   WHERE (CNOMEUSUA = :nome AND TO_CHAR(DNASCUSUA,'DD/MM/YYYY') = :nascimento)
                     AND CTIPOUSUA = :categoria
                     AND CSITUUSUA = 'A'
                     AND NNUMETITU = :contrato
                   UNION ALL
                  SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                   WHERE (CNOMEATCAD = :nome AND TO_CHAR(DNASCATCAD,'DD/MM/YYYY') = :nascimento)
                     AND CTIPOATCAD = :categoria
                     AND CFLAGATCAD IS NULL
                     AND COPERATCAD = 'I'
                     AND NNUMETITU = :contrato 
                   UNION ALL
                  SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                   WHERE (CNOMEATCAD = :nome AND TO_CHAR(DNASCATCAD,'DD/MM/YYYY') = :nascimento)
                     AND CTIPOATCAD = :categoria
                     AND CFLAGATCAD IS NULL
                     AND COPERATCAD = 'I'
                     AND NNUMEPPROP = :proposta ";               
                     
          $sql->clear();
          $sql->addParam(":nome",$nome);
          $sql->addParam(":nascimento",$nascimento);    
          $sql->addParam(":categoria",$categoria);
          $sql->addParam(":contrato",$id_contrato);
          $sql->addParam(":proposta",$id_proposta);    
          $sql->executeQuery($txt); 

             
          if ($sql->count() > 0) {
            $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio. Cadastro em duplicidade! (".$sql->result("CNOMEUSUA").").";
          } else { 
            if (($categoria == 'T') or ($categoria == 'F')) {
              if ($locacao > 0)
              $txt1 = " AND  NNUMESETOR = :id_locacao";
              else  
                $txt1 = ""; 

              $txt = "SELECT NNUMEUSUA FROM HSSUSUA
                       WHERE CTIPOUSUA = 'T'
                         AND CSITUUSUA = 'A'
                         AND NNUMETITU = :contrato
                         AND CCHAPUSUA = :matricula".
               $txt1.
                     "  UNION ALL
                      SELECT NNUMEUSUA FROM HSSATCAD
                       WHERE CTIPOATCAD = 'T'
                         AND CFLAGATCAD IS NULL
                         AND COPERATCAD = 'I'
                         AND NNUMETITU = :contrato
                         AND CCHAPATCAD = :matricula".
               $txt1.
                     " UNION ALL
                      SELECT NNUMEUSUA FROM HSSATCAD
                       WHERE CTIPOATCAD = 'T'
                         AND CFLAGATCAD IS NULL
                         AND COPERATCAD = 'I'
                         AND NNUMEPPROP = :proposta
                         AND CCHAPATCAD = :matricula".
               $txt1;
                         
              $sql->clear();
              $sql->addParam(":matricula",$matricula);
              $sql->addParam(":contrato",$id_contrato);
              $sql->addParam(":proposta",$id_proposta);
              if ($locacao > 0)
                $sql->addParam(":id_locacao",$locacao);     
              $sql->executeQuery($txt); 
                   
              if ($sql->count() > 0) {
                $continua = false;
              } else {
                $continua = true;
              }
            } else {
              $continua = true;
            }
            
            if (!$continua) {
              $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, pois j� existe um usu�rio com a mesma matr�cula.";
            } else {  
              $txt = "SELECT HSSTITU.NNUMETITU
                        FROM HSSTITU,HSSPLAN
                       WHERE HSSTITU.NNUMETITU = :contrato
                         AND (
                              (:categoria = 'D' AND :sexo = 'M' AND :grau <> 'E' AND :grau <> 'J' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMMTITU,NVL(NLDEMPLAN,999))) OR
                              (:categoria = 'D' AND :sexo = 'F' AND :grau <> 'E' AND :grau <> 'J' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMFTITU,NVL(NLDEFPLAN,999))) OR
                              (:categoria = 'U' AND :sexo = 'M' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMUTITU,NVL(NLDEUPLAN,999))) OR
                              (:categoria = 'U' AND :sexo = 'F' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIUFTITU,NVL(NLDUFPLAN,999))) OR
                              (:categoria = 'A' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMATITU,NVL(NLAGRPLAN,999)))
                              )
                         AND HSSPLAN.NNUMEPLAN = :plano
                         AND NVL(DLIMIPLAN,SYSDATE) <= SYSDATE ";
                         
              $sql->clear();
              $sql->addParam(":categoria",$categoria);
              $sql->addParam(":sexo",$sexo);
              $sql->addParam(":grau",$grau);
              $sql->addParam(":nascimento",$nascimento);        
              $sql->addParam(":plano",$plano);         
              $sql->addParam(":contrato",$id_contrato);
              $sql->executeQuery($txt); 

              if ($sql->count() > 0) {
                $retorno['erro'] = "Este usu�rio ultrapassa a idade limite para este contrato.<br />
                                    Verifique os campos de data de nascimento, categoria do usu�rio, grau de parentesco e sexo.";
                $continua = false;                    
              } 
          
              if ($estabelecimento == 2){  
                $txt = " SELECT COUNT(1) EXISTE FROM HSSATCAD
                          WHERE CTIPOATCAD IN ('T','F') 
                            AND NNUMEPPROP = :proposta " ;
                $sql->clear();
                $sql->addParam(":proposta",31294539);
                $sql->executeQuery($txt);
            
                if($sql->count() > 0){
                  $retorno['erro'] = "J� existe um titular para esta proposta.";
                  $continua = false;
                } else
                  $continua = true;                                 
              } else {            
                if (($continua) and (($grau == "E") or ($grau == "J"))) {        
                  $txt = "SELECT COUNT(1) QTDE
                            FROM (SELECT NNUMEUSUA FROM HSSUSUA
                                   WHERE CSITUUSUA = 'A'
                                     AND CGRAUUSUA IN ('E','J')
                                     AND NTITUUSUA = :titular 
                                   UNION
                                  SELECT NNUMEUSUA FROM HSSATCAD
                                   WHERE CFLAGATCAD IS NULL
                                     AND COPERATCAD = 'I'  
                                     AND CGRAUATCAD IN ('E','J')
                                     AND NTITUUSUA = :titular) ";
                             
                  $sql->clear();
                  $sql->addParam(":titular",$titular);
                  $sql->executeQuery($txt);   

                  if ($sql->result("QTDE") > 0) {
                    $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, pois j� existe um dependente com o grau de parentesco \"Esposa(o)\".";
                    $continua = false;
                  } else {   
                    $continua = true;
                  }
                }

                if (($continua) and ($categoria == 'T' or $categoria == 'F') and ($id_proposta > 0)) {
                
                  $sql->clear();
                  $txt = "SELECT CPESSPROSP NJ FROM HSSPPROP,HSSPROPO,HSSPROSP
                           WHERE HSSPPROP.NNUMEPPROP = :proposta
                             AND HSSPPROP.NNUMEPROPO = HSSPROPO.NNUMEPROPO
                             AND HSSPROPO.NNUMEPROSP = HSSPROSP.NNUMEPROSP ";
                              
                  $sql->addParam(":proposta",$id_proposta);          
                  $sql->executeQuery($txt);   

                  if ($sql->result("NJ") == "F") {
                  
                    $txt = "SELECT COUNT(1) QTDE
                              FROM (SELECT NNUMEUSUA FROM HSSATCAD
                                     WHERE CFLAGATCAD IS NULL
                                       AND COPERATCAD = 'I'  
                                       AND CTIPOATCAD IN ('T','F')
                                       AND NNUMEPPROP = :proposta) ";
                               
                    $sql->clear();
                    $sql->addParam(":proposta",$id_proposta);
                    $erro = $sql->executeQuery($txt);
                    
                    if ($erro <> '') {
                      $retorno['erro'] = $erro;
                      $continua = false;
                    } else if ($sql->result("QTDE") > 0) {
                      $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, pois j� existe um titular cadastrado.";
                      $continua = false;
                    } else
                      $continua = true;
                  } else
                    $continua = true;
                }  
                
                if ( (($id_contrato == '') or ($id_contrato == 0)) and (($id_proposta == '') or ($id_proposta == 0)) ) {
                  $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, o contrato n�o est� selecionado.";
                  $continua = false; 
                }                   
                    
                if (($continua) and ($retorno['erro'] == '')) {
                  $sql->clear();          
                  $txt = "BEGIN ".
                         "   INSERE_USUARIO_WEB30(:contrato,:titular,:nome,:categoria,TO_DATE(:nascimento,'DD/MM/YYYY'),:estadonasc,:cidadenasc,:sexo,:matricula, ".
                         "                     :mae,:pai,:cpf,:rg,:orgao,:pis,:grau,:locacao,TO_DATE(:admissao,'DD/MM/YYYY'),:endereco,:bairro, ".
                         "                     :cidade,:estado,:cep,:plano,:acomodacao,:telefone1,to_date(:expedicao,'DD/MM/YYYY'),:numero, ".
                         "                     :tipo_logradouro,:complemento,:telefone2,:celular,:email,:salario,:inclusao,:estado_civil, ".
                         "                     :profissao,:setor,:local,:vendedor,:operador,:proposta,:valor,:csus,:ndnv,:observacao,:uniao,".
                         "                     :centro_custo,:atcadpai,:peso,:altura,:ip,:id_usuario,:retorno,:id_operadora_repasse,:cidade_atendimento); ".
                         "END; ";   

                  $sql->addParam(":contrato",$id_contrato);
                  $sql->addParam(":titular",$titular);
                  $sql->addParam(":nome",$nome);
                  $sql->addParam(":categoria",$categoria);
                  $sql->addParam(":nascimento",$nascimento);
                  $sql->addParam(":estadonasc",$estadonasc);
                  $sql->addParam(":cidadenasc",$cidadenasc);
                  $sql->addParam(":sexo",$sexo);
                  $sql->addParam(":matricula",$matricula);
                  $sql->addParam(":mae",$mae);
                  $sql->addParam(":pai",$pai);
                  $sql->addParam(":cpf",$cpf);
                  $sql->addParam(":rg",$rg);
                  $sql->addParam(":orgao",$orgao);
                  $sql->addParam(":pis",$pis);
                  $sql->addParam(":grau",$grau);
                  $sql->addParam(":locacao",$locacao);
                  $sql->addParam(":admissao",(($admissao <>'')?$admissao:NULL));
                  $sql->addParam(":endereco",$logradouro);
                  $sql->addParam(":bairro",$bairro);
                  $sql->addParam(":cidade",$cidade);
                  $sql->addParam(":estado",$estado);
                  $sql->addParam(":cep",$cep);
                  $sql->addParam(":plano",$plano);
                  $sql->addParam(":acomodacao",$acomodacao);            
                  $sql->addParam(":telefone1",$telefone1);            
                  $sql->addParam(":expedicao",$expedicao);            
                  $sql->addParam(":numero",$numero);            
                  $sql->addParam(":tipo_logradouro",$tipo_logradouro);            
                  $sql->addParam(":complemento",$complemento);            
                  $sql->addParam(":telefone2",$telefone2);            
                  $sql->addParam(":celular",$celular);            
                  $sql->addParam(":email",$email);            
                  $sql->addParam(":salario",$salario);            
                  $sql->addParam(":inclusao",$inclusao);
                  $sql->addParam(":estado_civil",$estado_civil);
                  $sql->addParam(":profissao",$profissao);
                  $sql->addParam(":id_operadora_repasse",$id_operadora_repasse);
                  $sql->addParam(":setor",$setor);               
                  $sql->addParam(":local",$local);
                  $sql->addParam(":vendedor",$vendedor);    
                  $sql->addParam(":peso",$peso);
                  $sql->addParam(":altura",$altura);   
                  $sql->addParam(":cidade_atendimento",$cidade_atendimento);

                  if ($_SESSION['sistema'] <> 'Usuario')
                    $sql->addParam(":operador",$_SESSION['id_operador']);              
                  else
                    $sql->addParam(":operador",'');              
                    
                  $sql->addParam(":proposta",$id_proposta);              
                  $sql->addParam(":valor",$valor);              
                  $sql->addParam(":id_usuario",0,12);
                  $sql->addParam(":csus",$csus);
                  $sql->addParam(":ndnv",$ndnv);              
                  $sql->addParam(":observacao",$observacao);
                  $sql->addParam(":uniao",$uniao);
                  $sql->addParam(":centro_custo",$centro_custo);
                  $sql->addParam(":atcadpai",$atcadpai);
                  $sql->addParam(":ip",getenv("REMOTE_ADDR"));
                  $sql->addParam(":retorno",0,12);
                  $erro = $sql->executeSQL($txt);

                  $retorno['erro'] = $erro;
                  $id_atcad = $sql->getReturn(":retorno");
                  $retorno['id'] = $id_atcad;
                  $id_usuario = $sql->getReturn(":id_usuario");
                  $retorno['id_usuario'] = $id_usuario;

                  //Verifica��o de erros retornados do INSERE_USUARIO_WEB30
                  if($id_atcad == -1)
                    $retorno['erro']='N�o � poss�vel cadastrar usuario com nome vazio';
                  if($id_atcad == -2)
                    $retorno['erro']='n�o � poss�vel cadastrar usuario sem data de nascimento';
                  if($id_atcad == -3)
                    $retorno['erro']='N�o � poss�vel cadastrar usuario sem sexo';
                  if($id_atcad == -4)
                    $retorno['erro']='N�o � poss�vel cadastrar usuario sem o nome da m�e ou pis';
                  
                  if ($retorno['erro'] == '') {
                    //aditivos
                    if (sizeOf($aditivos) > 0) {
                      foreach($aditivos as $aditivo1) {
                        $txt = "INSERT INTO HSSADCAD (NNUMEATCAD,NNUMETXMEN)
                                              VALUES (:id_atcad,:id_aditivo)";
                        $sql->clear();
                        $sql->addParam(":id_atcad",$id_atcad);
                        $sql->addParam(":id_aditivo",$aditivo1);
                        $sql->executeSQL($txt);                
                      }
                    }
                          
                    //anexo 
                    if (isset($anexo["name"])) {
                      $path = getcwd();    
                      $util->criaDiretorio("anexos");
                      $util->criaDiretorio($data->dataAtual('YYYY'));
                      $util->criaDiretorio($_SESSION['codigo_contrato'] );  
                      $dir = getcwd()."/";                
                                          
                      $nome_arquivo = $anexo["name"];     
                      $nome_arquivo = $util->corrigeNomeArquivo($nome_arquivo);
                      
                      $file =  str_replace('//','/',$dir).$id_usuario. "_".$nome_arquivo;

                      if (move_uploaded_file( $anexo["tmp_name"], $file )) {
                    
                        $txt = "INSERT INTO HSSANEXO (NNUMEUSUA,CNOMEANEXO,DDATAANEXO,NSIZEANEXO)
                                              VALUES (:id_usuario,:novoarquivo,SYSDATE,:tamanho)";
                        $sql->clear();
                        $sql->addParam(":id_usuario",$id_usuario);
                        $sql->addParam(":novoarquivo",$id_usuario."_".$nome_arquivo);
                        $sql->addParam(":tamanho",filesize($file));
                        $sql->executeSQL($txt);
                      }
                      
                      if ($path <> '')
                        chdir($path);                  
                    }
                  }
                }
              }        
            }
          }
        }
      } else
        $retorno['erro'] = 'O tamanho do arquivo n�o pode ser superior a '.ini_get("upload_max_filesize") . '.';
          
      
      return $retorno;
    }   

    function alteraUsuario ($bd,$id_contrato,$operacao,$id,$nome,$categoria,$nascimento,$estadonasc,$cidadenasc,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,
                            $matricula,$salario,$admissao,$demissao,$locacao,$plano,$acomodacao,
                            $telefone1,$telefone2,$celular,$email,
                            $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,
                            $ini_afast,$fim_afast,$motivo,$motivo_canc,$data_canc,$estado_civil,$profissao,
                            $setor,$local,$vendedor,$anexo,$csus,$ndnv,$uniao,$centro_custo,$cidadeatendimento='',$idatcad=0,$re=0) {
                             
      // Local E = Empresa C = Comercial H = Estrutural
      
      if (isset($anexo['tmp_name']))
        $tamanhoArquivo = filesize($anexo['tmp_name'])/1024;
      else
        $tamanhoArquivo = 0;
        
      if ($tamanhoArquivo <= $this->convertBytes(ini_get("upload_max_filesize"))) {
        
        $formata = new Formata();
        $seg     = new Seguranca();
        $util    = new Util();
        $data    = new Data();
        
        $sql = new Query($bd);
        $txt = "SELECT NNUMETITU
                  FROM HSSUSUA
                 WHERE NNUMEUSUA = :id_usuario";
        $sql->addParam(":id_usuario",$id);
        $sql->executeQuery($txt);     
      
        $id_titular = $sql->result("NNUMETITU");

        $csus= $formata->somenteNumeros($csus);
                
        if (($motivo_canc == '') and ($operacao == 'C'))
          $retorno['erro'] = "E necessario informar o motivo do cancelamento";
        else if (!$util->validaCNS($csus))
          $retorno['erro'] = "N�mero do Cart�o do SUS inv�lido.";        
        else if ($id_titular == $_SESSION['id_contrato']) {      
          //Veririfica se o contrato logado � o mesmo do contrato do id do benefici�rio que esta sendo alterado
         if (($_SESSION['apelido_operadora'] == 'agemed')){
            $sql = new Query($bd);
            $txt = "SELECT CWEFCPLAN
                FROM hssplan
                WHERE NNUMEPLAN=:plano";
              $sql->addParam(":plano",$plano);
              $sql->executeQuery($txt);     
            
             if(($sql->result("CWEFCPLAN") ==  'S') and ($categoria == 'T')){
              $retorno['erro'] = "E necess�rio anexar o formul�rio de cancelamento";
              return $retorno;
             }  
         }

      
          $sql = new Query($bd);
          $sql->clear();  

          $txt = "BEGIN ".
                 "  INSERE_ALTERACAO_WEB30(:operacao,:usuario,:operador,:nome,:categoria,to_date(:nascimento,'DD/MM/YYYY'),:estadonasc,:cidadenasc,:sexo,:matricula,:mae,:pai,".
                 "                        :cpf,:rg,:orgao,:pis,:grau,:locacao,to_date(:admissao,'DD/MM/YYYY'),:endereco,:bairro,".
                 "                        :cidade,:estado,:cep,to_date(:ini_afast,'DD/MM/YYYY'),to_date(:fim_afast,'DD/MM/YYYY'),".
                 "                        :motivo,:plano,:acomodacao,to_date(:expedicao,'DD/MM/YYYY'),:numero,:tipo_logradouro,:complemento,".
                 "                        :email,:telefone,:salario,:motivo_canc,:data_canc,:estado_civil,:profissao,:setor,:local,:vendedor,".
                 "                        :demissao,:csus,:ndnv,:idatcad,:re,:retorno1,to_date(:uniao,'DD/MM/YYYY'),:centro_custo,:ip,".
                 "                        :peso,:altura,:cidadeatendimento); ".
                 "END;";

          $cpf = $formata->somenteNumeros($cpf);
          $pis = $formata->somenteNumeros($pis);
          $cep = $formata->somenteNumeros($cep); 
          $ndnv= $formata->somenteNumeros($ndnv);  

          $sql->addParam(":operacao",$operacao);  
          $sql->addParam(":usuario",$id);
          
          if ($_SESSION['sistema'] <> 'Usuario')
            $sql->addParam(":operador",$_SESSION['id_operador']);              
          else
            $sql->addParam(":operador",'');             
            
          $sql->addParam(":nome",$nome);
          $sql->addParam(":categoria",$categoria);
          $sql->addParam(":nascimento",$nascimento);
          $sql->addParam(":estadonasc",$estadonasc);
          $sql->addParam(":cidadenasc",$cidadenasc);
          $sql->addParam(":sexo",$sexo);
          $sql->addParam(":matricula",$matricula);
          $sql->addParam(":mae",$mae);
          $sql->addParam(":pai",$pai);
          $sql->addParam(":cpf",$cpf);
          $sql->addParam(":rg",$rg);
          $sql->addParam(":orgao",$orgao);
          $sql->addParam(":pis",$pis);
          $sql->addParam(":grau",$grau);
          $sql->addParam(":locacao",$locacao);
          $sql->addParam(":admissao",$admissao);         
          $sql->addParam(":demissao",$demissao);          
          $sql->addParam(":endereco",$logradouro);
          $sql->addParam(":bairro",$bairro);
          $sql->addParam(":cidade",$cidade);
          $sql->addParam(":estado",$estado);
          $sql->addParam(":cep",$cep);
          $sql->addParam(":plano",$plano);
          $sql->addParam(":acomodacao",$acomodacao); 
          $sql->addParam(":telefone",$telefone1);          
          $sql->addParam(":expedicao",$expedicao);  
          $sql->addParam(":numero",$numero);
          $sql->addParam(":tipo_logradouro",$tipo_logradouro);
          $sql->addParam(":complemento",$complemento);                
          $sql->addParam(":email",$email);  
          $sql->addParam(":salario",$salario);                 
          $sql->addParam(":ini_afast",$ini_afast);            
          $sql->addParam(":fim_afast",$fim_afast);
          $sql->addParam(":motivo",$motivo); 
          $sql->addParam(":motivo_canc",$motivo_canc);
          $sql->addParam(":data_canc",$data_canc);  
          $sql->addParam(":estado_civil",$estado_civil);
          $sql->addParam(":profissao",$profissao);
          $sql->addParam(":setor",$setor);    
          $sql->addParam(":local",$local);   
          $sql->addParam(":vendedor",$vendedor);
          $sql->addParam(":csus",$csus);
          $sql->addParam(":ndnv",$ndnv);
          $sql->addParam(":idatcad",$idatcad);
          $sql->addParam(":re",$re);
          $sql->addParam(":retorno1",0,12);
          $sql->addParam(":uniao",$uniao);
          $sql->addParam(":centro_custo",$centro_custo);
          $sql->addParam(":ip",getenv("REMOTE_ADDR"));      
          $sql->addParam(":peso",'');      
          $sql->addParam(":altura",'');
          $sql->addParam(":cidadeatendimento",$cidadeatendimento);

          $erro = $sql->executeSQL($txt);  
          $retorno['erro'] = $erro;
          $ver_erro = $sql->getReturn(":retorno1");
          $id_usuario = $id;
          
          //Verifica��o de erros retornados do INSERE_USUARIO_WEB30
          if($ver_erro == -1)
            $retorno['erro']='N�o � poss�vel cadastrar usuario com nome vazio';
          if(($ver_erro == -3) and ($operacao != 'C'))
            $retorno['erro']='N�o � poss�vel cadastrar usuario sem sexo';
          if($ver_erro == -4)
            $retorno['erro']='N�o � poss�vel cadastrar usuario sem o nome da m�e ou pis';
          
          if ($retorno['erro'] == '') {
            //anexo 
            if (isset($anexo["name"])) {
              $path = getcwd();    
              $util->criaDiretorio("anexos");
              $util->criaDiretorio($data->dataAtual('YYYY'));
              $util->criaDiretorio($_SESSION['codigo_contrato'] );  
              $dir = getcwd()."/";                
                                  
              $nome_arquivo = $anexo["name"];     
              $nome_arquivo = $util->corrigeNomeArquivo($nome_arquivo);
              
              $file =  str_replace('//','/',$dir).$id_usuario. "_".$nome_arquivo;
                                           
              if (move_uploaded_file( $anexo["tmp_name"], $file )) {
            
                $txt = "INSERT INTO HSSANEXO (NNUMEUSUA,CNOMEANEXO,DDATAANEXO,NSIZEANEXO)
                                      VALUES (:id_usuario,:novoarquivo,SYSDATE,:tamanho)";
                $sql->clear();
                $sql->addParam(":id_usuario",$id_usuario);
                $sql->addParam(":novoarquivo",$id_usuario."_".$nome_arquivo);
                $sql->addParam(":tamanho",filesize($file));
                $sql->executeSQL($txt);
              }
              
              if ($path <> '')
                chdir($path);  
            }              
          }
                  
          $id_retorno = $sql->getReturn(":retorno1");
          $retorno['id'] = $id_retorno;
        
          if ($id == 1)
            $retorno['erro'] = 'Nenhum dado foi alterado. Altere o campo desejado antes de gravar a altera��o.';
        } else
          $retorno['erro'] = "O beneficiario ".$nome." nao pertence ao contrato: ".$_SESSION['codigo_contrato']." da empresa ".$_SESSION['titular_contrato'];
          
      } else
        $retorno['erro'] = 'O tamanho do arquivo n�o pode ser superior a ' . ini_get("upload_max_filesize"). '.';
      
      return $retorno;
    }    
    
    function convertBytes( $value ) {
      if ( is_numeric( $value ) ) {
          return $value;
      } else {
        $value_length = strlen( $value );
        $qty = substr( $value, 0, $value_length - 1 );
        $unit = strtolower( substr( $value, $value_length - 1 ) );
        switch ( $unit ) {
            case 'k':
                $qty *= 1024;
                break;
            case 'm':
                $qty *= 1048576;
                break;
            case 'g':
                $qty *= 1073741824;
                break;
        }
        return $qty;
      }
    }    
    
    function retornaStatusUsuario($bd,$usuario) {
      $sql = new Query($bd);
      $txt = "SELECT CDESCSTAT,CSITUUSUA,TO_CHAR(DSITUUSUA,'DD/MM/YY') DSITUUSUA 
                FROM HSSUSUA,HSSSTAT
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NNUMESTAT = HSSSTAT.NNUMESTAT(+) ";
      $sql->clear();
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);  
      
      if (($sql->result("CDESCSTAT") <> '') and ($_SESSION['apelido_operadora'] <> 'clinipam'))
        return $sql->result("CDESCSTAT");
      else  if ($sql->result("CSITUUSUA") == 'A')
        return "Ativo";
      else
        return "Cancelado em ".$sql->result("DSITUUSUA");
    }    
    
    function retornaTipoLogradouro($bd,$id) {
      $txt = "SELECT CDESCTLOGR FROM HSSTLOGR WHERE NNUMETLOGR = :id";
      $sql = new Query($bd);
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CDESCTLOGR");
    }
   
    function retornaNomePlano ($bd,$plano) {
      $txt = "SELECT CDESCPLAN FROM HSSPLAN
               WHERE NNUMEPLAN = :plano ";
      $sql = new Query($bd);
      $sql->addParam(":plano",$plano);
      $sql->executeQuery($txt);
      return $sql->result("CDESCPLAN");
    }
   
    function retornaNomeAcomodacao ($bd,$acomodacao) {
      $txt = "SELECT CDESCACOM FROM HSSACOM 
               WHERE NNUMEACOM = :acomodacao ";
      $sql = new Query($bd);
      $sql->addParam(":acomodacao",$acomodacao);
      $sql->executeQuery($txt);
      return $sql->result("CDESCACOM");
    }
   
    function retornaNomeLocacao($bd,$id,$fantasia="N") {
      $txt = "SELECT CNOMESETOR,CFANTSETOR FROM HSSSETOR WHERE NNUMESETOR = :id";
      $sql = new Query($bd);
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      
      if ($fantasia == "S")
        return $sql->result("CFANTSETOR");
      else
        return $sql->result("CNOMESETOR");
    }   
    
    function retornaMotivoCancelamento($bd,$id) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CDESCMCANC FROM HSSMCANC WHERE NNUMEMCANC = :id ";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CDESCMCANC");
    }  

    function alteracaoAtcad($id,$tabela,$campo,$valor) {
      $sql = new Query();
      $txt = "SELECT CNOVOATUCA FROM HSSATUCA
               WHERE NNUMEATCAD = :id
                 AND CTABEATUCA = :tabela
                 AND CCAMPATUCA = :campo ";
      $sql->addParam(":id",$id);
      $sql->addParam(":tabela",$tabela);
      $sql->addParam(":campo",$campo);  
      $sql->executeQuery($txt);

      if ($sql->count() > 0)
        return $sql->result("CNOVOATUCA");
      else
        return $valor;
    } 

    function emailNota($bd,$id_nota) {

      $sql = new Query($bd);
      $txt = "SELECT NNUMETITU,NNUMEUSUA
                FROM HSSCNOTA, HSSPAGA
               WHERE NNUMENOTAF = :nota
                 AND HSSCNOTA.NNUMEPAGA = HSSPAGA.NNUMEPAGA";
      $sql->addParam(":nota",$id_nota);
      $sql->executeQuery($txt);
      
      if ($sql->result("NNUMEUSUA") > 0) {
        $sql_mail = new Query($bd);
        $txt_mail = "SELECT NVL(NVL(USUA.CMAILUSUA,TITU.CMAILUSUA),HSSTITU.CMAILTITU) CMAILUSUA
                       FROM HSSUSUA USUA,HSSUSUA TITU,HSSTITU
                      WHERE USUA.NNUMEUSUA = :usuario
                        AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                        AND USUA.NNUMETITU = HSSTITU.NNUMETITU";
        $sql_mail->addParam(":usuario",$sql->result("NNUMEUSUA"));
        $sql_mail->executeQuery($txt_mail);
        $email = $sql_mail->result("CMAILUSUA");   
      } else {
        $sql_mail = new Query($bd);
        $txt_mail = "SELECT CMAILTITU FROM HSSTITU WHERE NNUMETITU = :contrato";
        $sql_mail->addParam(":contrato",$sql->result("NNUMETITU"));
        $sql_mail->executeQuery($txt_mail);
        $email = $sql_mail->result("CMAILTITU");    
      }

      return $email;
    }

    function telefoneNota($bd,$id_nota) {
      $formata = new Formata();
      
      $sql = new Query($bd);      
      $txt = "SELECT NNUMETITU,NNUMEUSUA
                FROM HSSCNOTA, HSSPAGA
               WHERE NNUMENOTAF = :nota
                 AND HSSCNOTA.NNUMEPAGA = HSSPAGA.NNUMEPAGA";
      $sql->addParam(":nota",$id_nota);
      $sql->executeQuery($txt);
      
      if ($sql->result("NNUMEUSUA") > 0) {
        $sql_fone = new Query($bd);
        $txt_fone = "SELECT CBOLEUSUA,1 ORDEM,CFONETLUSU,NNUMETLUSU
                       FROM HSSUSUA,HSSTLUSU
                      WHERE HSSUSUA.NNUMEUSUA = :usuario
                        AND HSSUSUA.NNUMEUSUA = HSSTLUSU.NNUMEUSUA
                        AND HSSTLUSU.CFONETLUSU IS NOT NULL
                      UNION ALL
                     SELECT HSSUSUA.CBOLEUSUA,2 ORDEM,CFONETLUSU,NNUMETLUSU
                       FROM HSSUSUA,HSSUSUA TITULAR,HSSTLUSU
                      WHERE HSSUSUA.NNUMEUSUA = :usuario
                        AND HSSUSUA.NNUMEUSUA <> HSSUSUA.NTITUUSUA
                        AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
                        AND TITULAR.NNUMEUSUA = HSSTLUSU.NNUMEUSUA
                        AND HSSTLUSU.CFONETLUSU IS NOT NULL
                      UNION ALL
                     SELECT CBOLEUSUA,3 ORDEM,CFONETLCON,NNUMETLCON
                       FROM HSSUSUA,HSSTLCON
                      WHERE HSSUSUA.NNUMEUSUA = :usuario
                        AND HSSUSUA.NNUMETITU = HSSTLCON.NNUMETITU
                        AND HSSTLCON.CFONETLCON IS NOT NULL
                      ORDER BY 2,4";
        $sql_fone->addParam(":usuario",$sql->result("NNUMEUSUA"));
        $sql_fone->executeQuery($txt_fone);
        $fone = $sql_fone->result("CFONETLUSU");   
      } else {
        $sql_fone = new Query($bd);
        $txt_fone = "SELECT CFONETLCON FROM HSSTLCON WHERE NNUMETITU = :contrato";
        $sql_fone->addParam(":contrato",$sql->result("NNUMETITU"));
        $sql_fone->executeQuery($txt_fone);
        $fone = $sql_fone->result("CFONETLCON");    
      }

      return $formata->acrescentaBrancos($fone,15,'D',' ');
    }    
       
    function retornaCodigoPlano ($bd,$plano) {
      $txt = "SELECT CCODIPLAN FROM HSSPLAN
               WHERE NNUMEPLAN = :plano ";
      $sql = new Query($bd);
      $sql->addParam(":plano",$plano);
      $sql->executeQuery($txt);
      return $sql->result("CCODIPLAN");
    }    
    
    function statusCorpoClinico($bd,$prestador,$especialidade) {
      $sql = new Query($bd);
      $sql->clear();   
      $txt = "SELECT NNUMEPRES,CSTATCCLIN
                FROM HSSCCLIN
               WHERE NNUMEPRES = :prestador
                 AND NHOSPPRES = :contratado
                 AND NNUMEESPEC = :especialidade ";
      $sql->addParam(":prestador",$prestador);
      $sql->addParam(":especialidade",$especialidade);
      $sql->addParam(":contratado",$_SESSION['id_contratado']);  
      $sql->executeQuery($txt);
      
      if (($sql->result("CSTATCCLIN") == 'I') or 
          ($sql->result("CSTATCCLIN") == 'E'))       
        return "Selecionado";
      else if ($sql->result("CSTATCCLIN") == 'A')
        return "Ativo";
      else
        return "";
    }    
    
    function inserePrestador($bd,$conselho,$numero_conselho,$uf_conselho,$nome,$nascimento,$sexo,$cpf,$especialidade,
                             $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,
                             $telefone,$enviaPTU,$residenciaMEC,$divulgado) {
      
      $sql = new Query($bd);
      $txt = "SELECT NNUMEPRES,CNOMEPRES
                FROM FINPRES
               WHERE ((CCONSPRES = :conselho AND NCRM_PRES = :numero AND CUFCRPRES = :uf) 
                       OR CCPF_PRES = :cpf) ";
      $sql->addParam(":conselho",$conselho);                  
      $sql->addParam(":numero",$numero_conselho);                  
      $sql->addParam(":uf",$uf_conselho);                  
      $sql->addParam(":cpf",$cpf);
      $sql->executeQuery($txt);
      
      $retorno = array();
      
      if ($sql->count() > 0) {
        $retorno['erro'] = "N�o � poss�vel cadastrar esse novo prestador. Cadastro em duplicidade! (".$sql->result("CNOMEPRES").").";
      } else {
        //if ($_SESSION['apelido_operadora'] == 'promedmg')
          $status = 'C';
        //else
        //  $status = 'Z';
          
        $sql = new Query($bd);         
        $txt = "BEGIN ".
               "   INSERE_PRESTADOR_WEB30(:contratado,:operador,:conselho,:numero_conselho,:uf_conselho,:nome,TO_DATE(:nascimento,'DD/MM/YYYY'),:sexo, ".
               "                        :cpf,:especialidade, ".
               "                        :cep,:tipo_logradouro,:logradouro,:numero,:complemento,:bairro,:estado,:cidade,:status,:id,".
               "                        :telefone,:enviaPTU,:residenciaMEC,:divulgado); ".
               "END; ";  
        $sql->addParam(":contratado",$_SESSION['id_contratado']); 
        $sql->addParam(":operador",$_SESSION['id_operador']);                  
        $sql->addParam(":conselho",$conselho);                  
        $sql->addParam(":numero_conselho",$numero_conselho);                  
        $sql->addParam(":uf_conselho",$uf_conselho);                  
        $sql->addParam(":nome",$nome);     
        $sql->addParam(":nascimento",$nascimento);     
        $sql->addParam(":sexo",$sexo);     
        $sql->addParam(":cpf",$cpf);
        $sql->addParam(":especialidade",$especialidade);    
        $sql->addParam(":cep",$cep);
        $sql->addParam(":tipo_logradouro",$tipo_logradouro);
        $sql->addParam(":logradouro",$logradouro);
        $sql->addParam(":numero",$numero);
        $sql->addParam(":complemento",$complemento);
        $sql->addParam(":bairro",$bairro);
        $sql->addParam(":estado",$estado);
        $sql->addParam(":cidade",$cidade);
        $sql->addParam(":status",$status);    
        $sql->addParam(":telefone",$telefone);    
        $sql->addParam(":enviaPTU",$enviaPTU);    
        $sql->addParam(":residenciaMEC",$residenciaMEC);    
        $sql->addParam(":divulgado",$divulgado);    
        $sql->addParam(":id",0,12);    
        $erro = $sql->executeSQL($txt);  
        
        if ($erro <> '')
          $retorno['erro'] = $erro;
        else
          $retorno['id'] = $sql->getReturn(":id");
      }
      
      return $retorno;
    }    
    
    function carregaDDD($bd) {
      $sql = new Query($bd);
      $txt = "SELECT DISTINCT NDDD_CIDAD FROM HSSCIDAD WHERE NDDD_CIDAD > 0 ORDER BY 1 ";
      $sql->executeQuery($txt);
      
      $html = '<option value=""></option>';
      
      while (!$sql->eof()) {
        $html .= '<option value="'.$sql->result("NDDD_CIDAD").'">'.$sql->result("NDDD_CIDAD").'</option>';
        $sql->next();
      }
      
      return $html;
    }
    
    function emailUsuario($bd,$usuario) {

      $sql_mail = new Query($bd);
      $txt_mail = "SELECT NVL(NVL(USUA.CMAILUSUA,TITU.CMAILUSUA),HSSTITU.CMAILTITU) CMAILUSUA,USUA.NNUMETITU
                     FROM HSSUSUA USUA,HSSUSUA TITU,HSSTITU
                    WHERE USUA.NNUMEUSUA = :usuario
                      AND USUA.NTITUUSUA = TITU.NNUMEUSUA
                      AND USUA.NNUMETITU = HSSTITU.NNUMETITU";
      $sql_mail->addParam(":usuario",$usuario);  
      $sql_mail->executeQuery($txt_mail);
      
      if ($sql_mail->result("CMAILUSUA") == '') {
        $sql_mail2 = new Query($bd);
        $txt_mail2 = "SELECT CMAILTITU FROM HSSTITU WHERE NNUMETITU = :contrato";
        $sql_mail2->addParam(":contrato",$sql_mail->result("NNUMETITU"));  
        $sql_mail2->executeQuery($txt_mail2);

        $email = $sql_mail2->result("CMAILTITU");       
      }
      else 
        $email = $sql_mail->result("CMAILUSUA");   
        
      return $email;
    }

    function avisoGuiaEmail($bd,$guia,$operacaoEmail='L',$enviaEmailTitu = 'N') {
      // $operacaoEmail = C - Envio de email de cancelamento de guia
      // $operacaoEmail = L - Envio de email de libera��o de guia
      $sql = new Query($bd);
      $txt = "SELECT HSSGUIA.NNUMEUSUA,HSSUSUA.CNOMEUSUA,HSSGUIA.NNUMEGUIA,CNOMEPRES,TITULAR.CNOMEUSUA NOME_TITULAR,HSSUSUA.NTITUUSUA
                FROM HSSGUIA,HSSUSUA,FINPRES,HSSUSUA TITULAR
               WHERE NNUMEGUIA = :guia
                 AND HSSGUIA.NLOCAPRES = FINPRES.NNUMEPRES
                 AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                 AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);

      if (($enviaEmailTitu == 'S') and ($sql->result("NTITUUSUA") <> $sql->result("NNUMEUSUA")))
        $emailTitu = $this->emailUsuario($bd,$sql->result("NTITUUSUA")).';';

      $emailUsu = $this->emailUsuario($bd,$sql->result("NNUMEUSUA"));	   

      $para  = explode(';',$emailTitu.$emailUsu); 
      
      if ($para != null) {
        $assunto = $_SESSION['nome_operadora']. ' - Aviso de guia emitida';

        if($operacaoEmail == 'L') {
          $assunto = $_SESSION['nome_operadora']. ' - Aviso de guia emitida';

        $mensagem = '<p>
                         Prezado(a) '.$sql->result("NOME_TITULAR").'.<br/><br/><br/>
                         Informamos a emiss�o de autoriza��o n�mero ' .$sql->result("NNUMEGUIA"). ' para realiza��o do(s) procedimento(s).<br/><br/>';
        }
        else if ($operacaoEmail == 'C'){
          $assunto = $_SESSION['nome_operadora']. ' - Aviso de guia cancelada';
                       
          $mensagem = '<p>
                       Prezado(a) '.$sql->result("NOME_TITULAR").'.<br/><br/><br/>
                       Informamos o cancelamento da autoriza��o n�mero ' .$sql->result("NNUMEGUIA"). ' para realiza��o do(s) procedimento(s).<br/><br/>';			
        }
        
        $sql_proc = new Query($bd);
        $txt_proc = "SELECT HSSPMED.CNOMEPMED,HSSPMED.CCODIPMED 
                       FROM HSSPGUI,HSSPMED
                      WHERE NNUMEGUIA = :guia 
                        AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED";
        $sql_proc->addParam(":guia",$guia);
        $sql_proc->executeQuery($txt_proc);  
        
        while (!$sql_proc->eof()) {
          $mensagem .= ' Procedimento: '.$sql_proc->result("CCODIPMED"). '  -  ' .$sql_proc->result("CNOMEPMED").'.<br/>';
          $sql_proc->next();
        }

        $mensagem .= '<br/><br/>
                     Benefici�rio: ' .$sql->result("CNOMEUSUA"). '<br/>
                     Prestador: ' .$sql->result("CNOMEPRES").  '<br/><br/>
                     Em caso de d�vidas, entre em contato com nossa Central de Autoriza��es pelo n�mero (81)3126-7730 (segunda a sexta das 7h �s 19h ou s�bados das 7h �s 13h) ou envie email para fiscosaude@fiscosaudepe.com.br.<br/>
                     <i>Atenciosamente, !</i><br/><br/><br/>'.
                    $_SESSION['nome_operadora'].'.<br/>
                    </p>';
        
        foreach($para as $destinatario){
          $this->enviaEmail($destinatario,$assunto,$mensagem);
        }
        return null;

      } else {
        return null;
      }
    }     
    
    function enviaEmail($para,$assunto,$mensagem,$anexo='') {
            
      $host      = $_SESSION['email_smtp'];
      $port      = $_SESSION['email_porta'];  
      $autentica = $_SESSION['email_ssl'];
            
      if (($autentica == '1') and (strpos($host,'tls://') === false))
        $host = 'tls://'.$host;
      else if (($autentica == '2') and (strpos($host,'ssl://') === false))
        $host = 'ssl://'.$host;

      $smtp = new Smtp($host,$port);

      if ($smtp->error == '') {
        $smtp->user = $_SESSION['email_usuario'];
        $smtp->pass = $_SESSION['email_senha'];  
        $smtp->debug =true;
        
        if ($anexo == '')
          $anexo = array();

        $enviado = $smtp->Send($para, $_SESSION['email_remetente'], $assunto, utf8_encode($mensagem),$anexo);
        
        if ($enviado == true)
          return '';
        else
          return '<br>'. utf8_encode($smtp->error);
      } else
        return utf8_encode($smtp->error);
    }  
    
    function enviaSMSIagente($bd,$guia) {
      
      $sqlconfig = new Query($bd);
      $txtconfig = " SELECT CUSMSINTER, CSSMSINTER,CMLSMSINTER FROM HSSINTER";
      $sqlconfig->executeQuery($txtconfig);   
      
      $conta = $sqlconfig->result("CUSMSINTER");
      $senha = $sqlconfig->result("CSSMSINTER");    
       
      
      $sql = new Query($bd);
      $txt = "SELECT HSSGUIA.NNUMEUSUA,SUBSTR(CNOMEUSUA, 1, INSTR(CNOMEUSUA,' ')-1) || ' ' || SUBSTR(CNOMEUSUA,INSTR(CNOMEUSUA,' ',-1)+1,LENGTH(CNOMEUSUA)) NOME,HSSGUIA.NNUMEGUIA,
                SUBSTR(CFANTPRES, 1, INSTR(CFANTPRES,' ')-1) || ' ' || SUBSTR(CFANTPRES,INSTR(CFANTPRES,' ',-1)+1,LENGTH(CFANTPRES)) PRESTADOR,
                TO_CHAR(DEMISGUIA,'HH24')||'h'||TO_CHAR(DEMISGUIA,'MI') HORARIO, TO_CHAR(DEMISGUIA,'DD/MM/YY') DATA
                FROM HSSGUIA,HSSUSUA,FINPRES
               WHERE NNUMEGUIA = :guia
                 AND HSSGUIA.NLOCAPRES = FINPRES.NNUMEPRES
                 AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);
      
      $mensagem = $sqlconfig->result("CMLSMSINTER");
      
      $mensagem = str_replace('{BENEFICIARIO}',$sql->result("NOME"),$mensagem);
      $mensagem = str_replace('{PRESTADOR}',$sql->result("PRESTADOR"),$mensagem);
      $mensagem = str_replace('{DATA}',$sql->result("DATA"),$mensagem);
      $mensagem = str_replace('{HORARIO}',$sql->result("HORARIO"),$mensagem);
      

      $mensagem = urlencode($mensagem);    
      
     
      $sql_f = new Query($bd);      
      $txt_f = "SELECT SOMENTE_NUMEROS_STRING(CFONETLUSU) FONE
                FROM HSSUSUA,HSSTLUSU
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NNUMEUSUA = HSSTLUSU.NNUMEUSUA
                 AND HSSTLUSU.CTIPOTLUSU = 'C' 
                 AND ROWNUM = 1 ";    
      $sql_f->addParam(":usuario",$sql->result("NNUMEUSUA")); 
      $sql_f->executeQuery($txt_f);   
      
      $fone = $sql_f->result("FONE");
      
     
     
      if ($sql_f->count() > 0) {
        $endereco = 'http://www.iagentesms.com.br/webservices/smslote.php?usuario='.$conta.'&senha='.$senha.'&mensagem='.$mensagem.'&celulares='.$fone;
        $homepage = file_get_contents($endereco);
        return true;
      }
      else {
        $sql_f->clear();
        $txt_f = "SELECT SOMENTE_NUMEROS_STRING(CFONETLUSU) FONE
                  FROM HSSUSUA,HSSUSUA TITULAR,HSSTLUSU
                WHERE HSSUSUA.NNUMEUSUA = :usuario
                  AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA
                  AND TITULAR.NNUMEUSUA = HSSTLUSU.NNUMEUSUA  
                  AND HSSTLUSU.CTIPOTLUSU = 'C' 
                  AND ROWNUM = 1  ";   
        $sql_f->addParam(":usuario",$sql->result("NNUMEUSUA")); 
        $sql_f->executeQuery($txt_f);    

        $fone = $sql_f->result("FONE");    
        if ($sql_f->count() > 0) {
          $endereco = 'http://www.iagentesms.com.br/webservices/smslote.php?usuario='.$conta.'&senha='.$senha.'&mensagem='.$mensagem.'&celulares='.$fone;
          $homepage = file_get_contents($endereco);   
          return true;
        } 
        return false;        
      }  
    }   
    
    function insereLogOperacao($id,$descricao,$local = NULL) {
      $sql = new Query();
      $txt = "BEGIN ".
             "  INSERE_LOG_WEB30 (:id,:descricao,:operador,:ip,:protocolo,:local); ".
             "END; ";
      $sql->addParam(":id",$id);  
      $sql->addParam(":descricao",$descricao); 
      $sql->addParam(":operador",$_SESSION['id_operador']);                                                 
      $sql->addParam(":ip",getenv("REMOTE_ADDR"));
      $sql->addParam(":protocolo",0,12);  
      $sql->addParam(":local",$local); 
      $erro = $sql->executeSQL($txt);
      
      $retorno['erro'] = $erro;
      $retorno['id'] = $sql->getReturn(":protocolo");

      return $retorno;
    }  

    function podeEditarProspect($bd,$vendedor) {
      $sql = new Query($bd);               
      $txt = "SELECT 1 FROM (
              SELECT NNUMEVEND,CNOMEVEND,CPESSVEND
                FROM HSSVEND
               WHERE NNUMEVEND = :vendedor
               UNION
              SELECT DISTINCT HSSVEND.NNUMEVEND,CNOMEVEND,CPESSVEND
                FROM HSSUNCOM,HSSGRCOM,HSSVEND
               WHERE HSSUNCOM.NNUMEUNCOM = HSSGRCOM.NNUMEUNCOM
                 AND HSSGRCOM.NNUMEVEND = HSSVEND.NNUMEVEND
                 AND DVIGEGRCOM = (SELECT MAX(GRCOM.DVIGEGRCOM) 
                                     FROM HSSGRCOM GRCOM
                                    WHERE GRCOM.NNUMEVEND = HSSGRCOM.NNUMEVEND
                                      AND GRCOM.DVIGEGRCOM <= SYSDATE)
              CONNECT BY PRIOR HSSUNCOM.NNUMEUNCOM = HSSUNCOM.NSUPEUNCOM
              START WITH HSSUNCOM.NNUMEUNCOM IN (SELECT NNUMEUNCOM
                                                  FROM HSSGRCOM
                                                  WHERE NNUMEVEND = :vendedor 
                                                    AND DVIGEGRCOM = (SELECT MAX(GRCOM.DVIGEGRCOM) 
                                                                        FROM HSSGRCOM GRCOM
                                                                       WHERE GRCOM.NNUMEVEND = HSSGRCOM.NNUMEVEND
                                                                         AND GRCOM.DVIGEGRCOM <= SYSDATE))
               UNION
              SELECT NNUMEVEND,CNOMEVEND,CPESSVEND
                FROM HSSVEND
               WHERE NNUMEUNCOM IN (SELECT DISTINCT HSSUNCOM.NNUMEUNCOM
                                      FROM HSSUNCOM,HSSGRCOM,HSSVEND
                                     WHERE HSSUNCOM.NNUMEUNCOM = HSSGRCOM.NNUMEUNCOM
                                       AND DVIGEGRCOM = (SELECT MAX(GRCOM.DVIGEGRCOM) 
                                                           FROM HSSGRCOM GRCOM
                                                          WHERE GRCOM.NNUMEVEND = HSSGRCOM.NNUMEVEND
                                                            AND GRCOM.DVIGEGRCOM <= SYSDATE)                       
                                       AND HSSGRCOM.NNUMEVEND = HSSVEND.NNUMEVEND
                                   CONNECT BY PRIOR HSSUNCOM.NNUMEUNCOM = HSSUNCOM.NSUPEUNCOM
                                     START WITH HSSUNCOM.NNUMEUNCOM IN (SELECT NNUMEUNCOM
                                                                         FROM HSSGRCOM
                                                                        WHERE NNUMEVEND = :vendedor 
                                                                          AND HSSGRCOM.DVIGEGRCOM = (SELECT MAX(GRCOM.DVIGEGRCOM) 
                                                                                                       FROM HSSGRCOM GRCOM
                                                                                                      WHERE GRCOM.NNUMEVEND = HSSGRCOM.NNUMEVEND
                                                                                                        AND GRCOM.DVIGEGRCOM <= SYSDATE)))
               ORDER BY CPESSVEND DESC,CNOMEVEND ) A
              WHERE NNUMEVEND = :vendedor2";                                                                                                    
                                                                                              
      $sql->addParam(":vendedor",$_SESSION['id_vendedor']);           
      $sql->addParam(":vendedor2",$vendedor);           
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0)  
        return true;
      else
        return false;
    }    
    
    function carregaVendedores($bd,$tpl,$selecionado,$todos="N") {

      $seg = new Seguranca();
      
      $txt = "SELECT NVL(CVCOMCONF,'1') VERSAO FROM HSSCONF";
      $sql = new Query($bd);
      $sql->executeQuery($txt);
      $versao = $sql->result("VERSAO");

      $sql->clear();  
      if ($versao == '2') {            
        $txt = "SELECT DISTINCT NNUMEVEND, CNOMEVEND,CPESSVEND FROM (SELECT NNUMEVEND,CNOMEVEND,CPESSVEND
                  FROM HSSVEND
                 WHERE NNUMEVEND = :vendedor
                   AND CSITUVEND = 'A' 
                 UNION ALL
                SELECT NNUMEVEND,VENDEDOR,CPESSVEND FROM EQUIPE_VENDEDOR 
                 WHERE IDSUPERIOR = :vendedor
                   AND CSITUVEND = 'A'
                   AND DVIGEGRCOM = (SELECT MAX(GRCOM.DVIGEGRCOM) 
                                       FROM EQUIPE_VENDEDOR GRCOM
                                      WHERE GRCOM.SUPERIOR = EQUIPE_VENDEDOR.SUPERIOR
                                        AND GRCOM.DVIGEGRCOM <= SYSDATE) ";
                                                                                                    
        if ($seg->permissaoOutros($bd,'WEBCOMERCIALPODEVISUALIZARTODOSOSPROSPECTSDASUAUNIDADECOMERCIAL',false)) {
          $txt .= "  UNION ALL
                    SELECT NNUMEVEND,CNOMEVEND,CPESSVEND
                      FROM HSSVEND 
                     WHERE NNUMEUNCOM IN (SELECT NNUMEUNCOM FROM HSSVEND WHERE NNUMEVEND = :vendedor)
                       AND CSITUVEND = 'A' ";
        }
        $txt .= " ) 
                 ORDER BY CNOMEVEND ";                                                                                                    
      } else {
        $txt = "SELECT DISTINCT HSSVEND.NNUMEVEND,CNOMEVEND,CPESSVEND
                  FROM HSSVEND
                 WHERE (HSSVEND.NNUMEVEND = :vendedor OR
                        HSSVEND.NSUPEVEND = :vendedor OR
                        HSSVEND.NGEREVEND = :vendedor OR
                        HSSVEND.NCOORVEND = :vendedor OR
                        HSSVEND.NDIREVEND = :vendedor)
                   AND CSITUVEND = 'A'
                 ORDER BY CPESSVEND DESC,CNOMEVEND ";      
      }
                                                                                              
      $sql->addParam(":vendedor",$_SESSION['id_vendedor']);           
      $sql->executeQuery($txt);
      
      if ($todos == "S") {
        $tpl->VENDEDOR_ID = "0";
        $tpl->VENDEDOR_NOME = "Independente";
        $tpl->block("VENDEDOR");    
      }
          
      $vendedores = array();
      while (!$sql->eof()) {
        $tpl->VENDEDOR_ID   = $sql->result("NNUMEVEND");
        $tpl->VENDEDOR_NOME = $sql->result("CNOMEVEND");
        
        if ($selecionado == $sql->result("NNUMEVEND"))
          $tpl->VENDEDOR_SELECIONADO = "selected";
        else
          $tpl->VENDEDOR_SELECIONADO = "";
          
        $vendedores[] = $sql->result("NNUMEVEND");
        $tpl->block("VENDEDOR");
        $sql->next();
      }
      
      return $vendedores;
    }    
    
    function retornaNomeVendedor($bd,$id) {
      $sql = new Query($bd);
      $txt = "SELECT CNOMEVEND DESCRICAO FROM HSSVEND WHERE NNUMEVEND = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("DESCRICAO");
    }    
    
    function validaConfirmacao ($bd,$guia,$executante,$especialidade) {

      $seg = new Seguranca();
      
      $sql = new Query($bd);
      $txt = "SELECT HSSPGUI.CCODIPMED,HSSPGUI.NQUANPGUI,HSSGUIA.CTIPOGUIA,HSSUSUA.NNUMEUSUA,HSSGUIA.CURGEGUIA,HSSGUIA.CTIPOGUIA,
                     DECODE(HSSTITU.NNUMEEMPR,NULL,1,DECODE(HSSTITU.CBOLETITU,'S',1,2)) NJ,HSSGUIA.DVALIGUIA,HSSGUIA.NNUMECID_  
                FROM HSSGUIA,HSSPGUI,HSSUSUA,HSSTITU
               WHERE HSSGUIA.NNUMEGUIA = :guia
				 AND NVL(HSSPGUI.CSTATPGUI,'L') = 'L'
                 AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
                 AND HSSGUIA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                 AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU";
      $sql->addParam(":guia",$guia);
      $sql->executeQuery($txt);
      
      $retorno = array();
      $resultado = 0;  
      while (!$sql->eof() and $resultado == 0) {
        $procedimento = $sql->result("CCODIPMED");    
        $nj           = $sql->result("NJ");
        $validade     = $sql->result("DVALIGUIA");
        $id_usuario   = $sql->result("NNUMEUSUA");
        $resultado    = $this->validaProcedimento ($bd,$sql->result("NNUMEUSUA"),$procedimento,$sql->result("NQUANPGUI"),
                                                   $executante,$_SESSION['id_contratado'],$especialidade,
                                                   0,0,
                                                   $sql->result("CURGEGUIA"),$sql->result("CTIPOGUIA"),1,$guia,'S',null,$sql->result("NNUMECID_"));      
                          
        $sql->next();
      }

      if ($resultado == 0) {
        $seg->alteraOperador($bd,$_SESSION['id_operador']);
        
        $sql = new Query($bd);
        $txt = "UPDATE HSSGUIA 
                   SET NNUMEPRES = :prestador , 
                       NLOCAPRES = :local 
                 WHERE NNUMEGUIA = :guia ";
        $sql->addParam(":prestador",$executante);
        $sql->addParam(":local",$_SESSION['id_local']);
        $sql->addParam(":guia",$guia);    
        $sql->executeSQL($txt);  
        
        $retorno['guia'] = $guia;
      } else if ($resultado == 16) {
        $retorno['msg'] = $procedimento.' - C�digo de procedimento n�o existe.';
      } elseif ($resultado == 24) {
        $retorno['msg'] = $procedimento.' - C�digo de procedimento inativo.';    
      } elseif ($resultado == 19) {
          $retorno['msg'] = 'Benefici�rio com co-participa��o a pagar e com programa��o de cancelamento. Favor dirigir-se a Operadora.';    
      } elseif ($resultado == 20) {
        $retorno['msg'] = "A guia n�o pode ser liberada pela internet, pois sua co-participa��o deve ser paga diretamente no caixa da Operadora.";            
      } elseif ($resultado == 21) {
        $retorno['msg'] = "Problemas t�cnicos no sistema impossibilitam a autoriza��o.";
      } elseif ($resultado == 22) {
        $retorno['msg'] = "Problemas t�cnicos impossibilitam a autoriza��o. Entre em contato com a operadora.";  
      } elseif ($resultado == 26) {
        $retorno['msg'] = ' Guia com validade vencida - '.$validade .'.' ;  
      } elseif ( ($resultado == 7) or ($resultado == 17) ) {
        $retorno['msg'] = $procedimento.' - Procedimento n�o pode ser liberado pela internet.';
      } elseif ($resultado == 1) {
        if ($_SESSION['apelido_operadora'] == "lifeSul") {
          $retorno['msg'] = $procedimento.' - O procedimento solicitado n�o faz parte da tabela cadastrada.';
          } else {
            $retorno['msg'] = $procedimento.' - Prestador n�o autorizado a executar esse procedimento.';
          }
      } elseif ($resultado == 2) {
        $retorno['msg'] = $procedimento.' - Procedimento sem cobertura contratual.';
      } elseif ($resultado == 3) {
        $retorno['msg'] = $procedimento.' - Paciente em car�ncia.';
        $retorno['carencia'] = 'S';
        $retorno['id_usuario'] = $id_usuario;    
      } elseif ($resultado==4) {
        $retorno['msg'] = 'Retorno ou reincid�ncia de procedimento - Favor contactar a Operadora.';
        $retorno['id_usuario'] = $id_usuario;
        $retorno['retorno'] = $procedimento;        
      } elseif ($resultado == 6) {
        // Inadimpl�ncia
        if ($nj == 1) {
          $retorno['msg'] = 'H� um problema administrativo com esse paciente. Favor encaminha-lo a operadora para maiores detalhes.';
        } else {
          $retorno['msg'] = 'H� um problema administrativo. Favor solicite que entre em contato com a operadora.';
        } 
      } elseif ($resultado == 8) {
        $retorno['msg'] = 'Combina��o Dente / Face n�o permitida para o procedimento solicitado.';
      } elseif ($resultado == 9 and $_SESSION['apelido_operadora'] == "ApasRibeira") {
        $nome_especialidade = $this->retornaNomeEspecialidade($bd,$especialidade);
        $retorno['msg'] = 'Especialidade ('.$nome_especialidade.') n�o compat�vel com a idade do paciente.';
      } elseif ($resultado == 9) {
        $retorno['msg'] = 'Procedimento ('.$procedimento.') n�o compat�vel com a idade do paciente. Favor contatar a Operadora.';
      } elseif ($resultado == 10) {
        $retorno['msg'] = 'Procedimento/Especialidade n�o aplic�vel ao sexo do paciente.';
      } elseif ($resultado == 11) {
        $retorno['msg'] = 'Rede de atendimento n�o permitida para este paciente.';
      } elseif ($resultado == 12 and $_SESSION['apelido_operadora'] == "ApasRibeira") { // Numero de incidencias incompativel
        $retorno['msg'] = 'Este procedimento ('.$procedimento.')  s� pode ser liberado em 1(uma) quantidade.';
      } elseif ($resultado == 12) { 
        $retorno['msg'] = 'Este procedimento ('.$procedimento.') n�o pode ser liberado devido � quantidade incompat�vel com a utiliza��o do paciente.';  
      } elseif ($resultado == 13) {
        $retorno['msg'] = 'Este procedimento ('.$procedimento.') n�o pode ser liberado por necessitar de per�cia pr�via do servi�o de auditoria da Operadora.';
      } elseif ($resultado == 14) {
        $retorno['msg'] = 'Este procedimento ('.$procedimento.') j� foi liberado hoje para esse mesmo paciente.';     
        $retorno['id_usuario'] = $id_usuario;
        $retorno['retorno'] = $procedimento;        
      } elseif ($resultado == 29) {
        $retorno['msg'] = 'Esta guia ser� enviada para a unimed origem do benefici�rio';
      } elseif ($resultado == 30) {  
        $retorno['msg'] = 'Este procedimento ('.$procedimento.') j� foi liberado conforme tabela de faixa et�ria de puericultura.';
      } else {
        $retorno['msg'] = 'Problemas t�cnicos no sistema.';      
      }
      
      return $retorno;
     
    }
    
    function validaProcedimento ($bd,$id_usuario,$procedimento,$quantidade,$executante,$local,$especialidade,$dente,
                                 $face,$regime,$natureza,$tipo,$guia = 0,$confirmacao = 'N',$taxa = null, $cid = '') {
      
      $sql = new Query($bd);

      if (($regime <> 'E') and ($regime <> 'U'))
        $regime = 'E';
       
      if (($natureza <> 'A') and ($natureza <> 'I') and 
          ($natureza <> 'C') and ($natureza <> 'O') and   
          ($natureza <> 'R') and ($natureza <> 'P') and       
          ($natureza <> 'S')
          )
        $natureza = 'A';
        
      $txt = "SELECT VALIDA_PROCEDIMENTO_WEB30 (:id_usuario,:procedimento,:quantidade,:executante,:dente,:face,:especialidade,:local,nvl(:regime,'E'),nvl(:natureza,'A'),:tipo,:guia,:confirmacao,null,'S',:taxa,:cid) VALIDACAO FROM DUAL";
      $sql->clear();   
      $sql->addParam(":id_usuario",$id_usuario);
      $sql->addParam(":procedimento",$procedimento);
      $sql->addParam(":quantidade",$quantidade);
      $sql->addParam(":executante",$executante);
      $sql->addParam(":dente",$dente);
      $sql->addParam(":face",$face);
      $sql->addParam(":especialidade",$especialidade);
      $sql->addParam(":local",$local);
      $sql->addParam(":regime",$regime);
      $sql->addParam(":natureza",$natureza);
      $sql->addParam(":tipo",$tipo);
      $sql->addParam(":guia",$guia);
      $sql->addParam(":confirmacao",$confirmacao);
      $sql->addParam(":taxa",$taxa);
      $sql->addParam(":cid",$cid);
      
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0)
        return $sql->result("VALIDACAO");
      else
        return 21;
    }
    
    function listaCarencias($bd,$id) {
      // na agemed n�o querem que liste as carencias, alterei para todos utilizando o parametro mostra carencia. 
      if ($_SESSION['carencias'] == "S") {                      
        $sql = new Query($bd);
        $sql->clear();
        $txt = "SELECT HSSCRUS.NNUMECARE,CNOMECARE,TO_CHAR(DVENCCRUS,'DD/MM/YYYY') DVENCCRUS,HSSCRUS.NNUMECARE,DVENCCRUS
                  FROM HSSCRUS,HSSCARE
                 WHERE HSSCRUS.NNUMEUSUA = :usuario
                   AND HSSCRUS.NNUMECARE = HSSCARE.NNUMECARE
                 UNION ALL
                SELECT 0,CDESCTXMEN,TO_CHAR(DCARETXUSU,'DD/MM/YYYY') DVENCCRUS,HSSTXUSU.NNUMETXMEN,DCARETXUSU
                  FROM HSSTXUSU,HSSTXMEN
                 WHERE HSSTXUSU.NNUMEUSUA = :usuario
                   AND HSSTXUSU.DCARETXUSU IS NOT NULL
                   AND HSSTXUSU.NNUMETXMEN = HSSTXMEN.NNUMETXMEN ";
        $sql->addParam(":usuario",$id);
        $sql->executeQuery($txt);
        
        $text  = '<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align=center><font size=2 color=red>Quadro de car�ncias deste usu�rio</font></td>
                    </tr>
                    <tr>
                      <td bgcolor="#CCCCCC" class="fonte_11_b">Car�ncia</td>
                      <td bgcolor="#CCCCCC" class="fonte_11_b">Data</td>
                    </tr>';
                    
        while (!$sql->eof()) {
          $text .= '<tr>
                      <td align="left" class="fonte_11_n">'.$sql->result("CNOMECARE").'</td>
                      <td align="left" class="fonte_11_n">'.$sql->result("DVENCCRUS").'</td>
                    </tr>';
          $sql->next();                      
        }
        
        $text .= '</table>';
        
        return $text;
      }
    }

    function listaRetorno ($bd,$id,$prestador,$procedimento,$regime,$idGuia = 0) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CTIPOPMED FROM HSSPMED WHERE CCODIPMED = :procedimento";
      $sql->addParam(":procedimento",$procedimento);
      $sql->executeQuery($txt);
      $tipo_procedimento = $sql->result("CTIPOPMED");
      
      $sql->clear();  
      if ($tipo_procedimento == "C") {
        $txt = "SELECT HSSGUIA.NNUMEGUIA,TO_NUMBER(NULL) NNUMECONT,TO_CHAR(DEMISGUIA,'DD/MM/YYYY') DEMISGUIA,DEMISGUIA,PRES.CNOMEPRES,LOCAL.CNOMEPRES LOCAL_ATENDIMENTO
                  FROM HSSGUIA,HSSPGUI,HSSPMED,FINPRES PRES,FINPRES LOCAL
                 WHERE NUTILUSUA = :usuario
                   AND HSSGUIA.NNUMEPRES = :prestador
                   AND NVL(HSSGUIA.CSTATGUIA,'X') <> 'N'
                   --AND HSSGUIA.CURGEGUIA = :regime 
                   AND HSSGUIA.NNUMEGUIA <> :guia
                   AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
                   AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED
                   AND CTIPOPMED = 'C'
                   AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES
                   AND NVL(HSSGUIA.NLOCAPRES,HSSGUIA.NNUMEPRES) = LOCAL.NNUMEPRES
                 UNION ALL
                SELECT TO_NUMBER(NULL),HSSCONT.NNUMECONT,TO_CHAR(DATENCONT,'DD/MM/YYYY'),DATENCONT,PRES.CNOMEPRES,LOCAL.CNOMEPRES
                  FROM HSSCONT,HSSPCON,HSSPMED,FINPRES PRES,FINPRES LOCAL
                 WHERE HSSCONT.NNUMEUSUA = :usuario
                   AND HSSCONT.NNUMEPRES = :prestador
                   --AND HSSCONT.CURGECONT = :regime
                   AND HSSCONT.NNUMECONT NOT IN (SELECT HSSGCON.NNUMECONT FROM HSSGCON WHERE HSSGCON.NNUMECONT = HSSCONT.NNUMECONT)
                   AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                   AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                   AND CTIPOPMED = 'C'
                   AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES
                   AND NVL(HSSCONT.NLOCAPRES,HSSCONT.NNUMEPRES) = LOCAL.NNUMEPRES
                ORDER BY 4 DESC";
      } else {
        $txt = "SELECT HSSGUIA.NNUMEGUIA,TO_NUMBER(NULL) NNUMECONT,TO_CHAR(DEMISGUIA,'DD/MM/YYYY') DEMISGUIA,DEMISGUIA,PRES.CNOMEPRES,LOCAL.CNOMEPRES 
                  FROM HSSGUIA,HSSPGUI,FINPRES PRES,FINPRES LOCAL 
                 WHERE NUTILUSUA = :usuario
                   AND NVL(HSSGUIA.CSTATGUIA,'X') <> 'N'
                   --AND HSSGUIA.CURGEGUIA = :regime
                   AND HSSGUIA.NNUMEGUIA = HSSPGUI.NNUMEGUIA
                   AND CCODIPMED = :procedimento
                   AND HSSGUIA.NNUMEGUIA <> :guia
                   AND HSSGUIA.NNUMEPRES = PRES.NNUMEPRES
                   AND NVL(HSSGUIA.NLOCAPRES,HSSGUIA.NNUMEPRES) = LOCAL.NNUMEPRES               
                 UNION ALL 
                SELECT TO_NUMBER(NULL),HSSCONT.NNUMECONT,TO_CHAR(DATENCONT,'DD/MM/YYYY') DEMISGUIA,DATENCONT,PRES.CNOMEPRES,LOCAL.CNOMEPRES 
                  FROM HSSCONT,HSSPCON,FINPRES PRES,FINPRES LOCAL 
                 WHERE HSSCONT.NNUMEUSUA = :usuario 
                   AND HSSCONT.NNUMEPRES = :prestador
                   --AND HSSCONT.CURGECONT = :regime
                   AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
                   AND CCODIPMED = :procedimento
                   AND HSSCONT.NNUMEPRES = PRES.NNUMEPRES
                   AND NVL(HSSCONT.NLOCAPRES,HSSCONT.NNUMEPRES) = LOCAL.NNUMEPRES               
                ORDER BY 4 DESC";  
        $sql->addParam(":procedimento",$procedimento);            
      }
                
      $sql->addParam(":usuario",$id);
      $sql->addParam(":prestador",$prestador);     
      $sql->addParam(":guia",$idGuia);
      //$sql->addParam(":regime",$regime);
      $sql->executeQuery($txt);

      $text = '<table width="100%" cellspacing="1" cellpadding="0">
                <tr>
                  <th bgcolor="#CCCCCC" class="fonte_11_b" width="50">Guia</th>
                  <th bgcolor="#CCCCCC" class="fonte_11_b" width="50">Conta</th>
                  <th bgcolor="#CCCCCC" class="fonte_11_b" width="200">Prestador </th>
                  <th bgcolor="#CCCCCC" class="fonte_11_b" width="200">Local de atendimento</th>
                  <th bgcolor="#CCCCCC" class="fonte_11_b" width="30">Data </th>
                  <th bgcolor="#CCCCCC" class="fonte_11_b" width="100px">Procedimento </th>
                </tr>';
                
      while (!$sql->eof()) {
        $text .= '<tr>
                    <td align="center" class="fonte_10_n" width="50">'.$sql->result("NNUMEGUIA").'</td>
                    <td align="center" class="fonte_10_n" width="50">'.$sql->result("NNUMECONT").'</td>
                    <td align="left" class="fonte_10_n" width="200">'.$sql->result("CNOMEPRES").'</td>
                    <td align="left" class="fonte_10_n" width="200">'.$sql->result("LOCAL_ATENDIMENTO").'</td>
                    <td align="center" class="fonte_10_n" width="30">'.$sql->result("DEMISGUIA").'</td>
                    <td align="center" class="fonte_10_n" width="100px">'.$procedimento.'</td>
                  </tr>';              
        $sql->next();
      }   

      $text .= '</table>';  
      
      return $text;
    }

    function clausulaIn ($array,$coloca_aspas="N") {

      $in = "(";
      
      for ($i=0;$i<sizeOf($array);$i++) {
      
        if ($coloca_aspas == "S")
          $in .= "'".$array[$i]."',";
        else
          $in .= $array[$i].",";
      }

      $in = substr($in,0,-1).")";
      
      return $in;
    }    
    
    /* Empresa */
    function carregaEmpresas($bd,$tpl,$selecionado) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT HSSTITU.NNUMETITU,HSSTITU.CCODITITU,CRAZAEMPR,HSSTITU.NNUMEEMPR
                FROM HSSOPVEN,HSSVEND,HSSTITU,HSSEMPR
               WHERE HSSOPVEN.NNUMEUSUA = :operador
                 AND HSSOPVEN.NNUMEVEND = :vendedor
                 AND ((CSITUVEND = 'A') OR (CSITUVEND <> 'A' AND DCANCVEND > SYSDATE))
                 AND HSSOPVEN.NNUMEVEND = HSSVEND.NNUMEVEND
                 AND HSSVEND.NNUMEVEND = HSSTITU.NNUMEVEND
                 AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR
               ORDER BY 3 ";
               
      $sql->addParam(":operador",$_SESSION['id_operador']);           
      $sql->addParam(":vendedor",$_SESSION['id_vendedor']);    
      $sql->executeQuery($txt);
          
      while (!$sql->eof()) {
        $tpl->EMPRESA_ID = $sql->result("NNUMEEMPR");
        $tpl->EMPRESA_NOME = $sql->result("CRAZAEMPR");
        
        if ($selecionado == $sql->result("NNUMEEMPR"))
          $tpl->EMPRESA_SELECIONADO = "selected";
        else
          $tpl->EMPRESA_SELECIONADO = "";
          
        $tpl->block("EMPRESA");
        $sql->next();
      }
    }

    function retornaNomeEmpresa($bd,$id,$fantasia="N") {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CRAZAEMPR,CFANTEMPR FROM HSSEMPR WHERE NNUMEEMPR = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      
      if ($fantasia == "S")
        return $sql->result("CFANTEMPR");
      else
        return $sql->result("CRAZAEMPR");
    }

    function retornaNomeUnidadeComercial($bd,$id) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CDESCUNCOM FROM HSSUNCOM WHERE NNUMEUNCOM = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CNOMEUNCOM");
    }

    function retornaIdUnidadeComercial($bd,$vendedor) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CDESCUNCOM FROM HSSUNCOM WHERE NNUMEUNCOM = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CNOMEUNCOM");
    }

    function retornaNomeEstabelecimento($bd,$id) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT CNOMEESTAB FROM SFPESTAB WHERE NNUMEESTAB = :id";
      $sql->addParam(":id",$id);
      $sql->executeQuery($txt);
      return $sql->result("CNOMEESTAB");
    }

    function periodoSinistralidadeVendas($bd,$inicio,$referencia,&$p_inicial,&$p_final){
        $sql = new Query($bd);
        $txt = " SELECT FIRST_DAY(ADD_MONTHS(:inicio,-1)) MES_INICIAL, LAST_DAY(ADD_MONTHS(:inicio,-1)) MES_FINAL,
                        FIRST_DAY(ADD_MONTHS(:inicio,-13)) ANO_INICIAL, LAST_DAY(ADD_MONTHS(:inicio,-1)) ANO_FINAL FROM DUAL ";
        $sql->addParam(":inicio",$inicio);
        $sql->executeQuery($txt);
        if($referencia == 'A'){
            $p_inicial = $sql->result("ANO_INICIAL");
            $p_final = $sql->result("ANO_FINAL");
        }else{
            $p_inicial = $sql->result("MES_INICIAL");
            $p_final = $sql->result("MES_FINAL");    
        }    
    }
    /*Inicio Fun��es Comercial*/
    function primeiraMensalidade ($contrato,$titular,$usuario,$adesao,$aditivo) {
      $dados = array();
      
      $sql_paga = new Query();
      $txt_paga = "SELECT NDOCUPAGA,CPAGOPAGA,TO_CHAR(DPAGAPAGA,'DD/MM/YY') DPAGAPAGA,DVENCPAGA,NNUMEPAGA
                     FROM HSSPAGA
                    WHERE HSSPAGA.NNUMETITU = :contrato ";
                            
      if ($titular > 0) {
        $txt_paga .= "   AND HSSPAGA.NNUMEUSUA = :titular ";
        $sql_paga->addParam(":titular",$titular);
      }
              
      $txt_paga .= " ORDER BY 3 ASC";
                             
      $sql_paga->addParam(":contrato",$contrato);
      $sql_paga->executeQuery($txt_paga);
                          
      $sql_paga2 = new Query();
      $txt_paga2 = "SELECT SUM(VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,7)) VALOR,
                           SUM(VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,8)) ADESAO,
                           SUM(VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,9)) ADITIVO
                      FROM HSSUSUPG
                     WHERE NNUMEPAGA = :pagamento ";
                     
      $sql_paga2->addParam(":pagamento",$sql_paga->result("NNUMEPAGA"));
      
      if ($usuario > 0) {
        $txt_paga2 .= "   AND NNUMEUSUA = :usuario ";    
        $sql_paga2->addParam(":usuario",$usuario);
      }
        
      $sql_paga2->executeQuery($txt_paga2);              

      $valor = str_replace(',','.',$sql_paga2->result("VALOR"));
      
      if ($adesao <> 'N')
        $valor += str_replace(',','.',$sql_paga2->result("ADESAO"));
        
      if ($aditivo == 'S')
        $valor += str_replace(',','.',$sql_paga2->result("ADITIVO"));
                
      $dados['documento'] = $sql_paga->result("NDOCUPAGA");
      $dados['valor'] = $valor;
      
      if ($sql_paga->result("CPAGOPAGA") == 'N')
        $dados['pagamento'] = ' Em aberto';
      else
        $dados['pagamento'] = $sql_paga->result("DPAGAPAGA");
        
      return $dados;
    }

    function ultimaMensalidade ($contrato,$titular,$usuario,$adesao,$aditivo) {
      $dados = array();
      
      $sql_paga = new Query();
      $txt_paga = "SELECT NDOCUPAGA,CPAGOPAGA,TO_CHAR(DPAGAPAGA,'DD/MM/YY') DPAGAPAGA,DVENCPAGA,NNUMEPAGA
                     FROM HSSPAGA
                    WHERE HSSPAGA.NNUMETITU = :contrato ";
                            
      if ($titular > 0) {
        $txt_paga .= "   AND HSSPAGA.NNUMEUSUA = :titular ";
        $sql_paga->addParam(":titular",$titular);
      }
              
      $txt_paga .= " ORDER BY 3 DESC";
                             
      $sql_paga->addParam(":contrato",$contrato);
      $sql_paga->executeQuery($txt_paga);
                          
      $sql_paga2 = new Query();
      $txt_paga2 = "SELECT SUM(VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,7)) VALOR,
                           SUM(VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,8)) ADESAO,
                           SUM(VALOR_TOTAL_FATURADO(HSSUSUPG.NNUMEPAGA,HSSUSUPG.NNUMEUSUA,9)) ADITIVO
                      FROM HSSUSUPG
                     WHERE NNUMEPAGA = :pagamento ";
                     
      $sql_paga2->addParam(":pagamento",$sql_paga->result("NNUMEPAGA"));
      
      if ($usuario > 0) {
        $txt_paga2 .= "   AND NNUMEUSUA = :usuario ";    
        $sql_paga2->addParam(":usuario",$usuario);
      }
        
      $sql_paga2->executeQuery($txt_paga2);              

      $valor = str_replace(',','.',$sql_paga2->result("VALOR"));
      
      if ($adesao <> 'N')
        $valor += str_replace(',','.',$sql_paga2->result("ADESAO"));
        
      if ($aditivo == 'S')
        $valor += str_replace(',','.',$sql_paga2->result("ADITIVO"));
                
      $dados['documento'] = $sql_paga->result("NDOCUPAGA");
      $dados['valor'] = $valor;
      
      if ($sql_paga->result("CPAGOPAGA") == 'N'){
        if ($_SESSION['apelido_operadora'] == 'unimedFranca')
          $dados['pagamento'] = ' Inadimplente';
        else   
          $dados['pagamento'] = ' Em aberto'; 
      }else
        $dados['pagamento'] = $sql_paga->result("DPAGAPAGA");
        
      return $dados;
    }  

    function segmentacaoProposta($id) {
      $sql = new Query();
      $txt = "SELECT DISTINCT CCOBEPLAN
                FROM HSSIPROP,HSSPLAN
               WHERE HSSIPROP.NNUMEPROSP = :proposta
                 AND HSSIPROP.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";
      $sql->addParam(":proposta",$id);   
      $sql->executeQuery($txt);   

      return $sql->result("CCOBEPLAN");  }

    function segmentacaoPedido($id) {
      $sql = new Query();
      $txt = "SELECT DISTINCT CCOBEPLAN
                FROM HSSIPROP,HSSPLAN
               WHERE HSSIPROP.NNUMEPPROP = :pedido           
                 AND HSSIPROP.NNUMEPLAN = HSSPLAN.NNUMEPLAN ";
      $sql->addParam(":pedido",$id);   
      $sql->executeQuery($txt);   

      return $sql->result("CCOBEPLAN");  
    }

    function situacaoPedido($bd,$pedido) {

      $sql = new Query($bd);
      $txt = "SELECT TO_CHAR(DDATAPPROP,'DD/MM/YYYY') DDATAPPROP,
                     TO_CHAR(DGERAPAGA,'DD/MM/YYYY') DGERAPAGA,
                     TO_CHAR(DIMPRPPROP,'DD/MM/YYYY') DIMPRPPROP,
                     HSSPPROP.NNUMEPAGA,HSSPPROP.CSITUPPROP,CCODITITU
                FROM HSSPPROP,HSSPAGA,HSSTITU
               WHERE HSSPPROP.NNUMEPPROP = :pedido
                 AND HSSPPROP.NNUMEPAGA = HSSPAGA.NNUMEPAGA(+)
                 AND HSSPPROP.NNUMETITU = HSSTITU.NNUMETITU(+) ";
      $sql->addParam(":pedido",$pedido);   
      $sql->executeQuery($txt);             

      if ($sql->result("CSITUPPROP") == 'A') {
        $situacao['texto'] =  "Pedido finalizado. Contrato n� ".$sql->result("CCODITITU");
        $situacao['cor'] = 'navy';    
      } else if ($sql->result("CSITUPPROP") == 'F') {
        $situacao['texto'] =  "Encerrado inclus�o de benefici�rios";
        $situacao['cor'] = '#9900CC';   
      } else if ($sql->result("DIMPRPPROP") <> '') {
        if ($this->segmentacaoPedido($pedido) == '04')
          $situacao['texto'] =  "SECAD impressa em ".$sql->result("DIMPRPPROP");
        else
          $situacao['texto'] =  "SECAM impressa em ".$sql->result("DIMPRPPROP");
          
        $situacao['cor'] = '#0000FF';  
      } else if ($sql->result("NNUMEPAGA") > 0) {
        $situacao['texto'] =  "Boleto gerado em ".$sql->result("DGERAPAGA");
        $situacao['cor'] = 'olive';
      } else {
        $situacao['texto'] =  "Pedido efetuado em: ".$sql->result("DDATAPPROP");
        $situacao['cor'] = 'orange';
      }
        
      return $situacao;
    }
    /*Fim Fun��es Comercial*/    

    /* planos */
    function carregaPlanos($bd,$tpl,$selecionado) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT NNUMEPLAN,CDESCPLAN,CCODIPLAN
                FROM HSSPLAN
               WHERE CSITUPLAN = 'A'
               ORDER BY 1 ";
      $sql->executeQuery($txt);
          
      while (!$sql->eof()) {
        $tpl->PLANO_ID = $sql->result("NNUMEPLAN");
        $tpl->PLANO_NOME = $sql->result("CCODIPLAN")." - ".$sql->result("CDESCPLAN");
        
        if ($selecionado == $sql->result("NNUMEPLAN"))
          $tpl->PLANO_SELECIONADO = "selected";
        else
          $tpl->PLANO_SELECIONADO = "";
          
        $tpl->block("PLANO");
        $sql->next();
      }
    }

    /* estabelecimentos */
    function carregaEstabelecimentos($bd,$tpl,$selecionado) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT NNUMEESTAB,CNOMEESTAB
                FROM SFPESTAB
               WHERE CSITUESTAB = 'A'
               ORDER BY 1 ";
      $sql->executeQuery($txt);
          
      while (!$sql->eof()) {
        $tpl->ESTABELECIMENTO_ID = $sql->result("NNUMEESTAB");
        $tpl->ESTABELECIMENTO_NOME = $sql->result("CNOMEESTAB");
        
        if ($selecionado == $sql->result("NNUMEESTAB"))
          $tpl->ESTABELECIMENTO_SELECIONADO = "selected";
        else
          $tpl->ESTABELECIMENTO_SELECIONADO = "";
          
        $tpl->block("FILTRO_ESTABELECIMENTO");
        $sql->next();
      }
    }

    /* unidades comerciais */
    function carregaUnidades($bd,$tpl,$selecionado) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT NNUMEUNCOM,CDESCUNCOM
                FROM HSSUNCOM
               ORDER BY 1 ";
      $sql->executeQuery($txt);
          
      while (!$sql->eof()) {
        $tpl->UNIDADE_ID = $sql->result("NNUMEUNCOM");
        $tpl->UNIDADE_NOME = $sql->result("CDESCUNCOM");
        
        if ($selecionado == $sql->result("NNUMEUNCOM"))
          $tpl->UNIDADE_SELECIONADO = "selected";
        else
          $tpl->UNIDADE_SELECIONADO = "";
          
        $tpl->block("FILTRO_UNIDADE");
        $sql->next();
      }
    }

    function situacaoProposta($bd,$proposta) {

      $sql = new Query($bd);
      $txt = "SELECT TO_CHAR(DDATAPROPO,'DD/MM/YYYY') DDATAPROPO,
                     TO_CHAR(DVALIPROPO,'DD/MM/YYYY') DVALIPROPO,
                     TO_CHAR(DCANCPROPO,'DD/MM/YYYY') DCANCPROPO,
                     TO_CHAR(DIMPRPROPO,'DD/MM/YYYY') DIMPRPROPO,
                     CSITUPROPO,HSSPPROP.NNUMEPPROP,TO_CHAR(DDATAPPROP,'DD/MM/YYYY') DDATAPPROP,
                     CASE WHEN DVALIPROPO < SYSDATE THEN 1 ELSE 0 END VENCIDO,
                     HSSPPROP.NNUMEPPROP, HSSPAGA.DPAGAPAGA
                FROM HSSPROPO,HSSPPROP,HSSPAGA
               WHERE HSSPROPO.NNUMEPROPO = :proposta 
                 AND HSSPROPO.NNUMEPROPO = HSSPPROP.NNUMEPROPO(+) 
                 AND HSSPPROP.NNUMEPAGA = HSSPAGA.NNUMEPAGA(+) ";
      $sql->addParam(":proposta",$proposta);   
      $sql->executeQuery($txt);             

      if ($sql->result("CSITUPROPO") == 'I') {
        if ($sql->result("VENCIDO") == 1) {
          $situacao['texto'] =  "Proposta vencida em: ".$sql->result("DVALIPROPO");
          $situacao['cor'] = '#FA16BB';
        } else if ($sql->result("DIMPRPROPO") <> '') {
          $situacao['texto'] =  "Proposta impressa em: ".$sql->result("DIMPRPROPO");
          $situacao['cor'] = 'brown';      
        } else {
          $situacao['texto'] =  "Proposta efetuada em: ".$sql->result("DDATAPROPO");
          $situacao['cor'] = '#363636';    
        }
      } else if ($sql->result("CSITUPROPO") == 'P') {
        $pedido = $this->situacaoPedido($bd,$sql->result("NNUMEPPROP"));
        $situacao['texto'] =  $pedido['texto'];
        $situacao['cor'] = $pedido['cor'];
      } else if ($sql->result("CSITUPROPO") == 'G') {
        $situacao['texto'] =  "Boleto gerado";
        $situacao['cor'] = 'olive';
      } else if ($sql->result("CSITUPROPO") == 'T') {
       
      } else if ($sql->result("CSITUPROPO") == 'L') {
        $situacao['texto'] =  "Liberado para a inclus�o de benefici�rios";
        $situacao['cor'] = 'green';
      } else if ($sql->result("CSITUPROPO") == 'X') {
        $situacao['texto'] =  "Proposta cancelada em: ".$sql->result("DCANCPROPO");
        $situacao['cor'] = 'red';
      } else if ($sql->result("CSITUPROPO") == 'F') {
        $situacao['texto'] =  "Proposta finalizada.";
        $situacao['cor'] = 'navy';
      }
        
      if ($sql->result("DPAGAPAGA") <> '') {
        $situacao['texto'] .= ' - boleto pago em '.($sql->result("DPAGAPAGA"));
      }
      return $situacao;
    }

    function corrigeCaracter($texto) {
      $texto = str_replace('?o','�o',$texto);
      $texto = str_replace('?O','�O',$texto);
      return $texto;
    }
    
    function descDiaVencimento($vencimento) {
      if ($vencimento > 0)
        $descricao = $vencimento;
      else {
        switch ($vencimento) {
          case  -1 : $descricao = 'Primeiro dia �til';    break;
          case  -2 : $descricao = 'Segundo dia �til';     break;
          case  -3 : $descricao = 'Terceiro dia �til';    break;
          case  -4 : $descricao = 'Quarto dia �til';      break;
          case  -5 : $descricao = 'Quinto dia �til';      break;
          case -15 : $descricao = '15 dias ap�s gera��o'; break;
          case -20 : $descricao = '20 dias ap�s gera��o'; break;
          case -30 : $descricao = '30 dias ap�s gera��o'; break;
        }
      } 
      
      return $descricao;
    }
    
    function somenteNumeros($variavel) {
      $indice = 0;
      $retorno = "";
      while ($indice < strlen($variavel)) {
        if (substr_count("0123456789",substr($variavel,$indice,1)) > 0)
          $retorno = $retorno.substr($variavel,$indice,1);
          
        $indice = $indice + 1;
      }
      
      return $retorno;
    }
    function atualizaPedido($bd,$pedido,$vigencia,$cep,$tipo_logradouro,$endereco,$numero,$complemento,
                             $bairro,$cidade,$estado,$banco,$agencia,$conta,$correntista,$vencimento,$faturar,
                             $subsidio_s,$subsidio_f,$subsidio_o,$subsidio_d,$subsidio_q,
                             $regra_adesao,$observacao,$dia_vencimento) {

      $sql = new Query($bd);
      $txt = "UPDATE HSSPPROP
                 SET DVIGEPPROP = :vigencia,
                     CENDEPPROP = :endereco,
                     NNUMETLOGR = :tipo_logradouro,
                     CCEP_PPROP = :cep,
                     CNUMEPPROP = :numero,
                     CCOMPPPROP = :complemento,
                     CBAIRPPROP = :bairro,
                     CCIDAPPROP = :cidade,
                     CESTAPPROP = :estado,
                     NNUMECDBA  = :banco,
                     CAGENPPROP = :agencia,
                     CCC__PPROP = :conta,
                     CCORRPPROP = :correntista,
                     DVENCPPROP = :vencimento,
                     CFATUPPROP = :faturar,
                     NSUBSPPROP = :subsidio_s,
                     NSUBFPPROP = :subsidio_f,
                     NSUBOPPROP = :subsidio_o,
                     NSUBDPPROP = :subsidio_d,
                     NSUBQPPROP = :subsidio_q,
                     CREGAPPROP = :regra_adesao,
                     COBSEPPROP = :observacao,
                     NDIA_VENCI = :dia_vencimento
               WHERE NNUMEPPROP = :pedido ";
      $sql->addParam(":vigencia",$vigencia);
      $sql->addParam(":endereco",$endereco);
      $sql->addParam(":tipo_logradouro",$tipo_logradouro);
      $sql->addParam(":cep",$this->somenteNumeros($cep));
      $sql->addParam(":numero",$numero);
      $sql->addParam(":complemento",$complemento);
      $sql->addParam(":bairro",$bairro);
      $sql->addParam(":cidade",$cidade);
      $sql->addParam(":estado",$estado);
      $sql->addParam(":banco",$banco);
      $sql->addParam(":agencia",$agencia);
      $sql->addParam(":conta",$conta);
      $sql->addParam(":correntista",$correntista);
      $sql->addParam(":pedido",$pedido);
      $sql->addParam(":vencimento",$vencimento);
      $sql->addParam(":faturar",$faturar);
      $sql->addParam(":subsidio_s",$subsidio_s);
      $sql->addParam(":subsidio_f",$subsidio_f);
      $sql->addParam(":subsidio_o",$subsidio_o);
      $sql->addParam(":subsidio_d",$subsidio_d);
      $sql->addParam(":subsidio_q",$subsidio_q);
      $sql->addParam(":regra_adesao",$regra_adesao);
      $sql->addParam(":observacao",$observacao);
      $sql->addParam(":dia_vencimento",$dia_vencimento);
      $erro = $sql->executeSQL($txt);
      
      return $erro;            
    }

    function alteraUsuarioComercial ($bd,$id,$id_contrato,$nome,$categoria,$nascimento,$sexo,$grau,$cpf,$rg,$expedicao,$orgao,$pis,$mae,$pai,$titular,
                                       $matricula,$salario,$admissao,$locacao,$plano,$acomodacao,$cidadenasc,$estadonasc,
                                       $telefone1,$telefone2,$celular,$email,
                                       $cep,$tipo_logradouro,$logradouro,$numero,$complemento,$bairro,$estado,$cidade,$inclusao,
                                       $estado_civil,$profissao,$setor,$vendedor,$id_proposta,$valor,$csus,$ndnv) {
      
      $cpf = $this->somenteNumeros($cpf);
      $pis = $this->somenteNumeros($pis);
      $cep = $this->somenteNumeros($cep);
      $csus = $this->somenteNumeros($csus);
      $ndnv = $this->somenteNumeros($ndnv);
        
      if ($categoria == 'T' or $categoria == 'F')
        $grau = 'X';
        
      $sql = new Query($bd);
      
      $retorno = array();
      
      
      $txt = "SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
               WHERE C_CPFUSUA = :cpf
                 AND CTIPOUSUA = :categoria
                 AND CSITUUSUA = 'A'
                 AND NNUMETITU = :contrato
               UNION ALL
              SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
               WHERE C_CPFUSUA = :cpf
                 AND NTITUUSUA = :titular
                 AND CSITUUSUA = 'A'
                 AND NNUMETITU = :contrato           
               UNION ALL
              SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
               WHERE C_CPFATCAD = :cpf
                 AND CTIPOATCAD = :categoria
                 AND CFLAGATCAD IS NULL
                 AND COPERATCAD = 'I'
                 AND NNUMETITU = :contrato
                 AND NNUMEATCAD <> :atcad
               UNION ALL
              SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
               WHERE C_CPFATCAD = :cpf
                 AND NTITUUSUA = :titular
                 AND CFLAGATCAD IS NULL
                 AND COPERATCAD = 'I'
                 AND NNUMETITU = :contrato        
                 AND NNUMEATCAD <> :atcad             
               UNION ALL
              SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
               WHERE C_CPFATCAD = :cpf
                 AND CTIPOATCAD = :categoria
                 AND CFLAGATCAD IS NULL
                 AND COPERATCAD = 'I'
                 AND NNUMEPPROP = :proposta
                 AND NNUMEATCAD <> :atcad             
               UNION ALL
              SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
               WHERE C_CPFATCAD = :cpf
                 AND NTITUUSUA = :titular
                 AND CFLAGATCAD IS NULL
                 AND COPERATCAD = 'I'
                 AND NNUMEATCAD <> :atcad             
                 AND NNUMEPPROP = :proposta ";
             
      $sql->clear();
      $sql->addParam(":cpf",$cpf);
      $sql->addParam(":categoria",$categoria);
      $sql->addParam(":titular",$titular);
      $sql->addParam(":contrato",$id_contrato);
      $sql->addParam(":proposta",$id_proposta);
      $sql->addParam(":atcad",$id);  
      $sql->executeQuery($txt);
      
      if ($sql->count() > 0) {
        $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio. CPF em duplicidade! (".$sql->result("CNOMEUSUA").").";
      } else {
        $txt = "SELECT CNOMEUSUA,NNUMEUSUA, DNASCUSUA FROM HSSUSUA
                 WHERE (CNOMEUSUA = :nome AND TO_CHAR(DNASCUSUA,'DD/MM/YYYY') = :nascimento)
                   AND CTIPOUSUA = :categoria
                   AND CSITUUSUA = 'A'
                   AND NNUMETITU = :contrato
                 UNION ALL
                SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                 WHERE (CNOMEATCAD = :nome AND TO_CHAR(DNASCATCAD,'DD/MM/YYYY') = :nascimento)
                   AND CTIPOATCAD = :categoria
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I'
                   AND NNUMEATCAD <> :atcad               
                   AND NNUMETITU = :contrato 
                 UNION ALL
                SELECT CNOMEATCAD,NNUMEUSUA, DNASCATCAD FROM HSSATCAD
                 WHERE (CNOMEATCAD = :nome AND TO_CHAR(DNASCATCAD,'DD/MM/YYYY') = :nascimento)
                   AND CTIPOATCAD = :categoria
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I'
                   AND NNUMEATCAD <> :atcad               
                   AND NNUMEPPROP = :proposta ";               
                   
        $sql->clear();
        $sql->addParam(":nome",$nome);
        $sql->addParam(":nascimento",$nascimento);    
        $sql->addParam(":categoria",$categoria);
        $sql->addParam(":contrato",$id_contrato);
        $sql->addParam(":proposta",$id_proposta);
        $sql->addParam(":atcad",$id);      
        $sql->executeQuery($txt);  
           
        if ($sql->count() > 0) {
          $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio. Cadastro em duplicidade! (".$sql->result("CNOMEUSUA").").";
        } else { 
          if (($categoria == 'T') or ($categoria == 'F')) {
            $txt = "SELECT NNUMEUSUA FROM HSSUSUA
                     WHERE CTIPOUSUA = 'T'
                       AND CSITUUSUA = 'A'
                       AND NNUMETITU = :contrato
                       AND CCHAPUSUA = :matricula
                     UNION ALL
                    SELECT NNUMEUSUA FROM HSSATCAD
                     WHERE CTIPOATCAD = 'T'
                       AND CFLAGATCAD IS NULL
                       AND COPERATCAD = 'I'
                       AND NNUMETITU = :contrato
                       AND NNUMEATCAD <> :atcad
                       AND CCHAPATCAD = :matricula
                     UNION ALL
                    SELECT NNUMEUSUA FROM HSSATCAD
                     WHERE CTIPOATCAD = 'T'
                       AND CFLAGATCAD IS NULL
                       AND COPERATCAD = 'I'
                       AND NNUMEPPROP = :proposta
                       AND NNUMEATCAD <> :atcad
                       AND CCHAPATCAD = :matricula ";
                       
            $sql->clear();
            $sql->addParam(":matricula",$matricula);
            $sql->addParam(":contrato",$id_contrato);
            $sql->addParam(":proposta",$id_proposta);      
            $sql->addParam(":atcad",$id);          
            $sql->executeQuery($txt); 
                 
            if ($sql->count() > 0) {
              $continua = false;
            } else {
              $continua = true;
            }
          } else {
            $continua = true;
          }
          
          if (!$continua) {
            $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, pois j� existe um usu�rio com a mesma matr�cula.";
          } else {  
            $txt = "SELECT HSSTITU.NNUMETITU
                      FROM HSSTITU,HSSPLAN
                     WHERE HSSTITU.NNUMETITU = :contrato
                       AND (
                            (:categoria = 'D' AND :sexo = 'M' AND :grau <> 'E' AND :grau <> 'J' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMMTITU,NVL(NLDEMPLAN,999))) OR
                            (:categoria = 'D' AND :sexo = 'F' AND :grau <> 'E' AND :grau <> 'J' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMFTITU,NVL(NLDEFPLAN,999))) OR
                            (:categoria = 'U' AND :sexo = 'M' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMUTITU,NVL(NLDEUPLAN,999))) OR
                            (:categoria = 'U' AND :sexo = 'F' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIUFTITU,NVL(NLDUFPLAN,999))) OR
                            (:categoria = 'A' AND IDADE(TO_DATE(:nascimento,'DD/MM/YYYY'),LAST_DAY(SYSDATE)) >= NVL(NLIMATITU,NVL(NLAGRPLAN,999)))
                            )
                       AND HSSPLAN.NNUMEPLAN = :plano
                       AND NVL(DLIMIPLAN,SYSDATE) <= SYSDATE ";
                       
            $sql->clear();
            $sql->addParam(":categoria",$categoria);
            $sql->addParam(":sexo",$sexo);
            $sql->addParam(":grau",$grau);
            $sql->addParam(":nascimento",$nascimento);        
            $sql->addParam(":plano",$plano);         
            $sql->addParam(":contrato",$id_contrato);
            $sql->executeQuery($txt);   

            if ($sql->count() > 0) {
              $retorno['erro'] = "Este usu�rio ultrapassa a idade limite para este contrato.<br />
                                  Verifique os campos de data de nascimento, categoria do usu�rio, grau de parentesco e sexo.";
            } else {   
              if (($grau == "E") or ($grau == "J")) {        
                $txt = "SELECT COUNT(1) QTDE
                          FROM (SELECT NNUMEUSUA FROM HSSUSUA
                                 WHERE CSITUUSUA = 'A'
                                   AND CGRAUUSUA IN ('E','J')
                                   AND NTITUUSUA = :titular 
                                 UNION
                                SELECT NNUMEUSUA FROM HSSATCAD
                                 WHERE CFLAGATCAD IS NULL
                                   AND COPERATCAD = 'I'  
                                   AND NNUMEATCAD <> :atcad
                                   AND CGRAUATCAD IN ('E','J')
                                   AND NTITUUSUA = :titular) ";
                           
                $sql->clear();
                $sql->addParam(":titular",$titular);
                $sql->addParam(":atcad",$id);            
                $sql->executeQuery($txt);   

                if ($sql->result("QTDE") > 0) {
                  $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, pois j� existe um dependente com o grau de parentesco \"Esposa(o)\".";
                  $continua = false;
                } else {   
                  $continua = true;
                }
              } else {
                $continua = true;
              } 

              if (($continua) and ($categoria == 'T' or $categoria == 'F') and ($id_proposta > 0)) {
              
                $sql->clear();
                $txt = "SELECT CPESSPROSP NJ FROM HSSPPROP,HSSPROPO,HSSPROSP
                         WHERE HSSPPROP.NNUMEPPROP = :proposta
                           AND HSSPPROP.NNUMEPROPO = HSSPROPO.NNUMEPROPO
                           AND HSSPROPO.NNUMEPROSP = HSSPROSP.NNUMEPROSP ";
                            
                $sql->addParam(":proposta",$id_proposta);          
                $sql->executeQuery($txt);   

                if ($sql->result("NJ") == "F") {
                
                  $txt = "SELECT COUNT(1) QTDE
                            FROM (SELECT NNUMEUSUA FROM HSSATCAD
                                   WHERE CFLAGATCAD IS NULL
                                     AND COPERATCAD = 'I'  
                                     AND CTIPOATCAD IN ('T','F')
                                     AND NNUMEATCAD <> :id
                                     AND NNUMEPPROP = :proposta) ";
                             
                  $sql->clear();
                  $sql->addParam(":proposta",$id_proposta);
                  $sql->addParam(":id",$id);
                  $erro = $sql->executeQuery($txt);
                  
                  if ($erro <> '') {
                    $retorno['erro'] = $erro;
                    $continua = false;
                  } else if ($sql->result("QTDE") > 0) {
                    $retorno['erro'] = "N�o � poss�vel cadastrar esse novo usu�rio, pois j� existe um titular cadastrado.";
                    $continua = false;
                  } else
                    $continua = true;
                } else
                  $continua = true;
              }  
              
              if ($continua) {
               
               $sql->clear();
                $txt = "SELECT DNASCATCAD FROM HSSATCAD
                         WHERE NNUMEATCAD = :atcad ";
                $sql->addParam(":atcad",$id);            
                $sql->executeQuery($txt);
                $nascimento_anterior = $sql->result("DNASCATCAD");
                
                $sql->clear();            
                $txt = "UPDATE HSSATCAD
                           SET CNOMEATCAD = :nome,
                               CCHAPATCAD = :matricula,
                               DNASCATCAD = :nascimento,
                               CESTNATCAD = :estadonasc,
                               CCIDNATCAD = :cidadenasc,
                               CPIS_ATCAD = :pis,
                               C_CPFATCAD = :cpf,
                               C__RGATCAD = :rg,
                               DADMIATCAD = :admissao,
                               CORRGATCAD = :orgao,
                               CGRAUATCAD = :grau,
                               CTIPOATCAD = :categoria,
                               CNMAEATCAD = :mae,
                               CNPAIATCAD = :pai,
                               CSEXOATCAD = :sexo,
                               CENDEATCAD = :endereco,
                               CBAIRATCAD = :bairro,
                               CCIDAATCAD = :cidade,
                               CESTAATCAD = :estado,
                               CCEP_ATCAD = :cep,
                               DEXRGATCAD = :expedicao,
                               CCOMPATCAD = :complemento,
                               CNUMEATCAD = :numero,
                               NFATUTLOGR = :tipo_logradouro,
                               CMAILATCAD = :email,
                               CFONEATCAD = :telefone1,
                               NSALAATCAD = :salario,
                               CESTCATCAD = :estado_civil,
                               NNUMEPROF  = :profissao,
                               CIP__ATCAD = :ip,
                               NNUMEFUNSE = :setor,
                               CSUS_ATCAD = :csus,
                               NDCNVATCAD = :ndnv
                         WHERE NNUMEATCAD = :atcad ";

                $sql->addParam(":nome",$nome);
                $sql->addParam(":matricula",$matricula);
                $sql->addParam(":nascimento",$nascimento);
                $sql->addParam(":estadonasc",$estadonasc);
                $sql->addParam(":cidadenasc",$cidadenasc);
                $sql->addParam(":pis",$pis);
                $sql->addParam(":cpf",$cpf);
                $sql->addParam(":rg",$rg);
                $sql->addParam(":admissao",$admissao);
                $sql->addParam(":orgao",$orgao);
                $sql->addParam(":grau",$grau);            
                $sql->addParam(":categoria",$categoria);
                $sql->addParam(":mae",$mae);
                $sql->addParam(":pai",$pai);
                $sql->addParam(":sexo",$sexo);
                $sql->addParam(":endereco",$logradouro);
                $sql->addParam(":bairro",$bairro);
                $sql->addParam(":cidade",$cidade);
                $sql->addParam(":estado",$estado);
                $sql->addParam(":cep",$cep);
                $sql->addParam(":expedicao",$expedicao);            
                $sql->addParam(":complemento",$complemento);  
                $sql->addParam(":numero",$numero);            
                $sql->addParam(":tipo_logradouro",$tipo_logradouro);            
                $sql->addParam(":email",$email);            
                $sql->addParam(":telefone1",$telefone1);            
                $sql->addParam(":salario",$salario);           
                $sql->addParam(":ip",getenv("REMOTE_ADDR"));            
                $sql->addParam(":estado_civil",$estado_civil);
                $sql->addParam(":profissao",$profissao);
                $sql->addParam(":setor",$setor);
                $sql->addParam(":csus",$csus);
                $sql->addParam(":ndnv",$ndnv);            
                $sql->addParam(":atcad",$id);            
                $erro = $sql->executeSQL($txt);  
   
                $retorno['erro'] = $erro;
                
                $sql->clear();
                $txt = "SELECT CSITUPPROP SITUACAO FROM HSSPPROP
                         WHERE NNUMEPPROP = :id ";
                $sql->addParam(":id",$id_proposta);
                $sql->executeQuery($txt);
                  
                if ($sql->result("SITUACAO") == 'F') {
                  $sql->clear();
                  $txt = "SELECT VALIDA_PEDIDO_COMERCIAL(:id) VALIDO FROM DUAL ";
                  $sql->addParam(":id",$id_proposta);
                  $sql->executeQuery($txt);
                  
                  if ($sql->result("VALIDO") <> '') {
                    $sql->clear();            
                    $txt = "UPDATE HSSATCAD
                               SET DNASCATCAD = :nascimento 
                             WHERE NNUMEATCAD = :atcad ";
                    $sql->addParam(":nascimento",$nascimento_anterior);            
                    $sql->addParam(":atcad",$id);            
                    $erro = $sql->executeSQL($txt);  
                    
                    $retorno['erro'] = str_replace(chr(13),'<br>',$sql->result("VALIDO"));
                    
                   
                  }

                }
                
              }
            }        
          }
        }
      }
                
      return $retorno;
    }
    
    function salvaGuiaErro($guia,$erroGuias,$erroBeneficiario) {
          // Abre o arquivo para leitura e escrita
           $f = fopen("../anexos/errosGuias/".$guia.'_'.$_SESSION['id_operador'].".txt", "wr+");
          // Escreve no arquivo
          fwrite($f, $guia."\n".$erroGuias."\n".$erroBeneficiario."\n");
          // Libera o arquivo
         
          fclose($f);
    }    
  
    function verificaUsuarioResponsavelBolet ($bd,$usuario) {
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT NVL(NNUMEUSUA,0) USUARIO
                FROM HSSBOLET
               WHERE CRESPBOLET = 'S'
                 AND NNUMEUSUA = :usuario";
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);
/*      
      if (($_SESSION["apelido_operadora"] == "casf") and ($sql->result("USUARIO") > 0)) {
        $sql->clear();
        $txt = "SELECT NVL(HSSUSUA.NNUMEUSUA,0) USUARIO
                  FROM HSSPAGA,HSSUSUA
                 WHERE HSSUSUA.NNUMEUSUA = :usuario 
                   AND CSITUUSUA <> 'A'
                   AND CTIPOUSUA NOT IN ('T','F')
                   AND HSSUSUA.NNUMEUSUA = HSSPAGA.NNUMEUSUA"; 
        $sql->addParam(":usuario",$usuario);
        $sql->executeQuery($txt);
      }
*/
      return $sql->result("USUARIO");    
    }
    
    function verificaUsuarioBolet ($bd,$usuario){
      $sql = new Query($bd);
      $sql->clear();
      $txt = "SELECT NVL(NNUMEUSUA,0) USUARIO
                FROM HSSBOLET
               WHERE NNUMEUSUA = :usuario ";
      $sql->addParam(":usuario",$usuario);
      $sql->executeQuery($txt);
      
      return $sql->result("USUARIO");
    }
  
  function Hora_Final_Atendimento($bd,$guia) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = "SELECT CHFIMGUIA FROM HSSGUIA WHERE NNUMEGUIA = :guia";
  
    $sql->addParam(":guia",$guia);
    $sql->executeQuery($txt);
  
    return $sql->result("CHFIMGUIA");
  } 
  
  function retorna_nome_operador($bd,$id) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = "SELECT CNOMEUSUA FROM SEGUSUA WHERE NNUMEUSUA = :id ";
    $sql->addParam(":id",$id);
    $sql->executeQuery($txt);
    return $sql->result("CNOMEUSUA");
  }
  
  function dados_para_agenda($bd,$usuario,&$pip_plano,&$situacao,&$codigo_usuario,&$codigo_plano,&$tipo_usuario,&$digital) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CNOMEUSUA,HSSUSUA.NNUMEPLAN,CSITUUSUA,CCODIUSUA,CCODIPLAN,CDINDUSUA,
          DECODE(CTIPOUSUA,'T','Titular','D','Dep','U','Dep Univ','E','Dep Def','L','Dep Leg','F','Tit Finac','A','Agregado') Tipo_Usuario
            FROM HSSUSUA,HSSPLAN
           WHERE NNUMEUSUA = :usuario
             AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN";
   
  $sql->addParam(":usuario",$usuario);
  $sql->executeQuery($txt);  
    

  $pid_plano      = $sql->result("NNUMEPLAN");
  $situacao       = $sql->result("CSITUUSUA");
  $codigo_usuario = $sql->result("CCODIUSUA");
  $codigo_plano   = $sql->result("CCODIPLAN");
  $tipo_usuario   = $sql->result("TIPO_USUARIO");
  $digital        = $sql->result("CDINDUSUA");
  return   $sql->result("CNOMEUSUA");
}

  function dadosParaAgenda($bd,$usuario) {
  //Utilizado tamb�m para o comprovante Unimed Leste
    $sql = new Query($bd);
    $txt = "SELECT CNOMEUSUA,HSSUSUA.NNUMEPLAN,CSITUUSUA,CCODIUSUA,CCODIPLAN,CDINDUSUA,
                   DECODE(CTIPOUSUA,'T','Titular','D','Dep','U','Dep Univ','E','Dep Def','L','Dep Leg','F','Tit Finac','A','Agregado') Tipo_Usuario,
                   NVL(HSSCONGE.CRAZACONGE,'N') CRAZACONGE,NVL(HSSCONGE.NNUMECONGE,0) NNUMECONGE,NVL(HSSEMPR.CFANTEMPR,HSSCONGE.CRAZACONGE) CFANTEMPR
              FROM HSSUSUA,HSSPLAN,HSSTITU,HSSCONGE,HSSEMPR
             WHERE NNUMEUSUA = :usuario
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
               AND HSSUSUA.NNUMETITU  = HSSTITU.NNUMETITU(+)
               AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+)";
   
    $sql->addParam(":usuario",$usuario);
    $sql->executeQuery($txt);  
   
    $retorno = array();
    $retorno['pid_plano']      = $sql->result("NNUMEPLAN");
    $retorno['situacao']       = $sql->result("CSITUUSUA");
    $retorno['codigo_usuario'] = $sql->result("CCODIUSUA");
    $retorno['codigo_plano']   = $sql->result("CCODIPLAN");
    $retorno['tipo_usuario']   = $sql->result("TIPO_USUARIO");
    $retorno['digital']        = $sql->result("CDINDUSUA");
    $retorno['nome']           = $sql->result("CNOMEUSUA");
    $retorno['empresa']        = $sql->result("CRAZACONGE");
    $retorno['congenere']      = $sql->result("NNUMECONGE");
    $retorno['fantasia']       = $sql->result("CFANTEMPR");
    
    return $retorno;
  }

function Retorna_Nome_NaoUsuario($bd,$nusuario) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CNOMENUSUA FROM HSSNUSUA
           WHERE NNUMENUSUA = :nusuario";
           
  $sql->addParam(":nusuario",$nusuario);
  $sql->executeQuery($txt); 
  
  return $sql->result("CNOMENUSUA");
}

function retorna_bloqueio_agenda($bd,$bloqueios) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CMOTIBLOQU FROM HSSBLOQU
           WHERE NNUMEBLOQU = :bloqueios";
  $sql->addParam(":bloqueios",$bloqueios);
  $sql->executeQuery($txt);  
 $motivo = $sql->result("CMOTIBLOQU");
  return $motivo;
}

function Retorna_Operador_Bloqueio_Agenda($bd,$bloqueio) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CNOMEUSUA FROM HSSBLOQU, SEGUSUA
           WHERE NNUMEBLOQU = :bloqueio
             AND HSSBLOQU.NNUMEUSUA = SEGUSUA.NNUMEUSUA(+)";
  
  $sql->addParam(":bloqueio",$bloqueio);
  $sql->executeQuery($txt);  

  return $sql->result("CNOMEUSUA");
}
  
function ProcedimentosAgendados($bd,$idprocedimento) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CCODIPMED FROM HSSPRAGE
           WHERE NNUMEAGEND = :idprocedimento";
  $sql->addParam(":idprocedimento",$idprocedimento);
  $sql->executeQuery($txt);
  $procedimentos ="";
  while (!$sql->eof()) {
    $procedimentos +=$sql->result("CCODIPMED")." ";
  $sql->next();
  }
  
  return $procedimentos;
} 

function Operador_Guia($bd,$guia,$dataimpre) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CNOMEUSUA,NVL(DIMPRGUIA,DEMISGUIA) DIMPRGUIA FROM HSSGUIA,SEGUSUA
           WHERE NNUMEGUIA = :guia";
           
  $sql->addParam(":guia",$guia);
  $sql->executeQuery($txt);

  $dataimpre = $sql->result("DIMPRGUIA");
  return $sql->result("CNOMEUSUA");
}

function nomeProfissao($bd,$profissao) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CDESCPROF FROM HSSPROF
           WHERE NNUMEPROF = :id";
           
  $sql->addParam(":id",$profissao);
  $sql->executeQuery($txt);

  return $sql->result("CDESCPROF");
}

function Retorna_Convenio_NaoUsuario($bd,$naousuario) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CNOMECONVE FROM HSSNUSUA,HSSCONVE
           WHERE NNUMENUSUA = :naousuario
             AND HSSNUSUA.NNUMECONVE = HSSCONVE.NNUMECONVE";
           
  $sql->addParam(":naousuario",$naousuario);
  $sql->executeQuery($txt);
  
  if ($sql->result("CNOMEUSUA") == ""){
    $sql->clear();
    $txt = "SELECT CRAZAEMPR FROM HSSNUSUA,HSSTITU,HSSEMPR
             WHERE NNUMENUSUA = :naousuario
               AND HSSNUSUA.NNUMETITU = HSSTITU.NNUMETITU
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR";
           
    $sql->addParam(":naousuario",$naousuario);
    $sql->executeQuery($txt);
    
    return $sql->result("CRAZAEMPR");
  }
  else{
    return $sql->result("CNOMEUSUA");
  }
}

function Retorna_Prontuario_Ambulatorio($bd,$ambulatorio,$usuario) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = "SELECT NPRONAMBUS FROM HSSAMBUS
             WHERE NNUMEPRES = :ambulatorio
               AND NNUMEUSUA = :usuario";
             
    $sql->addParam(":ambulatorio",$ambulatorio);
    $sql->addParam(":usuario",$usuario);
    $sql->executeQuery($txt);
    
    return $sql->result("NPRONAMBUS");
}

function RetornaNomeProcedimento($bd,$codigo) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT CNOMEPMED FROM HSSPMED
       WHERE CCODIPMED = :codigo";
       
  $sql->addParam(":codigo",$codigo);
  $sql->executeQuery($txt);
  
  return $sql->result("CNOMEPMED");
}

function removeZerosEsquerda($bd,$texto) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT REMOVE_ZEROS_ESQUERDA(:Texto) RETORNO FROM DUAL";
  $sql->addParam(":Texto",$texto); 
  $sql->executeQuery($txt);
  
  return $sql->result("RETORNO");
}  

function removeCaracteresInvalidos($bd,$texto) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT REMOVECARACTERESINVALIDOS(:Texto) RETORNO FROM DUAL";
  $sql->addParam(":Texto",$texto); 
  $sql->executeQuery($txt);
  
  return $sql->result("RETORNO");
}  

function modulo_11($numero) {
  $soma = 0;
  $fator = 1;

  for ($i = strlen($numero)-1;$i >= 0;$i--) {
    $fator++;
    if ($fator == 10)
      $fator = 2;
    $soma += $numero[$i] * $fator;
  }

  $resto = $soma % 11;
  $resto = 11 - $resto;
  if ($resto < 10)
    return $resto;
  else
    return '0';
}

function nvl($valor,$retorno){
  if(!empty($valor))
    return $valor;
  else if(empty($retorno))
    return "";
  else
    $retorno;
}

function formata_numero($variavel, $casas_decimais = 2) {
//a vari�vel $casas_decimais foi inclu�da para utiliza��o no mat/med onde preciso de 3 caasas
//$variavel = str_replace('.','',$variavel);
  $variavel = str_replace(',','.',$variavel);
  return number_format($variavel,$casas_decimais,',','.');
}

function temProcedimentoConsulta($bd,$guia) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = "SELECT COUNT(*) QTDE FROM HSSPGUI
       WHERE NNUMEGUIA = :guia
         AND CCODIPMED = '10101012' ";
       
  $sql->addParam(":guia",$guia);
  $sql->executeQuery($txt);
  
  return $sql->result("QTDE");
}

function parametroString ($bd,$parametro) {
  $sql = new Query($bd);
  $sql->clear();
  $txt = " SELECT Parametro_String(:parametro) RESULTADO FROM DUAL";       
  $sql->addParam(":parametro",$parametro);
  $sql->executeQuery($txt);
  
  return $sql->result("RESULTADO");
}

  function parametroNumber ($bd,$parametro) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = " SELECT Parametro_Number(:parametro) RESULTADO FROM DUAL";       
    $sql->addParam(":parametro",$parametro);
    $sql->executeQuery($txt);
    
    return $sql->result("RESULTADO");
  }

  function parametroDate ($bd,$parametro) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = " SELECT Parametro_Date(:parametro) RESULTADO FROM DUAL";       
    $sql->addParam(":parametro",$parametro);
    $sql->executeQuery($txt);
    
    return $sql->result("RESULTADO");
  }

  function possuiCriticaGuia($bd,$guia,$critica) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = "SELECT COUNT(*) QTDE FROM HSSCRITI
         WHERE NNUMEGUIA = :guia
           AND NMOTICRITI = :critica ";
         
    $sql->addParam(":guia",$guia);
    $sql->addParam(":critica",$critica);
    $sql->executeQuery($txt);
    
    if ($sql->result("QTDE") > 0)
      return 'T';
    else
      return 'F';  
  }

  function retornaPorteAnestesicoTabela ($bd,
                                         $prestador,
                                         $congenere,
                                         $procedimento ,
                                         $vigencia) {
                                         
    $retorno = array();
      
    if ($procedimento <> '') {
      $sql = new Query($bd);
      $txt = "SELECT NPORTVPRO,NAUXIVPRO,DVIGEVPRO,1 FROM HSSLINEA,HSSVPRO
               WHERE NNUMEPRES = :prestador
                 AND (HSSLINEA.NNUMECONGE = :congenere OR HSSLINEA.NNUMECONGE IS NULL)
                 AND HSSLINEA.NNUMETABE = HSSVPRO.NNUMETABE
                 AND DVIGELINEA = (SELECT MAX(LINEA.DVIGELINEA) FROM HSSLINEA LINEA
                                    WHERE LINEA.NNUMEPRES = HSSLINEA.NNUMEPRES
                                      AND LINEA.DVIGELINEA <= TO_DATE(:vigencia,'DD/MM/YYYY')
                                      AND (LINEA.NNUMECONGE = :congenere OR LINEA.NNUMECONGE IS NULL)
                                      AND (LINEA.DDVALLINEA >= TO_DATE(:vigencia,'DD/MM/YYYY') OR LINEA.DDVALLINEA IS NULL))
                 AND CCODIPMED = :procedimento
               UNION ALL
              SELECT NPORTVPRO,NAUXIVPRO,DVIGEVPRO,2
                FROM HSSVPRO
               WHERE CCODIPMED = :procedimento
                 AND DVIGEVPRO  <= TO_DATE(:vigencia,'DD/MM/YYYY') 
               ORDER BY 4,3 DESC";
      $sql->addParam(":procedimento",$procedimento);
      $sql->addParam(":vigencia"    ,substr($vigencia,0,10));
      $sql->addParam(":prestador"   ,$prestador);
      $sql->addParam(":congenere"   ,$congenere);
      $sql->executeQuery($txt);

      $retorno['porte']      = $sql->result("NPORTVPRO");
      $retorno['auxiliares'] = $sql->result("NAUXIVPRO");
    }
    else {
      $retorno['porte']      = 0;
      $retorno['auxiliares'] = 0;
    }
    
    return $retorno;
  }

  function _utf8_decode($string) {
    $tmp = $string;
    $count = 0;
    while (mb_detect_encoding($tmp)=="UTF-8")
    {
      $tmp = utf8_decode($tmp);
      $count++;
    }
    
    for ($i = 0; $i < $count-1 ; $i++)
    {
      $string = utf8_decode($string);
      
    }
    return $string;
    
  }
  
  function retornaEmailPrestador ($id,$tipo) { 
    $sql = new Query();
    $txt = "SELECT LOWER(CMAILPRES) EMAIL FROM FINPRES
             WHERE NNUMEPRES = :prestador
               AND CMAILPRES IS NOT NULL
             UNION ALL
            SELECT LOWER(CEMAIEMAPR) EMAIL FROM HSSEMAPR
             WHERE NNUMEPRES = :prestador";

    if ($tipo == 'G') // email para envio de guias
      $txt .= "   AND CGUIAEMAPR = 'S' ";
    else if ($tipo == 'P') // email para pagamentos
      $txt .= "   AND CPAGAEMAPR = 'S' ";      
    else if ($tipo == 'L') // email para glosas
      $txt .= "   AND CGLOSEMAPR = 'S' ";      

    $sql->addParam(":prestador"   ,$id);
    $sql->executeQuery($txt);
      
    $result = '';
    
    while (!$sql->eof()) {
      $result .= $sql->result("EMAIL"). ';';
      $sql->next();
    }
    
    $result = substr($result,1,strlen($result)-1);
    
    return $result;
  }
  function dataExtenso($data1,$completa) {
  $data = getDate(strtotime(substr($data1,6,4)."-".substr($data1,3,2)."-".substr($data1,0,2)));

  if($data["wday"]==0) { $semana =  "Domingo, "; }
  elseif($data["wday"]==1) { $semana = "Segunda-feira, "; }
  elseif($data["wday"]==2) { $semana = "Ter�a-feira, "; }
  elseif($data["wday"]==3) { $semana = "Quarta-feira, "; }
  elseif($data["wday"]==4) { $semana = "Quinta-feira, "; }
  elseif($data["wday"]==5) { $semana = "Sexta-feira, "; }
  elseif($data["wday"]==6) { $semana = "S�bado, "; }

  if($data["mon"]==1) { $mes="Janeiro"; }
  elseif($data["mon"]==2) { $mes="Fevereiro"; }
  elseif($data["mon"]==3) { $mes="Mar�o"; }
  elseif($data["mon"]==4) { $mes="Abril"; }
  elseif($data["mon"]==5) { $mes="Maio"; }
  elseif($data["mon"]==6) { $mes="Junho"; }
  elseif($data["mon"]==7) { $mes="Julho"; }
  elseif($data["mon"]==8) { $mes="Agosto"; }
  elseif($data["mon"]==9) { $mes="Setembro"; }
  elseif($data["mon"]==10) { $mes="Outubro"; }
  elseif($data["mon"]==11) { $mes="Novembro"; }
  elseif($data["mon"]==12) { $mes="Dezembro"; }

  if ($completa==1)
    return $semana.$data["mday"]." de ".$mes." de ".$data["year"].".";
  else
    return $data["mday"]." de ".$mes." de ".$data["year"].".";
}
	
  function ImprimeSPSadt($bd,$guia) {
    $sql = new Query($bd);
    $sql->clear();
    $txt = "SELECT COUNT(*) QTDE
              FROM HSSGUIA, HSSGUIA GUIA
             WHERE HSSGUIA.NNUMEGUIA = :Guia
               AND HSSGUIA.NCOMPGUIA IS NOT NULL
               AND HSSGUIA.NNUMEPRES <> GUIA.NNUMEPRES
               AND HSSGUIA.NCOMPGUIA = GUIA.NNUMEGUIA";
         
    $sql->addParam(":guia",$guia);
    $sql->executeQuery($txt);
    
    if ($sql->result("QTDE") > 0)
      return true;
    else
      return false;  
  }	
  
  function validaGuiaPrincipal($idGuiaPrincipal,$idBeneficiario,$natureza) {
    if ( ($_SESSION['apelido_operadora'] == 'unimedJiParana') or ($_SESSION['apelido_operadora'] == 'casf')){       
      $sql_guia = new Query();
      $txt_guia = "SELECT NNUMEGUIA FROM HSSGUIA
                    WHERE NNUMEGUIA = :guia                             
                      AND NNUMEUSUA = :usuario
                      AND CSTATGUIA IS NULL
                      AND NCOMPGUIA IS NULL ";
      $sql_guia->addParam(":guia"      ,$idGuiaPrincipal);
      $sql_guia->addParam(":usuario"   ,$idBeneficiario);
      $sql_guia->executeQuery($txt_guia);    
    } 
    else {
      if ($this->parametroString ($bd,"CVALSTATUS") == "S"){
        $sql_guia = new Query();
        $txt_guia = "SELECT NNUMEGUIA FROM HSSGUIA
                      WHERE NNUMEGUIA = :guia                        
                        AND NNUMEUSUA = :usuario                        
                        AND NCOMPGUIA IS NULL";               
      }      
      else{
        $sql_guia = new Query();
        $txt_guia = "SELECT NNUMEGUIA FROM HSSGUIA
                      WHERE NNUMEGUIA = :guia
                        AND DECODE(CTIPOGUIA,'I','I','C','I','O','I','S','I','P','I',CTIPOGUIA) = DECODE(:natureza,'I','I','C','I','O','I','S','I','P','I',:natureza)
                        AND NNUMEUSUA = :usuario
                        AND CSTATGUIA IS NULL
                        AND NCOMPGUIA IS NULL ";        
      }
      $sql_guia->addParam(":guia"     ,$idGuiaPrincipal);
      $sql_guia->addParam(":natureza" ,$natureza);
      $sql_guia->addParam(":usuario"  ,$idBeneficiario);
      $sql_guia->executeQuery($txt_guia);    
    }
    
    return ($sql_guia->result("NNUMEGUIA"));     
      
  }  

  function retornaGrauUsuario ($bd,$grau) {
    $txt = "SELECT decode(hssusua.cgrauusua,'F','Filho (a)'  ,'E','Esposo (a)' ,'P','Pai'       ,'M','Mae'        ,'I','Irm�o (�)' ,
                   'A','Avo (�)'    ,'T','Tio (a)'    ,'R','Primo (a)' ,'C','Cunhado (a)','G','Genro/nora',
                   'N','Neto(a)'    ,'B','Sobrinho(a)','S','Sogro(a)'  ,'O','Outro'      ,'D','Agregado(a)',
                   'H','Enteado(a)' ,'J','Companh.(a)','X','Titular') CGRAUUSUA
          		FROM HSSUSUA
             WHERE CGRAUUSUA = :grau ";
    $sql = new Query($bd);
    $sql->addParam(":grau",$grau);
    $sql->executeQuery($txt);
    return $sql->result("CGRAUUSUA");
}

  function retornaGuiasConta($conta){
    $sql_guias = new Query();
    $txt_guias = "SELECT NNUMEGUIA FROM HSSGCON
                   WHERE NNUMECONT = :conta";
   
    $sql_guias->addParam(":conta",$conta);
    $sql_guias->executeQuery($txt_guias);
    
    while (!$sql_guias->eof()) {
      $guias .= $sql_guias->result("NNUMEGUIA").', ';
      $sql_guias->next();
    }   
      return substr($guias,0,-2);            
  } 
  
  function temRedeAtendimento(){
    $sql_temRede = new Query();	 
	 
    $txt_temRede = "SELECT CCODIPLAN       
                      FROM HSSPREDE,HSSREDPL,HSSPLAN,HSSREDEA      
                     WHERE HSSPREDE.NNUMEREDEA = HSSREDPL.NNUMEREDEA
                      AND HSSREDPL.NNUMEPLAN = HSSPLAN.NNUMEPLAN
                      AND HSSPREDE.NNUMEREDEA = HSSREDEA.NNUMEREDEA";	
    
		$sql_temRede->executeQuery($txt_temRede);
		
		if ($sql_temRede->count() > 0)
			return 1;
		else
			return 0;
    		
  }

  function enviaSMS($destinatario,$mensagem,$enviar_em) {
    $sql = new Query();
    $txt = "BEGIN PKG_ENVIA.SMS(:p_operador,:p_destinatario,:p_mensagem,TO_DATE(:p_enviar_em,'DD/MM/YYYY HH24:MI:SS'),:p_retorno,:p_id_saida); END;";
    $sql->addParam(":p_operador"    ,$_SESSION['id_operador']);
    $sql->addParam(":p_destinatario",$destinatario);
    $sql->addParam(":p_mensagem"    ,$mensagem);
    $sql->addParam(":p_enviar_em"   ,$enviar_em); // Informar data e hora
    $sql->addParam(":p_retorno"     ,'',1000);
    $sql->addParam(":p_id_saida"    ,0,1000);
    
    $sql->executeSQL($txt);
    
    if ($sql->getReturn(":p_id_saida") > 0)    
      return $sql->getReturn(":p_id_saida");
    else
      return $sql->getReturn(":p_retorno");
  }

  function enviaMail($destinatario,$assunto,$mensagem,$enviar_em) {
    $sql = new Query();
    $txt = "BEGIN PKG_ENVIA.EMAIL(:p_operador,:p_destinatarios,:p_assunto,:p_mensagem,TO_DATE(:p_enviar_em,'DD/MM/YYYY HH24:MI:SS'),:p_retorno,:p_id_saida); END;";
    $sql->addParam(":p_operador"     ,$_SESSION['id_operador']);
    $sql->addParam(":p_destinatarios",$destinatario);
    $sql->addParam(":p_assunto"      ,$assunto);
    $sql->addParam(":p_mensagem"     ,$mensagem);
    $sql->addParam(":p_enviar_em"    ,$enviar_em); // Informar data e hora
    $sql->addParam(":p_retorno"      ,'',1000);
    $sql->addParam(":p_id_saida"     ,0,1000);
    $sql->executeSQL($txt);
    
    if ($sql->getReturn(":p_id_saida") > 0)    
      return $sql->getReturn(":p_id_saida");
    else
      return $sql->getReturn(":p_retorno");
  }
  
  function validaTelefone($telefone,$tipo) {
    $sql = new Query();
    $txt = "SELECT PKG_UTIL.VALIDA_TELEFONE(:telefone,:tipo) VALIDO FROM DUAL";
    $sql->addParam(":telefone",$telefone);
    $sql->addParam(":tipo",$tipo);
    $sql->executeQuery($txt);
    
    return $sql->result("VALIDO");
  }	  
  
  function validaEmail($email) {
    $sql = new Query();
    $txt = "SELECT PKG_UTIL.VALIDA_EMAIL(:email) VALIDO FROM DUAL";
    $sql->addParam(":email",$email);
    $sql->executeQuery($txt);
    
    return $sql->result("VALIDO");
  }	  

}
?>