<?php
  Session_start();

  require_once("../comum/sessao.php");  
  require_once("../comum/autoload.php");
  
  $bd   = new Oracle();
  $util = new Util();
  
  $id = $_POST['id'];

  $txt = "SELECT NTITUUSUA, CTIPOATCAD FROM HSSATCAD".
         " WHERE NNUMEATCAD = :id";
  $sql = new Query($bd);
  $sql->addParam(":id",$id);
  $sql->executeQuery($txt);

  $qtde = 0;
  if (($sql->result("CTIPOATCAD") == 'T') or ($sql->result("CTIPOATCAD") == 'F')) {
    $txt_qtd = "SELECT COUNT(*) QTDE FROM HSSATCAD
                 WHERE NTITUUSUA = :titular
                   AND NTITUUSUA <> NNUMEUSUA
                   AND CFLAGATCAD IS NULL
                   AND COPERATCAD = 'I' ";

    $sql2 = new Query($bd);
    $sql2->addParam(":titular",$sql->result("NTITUUSUA"));
    $sql2->executeQuery($txt_qtd);
  
    $qtde = $sql2->result("QTDE");
  }

  if ($qtde > 0) {
    $filtros = array();
    $filtros['op']   = 400;
    $filtros['prot'] = '';
    
    $_SESSION['filtrosConfirmacao'] = $filtros;
    
    $util->redireciona($_SESSION['diretorio'].'/confirmacao.php?idSessao='.$_GET['idSessao']);    
  }
  else {
    $txt = "BEGIN ".
           "  CANCELA_OPERACAO_WEB30(:id,:operador); ".
           "END;";
    $sql = new Query($bd);
    $sql->addParam(":id",$id);
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $sql->executeSQL($txt);
    
    $filtros = array();
    $filtros['op']   = 401;
    $filtros['prot'] = '';
    
    $_SESSION['filtrosConfirmacao'] = $filtros;
    
    $util->redireciona($_SESSION['diretorio'].'/confirmacao.php?idSessao='.$_GET['idSessao'],'N');       
  }

  $bd->close();
?>