<?php 
  session_start();
  ini_set("max_execution_time", 120);
  
  class PDF extends PDFSolus {

    function Header() {
      //Logo
      $this->Image('../comum/img/logo_relatorio.jpg',10,5,35,18);
      $this->SetFont('Arial','B',10);
      $this->Cell(40,5,'');
      $this->Cell(230,5,$_SESSION['nome_operadora']." - ".$_SESSION['cnpj_operadora'],0,1);
      $this->Cell(40,5,'');
      $this->Cell(230,5,'RESUMO GERENCIAL',0,1);
      $this->SetFont('Arial','B',7);           
      $this->Cell(40,3,'');
      $this->Cell(25,3,"Per�odo:",0,0,'R');
      $this->Cell(205,3,$this->getArray("Periodo"),0,1);      
      $this->Cell(270,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('L','mm','A4');
  $pdf->AliasNbPages();

  $arr = array("Periodo" => ($dt_inicio2." a ".$dt_fim2),
               "Valores" => $valores_desc);
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();  
  $pdf->SetFont('Arial','',8);
  $pdf->Ln(3);
  
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'1 - Faturamento',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(40,4,'1.1 Faturamento total:',0,0,'L');
  $pdf->Cell(10,4,$formata->formataNumero($Q_Faturamento->result("TOTAL_EMITIDO")),0,1,'R');
  if($Q_Faturamento->result("TOTAL_EMITIDO") > 0){
  	$pdf->Cell(40,4,'1.2 Faturamento PJ:',0,0,'L');
	$pdf->Cell(10,4,$formata->formataNumero($Q_Faturamento->result("PJ")),0,0,'R');
	$pdf->Cell(30,4,$formata->formataNumero(str_replace(',','.',$Q_Faturamento->result("PJ"))/str_replace(',','.',$Q_Faturamento->result("TOTAL_EMITIDO")) * 100 )."%",0,1,'R');
	$pdf->Cell(40,4,'1.3 Faturamento PF:',0,0,'L');
	$pdf->Cell(10,4,$formata->formataNumero($Q_Faturamento->result("PF")),0,0,'R');
	$pdf->Cell(30,4,$formata->formataNumero(str_replace(',','.',$Q_Faturamento->result("PF"))/str_replace(',','.',$Q_Faturamento->result("TOTAL_EMITIDO")) * 100 )."%",0,1,'R');
	$pdf->Ln(2);
	$pdf->Cell(45,4,'Total de benefici�rios faturados (PJ):',0,0,'L');
	$pdf->Cell(10,4,$Q_Total_Usuarios->result("QTDE_PJ"),0,1,'R');
	$pdf->Cell(45,4,'Total de benefici�rios faturados (PF):',0,0,'L');
	$pdf->Cell(10,4,$Q_Total_Usuarios->result("QTDE_PF"),0,1,'R');
  }
  else{
  	$pdf->Cell(40,4,'1.2 Faturamento PJ:',0,0,'L');
	$pdf->Cell(10,4,$formata->formataNumero($Q_Faturamento->result("PJ")),0,1,'R');
	$pdf->Cell(40,4,'1.3 Faturamento PF:',0,0,'L');
	$pdf->Cell(10,4,$formata->formataNumero($Q_Faturamento->result("PF")),0,1,'R');  
  }
  
  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'2 - Despesas assistenciais',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  if($Q_Faturamento->result("TOTAL_EMITIDO") > 0){
    $pdf->Cell(70,4,'2.1 Total de despesas assistenciais:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("TOTAL")),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre faturamento total:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$Q_Eventos->result("TOTAL")) / str_replace(',','.',$Q_Faturamento->result("TOTAL_EMITIDO"))*100)."%",0,1,'R');	
  }
  else{
    $pdf->Cell(40,4,'2.1 Total de despesas assistenciais:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("TOTAL")),0,1,'R');	  
  }

  if($Q_Eventos->result("TOTAL") > 0){
    $pdf->Cell(70,4,'2.2 Despesas assistenciais em pacientes PJ:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("PJ")),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$Q_Eventos->result("PJ")) / str_replace(',','.',$Q_Eventos->result("TOTAL")) * 100)."%",0,1,'R');	
    $pdf->Cell(70,4,'2.3 Despesas assistenciais em pacientes PF:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("PF")),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$Q_Eventos->result("PF")) / str_replace(',','.',$Q_Eventos->result("TOTAL")) * 100)."%",0,1,'R');
    $pdf->Cell(70,4,'2.4 Despesas assistenciais (cortesias):',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("CORTESIA")),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$Q_Eventos->result("CORTESIA")) / str_replace(',','.',$Q_Eventos->result("TOTAL")) * 100)."%",0,1,'R');
	$pdf->Ln(2);
	$pdf->Cell(70,4,'2.5 Despesas faturada a rede de prestadores:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$Q_Eventos->result("TOTAL")) - str_replace(',','.',$Q_Reembolso->result("TOTAL")) ),0,0,'R');	 
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero((str_replace(',','.',$Q_Eventos->result("TOTAL")) - str_replace(',','.',$Q_Reembolso->result("TOTAL"))) / str_replace(',','.',$Q_Eventos->result("TOTAL")) * 100)."%",0,1,'R');	  
    $pdf->Cell(70,4,'2.6 Reembolso/Ressarcimento a benefici�rios:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Reembolso->result("TOTAL")),0,0,'R');	 
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(str_replace(',','.',$Q_Reembolso->result("TOTAL")) / str_replace(',','.',$Q_Eventos->result("TOTAL")) * 100)."%",0,1,'R');	 				  }
  else{
    $pdf->Cell(60,4,'2.2 Despesas assistenciais em pacientes PJ:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("PJ")),0,1,'L');	
    $pdf->Cell(60,4,'2.3 Despesas assistenciais em pacientes PF:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("PF")),0,1,'L');	
    $pdf->Cell(60,4,'2.4 Despesas assistenciais (cortesias):',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("CORTESIA")),0,1,'L');
	$pdf->Cell(70,4,'2.5 Despesas faturada a rede de prestadores:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$Q_Eventos->result("TOTAL")) - str_replace(',','.',$Q_Reembolso->result("TOTAL")) ),0,1,'R');	 
    $pdf->Cell(70,4,'2.6 Reembolso/Ressarcimento a benefici�rios:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($Q_Reembolso->result("TOTAL")),0,1,'R');	       
  }

  $ambulatoriais = 0;
  $consultas = 0;
  $exames = 0;
  $matMed = 0;
  $taxas = 0;
  $demais = 0;
  $opme = 0;
  $pcmso = 0;
  $internacoes = 0;
  $internacoes_eletivas = 0;
  $internacoes_urgentes = 0;
  $internacoes_procedimentos = 0;
  $internacoes_matmed = 0;
  $internacoes_taxas = 0; 
  $internacoes_opme = 0;
  
  while(!$Q_EventosDetalhado->eof()){
  	if($Q_EventosDetalhado->result("CAMBUCONT") == 'A') {
		$ambulatoriais += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));
		if($Q_EventosDetalhado->result("CTIPOPMED") == 'C')
			$consultas += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'E')
			$exames += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'M')
			$matMed += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));		
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'T')
			$taxas += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));				
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'OPME')
			$opme += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));	
		else
			$demais += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));		
	}
	else if($Q_EventosDetalhado->result("CAMBUCONT") == 'I') {
		$internacoes += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));
		if($Q_EventosDetalhado->result("CURGECONT") == 'E')
			$internacoes_eletivas += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));
		else
			$internacoes_urgentes += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));
		
		if($Q_EventosDetalhado->result("CTIPOPMED") == 'IP')
			$internacoes_procedimentos += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));	
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'IF')
			$internacoes_matmed += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));	
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'IT')
			$internacoes_taxas += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));	
		else if($Q_EventosDetalhado->result("CTIPOPMED") == 'IO')
			$internacoes_opme += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));			
	}
	else
		$pcmso += str_replace(',','.',$Q_EventosDetalhado->result("TOTAL"));		
	
	$Q_EventosDetalhado->next();
  }

  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);
  $pdf->SetFont('Arial','B',8);	
  $pdf->Cell(270,4,'3 - Tipos de despesas assistenciais',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(70,4,'3.1 Total de despesas assistenciais:',0,0,'L');
  $pdf->Cell(10,4,$formata->formataNumero($Q_Eventos->result("TOTAL")),0,1,'R');	  
  if($Q_Eventos->result("TOTAL") > 0){
    $pdf->Cell(70,4,'3.2 Total de despesas ambulatoriais:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($ambulatoriais),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$ambulatoriais) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.1 Despesas com consultas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($consultas),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$consultas) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.2 Despesas com exames:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($exames),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$exames) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.3 Despesas com mat/med ambulatorial:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($matMed),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$matMed) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.4 Despesas com taxas ambulatorias:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($taxas),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$taxas) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.5 Outras despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($demais),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$demais) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.6 Despesas com OPME ambulatorial:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($opmed),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$opme) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');
	$pdf->Cell(3,4,'',0,0);	
	$pdf->Cell(67,4,'3.2.7 Despesas com PCMSO:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($pcmso),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$pcmso) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Ln(2);
	$pdf->Cell(70,4,'3.3 Total de despesas com interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');
	$pdf->Cell(3,4,'',0,0);	
	$pdf->Cell(67,4,'3.3.1 Despesas com mat/met interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_matmed),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes_matmed) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.3.2 Despesas com taxas interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_taxas),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes_taxas) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');
	$pdf->Cell(3,4,'',0,0);	
	$pdf->Cell(67,4,'3.3.3 Honor�rios/Procedimentos em interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_procedimentos),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes_procedimentos) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.3.4 OPME em interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_opme),0,0,'R');	
    $pdf->Cell(70,4,'Percentual sobre total de despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes_opme) / str_replace(',','.',$Q_Eventos->result("TOTAL"))*100)."%",0,1,'R');	
    
	if(($internacoes_eletivas + $internacoes_urgentes) > 0){
		$pdf->Cell(3,4,'',0,0);
		$pdf->Cell(67,4,'3.3.5 Despesas com interna��es emergenciais:',0,0,'L');
		$pdf->Cell(10,4,$formata->formataNumero($internacoes_urgentes),0,0,'R');	
		$pdf->Cell(70,4,'Percentual sobre total de interna��es:',0,0,'L');
		$pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes_urgentes) / ( str_replace(',','.',$internacoes_eletivas) + str_replace(',','.',$internacoes_urgentes)  )   *100)."%",0,1,'R');	
		$pdf->Cell(3,4,'',0,0);
		$pdf->Cell(67,4,'3.3.6 Despesas com interna��es eletivas:',0,0,'L');
		$pdf->Cell(10,4,$formata->formataNumero($internacoes_eletivas),0,0,'R');	
		$pdf->Cell(70,4,'Percentual sobre total de interna��es:',0,0,'L');
		$pdf->Cell(10,4,$formata->formataNumero(  str_replace(',','.',$internacoes_eletivas) / ( str_replace(',','.',$internacoes_eletivas) + str_replace(',','.',$internacoes_urgentes)  )   *100)."%",0,1,'R');	
	}
	else{
		$pdf->Cell(3,4,'',0,0);
		$pdf->Cell(67,4,'3.3.5 Despesas com interna��es emergenciais:',0,0,'L');
		$pdf->Cell(10,4,$formata->formataNumero($internacoes_urgentes),0,1,'R');	
		$pdf->Cell(3,4,'',0,0);
		$pdf->Cell(67,4,'3.3.6 Despesas com interna��es eletivas:',0,0,'L');
		$pdf->Cell(10,4,$formata->formataNumero($internacoes_eletivas),0,1,'R');		
	}
  }
  else{
    $pdf->Cell(70,4,'3.2 Total de despesas ambulatoriais:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($ambulatoriais),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.1 Despesas com consultas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($consultas),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.2 Despesas com exames:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($exames),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.3 Despesas com mat/med ambulatorial:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($matMe),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.4 Despesas com taxas ambulatorias:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($taxas),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.5 Outras despesas:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($demais),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.6 Despesas com OPME ambulatorial:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($opmed),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.2.7 Despesas com PCMSO:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($pcmso),0,1,'R');	
	$pdf->Ln(2); 
	$pdf->Cell(70,4,'3.3 Total de despesas com interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.3.1 Despesas com mat/met interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_matmed),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.3.2 Despesas com taxas interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_taxas),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.3.3 Honor�rios/Procedimentos em interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_procedimentos),0,1,'R');	
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(67,4,'3.3.4 OPME em interna��es:',0,0,'L');
    $pdf->Cell(10,4,$formata->formataNumero($internacoes_opme),0,1,'R');		 
  }
 
  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);
  $pdf->SetFont('Arial','B',8);	
  $pdf->Cell(270,4,'4 - Consultas por especialidade',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(70,4,'4.1 Total de consultas:',0,0,'L');
  $pdf->Cell(10,4,$formata->formataNumero($consultas),0,1,'R');	
  $total = 0;
  while(!$Q_Consultas->eof()){
  	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(80,4,$Q_Consultas->result("CNOMEESPEC"),0,0,'L');
    $pdf->Cell(47,4,$formata->formataNumero($Q_Consultas->result("TOTAL")),0,0,'R');	
    $pdf->Cell(13,4,$Q_Consultas->result("QTDE"),0,0,'R');
    $pdf->Cell(17,4,$formata->formataNumero(str_replace(',','.',$Q_Consultas->result("TOTAL")) / str_replace(',','.',$consultas)*100)."%",0,1,'R');	
    $total += str_replace(',','.',$Q_Consultas->result("TOTAL"));
  	$Q_Consultas->next();
  }  
  if((str_replace(',','.',$consultas) - str_replace(',','.',$total)) > 0){ 
    $pdf->Cell(3,4,'',0,0);  
	$pdf->Cell(80,4,'Sem indicar especialidade',0,0,'L');  	
	$pdf->Cell(47,4,$formata->formataNumero(str_replace(',','.',$consultas) - str_replace(',','.',$total)),0,0,'R');	
	$pdf->Cell(13,4,'',0,0);
    $pdf->Cell(17,4,$formata->formataNumero((str_replace(',','.',$consultas) - str_replace(',','.',$total)) / str_replace(',','.',$consultas)*100)."%",0,1,'R');	
  }
  else{
    $pdf->Cell(3,4,'',0,0);  
	$pdf->Cell(80,4,'Sem indicar especialidade',0,0,'L');  	
	$pdf->Cell(47,4,$formata->formataNumero(str_replace(',','.',$consultas) - str_replace(',','.',$total)),0,0,'R');	  
  }

  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'5 - Exames',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(70,4,'5.1 Exames:',0,0,'L');
  $pdf->Cell(10,4,$formata->formataNumero($exames),0,1,'R');	
  while(!$Q_Exames->eof()){
    $pdf->Cell(3,4,'',0,0);
    if($Q_Exames->result("CTIPOPMED") == 'N')
		$pdf->Cell(80,4,'ANGIOGRAFIA',0,0,'L');
    else if($Q_Exames->result("CTIPOPMED") == 'H')
		$pdf->Cell(80,4,'HEMODINAMICA',0,0,'L');
    else if($Q_Exames->result("CTIPOPMED") == 'R')
		$pdf->Cell(80,4,'RESSONANCIA MAGNETICA',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == 'O')
		$pdf->Cell(80,4,'TOMOGRAFIA COMPUTADORIZADA',0,0,'L');								
    else if($Q_Exames->result("CTIPOPMED") == 'K')
		$pdf->Cell(80,4,'ANATOMOPATOLOGIA E CITOPATOLOGIA',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == 'V')
		$pdf->Cell(80,4,'DENSITOMETRIA',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == 'X')
		$pdf->Cell(80,4,'ECOCARDIOGRAFIA',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == '*')
		$pdf->Cell(80,4,'ENDOSCOPIA DAS VIAS AEREAS',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == 'Z')
		$pdf->Cell(80,4,'ENDOSCOPIA DAS VIAS DIGESTIVAS',0,0,'L');													
    else if($Q_Exames->result("CTIPOPMED") == '&')
		$pdf->Cell(80,4,'HOLTER',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == '$')
		$pdf->Cell(80,4,'TESTE ERGOMETRICO',0,0,'L');		
    else if($Q_Exames->result("CTIPOPMED") == 'Y')
		$pdf->Cell(80,4,'ELETROCARDIOGRAMA',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == 'U')
		$pdf->Cell(80,4,'ULTRASSONOGRAFIA',0,0,'L');		
    else if($Q_Exames->result("CTIPOPMED") == 'B')
		$pdf->Cell(80,4,'PATOLOGIA CLINICA',0,0,'L');		
    else if($Q_Exames->result("CTIPOPMED") == '%')
		$pdf->Cell(80,4,'MAMOGRAFIA',0,0,'L');		
    else if($Q_Exames->result("CTIPOPMED") == '@')
		$pdf->Cell(80,4,'MEDICINA NUCLEAR',0,0,'L');	
    else if($Q_Exames->result("CTIPOPMED") == '#')
		$pdf->Cell(80,4,'RADIODIAGNOSTICO',0,0,'L');		
    else
		$pdf->Cell(80,4,'DEMAIS EXAMES COMPLEMENTARES',0,0,'L');										
		
						
    $pdf->Cell(47,4,$formata->formataNumero($Q_Exames->result("TOTAL")),0,0,'R');	
    $pdf->Cell(13,4,$Q_Exames->result("QTDE"),0,0,'R');
    $pdf->Cell(17,4,$formata->formataNumero(str_replace(',','.',$Q_Exames->result("TOTAL")) / str_replace(',','.',$exames)*100)."%",0,1,'R');	
  	$Q_Exames->next();
  }
  
  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'6 - INTERNA��ES',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(70,4,'6.1 Interna��es:',0,0,'L');
  $pdf->Cell(10,4,$formata->formataNumero($internacoes),0,1,'R');
  $pdf->Ln(2);
  $pdf->Cell(3,4,'',0,0);
  $pdf->Cell(90,4,'Prestador',0,0,'L');
  $pdf->Cell(9,4,'Qtde',0,0,'R');    
  $pdf->Cell(15,4,'Diarias',0,0,'R');
  $pdf->Cell(15,4,'UTI',0,0,'R');
  $pdf->Cell(18,4,'Mat/Med',0,0,'R');
  $pdf->Cell(18,4,'Taxas',0,0,'R');
  $pdf->Cell(26,4,'Exames/Terapias',0,0,'R');
  $pdf->Cell(20,4,'Honorarios',0,0,'R');   
  $pdf->Cell(20,4,'Total',0,0,'R');
  $pdf->Cell(15,4,'Percentual',0,1,'R'); 
   
  while(!$Q_Internacoes->eof()){
    $pdf->Cell(3,4,'',0,0);	
    $pdf->Cell(90,4,$Q_Internacoes->result("CNOMEPRES"),0,0,'L');	
	$pdf->Cell(9,4,$Q_Internacoes->result("QTDE"),0,0,'R');
	$pdf->Cell(15,4,$formata->formataNumero($Q_Internacoes->result("DIARIAS")),0,0,'R');
	$pdf->Cell(15,4,$formata->formataNumero($Q_Internacoes->result("UTI")),0,0,'R');
	$pdf->Cell(18,4,$formata->formataNumero($Q_Internacoes->result("MATMED")),0,0,'R');
	$pdf->Cell(18,4,$formata->formataNumero(str_replace(',','.',$Q_Internacoes->result("TAXAS")) - str_replace(',','.',$Q_Internacoes->result("DIARIAS")) - str_replace(',','.',$Q_Internacoes->result("UTI"))),0,0,'R');
	$pdf->Cell(26,4,$formata->formataNumero($Q_Internacoes->result("EXAMES")),0,0,'R');
	$pdf->Cell(20,4,$formata->formataNumero($Q_Internacoes->result("HONORARIOS")),0,0,'R');
	$pdf->Cell(20,4,$formata->formataNumero($Q_Internacoes->result("TOTAL")),0,0,'R');
$pdf->Cell(15,4,$formata->formataNumero(str_replace(',','.',$Q_Internacoes->result("TOTAL")) / ( str_replace(',','.',$internacoes_eletivas) + str_replace(',','.',$internacoes_urgentes)) * 100),0,1,'R');
  	$Q_Internacoes->next();
  }
  							
  $pdf->Ln(2);
  $pdf->Cell(70,4,'6.2 As maiores contas de interna��o por prestador',0,1,'L');
  $pdf->Ln(2);
  $Q_ContasPrestador = new Query($bd);
  $txt_contasprestador = " SELECT HSSCONT.NNUMECONT,NVL(CNOMEUSUA,CCORTCONT) CNOMEUSUA,
								  TOTAL_CONTA_HONORARIOS(HSSCONT.NNUMECONT)+TOTAL_CONTA_HOSPITALAR(HSSCONT.NNUMECONT) TOTAL
							 FROM HSSLOTE,HSSCONT,HSSUSUA, HSSTITU
							WHERE HSSLOTE.CCOMPLOTE IN (" . $competencia . ")
							  AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
							  AND HSSCONT.NNUMEPRES = :Prestador 
							  AND CAMBUCONT = 'I'
							  AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
							  AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU	
							  AND HSSTITU.NNUMETITU = :contrato						
							ORDER BY 3 DESC ";
  $Q_Internacoes->first();
  while(!$Q_Internacoes->eof()){
	  $Q_ContasPrestador->clear();
	  $Q_ContasPrestador->addParam(":prestador",$Q_Internacoes->result("NNUMEPRES"));
	  $Q_ContasPrestador->addParam(":contrato",$_SESSION['id_contrato']);
	  $Q_ContasPrestador->executeQuery($txt_contasprestador);
	  
	  $contador = 0;
      $pdf->Cell(3,4,'',0,0);	
      $pdf->Cell(80,4,$Q_Internacoes->result("CNOMEPRES"),0,1,'L');
	  while(!$Q_ContasPrestador->eof() and $contador < 3){
	  	$contador++;
	  	$pdf->Cell(5,4,'',0,0);
		$pdf->Cell(12,4,$Q_ContasPrestador->result("NNUMECONT"),0,0,'L');	
		$pdf->Cell(60,4,$Q_ContasPrestador->result("CNOMEUSUA"),0,0,'L');
		$pdf->Cell(35,4,$formata->formataNumero($Q_ContasPrestador->result("TOTAL")),0,1,'R');	  
	  	$Q_ContasPrestador->next();
	  }
	  $Q_Internacoes->next();		  
  }
  
  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'7 - MAIORES CONTAS DE INTERNA��O POR USU�RIO',0,1,'L',true);
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(3,4,'',0,0);
  $pdf->Cell(90,4,'Usu�rio',0,0,'L');
  $pdf->Cell(9,4,'Qtde',0,0,'R');    
  $pdf->Cell(15,4,'Diarias',0,0,'R');
  $pdf->Cell(15,4,'UTI',0,0,'R');
  $pdf->Cell(18,4,'Mat/Med',0,0,'R');
  $pdf->Cell(18,4,'Taxas',0,0,'R');
  $pdf->Cell(26,4,'Exames/Terapias',0,0,'R');
  $pdf->Cell(20,4,'Honorarios',0,0,'R');   
  $pdf->Cell(20,4,'Total',0,1,'R');
  
  $contador = 0; 
  while(!$Q_Usuarios->eof() and $contador < 10){
  	$contador++;
	$pdf->Cell(3,4,'',0,0);
    $pdf->Cell(90,4,$Q_Usuarios->result("CNOMEUSUA"),0,0,'L');	
	$pdf->Cell(9,4,$Q_Usuarios->result("QTDE"),0,0,'R');
	$pdf->Cell(15,4,$formata->formataNumero($Q_Usuarios->result("DIARIAS")),0,0,'R');
	$pdf->Cell(15,4,$formata->formataNumero($Q_Usuarios->result("UTI")),0,0,'R');
	$pdf->Cell(18,4,$formata->formataNumero($Q_Usuarios->result("MATMED")),0,0,'R');
	$pdf->Cell(18,4,$formata->formataNumero(str_replace(',','.',$Q_Usuarios->result("TAXAS")) - str_replace(',','.',$Q_Usuarios->result("DIARIAS")) - str_replace(',','.',$Q_Usuarios->result("UTI"))),0,0,'R');
	$pdf->Cell(26,4,$formata->formataNumero($Q_Usuarios->result("EXAMES")),0,0,'R');
	$pdf->Cell(20,4,$formata->formataNumero($Q_Usuarios->result("HONORARIOS")),0,0,'R');
	$pdf->Cell(20,4,$formata->formataNumero($Q_Usuarios->result("TOTAL")),0,1,'R');
	$Q_Usuarios->next();
  }     
  $pdf->Ln(2);
  $pdf->Cell(3,4,'',0,0);
  $pdf->Cell(100,4,'* Quando a coluna Qtde for igual a 0 (zero) e mesmo assim houver valores, significa que o que foi faturado',0,1,'L');
  $pdf->Cell(3,4,'',0,0);
  $pdf->Cell(100,4,'  refere-se a uma conta complementar de interna��o',0,1,'L');

  $Q_Solicitados = new Query($bd);
  $txt_solicitados = " SELECT NVL(HSSCONT.NSOLIPRES,HSSCONT.NNUMEPRES) NNUMEPRES,
						      SUM(NQUANPCON) QTDE,
						    SUM(NVL(NCIRUHONO,0)+NVL(NANESHONO,0)+NVL(NPRIMHONO,0)+NVL(NSEGUHONO,0)+NVL(NTERCHONO,0)+NVL(NQUARHONO,0)+NVL(NINSTHONO,0)+NVL(NSALAPCON,0)) TOTAL
					     FROM HSSLOTE,HSSCONT,HSSUSUA,HSSTITU,HSSPCON,HSSPMED
					    WHERE CCOMPLOTE IN (" . $competencia . ")
					      AND HSSLOTE.NNUMELOTE = HSSCONT.NNUMELOTE
					      AND HSSCONT.CAMBUCONT <> 'I'
					      AND HSSCONT.NNUMEUSUA = HSSUSUA.NNUMEUSUA (+)
					      AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU (+)
					      AND HSSCONT.NNUMECONT = HSSPCON.NNUMECONT
					      AND NVL(NCIRUHONO,0)+NVL(NANESHONO,0)+NVL(NPRIMHONO,0)+NVL(NSEGUHONO,0)+NVL(NTERCHONO,0)+NVL(NQUARHONO,0)+NVL(NINSTHONO,0)+NVL(NSALAPCON,0) > 0
					      AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
					      AND NOT (CTIPOPMED = 'C' AND NVL(HSSCONT.NSOLIPRES,HSSCONT.NNUMEPRES) = HSSPCON.NNUMEPRES)
					      AND HSSTITU.NNUMETITU = :contrato
						  AND NVL(HSSCONT.NSOLIPRES,HSSCONT.NNUMEPRES) = :prestador 
					    GROUP BY NVL(HSSCONT.NSOLIPRES,HSSCONT.NNUMEPRES)
					    ORDER BY 1 ";						 


  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'8 - CONSULTAS POR ESPECIALIDADE / PRESTADOR',0,1,'L',true);
  $pdf->SetFont('Arial','',7);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(3,4,'',0,0);
  $pdf->Cell(86,4,'Prestador',0,0,'L');
  $pdf->Cell(11,4,'Qtde',0,0,'R');    
  $pdf->Cell(14,4,'Valor',0,0,'R');
  $pdf->Cell(62,4,'|------------- Exames e proced. solicitados -------------|',0,0,'R');
  $pdf->Cell(15,4,'Custo total',0,0,'R');
  $pdf->Cell(30,4,'Custo real da consulta',0,1,'R');
  $pdf->SetFont('Arial','',6);
  $pdf->Cell(6,4,'',0,0,'R');  
  $pdf->Cell(22,4,'Especialidade',0,0,'L');  
  $pdf->Cell(86,4,'',0,0,'R');    
  $pdf->Cell(12,4,'Qtde',0,0,'R');    
  $pdf->Cell(14,4,'Custo',0,0,'R');
  $pdf->Cell(14,4,'Custo m�dio',0,0,'R'); 
  $pdf->Cell(20,4,'M�dia p/ consulta',0,1,'R');   
  $pdf->SetFont('Arial','',8);
  
  $solicitacao = 0; 
  $especialidade = '';
  while(!$Q_Especialidades->eof()){
  	if($Q_Especialidades->result("CNOMEESPEC") <> $especialidade){
		$pdf->Ln(2);
		$pdf->Cell(6,4,'',0,0);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(40,4,substr($Q_Especialidades->result("CNOMEESPEC"),0,25),0,1,'L');
		$pdf->SetFont('Arial','',8);
		$especialidade = $Q_Especialidades->result("CNOMEESPEC");
	}
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(80,4,$Q_Especialidades->result("CNOMEPRES"),0,0,'L');
	$pdf->Cell(15,4,$Q_Especialidades->result("QTDE"),0,0,'R');
		
	$Q_Solicitados->clear();
	$Q_Solicitados->addParam(":contrato",$_SESSION['id_contrato']);
    $Q_Solicitados->addParam(":prestador",$Q_Especialidades->result("NNUMEPRES")); 
	$Q_Solicitados->executeQuery($txt_solicitados);
	
	if($Q_Solicitados->result("NNUMEPRES") > 0){
   	    $pdf->Cell(15,4,$formata->formataNumero($Q_Especialidades->result("TOTAL")),0,0,'R');
		$pdf->Cell(13,4,$Q_Solicitados->result("QTDE"),0,0,'R');
		$pdf->Cell(15,4,$formata->formataNumero($Q_Solicitados->result("TOTAL")),0,0,'R');
		if($Q_Solicitados->result("QTDE") > 0)
			$pdf->Cell(13,4,$formata->formataNumero(str_replace(',','.',$Q_Solicitados->result("TOTAL")) / str_replace(',','.',$Q_Solicitados->result("QTDE"))),0,0,'R');
		if($Q_Especialidades->result("QTDE") > 0)
			$pdf->Cell(20,4,$formata->formataNumero(str_replace(',','.',$Q_Solicitados->result("QTDE")) / str_replace(',','.',$Q_Especialidades->result("QTDE"))),0,0,'R');
        $pdf->Cell(18,4,$formata->formataNumero(str_replace(',','.',$Q_Solicitados->result("TOTAL")) + str_replace(',','.',$Q_Especialidades->result("TOTAL"))),0,0,'R');	
		if($Q_Especialidades->result("QTDE") > 0)
			$pdf->Cell(20,4,$formata->formataNumero((str_replace(',','.',$Q_Solicitados->result("TOTAL"))+str_replace(',','.',$Q_Especialidades->result("TOTAL"))) / str_replace(',','.',$Q_Especialidades->result("QTDE"))),0,1,'R');		
	}
	else
  		$pdf->Cell(15,4,$formata->formataNumero($Q_Especialidades->result("TOTAL")),0,1,'R');
	
    $Q_Especialidades->next();
  }

  $pdf->Ln(3);
  $pdf->SetFillColor(244,244,244);
  $pdf->SetTextColor(255,0,0);	
  $pdf->SetFont('Arial','B',8);
  $pdf->Cell(270,4,'9 - EXAMES POR ESPECIALIDADE / PRESTADOR',0,1,'L',true);  
  $pdf->SetFont('Arial','',7);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFillColor(255,255,255);
  $pdf->Ln(2);
  $pdf->Cell(3,4,'',0,0);
  $pdf->Cell(86,4,'Prestador',0,0,'L');
  $pdf->Cell(11,4,'Qtde',0,0,'R');    
  $pdf->Cell(14,4,'Valor',0,1,'R');
  $pdf->SetFont('Arial','',6);
  $pdf->Cell(6,4,'',0,0,'R');  
  $pdf->Cell(22,4,'Especialidade',0,1,'L');   
  $pdf->SetFont('Arial','',8);
  
  $solicitacao = 0; 
  $especialidade = '';
  while(!$Q_Especialidades_Exames->eof()){
  	if($Q_Especialidades_Exames->result("CNOMEESPEC") <> $especialidade){
		$pdf->Ln(2);
		$pdf->Cell(6,4,'',0,0);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(40,4,substr($Q_Especialidades_Exames->result("CNOMEESPEC"),0,25),0,1,'L');
		$pdf->SetFont('Arial','',8);
		$especialidade = $Q_Especialidades_Exames->result("CNOMEESPEC");
	}
	$pdf->Cell(3,4,'',0,0);
	$pdf->Cell(86,4,substr($Q_Especialidades_Exames->result("CNOMEPRES"),0,32),0,0,'L');
	$pdf->Cell(11,4,$Q_Especialidades_Exames->result("QTDE"),0,0,'R');
	$pdf->Cell(14,4,$formata->formataNumero($Q_Especialidades_Exames->result("TOTAL")),0,1,'R');
		
    $Q_Especialidades_Exames->next();
  }

  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";

?>
