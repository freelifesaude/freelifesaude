<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  
  $util = new Util();
  $bd   = new Oracle();
  
  $_SESSION['titulo'] = "RELA��O DE CARTEIRINHAS";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/relCarteirasDet.htm");
         
  $filtros = array();
  
  if (isset($_POST['id'])) {
    $filtros['op'] = $_POST['op'];
    $filtros['p']  = $_POST['id'];  
    
    $_SESSION['filtrosCarteira'] = $filtros;
  }
  else if (isset($_SESSION['filtrosCarteira'])) { 
    $filtros = $_SESSION['filtrosCarteira'];
  }
  
  $operacao  = $filtros['op'];
  $protocolo = $filtros['p'];
    
  if (isset($_POST['id_lote']))
    $id_lote = $_POST['id_lote'];
  
  if (isset($_POST['voltar']))
    $util->redireciona("relCarteiras.php?idSessao=".$_GET['idSessao'],"N");
  else if (isset($_POST['imprimir'])) { 
    $util->redireciona("../comum/impCarteirinhas.php?idSessao=".$_GET['idSessao'],'S');
  }
  else if (isset($_POST['excluir'])) {
    $sql = new Query($bd);  
    $txt = "DELETE FROM HSSCAWEB WHERE NNUMECAWEB IN (SELECT NNUMECAWEB FROM HSSCAWEB,HSSUSUA
                                                       WHERE CPROTCAWEB = :id
                                                         AND HSSCAWEB.NNUMETITU = :contrato
                                                         AND CSITUCAWEB = 'P' ";
                                                         
    if ($_SESSION['id_titular'] > 0) {             
      $txt .= "   AND HSSUSUA.NTITUUSUA = :titular ";
      $sql->addParam(":titular",$_SESSION['id_titular']);             
    }                                                         

    $txt .="   AND HSSCAWEB.NNUMEUSUA = HSSUSUA.NNUMEUSUA) ";
    
    $sql->addParam(":id",$protocolo);
    $sql->addParam(":contrato",$_SESSION['id_contrato']);
    $erro = $sql->executeSQL($txt);
  
    if ($erro == '')
      $util->redireciona("relCarteiras.php?idSessao=".$_GET['idSessao'],"N");
    else {
      $tpl->MSG = "Erro ao excluir remessa.";
      $tpl->CLASSE = "alert-error";      
      $tpl->block("MENSAGEM");
    }
  }
  
  $txt = "SELECT HSSUSUA.NNUMEUSUA, CNOMEUSUA, CCODIUSUA,
                 DECODE(HSSUSUA.CTIPOUSUA,'T','Titular','F','Tit Financ','A','Agregado','Dependente') CTIPOUSUA,
                 CMOTICAWEB,CDESCMTCAR,CBO__CAWEB,CMREJCAWEB,CSITUCAWEB
            FROM HSSCAWEB,HSSUSUA,HSSMTCAR
           WHERE HSSCAWEB.CSITUCAWEB = :operacao
             AND HSSCAWEB.CPROTCAWEB = :protocolo
             AND HSSCAWEB.NNUMEUSUA = HSSUSUA.NNUMEUSUA
             AND HSSUSUA.NNUMETITU = :contrato
             AND NVL(HSSCAWEB.NNUMESETOR,-1) = :locacao
             AND HSSCAWEB.NNUMEMTCAR = HSSMTCAR.NNUMEMTCAR(+)";            

  $sql = new Query($bd);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->addParam(":locacao",$_SESSION['id_locacao']);  
  $sql->addParam(":operacao",$operacao);
  $sql->addParam(":protocolo",$protocolo);
  $sql->executeQuery($txt);
  
  if ($sql->result("CSITUCAWEB") == 'R')
    $tpl->block("TITULO_REJEITADA");
  else
    $tpl->block("TITULO_DEMAIS");
       
  while (!$sql->eof()) {
    
    $tpl->CARTEIRINHA_PROTOCOLO = $protocolo;
    $tpl->CARTEIRINHA_CODIGO = $sql->result("CCODIUSUA");
    $tpl->CARTEIRINHA_NOME = $sql->result("CNOMEUSUA");
    $tpl->CARTEIRINHA_CATEGORIA = $sql->result("CTIPOUSUA");
    $tpl->CARTEIRINHA_REJEITADA = $sql->result("CMREJCAWEB");
    $tpl->CARTEIRINHA_MOTIVO = $sql->result("CDESCMTCAR");
    $tpl->CARTEIRINHA_OBSERVACAO = $sql->result("CMOTICAWEB");
    $tpl->CARTEIRINHA_BO = $sql->result("CBO__CAWEB");
        
    if ($sql->result("CSITUCAWEB") == 'R')
      $tpl->block("REJEITADA");
    else
      $tpl->block("DEMAIS");

    $tpl->block("LINHA");      
    
    $sql->next();
  }

  $sql = new Query($bd);  
  $txt = "SELECT NNUMECAWEB
            FROM HSSCAWEB,HSSUSUA
           WHERE CPROTCAWEB = :id
             AND HSSCAWEB.NNUMETITU = :contrato
             AND CSITUCAWEB = 'P'
             AND TRUNC(DDIGICAWEB) = TRUNC(SYSDATE)
             AND HSSCAWEB.NNUMEUSUA = HSSUSUA.NNUMEUSUA ";

  if (isset($_SESSION['id_titular'])) {             
    $txt .= "   AND HSSUSUA.NTITUUSUA = :titular ";
    $sql->addParam(":titular",$_SESSION['id_titular']);             
  }
  
  $sql->addParam(":id",$protocolo);
  $sql->addParam(":contrato",$_SESSION['id_contrato']);
  $sql->executeQuery($txt);

  if ($sql->result("NNUMECAWEB") > 0)
    $tpl->block("BOTAO_EXCLUIR");
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     

?>