<?php 
  require_once("../autoload.php");
  require_once("../funcao.php");
  $bd = new Oracle();
  
  if (isset($_POST['id_usuario'])){
  
    if ($_POST['id_usuario'] > 0) {
      $sql = new Query($bd);
      $mao = 'D';//$_POST['mao'];
      $dedo = 'P';//$_POST['dedo'];
        
        $txt = " SELECT CDPOLUSUA,CDINDUSUA,CDMEDUSUA,CDANEUSUA,CDMINUSUA,
                        CDPO2USUA,CDIN2USUA,CDME2USUA,CDAN2USUA,CDMI2USUA,CCODIUSUA
                   FROM HSSUSUA
                  WHERE NNUMEUSUA = :usuario";
        
        $sql->addParam(":usuario",$_POST['id_usuario']);
        $retorno = $sql->executeQuery($txt);
        
        if($mao == 'D'){
            switch($dedo){
              case 'P' : echo $sql->result("CDPOLUSUA");
              break;
              case 'I' : echo $sql->result("CDINDUSUA");
              break;
              case 'M' : echo $sql->result("CDMEDUSUA");
              break;
              case 'A' : echo $sql->result("CDANEUSUA");
              break;
              case 'N' : echo $sql->result("CDMINUSUA");
              break;
            }
        }else if($mao == 'E'){
            switch($dedo){
              case 'P' : echo $sql->result("CDPO2USUA");
              break;
              case 'I' : echo $sql->result("CDIN2USUA");
              break;
              case 'M' : echo $sql->result("CDME2USUA");
              break;
              case 'A' : echo $sql->result("CDAN2USUA");
              break;
              case 'N' : echo $sql->result("CDMI2USUA");
              break;
            }
        }            
                         
    } 
  }

  $bd->close();  
?>

