<?php
  
  class PDF extends PDFSolus {

    function Header() {
      //Logo
	  $formata = new Formata();
      $this->Image('../comum/img/logo_relatorio.jpg',80,3,40,25);
      $this->SetFont('Arial','B',10);
      $this->Cell(160,5,'');
      $this->Cell(30,5,'N.� '.$formata->acrescentaZeros($this->getArray("numero"),5),'BTLR',1,'C');
      $this->Ln(11);
//      $this->Cell(270,1,' ','B',0);
	  $this->SetFont('Arial','',8);
	  $this->Cell(190,5,'Matriz Centro: Av.Alberto Bins, 658/202 � Centro -  CEP 90030-140 -  Fone (51) 3226.4888 � Porto Alegre � RS',0,1);
	  $this->Cell(190,5,'Filial Administrativa: Av.Alberto Bins, 658/301 � Centro � Cep 90030-140 � Fone (51) 3378.8000 Porto Alegre � RS',0,1);
	  $this->Cell(190,5,'Filial Cachoeirinha: Av.Fernando Ferrari, 354 � Bairro Vila Regina � Cep 94930-075 Fone (51) 3439-4525 Cachoeirinha � RS',0,1);
	  $this->Cell(190,5,'Filial Canoas: Av.Getulio Vargas, 1749 � Bairro Niter�i � Cep 92110-330 � Fone (51) 3475-2233 Canoas � RS',0,1);
	  $this->Cell(190,1,' ','B',0);
      $this->Ln(2);
    }

    function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      $this->SetFont('Arial','',8);
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
  }

  $pdf=new PDF('P','mm','A4');
  $pdf->AliasNbPages();
 
  $proximo = new Query($bd);
  $txt = "SELECT SEQAUTAT.NEXTVAL PROXIMO FROM DUAL";
  $proximo->executeQuery($txt);
  
  $insere = new Query($bd);
  $txt = "INSERT INTO HSSAUTAT(NNUMEAUTAT,NNUMEUSUA,NNPROPROF,NAPROPROF,NOPERUSUA)
                        VALUES(:id,:usuario,:nova_profissao,null,:operador)"; 
  $insere->addParam(":id",$proximo->result("PROXIMO"));		
  $insere->addParam(":usuario",$id_funcionario);
  $insere->addParam(":nova_profissao",$id_nova_profissao);  								
//  $insere->addParam(":antiga_profissao",NULL);  
  $insere->addParam(":operador",$_SESSION['id_operador']);
  $erro = $insere->executeSQL($txt);
 
/*  if ($valores == '1')
    $valores_desc = "Todas";
  else if ($valores == '2')
    $valores_desc = "Somente valores n�o cobrados";
  else if ($valores == '3')
    $valores_desc = "Somente valores cobrados";*/

  $arr = array("numero" => $proximo->result("PROXIMO"));
  $pdf->SetArray($arr);

  $pdf->Open();
  $pdf->AddPage();
  $pdf->SetFillColor(220,220,200);
  $pdf->SetFont('Arial','',8);

  $usuario = "";
  $titular = "";
  $conta = 0;
  $total_titular = 0;
  $total_desp_usuario = 0;
  $total_copart_usuario = 0;
  $total_geral = 0;
  $desp_usuario = 0;
  $copart_usuario = 0;

  
  
  if($erro == ''){
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(190,12,'Autoriza��o de Atendimento',0,1,'C');
	$pdf->Ln(2);	
    $pdf->SetFont('Arial','B',8);	
	$pdf->Cell(33,4,'Empresa',0,0,'R');
    $pdf->SetFont('Arial','',8);	
	$pdf->Cell(160,4,$_SESSION['razao_social_empresa'],0,1);
	
    $pdf->SetFont('Arial','B',8);	
	$pdf->Cell(33,4,'Tipo de ASO',0,0,'R');
    $pdf->SetFont('Arial','',8);	
	$pdf->Cell(160,4,$tipo_aso,0,1);	
	
    $pdf->SetFont('Arial','B',8);	
	$pdf->Cell(33,4,'Candidato/Funcion�rio',0,0,'R');
    $pdf->SetFont('Arial','',8);	
	$pdf->Cell(160,4,$nome2,0,1);		
	
    $pdf->SetFont('Arial','B',8);	
	$pdf->Cell(33,4,'Fun��o',0,0,'R');
    $pdf->SetFont('Arial','',8);	
	if($prof != '')
	  $pdf->Cell(160,4,$func->nomeProfissao($bd,$prof),0,1);	
	else
	  $pdf->Cell(160,4,$profissao2,0,1);	
	
    $pdf->SetFont('Arial','B',8);	
	$pdf->Cell(190,4,'Exames que estamos autorizando',0,1,'C');
//    $pdf->SetFont('Arial','',8);	
//	$pdf->Cell(160,4,'teste',0,1);	
	
	$pdf->Ln(2);
	$pdf->SetFont('Arial','B',8);
	$insereP = new Query($bd);
	foreach($exames as $codigo){
	  $insereP->clear();
      $txt = "INSERT INTO HSSPAUTA(NNUMEAUTAT,CCODIPMED)
                            VALUES(:id,:procedimento) "; 
      $insereP->addParam(":id",$proximo->result("PROXIMO"));		
      $insereP->addParam(":procedimento",$codigo);
      $insereP->executeSQL($txt);
  
	  $pdf->Cell(34,5,'',0,0);
	  $pdf->Cell(20,5,$codigo,'BTLR',0);
	  $pdf->Cell(136,5,$func->RetornaNomeProcedimento($bd,$codigo),'BTLR',1);	
	}
  	$pdf->Ln(2);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(20,5,'Observa��es',0,1);	
	$pdf->SetFont('Arial','',8);
	$pdf->MultiCell(190,5,$obs,'BTLR',1);	
	
	$pdf->Ln(2);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(190,5,'ORIENTA��ES PARA REALIZA��O DE EXAMES',0,1,'C');		
	
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(25,5,'EXAME','BTLR',0);	
	$pdf->Cell(53,5,'ORIENTA��ES','BTLR',0);	
	
	$pdf->Cell(5,5,'',0,0);	
	
	$pdf->Cell(30,5,'EXAME','BTLR',0);
	$pdf->Cell(77,5,'ORIENTA��ES','BTLR',1);
	
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(25,5,'Acuidade Visual','BTLR',0);	
	$pdf->Cell(53,5,'Caso utilize �culos � necess�rio trazer','BTLR',0);	

	$pdf->Cell(5,5,'',0,0);	
	
	$pdf->Cell(30,5,'TPG','BTLR',0);
	$pdf->Cell(77,5,'04 horas de jejum','BTLR',1);		
	
	$pdf->Cell(25,15,'Audiometria','BTLR',0);	
	$pdf->Cell(53,15,'14 horas de repouso auditivo','BTLR',0);	

	$pdf->Cell(5,5,'',0,0);	
	
	$pdf->Cell(30,15,'Teste Ergom�trico','BTLR');
	$pdf->MultiCell(77,5,'Jejum de 01 hora, �gua � livre, roupa leve.Vestir roupa de 02 pe�as para que possa ser retirada somente a parte superior','BTLR',1);		

	$pdf->Cell(25,5,'Creatinina','BTLR',0);	
	$pdf->Cell(53,5,'04 horas de jejum','BTLR',0);	

	$pdf->Cell(5,5,'',0,0);	
	
	$pdf->Cell(30,5,'Gama GT','BTLR',0);
	$pdf->Cell(77,5,'04 horas de jejum.','BTLR',1);		
	
	$pdf->Cell(25,15,'Glicose','BTLR',0);	
	$pdf->Cell(53,15,'08 horas de jejum','BTLR',0);	

	$pdf->Cell(5,5,'',0,0);	
	
	$pdf->Cell(30,15,'Micol�gico direto','BTLR',0);
	$pdf->MultiCell(77,5,'Estar sem esmalte, creme e nenhum tipo de medicamento na unha. Exame realizado nos laborat�rios das unidade de segunda a quinta.','BTLR',1);					
		
	$pdf->Cell(25,5,'TGO','BTLR',0);	
	$pdf->Cell(53,5,'04 horas de jejum','BTLR',0);	

	$pdf->Cell(5,5,'',0,0);	
	
	$pdf->Cell(30,5,'EEG em vig�lia','BTLR',0);
	$pdf->Cell(77,5,'Estar bem alimentado, trazer toalha de rosto.','BTLR',1);				
	
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(190,5,'*Os exames que n�o est�o nessa lista n�o necessitam de preparo',0,1,'C');		
	
	$pdf->Ln(3);
	$pdf->Cell(190,5,'AUTORIZAMOS DEBITAR OS EXAMES AQUI SOLICITADOS EM NOSSA FATURA MENSAL.',0,1,'C');	

	$pdf->Ln(10);	
	$pdf->Cell(190,5,'______________________________________________',0,1,'C');
	$pdf->Cell(190,5,'Carimbo da empresa e assinatura do respons�vel',0,0,'C');	
  }else{
/*  while (!$sql->eof()) {
    if ($sql->result("TIPO") == 'G') {
      $txt2 = "SELECT NVALOGPAGA VALOR ".
              "  FROM HSSGPAGA ".
              " WHERE NNUMEGUIA = :guia ".
              " UNION ALL ".
              "SELECT SUM(NFRANPGUI) FROM HSSPGUI ".
              " WHERE NNUMEGUIA = :guia ".
              " GROUP BY NNUMEGUIA ".
              "HAVING SUM(NFRANPGUI) = 0 ".
              " UNION ALL ".
              "SELECT NVALOPACON VALOR ".
              "  FROM HSSGCON,HSSPACON ".
              " WHERE NNUMEGUIA = :guia ".
              "   AND HSSGCON.NNUMECONT = HSSPACON.NNUMECONT ";
    } elseif ($sql->result("TIPO") == "C") {
      $txt2 = "SELECT NVALOPACON VALOR FROM HSSPACON ".
              " WHERE NNUMECONT = :guia ".
              " UNION ALL ".
              "SELECT NFRANCONT VALOR FROM HSSCONT ".
              " WHERE NNUMECONT = :guia ".
              "   AND CCOBRCONT = 'X' ". // considero se a conta estiver "sem cobranca" como pago
              " UNION ALL ".
              "SELECT NFRANCONT VALOR FROM HSSCONT ".
              " WHERE NNUMECONT = :guia ".
              "   AND NFRANCONT = 0 ".
              " UNION ALL ".
              "SELECT NVALOGPAGA VALOR FROM HSSGCON,HSSGPAGA ".
              " WHERE NNUMECONT = :guia ".
              "   AND HSSGCON.NNUMEGUIA = HSSGPAGA.NNUMEGUIA ";
    }

    if ($valores <> '1') {
      $sql2 = new Query($bd);
      $sql2->addParam(":guia",$sql->result("NNUMECONT"));
      $sql2->executeQuery($txt2);
      
      if (($valores == '3') and ($sql2->result("VALOR") <> ''))
        $imprime = true;
      elseif (($valores == '2') and ($sql2->result("VALOR") == ''))
        $imprime = true;
      else
        $imprime = false;
    } else  {
      $imprime = true;
    }

    if ($imprime) {

      if ($titular <> $sql->result("TITULAR")) {
        $pdf->SetFont('Arial','B',8);
        
        $pdf->Cell(190,3,'Titular: '.$sql->result("TITULAR"),0,1);
        $titular = $sql->result("TITULAR");
        $total_copart_titular = 0;
        $total_desp_titular = 0;
        
        $pdf->Ln(1);
        $pdf->Cell(20,3,'Guia/Conta',0,0);        
        $pdf->Cell(70,3,'Benefici�rio',0,0);
        $pdf->Cell(18,3,'Atendimento',0,0);
        $pdf->Cell(67,3,'Executante',0,0);
        $pdf->Cell(15,3,'Co-part',0,1);
        $pdf->Ln(1);        
        $pdf->SetFont('Arial','',8);          
      }

      $pdf->Cell(20,3,$sql->result("NNUMECONT"),0,0);      
      $pdf->Cell(70,3,$pdf->Copy($sql->result("CNOMEUSUA"),69),0,0);      
      $pdf->Cell(18,3,$sql->result("DATA"),0,0);
      $pdf->Cell(67,3,$pdf->Copy($sql->result("CNOMEPRES"),66),0,0);

      if ($sql->result("TIPO") == "G") {
        $txt2 = "SELECT HSSPGUI.CCODIPMED,CNOMEPMED,NFRANPGUI COPART,NQINDPGUI VALOR,NQUANPGUI QTDE,
                        NCOPRPGUI COPART_PRES,0 RECIPROCIDADE
                   FROM HSSPGUI,HSSPMED
                  WHERE NNUMEGUIA = :numero
                    AND HSSPGUI.CCODIPMED = HSSPMED.CCODIPMED";
      } else {
        $txt2 = "SELECT HSSPCON.CCODIPMED,CNOMEPMED,NFRANPCON COPART,TOTAL_CONTA_HONORARIOS_PCON(HSSPCON.NNUMEPCON) VALOR,NQUANPCON QTDE,
                        NCOPRPCON COPART_PRES,NVL(NRECIPCON,0)+NVL(NREC1PCON,0)+NVL(NREC2PCON,0)+NVL(NREC3PCON,0)+NVL(NREC4PCON,0) RECIPROCIDADE
                   FROM HSSPCON,HSSPMED
                  WHERE NNUMECONT = :numero
                    AND HSSPCON.CCODIPMED = HSSPMED.CCODIPMED
                  UNION
                 SELECT 'TAXAS' CCODIPMED,CDESCTAXA CNOMEPMED,SUM(NFRANTCON) COPART,SUM(NVALOTCON) VALOR,SUM(NQUANTCON) QTDE,
                        0 COPART_PRES,SUM(NVL(NRECITCON,0)) RECIPROCIDADE
                   FROM HSSTCON,HSSTAXA
                  WHERE NNUMECONT = :numero
                    AND NVALOTCON > 0
                    AND HSSTCON.NNUMETAXA = HSSTAXA.NNUMETAXA
                  GROUP BY CDESCTAXA
                  UNION
                 SELECT 'MAT/MED' CCODIPMED,'MATERIAIS E MEDICAMENTOS' CNOMEPMED,VALOR_FRANQUIA_MATMED(HSSCONT.NNUMECONT) COPART,TOTAL_FARMACIA(HSSCONT.NNUMECONT) VALOR, 1 QTDE,
                        0 COPART_PRES,0 RECIPROCIDADE
                   FROM HSSCONT
                  WHERE NNUMECONT = :numero
                    AND TOTAL_FARMACIA(HSSCONT.NNUMECONT) > 0 ";
      }

      $sql3 = new Query($bd);
      $sql3->addParam(":numero",$sql->result("NNUMECONT"));
      $sql3->executeQuery($txt2);
      
      $copart_usuario = 0;

      while (!$sql3->eof()) {
        $copart_usuario = $copart_usuario + str_replace(',','.',$sql3->result("COPART"));
        $sql3->next();
      }

      $pdf->Cell(15,3,$formata->formataNumero($copart_usuario),0,1,'R');
      
      $sql->next();

      if ($titular <> $sql->result("TITULAR"))      
        $pdf->Ln(3);

    } else
      $sql->next();
  }
  
  if ($faltas == 'S'){
    $txt3 = "SELECT DDATAAGEND, CHORAAGEND, CNOMEUSUA, CNOMEPRES,
                    DECODE(NFALTTITU, NULL, (SELECT NVALOESPFA
                                               FROM HSSESPFA
                                              WHERE NNUMETITU = HSSUSUA.NNUMETITU
                                                AND ((NNUMEPRES = HSSAGEND.NNUMEPRES AND NNUMEESPEC = HSSAGEND.NNUMEESPEC) OR
                                                     (NNUMEPRES = HSSAGEND.NNUMEPRES AND NNUMEESPEC IS NULL) OR
                                                     (NNUMEPRES IS NULL AND NNUMEESPEC = HSSAGEND.NNUMEESPEC))), NFALTTITU) VALOR_FALTA
               FROM HSSAPAGA, HSSAGEND, HSSUSUA, FINPRES, HSSTITU
              WHERE HSSAPAGA.NNUMEPAGA IS NULL -- falta n�o cobrada
                AND CSITUAGEND = 'F' AND NVL(HSSTITU.CFALTTITU, '1') <> '1'
								AND DDATAAGEND >= TO_DATE(:datainicial,'DD/MM/YYYY')
								AND DDATAAGEND < TO_DATE(:datafinal,'DD/MM/YYYY') + 1
                AND HSSAGEND.NNUMEAGEND = HSSAPAGA.NNUMEAGEND (+)
                AND HSSAGEND.NNUMEUSUA = HSSUSUA.NNUMEUSUA(+)
                AND HSSAGEND.NNUMEPRES = FINPRES.NNUMEPRES
                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
                AND HSSTITU.NNUMETITU = :contrato
              ORDER BY DDATAAGEND,CHORAAGEND";
       
    $id_contrato = $_SESSION['id_contrato'];
    $sql4 = new Query($bd);
    $sql4->addParam(":contrato", $id_contrato);
    $sql4->addParam(":datainicial",$datainicial);   
    $sql4->addParam(":datafinal",$datafinal);   
    $sql4->executeQuery($txt3);

    if ($sql4->count() > 0) {
      $pdf->Ln(3);
      $pdf->SetFont('Arial', 'B', 8);
      $pdf->Cell(300, 3,'FALTAS N�O COBRADAS',0, 1, 'L');
      $pdf->Ln(1);
      $pdf->SetFont('Arial', '', 8);
      $pdf->Cell(30, 3,'Data','B', 0, 'L');
      $pdf->Cell(80, 3,'Usu�rio','B', 0, 'L');
      $pdf->Cell(80, 3,'Prestador','B', 0, 'L');
      $pdf->Cell(30, 3,'Valor','B', 1, 'L');
      $pdf->Ln(1);

      while (!$sql4->eof()) {
        $pdf->Cell(30, 3,$sql4->result("DDATAAGEND"). ' ' .$sql4->result("CHORAAGEND"),0, 0, 'L');
        $pdf->Cell(80, 3,$sql4->result("CNOMEUSUA"),0, 0, 'L');
        $pdf->Cell(80, 3,$sql4->result("CNOMEPRES"),0, 0, 'L');
        $pdf->Cell(30, 3,$formata->formataNumero($sql4->result("VALOR_FALTA")),0, 1, 'L');
        $sql4->next();
      }
    }
  }
*/
$pdf->Cell(10,5,'Ocorreu um erro ao tentar gerar a autoriza��o. Favor entrar em contato com a CARLOS CHAGAS NO SETOR DE PCMSO, fone: (51)3378.8036.',0,1);
  }
  $file='../temp/'.md5(uniqid(rand(), true)).'.pdf';
  $pdf->Output($file,'F');
  $tpl->RESULT = "<SCRIPT>window.open('$file');</SCRIPT>";
?>