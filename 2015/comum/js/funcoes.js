/*******************************************/
/** valida cpf                            **/
/*******************************************/
jQuery.validator.addMethod("cpf", function(value,element) {
  var cpf = value;
  var validar = false;

  if(cpf.length == 11)
    cpf = cpf.substr(0,3) + '.' + cpf.substr(3,3) + '.' + cpf.substr(6,3) + '-' + cpf.substr(9,2);
	
  if (cpf == "___.___.___-__") {
    validar = true;
  } else if (cpf.length <= 0){
    validar = true;
  } else if ( (cpf.length != 14) || (cpf.substr(11,1) != "-") ) {
    validar = false;
  }
  else {
    dv = cpf.substr(12,2); 
    cpf = cpf.substr(0,3).concat(cpf.substr(4,3).concat(cpf.substr(8,3)));

    if ((cpf.concat(dv).indexOf('1111111') > -1) ||
        (cpf.concat(dv).indexOf('2222222') > -1) ||
        (cpf.concat(dv).indexOf('3333333') > -1) ||
        (cpf.concat(dv).indexOf('4444444') > -1) ||
        (cpf.concat(dv).indexOf('5555555') > -1) ||
        (cpf.concat(dv).indexOf('6666666') > -1) ||
        (cpf.concat(dv).indexOf('7777777') > -1) ||
        (cpf.concat(dv).indexOf('8888888') > -1) ||
        (cpf.concat(dv).indexOf('9999999') > -1) ||
        (cpf.concat(dv).indexOf('0000000') > -1))
      validar = false;
    else {
      digito1 = dv.substr(0,1);
      digito2 = dv.substr(1,1);
      buffer = 0;
      
      for (i = 0;i < 9;i++) {
        buffer += cpf.charAt(i)*(10-i); 
      }

      buffer = 11 - (buffer % 11);
  
      if (buffer >= 10) 
        buffer = 0;

      if (buffer != digito1)
        validar = false;
      else {
        cpf = cpf.concat(buffer.toString());

        buffer = 0;
      
        for (i = 0;i < 10;i++) {
          buffer += cpf.charAt(i)*(11-i); 
        }

        buffer = 11 - (buffer % 11);
  
        if (buffer >= 10) 
          buffer = 0;
    
        if (buffer != digito2)
          validar = false;
        else
          validar = true;
      }
    }
  }

  return validar;
},"C.P.F. Inv�lido."); 

/*******************************************/
/** valida CEI                            **/
/*******************************************/
jQuery.validator.addMethod("cei", function(value,element) {
  var cei = value;
  var validar = false;
  var buffer = 0;
  var cei_caracter = " ";
  var tamanho = 0;
  var tamdec = 0;
  var resultado = 0;
  
  if (cei == "__.___._____-__") {
    validar = true;
  } 
  else if (cei.length <= 0) {
    validar = true;
  }
  else if ( (cei.length != 15) || (cei.substr(12,1) != "-") ) {
    validar = false;
  }
  else {
    dv = cei.substr(14,1);

    cei = cei.substr(0,2).concat(cei.substr(3,3).concat(cei.substr(7,5).concat(cei.substr(13,2))));
    buffer = 0; 
    	  
    buffer += 7 * cei.charAt(0);
    buffer += 4 * cei.charAt(1);
    buffer += 1 * cei.charAt(2);
    buffer += 8 * cei.charAt(3);
    buffer += 5 * cei.charAt(4);
    buffer += 2 * cei.charAt(5);
    buffer += 1 * cei.charAt(6);
    buffer += 6 * cei.charAt(7);
    buffer += 3 * cei.charAt(8);
    buffer += 7 * cei.charAt(9);
    buffer += 4 * cei.charAt(10);
				
    cei_caracter = ""+buffer;	
	tamdec = cei_caracter.length;
	var x = tamdec - 2;
	var y = tamdec - 1;
	var soma1 = 0;
	var soma2 = 0;
	
	soma1 = cei_caracter.substring(x,x+1);
	soma2 = cei_caracter.substring(y,y+1);
	resultado = parseInt(soma1)+ parseInt(soma2);
		
	cei_caracter = ""+resultado;
	tamdec = cei_caracter.length;
	x=tamdec - 1;
	soma1=cei_caracter.substring(x,x+1);
	
	resultado = parseInt(soma1);
	resultado = 10 - resultado;
	cei_caracter = ""+resultado;
	tamdec = cei_caracter.length;
	x = tamdec - 1;
	resultado=cei_caracter.substring(x,x+1);
		
    if (resultado == dv) 
      validar = true;
    else
      validar = false;
  }
  return validar;
},"C.E.I. Inv�lido."); 

/*******************************************/
/** valida nomes abreviados               **/
/*******************************************/
jQuery.validator.addMethod("abreviado", function(nome, element,param) {
  var esta_abreviado = "N";
  var i = 1;   
  var nomes = nome.toUpperCase().split(' ');
  
  for (x=0;x<nomes.length;x++) {
    if ((nomes[x].length == 1) && (esta_abreviado == "N") && (nomes[x] != 'E'))
      esta_abreviado = "S";
  }

  if ( !this.depend(param, element) )
	return "dependency-mismatch";  
  else if (esta_abreviado == "S")
    return false;
  else
    return true;  
  
},"Este campo est� abreviado. A ANS n�o aceita nomes abreviados."); 

/*******************************************/
/** valida pis                            **/
/*******************************************/
jQuery.validator.addMethod("pis", function(value,element) {
  var pis = value;
  var validar = false;

  if (pis == "___._____.__-_") {
    validar = true;
  } else if (pis.length > 0 ) {
    if (pis.length != 14)
     validar = false;
    else {
      dv = pis.substr(13,1);
      pis = pis.substr(0,3).concat(pis.substr(4,5).concat(pis.substr(10,2)));

      if (pis.concat(dv).indexOf('11111') > -1 ||
          pis.concat(dv).indexOf('22222') > -1 ||
          pis.concat(dv).indexOf('33333') > -1 ||
          pis.concat(dv).indexOf('44444') > -1 ||
          pis.concat(dv).indexOf('55555') > -1 ||
          pis.concat(dv).indexOf('66666') > -1 ||
          pis.concat(dv).indexOf('77777') > -1 ||
          pis.concat(dv).indexOf('88888') > -1 ||
          pis.concat(dv).indexOf('99999') > -1)
        validar = false;
      else {
      dig = 0;
      fator = 3;

      for (i = 0;i < 10;i++ ) {
        dig += (pis.charAt(i) * fator);
      fator--;

      if (fator == 1)
        fator = 9;
      }

        resto = dig % 11;
        dig = 11 - resto;

        if (dig > 9)
          dig = 0;

        if (dig != dv)
          validar = false;
        else
          validar = true;
      }
    }
  } else {
    validar = true;
  }

  return validar;
},"P.I.S. Inv�lido."); 

/*******************************************/
/** valida campos alfabeticos             **/
/*******************************************/
jQuery.validator.addMethod("letras", function(value,element,param) {
  if ( !this.depend(param, element) )
	return "dependency-mismatch";  
  else
    return this.optional(element) || /^[a-zA-Z ]+$/.test(value); 
},"Por favor, digite somente letras (a-z) sem acentua��o."); 

/*******************************************/
/** valida campos n�mericos               **/
/*******************************************/
jQuery.validator.addMethod("numeros", function(value,element,param) {
  if ( !this.depend(param, element) )
	return "dependency-mismatch";  
  else
    return this.optional(element) || /^[0-9Xx]+$/.test(value); 
},"Por favor, digite somente n�meros. ou X"); 

/*******************************************/
/** valida data                           **/
/*******************************************/
jQuery.validator.addMethod("data", function(value,element) {
  return this.optional(element) || /^(([0-2][1-9]|10|20)\/02\/(19|20)([13579][26]|[02468][048]))|(([0-2][1-8]|09|19|10|20)\/02\/(19|20)([02468][1235679]|[13579][13457890]))|(([0-2][1-9]|10|20|30)\/(0[469]|11)\/((19|20)[0-9][0-9]))|(([0-2][1-9]|10|20|30|31)\/(0[13578]|1[02])\/((19|20)[0-9][0-9]))$/.test(value);    
},"Data inv�lida."); 

/*******************************************/
/** valida hora                           **/
/*******************************************/
jQuery.validator.addMethod("time24", function(value, element) {
    return /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);
}, "Hora inv�lida.");

/*******************************************/
/** valida cnpj                           **/
/*******************************************/
jQuery.validator.addMethod("cnpj", function(value,element) {
  var cnpj = value;
  var validar = false;

  if (cnpj == "__.___.___/____-__") {
    validar = true;
  } else if (cnpj.length <= 0){
    validar = true;
  } else if ( (cnpj.length != 18) || (cnpj.substr(15,1) != "-") ) {
    validar = false;
  }
  else {
    dv = cnpj.substr(16,2); 
    cnpj = cnpj.substr(0,2).concat(cnpj.substr(3,3).concat(cnpj.substr(7,3).concat(cnpj.substr(11,4))));

    if ((cnpj.concat(dv).indexOf('1111111') > -1) ||
        (cnpj.concat(dv).indexOf('2222222') > -1) ||
        (cnpj.concat(dv).indexOf('3333333') > -1) ||
        (cnpj.concat(dv).indexOf('4444444') > -1) ||
        (cnpj.concat(dv).indexOf('5555555') > -1) ||
        (cnpj.concat(dv).indexOf('6666666') > -1) ||
        (cnpj.concat(dv).indexOf('7777777') > -1) ||
        (cnpj.concat(dv).indexOf('8888888') > -1) ||
        (cnpj.concat(dv).indexOf('9999999') > -1) ||
        (cnpj.concat(dv).indexOf('0000000') > -1))
      validar = false;
    else {
      digito1 = dv.substr(0,1);
      digito2 = dv.substr(1,1);
      buffer = 0;
      fator = 5;
      
      for (i = 0;i < 12;i++) {
        buffer += cnpj.charAt(i)*fator; 
        fator--;
        if (fator == 1)
          fator = 9;
      }

      buffer = 11 - (buffer % 11);
  
      if (buffer >= 10) 
        buffer = 0;

      if (buffer != digito1)
        validar = false;
      else {
        cnpj = cnpj.concat(buffer.toString());

        buffer = 0;
        fator = 6;
      
        for (i = 0;i < 13;i++) {
          buffer += cnpj.charAt(i)*fator; 
          fator--;
          if (fator == 1)
            fator = 9;
        }

        buffer = 11 - (buffer % 11);
  
        if (buffer >= 10) 
          buffer = 0;
    
        if (buffer != digito2)
          validar = false;
        else
          validar = true;
      }
    }
  }

  return validar;
},"C.N.P.J. Inv�lido."); 

/*******************************************/
/** valida e-mail                         **/
/*******************************************/
function validaEmail(mail){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

	if(er.test(mail))
		return true;
	else
		return false;
}

/*******************************************/
/** abrir pop-up                          **/
/*******************************************/
function abrirPopUp(endereco,titulo,height,width) {
  var tam1 = (window.screen.availWidth / 2) - (width / 2) - (window.screen.availWidth * 0.01);
  var tam2 = (window.screen.availHeight / 2) - (height / 2) - (window.screen.availHeight * 0.062);

  var win = open(endereco,titulo,'top='+tam2+',left='+tam1+',width='+width+',height='+height+',status=yes,scrollbars=yes,resizable=yes');
  //var tam1 = (window.screen.availWidth / 2) - (win.document.body.offsetWidth / 2) - (window.screen.availWidth * 0.01);
  //var tam2 = (window.screen.availHeight / 2) - (win.document.body.offsetHeight / 2) - (window.screen.availHeight * 0.062);
   
  //win.moveTo(tam1, tam2 - 100);
  win.focus();
  
  return win;
} 

function imc(peso,altura){
  var total;

  peso   = peso.replace(",",".");
  altura = altura.replace(",",".");
  
  if (peso > 0 && altura > 0)
    total = peso/(altura*altura);
  else
    total = 0;
 
  return total.toFixed(2);
}

function imcDescricao(total){
  var total;
  var imc;

  if (total < 17){
    imc = '(Muito abaixo do peso)';
  }  
  else if ((total >= 17) && (total < 18.9)) {
    imc = '(Abaixo do peso)';
  }  
  else if ((total >= 18.5) && (total < 25)) {
    imc = '(Peso normal)';
  }
  else if ((total >= 25) && (total < 30)) {
    imc = '(Acima do peso)';
  }
  else if ((total >= 30) && (total < 35)) {
    imc = '(Obesidade Grau I)';
  }
  else if ((total >= 35) && (total < 40)) { 
    imc = '(Obesidade Grau II - Severa)';
  }	
  else {
    imc = '(Obesidade Grau III - M�rbida)';
  }
 
  return imc;
}

function mensagem(mensagem,tipo,botoes,titulo,foo) {

  //Tipo = erro, alerta, info, sucesso, confirmacao

  var modelo = '';
  
  modelo = '<div class="row-fluid"><div class="span1">';
  
  if (tipo == 'erro')
    modelo += '<font color="#DA4F49"><i class="fa fa-times-circle fa-3x"></i></font>&nbsp;</div><div class="span11">';
  else if (tipo == 'alerta') 
    modelo += '<font color="#FAA732"><i class="fa fa-exclamation-circle fa-3x"></i></font>&nbsp;</div><div class="span11">';
  else if (tipo == 'sucesso')
    modelo += '<font color="#51A351"><i class="fa fa-check-circle fa-3x"></i></font>&nbsp;</div><div class="span11">';
  else if (tipo == 'confirmacao')
    modelo += '<font color="navy"><i class="fa fa-question-circle fa-3x"></i></font>&nbsp;</div><div class="span11">';
  else
    modelo += '<font color="#08C"><i class="fa fa-info-circle fa-3x"></i></font>&nbsp;</div><div class="span11">';

  modelo += mensagem + '</div></div>';
  
  var botao1 = '';
  var botao2 = '';
  var b = botoes.split('|',2);
  
  switch(b[0]) {
    case 'btOK':
        botao1 = 'Ok';
        break;
    case 'btYes':
        botao1 = 'Sim';
        break;
    case 'btNo':
        botao1 = 'N�o';
        break;
    case 'btCancel':
        botao1 = 'Cancelar';
        break;        
    default:
        botao1 = 'Ok';
  }   
  
  switch(b[1]) {
    case 'btOK':
        botao2 = 'Ok';
        break;
    case 'btYes':
        botao2 = 'Sim';
        break;
    case 'btNo':
        botao2 = 'N�o';
        break;
    case 'btCancel':
        botao2 = 'Cancelar';
        break;        
    default:
        botao2 = 'Cancelar';
  }    
    
  if (tipo == 'confirmacao') {   
    $.confirm({
      title: titulo,
      confirmButton: botao1,
      cancelButton: botao2,
      confirmButtonClass: 'btn-inverse',
      cancelButtonClass: 'btn-inverse',   
      backgroundDismiss: false,
      animate: 'none',
      content: modelo,
      confirm: function(){
        foo();
      },
      cancel: function () {
      }  
    });
  }
  else {
    $.alert({
      title: titulo,
      confirmButton: 'Ok',
      confirmButtonClass: 'btn-inverse',
      cancelButtonClass: 'btn-inverse',      
      animate: 'none',
      content: modelo,
      confirm: function(){
        foo();
      }      
    });  
  }
}