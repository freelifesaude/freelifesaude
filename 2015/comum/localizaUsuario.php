<?php
  Session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
  
  require_once("../comum/layoutJanela.php"); 
  $tpl->addFile("JANELA_CONTEUDO","../comum/localizaUsuario.html"); 
  $tpl->block("MOSTRA_LOGO");
  
  $nome       = '';  
  $qtde       = 20;
  $carteira   = '';
  $nascimento = '';
  $mae        = '';
  $local      = '';
  $inclusao   = '';
  $situacao   = '';
  $empresa    = '';
  $incluao    = '';
    
  //$local = $_GET['local'];
  
  //if (($_SESSION['apelido_operadora'] <> 'vitallis') or ($_SESSION['sistema'] <> 'Prestador'))
  $tpl->block("NOME_USUARIO");
    
  if (isset($_POST['localizar'])) {
    $nome       = strtoupper($seg->antiInjection($_POST['nome']));      
    $carteira   = $seg->antiInjection($_POST['carteira']);
    $qtde       = $seg->antiInjection($_POST['qtde']);  
	  $nascimento = $seg->antiInjection($_POST['nascimento']);
	  $mae        = strtoupper($seg->antiInjection($_POST['mae']));
	  $inclusao   = $seg->antiInjection($_POST['inclusao']);
    
    if (isset($_POST['empresa']))
	    $empresa    = strtoupper($seg->antiInjection($_POST['empresa']));
        
    $sql = new Query($bd);
    $txt = "SELECT CCODIUSUA,REPLACE(CNOMEUSUA,'''','') CNOMEUSUA,
                   TO_CHAR(DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,CCARTUSUA,CCHAPUSUA,CLUWMTITU,
                   DECODE(CSITUUSUA,'M','Migrado','C','Cancelado','Ativo') CSITUUSUA,IDADE(DNASCUSUA,SYSDATE) IDADE,
                   CDINDUSUA,CCODIPLAN,CDESCPLAN,TO_CHAR(NVL(DALIBUSUA,DSITUUSUA),'DD/MM/YYYY') DALIBUSUA, CNMAEUSUA
              FROM HSSUSUA,HSSPLAN,HSSTITU,HSSCONGE 
             WHERE HSSUSUA.NNUMEUSUA > 0 ";
             
    if (($_SESSION['apelido_operadora'] == 'carloschagas') or ($_SESSION['apelido_operadora'] == 'carloschagaspcmso') or ($seg->permissaoOutros($bd,'WEBPRESTADORNOMOSTRAUSUARIOPCMSO',false) ))
      $txt .= "   AND NVL(CASO_TITU,'N') = 'N'";               
        
    if ($nome <> '') {
      $nome2 = $nome;
      $nome2 = $formata->somenteCaracteres(strtoupper($nome2))."%"; 
      $txt .= "   AND (HSSUSUA.CNOMEUSUA LIKE :nome OR HSSUSUA.CNOMEUSUA LIKE :nome2) ";
      $sql->addParam(":nome",$nome);
      $sql->addParam(":nome2",$nome2);
    }       
            
    if ($carteira <> '') {
      $carteira2 = $carteira;
      $carteira2 = $carteira2."%";
      $txt .= "   AND (CCODIUSUA LIKE :carteira OR CCARTUSUA LIKE :carteira or ((CCHAPUSUA LIKE :CARTEIRA) AND (CLUWMTITU = 'S') )) ";
      $sql->addParam(":carteira",$carteira2);      
    }
  
    if ($nascimento <> '') {
      $nascimento2 = $nascimento;
      $txt .= "   AND HSSUSUA.DNASCUSUA = :nascimento ";
      $sql->addParam(":nascimento",$nascimento2);      
    }
    
    if ($inclusao <> '') {
      $inclusao2 = $inclusao;
      $txt .= "   AND HSSUSUA.DINCLUSUA = :inclusao ";
      $sql->addParam(":inclusao",$inclusao2);      
    }    

    if ($mae <> '') {
      $mae2 = $mae;
      $mae2 = $formata->somenteCaracteres(strtoupper($mae2))."%"; 
      $txt .= "   AND HSSUSUA.CNMAEUSUA LIKE :mae";
      $sql->addParam(":mae",$mae2);
    }	  
    
    if ($_SESSION['sistema'] == 'Prestador') {
      if (($_SESSION['apelido_operadora'] == "casse") or ($_SESSION['apelido_operadora'] == "casf") or ($_SESSION['apelido_operadora'] == "santaCasaMontesClaros") or ($_SESSION['apelido_operadora'] == "fatimaSaude"))
        $txt .= "   AND (CSITUUSUA = 'A' OR (CSITUUSUA = 'C') OR (CSITUUSUA = 'M' AND DALIBUSUA > SYSDATE))";
      else if ($_SESSION['apelido_operadora'] == "promedmg")  
			  $txt .= "   AND (CSITUUSUA = 'A' OR (CSITUUSUA <> 'A' AND >= TRUNC(SYSDATE)) OR (CSITUUSUA = 'M' AND DALIBUSUA >= TRUNC(SYSDATE)))";
			else if (($_SESSION['apelido_operadora'] == "unimedLimeira") or ($_SESSION['apelido_operadora'] == "sempreVida")) 
			  $txt .= "   AND (CSITUUSUA = 'A' OR (CSITUUSUA <> 'A' AND NVL(DALIBUSUA,DSITUUSUA) >= TRUNC(SYSDATE)))";
      else
        $txt .= "   AND (CSITUUSUA = 'A' OR (CSITUUSUA <> 'A' AND NVL(DALIBUSUA,DSITUUSUA) > SYSDATE))";

      $txt .= "   AND CTIPOUSUA <> 'F' 
                  AND DINCLUSUA <= SYSDATE ";
                  
      // Para casse lista somente beneficiarios da rede de atendimento do prestador logado
      if ($_SESSION['apelido_operadora'] == 'casse') {
        $txt .= "   AND HSSUSUA.NNUMEPLAN IN (SELECT NNUMEPLAN FROM HSSREDPL,HSSPREDE
                                               WHERE HSSREDPL.NNUMEREDEA = HSSPREDE.NNUMEREDEA
                                                 AND HSSPREDE.NNUMEPRES = :prestador
                                               UNION ALL
                                              SELECT NNUMEPLAN FROM HSSPLAN
                                               WHERE 0 = (SELECT COUNT(*) FROM HSSPREDE WHERE NNUMEPRES = :prestador))";
        $sql->addParam(":prestador",$_SESSION['id_contratado']);                                  
      } 
	  
      if ($_SESSION['apelido_operadora'] == 'Garantia'){
        $txt .= "   AND CWUSUPLAN <> ''S'' ";
      }
  
    } else if ($_SESSION['sistema'] == 'Empresa') {
      $txt .= "   AND CTIPOUSUA <> 'F' 
                  AND HSSUSUA.NNUMETITU = :contrato ";      
      $sql->addParam(":contrato",$_SESSION['id_contrato']);                          
    }
                    
    $txt .= "   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                AND NVL(HSSUSUA.CLINTUSUA,'S') = 'S'";

    if ($_SESSION['sistema'] <> 'Empresa')
      $txt .= "   AND NVL(HSSTITU.CLINTTITU,'S') = 'S'";

    $txt .= "   AND HSSTITU.NNUMECONGE = HSSCONGE.NNUMECONGE(+)
                AND NVL(HSSCONGE.CLINTCONGE,'S') = 'S'
                AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";
                
    if (isset($_SESSION['id_estabelecimento']) and $_SESSION['id_estabelecimento'] > 0 ) {
      $txt .= "   AND HSSTITU.NNUMEESTAB = :estabelecimento ";
      $sql->addParam(":estabelecimento",$_SESSION['id_estabelecimento']);                                  
    }
                                  
    $txt .= "   AND ROWNUM <= :qtde
              ORDER BY CNOMEUSUA ";            

    $sql->addParam(":qtde",$qtde);  
  
    $sql->executeQuery($txt);
    
    if ($formata->somenteNumeros($sql->result("DNASCUSUA")) == substr($formata->somenteNumeros(date('d/m/Y')),0,4)){
      $aniv = "SELECT COUNT(*) QTD
                 FROM HSSGUIA
                WHERE (CLOCAGUIA = '5' OR CLOCAGUIA = '6')
                  AND DEMISGUIA = :hoje";
      $select->addParam(":hoje",date('d/m/Y'));
      $select-executeQuery($aniv);
      
      $parabens = "Parab�ns pelos seus " . $sql->result("IDADE") . 
                  " anos devida, sr(a). " . $sql->result("CNOMEUSUA") . "!";
      
      if($select->result("QTD") == 0) {
        $tpl->MSG = "<p>".$_SESSION['mensagem_web_empresa_pop']."</p>";
      }
    
    }

    while (!$sql->eof()) {
      $situacao =$sql->result("CSITUUSUA");        
      
      if (($situacao <> "Ativo") and 
	      ($formata->somenteCaracteres($data->dataInvertida($sql->result('DALIBUSUA')),$caracteres = '01234567890')  > $formata->somenteCaracteres(date('Y/m/d'),$caracteres = '01234567890') )  )
        $situacao = "Ativo";
      if ($situacao == "Ativo")
        $tpl->USUARIO_COR = "#000000";
      else
        $tpl->USUARIO_COR = "#FF0000";

	    if ($_SESSION['apelido_operadora'] == 'casf'){ 
        $tpl->USUARIO_ID     = $sql->result("CCARTUSUA");
        $tpl->USUARIO_CODIGO = $sql->result("CCARTUSUA");
	    }
	    else {
        $tpl->USUARIO_ID     = $sql->result("CCODIUSUA");
        $tpl->USUARIO_CODIGO = $sql->result("CCODIUSUA");
	    }        

      $tpl->USUARIO_NOME       = $formata->initCap($sql->result('CNOMEUSUA'));
	    if ($_SESSION['apelido_operadora'] == 'assimedica')
          $tpl->USUARIO_PLANO      = $sql->result("CDESCPLAN");
        else  
          $tpl->USUARIO_PLANO      = $sql->result("CCODIPLAN");
        
      $tpl->USUARIO_NASCIMENTO = $sql->result("DNASCUSUA");
      $tpl->USUARIO_SITUACAO   = $situacao;
      $tpl->MAE_USUARIO        = $sql->result("CNMAEUSUA");	  
      $tpl->block("USUARIO");
      $sql->next();
    }
    
    $tpl->block("RESULTADO");    
  }
  
  if ($_SESSION['sistema'] == 'Operadora') {
    $sql = new Query($bd);
    $txt = "SELECT NNUMEEMPR, CRAZAEMPR, CSITUEMPR
              FROM HSSEMPR
             ORDER BY CRAZAEMPR";
    $sql->executeQuery($txt);
    
    while (!$sql->eof()) {
      $tpl->EMPRESA_ID        = $sql->result("NNUMEEMPR");
      $tpl->EMPRESA_DESCRICAO = $sql->result("CRAZAEMPR");
      
      if ($sql->result("CSITUEMPR") <> 'A') 
        $tpl->EMPRESA_COR = "red";
      else
        $tpl->EMPRESA_COR = "";

      $tpl->block("LINHA_EMPRESA");
      
      $sql->next();
    }
    
    $tpl->block("MOSTRA_EMPRESA");    
  }
  
  $tpl->NOME = $nome;
  
  if ($_SESSION['apelido_operadora'] == 'nossaSaude')
    $tpl->MASCARA = "999999.9999.99";
  else
    $tpl->MASCARA = "";
    
  $tpl->CARTEIRA   = $carteira;
  $tpl->NASCIMENTO = $nascimento;
  $tpl->MAE        = $mae;
  $tpl->QTDE       = $qtde;
  $tpl->INCLUSAO   = $inclusao;
  
  $bd->close();
  $tpl->show();  
?>