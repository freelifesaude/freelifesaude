<?php
  header("P3P: CP=\"CAO PSA OUR\"");
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd      = new Oracle();
  $util    = new Util();
  $func    = new Funcao();
  $formata = new Formata();
  $data    = new Data();
  
  $arquivo = str_replace(' ','_','EXTRATO DE MENSALIDADES E ATENDIMENTOS COBRADOS'."_".$data->dataAtual('YYYMMDD'));    
  $arquivo = $formata->somenteCaracteres($arquivo,'ABCDEFGHIJKLMNOPQRSTUVXYWZ01234567890_');
  $arquivo = $util->corrigeNomeArquivo($arquivo);  
  
  $util->excluiArquivo("../temp",$arquivo.".csv");
  $ponteiro = fopen ("../temp/".$arquivo.".csv", "w");
  $delimitador = ';';
  
  $titular = "";
  $competencia = "";
  $matricula = "";
  $cpf = "";
  $total_copart_titular = 0;
  $total_mensal_titular = 0;
  $total_copart_dep = 0;
  $total_mensal_dep = 0;    
  $total_geral  = 0;
  
  $linha = 'NOME TITULAR'.$delimitador.
           'MATRICULA'.$delimitador.  
           'COMPETENCIA'.$delimitador.  
           'CPF'.$delimitador.
           'VALOR MENSALIDADE TITULAR'.$delimitador.
           'VALOR MENSALIDADE DEPENDENTES'.$delimitador.
           'UTILIZAÇÃO TITULAR'.$delimitador.    
           'UTILIZAÇÃO DEPENDENTES'.$delimitador.           
           'TOTAL'.$delimitador;

  fwrite($ponteiro,$linha.chr(13).chr(10));    
  
  
  while (!$sql->eof()) {
  
    if (($titular <> $sql->result("TITULAR")) or ($competencia <> $sql->result("COMPETENCIA"))) {   
       
      $titular = $sql->result("TITULAR");
      $competencia = $sql->result("COMPETENCIA");
      $total_copart_titular = 0;
      $total_mensal_titular = 0;
      $total_copart_dep = 0;
      $total_mensal_dep = 0;        
      $total_geral  = 0;
    }
    
    if ($sql->result("COBRANCA") == "3") {
      if ($sql->result("CTIPOUSUA") == "T") 
        $total_mensal_titular = $total_mensal_titular + str_replace(',','.',$sql->result("COBRADO"));
      else
        $total_mensal_dep = $total_mensal_dep + str_replace(',','.',$sql->result("COBRADO"));
      $total_geral = $total_geral + str_replace(',','.',$sql->result("COBRADO")); 
    }
    else { 
      if ($sql->result("CTIPOUSUA") == "T")
        $total_copart_titular = $total_copart_titular + str_replace(',','.',$sql->result("COBRADO"));
      else
        $total_copart_dep = $total_copart_dep + str_replace(',','.',$sql->result("COBRADO"));      
      $total_geral = $total_geral + str_replace(',','.',$sql->result("COBRADO"));
    }  
      
    $matricula = $sql->result("CCHAPUSUA");
    $cpf = $sql->result("C_CPFUSUA");
    
    $sql->next();
    
    if (($titular <> $sql->result("TITULAR")) or ($competencia <> $sql->result("COMPETENCIA"))) {
      $linha = $titular.$delimitador.$matricula.$delimitador.$competencia.$delimitador.$cpf.$delimitador.$formata->formataNumero($total_mensal_titular).$delimitador.$formata->formataNumero($total_mensal_dep).$delimitador.$formata->formataNumero($total_copart_titular).$delimitador.$formata->formataNumero($total_copart_dep).$delimitador.$formata->formataNumero($total_geral).$delimitador;
      fwrite($ponteiro,$linha.chr(13).chr(10));
    }      
   
  }    
  
  fclose ($ponteiro);
  
  if (extension_loaded('zip')) {  
    
    $zip = new ZipArchive();
    $zip->open("../temp/".$arquivo.".zip", ZipArchive::CREATE);
    $zip->addfile("../temp/".$arquivo.".csv",$arquivo.".csv" );
    $zip->close();
    
    $tpl->RESULT = $util->abreArquivo('../temp/'.$arquivo.'.zip');  
    
  } else {
    echo "Erro ao gerar arquivo. Entre em contato com a Operadora. (Erro: php_zip)";
  }  
     
?>