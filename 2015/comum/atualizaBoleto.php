<?php          
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");  
  
  $util = new Util();
  $bd   = new Oracle();
  $seg  = new Seguranca();
  $data = new Data();
  
  $erro         = '';
  $id_pagamento = $_POST['id_pagamento'];

  if ($_SESSION['sistema'] == 'Empresa')
    $atualiza_boleto = $seg->permissaoOutros($bd,"WEBEMPRESAATUALIZABOLETO",false);
  else
    $atualiza_boleto = false;  
    
  if ($_SESSION['apelido_operadora'] == 'clinipam') { 
  $sql_proximo = new Query($bd);
  $txt_proximo = " SELECT SEQATEND.NEXTVAL PROXIMO FROM DUAL";
  $sql_proximo->executeQuery($txt_proximo);
  $proximo = $sql_proximo->result("PROXIMO");
  
  
  $sql_usuario = new Query($bd);
  $txt_usuario = "SELECT CNOMEUSUA, CCODIUSUA FROM HSSUSUA WHERE NNUMEUSUA = :USUARIO";
  $sql_usuario->addParam(":USUARIO",$_SESSION['id_usuario']);
  $sql_usuario->executeQuery($txt_usuario);
  
  $sql_paga_log = new Query($bd);
    $txt_paga_log = "SELECT HSSLOPG.NLIBWLOPG,HSSPAGA.NNUMELOPG,NVENCPAGA,TO_CHAR(DVENCPAGA,'DD/MM/YYYY') DVENCPAGA,NNUMETITU,
                        CASE WHEN DVENCPAGA < TRUNC(SYSDATE) THEN 1 ELSE 0 END VENCIDO,
                        NVL(NLOCAINTER,0) NLOCAINTER,NDOCUPAGA
                   FROM HSSPAGA,HSSINTER,HSSLOPG
                  WHERE NNUMEPAGA = :id 
                    AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG";
    $sql_paga_log->addParam(":id",$id_pagamento);
    $sql_paga_log->executeQuery($txt_paga_log);
    
  
  $assunto = "Benefici�rio(a) ".$sql_usuario->result('CNOMEUSUA')." - c�digo ".$sql_usuario->result('CCODIUSUA').", imprimiu boleto com vencimento em ".$sql_paga_log->result('DVENCPAGA')." e documento n� ".$sql_paga_log->result('NDOCUPAGA')." no Site Clinipam.";
  
  $sql4 = new Query($bd);
  $txt4 = " INSERT INTO HSSATEND(NNUMEATEND,DDATAATEND,CASSUATEND,NNUMEUSUA, NNUMECLATE, NCOBRCLATE)
              VALUES(:id_atend,SYSDATE,:assunto,:usuario, 63183501, 63184299)";
  $sql4->addParam(":id_atend",$proximo);
  $sql4->addParam(":assunto",$assunto);
  $sql4->addParam(":usuario",$_SESSION['id_usuario']);
  $erro4 = $sql4->executeSQL($txt4);
  }   
  if (($atualiza_boleto) or ($_SESSION['atualiza_boleto'] ==  'S') ){  

    $sql_paga = new Query($bd);
    $txt_paga = "SELECT HSSLOPG.NLIBWLOPG,HSSPAGA.NNUMELOPG,NVENCPAGA,TO_CHAR(DVENCPAGA,'DD/MM/YYYY') DVENCPAGA,NNUMETITU,
                        CASE WHEN DVENCPAGA < TRUNC(SYSDATE) THEN 1 ELSE 0 END VENCIDO,
                        NVL(NLOCAINTER,0) NLOCAINTER,NDOCUPAGA
                   FROM HSSPAGA,HSSINTER,HSSLOPG
                  WHERE NNUMEPAGA = :id 
                    AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG";
    $sql_paga->addParam(":id",$id_pagamento);
    $sql_paga->executeQuery($txt_paga);
    
    $boleto = new Boleto($id_pagamento);    
      
    if (($sql_paga->result("VENCIDO") == 1) and
        ($data->numeroDias(date('d/m/Y'),$sql_paga->result("DVENCPAGA")) <= 60)) {    
        
      $retorno = $boleto->CalculaJuros ($sql_paga->result("NNUMETITU"),$sql_paga->result("DVENCPAGA"),'',$sql_paga->result("NVENCPAGA"));
      
      if ((($retorno['juros'] + $retorno['multa']) > 0) or ($sql_paga->result("NLOCAINTER") > 0)) {
            
        $valor = str_replace(',','.',$sql_paga->result("NVENCPAGA")) + $retorno['juros'] + $retorno['multa'];
        $valor = str_replace('.',',',$valor);
        
        $sql = new Query($bd);
        $txt = "SELECT NNUMEJPAGA
                  FROM HSSJPAGA
                 WHERE DVENCJPAGA >= TRUNC(SYSDATE)
                   AND ROUND(NVALOJPAGA,2) = ROUND(:valor,2)
                   AND 1 = (SELECT COUNT(*) FROM HSSMJPAG
                             WHERE NNUMEPAGA = :id
                               AND NNUMEJPAGA = HSSJPAGA.NNUMEJPAGA) ";
        $sql->addParam(":id",$id_pagamento);
        $sql->addParam(":valor",$valor);
        $sql->executeQuery($txt);
      
        if ($sql->count() == 0) {
          $sql->clear();
          $txt = "SELECT SEQPAGA.NEXTVAL NUMERO FROM DUAL";
          $sql->executeQuery($txt);
          $id_jpaga = $sql->result("NUMERO");

          $local = $sql_paga->result("NLIBWLOPG");
          
          if ($local == 0)
            $local = $sql_paga->result("NLOCAINTER");
          
          if ($local == 0)
            $local = $sql_paga->result("NNUMELOPG");
          
          if ($id_jpaga > 0) {    
            $sql->clear();
                        
            $txt = "INSERT INTO HSSJPAGA (NNUMEJPAGA,NNUMELOPG,NVALOJPAGA,NVENCJPAGA,DVENCJPAGA,DDATAJPAGA,NOPERUSUA,CMOTIJPAGA,NJUROJPAGA,NDESCJPAGA,CINSTJPAGA) ".
                   "              VALUES (:id_jpaga,:local,ROUND(:valor,2),:valor,TRUNC(SYSDATE),SYSDATE,decode(:operador,0,null,:operador),'2 via pela web',0,0,:instrucao) ";
            $sql->addParam(":id_jpaga",$id_jpaga);
            $sql->addParam(":local",$local);
            $sql->addParam(":valor",$valor);            

            if ($_SESSION['sistema'] <> 'Usuario')
              $sql->addParam(":operador",$_SESSION['id_operador']);
            else
              $sql->addParam(":operador",0);
              
            $sql->addParam(":instrucao","Referente � mensalidade: ".$sql_paga->result("NDOCUPAGA")." - ".$sql_paga->result("DVENCPAGA") );
              
            $erro = $sql->executeSQL($txt);
            
            if ($erro == '') {
              $sql->clear();
              $txt = "INSERT INTO HSSMJPAG (NNUMEJPAGA,NNUMEPAGA,NVALOMJPAG) ".
                     "              VALUES (:id_jpaga,:paga,:valor) ";
              $sql->addParam(":id_jpaga",$id_jpaga);
              $sql->addParam(":paga",$id_pagamento);
              $sql->addParam(":valor",$valor);
              $erro = $sql->executeSQL($txt);      
            }
          }
        } else
          $id_jpaga = $sql->result("NNUMEJPAGA");
      }          
    } 
    else
      $id_jpaga = 0;
  }
  
  if ($_SESSION['sistema'] == 'Comercial') {
    $sql = new Query($bd);
    $txt = "UPDATE HSSPAGA SET DIMPRPAGA = SYSDATE, NBOLEUSUA = :operador WHERE NNUMEPAGA = :id";
    $sql->addParam(":id",$id_pagamento);
    $sql->addParam(":operador",$_SESSION['id_operador']);
    $erro = $sql->executeSQL($txt);   
  }

  if ($erro == '') {
    $filtros = array();
    $filtros['id_pagamento'] = $id_pagamento;
    $filtros['jpaga']        = $id_jpaga;    
    $filtros['arquivo']      = '';    
    $filtros['para']         = '';    
    $filtros['pedido']       = '';    
    
    $_SESSION['filtrosBoleto'] = $filtros;
    
    $util->redireciona("../comum/boleto.php?idSessao=".$_GET['idSessao'],"N");        
  }                         
  
?>