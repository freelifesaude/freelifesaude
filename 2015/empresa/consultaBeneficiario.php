<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  
  $_SESSION['titulo'] = "CONSULTA DE BENEFICI�RIO / ELEGIBILIDADE";
  require_once("../comum/layout.php");
  $tpl->addFile("CONTEUDO","../comum/consultaBeneficiario.html");   
        
  $tpl->ID_SESSAO = $_GET['idSessao'];
  $tpl->MASCARA = $_SESSION['mascara'];

	
  $carteira     = '';
  $procedimento = '';
  $natureza     = '';
  $regime       = '';
  $tpl->TYPE = "text";
  $tpl->HABILITADO = "readonly";
  $tpl->block("LEITORA");
  $tpl->DESC_LEITORA = "Cart�o";
    
	//if (($_SESSION['localizacao_usuarios'] == "S" and $_SESSION['tem_leitora'] == "N") or
   //   ($seg->permissaoOutros($bd,"WEBPRESTADORLOCALIZABENEFICIRIO",true)))
    $tpl->block("BUSCA_BENEFICIARIO");
  if (isset($_SESSION['tem_leitor_biometrico'])) {
    if ($_SESSION['tem_leitor_biometrico'] == 'S')
      $tpl->block("VERIFICA_BIOMETRIA");
  }    
  
  $tpl->BOTAO = "Consultar elegibilidade";
  //$tpl->block("ELEGIBILIDADE");
    
  if (isset($_POST['consultar'])) {
	
		
    $carteira = trim($_POST['codigo']);
    $procedimento = trim($_POST['procedimento']);
    $natureza = trim($_POST['natureza']);
    $regime = trim($_POST['regime']);
	
    $sql = new Query($bd);
    $sql->clear();
    $txt = "SELECT HSSUSUA.CGRAUUSUA,HSSUSUA.CNOMEUSUA,HSSUSUA.CCODIUSUA,HSSUSUA.CTIPOUSUA,CDESCTLOGR,HSSUSUA.CENDEUSUA,HSSUSUA.CBAIRUSUA,
                   HSSUSUA.CCEP_USUA,HSSUSUA.CCIDAUSUA,HSSUSUA.CESTAUSUA,TITULAR.CNOMEUSUA TITULAR, 
                   CDESCPLAN,CRAZAEMPR,HSSUSUA.C_CPFUSUA,HSSUSUA.CORRGUSUA,HSSUSUA.C__RGUSUA, 
                   TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY') DSITUUSUA,HSSUSUA.NNUMEUSUA, 
                   TO_CHAR(HSSUSUA.DALIBUSUA,'DD/MM/YYYY') DALIBUSUA, 
                   TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,HSSUSUA.CSITUUSUA, 
                   TO_CHAR(HSSUSUA.DDEMIUSUA,'DD/MM/YYYY') DDEMIUSUA, 
                   TO_CHAR(HSSUSUA.DINCLUSUA,'DD/MM/YYYY') DINCLUSUA,HSSUSUA.CCARTUSUA,HSSTITU.NCPOSTITU, 
                   DECODE(HSSSTAT.NNUMESTAT,NULL,STAT_TITU.NNUMESTAT,HSSSTAT.NNUMESTAT) NNUMESTAT,
                   DECODE(HSSSTAT.NNUMESTAT,NULL,STAT_TITU.CWEB_STAT,HSSSTAT.CWEB_STAT) CWEB_STAT,
                   DECODE(HSSSTAT.NNUMESTAT,NULL,STAT_TITU.CGUIASTAT,HSSSTAT.CGUIASTAT) CGUIASTAT,
                   VALIDA_REDE_ATENDIMENTO30(HSSUSUA.NNUMEUSUA,:contratado) REDE,
                   VALOR_USUARIO_VENCIMENTO(HSSUSUA.NNUMEUSUA,SYSDATE) VALOR,CCOBEPLAN                   
              FROM HSSUSUA,HSSUSUA TITULAR,HSSPLAN,HSSTITU,HSSEMPR,HSSSTAT,HSSSTAT STAT_TITU,HSSTLOGR 
             WHERE HSSUSUA.CCODIUSUA = :carteira
							 AND HSSTITU.NNUMETITU = :id_contrato
               AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA 
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU 
               AND HSSTITU.NNUMEEMPR = HSSEMPR.NNUMEEMPR(+) 
               AND HSSUSUA.NNUMESTAT = HSSSTAT.NNUMESTAT(+)
               AND HSSTITU.NNUMESTAT = STAT_TITU.NNUMESTAT(+)               
               AND HSSUSUA.NFATUTLOGR = HSSTLOGR.NNUMETLOGR(+)
             ORDER BY 1 DESC,2";
    $sql->addParam(":carteira",$carteira);
    $sql->addParam(":contratado",$_SESSION['id_contratado']);
		$sql->addParam(":id_contrato",$_SESSION['id_contrato']);		
		
    $sql->executeQuery($txt);	
		
    
    if ($sql->count() > 0) {
      if ($procedimento <> '' and $natureza <> '' and $regime <> '') {
        $txt_cob = "SELECT ELEGIBILIDADEWEB30(:usuario,:procedimento,:natureza,:regime,:prestador) ELEGIBILIDADE FROM DUAL ";
        $sql_cob = new Query($bd);
        $sql_cob->addParam(":usuario",$sql->result("NNUMEUSUA"));
        $sql_cob->addParam(":procedimento",$procedimento);
        $sql_cob->addParam(":regime",$regime);
        $sql_cob->addParam(":natureza",$natureza);
        $sql_cob->addParam(":prestador",$_SESSION['id_contratado']);
        $sql_cob->executeQuery($txt_cob);

        if ($sql_cob->count() > 0) {
          $tpl->MSG_ELE = $sql_cob->result("ELEGIBILIDADE");
          $tpl->block("RESULTADO_ELEGIBILIDADE");
        }
      } 
    
      if ($sql->result("CSITUUSUA") == 'A') {
        if (strtotime($data->dataInvertida($sql->result("DINCLUSUA"))) > strtotime(date('Ymd')))
          $situacao = '<font color="red">Atendimento liberado ap�s ' . $sql->result("DINCLUSUA") . '</font>';
        else if ($sql->result("NNUMESTAT") > 0) {
        
          $situacao = '<span style="background-color:yellow">' . $func->retornaNomeStatus($bd,$sql->result("NNUMESTAT"));          
          
          if (($sql->result("CWEB_STAT") == 'N') or ($sql->result("CGUIASTAT") == 'N'))
            $situacao .= " - Entre em contato com a operadora";
          
          $situacao .= '</span>'; 
          
        } else
          $situacao = '<font color="green">ATIVO ' . $func->trocaDePlanos($bd,$sql->result("NNUMEUSUA")) . '</font>';
      }
      else if ($sql->result("CSITUUSUA") == 'F')
        $situacao = '<font color="red">Falecido em ' . $sql->result("DSITUUSUA") . '</font>';
      else if ($sql->result("CSITUUSUA") == 'C')
        if ($sql->result("DALIBUSUA") <> null && strtotime($data->dataInvertida($sql->result("DALIBUSUA"))) >= strtotime($data->dataInvertida($_SESSION['hoje']))) 
          $situacao = '<span style="background-color:yellow">Atendimento at� ' . $sql->result("DALIBUSUA") . ' - Cancelado em ' . $sql->result("DSITUUSUA") . '</span>';
        else
          $situacao = '<font color="red">Cancelado em ' . $sql->result("DSITUUSUA") . '</font>';
      else if ($sql->result("CSITUUSUA") == 'S')
        $situacao = '<font color="red">Suspenso em ' . $sql->result("DSITUUSUA") . '</font>';
      else if ($sql->result("CSITUUSUA") == 'M') {
        if ($sql->result("DALIBUSUA") <> null && strtotime($data->dataInvertida($sql->result("DALIBUSUA"))) >= strtotime($data->dataInvertida($_SESSION['hoje']))) 
          $situacao = '<span style="background-color:yellow">Atendimento at� ' . $sql->result("DALIBUSUA") . ' - Migrado em ' . $sql->result("DSITUUSUA") . '</span>';
        else if ($sql->result("DDEMIUSUA") <> null)
          $situacao = '<font color="red">Demitido em ' . $sql->result("DDEMIUSUA") . '(' . $func->codigoTitulo($bd,$sql->result("NCPOSTITU")) . ')' . '</font>';
        else
          $situacao = '<font color="red">Migrou em ' . $sql->result("DSITUUSUA") . ' para ' . $func->codigoTitulo($bd,$sql->result("NCPOSTITU")) . '</font>';
      }    
    
      $tpl->CONSULTA_CODIGO = $sql->result("CCODIUSUA");
      $tpl->CONSULTA_NOME = $sql->result("CNOMEUSUA");
      $tpl->CONSULTA_INCLUSAO = $sql->result("DINCLUSUA");
      $tpl->CONSULTA_NASCIMENTO = $sql->result("DNASCUSUA");
      $tpl->CONSULTA_CATEGORIA = $func->categoriaUsuario($sql->result("CTIPOUSUA"));
      $tpl->CONSULTA_PARENTESCO = $func->grauDeParentesco($sql->result("CGRAUUSUA"));
      $tpl->CONSULTA_SITUACAO = $situacao;
      $tpl->CONSULTA_VALOR = $formata->formataNumero($sql->result("VALOR"));
	    if ($_SESSION['apelido_operadora'] == "affego"){
        if ($seg->permissaoOutros($bd,"WEBPRESTADORMOSTRACPFENDERECO",true)){
	        $tpl->CONSULTA_CPF = $formata->formataCPF($sql->result("C_CPFUSUA"));
          $tpl->CONSULTA_LOGR = ($sql->result("CDESCTLOGR"))." ".$sql->result("CENDEUSUA")." - "."Bairro ".$sql->result("CBAIRUSUA")." - ".$sql->result("CCEP_USUA")." - ".$sql->result("CCIDAUSUA")."  -  ". $sql->result("CESTAUSUA");
          $tpl->block("MOSTRA_CPF_END");                  
         }                     
      }	
      else {
        $tpl->CONSULTA_CPF = $formata->formataCPF($sql->result("C_CPFUSUA"));
        $tpl->CONSULTA_RG = $sql->result("C__RGUSUA")." / ".$sql->result("CORRGUSUA");
        $tpl->block("MOSTRA_CPF_RG");
      }
      $tpl->CONSULTA_CARTEIRA = $sql->result("CCARTUSUA");
      $tpl->CONSULTA_PLANO = $sql->result("CDESCPLAN");
      $tpl->CONSULTA_ACOMODACAO = $sql->result("ACOMODACAO");
      $tpl->CONSULTA_COBERTURA = $func->coberturaPlano($sql->result("CCOBEPLAN"));
      
      if (($sql->result("CTIPOUSUA") <> 'T') and ($sql->result("CTIPOUSUA") <> 'F')) {
        $tpl->CONSULTA_TITULAR = $sql->result("TITULAR");
        $tpl->block("MOSTRA_TITULAR");
      }
      
      if ($sql->result("CRAZAEMPR") <> '') {
        $tpl->CONSULTA_EMPRESA = $sql->result("CRAZAEMPR");
        $tpl->block("MOSTRA_EMPRESA");        
      }
      
      if ($_SESSION['id_contratado'] > 0) {
        if ($sql->result("REDE") == 0)
          $tpl->CONSULTA_REDE = "N�o";
        else
          $tpl->CONSULTA_REDE = "Sim";
        
        $tpl->block("MOSTRA_REDE");
      }
      
      $sql2 = new Query($bd);
      $sql2->clear();
      $txt = "SELECT CFONETLUSU,COBSETLUSU FROM HSSTLUSU
               WHERE NNUMEUSUA = :usuario
               UNION ALL
              SELECT CFONETLCON,COBSETLCON FROM HSSUSUA,HSSTLCON
               WHERE NNUMEUSUA = :usuario
                 AND HSSUSUA.NNUMETITU = HSSTLCON.NNUMETITU
               UNION ALL
              SELECT CFONETLUSU,COBSETLUSU FROM HSSUSUA,HSSTLUSU
               WHERE HSSUSUA.NNUMEUSUA = :usuario
                 AND HSSUSUA.NTITUUSUA <> HSSUSUA.NNUMEUSUA
                 AND HSSUSUA.NTITUUSUA = HSSTLUSU.NNUMEUSUA ";
                
      $sql2->addParam(":usuario",$sql->result("NNUMEUSUA"));
      $sql2->executeQuery($txt); 

      $telefone = '';
      while (!$sql2->eof()) {
        $telefone .= $sql2->result("CFONETLUSU")." ".$sql2->result("COBSETLUSU")."<br />";
        $sql2->next();
      }       
      
      $tpl->CONSULTA_TELEFONES = substr($telefone,0,strlen($telefone)-6);
      
      $sql2->clear();
      $txt = "SELECT CDESCTXMEN
                FROM HSSTXUSU,HSSTXMEN
               WHERE NNUMEUSUA = :usuario
                 AND DCANCTXUSU IS NULL
                 AND HSSTXUSU.NNUMETXMEN = HSSTXMEN.NNUMETXMEN";
      $sql2->addParam(":usuario",$sql->result("NNUMEUSUA"));
      $sql2->executeQuery($txt); 

      $aditivo = '';
      while (!$sql2->eof()) {
        $aditivo .= $sql2->result("CDESCTXMEN")."<br />";
        $sql2->next();
      }  
         
      $tpl->CONSULTA_ADITIVOS = substr($aditivo,0,strlen($aditivo)-6);
      $tpl->block("ADITIVOS"); 
      
      $sql->clear();       
      $txt = "SELECT CGRAUUSUA,CNOMEUSUA,CNOMECARE,DVENCCRUS,CCODIUSUA,HSSCRUS.NNUMECARE,HSSCRUS.NNUMEUSUA
                FROM HSSUSUA,HSSCRUS,HSSCARE
               WHERE HSSUSUA.CCODIUSUA = :carteira
                 AND HSSUSUA.NNUMEUSUA = HSSCRUS.NNUMEUSUA
                 AND HSSCRUS.NNUMECARE = HSSCARE.NNUMECARE
               ORDER BY 1 DESC,2,4 DESC";
      $sql->addParam(":carteira",$carteira);
      $sql->executeQuery($txt);
          
      if ($sql->count() > 0) { 
        while (!$sql->eof()) {
          $tpl->CARENCIA_NOME = $sql->result("CNOMECARE");
          $tpl->CARENCIA_VENCIMENTO = $sql->result("DVENCCRUS");
          $tpl->CARENCIA_ID = $sql->result("NNUMECARE");
          $tpl->ID_USUARIO = $sql->result("NNUMEUSUA");
          $tpl->block("LINHA");        
          $sql->next();     
        }
        $tpl->block("CARENCIAS");              
        
      } else {
        $tpl->MSG = "** Benefici�rio sem car�ncia **";
        $tpl->block("SEM_CARENCIA");
      }      
      $tpl->block("RESULTADO_CARENCIAS"); 
    
      $tpl->block("RESULTADO");
    } else {
      $tpl->block("ERRO");    
    }
  }

  $tpl->CODIGO_BENEFICIARIO = $carteira;
  $tpl->PROCEDIMENTO        = $procedimento;  
  $tpl->NATUREZA            = $natureza;
  $tpl->REGIME              = $regime;  
  
  $tpl->block("MOSTRA_MENU");  
  $bd->close();
  $tpl->show();     

?>