<?php
  Session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");
  $bd = new Oracle();

  require_once("../comum/layoutJanela.php"); 
  $tpl->block("MOSTRA_LOGO");
  
  $tpl->addFile("JANELA_CONTEUDO","localizaProcedimento.html"); 
  
  $qtde  = $_SESSION['quantidade_resultado'];
  $campo = $seg->antiInjection($_POST['campo']);
  $tpl->CAMPO = $campo;  

  if (isset($_POST['prestador']))
    $prestador = $seg->antiInjection($_POST['prestador']);
  else
    $prestador = '';
    
  if (isset($_POST['pedido']))
    $pedido = $seg->antiInjection($_POST['pedido']);  
  else
    $pedido = '';

  if (isset($_POST['localizar'])){
    $prestador = $seg->antiInjection($_POST['id_prestador']);
    $pedido    = $seg->antiInjection($_POST['pedido']);

    if (($_SESSION['regra_autorizacao'] == 'L') and ($pedido == 'N')) {
      $prestador = $_SESSION['id_contratado'];
    }
  }
	
  $tpl->PRESTADOR = $prestador;
  $tpl->PEDIDO    = $pedido;
  $tpl->PCMSO     = $_POST['pcmso'];
  
  $nome   = '';
  $codigo = '';
  
  if (isset($_POST['localizar'])) {
    $nome   = strtoupper($seg->antiInjection($_POST['nome']));   
    $qtde   = $seg->antiInjection($_POST['qtde']);
    $codigo = $seg->antiInjection($_POST['codigo']); 
	
	if($_POST['pcmso'] == 'S')
      $pcmso = " AND NVL(CPCMSPMED,'N') = 'S' ";
    else
      $pcmso = "";
        
    if ((strlen($nome) >= 2 and $nome <> '') or ($codigo <> '')) {
      $sql = new Query($bd);
      $sql->clear();
      
      if ($codigo <> '') {
        $f_codigo = "   AND CCODIPMED LIKE :codigo";
        $codigo2 = $codigo;
        $codigo2 = $codigo2."%";
        $sql->addParam(":codigo",$codigo2);
      } 
      else
        $f_codigo = "";
      
      if ($nome <> '') {
		if($_SESSION['sistema'] == 'Saude_Ocupacional') 
		  $f_nome = "   AND (CNOMEPMED LIKE :nome OR CNPCMPMED LIKE :nome)";
		else		  
          $f_nome = "   AND (CNOMEPMED LIKE :nome OR CMENSPMED LIKE :nome)";
        $nome2 = $nome;
        if (($_SESSION['apelido_operadora'] != "unimedFranca") && ($_SESSION['apelido_operadora'] != "unimedSalto")) {
          $nome2 = strtoupper($formata->somenteCaracteres($nome2))."%";
        } 
        else
          $nome2 = "%".strtoupper($nome2)."%";		  
        $sql->addParam(":nome",$nome2);      
      }
          
      $txt = "SELECT CCODIPMED,NVL(CNPCMPMED,CNOMEPMED) CNOMEPMED
               FROM HSSPMED 
              WHERE CSITUPMED = 'A' ".
              $f_codigo.
              $f_nome.
			  $pcmso.
             "  AND CCODIPMED IN ( SELECT PMED.CCODIPMED FROM HSSPMED PMED
                                    WHERE PMED.CLINTPMED = 'S'
                                      AND PMED.CCODIPMED = HSSPMED.CCODIPMED
                                    UNION ALL
                                    SELECT PMED.CCODIPMED FROM HSSPMED GRUP,HSSPMED PMED
                                    WHERE GRUP.CLINTPMED = 'S'
                                      AND SUBSTR(GRUP.CCODIPMED,1,7) = GRUPOPROCEDIMENTO(HSSPMED.CCODIPMED)
                                      AND SUBSTR(GRUP.CCODIPMED,1,4) = SUBSTR(PMED.CCODIPMED,1,4)
                                    UNION ALL
                                    SELECT PMED.CCODIPMED FROM HSSPMED GRUP,HSSPMED PMED
                                    WHERE GRUP.CLINTPMED = 'S'
                                      AND SUBSTR(GRUP.CCODIPMED,1,7) = ESPECIALIDADEPROCEDIMENTO(HSSPMED.CCODIPMED)
                                      AND SUBSTR(GRUP.CCODIPMED,1,2) = SUBSTR(PMED.CCODIPMED,1,2)
									UNION ALL
                                   SELECT CCODIPMED FROM HSSPRWEB
								    WHERE NVL(NNUMEPRES,0) = NVL(:prestador,0))
                AND SUBSTR(HSSPMED.CCODIPMED,1,7) <> GRUPOPROCEDIMENTO(HSSPMED.CCODIPMED) 
                AND SUBSTR(HSSPMED.CCODIPMED,1,7) <> ESPECIALIDADEPROCEDIMENTO(HSSPMED.CCODIPMED) " ;
                      
      $sql->addParam(":prestador",$prestador);          
		
      if ($prestador > 0) {
        $txt .= "   AND SUBSTR(PROCEDIMENTO_AUTORIZADO(:prestador,CCODIPMED,'T',SYSDATE,'B',0,null,'N','S'),1,1) = 'S' ";
        $sql->addParam(":prestador",$prestador);          
      }

      $txt .= "   AND ROWNUM <= :qtde
                ORDER BY CNOMEPMED ";
                          
      $sql->addParam(":qtde",$qtde);    

      $sql->executeQuery($txt);
              
      while (!$sql->eof()) {                                           
        $tpl->PROCEDIMENTO_ID   = $sql->result('CCODIPMED');
        $tpl->PROCEDIMENTO_NOME = $formata->initCap($sql->result('CNOMEPMED'));
        $tpl->block("PROCEDIMENTO");
        $sql->next();
      }
    }
    $tpl->block("RESULTADO");    
  }
  
  $tpl->NOME   = $nome;
  $tpl->CODIGO = $codigo;  
  $tpl->QTDE   = $qtde;

  $bd->close();
  $tpl->show();  
?>