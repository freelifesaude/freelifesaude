<?php
  session_start();

  require_once('../comum/sessao.php'); 
  require_once("../comum/autoload.php");

  $bd = new Oracle();
  
  $_SESSION['titulo'] = "CADASTRO DO BENEFICIÁRIO";
  
  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/dados.htm");  

  $sql =  new Query($bd);
  
  $id     = $_POST['id'];
  $estado = $_SESSION['uf_operadora'];

  if (isset($_POST['voltar']))
    redireciona("cadastro.php","N");
    
  if ($id > 0) {
    $sql->clear();    
    $txt = "SELECT HSSUSUA.CNOMEUSUA,HSSUSUA.CCHAPUSUA,TO_CHAR(HSSUSUA.DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,HSSUSUA.DINCLUSUA, 
                   HSSUSUA.CPIS_USUA,HSSUSUA.C_CPFUSUA,HSSUSUA.C__RGUSUA,TO_CHAR(HSSUSUA.DSITUUSUA,'DD/MM/YYYY') DSITUUSUA, 
                   HSSUSUA.CORRGUSUA,HSSUSUA.CGRAUUSUA,HSSUSUA.CTIPOUSUA,HSSUSUA.NTITUUSUA,HSSUSUA.NNUMEPLAN,
                   HSSUSUA.NNUMEUSUA,HSSUSUA.CNMAEUSUA,HSSUSUA.CSEXOUSUA,HSSUSUA.NNUMESETOR,HSSUSUA.CENDEUSUA,HSSUSUA.CBAIRUSUA,
                   HSSUSUA.CCIDAUSUA,HSSUSUA.CESTAUSUA,HSSUSUA.CCEP_USUA,TO_CHAR(HSSUSUA.DEXRGUSUA,'DD/MM/YYYY') DEXRGUSUA,
                   HSSUSUA.CREFEUSUA,HSSUSUA.CNUMEUSUA,HSSUSUA.CMAILUSUA,HSSUSUA.NSALAFUNCI,CDESCTLOGR,
                   CDESCPLAN,TITULAR.CNOMEUSUA TITULAR,HSSUSUA.CCODIUSUA,CDESCMCANC,HSSUSUA.CSITUUSUA,
                   NVL(HSSUSUA.NNUMEACOM,NVL(HSSTITU.NNUMEACOM,HSSPLAN.NNUMEACOM)) NNUMEACOM
              FROM HSSUSUA,HSSTITU,HSSPLAN,HSSTLOGR,HSSUSUA TITULAR,HSSMCANC
             WHERE HSSUSUA.NNUMEUSUA = :id 
               AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN
               AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU
               AND HSSUSUA.NFATUTLOGR = HSSTLOGR.NNUMETLOGR(+)
               AND HSSUSUA.NNUMEMCANC = HSSMCANC.NNUMEMCANC(+)
               AND HSSUSUA.NTITUUSUA = TITULAR.NNUMEUSUA ";
    
    $sql->addParam(":id",$id);
    $sql->executeQuery($txt);

    $tpl->CADASTRO_CODIGO = $sql->result("CCODIUSUA");  
    $tpl->CADASTRO_NOME = $sql->result("CNOMEUSUA");
    $tpl->CADASTRO_CATEGORIA = $func->categoriaUsuario($sql->result("CTIPOUSUA"));  
    $tpl->CADASTRO_DATA_INCLUSAO = $sql->result("DINCLUSUA");
    $tpl->CADASTRO_NASCIMENTO = $sql->result("DNASCUSUA"); 
    
    if ($sql->result("CSEXOUSUA") == 'F')
      $tpl->CADASTRO_SEXO = "Feminino";
    else
      $tpl->CADASTRO_SEXO = "Masculino";  
    $tpl->CADASTRO_GRAU = $func->grauDeParentesco($sql->result("CGRAUUSUA"));  
    $tpl->CADASTRO_CPF = $sql->result("C_CPFUSUA");  
    $tpl->CADASTRO_RG = $sql->result("C__RGUSUA");
    $tpl->CADASTRO_EXPEDICAO = $sql->result("DEXRGUSUA");
    $tpl->CADASTRO_ORGAO = $sql->result("CORRGUSUA");
    $tpl->CADASTRO_PIS = $sql->result("CPIS_USUA");
    $tpl->CADASTRO_MAE = $sql->result("CNMAEUSUA");  
    $tpl->CADASTRO_TITULAR = $sql->result("TITULAR");
    $tpl->CADASTRO_PLANO = $sql->result("CDESCPLAN"); 
    $tpl->CADASTRO_ACOMODACAO = $func->retornaNomeAcomodacao($bd,$sql->result("NNUMEACOM")); 
    $tpl->CADASTRO_CEP = $sql->result("CCEP_USUA");
    $tpl->CADASTRO_TIPO_LOGRADOURO = $sql->result("CDESCTLOGR");
    $tpl->CADASTRO_LOGRADOURO = $sql->result("CENDEUSUA");
    $tpl->CADASTRO_NUMERO = $sql->result("CNUMEUSUA");
    $tpl->CADASTRO_COMPLEMENTO = $sql->result("CREFEUSUA");
    $tpl->CADASTRO_BAIRRO = $sql->result("CBAIRUSUA");
    $tpl->CADASTRO_ESTADO = $sql->result("CESTAUSUA");
    $tpl->CADASTRO_CIDADE = $sql->result("CCIDAUSUA"); 
    $tpl->CADASTRO_DATA_CANCELAMENTO = $sql->result("DSITUUSUA");
    $tpl->CADASTRO_MOTIVO_CANC = $sql->result("CDESCMCANC");  

    if ($sql->result("CSITUUSUA") == 'A') {
      $tpl->COR = "navy";
      $tpl->CADASTRO_SITUACAO = "ATIVO";
    }
    else if ($sql->result("CSITUUSUA") == 'C') {
      $tpl->COR = "red";    
      $tpl->CADASTRO_SITUACAO = "CANCELADO";
      $tpl->block("DADOS_CANCELAMENTO");
    }
    else if ($sql->result("CSITUUSUA") == 'C') {
      $tpl->COR = "purple";    
      $tpl->CADASTRO_SITUACAO = "MIGRADO";
    }
  }
 
  $tpl->PAGINA_VOLTAR = "cadastro.php?op=A&idSessao=".$_GET['idSessao'];
  
  $tpl->block("MOSTRA_MENU");  
 
  $bd->close();
  $tpl->show();       
?>
  
  