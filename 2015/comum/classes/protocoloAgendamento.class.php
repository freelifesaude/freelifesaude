<?php
	define("FPDF_FONTPATH", "../comum/pdf/font");
	require_once("../comum/pdf/fpdf.php");
  
  require_once("../comum/autoload.php");    
  
	class protocoloAgendamento extends FPDF{ 
	  private $arquivo_gerado;
	  private $marcacao;
	  private $arquivopdf;
  	  
	  function arquivoGerado(){
		  return $this->arquivo_gerado;
	  }
    
	  function geraProtocolo($banco, $id_agenda, $exibeImprime = ''){
		  if($banco == null)
		    $bd = new Oracle();
		  else
		    $bd = $banco;
	
		  $pdf=new FPDF('P','mm','A4');
		  $pdf->AliasNbPages();    
		      
		  $sql = new Query($bd);
		  $txt = "SELECT NNUMEAGEND,TO_CHAR(DMARCAGEND,'DD/MM/YYYY HH24:Mi') DMARCAGEND,TO_CHAR(DDATAAGEND,'DD/MM/YYYY') DDATAAGEND,
                     CHORAAGEND, A.CNOMEPRES PRESTADOR, P.CNOMEPRES AMBULATORIO,CDESCTLOGR,P.CENDEPRES,P.CNENDPRES,P.CBAIRPRES,CNOMEUSUA,NHCNAINTER
                FROM HSSAGEND, HSSUSUA, FINPRES A, FINPRES P, HSSTLOGR, HSSINTER
               WHERE HSSAGEND.NNUMEUSUA = HSSUSUA.NNUMEUSUA
                 AND HSSAGEND.NNUMEPRES = A.NNUMEPRES
                 AND HSSAGEND.NAMBUPRES = P.NNUMEPRES
                 AND A.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)
                 AND NNUMEAGEND = :agenda ";
      $sql->addParam(":agenda",$id_agenda);
      $sql->executeQuery($txt);
      		
		  if($sql->result("CNENDPRES") <> '')
		    $num_endereco = $sql->result("CNENDPRES");
		  else
		    $num_endereco = '';
		
		  $this->marcacao =  $sql->result("DMARCAGEND");
		  $pdf->Open();
		  $pdf->AddPage();
		  $pdf->Image('../comum/img/logo_relatorio.jpg',10,5,40,10);
		  $pdf->SetFont('Arial','B',10);
		  $pdf->Cell(45,3,'');
		
		  $pdf->Cell(125,4,$_SESSION['nome_operadora'],0,1);

		  $pdf->Ln(4);		
		  $pdf->SetFillColor(0,0,0);
		  $pdf->Cell(185,1,'',0,1,'',true);
		  $pdf->Ln(2);
		
		  $pdf->Cell(43,5,utf8_decode('Protocolo número: '),0,0,'R');
		  $pdf->Cell(30,5,$sql->result("NNUMEAGEND"),0,1);
		  $pdf->Cell(43,5,'Consulta agendada em: ',0,0,'R');
		  $pdf->Cell(50,5,$sql->result("DMARCAGEND"),0,1);
		  $pdf->SetFont('Arial','',8);
		  $pdf->Ln(3);
		  $texto = 'Sr(a) ' . $sql->result("CNOMEUSUA") . ', sua consulta ficou agendada com o(a) Dr(a). ' . $sql->result("PRESTADOR") . ' dia ' .
		           $sql->result("DDATAAGEND") . ' as ' . $sql->result("CHORAAGEND") . utf8_decode(' no endereço ') . $sql->result("CDESCTLOGR") . ', ' . $sql->result("CENDEPRES") .
			         ' ' . $num_endereco ;
				  				  
		  $pdf->MultiCell(185,5,$texto,0,'J');
		  $pdf->Ln(2);
		  $texto1 = 'Solicitamos que o sr(a) compareça na data da consulta  com 10 minutos de antecedência.';	
		  $pdf->MultiCell(185,5,utf8_decode($texto1),0,'J');
		  $pdf->Ln(2);
      
      if ($_SESSION['apelido_operadora'] == 'clinipam')
		    $texto2 = 'Se for necessário desmarcar sua consulta, favor entrar em contato com a Clinipam com '. $sql->result("NHCNAINTER") .' horas de antecedência através do site ' .
		              'www.clinipam.com.br ou ligue no fone 3021-3001 opção 1';	
      else      
		    $texto2 = 'Se for necessário desmarcar sua consulta, favor entrar em contato com a Unimed com '. $sql->result("NHCNAINTER") .' horas de antecedência através do site ' .
		              'xxxxxxxx ou ligue no fone 9999999';	
      
		  $pdf->MultiCell(185,5,utf8_decode($texto2),0,'J');
		
		  $pdf->Ln(6);
		  $pdf->Cell(150,5,utf8_decode('Contamos com sua colaboração.'),0,0);
		
      /*	$pdf->Cell(40,5,'Data do atendimento:',0,0,'R');  
      $pdf->Cell(40,5,$sql->result("DDATAAGEND"),0,1);
      $pdf->Cell(40,5,utf8_decode("Horário:"),0,0,'R');  
      $pdf->Cell(40,5,$sql->result("CHORAAGEND"),0,1);  
      $pdf->Cell(40,5,utf8_decode("Médico:"),0,0,'R');  
      $pdf->Cell(40,5,utf8_decode($sql->result("PRESTADOR")),0,1);  
      $pdf->Cell(40,5,'Local:',0,0,'R');  
      $pdf->Cell(40,5,utf8_decode($sql->result("AMBULATORIO") . " - " . $sql->result("CDESCTLOGR") . ", " . $sql->result("CENDEPRES") . " " . $num_endereco),0,1);    
      $pdf->Cell(40,5,'Atendimento agendado em:',0,0,'R');  
      $pdf->Cell(40,5,utf8_decode($sql->result("DMARCAGEND")),0,1);    
      $pdf->Ln(1);*/
		
		  //  $file=basename(tempnam("../temp/",'tmp'));
		  $file='../temp/protocolo_' .$sql->result("NNUMEAGEND"). '.pdf';
		  $this->arquivopdf = $file;
		  $pdf->Output($file,'F');
		  $this->arquivo_gerado = $file;
		  $bd->close();
		  //exibe no momento da geração (mesma tela)
		  if($exibeImprime == 'E')			
			  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
        //exibe, porém em uma nova janela
		  else if($exibeImprime == 'I')
			  echo "<HTML><SCRIPT>window.open('$file');</SCRIPT></HTML>";
      //else
      //  echo $this->arquivo_gerado;
	  }
	  	  
	  function enviaProtocolo($email){
      $func  = new Funcao();
      $datas = new Data();

		  $resultado = '1';
      
		  if(file_exists($this->arquivo_gerado)){
				
        if ($_SESSION['apelido_operadora'] == 'clinipam')
          $logo = "<img src=\"http://200.175.6.50:1000/solusweb_teste/comum/imagens/logo_agenda.jpg\" width=\"136\" height=\"104\"/>";
        else
          $logo = '';
  
				$msg = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
				        <html xmlns=\"http://www.w3.org/1999/xhtml\">
				        <head>
				          <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
				          <title>Protocolo de agendamento</title>
				          <style type=\"text/css\">
				            .titulo {
					            font: normal 14px Verdana, Geneva, sans-serif;
					            font-weight:bold;
				            }
				
                    .texto {
                      font: normal 14px Verdana, Geneva, sans-serif;
                      font-weight:bold;
                      color:#060;
                    }
                    
                    .texto1 {
                      font: normal 12px Verdana, Geneva, sans-serif;
                      color:#060;
                    }
				          </style>
				        </head>				
				        <body>
				          <table width=\"700\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				            <tr>
					            <td width=\"148\" height=\"72\" align=\"left\">".$logo."</td>
					            <td width=\"552\" class=\"titulo\">".$_SESSION['nome_operadora']."</td>
				            </tr>
				            <tr>
					            <td>&nbsp;</td>
					            <td>&nbsp;</td>
				            </tr>  
				            <tr>
					            <td>&nbsp;</td>
					            <td>
                        <p class=\"texto\">" . $datas->boasVindas() . " , " . $_SESSION['nome_usuario'] . "</p>
					              <p class=\"texto1\">Anexo segue seu protocolo de agendamento realizado em " . $this->marcacao . "</p>
                      </td>
				            </tr>				
				          </table>
				        </body>
				      </html>";	
										
				$arquivo_anexo[0]['arquivo'] = $this->arquivo_gerado; 
				$arquivo_anexo[0]['name']    = basename($this->arquivo_gerado);
				$arquivo_anexo[0]['type']    = 'pdf';
        
				$resultado = $func->enviaEmail($email,$_SESSION['nome_operadora']."  - Agendamento de consulta",$msg,$arquivo_anexo);	
      }      
      
			return $resultado;
	}
	function geraProtocoloCancelamento($banco, $id_agenda, $id_usuario, $exibeImprime = ''){
		  if($banco == null)
		    $bd = new Oracle();
		  else
		    $bd = $banco;
	
		  $pdf=new FPDF('P','mm','A4');
		  $pdf->AliasNbPages();    
		      
		  $sql = new Query($bd);
		  $txt ="SELECT TO_CHAR(DDATALOGCA,'DD/MM/YYYY') DDATAAGEND, CDESCTLOGR,  P.CNENDPRES, HSSUSUA.CNOMEUSUA,  HSSLOGCA.NNUMEUSUA, 
				HSSLOGCA.NNUMEAGEND,CHORALOGCA, P.CENDEPRES, P.CNOMEPRES PRESTADOR, L.CNOMEPRES LOCAL, CNOMEESPEC, 0 NNUMEGUIA, 2, NVL(CDESCMOTCA,CMOTILOGCA),
				TO_CHAR(DCANCLOGCA,'DD/MM/YYYY HH24:MI') DMARCAGEND
				FROM HSSAGEND, FINPRES P, FINPRES L, HSSESPEC, HSSLOGCA, HSSINTER, SEGUSUA, HSSMOTCA, HSSUSUA, HSSTLOGR
				WHERE HSSLOGCA.NNUMEPRES = P.NNUMEPRES
				AND HSSLOGCA.NAMBUPRES = L.NNUMEPRES
				AND HSSAGEND.NNUMEESPEC = HSSESPEC.NNUMEESPEC(+)                      
				AND NVL(HSSAGEND.NNUMEUSUA,0) <> NVL(HSSLOGCA.NNUMEUSUA,0) --ISSO IMPEDE QUE MOSTRE UMA MESMA DATA ONDE O USUÁRIO JÁ TINHA FEITO UM AGENDAMENTO E DEPOIS CANCELOU
				AND HSSLOGCA.NNUMEUSUA = :nnumeusua
				AND HSSLOGCA.NNUMEAGEND = HSSAGEND.NNUMEAGEND(+)
				AND HSSLOGCA.NOPERUSUA = SEGUSUA.NNUMEUSUA(+)
				AND HSSLOGCA.NNUMEMOTCA = HSSMOTCA.NNUMEMOTCA(+)
				AND HSSAGEND.NNUMEAGEND = :agenda
				AND HSSLOGCA.NNUMEUSUA = HSSUSUA.NNUMEUSUA
				AND P.NNUMETLOGR = HSSTLOGR.NNUMETLOGR(+)
				ORDER BY DDATAAGEND DESC";
      $sql->addParam(":agenda",$id_agenda);
      $sql->addParam(":nnumeusua",$id_usuario);
      $sql->executeQuery($txt);
      		
		  if($sql->result("CNENDPRES") <> '')
		    $num_endereco = $sql->result("CNENDPRES");
		  else
		    $num_endereco = '';
		
		  $this->marcacao =  $sql->result("DMARCAGEND");
		  $pdf->Open();
		  $pdf->AddPage();
		  $pdf->Image('../comum/img/logo_relatorio.jpg',10,5,40,10);
		  $pdf->SetFont('Arial','B',10);
		  $pdf->Cell(45,3,'');
		
		  $pdf->Cell(125,4,$_SESSION['nome_operadora'],0,1);

		  $pdf->Ln(4);		
		  $pdf->SetFillColor(0,0,0);
		  $pdf->Cell(185,1,'',0,1,'',true);
		  $pdf->Ln(2);
		
		  $pdf->Cell(43,5,utf8_decode('Protocolo número: '),0,0,'R');
		  $pdf->Cell(30,5,$sql->result("NNUMEAGEND"),0,1);
		  $pdf->Cell(43,5,'Consulta cancelada em: ',0,0,'R');
		  $pdf->Cell(50,5,$sql->result("DMARCAGEND"),0,1);
		  $pdf->SetFont('Arial','',8);
		  $pdf->Ln(3);
		  $texto = 'Sr(a) ' . $sql->result("CNOMEUSUA") . ', sua consulta foi cancelada com o(a) Dr(a). ' . $sql->result("PRESTADOR") . ' dia ' .
		           $sql->result("DDATAAGEND") . ' as ' . $sql->result("CHORALOGCA") . utf8_decode(' no endereço ') . $sql->result("CDESCTLOGR") . ', ' . $sql->result("CENDEPRES") .
			         ' ' . $num_endereco ;
				  				  
		  $pdf->MultiCell(185,5,$texto,0,'J');
		  $pdf->Ln(2);
	
		  $file='../temp/protocolo_' .$sql->result("NNUMEAGEND"). '.pdf';
		  $this->arquivopdf = $file;
		  $pdf->Output($file,'F');
		  $this->arquivo_gerado = $file;
		  $bd->close();
		  //exibe no momento da geração (mesma tela)
		  if($exibeImprime == 'E')			
			  echo "<HTML><SCRIPT>document.location='$file';</SCRIPT></HTML>";
        //exibe, porém em uma nova janela
		  else if($exibeImprime == 'I')
			  echo "<HTML><SCRIPT>window.open('$file');</SCRIPT></HTML>";
	}
	function enviaProtocoloCancelamento($email){
      $func  = new Funcao();
      $datas = new Data();

		  $resultado = '1';
		  $sql_email = new Query($bd);																	
	      $txt_email = "UPDATE HSSUSUA SET CMAILUSUA = :emailuser WHERE NNUMEUSUA = :nnumeusua";		   
		  $sql_email->addParam(":nnumeusua",$_SESSION["id_usuario"]);
		  $sql_email->addParam(":emailuser",$email);				
		  $sql_email->executeSQL($txt_email);
		  
		  if(file_exists($this->arquivo_gerado)){
				
        if ($_SESSION['apelido_operadora'] == 'clinipam')
          $logo = "<img src=\"http://200.175.6.50:1000/solusweb_teste/comum/imagens/logo_agenda.jpg\" width=\"136\" height=\"104\"/>";
        else
          $logo = '';
  
				$msg = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
				        <html xmlns=\"http://www.w3.org/1999/xhtml\">
				        <head>
				          <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
				          <title>Protocolo de Cancelamento</title>
				          <style type=\"text/css\">
				            .titulo {
					            font: normal 14px Verdana, Geneva, sans-serif;
					            font-weight:bold;
				            }
				
                    .texto {
                      font: normal 14px Verdana, Geneva, sans-serif;
                      font-weight:bold;
                      color:#060;
                    }
                    
                    .texto1 {
                      font: normal 12px Verdana, Geneva, sans-serif;
                      color:#060;
                    }
				          </style>
				        </head>				
				        <body>
				          <table width=\"700\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				            <tr>
					            <td width=\"148\" height=\"72\" align=\"left\">".$logo."</td>
					            <td width=\"552\" class=\"titulo\">".$_SESSION['nome_operadora']."</td>
				            </tr>
				            <tr>
					            <td>&nbsp;</td>
					            <td>&nbsp;</td>
				            </tr>  
				            <tr>
					            <td>&nbsp;</td>
					            <td>
                        <p class=\"texto\">" . $datas->boasVindas() . " , " . $_SESSION['nome_usuario'] . "</p>
					              <p class=\"texto1\">Anexo segue seu protocolo de cancelamento realizado em " . $this->marcacao . "</p>
                      </td>
				            </tr>				
				          </table>
				        </body>
				      </html>";	
										
				$arquivo_anexo[0]['arquivo'] = $this->arquivo_gerado; 
				$arquivo_anexo[0]['name']    = basename($this->arquivo_gerado);
				$arquivo_anexo[0]['type']    = 'pdf';
        
				$resultado = $func->enviaEmail($email,$_SESSION['nome_operadora']."  - Cancelamento de consulta",$msg,$arquivo_anexo);	
      }      
      
			return $resultado;
	  }
	}

?>