<?php
  Session_start();

  require_once("../comum/sessao.php");
  require_once("../comum/autoload.php");
  
  $bd = new Oracle();  
  
  require_once("../comum/layoutJanela.php"); 
  $tpl->addFile("JANELA_CONTEUDO","../comum/localizaTitular.htm"); 
  $tpl->block("MOSTRA_LOGO");
  
  $nome       = '';  
  $qtde       = 20;
  $carteira   = '';
    
  if (isset($_POST['localizar'])) {
    $nome     = strtoupper($seg->antiInjection($_POST['nome']));   
    $carteira = $seg->antiInjection($_POST['carteira']);
    $qtde     = $seg->antiInjection($_POST['qtde']);
    
    if (($nome <> '') or ($carteira <> '')) {
      $sql = new Query($bd);
      $txt = "SELECT * FROM (
              SELECT NNUMEUSUA,REPLACE(CNOMEUSUA,'''','') CNOMEUSUA,
                     TO_CHAR(DNASCUSUA,'DD/MM/YYYY') DNASCUSUA,CCARTUSUA,
                     DECODE(CSITUUSUA,'M','Migrado','C','Cancelado','Ativo') CSITUUSUA,IDADE(DNASCUSUA,SYSDATE) IDADE,
                     CDINDUSUA,CCODIPLAN,TO_CHAR(NVL(DALIBUSUA,DSITUUSUA),'DD/MM/YYYY') DALIBUSUA 
                FROM HSSUSUA,HSSPLAN,HSSTITU 
               WHERE HSSUSUA.NNUMEUSUA > 0 ";
          
      if ($nome <> '') {
        $nome2 = $nome;
        $nome2 = strtoupper($formata->somenteCaracteres($nome2))."%"; 
        $txt .= "   AND HSSUSUA.CNOMEUSUA LIKE :nome";
        $sql->addParam(":nome",$nome2);
      }
        
      if ($carteira <> '') {
        $carteira2 = $carteira;
        $carteira2 = $carteira2."%";
        $txt .= "   AND (CCODIUSUA LIKE :carteira OR CCARTUSUA LIKE :carteira) ";
        $sql->addParam(":carteira",$carteira2);      
      }
  
      $txt .= "   AND CTIPOUSUA IN ('T','F')
                  AND CSITUUSUA = 'A'
                  AND HSSUSUA.NNUMETITU = :contrato ";      
      $sql->addParam(":contrato",$_SESSION['id_contrato']);                          
                      
      $txt .= "   AND HSSUSUA.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                  AND HSSUSUA.NNUMETITU = HSSTITU.NNUMETITU ";
                  
      if (isset($_SESSION['id_estabelecimento']) and $_SESSION['id_estabelecimento'] > 0 ) {
        $txt .= "   AND HSSTITU.NNUMEESTAB = :estabelecimento ";
        $sql->addParam(":estabelecimento",$_SESSION['id_estabelecimento']);                                  
      }
      
      $txt .= " UNION ALL 
               SELECT NNUMEUSUA,REPLACE(CNOMEATCAD,'''','') CNOMEUSUA,
                      TO_CHAR(DNASCATCAD,'DD/MM/YYYY') DNASCUSUA,' ' CCARTUSUA,
                      'Ativo' CSITUUSUA,IDADE(DNASCATCAD,SYSDATE) IDADE,
                      '' CDINDUSUA,CCODIPLAN,NULL DALIBUSUA 
                 FROM HSSATCAD,HSSPLAN,HSSTITU 
                WHERE HSSATCAD.NNUMEATCAD > 0 ";  

      if ($nome <> '') {
        $nome2 = $nome;
        $nome2 = strtoupper($formata->somenteCaracteres($nome2))."%"; 
        $txt .= "   AND HSSATCAD.CNOMEATCAD LIKE :nome";
        $sql->addParam(":nome",$nome2);
      }
        
      if ($carteira <> '') {
        $carteira2 = $carteira;
        $carteira2 = $carteira2."%";
        $txt .= "   AND CCHAPATCAD LIKE :carteira ";
        $sql->addParam(":carteira",$carteira2);      
      }
      
      $txt .= "   AND CTIPOATCAD IN ('F','T')
                  AND CFLAGATCAD IS NULL
                  AND COPERATCAD = 'I'
                  AND HSSATCAD.NNUMETITU = :contrato ";      
      $sql->addParam(":contrato",$_SESSION['id_contrato']);              
    
      $txt .= "   AND HSSATCAD.NNUMEPLAN = HSSPLAN.NNUMEPLAN 
                  AND HSSATCAD.NNUMETITU = HSSTITU.NNUMETITU ";
                  
      if (isset($_SESSION['id_estabelecimento']) and $_SESSION['id_estabelecimento'] > 0 ) {
        $txt .= "   AND HSSTITU.NNUMEESTAB = :estabelecimento ";
        $sql->addParam(":estabelecimento",$_SESSION['id_estabelecimento']);                                  
      }         
                                    
      $txt .= ") WHERE ROWNUM <= :qtde
                ORDER BY CNOMEUSUA ";            

      $sql->addParam(":qtde",$qtde);                    
      $sql->executeQuery($txt);

      while (!$sql->eof()) {
        $situacao =$sql->result("CSITUUSUA");        
        
        if (($situacao <> "Ativo") and (strtotime($data->dataInvertida($sql->result('DALIBUSUA'))) > strtotime(date('Y/m/d'))))
          $situacao = "Ativo";

        if ($situacao == "Ativo")
          $tpl->USUARIO_COR = "#000000";
        else
          $tpl->USUARIO_COR = "#FF0000";
                                                             
        $tpl->USUARIO_ID         = $sql->result('NNUMEUSUA');
        $tpl->USUARIO_NOME       = $formata->initCap($sql->result('CNOMEUSUA'));
        $tpl->USUARIO_CODIGO     = $sql->result("CCODIUSUA");
        $tpl->USUARIO_PLANO      = $sql->result("CCODIPLAN");
        $tpl->USUARIO_NASCIMENTO = $sql->result("DNASCUSUA");
        $tpl->USUARIO_SITUACAO   = $situacao;

        $tpl->block("USUARIO");
        $sql->next();
      }
    }
    
    $tpl->block("RESULTADO");    
  }
  
  $tpl->NOME     = $nome;   
  $tpl->CARTEIRA = $carteira;
  $tpl->QTDE     = $qtde;
  
  $bd->close();
  $tpl->show();  
?>