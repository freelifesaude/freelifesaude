<?php 
  require_once("../comum/autoload.php");
    
  $seg->secureSessionStart();
  
  $bd = new Oracle();
        
  if (isset($_POST['rede'])){
  
    $rede = $seg->antiInjection($_POST['rede']);
  
    $padrao = "Todos os planos";
   
    $opcao = '<option value="">'.$padrao.'</option>';  
        
    $sql = new Query($bd);
    
    if (($_SESSION['apelido_operadora'] == 'vitallis') or ($_SESSION['apelido_operadora'] == 'sampmg')) {
      $txt = "SELECT DISTINCT NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) NNUMEPLAN,CDESCPLAN || ' - Reg: ' || NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) CDESCPLAN
                FROM HSSPLAN,HSSREDPL
               WHERE NVL(CSITUPLAN,'A') <> 'C' 
                 AND HSSPLAN.NNUMEPLAN = HSSREDPL.NNUMEPLAN
                 AND HSSREDPL.NNUMEREDEA = :rede
               ORDER BY CDESCPLAN";    
      $sql->addParam(":rede",$rede);        
    }
    else {    
      $txt = "SELECT DISTINCT HSSPLAN.NNUMEPLAN,CCODIPLAN || ' - ' || CDESCPLAN || ' - Reg: ' || NVL(TO_CHAR(NRGMSPLAN),CANTIPLAN) CDESCPLAN
                FROM HSSPLAN
               WHERE NVL(CSITUPLAN,'A') <> 'C'
               ORDER BY CDESCPLAN";
    }
    
    $sql->executeQuery($txt);  

    if ($sql->count() > 0) {		                    
      while (!$sql->eof()) {
      
        if ($sql->count() == 1)          
          $opcao .= '<option selected value="'.$sql->result("NNUMEPLAN").'">'.$sql->result("CDESCPLAN").'</option>'; 
        else
          $opcao .= '<option value="'.$sql->result("NNUMEPLAN").'">'.$sql->result("CDESCPLAN").'</option>'; 
              
        $sql->next();
      }
    }

    echo utf8_encode($opcao);
  }
  else
    echo utf8_encode('<option value="">Todos os planos</option>');

  $bd->close();  
?>