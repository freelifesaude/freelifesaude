<?php
  require_once("../comum/autoload.php");    
  $seg->secureSessionStart();  
  require_once("../comum/sessao.php");

  define("FPDF_FONTPATH", "../comum/pdf/font");
  require_once("../comum/pdf/fpdf.php");  
    
  $bd = new Oracle();
    
  $_SESSION['titulo'] = "DEMONSTRATIVO DE QUITA��O DE D�BITOS";

  require_once("../comum/layout.php"); 
  $tpl->addFile("CONTEUDO","../comum/irpf.htm");
  
  $sql_ano = new Query($bd);  
  $txt = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'YYYY'))-1 ANO FROM DUAL";
  $sql_ano->executeQuery($txt);

  $ano = $sql_ano->result("ANO");
  $ano2 = $sql_ano->result("ANO") - 3;
  
  if (($_SESSION['apelido_operadora'] == 'casse') and ($ano2 < 2013))
    $ano2 = 2013;  

  while ($ano >= $ano2) {
    $tpl->ANO = $ano;
    $tpl->block("ANOS");
    $ano = $ano - 1;
  }
  
  if (isset($_POST['listar'])) {

    $usuario  = 0;
    $contrato = $_SESSION['id_contrato'];

    $sql = new Query($bd);    
    $txt = "SELECT DISTINCT TO_CHAR(DPAGAPAGA,'YYYY') ANO 
              FROM HSSPAGA,HSSLOPG
             WHERE NNUMETITU = :contrato
               AND NNUMEUSUA IS NULL
               AND CPAGOPAGA = 'S' 
               AND DCANCPAGA IS NULL
               AND DPARCPAGA IS NULL
               AND TO_CHAR(DPAGAPAGA,'YYYY') = :ano 
               AND HSSPAGA.NNUMELOPG = HSSLOPG.NNUMELOPG
               AND HSSLOPG.CQUITLOPG = 'S'
             ORDER BY 1 ";

    $sql->addParam(":ano",$_POST['select_ano']);
    $sql->addParam(":contrato",$contrato);      
    $sql->executeQuery($txt);

    if ($sql->count() == 0) {
      $tpl->CLASSE = "";    
      $tpl->MSG = "O contrato n�o possui pagamentos no ano selecionado.";
      $tpl->block("ERRO");
    } else {
      $filtros = array();
      $filtros['contrato']  = $contrato;
      $filtros['usuario']   = $usuario;
      $filtros['ano']       = $_POST['select_ano'];
      
      $_SESSION['filtrosQuitacao'] = $filtros;

      $sqlNpaga = new Query();
      $txtNpaga = "SELECT NNUMEPAGA
                     FROM HSSPAGA
                    WHERE NNUMETITU = :contrato
                      AND NNUMEUSUA IS NULL
                      AND CPAGOPAGA = 'N' 
                      AND DCANCPAGA IS NULL
                      AND DPARCPAGA IS NULL
                      AND DVENCPAGA BETWEEN :inicio and :final
                    ORDER BY 1 ";
      
      $sqlNpaga->addParam(":ano",$_POST['select_ano']);
      $sqlNpaga->addParam(":contrato",$contrato);  
      $sqlNpaga->addParam(":inicio","01/01/" . $_POST['select_ano']);
      $sqlNpaga->addParam(":final","31/12/" . $_POST['select_ano']);                
      $sqlNpaga->executeQuery($txtNpaga);
      
      if ($sqlNpaga->count() > 0) {
        $tpl->CLASSE = "";    
        $tpl->MSG = "N�o � poss�vel imprimir a quita��o para o exerc�cio selecionado (" . $_POST['select_ano'] . 
                    "). Favor entrar em contato com a operadora.";
        $tpl->block("ERRO");
      } else {  
        $tpl->RESULT = $util->redireciona("quitacaoImp.php?idSessao=".$_GET['idSessao'],'S','','1');
      }
    }
  }  

  $tpl->block("MOSTRA_MENU");
  $bd->close();
  $tpl->show();     

?>