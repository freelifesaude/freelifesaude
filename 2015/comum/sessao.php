<?php
  if (($_SESSION['uf_operadora'] == 'RR') or
      ($_SESSION['uf_operadora'] == 'AM') or
      ($_SESSION['uf_operadora'] == 'RO') or
      ($_SESSION['uf_operadora'] == 'MT') or
      ($_SESSION['uf_operadora'] == 'MS')) 
    date_default_timezone_set('America/Manaus');
  else if ($_SESSION['uf_operadora'] == 'AC')
    date_default_timezone_set('America/Mexico_City');
  else
    date_default_timezone_set('America/Sao_Paulo');
 

  if(isset($_GET['idSessao'])){ 
    if (($_GET['idSessao'] <> $_SESSION['idSessao']) or 
        ($_SESSION['idSessao'] == '') or
        ($_SESSION['sistemaLogado'] <> $_SESSION['sistema']) ) {
      if (!headers_sent()) {
        if ($_SESSION['diretorio'] <> '')
          header('Location: '.$_SESSION['diretorio'].'/index.php?erro=-1');
        else
          header('Location: ../index.html');
      }
      else {
        echo '<script type="text/javascript">
                window.location.href="index.php?erro=-1";
              </script>
              <noscript>
                <meta http-equiv="refresh" content="0;url=../comum/browser/sessao.html" />
              </noscript> ';
      }      
    }
  }	
?>