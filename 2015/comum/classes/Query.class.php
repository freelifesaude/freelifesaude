<?php 
//  ini_set('display_errors','off'); 
  //ini_set  ("error_reporting","E_ALL & ~E_NOTICE");

  class Query {
    private $connect;    
    private $transaction;
    private $binds = array();    
    private $lobData;
    private $lobParam;
    private $results;
    private $count;
    private $start = 0;
    private $pos = 0;
    private $end = 0;
    private $close;
    private $bd;
    
    public function Query($bd="") {
    
      if ($bd == '') {
        $bd = new Oracle();
        $this->close = true;        
      } else
        $this->close = false;

      $this->bd = $bd;
      $this->connect = $this->bd->conn();        
      $this->transaction = $bd->inTransaction();      
    }
        
    public function executeQuery($sql) {    
      try {
        $stmt = oci_parse($this->connect,$sql);
        
        for ($b = 0;$b < sizeof($this->binds);$b++) {
          oci_bind_by_name($stmt,$this->binds[$b][0],$this->binds[$b][1]);     
        }

        if (oci_execute($stmt,OCI_DEFAULT)) {
          $this->count = oci_fetch_all($stmt,$this->results);
          $this->pos = 0;
          $this->end = $this->count;
        } else {
          $this->count = 0;              
        }
        
        $e = oci_error($stmt);      
        
        if ($e <> '')
          throw new Exception($e['message']);        
        
      } 
      catch (Exception $erro) {          
        $last = array();
        $last = error_get_last();
        $erroQuery = "Function: executeQuery".chr(13).chr(10).
                     "Erro Oracle: ".chr(13).chr(10).
                     htmlentities($erro).chr(13).chr(10).
                     "Mensagem: ".$last['message'].chr(13).chr(10). 
                     "Arquivo: ".$last['file'].chr(13).chr(10). 
                     "Linha: ".$last['line'].chr(13).chr(10).chr(13).chr(10).
                     "SQL: " . $sql.chr(13).chr(10). 
                     "Par�metros: ".chr(13).chr(10).
                     $this->printParams(true,$sql);
        gravaErro("Query.class.php",$erroQuery);  
        exit;
      }
      
      if ($this->close)
        $this->bd->close();
    }

    public function executeSQL($sql) {
      try {
        $this->stmt = oci_parse($this->connect,$sql);
               
        if ($this->lobParam <> '') {
          $plob = oci_new_descriptor($this->connect, OCI_D_LOB);          
          oci_bind_by_name($this->stmt, $this->lobParam, $plob, -1, OCI_B_BLOB); 
        }
                        
        for ($b = 0;$b < sizeof($this->binds);$b++) {        
          oci_bind_by_name($this->stmt,$this->binds[$b][0],$this->binds[$b][1],$this->binds[$b][2]);
        }
        
        if (($this->transaction) or ($this->lobParam <> ''))
          $error = !oci_execute($this->stmt,OCI_DEFAULT);
        else
          $error = !oci_execute($this->stmt);
                 
        if ($error) {
          $e = oci_error($this->stmt);    
          throw new Exception($e['message']);          
        }  
                 
        if ($this->lobParam <> '') {
          $plob->save($this->lobData);          
       
          if ($error)
            oci_rollback($this->connect);              
          else
            oci_commit($this->connect);                      
            
          $plob->free();        
        }

        oci_free_statement($this->stmt);           
      } 
      catch (Exception $erro) {   
        $last = array();
        $last = error_get_last();
	      $seg = new Seguranca();
        $erroQuery = "Function: executeSQL".chr(13).chr(10).
                     "Erro Oracle: ".chr(13).chr(10).
                     htmlentities($erro).chr(13).chr(10).
                     "Mensagem: ".$last['message'].chr(13).chr(10). 
                     "Arquivo: ".$last['file'].chr(13).chr(10). 
                     "Linha: ".$last['line'].chr(13).chr(10).chr(13).chr(10).
                     "SQL: " . $sql.chr(13).chr(10). 
                     "Par�metros: ".chr(13).chr(10).
                     $this->printParams(true,$sql);
                     
        //echo $erroQuery;                     
					 
        gravaErro("Query.class.php",$erroQuery);
        exit;
      }
    }
    
    public function inTransaction() {
      return $this->transaction;
    }    

    public function getReturn ($bind) {
      $indice = -1;
      
      for ($b = 0;$b < sizeof($this->binds);$b++) {
        if ($this->binds[$b][0] == $bind) {
          $indice = $b;        
        }
      }

      return $this->binds[$indice][1];
    }  

    public function printParams ($return=false,$sql = '') {
    
      $t = '';    

      $t = "SQL original: ".chr(13).chr(10).chr(13).chr(10).$sql.chr(13).chr(10).chr(13).chr(10);    
      
      if ($sql <> '') {
        $sql2 = $sql;
        
        for ($b = 0;$b < sizeof($this->binds);$b++) {
          $sql2 = str_replace($this->binds[$b][0],"'".$this->binds[$b][1]."'",$sql2);
        }   

        $t .= "SQL com par�metros: ".chr(13).chr(10).chr(13).chr(10).$sql2.chr(13).chr(10).chr(13).chr(10);       
      }
                                    
      $t .= "Par�metros: ".chr(13).chr(10).chr(13).chr(10);       

      for ($b = 0;$b < sizeof($this->binds);$b++) {
        $t .= $this->binds[$b][0]." = ".$this->binds[$b][1].chr(13).chr(10);
      }
      
      if ($return)
        return $t;
      else {
        $t = str_replace(chr(13).chr(10),"<br>",$t);
        echo $t;
      }        
    }  
    
    public function getParams () {     
      return $this->binds;
    }      
    
    public function setParams ($arr) {     
      $this->binds = $arr;
    }     

    public function addLob($param,$value) {
      $this->lobParam = $param;
      $this->lobData  = $value;
    }
    
    public function addParam($bind,$value,$return=-1) { 
      $seg = new Seguranca();
            
      array_push($this->binds,array($bind,trim($value),$return));
      //array_push($this->binds,array($bind,trim($value),$return));
    }
    
	  //passo o FALSE para o m�todo antiInjection para n�o converter acentos em caracteres html
	  public function addParam2($bind,$value,$return=-1) { 
      $seg = new Seguranca();
            
      array_push($this->binds,array($bind,$seg->antiInjection(trim($value),false),$return));
      //array_push($this->binds,array($bind,trim($value),$return));
    }
    
    public function removeParam($bind) { 
      $temp = array();
      
      for ($b = 0;$b < sizeof($this->binds);$b++) {
        if ($this->binds[$b][0] !== $bind)
          $temp[] = $this->binds[$b];
      }
      
      $this->binds = $temp;      
    }
    
    public function eof() {
      if ($this->pos == $this->count)
        $value = true;
      else
        $value = false;   
      
      return $value;
    }
    
    public function next() {
      if ($this->pos < $this->count)    
        $this->pos = $this->pos + 1;  
    }
    
    public function result($column) {
    
      if (sizeOf($this->results) > 0) {
        if (array_key_exists($column, $this->results)) {
          if (array_key_exists($this->pos, $this->results[$column]))
            return $this->results[$column][$this->pos];
          else
            return "";
        }
        else
          return "";
      }
      else
        return "";
    }
    
    public function count() {
      return $this->count;
    }
    
    public function first() {
      $this->pos = 0;
    }

    public function end() {
      $this->pos = $this->count-1;
    }
    
    public function clear() {
      $this->count = 0;    
      $this->binds = array();
    }      
  }
?>